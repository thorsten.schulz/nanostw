#!/bin/bash

# This part becomes relevant, when working with QEMU.
# It may fully differ on real setups

if [[ $UID != 0 ]]; then
	echo "Please run this network script as root!"
	exit 1
fi

echo "If you see errors, call network-qemu.sh first."

ip addr del 172.21.0.10/20 dev br-up
ip addr add 172.21.0.10/24 dev wg-up     #core GG instance
wg addconf wg-up ../config/wg?gg.conf
ip link set wg-up up
#tell the kernel where to find our peers, there is a bit of routing necessary
#only add this for one train
ip route add 172.21.3.0/24 via 172.21.0.103 dev wg-up
ip route add 172.21.2.0/24 via 172.21.0.102 dev wg-up
#ip route add 172.21.1.0/24 via 172.21.0.101 dev wg-up

