SRC_URI_append = " \
    file://chrony.root.conf \
    file://estw_bash_history \
    "

do_install_append() {
    install -v -d ${D}/etc/chrony
    install -v -m 644 ${WORKDIR}/chrony.root.conf      ${D}/etc/chrony/

    cat ${WORKDIR}/estw_bash_history >> ${D}/root/.bash_history
}
