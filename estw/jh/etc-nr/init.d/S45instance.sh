#!/bin/sh

export instance=$(sed 's/.*instance=\([^][,[:space:]]*\).*/\1/' < /proc/cmdline)

echo "Configuring instance $instance"
i_script="/etc/instance/$instance"

[ -f "$i_script" ] && . $i_script || echo "$i_script not found"

