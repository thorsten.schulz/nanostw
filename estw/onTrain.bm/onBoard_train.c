/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "onBoard_train.h"

/* train::onBoard/ */
void onBoard_train(
  /* _L431/, self/ */
  TrainId_TIU self,
  /* turnOff/ */
  kcg_bool turnOff,
  /* trip/ */
  kcg_bool trip,
  /* ctrl/ */
  AccelerationUnit_TIU ctrl,
  /* selectDir/ */
  M_directioncontroller_command_T_TIU selectDir,
  /* _L373/, dT/ */
  TimeUnit_TIU dT,
  /* _L439/, dOdo/ */
  VelocityUnit_TIU dOdo,
  /* _L441/, dirOdo/ */
  VelocityUnit_TIU dirOdo,
  /* iv/ */
  InterventionCommand_TIU *iv,
  /* correctionPoint/ */
  TrackLocation_TIU *correctionPoint,
  /* param/ */
  TrainParam_TIU *param,
  /* modelPWM/ */
  VelocityUnit_TIU *modelPWM,
  /* _L400/, report/ */
  TrainReport_TIU *report,
  /* send_report/ */
  kcg_bool *send_report,
  /* @3/_L15/, @3/bitmap/, _L415/, packedFlags/ */
  kcg_uint8 *packedFlags,
  /* _L438/, isParamUpdate/, paramChanged/ */
  kcg_bool *paramChanged,
  /* ivState/ */
  array_bool_5 *ivState,
  outC_onBoard_train *outC)
{
  kcg_int16 tmp;
  kcg_int32 tmp1;
  kcg_bool tmp2;
  kcg_bool tmp3;
  kcg_bool tmp4;
  kcg_uint8 tmp5;
  kcg_uint8 tmp6;
  kcg_uint8 tmp7;
  kcg_uint8 tmp8;
  InterventionCommand_TIU tmp9;
  VelocityUnit_TIU tmp10;
  kcg_bool tmp11;
  kcg_bool tmp12;
  /* _L440/ */
  kcg_int16 _L440;
  /* @3/_L1/, @3/flag/, _L390/ */
  StatusWord_TIU _L390;
  /* _L404/, _L406/ */
  kcg_bool _L404;
  /* distance/ */
  LengthUnit_TIU distance;
  /* Physics: */
  SSM_ST_Physics Physics_state_act;
  /* Control: */
  SSM_TR_Control Control_fired_strong;
  /* Control: */
  kcg_bool Control_reset_act;
  /* Control: */
  SSM_ST_Control Control_state_act;
  /* Control:, Control:off:<1>, Physics:preCharging:<1>, _L392/, stopped/ */
  kcg_bool tr_1_clock_off_Control;
  /* @5/IfBlock1:,
     Control:off:<1><1>,
     Physics:preCharging:<2>,
     Physics:rolling:odo_sim:,
     Physics:rolling:odo_sim:odo:<1>,
     _L430/,
     isTripped/ */
  kcg_bool tr_1_guard_1_off_Control;
  /* Control:active:InitialPath:initialOnSight:<2> */
  kcg_bool tr_2_clock_initialOnSight_InitialPath_active_Control;
  /* Control:active:InitialPath:, Control:active:InitialPath:Level_2:<1> */
  kcg_bool tr_1_clock_Level_2_InitialPath_active_Control;
  /* Control:active:TripState:, Control:active:TripState:normal:<1> */
  kcg_bool TripState_reset_act_partial_active_Control;
  /* Control:active:TripState:funcTrip:<2> */
  kcg_bool tr_2_guard_funcTrip_TripState_active_Control;
  /* Control:active:TripState:funcTrip:<1> */
  kcg_bool tr_1_guard_funcTrip_TripState_active_Control;
  /* Control:active:Direction:, Control:active:Direction:CabA:<1> */
  kcg_bool tr_1_clock_CabA_Direction_active_Control;
  /* Control:active:Direction:, Control:active:Direction:CabB:<1> */
  kcg_bool tr_1_clock_CabB_Direction_active_Control;
  /* Control:active:, Control:active:TripState: */
  kcg_bool active_weakb_clock_Control;
  /* Control:active:TripState: */
  SSM_ST_TripState_active_Control TripState_clock_active_Control;
  /* Control:active:Direction: */
  SSM_ST_Direction_active_Control Direction_state_sel_active_Control;
  /* Control:active:Direction: */
  SSM_ST_Direction_active_Control Direction_state_act_active_Control;
  /* Control:active:TripState: */
  SSM_ST_TripState_active_Control TripState_state_act_active_Control;
  /* Control:active:InitialPath: */
  SSM_ST_InitialPath_active_Control InitialPath_state_sel_active_Control;
  /* Control:active:InitialPath: */
  SSM_ST_InitialPath_active_Control InitialPath_state_act_active_Control;
  /* @4/_L23/, @4/o/, Control:active:_L34/, locachion/ */
  TrackLocation_TIU _L34_active_Control;
  /* @4/location/, @5/o/, Control:active:_L32/ */
  TrackLocation_TIU _L32_active_Control;
  /* Control:active:_L40/,
     Control:active:_L50/,
     Control:active:_L53/,
     Control:active:haveIV/ */
  kcg_bool _L40_active_Control;
  /* Physics:rolling:odo_sim: */
  SSM_ST_odo_sim_rolling_Physics odo_sim_state_sel_rolling_Physics;
  /* Physics:rolling:odo_sim: */
  SSM_ST_odo_sim_rolling_Physics odo_sim_state_act_rolling_Physics;
  /* Physics:rolling:_L12/, Physics:rolling:_L40/, a/ */
  kcg_int16 _L12_rolling_Physics;
  /* @1/_/v4/, @2/_/v4/ */
  kcg_size v4_times_4_size;
  /* @5/IfBlock1:then:_L2/, @6/_L6/, @6/dist/ */
  LengthUnit_TIU _L6_alignDistance_2_correctTrack_5;

  _L440 = dOdo * dirOdo;
  /* Physics: */
  switch (outC->Physics_state_nxt) {
    case SSM_st_preCharging_Physics :
      if (outC->Physics_reset_act) {
        outC->init = kcg_true;
      }
      /* @2/_/v4= */
      if (outC->init) {
        v4_times_4_size = 20;
      }
      else {
        v4_times_4_size = outC->v3_times_11_size;
      }
      /* @2/_/v3= */
      if (v4_times_4_size < 0) {
        outC->v3_times_11_size = v4_times_4_size;
      }
      else {
        outC->v3_times_11_size = v4_times_4_size - 1;
      }
      tr_1_guard_1_off_Control = (outC->v3_times_11_size == 0) |
        (/* _L442= */(kcg_int32) _L440 > kcg_lit_int32(0));
      tr_1_clock_off_Control = outC->aTrac <= aDrag_train;
      if (tr_1_clock_off_Control) {
        Physics_state_act = SSM_st_standing_Physics;
      }
      else if (tr_1_guard_1_off_Control) {
        Physics_state_act = SSM_st_rolling_Physics;
      }
      else {
        Physics_state_act = SSM_st_preCharging_Physics;
      }
      outC->Physics_reset_act = tr_1_clock_off_Control | tr_1_guard_1_off_Control;
      outC->init = kcg_false;
      break;
    case SSM_st_standing_Physics :
      outC->Physics_reset_act = outC->aTrac > aDrag_train;
      if (outC->Physics_reset_act) {
        Physics_state_act = SSM_st_preCharging_Physics;
      }
      else {
        Physics_state_act = SSM_st_standing_Physics;
      }
      break;
    case SSM_st_rolling_Physics :
      outC->Physics_reset_act = (outC->aTrac <= aDrag_train) & (outC->v < vMin_train);
      if (outC->Physics_reset_act) {
        Physics_state_act = SSM_st_standing_Physics;
      }
      else {
        Physics_state_act = SSM_st_rolling_Physics;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* Control: */
  switch (outC->Control_state_nxt) {
    case SSM_st_unknown_location_reset_on_powercycle_Control :
      Control_reset_act = kcg_false;
      Control_state_act = SSM_st_unknown_location_reset_on_powercycle_Control;
      tmp3 = kcg_false;
      Control_fired_strong = SSM_TR_no_trans_Control;
      tmp11 = kcg_false;
      break;
    case SSM_st_invalTrainData_Control :
      Control_reset_act = kcg_true;
      Control_state_act = SSM_st_off_Control;
      tmp3 = kcg_false;
      Control_fired_strong = SSM_TR_invalTrainData_off_1_invalTrainData_Control;
      tmp11 = kcg_false;
      break;
    case SSM_st_active_Control :
      Control_reset_act = kcg_false;
      Control_state_act = SSM_st_active_Control;
      tmp3 = kcg_false;
      Control_fired_strong = SSM_TR_no_trans_Control;
      tmp11 = kcg_false;
      break;
    case SSM_st_off_Control :
      tr_1_clock_off_Control = selectDir !=
        directioncontroller_command_not_defined_TIU;
      tr_1_guard_1_off_Control = ((*param).BrPc <= kcg_lit_uint16(0)) |
        ((*param).modelFactor == kcg_lit_int16(0)) | ((*param).vMax <
          (*param).vOnSight);
      Control_reset_act = tr_1_clock_off_Control;
      /* Control:off:<1> */
      if (tr_1_clock_off_Control) {
        if (tr_1_guard_1_off_Control) {
          Control_state_act = SSM_st_invalTrainData_Control;
          Control_fired_strong = SSM_TR_off_invalTrainData_1_1_off_Control;
        }
        else {
          Control_state_act = SSM_st_active_Control;
          Control_fired_strong = SSM_TR_off_active_2_1_off_Control;
        }
        /* Control:off:<1><1> */
        if (tr_1_guard_1_off_Control) {
          tmp3 = kcg_true;
          tmp11 = kcg_false;
        }
        else {
          kcg_copy_TrainParam_TIU(&outC->p, param);
          tmp3 = kcg_false;
          tmp11 = kcg_true;
        }
      }
      else {
        Control_state_act = SSM_st_off_Control;
        tmp3 = kcg_false;
        Control_fired_strong = SSM_TR_no_trans_Control;
        tmp11 = kcg_false;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* Physics: */
  switch (Physics_state_act) {
    case SSM_st_preCharging_Physics :
      *modelPWM = outC->mem_modelPWM + outC->p.modelOffset / preChargeCycles_train;
      distance = kcg_lit_int32(0);
      tr_1_clock_off_Control = kcg_false;
      outC->v = kcg_lit_int16(0);
      break;
    case SSM_st_standing_Physics :
      *modelPWM = kcg_lit_int16(0);
      distance = kcg_lit_int32(0);
      tr_1_clock_off_Control = kcg_true;
      outC->v = kcg_lit_int16(0);
      break;
    case SSM_st_rolling_Physics :
      if (outC->Physics_reset_act) {
        outC->init2 = kcg_true;
        /* Physics:rolling:_L38=(train::helper::tIntegratorHPI#5)/ */
        tIntegratorHPI_reset_train_helper(&outC->Context_tIntegratorHPI_5);
      }
      /* Physics:rolling:odo_sim:, Physics:rolling:vSimOffs/ */
      if (outC->init2) {
        tmp10 = kcg_lit_int16(0);
        odo_sim_state_sel_rolling_Physics = SSM_st_odo_odo_sim_rolling_Physics;
      }
      else {
        tmp10 = outC->vSimOffs_rolling_Physics;
        odo_sim_state_sel_rolling_Physics = outC->odo_sim_state_nxt_rolling_Physics;
      }
      _L12_rolling_Physics = outC->aTrac - (aDrag_train +
          /* Physics:rolling:_L35= */(kcg_int16)
            (/* Physics:rolling:_L28= */(kcg_int32) outC->v *
              /* Physics:rolling:_L28= */(kcg_int32) outC->v / kcg_lit_int32(
                1956525)));
      /* Physics:rolling:odo_sim: */
      switch (odo_sim_state_sel_rolling_Physics) {
        case SSM_st_odo_odo_sim_rolling_Physics :
          tr_1_guard_1_off_Control = dirOdo == kcg_lit_int16(0);
          /* Physics:rolling:odo_sim:odo:<1> */
          if (tr_1_guard_1_off_Control) {
            /* Physics:rolling:vSim/ */
            if (outC->init2) {
              tmp = kcg_lit_int16(0);
            }
            else {
              tmp = outC->_L38_rolling_Physics;
            }
            outC->vSimOffs_rolling_Physics = outC->v - tmp;
            odo_sim_state_act_rolling_Physics = SSM_st_sim_odo_sim_rolling_Physics;
          }
          else {
            outC->vSimOffs_rolling_Physics = tmp10;
            odo_sim_state_act_rolling_Physics = SSM_st_odo_odo_sim_rolling_Physics;
          }
          break;
        case SSM_st_sim_odo_sim_rolling_Physics :
          outC->vSimOffs_rolling_Physics = tmp10;
          if (dirOdo != kcg_lit_int16(0)) {
            odo_sim_state_act_rolling_Physics = SSM_st_odo_odo_sim_rolling_Physics;
          }
          else {
            odo_sim_state_act_rolling_Physics = SSM_st_sim_odo_sim_rolling_Physics;
          }
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      /* Physics:rolling:_L38=(train::helper::tIntegratorHPI#5)/ */
      tIntegratorHPI_train_helper(
        _L12_rolling_Physics,
        dT,
        kcg_true,
        vMin_train,
        &outC->_L38_rolling_Physics,
        &outC->Context_tIntegratorHPI_5);
      *modelPWM = outC->_L38_rolling_Physics / outC->p.modelFactor +
        outC->p.modelOffset;
      tr_1_clock_off_Control = kcg_false;
      /* Physics:rolling:odo_sim: */
      switch (odo_sim_state_act_rolling_Physics) {
        case SSM_st_odo_odo_sim_rolling_Physics :
          distance = /* _L442= */(kcg_int32) _L440;
          outC->v = /* Physics:rolling:odo_sim:odo:_L10= */(kcg_int16)
              (/* _L442= */(kcg_int32) _L440 * deltaTfpDivisor_train / dT);
          break;
        case SSM_st_sim_odo_sim_rolling_Physics :
          outC->v = outC->_L38_rolling_Physics + outC->vSimOffs_rolling_Physics;
          distance = /* Physics:rolling:odo_sim:sim:_L8= */(kcg_int32) outC->v *
            dT / deltaTfpDivisor_train;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  outC->mem_modelPWM = *modelPWM;
  _L390.stopped = tr_1_clock_off_Control;
  /* Control: */
  switch (Control_state_act) {
    case SSM_st_unknown_location_reset_on_powercycle_Control :
      _L404 = kcg_false;
      tmp4 = kcg_false;
      break;
    case SSM_st_invalTrainData_Control :
      _L404 = kcg_false;
      tmp4 = kcg_false;
      break;
    case SSM_st_active_Control :
      if (Control_reset_act) {
        outC->init3 = kcg_true;
        outC->init1 = kcg_true;
      }
      active_weakb_clock_Control = (!outC->init3) &
        outC->TripState_reset_act_active_Control;
      /* Control:active:Direction:,
         Control:active:InitialPath:,
         Control:active:TripState: */
      if (outC->init3) {
        TripState_clock_active_Control = SSM_st_normal_TripState_active_Control;
        Direction_state_sel_active_Control = SSM_st_CabA_Direction_active_Control;
        InitialPath_state_sel_active_Control =
          SSM_st_initialOnSight_InitialPath_active_Control;
      }
      else {
        TripState_clock_active_Control = outC->TripState_state_nxt_active_Control;
        Direction_state_sel_active_Control = outC->Direction_state_nxt_active_Control;
        InitialPath_state_sel_active_Control =
          outC->InitialPath_state_nxt_active_Control;
      }
      tr_1_guard_1_off_Control = ((*correctionPoint).sid != UnknownSid_TIU) &
        ((*correctionPoint).sid != outC->locachion.sid);
      /* @5/IfBlock1: */
      if (tr_1_guard_1_off_Control) {
        _L32_active_Control.sid = (*correctionPoint).sid;
        _L32_active_Control.location = outC->locachion.location;
        _L6_alignDistance_2_correctTrack_5 = outC->locachion.location -
          (*correctionPoint).location;
        /* @6/_L5= */
        if (((*correctionPoint).sid != UnknownSid_TIU) &
          (((*correctionPoint).sid < kcg_lit_int32(0)) ^ outC->isCabB)) {
          _L32_active_Control.distance = - _L6_alignDistance_2_correctTrack_5;
        }
        else {
          _L32_active_Control.distance = _L6_alignDistance_2_correctTrack_5;
        }
      }
      else {
        kcg_copy_TrackLocation_TIU(&_L32_active_Control, &outC->locachion);
      }
      _L34_active_Control.sid = _L32_active_Control.sid;
      _L34_active_Control.location = distance + _L32_active_Control.location;
      /* Control:active:Direction: */
      switch (Direction_state_sel_active_Control) {
        case SSM_st_CabB_Direction_active_Control :
          tr_1_clock_CabB_Direction_active_Control = tr_1_clock_off_Control &
            (selectDir == directioncontroller_command_cabA_TIU);
          if (tr_1_clock_CabB_Direction_active_Control) {
            Direction_state_act_active_Control = SSM_st_CabA_Direction_active_Control;
          }
          else {
            Direction_state_act_active_Control = SSM_st_CabB_Direction_active_Control;
          }
          break;
        case SSM_st_CabA_Direction_active_Control :
          tr_1_clock_CabA_Direction_active_Control = tr_1_clock_off_Control &
            (selectDir == directioncontroller_command_cabB_TIU);
          if (tr_1_clock_CabA_Direction_active_Control) {
            Direction_state_act_active_Control = SSM_st_CabB_Direction_active_Control;
          }
          else {
            Direction_state_act_active_Control = SSM_st_CabA_Direction_active_Control;
          }
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      /* Control:active:Direction: */
      switch (Direction_state_act_active_Control) {
        case SSM_st_CabB_Direction_active_Control :
          _L404 = kcg_true;
          break;
        case SSM_st_CabA_Direction_active_Control :
          _L404 = kcg_false;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      /* @7/_L5= */
      if ((_L32_active_Control.sid != UnknownSid_TIU) &
        ((_L32_active_Control.sid < kcg_lit_int32(0)) ^ _L404)) {
        tmp1 = - distance;
      }
      else {
        tmp1 = distance;
      }
      _L34_active_Control.distance = tmp1 + _L32_active_Control.distance;
      _L40_active_Control = ((*iv).permittedSpeed > kcg_lit_int16(0)) |
        ((*iv).interventionSpeed > kcg_lit_int16(0)) | ((*iv).targetDistance >
          kcg_lit_int32(0));
      /* Control:active:InitialPath: */
      switch (InitialPath_state_sel_active_Control) {
        case SSM_st_Level_2_InitialPath_active_Control :
          tr_1_clock_Level_2_InitialPath_active_Control = !_L40_active_Control;
          /* Control:active:InitialPath:Level_2:<1> */
          if (tr_1_clock_Level_2_InitialPath_active_Control) {
            tmp2 = kcg_true;
          }
          else {
            tmp2 = kcg_false;
          }
          break;
        case SSM_st_initialOnSight_InitialPath_active_Control :
          /* Control:active:InitialPath:initialOnSight:<1> */
          if (_L40_active_Control) {
            tmp2 = kcg_false;
          }
          else {
            tr_2_clock_initialOnSight_InitialPath_active_Control =
              _L34_active_Control.distance > DEFAULT_OS_MA_TIU;
            /* Control:active:InitialPath:initialOnSight:<2> */
            if (tr_2_clock_initialOnSight_InitialPath_active_Control) {
              tmp2 = kcg_true;
            }
            else {
              tmp2 = kcg_false;
            }
          }
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      tmp12 = ((!tr_1_clock_off_Control) & turnOff) | tmp2;
      /* Control:active:TripState: */
      switch (TripState_clock_active_Control) {
        case SSM_st_funcTrip_TripState_active_Control :
          if (active_weakb_clock_Control) {
            outC->init1 = kcg_true;
          }
          /* @1/_/v4= */
          if (outC->init1) {
            v4_times_4_size = 50;
          }
          else {
            v4_times_4_size = outC->v3_times_4_size;
          }
          /* @1/_/v3= */
          if (v4_times_4_size < 0) {
            outC->v3_times_4_size = v4_times_4_size;
          }
          else /* @1/_/v3= */
          if (tr_1_clock_off_Control) {
            outC->v3_times_4_size = v4_times_4_size - 1;
          }
          else {
            outC->v3_times_4_size = v4_times_4_size;
          }
          tr_2_guard_funcTrip_TripState_active_Control =
            tr_1_clock_off_Control & (outC->v3_times_4_size == 0) & (ctrl <=
              kcg_lit_int16(0));
          tr_1_guard_funcTrip_TripState_active_Control = trip | tmp12;
          if (tr_1_guard_funcTrip_TripState_active_Control) {
            TripState_state_act_active_Control = SSM_st_funcTrip_TripState_active_Control;
          }
          else if (tr_2_guard_funcTrip_TripState_active_Control) {
            TripState_state_act_active_Control = SSM_st_normal_TripState_active_Control;
          }
          else {
            TripState_state_act_active_Control = SSM_st_funcTrip_TripState_active_Control;
          }
          break;
        case SSM_st_normal_TripState_active_Control :
          TripState_reset_act_partial_active_Control = trip | tmp12;
          if (TripState_reset_act_partial_active_Control) {
            TripState_state_act_active_Control = SSM_st_funcTrip_TripState_active_Control;
          }
          else {
            TripState_state_act_active_Control = SSM_st_normal_TripState_active_Control;
          }
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      /* Control:active:TripState: */
      switch (TripState_state_act_active_Control) {
        case SSM_st_funcTrip_TripState_active_Control :
          tmp4 = kcg_true;
          break;
        case SSM_st_normal_TripState_active_Control :
          tmp4 = kcg_false;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      break;
    case SSM_st_off_Control :
      _L404 = kcg_false;
      tmp4 = kcg_false;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  _L390.otherDir = _L404;
  tr_1_guard_1_off_Control = tmp3 | tmp4;
  _L390.tripped = tr_1_guard_1_off_Control;
  /* Control: */
  switch (Control_state_act) {
    case SSM_st_unknown_location_reset_on_powercycle_Control :
      _L390.active = kcg_false;
      kcg_copy_TrackLocation_TIU(
        &outC->locachion,
        (TrackLocation_TIU *) &unknownLocation_TIU);
      break;
    case SSM_st_invalTrainData_Control :
      _L390.active = kcg_false;
      break;
    case SSM_st_active_Control :
      _L390.active = (!tr_1_clock_off_Control) | (!turnOff);
      kcg_copy_TrackLocation_TIU(&outC->locachion, &_L34_active_Control);
      break;
    case SSM_st_off_Control :
      _L390.active = kcg_false;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* @3/_L10= */
  if (_L390.active) {
    tmp5 = kcg_lit_uint8(1);
  }
  else {
    tmp5 = kcg_lit_uint8(0);
  }
  /* @3/_L11= */
  if (_L390.otherDir) {
    tmp6 = kcg_lit_uint8(2);
  }
  else {
    tmp6 = kcg_lit_uint8(0);
  }
  /* @3/_L12= */
  if (_L390.tripped) {
    tmp7 = kcg_lit_uint8(4);
  }
  else {
    tmp7 = kcg_lit_uint8(0);
  }
  /* @3/_L13= */
  if (_L390.stopped) {
    tmp8 = kcg_lit_uint8(8);
  }
  else {
    tmp8 = kcg_lit_uint8(0);
  }
  *packedFlags = tmp5 | tmp6 | tmp7 | tmp8;
  (*report).sid = outC->locachion.sid;
  (*report).distance = outC->locachion.distance;
  (*report).trainData.location = outC->locachion.location;
  kcg_copy_StatusWord_TIU(&(*report).trainData.flag, &_L390);
  (*report).trainData.BrPc = outC->p.BrPc;
  (*report).trainData.vMax = outC->p.vMax;
  (*report).trainData.vRelease = outC->p.vRelease;
  (*report).trainData.vOnSight = outC->p.vOnSight;
  (*report).trainData.length = outC->p.length;
  (*report).trainData.TrainId = self;
  (*report).trainData.velocity = outC->v;
  outC->t = outC->t + dT;
  (*report).trainData.time = outC->t;
  /* Physics: */
  switch (Physics_state_act) {
    case SSM_st_preCharging_Physics :
      (*report).trainData.acc = kcg_lit_int16(0);
      outC->Physics_state_nxt = SSM_st_preCharging_Physics;
      break;
    case SSM_st_standing_Physics :
      (*report).trainData.acc = kcg_lit_int16(0);
      outC->Physics_state_nxt = SSM_st_standing_Physics;
      break;
    case SSM_st_rolling_Physics :
      (*report).trainData.acc = _L12_rolling_Physics;
      /* Physics:rolling:odo_sim: */
      switch (odo_sim_state_act_rolling_Physics) {
        case SSM_st_odo_odo_sim_rolling_Physics :
          outC->odo_sim_state_nxt_rolling_Physics = SSM_st_odo_odo_sim_rolling_Physics;
          break;
        case SSM_st_sim_odo_sim_rolling_Physics :
          outC->odo_sim_state_nxt_rolling_Physics = SSM_st_sim_odo_sim_rolling_Physics;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      outC->Physics_state_nxt = SSM_st_rolling_Physics;
      outC->init2 = kcg_false;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* _L407= */
  if (_L404) {
    /* _L408= */
    if (outC->p.Ant2CabB <= kcg_lit_int32(0)) {
      (*report).trainData.Ant2Cab = outC->p.length - outC->p.Ant2CabA;
    }
    else {
      (*report).trainData.Ant2Cab = outC->p.Ant2CabB;
    }
  }
  else {
    (*report).trainData.Ant2Cab = outC->p.Ant2CabA;
  }
  /* Control: */
  switch (Control_state_act) {
    case SSM_st_unknown_location_reset_on_powercycle_Control :
      *send_report = kcg_true;
      for (v4_times_4_size = 0; v4_times_4_size < 5; v4_times_4_size++) {
        (*ivState)[v4_times_4_size] = kcg_false;
      }
      outC->aTrac = kcg_lit_int16(0);
      outC->Control_state_nxt = SSM_st_off_Control;
      tmp12 = kcg_false;
      break;
    case SSM_st_invalTrainData_Control :
      *send_report = kcg_true;
      for (v4_times_4_size = 0; v4_times_4_size < 5; v4_times_4_size++) {
        (*ivState)[v4_times_4_size] = kcg_false;
      }
      outC->aTrac = kcg_lit_int16(0);
      outC->Control_state_nxt = SSM_st_invalTrainData_Control;
      tmp12 = kcg_false;
      break;
    case SSM_st_active_Control :
      *send_report = kcg_true;
      active_weakb_clock_Control = Control_fired_strong != SSM_TR_no_trans_Control;
      /* Control:active:InitialPath: */
      switch (InitialPath_state_sel_active_Control) {
        case SSM_st_Level_2_InitialPath_active_Control :
          if (tr_1_clock_Level_2_InitialPath_active_Control) {
            InitialPath_state_act_active_Control =
              SSM_st_initialOnSight_InitialPath_active_Control;
          }
          else {
            InitialPath_state_act_active_Control =
              SSM_st_Level_2_InitialPath_active_Control;
          }
          break;
        case SSM_st_initialOnSight_InitialPath_active_Control :
          if (_L40_active_Control) {
            InitialPath_state_act_active_Control =
              SSM_st_Level_2_InitialPath_active_Control;
          }
          else {
            InitialPath_state_act_active_Control =
              SSM_st_initialOnSight_InitialPath_active_Control;
          }
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      /* Control:active:InitialPath: */
      switch (InitialPath_state_act_active_Control) {
        case SSM_st_Level_2_InitialPath_active_Control :
          outC->InitialPath_state_nxt_active_Control =
            SSM_st_Level_2_InitialPath_active_Control;
          break;
        case SSM_st_initialOnSight_InitialPath_active_Control :
          outC->InitialPath_state_nxt_active_Control =
            SSM_st_initialOnSight_InitialPath_active_Control;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      if (Control_reset_act) {
        /* Control:active:_L44=(train::traction#6)/ */
        traction_reset_train(&outC->Context_traction_6);
      }
      /* Control:active:_L47=, Control:active:_L52= */
      if (_L40_active_Control) {
        kcg_copy_InterventionCommand_TIU(&tmp9, iv);
        tmp10 = outC->p.vMax;
      }
      else {
        kcg_copy_InterventionCommand_TIU(
          &tmp9,
          (InterventionCommand_TIU *) &emptyIV_TIU);
        tmp10 = outC->p.vOnSight;
      }
      /* Control:active:_L44=(train::traction#6)/ */
      traction_train(
        ctrl,
        &tmp9,
        tr_1_guard_1_off_Control,
        outC->v,
        /* Control:active:_L41= */(kcg_int16) outC->p.BrPc,
        tmp10,
        &outC->aTrac,
        ivState,
        &outC->Context_traction_6);
      /* Control:active:TripState: */
      switch (TripState_state_act_active_Control) {
        case SSM_st_funcTrip_TripState_active_Control :
          outC->TripState_state_nxt_active_Control =
            SSM_st_funcTrip_TripState_active_Control;
          break;
        case SSM_st_normal_TripState_active_Control :
          outC->TripState_state_nxt_active_Control =
            SSM_st_normal_TripState_active_Control;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      /* Control:active:Direction: */
      switch (Direction_state_act_active_Control) {
        case SSM_st_CabB_Direction_active_Control :
          outC->Direction_state_nxt_active_Control = SSM_st_CabB_Direction_active_Control;
          break;
        case SSM_st_CabA_Direction_active_Control :
          outC->Direction_state_nxt_active_Control = SSM_st_CabA_Direction_active_Control;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      /* Control:active: */
      if (active_weakb_clock_Control) {
        outC->Control_state_nxt = SSM_st_active_Control;
      }
      else if (turnOff & tr_1_clock_off_Control) {
        if (_L34_active_Control.sid == UnknownSid_TIU) {
          outC->Control_state_nxt = SSM_st_unknown_location_reset_on_powercycle_Control;
        }
        else {
          outC->Control_state_nxt = SSM_st_off_Control;
        }
      }
      else {
        outC->Control_state_nxt = SSM_st_active_Control;
      }
      /* Control:active:Direction: */
      switch (Direction_state_sel_active_Control) {
        case SSM_st_CabB_Direction_active_Control :
          /* Control:active:Direction:CabB:<1> */
          if (tr_1_clock_CabB_Direction_active_Control) {
            tmp12 = kcg_true;
          }
          else {
            tmp12 = kcg_false;
          }
          break;
        case SSM_st_CabA_Direction_active_Control :
          /* Control:active:Direction:CabA:<1> */
          if (tr_1_clock_CabA_Direction_active_Control) {
            tmp12 = kcg_true;
          }
          else {
            tmp12 = kcg_false;
          }
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      outC->init3 = kcg_false;
      /* Control:active:TripState: */
      switch (TripState_clock_active_Control) {
        case SSM_st_funcTrip_TripState_active_Control :
          outC->TripState_reset_act_active_Control =
            tr_1_guard_funcTrip_TripState_active_Control |
            tr_2_guard_funcTrip_TripState_active_Control;
          outC->init1 = kcg_false;
          break;
        case SSM_st_normal_TripState_active_Control :
          outC->TripState_reset_act_active_Control =
            TripState_reset_act_partial_active_Control;
          break;
        default :
          /* this default branch is unreachable */
          break;
      }
      break;
    case SSM_st_off_Control :
      *send_report = kcg_false;
      for (v4_times_4_size = 0; v4_times_4_size < 5; v4_times_4_size++) {
        (*ivState)[v4_times_4_size] = kcg_false;
      }
      outC->aTrac = kcg_lit_int16(0);
      outC->Control_state_nxt = SSM_st_off_Control;
      tmp12 = kcg_false;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  *paramChanged = tmp11 | tmp12;
  outC->isCabB = _L404;
}

#ifndef KCG_USER_DEFINED_INIT
void onBoard_init_train(outC_onBoard_train *outC)
{
  outC->Physics_reset_act = kcg_false;
  outC->isCabB = kcg_false;
  outC->TripState_reset_act_active_Control = kcg_true;
  outC->init = kcg_true;
  outC->init1 = kcg_true;
  outC->init2 = kcg_true;
  outC->init3 = kcg_true;
  outC->v3_times_11_size = 0;
  outC->v3_times_4_size = 0;
  outC->_L38_rolling_Physics = kcg_lit_int16(0);
  outC->vSimOffs_rolling_Physics = kcg_lit_int16(0);
  outC->odo_sim_state_nxt_rolling_Physics = SSM_st_sim_odo_sim_rolling_Physics;
  outC->InitialPath_state_nxt_active_Control =
    SSM_st_initialOnSight_InitialPath_active_Control;
  outC->TripState_state_nxt_active_Control =
    SSM_st_normal_TripState_active_Control;
  outC->Direction_state_nxt_active_Control = SSM_st_CabA_Direction_active_Control;
  /* Control:active:_L44=(train::traction#6)/ */
  traction_init_train(&outC->Context_traction_6);
  /* Physics:rolling:_L38=(train::helper::tIntegratorHPI#5)/ */
  tIntegratorHPI_init_train_helper(&outC->Context_tIntegratorHPI_5);
  outC->t = kcg_lit_int32(0);
  kcg_copy_TrackLocation_TIU(
    &outC->locachion,
    (TrackLocation_TIU *) &unknownLocation_TIU);
  outC->mem_modelPWM = kcg_lit_int16(0);
  kcg_copy_TrainParam_TIU(&outC->p, (TrainParam_TIU *) &emptyParam_TIU);
  outC->Control_state_nxt = SSM_st_off_Control;
  outC->v = kcg_lit_int16(0);
  outC->aTrac = kcg_lit_int16(0);
  outC->Physics_state_nxt = SSM_st_standing_Physics;
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void onBoard_reset_train(outC_onBoard_train *outC)
{
  outC->Physics_reset_act = kcg_false;
  outC->isCabB = kcg_false;
  outC->init = kcg_true;
  outC->init1 = kcg_true;
  outC->init2 = kcg_true;
  outC->init3 = kcg_true;
  /* Control:active:_L44=(train::traction#6)/ */
  traction_reset_train(&outC->Context_traction_6);
  /* Physics:rolling:_L38=(train::helper::tIntegratorHPI#5)/ */
  tIntegratorHPI_reset_train_helper(&outC->Context_tIntegratorHPI_5);
  outC->t = kcg_lit_int32(0);
  kcg_copy_TrackLocation_TIU(
    &outC->locachion,
    (TrackLocation_TIU *) &unknownLocation_TIU);
  outC->mem_modelPWM = kcg_lit_int16(0);
  kcg_copy_TrainParam_TIU(&outC->p, (TrainParam_TIU *) &emptyParam_TIU);
  outC->Control_state_nxt = SSM_st_off_Control;
  outC->v = kcg_lit_int16(0);
  outC->aTrac = kcg_lit_int16(0);
  outC->Physics_state_nxt = SSM_st_standing_Physics;
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/*
  Expanded instances for: train::onBoard/
  @4: (train::helper::moveLocation#5)
  @2: (times#11)
  @5: (train::helper::correctTrack#5)
  @6: @5/(train::helper::alignDistance#2)
  @7: @4/(train::helper::alignDistance#1)
  @1: (times#4)
  @3: (train::packStatus#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** onBoard_train.c
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

