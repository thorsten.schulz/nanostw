/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */
#ifndef _KCG_CONSTS_H_
#define _KCG_CONSTS_H_

#include "kcg_types.h"

/* TIU::emptyParam/ */
extern const TrainParam_TIU emptyParam_TIU;

/* TIU::unknownLocation/ */
extern const TrackLocation_TIU unknownLocation_TIU;

/* TIU::DEFAULT_OS_MA/ */
#define DEFAULT_OS_MA_TIU (kcg_lit_int32(1000) * kcg_lit_int32(100))

/* TIU::emptyIV/ */
extern const InterventionCommand_TIU emptyIV_TIU;

/* TIU::UnknownSid/ */
#define UnknownSid_TIU (kcg_lit_int32(0))

/* TIU::cmd_release_brake/ */
#define cmd_release_brake_TIU (kcg_lit_uint8(2))

/* TIU::cmd_apply_brake/ */
#define cmd_apply_brake_TIU (kcg_lit_uint8(1))

/* train::kmh2cmsFactorLowerHys/ */
#define kmh2cmsFactorLowerHys_train (kcg_lit_int16(25))

/* train::kmh2cmsFactorUpperHys/ */
#define kmh2cmsFactorUpperHys_train (kcg_lit_int16(28))

/* train::CtrlRange/ */
#define CtrlRange_train (kcg_lit_int16(15))

/* train::preChargeCycles/ */
#define preChargeCycles_train (kcg_lit_int16(20))

/* train::vMin/ */
#define vMin_train (kcg_lit_int16(120))

/* train::deltaTfpDivisor/ */
#define deltaTfpDivisor_train (kcg_lit_int32(1000))

/* train::aDrag/ */
#define aDrag_train (kcg_lit_int16(2))

/* train::cDrag/ */
#define cDrag_train (kcg_lit_int32(130435))

/* train::tripCycles/ */
#define tripCycles_train (kcg_lit_uint16(50))

#endif /* _KCG_CONSTS_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_consts.h
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

