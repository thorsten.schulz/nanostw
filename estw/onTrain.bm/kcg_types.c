/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

#include "kcg_types.h"

#ifdef kcg_use_array_bool_5
kcg_bool kcg_comp_array_bool_5(array_bool_5 *kcg_c1, array_bool_5 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_size kcg_ci;

  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 5; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_bool_5 */

#ifdef kcg_use_TrainData_TIU
kcg_bool kcg_comp_TrainData_TIU(TrainData_TIU *kcg_c1, TrainData_TIU *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->TrainId == kcg_c2->TrainId);
  kcg_equ = kcg_equ & (kcg_c1->Ant2Cab == kcg_c2->Ant2Cab);
  kcg_equ = kcg_equ & (kcg_c1->length == kcg_c2->length);
  kcg_equ = kcg_equ & (kcg_c1->vOnSight == kcg_c2->vOnSight);
  kcg_equ = kcg_equ & (kcg_c1->vRelease == kcg_c2->vRelease);
  kcg_equ = kcg_equ & (kcg_c1->vMax == kcg_c2->vMax);
  kcg_equ = kcg_equ & (kcg_c1->BrPc == kcg_c2->BrPc);
  kcg_equ = kcg_equ & kcg_comp_StatusWord_TIU(&kcg_c1->flag, &kcg_c2->flag);
  kcg_equ = kcg_equ & (kcg_c1->time == kcg_c2->time);
  kcg_equ = kcg_equ & (kcg_c1->acc == kcg_c2->acc);
  kcg_equ = kcg_equ & (kcg_c1->velocity == kcg_c2->velocity);
  kcg_equ = kcg_equ & (kcg_c1->location == kcg_c2->location);
  return kcg_equ;
}
#endif /* kcg_use_TrainData_TIU */

#ifdef kcg_use_TrainReport_TIU
kcg_bool kcg_comp_TrainReport_TIU(
  TrainReport_TIU *kcg_c1,
  TrainReport_TIU *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_TrainData_TIU(
      &kcg_c1->trainData,
      &kcg_c2->trainData);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  kcg_equ = kcg_equ & (kcg_c1->sid == kcg_c2->sid);
  return kcg_equ;
}
#endif /* kcg_use_TrainReport_TIU */

#ifdef kcg_use_TrackLocation_TIU
kcg_bool kcg_comp_TrackLocation_TIU(
  TrackLocation_TIU *kcg_c1,
  TrackLocation_TIU *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->location == kcg_c2->location);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  kcg_equ = kcg_equ & (kcg_c1->sid == kcg_c2->sid);
  return kcg_equ;
}
#endif /* kcg_use_TrackLocation_TIU */

#ifdef kcg_use_StatusWord_TIU
kcg_bool kcg_comp_StatusWord_TIU(StatusWord_TIU *kcg_c1, StatusWord_TIU *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->stopped == kcg_c2->stopped);
  kcg_equ = kcg_equ & (kcg_c1->tripped == kcg_c2->tripped);
  kcg_equ = kcg_equ & (kcg_c1->otherDir == kcg_c2->otherDir);
  kcg_equ = kcg_equ & (kcg_c1->active == kcg_c2->active);
  return kcg_equ;
}
#endif /* kcg_use_StatusWord_TIU */

#ifdef kcg_use_TrainParam_TIU
kcg_bool kcg_comp_TrainParam_TIU(TrainParam_TIU *kcg_c1, TrainParam_TIU *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->Ant2CabB == kcg_c2->Ant2CabB);
  kcg_equ = kcg_equ & (kcg_c1->Ant2CabA == kcg_c2->Ant2CabA);
  kcg_equ = kcg_equ & (kcg_c1->length == kcg_c2->length);
  kcg_equ = kcg_equ & (kcg_c1->vOnSight == kcg_c2->vOnSight);
  kcg_equ = kcg_equ & (kcg_c1->vRelease == kcg_c2->vRelease);
  kcg_equ = kcg_equ & (kcg_c1->vMax == kcg_c2->vMax);
  kcg_equ = kcg_equ & (kcg_c1->BrPc == kcg_c2->BrPc);
  kcg_equ = kcg_equ & (kcg_c1->modelOffset == kcg_c2->modelOffset);
  kcg_equ = kcg_equ & (kcg_c1->modelFactor == kcg_c2->modelFactor);
  return kcg_equ;
}
#endif /* kcg_use_TrainParam_TIU */

#ifdef kcg_use_InterventionCommand_TIU
kcg_bool kcg_comp_InterventionCommand_TIU(
  InterventionCommand_TIU *kcg_c1,
  InterventionCommand_TIU *kcg_c2)
{
  kcg_bool kcg_equ;

  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->tco == kcg_c2->tco);
  kcg_equ = kcg_equ & (kcg_c1->sb == kcg_c2->sb);
  kcg_equ = kcg_equ & (kcg_c1->eb == kcg_c2->eb);
  kcg_equ = kcg_equ & (kcg_c1->interventionSpeed == kcg_c2->interventionSpeed);
  kcg_equ = kcg_equ & (kcg_c1->permittedSpeed == kcg_c2->permittedSpeed);
  kcg_equ = kcg_equ & (kcg_c1->targetDistance == kcg_c2->targetDistance);
  return kcg_equ;
}
#endif /* kcg_use_InterventionCommand_TIU */

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_types.c
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

