/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */
#ifndef _onBoard_train_H_
#define _onBoard_train_H_

#include "kcg_types.h"
#include "tIntegratorHPI_train_helper.h"
#include "traction_train.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  VelocityUnit_TIU /* modelPWM/ */ mem_modelPWM;
  TrainParam_TIU /* p/ */ p;
  TrackLocation_TIU /* locachion/ */ locachion;
  TimeUnit_TIU /* t/ */ t;
  VelocityUnit_TIU /* v/ */ v;
  AccelerationUnit_TIU /* aTrac/ */ aTrac;
  kcg_bool /* Physics: */ Physics_reset_act;
  SSM_ST_Physics /* Physics: */ Physics_state_nxt;
  SSM_ST_Control /* Control: */ Control_state_nxt;
  kcg_bool /* isCabB/ */ isCabB;
  SSM_ST_Direction_active_Control /* Control:active:Direction: */ Direction_state_nxt_active_Control;
  SSM_ST_TripState_active_Control /* Control:active:TripState: */ TripState_state_nxt_active_Control;
  kcg_bool /* Control:active:TripState: */ TripState_reset_act_active_Control;
  SSM_ST_InitialPath_active_Control /* Control:active:InitialPath: */ InitialPath_state_nxt_active_Control;
  SSM_ST_odo_sim_rolling_Physics /* Physics:rolling:odo_sim: */ odo_sim_state_nxt_rolling_Physics;
  VelocityUnit_TIU /* Physics:rolling:vSimOffs/ */ vSimOffs_rolling_Physics;
  VelocityUnit_TIU /* Physics:rolling:_L38/,
     Physics:rolling:_L41/,
     Physics:rolling:vSim/ */ _L38_rolling_Physics;
  kcg_size /* @1/_/v3/ */ v3_times_4_size;
  kcg_size /* @2/_/v3/ */ v3_times_11_size;
  kcg_bool init;
  kcg_bool init1;
  kcg_bool init2;
  kcg_bool init3;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_traction_train /* Control:active:_L44=(train::traction#6)/ */ Context_traction_6;
  outC_tIntegratorHPI_train_helper /* Physics:rolling:_L38=(train::helper::tIntegratorHPI#5)/ */ Context_tIntegratorHPI_5;
  /* ----------------- no clocks of observable data ------------------ */
} outC_onBoard_train;

/* ===========  node initialization and cycle functions  =========== */
/* train::onBoard/ */
extern void onBoard_train(
  /* _L431/, self/ */
  TrainId_TIU self,
  /* turnOff/ */
  kcg_bool turnOff,
  /* trip/ */
  kcg_bool trip,
  /* ctrl/ */
  AccelerationUnit_TIU ctrl,
  /* selectDir/ */
  M_directioncontroller_command_T_TIU selectDir,
  /* _L373/, dT/ */
  TimeUnit_TIU dT,
  /* _L439/, dOdo/ */
  VelocityUnit_TIU dOdo,
  /* _L441/, dirOdo/ */
  VelocityUnit_TIU dirOdo,
  /* iv/ */
  InterventionCommand_TIU *iv,
  /* correctionPoint/ */
  TrackLocation_TIU *correctionPoint,
  /* param/ */
  TrainParam_TIU *param,
  /* modelPWM/ */
  VelocityUnit_TIU *modelPWM,
  /* _L400/, report/ */
  TrainReport_TIU *report,
  /* send_report/ */
  kcg_bool *send_report,
  /* @3/_L15/, @3/bitmap/, _L415/, packedFlags/ */
  kcg_uint8 *packedFlags,
  /* _L438/, isParamUpdate/, paramChanged/ */
  kcg_bool *paramChanged,
  /* ivState/ */
  array_bool_5 *ivState,
  outC_onBoard_train *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void onBoard_reset_train(outC_onBoard_train *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void onBoard_init_train(outC_onBoard_train *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: train::onBoard/
  @1: (times#4)
  @2: (times#11)
  @3: (train::packStatus#1)
*/

#endif /* _onBoard_train_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** onBoard_train.h
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

