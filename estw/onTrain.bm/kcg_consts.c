/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

#include "kcg_consts.h"

/* TIU::emptyParam/ */
const TrainParam_TIU emptyParam_TIU = { kcg_lit_int16(1), kcg_lit_int16(0),
  kcg_lit_uint16(0), kcg_lit_int16(0), kcg_lit_int16(0), kcg_lit_int16(0),
  kcg_lit_int32(0), kcg_lit_int32(0), kcg_lit_int32(0) };

/* TIU::unknownLocation/ */
const TrackLocation_TIU unknownLocation_TIU = { kcg_lit_int32(0), kcg_lit_int32(
    0), kcg_lit_int32(0) };

/* TIU::emptyIV/ */
const InterventionCommand_TIU emptyIV_TIU = { kcg_lit_int32(0), kcg_lit_int16(
    0), kcg_lit_int16(0), kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(
    0) };

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_consts.c
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

