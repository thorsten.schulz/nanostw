/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "traction_train.h"

/* train::traction/ */
void traction_train(
  /* _L34/, ctrl/ */
  AccelerationUnit_TIU ctrl,
  /* iv/ */
  InterventionCommand_TIU *iv,
  /* _L40/, techStop/ */
  kcg_bool techStop,
  /* _L43/, speed/ */
  VelocityUnit_TIU speed,
  /* TractionPercentage/, _L42/ */
  AccelerationUnit_TIU TractionPercentage,
  /* _L54/, vMax/ */
  VelocityUnit_TIU vMax,
  /* _L19/, a/ */
  AccelerationUnit_TIU *a,
  /* _L58/, ivState/ */
  array_bool_5 *ivState,
  outC_traction_train *outC)
{
  kcg_int16 tmp;
  /* _L39/ */
  kcg_int16 _L39;
  /* _L12/ */
  kcg_int16 _L12;

  (*ivState)[4] = kcg_true;
  outC->_L14 = (!outC->init) & ((cmd_apply_brake_TIU == (*iv).tco) |
      ((!((*iv).tco == cmd_release_brake_TIU)) & outC->_L14));
  (*ivState)[2] = outC->_L14;
  outC->_L20 = (!outC->init) & ((cmd_apply_brake_TIU == (*iv).sb) |
      ((!((*iv).sb == cmd_release_brake_TIU)) & outC->_L20));
  (*ivState)[1] = outC->_L20;
  outC->_L32 = (!outC->init) & ((cmd_apply_brake_TIU == (*iv).eb) |
      ((!((*iv).eb == cmd_release_brake_TIU)) & outC->_L32));
  (*ivState)[0] = outC->_L32;
  outC->_L48 = (!outC->init) & ((speed > kmh2cmsFactorUpperHys_train * vMax) |
      ((!(speed < vMax * kmh2cmsFactorLowerHys_train)) & outC->_L48));
  outC->init = kcg_false;
  (*ivState)[3] = outC->_L48;
  /* _L39= */
  if (techStop) {
    _L39 = kcg_lit_int16(-30);
  }
  else /* _L31= */
  if (outC->_L32) {
    _L39 = kcg_lit_int16(-15);
  }
  else /* _L7= */
  if (outC->_L20) {
    _L39 = kcg_lit_int16(-30) / kcg_lit_int16(3);
  }
  else /* _L3= */
  if (outC->_L14 | outC->_L48) {
    _L39 = kcg_lit_int16(0);
  }
  else {
    _L39 = CtrlRange_train;
  }
  /* _L12= */
  if (ctrl < kcg_lit_int16(-15)) {
    _L12 = kcg_lit_int16(-15);
  }
  else {
    _L12 = ctrl;
  }
  /* _L29= */
  if (_L12 > _L39) {
    tmp = _L39;
  }
  else {
    tmp = _L12;
  }
  *a = TractionPercentage * kcg_lit_int16(3) * tmp / kcg_lit_int16(60);
}

#ifndef KCG_USER_DEFINED_INIT
void traction_init_train(outC_traction_train *outC)
{
  outC->init = kcg_true;
  outC->_L32 = kcg_true;
  outC->_L20 = kcg_true;
  outC->_L14 = kcg_true;
  outC->_L48 = kcg_true;
}
#endif /* KCG_USER_DEFINED_INIT */


void traction_reset_train(outC_traction_train *outC)
{
  outC->init = kcg_true;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** traction_train.c
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

