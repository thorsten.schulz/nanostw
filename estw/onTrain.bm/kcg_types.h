/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */
#ifndef _KCG_TYPES_H_
#define _KCG_TYPES_H_

#include "stddef.h"

#define KCG_MAPW_CPY

#include "../user_macros.h"

#ifndef kcg_char
#define kcg_char kcg_char
typedef char kcg_char;
#endif /* kcg_char */

#ifndef kcg_bool
#define kcg_bool kcg_bool
typedef unsigned char kcg_bool;
#endif /* kcg_bool */

#ifndef kcg_float32
#define kcg_float32 kcg_float32
typedef float kcg_float32;
#endif /* kcg_float32 */

#ifndef kcg_float64
#define kcg_float64 kcg_float64
typedef double kcg_float64;
#endif /* kcg_float64 */

#ifndef kcg_size
#define kcg_size kcg_size
typedef ptrdiff_t kcg_size;
#endif /* kcg_size */

#ifndef kcg_uint64
#define kcg_uint64 kcg_uint64
typedef unsigned long long kcg_uint64;
#endif /* kcg_uint64 */

#ifndef kcg_uint32
#define kcg_uint32 kcg_uint32
typedef unsigned long kcg_uint32;
#endif /* kcg_uint32 */

#ifndef kcg_uint16
#define kcg_uint16 kcg_uint16
typedef unsigned short kcg_uint16;
#endif /* kcg_uint16 */

#ifndef kcg_uint8
#define kcg_uint8 kcg_uint8
typedef unsigned char kcg_uint8;
#endif /* kcg_uint8 */

#ifndef kcg_int64
#define kcg_int64 kcg_int64
typedef signed long long kcg_int64;
#endif /* kcg_int64 */

#ifndef kcg_int32
#define kcg_int32 kcg_int32
typedef signed long kcg_int32;
#endif /* kcg_int32 */

#ifndef kcg_int16
#define kcg_int16 kcg_int16
typedef signed short kcg_int16;
#endif /* kcg_int16 */

#ifndef kcg_int8
#define kcg_int8 kcg_int8
typedef signed char kcg_int8;
#endif /* kcg_int8 */

#ifndef kcg_lit_float32
#define kcg_lit_float32(kcg_C1) ((kcg_float32) (kcg_C1))
#endif /* kcg_lit_float32 */

#ifndef kcg_lit_float64
#define kcg_lit_float64(kcg_C1) ((kcg_float64) (kcg_C1))
#endif /* kcg_lit_float64 */

#ifndef kcg_lit_size
#define kcg_lit_size(kcg_C1) ((kcg_size) (kcg_C1))
#endif /* kcg_lit_size */

#ifndef kcg_lit_uint64
#define kcg_lit_uint64(kcg_C1) ((kcg_uint64) (kcg_C1))
#endif /* kcg_lit_uint64 */

#ifndef kcg_lit_uint32
#define kcg_lit_uint32(kcg_C1) ((kcg_uint32) (kcg_C1))
#endif /* kcg_lit_uint32 */

#ifndef kcg_lit_uint16
#define kcg_lit_uint16(kcg_C1) ((kcg_uint16) (kcg_C1))
#endif /* kcg_lit_uint16 */

#ifndef kcg_lit_uint8
#define kcg_lit_uint8(kcg_C1) ((kcg_uint8) (kcg_C1))
#endif /* kcg_lit_uint8 */

#ifndef kcg_lit_int64
#define kcg_lit_int64(kcg_C1) ((kcg_int64) (kcg_C1))
#endif /* kcg_lit_int64 */

#ifndef kcg_lit_int32
#define kcg_lit_int32(kcg_C1) ((kcg_int32) (kcg_C1))
#endif /* kcg_lit_int32 */

#ifndef kcg_lit_int16
#define kcg_lit_int16(kcg_C1) ((kcg_int16) (kcg_C1))
#endif /* kcg_lit_int16 */

#ifndef kcg_lit_int8
#define kcg_lit_int8(kcg_C1) ((kcg_int8) (kcg_C1))
#endif /* kcg_lit_int8 */

#ifndef kcg_false
#define kcg_false ((kcg_bool) 0)
#endif /* kcg_false */

#ifndef kcg_true
#define kcg_true ((kcg_bool) 1)
#endif /* kcg_true */

#ifndef kcg_lsl_uint64
#define kcg_lsl_uint64(kcg_C1, kcg_C2)                                        \
  ((kcg_uint64) ((kcg_C1) << (kcg_C2)) & 0xffffffffffffffff)
#endif /* kcg_lsl_uint64 */

#ifndef kcg_lsl_uint32
#define kcg_lsl_uint32(kcg_C1, kcg_C2)                                        \
  ((kcg_uint32) ((kcg_C1) << (kcg_C2)) & 0xffffffff)
#endif /* kcg_lsl_uint32 */

#ifndef kcg_lsl_uint16
#define kcg_lsl_uint16(kcg_C1, kcg_C2)                                        \
  ((kcg_uint16) ((kcg_C1) << (kcg_C2)) & 0xffff)
#endif /* kcg_lsl_uint16 */

#ifndef kcg_lsl_uint8
#define kcg_lsl_uint8(kcg_C1, kcg_C2)                                         \
  ((kcg_uint8) ((kcg_C1) << (kcg_C2)) & 0xff)
#endif /* kcg_lsl_uint8 */

#ifndef kcg_lnot_uint64
#define kcg_lnot_uint64(kcg_C1) ((kcg_C1) ^ 0xffffffffffffffff)
#endif /* kcg_lnot_uint64 */

#ifndef kcg_lnot_uint32
#define kcg_lnot_uint32(kcg_C1) ((kcg_C1) ^ 0xffffffff)
#endif /* kcg_lnot_uint32 */

#ifndef kcg_lnot_uint16
#define kcg_lnot_uint16(kcg_C1) ((kcg_C1) ^ 0xffff)
#endif /* kcg_lnot_uint16 */

#ifndef kcg_lnot_uint8
#define kcg_lnot_uint8(kcg_C1) ((kcg_C1) ^ 0xff)
#endif /* kcg_lnot_uint8 */

#ifndef kcg_assign
#include "kcg_assign.h"
#endif /* kcg_assign */

#ifndef kcg_assign_struct
#define kcg_assign_struct kcg_assign
#endif /* kcg_assign_struct */

#ifndef kcg_assign_array
#define kcg_assign_array kcg_assign
#endif /* kcg_assign_array */

/* TIU::M_directioncontroller_command_T/ */
typedef enum kcg_tag_M_directioncontroller_command_T_TIU {
  directioncontroller_command_not_defined_TIU,
  directioncontroller_command_cabA_TIU,
  directioncontroller_command_cabB_TIU
} M_directioncontroller_command_T_TIU;
/* train::onBoard/Physics: */
typedef enum kcg_tag_SSM_TR_Physics {
  SSM_TR_no_trans_Physics,
  SSM_TR_rolling_standing_1_rolling_Physics,
  SSM_TR_standing_preCharging_1_standing_Physics,
  SSM_TR_preCharging_standing_1_preCharging_Physics,
  SSM_TR_preCharging_rolling_2_preCharging_Physics
} SSM_TR_Physics;
/* train::onBoard/Physics: */
typedef enum kcg_tag_SSM_ST_Physics {
  SSM_st_rolling_Physics,
  SSM_st_standing_Physics,
  SSM_st_preCharging_Physics
} SSM_ST_Physics;
/* train::onBoard/Physics:rolling:odo_sim: */
typedef enum kcg_tag_SSM_TR_odo_sim_rolling_Physics {
  SSM_TR_no_trans_odo_sim_rolling_Physics,
  SSM_TR_sim_odo_1_sim_odo_sim_rolling_Physics,
  SSM_TR_odo_sim_1_odo_odo_sim_rolling_Physics
} SSM_TR_odo_sim_rolling_Physics;
/* train::onBoard/Physics:rolling:odo_sim: */
typedef enum kcg_tag_SSM_ST_odo_sim_rolling_Physics {
  SSM_st_sim_odo_sim_rolling_Physics,
  SSM_st_odo_odo_sim_rolling_Physics
} SSM_ST_odo_sim_rolling_Physics;
/* train::onBoard/Control: */
typedef enum kcg_tag_SSM_TR_Control {
  SSM_TR_no_trans_Control,
  SSM_TR_off_invalTrainData_1_1_off_Control,
  SSM_TR_off_active_2_1_off_Control,
  SSM_TR_active_unknown_location_reset_on_powercycle_1_1_active_Control,
  SSM_TR_active_off_2_1_active_Control,
  SSM_TR_invalTrainData_off_1_invalTrainData_Control,
  SSM_TR_unknown_location_reset_on_powercycle_off_1_unknown_location_reset_on_powercycle_Control
} SSM_TR_Control;
/* train::onBoard/Control: */
typedef enum kcg_tag_SSM_ST_Control {
  SSM_st_off_Control,
  SSM_st_active_Control,
  SSM_st_invalTrainData_Control,
  SSM_st_unknown_location_reset_on_powercycle_Control
} SSM_ST_Control;
/* train::onBoard/Control:active:InitialPath: */
typedef enum kcg_tag_SSM_TR_InitialPath_active_Control {
  SSM_TR_no_trans_InitialPath_active_Control,
  SSM_TR_initialOnSight_Level_2_1_initialOnSight_InitialPath_active_Control,
  SSM_TR_initialOnSight_initialOnSight_2_initialOnSight_InitialPath_active_Control,
  SSM_TR_Level_2_initialOnSight_1_Level_2_InitialPath_active_Control
} SSM_TR_InitialPath_active_Control;
/* train::onBoard/Control:active:InitialPath: */
typedef enum kcg_tag_SSM_ST_InitialPath_active_Control {
  SSM_st_initialOnSight_InitialPath_active_Control,
  SSM_st_Level_2_InitialPath_active_Control
} SSM_ST_InitialPath_active_Control;
/* train::onBoard/Control:active:TripState: */
typedef enum kcg_tag_SSM_TR_TripState_active_Control {
  SSM_TR_no_trans_TripState_active_Control,
  SSM_TR_normal_funcTrip_1_normal_TripState_active_Control,
  SSM_TR_funcTrip_funcTrip_1_funcTrip_TripState_active_Control,
  SSM_TR_funcTrip_normal_2_funcTrip_TripState_active_Control
} SSM_TR_TripState_active_Control;
/* train::onBoard/Control:active:TripState: */
typedef enum kcg_tag_SSM_ST_TripState_active_Control {
  SSM_st_normal_TripState_active_Control,
  SSM_st_funcTrip_TripState_active_Control
} SSM_ST_TripState_active_Control;
/* train::onBoard/Control:active:Direction: */
typedef enum kcg_tag_SSM_TR_Direction_active_Control {
  SSM_TR_no_trans_Direction_active_Control,
  SSM_TR_CabA_CabB_1_CabA_Direction_active_Control,
  SSM_TR_CabB_CabA_1_CabB_Direction_active_Control
} SSM_TR_Direction_active_Control;
/* train::onBoard/Control:active:Direction: */
typedef enum kcg_tag_SSM_ST_Direction_active_Control {
  SSM_st_CabA_Direction_active_Control,
  SSM_st_CabB_Direction_active_Control
} SSM_ST_Direction_active_Control;
/* TIU::StatusWord/ */
typedef struct kcg_tag_StatusWord_TIU {
  kcg_bool active;
  kcg_bool otherDir;
  kcg_bool tripped;
  kcg_bool stopped;
} StatusWord_TIU;

typedef kcg_bool array_bool_5[5];

/* TIU::TrainId/ */
typedef kcg_int32 TrainId_TIU;

/* TIU::TrackId/ */
typedef kcg_int32 TrackId_TIU;

/* TIU::LengthUnit/ */
typedef kcg_int32 LengthUnit_TIU;

/* TIU::TrackLocation/ */
typedef struct kcg_tag_TrackLocation_TIU {
  TrackId_TIU sid;
  LengthUnit_TIU distance;
  LengthUnit_TIU location;
} TrackLocation_TIU;

/* TIU::TimeUnit/ */
typedef kcg_int32 TimeUnit_TIU;

/* TIU::AccelerationUnit/ */
typedef kcg_int16 AccelerationUnit_TIU;

/* TIU::VelocityUnit/ */
typedef kcg_int16 VelocityUnit_TIU;

/* TIU::InterventionCommand/ */
typedef struct kcg_tag_InterventionCommand_TIU {
  LengthUnit_TIU targetDistance;
  VelocityUnit_TIU permittedSpeed;
  VelocityUnit_TIU interventionSpeed;
  kcg_uint8 eb;
  kcg_uint8 sb;
  kcg_uint8 tco;
} InterventionCommand_TIU;

/* TIU::TrainParam/ */
typedef struct kcg_tag_TrainParam_TIU {
  VelocityUnit_TIU modelFactor;
  VelocityUnit_TIU modelOffset;
  kcg_uint16 BrPc;
  VelocityUnit_TIU vMax;
  VelocityUnit_TIU vRelease;
  VelocityUnit_TIU vOnSight;
  LengthUnit_TIU length;
  LengthUnit_TIU Ant2CabA;
  LengthUnit_TIU Ant2CabB;
} TrainParam_TIU;

/* TIU::TrainData/ */
typedef struct kcg_tag_TrainData_TIU {
  LengthUnit_TIU location;
  VelocityUnit_TIU velocity;
  AccelerationUnit_TIU acc;
  TimeUnit_TIU time;
  StatusWord_TIU flag;
  kcg_uint16 BrPc;
  VelocityUnit_TIU vMax;
  VelocityUnit_TIU vRelease;
  VelocityUnit_TIU vOnSight;
  LengthUnit_TIU length;
  LengthUnit_TIU Ant2Cab;
  TrainId_TIU TrainId;
} TrainData_TIU;

/* TIU::TrainReport/ */
typedef struct kcg_tag_TrainReport_TIU {
  TrackId_TIU sid;
  LengthUnit_TIU distance;
  TrainData_TIU trainData;
} TrainReport_TIU;

#ifndef kcg_copy_TrainData_TIU
#define kcg_copy_TrainData_TIU(kcg_C1, kcg_C2)                                \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (TrainData_TIU)))
#endif /* kcg_copy_TrainData_TIU */

#ifndef kcg_copy_TrainReport_TIU
#define kcg_copy_TrainReport_TIU(kcg_C1, kcg_C2)                              \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (TrainReport_TIU)))
#endif /* kcg_copy_TrainReport_TIU */

#ifndef kcg_copy_TrackLocation_TIU
#define kcg_copy_TrackLocation_TIU(kcg_C1, kcg_C2)                            \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (TrackLocation_TIU)))
#endif /* kcg_copy_TrackLocation_TIU */

#ifndef kcg_copy_StatusWord_TIU
#define kcg_copy_StatusWord_TIU(kcg_C1, kcg_C2)                               \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (StatusWord_TIU)))
#endif /* kcg_copy_StatusWord_TIU */

#ifndef kcg_copy_TrainParam_TIU
#define kcg_copy_TrainParam_TIU(kcg_C1, kcg_C2)                               \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (TrainParam_TIU)))
#endif /* kcg_copy_TrainParam_TIU */

#ifndef kcg_copy_InterventionCommand_TIU
#define kcg_copy_InterventionCommand_TIU(kcg_C1, kcg_C2)                      \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (InterventionCommand_TIU)))
#endif /* kcg_copy_InterventionCommand_TIU */

#ifndef kcg_copy_array_bool_5
#define kcg_copy_array_bool_5(kcg_C1, kcg_C2)                                 \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_bool_5)))
#endif /* kcg_copy_array_bool_5 */

#ifdef kcg_use_TrainData_TIU
#ifndef kcg_comp_TrainData_TIU
extern kcg_bool kcg_comp_TrainData_TIU(
  TrainData_TIU *kcg_c1,
  TrainData_TIU *kcg_c2);
#endif /* kcg_comp_TrainData_TIU */
#endif /* kcg_use_TrainData_TIU */

#ifdef kcg_use_TrainReport_TIU
#ifndef kcg_comp_TrainReport_TIU
extern kcg_bool kcg_comp_TrainReport_TIU(
  TrainReport_TIU *kcg_c1,
  TrainReport_TIU *kcg_c2);
#endif /* kcg_comp_TrainReport_TIU */
#endif /* kcg_use_TrainReport_TIU */

#ifdef kcg_use_TrackLocation_TIU
#ifndef kcg_comp_TrackLocation_TIU
extern kcg_bool kcg_comp_TrackLocation_TIU(
  TrackLocation_TIU *kcg_c1,
  TrackLocation_TIU *kcg_c2);
#endif /* kcg_comp_TrackLocation_TIU */
#endif /* kcg_use_TrackLocation_TIU */

#ifdef kcg_use_StatusWord_TIU
#ifndef kcg_comp_StatusWord_TIU
extern kcg_bool kcg_comp_StatusWord_TIU(
  StatusWord_TIU *kcg_c1,
  StatusWord_TIU *kcg_c2);
#endif /* kcg_comp_StatusWord_TIU */
#endif /* kcg_use_StatusWord_TIU */

#ifdef kcg_use_TrainParam_TIU
#ifndef kcg_comp_TrainParam_TIU
extern kcg_bool kcg_comp_TrainParam_TIU(
  TrainParam_TIU *kcg_c1,
  TrainParam_TIU *kcg_c2);
#endif /* kcg_comp_TrainParam_TIU */
#endif /* kcg_use_TrainParam_TIU */

#ifdef kcg_use_InterventionCommand_TIU
#ifndef kcg_comp_InterventionCommand_TIU
extern kcg_bool kcg_comp_InterventionCommand_TIU(
  InterventionCommand_TIU *kcg_c1,
  InterventionCommand_TIU *kcg_c2);
#endif /* kcg_comp_InterventionCommand_TIU */
#endif /* kcg_use_InterventionCommand_TIU */

#ifdef kcg_use_array_bool_5
#ifndef kcg_comp_array_bool_5
extern kcg_bool kcg_comp_array_bool_5(
  array_bool_5 *kcg_c1,
  array_bool_5 *kcg_c2);
#endif /* kcg_comp_array_bool_5 */
#endif /* kcg_use_array_bool_5 */

#endif /* _KCG_TYPES_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_types.h
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

