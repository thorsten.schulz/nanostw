/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */
#ifndef _tIntegratorHPI_train_helper_H_
#define _tIntegratorHPI_train_helper_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_bool init;
  kcg_int32 /* _L18/, _L32/, acc/ */ _L18;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_tIntegratorHPI_train_helper;

/* ===========  node initialization and cycle functions  =========== */
/* train::helper::tIntegratorHPI/ */
extern void tIntegratorHPI_train_helper(
  /* _L1/, in/ */
  AccelerationUnit_TIU in,
  /* @1/I1/, @1/_L21/, _L24/, dT/ */
  TimeUnit_TIU dT,
  /* _L17/, positive/ */
  kcg_bool positive,
  /* v0/ */
  VelocityUnit_TIU v0,
  /* _L25/, out/ */
  VelocityUnit_TIU *out,
  outC_tIntegratorHPI_train_helper *outC);

extern void tIntegratorHPI_reset_train_helper(
  outC_tIntegratorHPI_train_helper *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void tIntegratorHPI_init_train_helper(
  outC_tIntegratorHPI_train_helper *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: train::helper::tIntegratorHPI/
  @1: (math::Min#1)
*/

#endif /* _tIntegratorHPI_train_helper_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** tIntegratorHPI_train_helper.h
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

