/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */
#ifndef _traction_train_H_
#define _traction_train_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_bool init;
  kcg_bool /* @1/FFS_Output/, @1/_L2/, _L32/ */ _L32;
  kcg_bool /* @2/FFS_Output/, @2/_L2/, _L20/ */ _L20;
  kcg_bool /* @3/FFS_Output/, @3/_L2/, _L14/ */ _L14;
  kcg_bool /* @4/FFS_Output/, @4/_L2/, _L48/, _L55/, _L59/, overSpeed/ */ _L48;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_traction_train;

/* ===========  node initialization and cycle functions  =========== */
/* train::traction/ */
extern void traction_train(
  /* _L34/, ctrl/ */
  AccelerationUnit_TIU ctrl,
  /* iv/ */
  InterventionCommand_TIU *iv,
  /* _L40/, techStop/ */
  kcg_bool techStop,
  /* _L43/, speed/ */
  VelocityUnit_TIU speed,
  /* TractionPercentage/, _L42/ */
  AccelerationUnit_TIU TractionPercentage,
  /* _L54/, vMax/ */
  VelocityUnit_TIU vMax,
  /* _L19/, a/ */
  AccelerationUnit_TIU *a,
  /* _L58/, ivState/ */
  array_bool_5 *ivState,
  outC_traction_train *outC);

extern void traction_reset_train(outC_traction_train *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void traction_init_train(outC_traction_train *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: train::traction/
  @1: (digital::FlipFlopSet#1)
  @2: (digital::FlipFlopSet#2)
  @3: (digital::FlipFlopSet#3)
  @4: (digital::FlipFlopSet#4)
*/

#endif /* _traction_train_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** traction_train.h
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

