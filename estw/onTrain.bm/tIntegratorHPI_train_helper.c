/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/EW/ESTW/onTrain.bm/config.txt
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "tIntegratorHPI_train_helper.h"

/* train::helper::tIntegratorHPI/ */
void tIntegratorHPI_train_helper(
  /* _L1/, in/ */
  AccelerationUnit_TIU in,
  /* @1/I1/, @1/_L21/, _L24/, dT/ */
  TimeUnit_TIU dT,
  /* _L17/, positive/ */
  kcg_bool positive,
  /* v0/ */
  VelocityUnit_TIU v0,
  /* _L25/, out/ */
  VelocityUnit_TIU *out,
  outC_tIntegratorHPI_train_helper *outC)
{
  TimeUnit_TIU tmp;
  kcg_int32 tmp1;
  /* _L16/ */
  kcg_int32 _L16;

  /* @1/_L24= */
  if (dT <= kcg_lit_int32(100000)) {
    tmp = dT;
  }
  else {
    tmp = kcg_lit_int32(100000);
  }
  /* acc/ */
  if (outC->init) {
    outC->init = kcg_false;
    tmp1 = deltaTfpDivisor_train * /*  */(kcg_int32) v0;
  }
  else {
    tmp1 = outC->_L18;
  }
  _L16 = /* _L27= */(kcg_int32) in * tmp + tmp1;
  if (positive) {
    /* @2/_L4= */
    if (kcg_lit_int32(0) >= _L16) {
      outC->_L18 = kcg_lit_int32(0);
    }
    else {
      outC->_L18 = _L16;
    }
  }
  else {
    outC->_L18 = _L16;
  }
  *out = /* _L25= */(kcg_int16)
      ((outC->_L18 + kcg_lit_int32(500)) / deltaTfpDivisor_train);
}

#ifndef KCG_USER_DEFINED_INIT
void tIntegratorHPI_init_train_helper(outC_tIntegratorHPI_train_helper *outC)
{
  outC->init = kcg_true;
  outC->_L18 = kcg_lit_int32(0);
}
#endif /* KCG_USER_DEFINED_INIT */


void tIntegratorHPI_reset_train_helper(outC_tIntegratorHPI_train_helper *outC)
{
  outC->init = kcg_true;
}

/*
  Expanded instances for: train::helper::tIntegratorHPI/
  @1: (math::Min#1)
  @2: (math::Max#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** tIntegratorHPI_train_helper.c
** Generation date: 2020-12-07T19:39:23
*************************************************************$ */

