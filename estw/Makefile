#
# nanoStw - an integration of the openETCS on-board function Speed and Distance
#           Monitoring implemented as a Scade model.
#
# This provides a Make'able base for the Stw integrations and the small Ctrl
# helper. If you want to extend, some rules provide some magic, others have to
# be manually duplicated accordingly. Good luck!
#
# terms:
#   nano     -- refering to the small model train simulator using the model
#               scale standard 'N', that is 1:160
#   Stw (DE) -- interlocking, (e-Stw ~ electronic interlocking)
#
# Copyright (c) Universität Rostock, 2015-2020
#
# Authors:
#  Thorsten Schulz <thorsten.schulz@uni-rostock.de>
#
# SPDX-License-Identifier: Apache-2.0
#

# Set to NO or unset to switch off debug information
DEBUG ?= TRUE

.DELETE_ON_ERROR: 
.SHELLFLAGS := -eu -c
CP = cp

# Where do you like build-artifacts to be dumped?
BUILDTMP = build

DATE_FMT = +%Y-%m-%d_%H:%M
ifdef SOURCE_DATE_EPOCH
    BUILD_DATE ?= $(shell date -u -d "@$(SOURCE_DATE_EPOCH)" "$(DATE_FMT)" 2>/dev/null || date -u -r "$(SOURCE_DATE_EPOCH)" "$(DATE_FMT)" 2>/dev/null || date -u "$(DATE_FMT)")
else
    BUILD_DATE ?= $(shell date "$(DATE_FMT)")
endif

# This arch splitter only influences the Linux build
ifeq ($(ARCH),armhf)
	CC := arm-linux-gnueabihf-gcc
	AR := arm-linux-gnueabihf-ar
	TRDP_ARCH =	armhf
else
ifeq ($(ARCH),i386)
	CFLAGS += -m32
	LDFLAGS += -m32
	TRDP_ARCH = x86
else
	TRDP_ARCH = x86_64
endif
endif

CPPFLAGS += -DPOSIX -DBUILD_DATE=\"$(BUILD_DATE)\"
CPPFLAGS += -Os -g
LDFLAGS  += -g

ifeq ($(DEBUG),TRUE)
	CFLAGS += -DDEBUG
endif

#use the included copy of the trdp-library or leave empty to refer to system libraries (can only build for Linux then)
LOCAL_TRDP := ../TCNopen/trdp
#define, if having and using PikeOS otherwise leave empty
PIKEOS_PREFIX := /opt/pikeos-4.2
#define to compile ELinOS TrdpSvc instead of PIKEOS-POSIX variant. However, currently unfinished.
#ELINOS_PREFIX := /opt/elinos-6.1

#pull jailhouse-images repo to build these
JI = ../jailhouse-images
#the submodule repo is only required to test-build bare-metal cells
#if using branch 'next' here, jailhouse-images must be built with './build-images.sh --latest'
JH = ../jailhouse
# you need to build against the installed kernel on the target system
# to firstly just check general compilation, leave it empty, to compile against the dev-host kernel (which may break, if it is too new)
# then build an image through jailhouse-images './build-images.sh --latest --rt' (yes, some examples make use of the rt-patch)
# somewhere in the build-artifacts you will find linux-{image,headers}-jailhouse-rt_5.4.28-rt19+r0_amd64.deb or a newer version
# install that on your dev-host, yet, be careful not to make it the default kernel for your dev-host
# then you can point jailhouse module compilation to your locally installed headers according to the installed version
JH_KDIRVAR := KDIR=/lib/modules/5.4.28-rt19/build

#Linux build
LX = $(BUILDTMP)/lx
#TRDP build
TRDP_BUILD_BASE = $(BUILDTMP)/trdp
#Chrony build
CHRONY = ../chrony

C_SRC_MODEL_STW = Stw.bm
C_SRC_MODEL_CTL = onTrain.bm
C_SRC_MODEL_TSM = tindyguard/slide-marks.bm

C_SRC_M_STW := $(addprefix $(C_SRC_MODEL_STW)/,$(shell grep -h '\.c' $(C_SRC_MODEL_STW)/kcg_files.txt))    #$(foreach dir, $(C_SRC_MODEL_STW), $(wildcard $(dir)/*.c))
C_SRC_M_CTL := $(addprefix $(C_SRC_MODEL_CTL)/,$(shell grep -h '\.c' $(C_SRC_MODEL_CTL)/kcg_files.txt))    #$(foreach dir, $(C_SRC_MODEL_CTL), $(wildcard $(dir)/*.c))
C_SRC_M_TSM := $(addprefix $(C_SRC_MODEL_TSM)/,$(shell grep -h '\.c' $(C_SRC_MODEL_TSM)/kcg_files.txt))

C_SRC_STW   = $(C_SRC_M_STW) app/onBoardStw.c  

C_SRC_CTL   = $(C_SRC_M_CTL) app/onBoardCtrl.c

C_SRC_SSTW  = $(C_SRC_M_STW) app/stw.c

C_SRC_SCTL  = $(C_SRC_M_CTL) app/ctrl.c

C_SRC_KEY   = app/keyCtrl.c app/keypoll.c

C_HDR_SVC   = app/trdpSvc.h app/ivshmem.h

C_SRC_SVC   = app/trdpSvc.c app/ivshmem-posix.c app/ivshmem-common.c

C_SRC_USVC  = app/trdpSvc.c app/ivshmem-uio.c app/ivshmem-common.c

C_SRC_TSVC  = app/timeSvc.c app/ivshmem-uio.c app/ivshmem-common.c

C_SRC_APPUTL= app/keypoll.c app/tau_xservice_client.c app/ivshmem-posix.c app/ivshmem-common.c

C_SRC_TSM   = $(C_SRC_M_TSM) app/slide-marks.c app/curve25519-donna.c utils.bm/sys.c

#the patch is expected to be the first item!
C_SRC_CNY_U = patches/chrony+uio.patch app/ivshmem-uio.c app/ivshmem-common.c app/ivshmem.h

DEPS_ALL = \
	$(C_SRC_STW:%.c=$(LX)/%.d) \
	$(C_SRC_CTL:%.c=$(LX)/%.d) \
	$(C_SRC_SVC:%.c=$(LX)/%.d) \
	$(C_SRC_SCTL:%.c=$(LX)/%.d) \
	$(C_SRC_SSTW:%.c=$(LX)/%.d) \
	$(C_SRC_USVC:%.c=$(LX)/%.d) \
	$(C_SRC_TSVC:%.c=$(LX)/%.d) \
	$(C_SRC_APPUTL:%.c=$(LX)/%.d) \
	$(C_SRC_TSM:%.c=$(LX)/%.d) \
	$(C_SRC_KEY:%.c=$(LX)/%.d) \


LDFLAGS  += -pthread $(addprefix -L,$(TRDP_LDPATHS))
CPPFLAGS += -pthread $(addprefix -I,. $(TRDP_HDR_COMMON) $(TRDP_HDR_API))

LDLIBS += -lm -lrt

CPPFLAGS += -Wall -Werror
CPPFLAGS_FOR_KCG += -Wno-parentheses -Wno-unused-but-set-variable -Wno-maybe-uninitialized -Wno-switch
CPPFLAGS_FOR_KCG += -Wno-strict-overflow  # skipping this is actually bad, should be checked in the Scade model
CPPFLAGS_FOR_KCG += -include "app/user_macros.h"
CFLAGS += -c -fstrength-reduce -fmessage-length=0 -funsigned-bitfields


#meta-targets need definition first

.PHONY: all clean distclean run run-stw run-train slide-marks

all: $(addprefix $(LX)/,onBoardStw trainCtrl trdpSvc trdpUioSvc timeSvc stw ctrl keyCtrl)


#needed to build inline TRDP-library, instead of a system-installed one
ifneq ($(LOCAL_TRDP),)

include trdp.mk

else

TRDP_HDR_API    = /usr/include/trdp
LDLIBS_TRDP     = -ltau -ltrdp -luuid

endif

#=== === === === === === === === === === === === === === === === === === === ===
#=== Jailhouse rules moved to an mk-include  === === === === === === === === ===
#=== === === === === === === === === === === === === === === === === === === ===

ifneq ($(or $(JH),$(JI)),)

-include jh.mk

endif

#=== === === === === === === === === === === === === === === === === === === ===
#=== crazy PikeOS stuff moved to an include  === === === === === === === === ===
#=== === === === === === === === === === === === === === === === === === === ===
ifneq ($(realpath $(PIKEOS_PREFIX)),)

-include pikeos.mk

else
$(and $(PIKEOS_PREFIX),$(warning PIKEOS_PREFIX path set to $(PIKEOS_PREFIX), but it is inaccessible.))
endif

$(LX):
	@mkdir -p "$@"

$(LX)/stw:        $(C_SRC_SSTW:%.c=$(LX)/%.o) $(C_SRC_APPUTL:%.c=$(LX)/%.o)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(LX)/ctrl:       $(C_SRC_SCTL:%.c=$(LX)/%.o) $(C_SRC_APPUTL:%.c=$(LX)/%.o)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(LX)/trdpSvc:    $(C_SRC_SVC:%.c=$(LX)/%.o)  $(LDEPS)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) $(LDLIBS_TRDP)

$(LX)/trdpUioSvc: $(C_SRC_USVC:%.c=$(LX)/%.o) $(LDEPS)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) $(LDLIBS_TRDP)

$(LX)/timeSvc:    $(C_SRC_TSVC:%.c=$(LX)/%.o) $(LDEPS)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) $(LDLIBS_TRDP)

$(LX)/trainCtrl:  $(C_SRC_CTL:%.c=$(LX)/%.o)  $(C_SRC_APPUTL:%.c=$(LX)/%.o) $(LDEPS)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) $(LDLIBS_TRDP)

$(LX)/onBoardStw:  $(C_SRC_STW:%.c=$(LX)/%.o) $(C_SRC_APPUTL:%.c=$(LX)/%.o) $(LDEPS)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) $(LDLIBS_TRDP)

$(LX)/keyCtrl:     $(C_SRC_KEY:%.c=$(LX)/%.o) $(LDEPS)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) $(LDLIBS_TRDP)

$(LX)/slide-marks: $(C_SRC_TSM:%.c=$(LX)/%.o)
	@echo "  LD      $@"
	@$(CC) $(LDFLAGS) -o $@ $^ #$(LDLIBS)

clean:
	@echo "  RM      *.o *.d"
	@$(RM)  $(DEPS_ALL:%.d=%.o) $(DEPS_ALL)

#only make the UIO-svcs in jhnr installable
install:  $(LX)/trdpUioSvc $(LX)/slide-marks $(LX)/timeSvc
	echo "Installing TRDP-SVC to prefix $(prefix)"
	$(CP) $^ $(prefix)/usr/bin/

distclean: clean
	$(RM) -r $(LX)

#probably use tmux for this, it will NOT work like this
run: all
	@echo "Start './onBoardStw ../config/db.xml' and './trainCtrl ../config/db.xml' in separate processes. (I would produce a mess, if it'd started all here.)"

run-stw: $(LX)/onBoardStw
	$< ../config/db.xml

run-train: $(LX)/trainCtrl
	$< ../config/db.xml

slide-marks: $(LX)/slide-marks
	$<

chrony-uio: $(C_SRC_CNY_U)
	@patch -d "$(CHRONY)" -p1 <"$<"
	@echo "  CP       $(<F)"
	@$(CP) $? "$(CHRONY)"
	@cd "$(CHRONY)" && ./configure --sysconfdir=/etc/chrony
	@make -C "$(CHRONY)"

chrony-clean:
	-make -C "$(CHRONY)" distclean
	-rm -f $(addprefix $(CHRONY)/,$(notdir $(C_SRC_CNY_U)))
	-patch -R -d "$(CHRONY)" -p1 <"$(firstword $(C_SRC_CNY_U))"

%.o : %.c

$(addprefix $(LX)/,$(C_SRC_MODEL_STW) $(C_SRC_MODEL_CTL) $(C_SRC_MODEL_TSM) app utils.bm): 
	mkdir -p "$@"

$(LX)/$(C_SRC_MODEL_STW)/%.o : $(C_SRC_MODEL_STW)/%.c $(LX)/$(C_SRC_MODEL_STW)/%.d | $(LX)/$(C_SRC_MODEL_STW)
	@echo "  CC      $@"
	@$(CC) $(CPPFLAGS) $(CPPFLAGS_FOR_KCG) $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"

$(LX)/$(C_SRC_MODEL_CTL)/%.o : $(C_SRC_MODEL_CTL)/%.c $(LX)/$(C_SRC_MODEL_CTL)/%.d | $(LX)/$(C_SRC_MODEL_CTL)
	@echo "  CC      $@"
	@$(CC) $(CPPFLAGS) $(CPPFLAGS_FOR_KCG) $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"

$(LX)/$(C_SRC_MODEL_TSM)/%.o : $(C_SRC_MODEL_TSM)/%.c $(LX)/$(C_SRC_MODEL_TSM)/%.d | $(LX)/$(C_SRC_MODEL_TSM)
	@echo "  CC      $@"
	@$(CC) $(CPPFLAGS) $(CPPFLAGS_FOR_KCG) $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"

#on Linux, sys.c is only ever built for slide-marks, so this is not the cleanest rule
$(LX)/utils.bm/%.o : utils.bm/%.c $(LX)/utils.bm/%.d | $(LX)/utils.bm
	@echo "  CC      $@"
	@$(CC) $(CPPFLAGS) -I$(C_SRC_MODEL_TSM) $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"

$(LX)/app/slide-marks.o : app/slide-marks.c $(LX)/app/slide-marks.d | $(LX)/app
	@echo "  CC      $@"
	@$(CC) $(CPPFLAGS) -I$(C_SRC_MODEL_TSM) $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"

$(LX)/%.o : %.c $(LX)/%.d | $(LX)/app
	@echo "  CC      $@"
	@$(CC) $(CPPFLAGS) $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"

#Mention each dependency file as a target, so that make won’t fail if the file doesn’t exist.
$(DEPS_ALL) : | $(LX)

#Include the dependency files that exist.
-include $(DEPS_ALL)

