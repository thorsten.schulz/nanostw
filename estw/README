
nanoStw - an integration of the openETCS on-board function Speed and Distance Monitoring implemented as a Scade model.


terms:
   nano             -- refering to the small model train simulator using the model-scale standard 'N', that is 1:160
   Stw (DE, abrv.)  -- interlocking, (e-Stw ~ electronic interlocking)

Copyright (c) Universität Rostock, 2015-2020

Authors:
  Thorsten Schulz <thorsten.schulz@uni-rostock.de>

Notice:
  Work on this matter has received funding directly and indirectly from the European Union through different research grants.

SPDX-License-Identifier: Apache-2.0


The project consists of multiple components. Some of them can be built for different targets and architectures
   -- your milage may vary.

Major components are:
 * Stw             The wrapper for the Scade model of Speed and Distance Monitoring and a Movement-Authority generator
 * Ctrl            A small Scade model that simulates the movement of the train and read minimal controls from the user.
 * onBoardStw,     These are the versions of Stw and Ctrl where TRDP is burnt in. Easier to run on Linux.
   trainCtrl       (see 'make run-stw' and 'make run-train').
 * TrdpSvc         However, for all the Hypervisor targets and optionally for Linux, the library is in a separate
                   component for demonstration of the MILS architecture approach. They processes communicate via SHM 
                   with different target abstractions.
 * Gleis-GUI       {see separate repository} Visualizes the braking curves and the trains on a track map. It also acts 
                   as a source of track-data for the Stw. Gleis-GUI requires Qt4 and is required for Stw and Ctrl.
 * openETCS-DMI    An on-board Driver-Machine-Interface. Sideshow toy, only very limited functionality.
 * Android App     {see separate repository} An Android version of @Ctrl that also connects to the model railway trains
                   via Bluetooth-LE.
 * libtrdp         This is a separate, but forked library from the TCNopen project. Herein included is a stripped-down
                   version of the fork that is enough to compile the library used in the components. It also contains a
                   WireShark dissector and the ability to build the library required by Gleis-GUI. The latter requires
                   the Debian packaging infrastructure.

The Makefile next to this README is the central control tool. It has a long list of targets and pulls in multiple
sub-Makefiles / sub-folders.

With the second upgrade, the encrypted PikeOS target (tindyguard) and the jailhouse targets can be built. JH requires
other external checked-out repos at magic positions, so this is considered internal yet. The PikeOS-integration with
tindyguard seems to deadlock the scheduler, so it does not work.

Network scripts:
 * network.sh      Setup local networks for host-Linux-only tests.
 * network-qemu.sh Setup local networks for PikeOS-QEMU integration of STW and all other tools on the host Linux.
   + network-qemu-wg.sh is an add-on to the former to route "uplink" traffic to the Gleis-GUI via a WireGuard encrypted
                        connection.



