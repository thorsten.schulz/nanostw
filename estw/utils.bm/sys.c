/*
 * A system integration helper for Scade models, a work in progress library.
 *
 * Used w/ Windows-API, Linux, PikeOS and lwip/TI-Hercules
 *
 * Authors:
 *  Thorsten Schulz  <thorsten.schulz@uni-rostock.de>
 * 
 * (c) Universität Rostock 2017-2020
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* to avoid inclusion of the default empty user_macros.h, force-include it here */
#include "../user_macros.h"

#include "kcg_types.h"
#include "kcg_consts.h"
#include "kcg_imported_functions.h"
#include "kcg_imported_types.h"

#ifndef __USE_MISC
#define __USE_MISC
#endif

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#ifdef PIKEOS_NATIVE
#include <vm.h>                   /* PSSW API */
#include <stand/string.h>         /* memset() */
#include <time_devrtc.h>          /* requires rtc.dev component */
#include <net/util/inet.h>
#elif defined( JAILHOUSE )
#include <inmate.h>
struct timespec {
	long long tv_sec;
	unsigned long long tv_nsec;
};
#else
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#ifndef SIOCGSTAMPNS
#include <linux/sockios.h>
#endif
#endif

/* in case SCADE does not provide/use these. We, however, need them: */
typedef int32_t length_t_udp;
typedef int fd_t_udp;
typedef kcg_int32 time_t_sys;

#ifndef nil_udp
#define nil_udp (-1)
#endif
#ifndef invalidSocket_udp
#define invalidSocket_udp nil_udp
#endif

void now_sys( /* millis */ time_t_sys *millis, /* nanos */ kcg_int64 *nanos);
void nowfd(fd_t_udp fd, /* millis */ time_t_sys *millis, /* nanos */ kcg_int64 *nanos);
void tai_sys( kcg_bool bigEndian, kcg_uint32 *xsec, kcg_uint32 *sec, kcg_uint32 *nano);

kcg_uint32 htonl_sys_specialization(/* h/ */ kcg_uint32 h);
kcg_uint16 htons_sys_specialization(/* h/ */ kcg_uint16 h);

kcg_bool random_sys(uint32_t *data, length_t_udp length);
void random_sys_bytes(uint32_t *buffer, size_t size);
#ifdef kcg_copy_array_uint32_1
void random01_sys_specialization(kcg_bool *fail, array_uint32_1 *data);
#endif
#ifdef kcg_copy_array_uint32_2
void random02_sys_specialization(kcg_bool *fail, array_uint32_2 *data);
#endif
#ifdef kcg_copy_array_uint32_4
void random04_sys_specialization(kcg_bool *fail, array_uint32_4 *data);
#endif
#ifdef kcg_copy_array_uint32_8
void random08_sys_specialization(kcg_bool *fail, array_uint32_8 *data);
#endif
#ifdef kcg_copy_array_uint32_16
void random16_sys_specialization(kcg_bool *fail, array_uint32_16 *data);
#else
#ifdef kcg_copy_chunk_t_udp
void random16_sys_specialization(kcg_bool *fail, chunk_t_udp *data);
#endif
#endif

static int64_t 	baseTime = -1;

#if (defined(__WIN32__) || defined(WIN32) || defined(__CYGWIN32__))
#if !defined(WINDOWS)
#define WINDOWS
#endif
#elif defined( JAILHOUSE )
/* no adjusts */
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__APPLE__)
#ifndef LINUX
#define LINUX
#endif
#elif PIKEOS_NATIVE
/* no adjusts */
#else /* _RM57Lx_ */

#endif

#ifdef PIKEOS_NATIVE
#define dprint(...) vm_cprintf("sys:: "__VA_ARGS__)
#define iprint(...) vm_cprintf("sys:: "__VA_ARGS__)
#elif defined( _RM57Lx_ )
#define dprint(...) { char s[256]; snprintf(s, sizeof(s), __VA_ARGS__); sciDisplayText(s); }
#define iprint(...) { char s[256]; snprintf(s, sizeof(s), __VA_ARGS__); sciDisplayText(s); }
#elif defined( JAILHOUSE )
#define dprint(...)	printk("sys:! "__VA_ARGS__)
#define iprint(...)	printk("sys:: "__VA_ARGS__)
#else
#define dprint(...)	fprintf(stderr, "sys:: "__VA_ARGS__), fflush(stderr)
#define iprint(...)	fprintf(stdout, "sys:: "__VA_ARGS__), fflush(stdout)
#include <sys/ioctl.h>
#endif

#ifndef __unused
#define __unused __attribute__((unused))
#endif

#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)>(b))?(b):(a))

#ifdef WINDOWS
#include <wtypes.h>
#include <winioctl.h>
#define CLOCK_REALTIME 0
int clock_gettime(int id, struct timespec *spec) {
	__int64 wintime; GetSystemTimeAsFileTime((FILETIME*)&wintime);
	wintime      -=116444736000000000LL;  /*1. Jan 1901 to 1. Jan 1970 */
	spec->tv_sec  =wintime / 10000000LL;
	spec->tv_nsec =wintime % 10000000LL *100;
	return 0;
}
#endif


void now_sys( /* millis */ time_t_sys *millis, /* nanos */ kcg_int64 *nanos) {
#if !defined( JAILHOUSE )
	struct timespec tp = {0,0};
#ifdef CLOCK_TAI
	clock_gettime(CLOCK_TAI, &tp);
#else
	clock_gettime(CLOCK_REALTIME, &tp);
#endif
	int64_t t = (int64_t)tp.tv_sec*1000000000LL+tp.tv_nsec;
#else
	uint64_t t = tsc_read_ns();
#endif
	*nanos = t;
	t /= 1000000LL;
	if (baseTime < 0) baseTime = t;
	*millis = (t - baseTime);
}

void nowfd(fd_t_udp fd __unused, /* millis */ time_t_sys *millis, /* nanos */ kcg_int64 *nanos) {
#ifdef LINUX
	struct timespec tp_ioctl = {0,0};

	if (( fd == (fd_t_udp)invalidSocket_udp ) || ( ioctl(fd, SIOCGSTAMPNS, &tp_ioctl) < 0 )) {
#endif
		now_sys( millis, nanos );
#ifdef LINUX
	} else {
		int64_t t = (int64_t)tp_ioctl.tv_sec*1000000000LL+tp_ioctl.tv_nsec;
		*nanos = t;
		t /= 1000000LL; /* nano -> milli */
		if (baseTime < 0) baseTime = t;
		*millis = (t - baseTime);
	}
#endif
}

void tai_sys( kcg_bool bigEndian, kcg_uint32 *xsec, kcg_uint32 *sec, kcg_uint32 *nano) {
	struct timespec tp = {0,0};
#ifdef CLOCK_TAI
	clock_gettime(CLOCK_TAI, &tp);
#else
#ifdef JAILHOUSE
	tp.tv_nsec = tsc_read_ns();
	tp.tv_sec = tp.tv_nsec / 1000000000ULL;
	tp.tv_nsec= tp.tv_nsec % 1000000000ULL;
#else
	clock_gettime(CLOCK_REALTIME, &tp);
#endif
#endif
	/* offset = 4611686018427387914 = 0x400000000000000A = 4000 0000 0000 000A*/
	/* the 0xA part is due to Gregorian-Julian-Calendar rivalry */
	if (bigEndian) {
		*xsec = htonl_sys_specialization(0x40000000);
		*sec = htonl_sys_specialization(tp.tv_sec);
		*nano = htonl_sys_specialization(tp.tv_nsec);
	} else {
		*xsec = 0x40000000;
		*sec = tp.tv_sec;
		*nano = tp.tv_nsec;
	}
}

#ifndef JAILHOUSE
kcg_uint32 htonl_sys_specialization(/* h/ */ kcg_uint32 h) { return htonl(h); }
kcg_uint16 htons_sys_specialization(/* h/ */ kcg_uint16 h) { return htons(h); }
#else
/* I only have LITTLE systems */
kcg_uint32 htonl_sys_specialization(/* h/ */ kcg_uint32 h) { return (h << 24) | (h >> 24) | ((h << 8)&0xFF0000) | ((h >> 8)&0x00FF00) ; }
kcg_uint16 htons_sys_specialization(/* h/ */ kcg_uint16 h) { return (h << 8) | (h >> 8); }
#endif

/*
 * following random for Linux/Win is "exported" from noise-c library
 */

#ifdef WINDOWS
#include <bcrypt.h>
#define RANDOM_WIN32    1
#elif defined (_RM57Lx_)
#warning Hercules cannot create data entropy
#elif defined(LINUX)
#define RANDOM_DEVICE   "/dev/urandom"
#include <fcntl.h>
#elif defined(PIKEOS_NATIVE)
#define RANDOM_GATE     "random:"
#elif defined( JAILHOUSE )
#else
#error platform undefined
#endif

kcg_bool random_sys(uint32_t *data, length_t_udp length) {
#if defined(RANDOM_DEVICE)
	int fd = open(RANDOM_DEVICE, O_RDONLY);
	if (fd >= 0) {
		for (int i=0;i<5;i++) {
			int len = read(fd, data, length);
			if (len == (int)length) {
				/* We have the bytes we wanted */
				close(fd);
				return kcg_false;
			} else if (len >= 0) {
				/* Short read - this shouldn't happen.  Treat it as "no data" */
				break;
			} else if (errno != EINTR) {
				/* Some other error than "interrupted due to signal" */
				perror(RANDOM_DEVICE);
				break;
			}
		}
		close(fd);
	} else {
		dprint("[FAILED (%d)] random(\"%s\", length=%d) = %s\n", errno, RANDOM_DEVICE, length, strerror(errno));
	}
#elif defined(PIKEOS_NATIVE)
	static P4_e_t status = P4_E_STATE;
	static vm_file_desc_t fdrandom;

	if (!data) {
		dprint("[FAILED (NULL buffer)] random(\"%s\", length=%d)\n", RANDOM_GATE, length);
		return kcg_true;
	}

	P4_size_t rx;
	if (status == P4_E_STATE)
		status = vm_open(RANDOM_GATE, VM_O_RD, &fdrandom);

	if (status == P4_E_OK && length)
		status = vm_read(&fdrandom, data, length, &rx);

	if (status == P4_E_OK) return kcg_false;
	else
		dprint("[FAILED (%d)] random(\"%s\", length=%d)\n", status, RANDOM_GATE, length);

#elif defined(WINDOWS)

	if ( BCryptGenRandom( NULL, (uint8_t *)data, length, BCRYPT_USE_SYSTEM_PREFERRED_RNG) == 0 /* success */)
		return kcg_false;
	else
		dprint("[FAILED (?)] random()\n");
#else
	/* until then, just source predictability instead of randomness*/
	unsigned long long micros = tsc_read_ns();
	data[0] = (micros >> 27) ^ (micros >> 47) ^ (micros << 5);
	for (length_t_udp i=1; i<(length+3)/4; i++) {
		data[i] = data[i-1] ^ (data[i-1] << 13) ^ (data[i-1] >> 3) ^ (data[i-1] << ((data[0]^i)&31));
	}
	return kcg_false;
#endif

	return kcg_true;
}

void random_sys_bytes(uint32_t *buffer, size_t size) {
	random_sys(buffer, size&(~3));
}

#ifdef kcg_copy_array_uint32_1
void random01_sys_specialization(kcg_bool *fail, array_uint32_1 *data) {
	*fail = random_sys(*data, 1*sizeof(kcg_uint32));
}
#endif

#ifdef kcg_copy_array_uint32_2
void random02_sys_specialization(kcg_bool *fail, array_uint32_2 *data) {
	*fail = random_sys(*data, 2*sizeof(kcg_uint32));
}
#endif

#ifdef kcg_copy_array_uint32_4
void random04_sys_specialization(kcg_bool *fail, array_uint32_4 *data) {
	*fail = random_sys(*data, 4*sizeof(kcg_uint32));
}
#endif

#ifdef kcg_copy_array_uint32_8
void random08_sys_specialization(kcg_bool *fail, array_uint32_8 *data) {
	*fail = random_sys(*data, 8*sizeof(kcg_uint32));
}
#endif

#ifdef kcg_copy_array_uint32_16
void random16_sys_specialization(kcg_bool *fail, array_uint32_16 *data) {
	*fail = random_sys(*data,16*sizeof(kcg_uint32));
}
#else
#ifdef kcg_copy_chunk_t_udp
void random16_sys_specialization(kcg_bool *fail, chunk_t_udp *data) {
	*fail = random_sys(*data,16*sizeof(kcg_uint32));
}
#endif
#endif
