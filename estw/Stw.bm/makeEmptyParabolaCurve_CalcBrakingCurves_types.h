/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _makeEmptyParabolaCurve_CalcBrakingCurves_types_H_
#define _makeEmptyParabolaCurve_CalcBrakingCurves_types_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* CalcBrakingCurves_types::makeEmptyParabolaCurve */
extern void makeEmptyParabolaCurve_CalcBrakingCurves_types(
  /* CalcBrakingCurves_types::makeEmptyParabolaCurve::Curve */ ParabolaCurve_T_CalcBrakingCurves_types *Curve);

#endif /* _makeEmptyParabolaCurve_CalcBrakingCurves_types_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** makeEmptyParabolaCurve_CalcBrakingCurves_types.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

