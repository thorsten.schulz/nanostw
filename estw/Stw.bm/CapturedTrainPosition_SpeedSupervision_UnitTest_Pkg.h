/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg_H_
#define _CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition */
extern void CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg(
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::PR */ TrainData_SpeedSupervision_UnitTest_Pkg *PR,
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::odo_abs_diversion */ L_internal_Type_Obu_BasicTypes_Pkg odo_abs_diversion,
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::odo */ odometry_T_Obu_BasicTypes_Pkg *odo);

#endif /* _CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

