/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "nanoSDM_SpeedSupervision_UnitTest_Pkg_35.h"

#ifndef KCG_USER_DEFINED_INIT
void nanoSDM_init_SpeedSupervision_UnitTest_Pkg_35(
  outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35 *outC)
{
  kcg_int i;
  
  outC->toIX.Rear = 0;
  outC->toIX.Front = 0;
  outC->toIX.PI = 0;
  outC->toIX.Indication = 0;
  outC->toIX.Permitted = 0;
  outC->toIX.FLOI = 0;
  outC->toIX.Target = 0;
  outC->toIX.ProtectedFront = 0;
  outC->InterventionCmd.targetDistance = 0;
  outC->InterventionCmd.permittedSpeed = 0;
  outC->InterventionCmd.interventionSpeed = 0;
  outC->InterventionCmd.eb = 0;
  outC->InterventionCmd.sb = 0;
  outC->InterventionCmd.tco = 0;
  outC->sdmToDMI.sdm.valid = kcg_true;
  outC->sdmToDMI.sdm.targetSpeed = 0;
  outC->sdmToDMI.sdm.permittedSpeed = 0;
  outC->sdmToDMI.sdm.releaseSpeed = 0;
  outC->sdmToDMI.sdm.locationBrakeTarget = 0;
  outC->sdmToDMI.sdm.location_brake_curve_starting_point = 0;
  outC->sdmToDMI.sdm.interventionSpeed = 0;
  outC->sdmToDMI.sdm.sup_status = CSM_DMI_Types_Pkg;
  outC->sdmToDMI.sdm.supervisionDisplay = supDis_normal_DMI_Types_Pkg;
  outC->sdmToDMI.sdm.distanceIndicationPoint = 0;
  outC->sdmToDMI.odo.valid = kcg_true;
  outC->sdmToDMI.odo.timestamp = 0;
  outC->sdmToDMI.odo.odo.o_nominal = 0;
  outC->sdmToDMI.odo.odo.o_min = 0;
  outC->sdmToDMI.odo.odo.o_max = 0;
  outC->sdmToDMI.odo.speed.v_safeNominal = 0;
  outC->sdmToDMI.odo.speed.v_rawNominal = 0;
  outC->sdmToDMI.odo.speed.v_lower = 0;
  outC->sdmToDMI.odo.speed.v_upper = 0;
  outC->sdmToDMI.odo.acceleration = 0;
  outC->sdmToDMI.odo.motionState = noMotion_Obu_BasicTypes_Pkg;
  outC->sdmToDMI.odo.motionDirection = unknownDirection_Obu_BasicTypes_Pkg;
  outC->sdmToDMI.brake.valid = kcg_true;
  outC->sdmToDMI.brake.m_servicebrake_cm =
    brake_signal_command_not_defined_TIU_Types_Pkg;
  outC->sdmToDMI.brake.m_emergencybrake_cm =
    brake_signal_command_not_defined_TIU_Types_Pkg;
  outC->sdmToDMI.ta.valid = kcg_true;
  outC->sdmToDMI.ta.system_clock = 0;
  outC->sdmToDMI.ta.speedProfiles.profileChanged = kcg_true;
  for (i = 0; i < 32; i++) {
    outC->sdmToDMI.ta.speedProfiles.speedProfiles[i].valid = kcg_true;
    outC->sdmToDMI.ta.speedProfiles.speedProfiles[i].Loc_Abs = 0;
    outC->sdmToDMI.ta.speedProfiles.speedProfiles[i].Loc_LRBG = 0;
    outC->sdmToDMI.ta.speedProfiles.speedProfiles[i].MRS = 0;
  }
  outC->sdmToDMI.ta.gradientProfiles.profileChanged = kcg_true;
  for (i = 0; i < 32; i++) {
    outC->sdmToDMI.ta.gradientProfiles.gradientProfiles[i].valid = kcg_true;
    outC->sdmToDMI.ta.gradientProfiles.gradientProfiles[i].begin_section = 0;
    outC->sdmToDMI.ta.gradientProfiles.gradientProfiles[i].end_section = 0;
    outC->sdmToDMI.ta.gradientProfiles.gradientProfiles[i].gradient = 0;
  }
  outC->sdmToDMI.ta.trackConditions.nIter = 0;
  for (i = 0; i < 32; i++) {
    outC->sdmToDMI.ta.trackConditions.trackCondition[i].d_trackcond = 0;
    outC->sdmToDMI.ta.trackConditions.trackCondition[i].m_trackcond =
      M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted;
  }
  /* 1 */
  SpeedSupervision_Integration_init_SpeedSupervision_Integration_Pkg(
    &outC->_2_Context_1);
  /* 1 */
  GenerateTrack_init_SpeedSupervision_UnitTest_Pkg_35(&outC->_1_Context_1);
  /* 1 */ fakeOdometricTrainPosition_init_SDM_Types_Pkg(&outC->Context_1);
}
#endif /* KCG_USER_DEFINED_INIT */


void nanoSDM_reset_SpeedSupervision_UnitTest_Pkg_35(
  outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35 *outC)
{
  /* 1 */
  SpeedSupervision_Integration_reset_SpeedSupervision_Integration_Pkg(
    &outC->_2_Context_1);
  /* 1 */
  GenerateTrack_reset_SpeedSupervision_UnitTest_Pkg_35(&outC->_1_Context_1);
  /* 1 */ fakeOdometricTrainPosition_reset_SDM_Types_Pkg(&outC->Context_1);
}

/* SpeedSupervision_UnitTest_Pkg::nanoSDM */
void nanoSDM_SpeedSupervision_UnitTest_Pkg_35(
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::PR_TrainData */ TrainData_SpeedSupervision_UnitTest_Pkg *PR_TrainData,
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::TA_data */ TrackAtlasData_SpeedSupervision_UnitTest_Pkg *TA_data,
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::TA_sections */ array_21157 *TA_sections,
  outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35 *outC)
{
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM */ trainProperties_T_TrainPosition_Types_Pck mk_struct;
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM */ MRSP_Profile_t_TrackAtlasTypes tmp3;
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM */ P3_NationalValues_T_Packet_Types_Pkg tmp2;
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM */ V_odometry_Type_Obu_BasicTypes_Pkg tmp1;
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM */ kcg_int tmp;
  /* SpeedSupervision_UnitTest_Pkg::ReasonableTrainData::_L70 */ sTractionIdentity_T_Packet_TrainTypes_Pkg _L70_1;
  /* SpeedSupervision_UnitTest_Pkg::ReasonableTrainData::_L21 */ LocWithInAcc_T_Obu_BasicTypes_Pkg _L21_1;
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::_L9 */ odometry_T_Obu_BasicTypes_Pkg _L9;
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::_L14 */ trainData_T_TIU_Types_Pkg _L14;
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::_L127 */ DataForSupervision_nextGen_t_TrackAtlasTypes _L127;
  
  _L21_1.nominal = 0;
  _L21_1.d_min = 0 - 5;
  _L21_1.d_max = 0 + 5;
  _L14.valid = kcg_true;
  _L14.acknowledgedByDriver = kcg_true;
  _L14.trainCategory = NC_TRAIN_Passenger_train;
  _L14.cantDeficientcy = NC_CDTRAIN_Cant_Deficiency_165_mm;
  _L14.trainLength = (*PR_TrainData).length;
  _L14.brakePerctage = (*PR_TrainData).BrPc;
  _L14.maxTrainSpeed = (*PR_TrainData).vMax;
  _L14.loadingGauge = M_LOADINGGAUGE_G1;
  _L14.axleLoadCategory = M_AXLELOADCAT_C4;
  _L14.airtightSystem = M_AIRTIGHT_Not_fitted;
  _L14.axleNumber = 12;
  _L14.numberNationalSystems = 2;
  _L14.numberTractionSystems = 2;
  _L70_1.m_voltage = M_VOLTAGE_AC_15_kV_16_7_Hz;
  _L70_1.nid_ctraction = 2;
  /* 1 */
  CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg(
    PR_TrainData,
    3 * 160,
    &_L9);
  kcg_copy_odometry_T_Obu_BasicTypes_Pkg(&outC->sdmToDMI.odo, &_L9);
  for (tmp = 0; tmp < 4; tmp++) {
    kcg_copy_sTractionIdentity_T_Packet_TrainTypes_Pkg(
      &_L14.tractionSystem[tmp],
      &_L70_1);
  }
  for (tmp = 0; tmp < 5; tmp++) {
    _L14.nationSystems[tmp] = 2;
  }
  /* 1 */
  fakeOdometricTrainPosition_SDM_Types_Pkg(&_L9, &_L14, &outC->Context_1);
  outC->toIX.Rear = outC->Context_1.tpos.estimatedRearEndPosition;
  outC->toIX.Front = outC->Context_1.tpos.estimatedFrontEndPosition;
  /* 2 */ for (tmp = 0; tmp < 35; tmp++) {
    /* 1 */
    mapEStwSections_SpeedSupervision_UnitTest_Pkg(
      &(*TA_sections)[tmp],
      &tmp3[tmp]);
  }
  /* 1 */
  GenerateTrack_SpeedSupervision_UnitTest_Pkg_35(
    (*TA_data).EoA,
    (*TA_data).vRelease,
    kcg_false,
    (GradientProfile_t_TrackAtlasTypes *)
      &DEFAULT_GradientProfile_TrackAtlasTypes,
    (*TA_data).fresh,
    &tmp3,
    &outC->_1_Context_1);
  kcg_copy_DataForSupervision_nextGen_t_TrackAtlasTypes(
    &_L127,
    &outC->_1_Context_1.TA);
  /* 1 */ DefaultNationalValues_SpeedSupervision_UnitTest_Pkg(&tmp2);
  mk_struct.nid_engine = 0;
  mk_struct.nid_operational = 0;
  mk_struct.l_train = (*PR_TrainData).length;
  mk_struct.d_baliseAntenna_2_frontend.nominal = (*PR_TrainData).Ant2Cab;
  mk_struct.d_baliseAntenna_2_frontend.d_min = (*PR_TrainData).Ant2Cab - 5;
  mk_struct.d_baliseAntenna_2_frontend.d_max = (*PR_TrainData).Ant2Cab + 5;
  mk_struct.d_frontend_2_rearend.nominal = (*PR_TrainData).length;
  mk_struct.d_frontend_2_rearend.d_min = (*PR_TrainData).length - 5;
  mk_struct.d_frontend_2_rearend.d_max = (*PR_TrainData).length + 5;
  kcg_copy_LocWithInAcc_T_Obu_BasicTypes_Pkg(
    &mk_struct.locationAccuracy_DefaultValue,
    &_L21_1);
  kcg_copy_LocWithInAcc_T_Obu_BasicTypes_Pkg(
    &mk_struct.centerDetectionAcc_DefaultValue,
    &_L21_1);
  /* 1 */
  SpeedSupervision_Integration_SpeedSupervision_Integration_Pkg(
    &tmp2,
    &outC->Context_1.tpos,
    &_L9,
    &mk_struct,
    &_L14,
    &_L127,
    kcg_false,
    kcg_false,
    &outC->_2_Context_1);
  outC->toIX.PI = outC->_2_Context_1.limits.EBD_preindication_location;
  outC->toIX.Indication = outC->_2_Context_1.limits.d_I_of_V_est;
  outC->toIX.Permitted = outC->_2_Context_1.limits.d_P_of_V_est;
  outC->toIX.FLOI = outC->_2_Context_1.limits.d_FLOI_of_V_est;
  outC->toIX.Target = outC->_2_Context_1.target.distance;
  outC->toIX.ProtectedFront = - outC->_2_Context_1.limits.d_FLOI_of_V_est +
    outC->_2_Context_1.target.distance +
    outC->Context_1.tpos.estimatedFrontEndPosition;
  outC->InterventionCmd.targetDistance =
    outC->_2_Context_1.sdmCommands.targetDistance;
  kcg_copy_Brake_command_T_TIU_Types_Pkg(
    &outC->sdmToDMI.brake,
    &outC->_2_Context_1.brakeCmd);
  kcg_copy_speedSupervisionForDMI_T_DMI_Types_Pkg(
    &outC->sdmToDMI.sdm,
    &outC->_2_Context_1.sdmToDMI);
  outC->sdmToDMI.sdm.valid = (*PR_TrainData).flag.active;
  /* 1 */
  transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35(
    &_L127,
    (*TA_data).EoA,
    &outC->sdmToDMI.ta);
  switch (outC->_2_Context_1.sdmCommands.sdmType) {
    case No_SDM_Type_SDM_Types_Pkg :
      outC->InterventionCmd.permittedSpeed =
        outC->_2_Context_1.sdmCommands.permittedSpeed;
      tmp1 = outC->_2_Context_1.sdmCommands.sbiSpeed;
      break;
    case RSM_SDM_Types_Pkg :
      outC->InterventionCmd.permittedSpeed =
        outC->_2_Context_1.sdmCommands.releaseSpeed;
      tmp1 = outC->_2_Context_1.sdmCommands.mrdtSpeed;
      break;
    case TSM_SDM_Types_Pkg :
      outC->InterventionCmd.permittedSpeed =
        outC->_2_Context_1.sdmCommands.mrdtSpeed;
      tmp1 = outC->_2_Context_1.sdmCommands.sbiSpeed;
      break;
    case CSM_SDM_Types_Pkg :
      outC->InterventionCmd.permittedSpeed =
        outC->_2_Context_1.sdmCommands.permittedSpeed;
      tmp1 = outC->_2_Context_1.sdmCommands.sbiSpeed;
      break;
    
  }
  outC->InterventionCmd.interventionSpeed = tmp1;
  /* 4 */ if (outC->_2_Context_1.sdmCommands.triggeredSB) {
    outC->InterventionCmd.eb = IVcmd_apply_SpeedSupervision_UnitTest_Pkg;
    tmp = IVcmd_apply_SpeedSupervision_UnitTest_Pkg;
  }
  else /* 3 */ if (outC->_2_Context_1.sdmCommands.revokedSB) {
    outC->InterventionCmd.eb = IVcmd_release_SpeedSupervision_UnitTest_Pkg;
    tmp = IVcmd_release_SpeedSupervision_UnitTest_Pkg;
  }
  else {
    outC->InterventionCmd.eb = IVcmd_not_defined_SpeedSupervision_UnitTest_Pkg;
    tmp = IVcmd_not_defined_SpeedSupervision_UnitTest_Pkg;
  }
  outC->InterventionCmd.sb = tmp;
  /* 1 */ if (outC->_2_Context_1.sdmCommands.triggeredTCO) {
    outC->InterventionCmd.tco = IVcmd_apply_SpeedSupervision_UnitTest_Pkg;
  }
  else /* 2 */ if (outC->_2_Context_1.sdmCommands.revokedTCO) {
    outC->InterventionCmd.tco = IVcmd_release_SpeedSupervision_UnitTest_Pkg;
  }
  else {
    outC->InterventionCmd.tco = IVcmd_not_defined_SpeedSupervision_UnitTest_Pkg;
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** nanoSDM_SpeedSupervision_UnitTest_Pkg_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

