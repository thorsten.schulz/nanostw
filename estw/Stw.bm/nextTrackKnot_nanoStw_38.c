/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "nextTrackKnot_nanoStw_38.h"

/* nanoStw::nextTrackKnot */
void nextTrackKnot_nanoStw_38(
  /* nanoStw::nextTrackKnot::track */ TrackKnot_nanoStw *track,
  /* nanoStw::nextTrackKnot::goCW */ kcg_bool goCW,
  /* nanoStw::nextTrackKnot::tracks */ array_21324 *tracks,
  /* nanoStw::nextTrackKnot::ntrack */ TrackKnot_nanoStw *ntrack,
  /* nanoStw::nextTrackKnot::isOn */ kcg_bool *isOn,
  /* nanoStw::nextTrackKnot::isNLineBlunt */ kcg_bool *isNLineBlunt)
{
  /* nanoStw::nextTrackKnot */ TrackKnotLine_nanoStw tmp;
  /* nanoStw::nextTrackKnot::_L7 */ TrackKnotLine_nanoStw _L7;
  /* nanoStw::nextTrackKnot::_L24 */ kcg_bool _L24;
  
  /* 1 */ if (((*track).sid < UnknownSid_nanoStw) != goCW) {
    kcg_copy_TrackKnotLine_nanoStw(&_L7, &(*track).line[0]);
  }
  else /* 2 */ if ((*track).line[2].on) {
    kcg_copy_TrackKnotLine_nanoStw(&_L7, &(*track).line[2]);
  }
  else /* 3 */ if ((*track).line[3].on) {
    kcg_copy_TrackKnotLine_nanoStw(&_L7, &(*track).line[3]);
  }
  else {
    kcg_copy_TrackKnotLine_nanoStw(&_L7, &(*track).line[1]);
  }
  /* 1 */ selectTrackBySid_nanoStw_38(_L7.to, tracks, ntrack, &_L24);
  if ((0 <= _L7.rLine) & (_L7.rLine < 4)) {
    kcg_copy_TrackKnotLine_nanoStw(&tmp, &(*ntrack).line[_L7.rLine]);
  }
  else {
    kcg_copy_TrackKnotLine_nanoStw(
      &tmp,
      (TrackKnotLine_nanoStw *) &emptyTrackLine_nanoStw);
  }
  *isOn = tmp.on;
  *isNLineBlunt = _L7.rLine == TKL_BLUNT_nanoStw;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** nextTrackKnot_nanoStw_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

