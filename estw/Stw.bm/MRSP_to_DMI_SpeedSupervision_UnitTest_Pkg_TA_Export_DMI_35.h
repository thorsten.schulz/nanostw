/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35_H_
#define _MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35_H_

#include "kcg_types.h"
#include "MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h"
#include "MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI */
extern void MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::fresh */ kcg_bool fresh,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::MRSP_in */ MRSP_Profile_t_TrackAtlasTypes *MRSP_in,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::MRSP_to_DMI */ DMI_speedProfile_T_DMI_Types_Pkg *MRSP_to_DMI);

#endif /* _MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

