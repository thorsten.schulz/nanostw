/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "exportTrainEstate_nanoStw_35_38.h"

/* nanoStw::exportTrainEstate */
void exportTrainEstate_nanoStw_35_38(
  /* nanoStw::exportTrainEstate::Tlocs */ ProtectionLocations_SpeedSupervision_UnitTest_Pkg *Tlocs,
  /* nanoStw::exportTrainEstate::sections */ array_21021 *sections,
  /* nanoStw::exportTrainEstate::runsCW */ kcg_bool runsCW,
  /* nanoStw::exportTrainEstate::TrainRelLimitLoc */ array_20997 *TrainRelLimitLoc)
{
  kcg_int i;
  
  /* 1 */ for (i = 0; i < 38; i++) {
    /* 1 */
    collectTrack_nanoStw_35(
      i,
      sections,
      Tlocs,
      runsCW,
      &(*TrainRelLimitLoc)[i]);
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** exportTrainEstate_nanoStw_35_38.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

