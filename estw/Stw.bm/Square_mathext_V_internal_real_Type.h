/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _Square_mathext_V_internal_real_Type_H_
#define _Square_mathext_V_internal_real_Type_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* mathext::Square */
extern kcg_real Square_mathext_V_internal_real_Type(
  /* mathext::Square::Square_In */ kcg_real Square_In);

#endif /* _Square_mathext_V_internal_real_Type_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** Square_mathext_V_internal_real_Type.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

