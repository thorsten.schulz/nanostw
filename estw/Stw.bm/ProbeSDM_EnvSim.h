/*
 * ProbeSDM_EnvSim.h
 *
 *  Created on: 06.02.2016, last modified 06.06.2017
 *      Author: thorsten
 */

#ifndef SDM_PROBESDM_ENVSIM_H_
#define SDM_PROBESDM_ENVSIM_H_

/* this is a dummy helper */
typedef char     outC_ProbeSDM_EnvSim[1];
typedef char sdm_outC_ProbeSDM_EnvSim[1];
typedef char  gT_outC_ProbeSDM_EnvSim[1];

#define ProbeSDM_init_EnvSim(outC)
#define ProbeSDM_reset_EnvSim(outC)
#define ProbeSDM_EnvSim(a,b,c,d,e,f)

#define sdm_ProbeSDM_init_EnvSim(outC)
#define sdm_ProbeSDM_reset_EnvSim(outC)
#define sdm_ProbeSDM_EnvSim(a,b,c,d,e,f)

#define gT_ProbeSDM_init_EnvSim(outC)
#define gT_ProbeSDM_reset_EnvSim(outC)
#define gT_ProbeSDM_EnvSim(a,b,c,d,e,f)

#endif /* SDM_PROBESDM_ENVSIM_H_ */
