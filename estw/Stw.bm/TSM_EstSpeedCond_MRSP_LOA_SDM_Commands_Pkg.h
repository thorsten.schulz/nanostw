/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _TSM_EstSpeedCond_MRSP_LOA_SDM_Commands_Pkg_H_
#define _TSM_EstSpeedCond_MRSP_LOA_SDM_Commands_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDM_Commands_Pkg::TSM_EstSpeedCond_MRSP_LOA */
extern void TSM_EstSpeedCond_MRSP_LOA_SDM_Commands_Pkg(
  /* SDM_Commands_Pkg::TSM_EstSpeedCond_MRSP_LOA::speeds */ Speeds_T_SDM_Types_Pkg *speeds,
  /* SDM_Commands_Pkg::TSM_EstSpeedCond_MRSP_LOA::triggerConds */ TSM_triggerCond_T_SDM_Commands_Pkg *triggerConds);

#endif /* _TSM_EstSpeedCond_MRSP_LOA_SDM_Commands_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** TSM_EstSpeedCond_MRSP_LOA_SDM_Commands_Pkg.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

