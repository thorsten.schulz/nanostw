/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _guessTrackKnotLine_nanoStw_38_H_
#define _guessTrackKnotLine_nanoStw_38_H_

#include "kcg_types.h"
#include "selectTrackBySid_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::guessTrackKnotLine */
extern void guessTrackKnotLine_nanoStw_38(
  /* nanoStw::guessTrackKnotLine::loc */ TrackLocation_nanoStw *loc,
  /* nanoStw::guessTrackKnotLine::tracks */ array_21324 *tracks,
  /* nanoStw::guessTrackKnotLine::vOnSight */ VelocityUnit vOnSight,
  /* nanoStw::guessTrackKnotLine::vMax */ VelocityUnit *vMax,
  /* nanoStw::guessTrackKnotLine::track */ TrackKnot_nanoStw *track);

#endif /* _guessTrackKnotLine_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** guessTrackKnotLine_nanoStw_38.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

