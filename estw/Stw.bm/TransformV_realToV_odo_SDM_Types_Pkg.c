/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "TransformV_realToV_odo_SDM_Types_Pkg.h"

/* SDM_Types_Pkg::TransformV_realToV_odo */
V_odometry_Type_Obu_BasicTypes_Pkg TransformV_realToV_odo_SDM_Types_Pkg(
  /* SDM_Types_Pkg::TransformV_realToV_odo::v_real */ V_internal_real_Type_SDM_Types_Pkg v_real)
{
  /* SDM_Types_Pkg::TransformV_realToV_odo::v_odo */ V_odometry_Type_Obu_BasicTypes_Pkg v_odo;
  
  v_odo = (kcg_int) (v_real * 100.0);
  return v_odo;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** TransformV_realToV_odo_SDM_Types_Pkg.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

