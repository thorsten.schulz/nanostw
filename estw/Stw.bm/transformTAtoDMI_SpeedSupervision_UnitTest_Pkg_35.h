/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35_H_
#define _transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35_H_

#include "kcg_types.h"
#include "GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h"
#include "MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI */
extern void transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35(
  /* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI::to_Supervision */ DataForSupervision_nextGen_t_TrackAtlasTypes *to_Supervision,
  /* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI::eoa */ L_internal_Type_Obu_BasicTypes_Pkg eoa,
  /* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI::to_DMI */ DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg *to_DMI);

#endif /* _transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

