/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _KCG_CONSTS_H_
#define _KCG_CONSTS_H_

#include "kcg_types.h"

/* nanoSim::cMAlength */
#define cMAlength_nanoSim 35

/* nanoSim::DimTrain */
#define DimTrain_nanoSim 3

/* nanoSim::TRACKS */
#define TRACKS_nanoSim 38

/* nanoStw::GlobalEBintegrity */
#define GlobalEBintegrity_nanoStw kcg_true

/* nanoStw::emptyTrainData */
extern const TrainData_SpeedSupervision_UnitTest_Pkg emptyTrainData_nanoStw;

/* nanoStw::emptySimplePos */
extern const simpleTrainPos_nanoStw emptySimplePos_nanoStw;

/* nanoStw::invalidTid */
#define invalidTid_nanoStw nil_nanoStw

/* nanoStw::invalidLocation */
extern const TrackLocation_nanoStw invalidLocation_nanoStw;

/* nanoStw::emptyOtherPos */
extern const otherTrainPos_nanoStw emptyOtherPos_nanoStw;

/* nanoStw::nil */
#define nil_nanoStw (- 1)

/* nanoStw::dimLimitSections */
#define dimLimitSections_nanoStw 8

/* nanoStw::rlocScale */
#define rlocScale_nanoStw 255

/* nanoStw::MaxTrainLocNormRuns */
#define MaxTrainLocNormRuns_nanoStw 20

/* nanoStw::trdp::PRstr8null */
extern const array_char_10 PRstr8null_nanoStw_trdp;

/* nanoStw::trdp::PRstr16null */
extern const array_int_8 PRstr16null_nanoStw_trdp;

/* nanoStw::emptySpeedSegment */
extern const TrainSpeedSegment_nanoStw emptySpeedSegment_nanoStw;

/* nanoStw::EndOfTrackSid */
#define EndOfTrackSid_nanoStw (- 128)

/* nanoStw::FlankenOverlap */
#define FlankenOverlap_nanoStw 150

/* nanoStw::TKL_STRAIGHT */
#define TKL_STRAIGHT_nanoStw 1

/* nanoStw::TKL_BLUNT */
#define TKL_BLUNT_nanoStw 0

/* nanoStw::emptyTrackLine */
extern const TrackKnotLine_nanoStw emptyTrackLine_nanoStw;

/* nanoStw::TKL_RIGHT */
#define TKL_RIGHT_nanoStw 2

/* nanoStw::TKL_LEFT */
#define TKL_LEFT_nanoStw 3

/* nanoStw::DEFAULT_OS_MA */
#define DEFAULT_OS_MA_nanoStw (1000 * 100)

/* nanoStw::UnknownSid */
#define UnknownSid_nanoStw 0

/* nanoStw::emptyTrack */
extern const TrackKnot_nanoStw emptyTrack_nanoStw;

/* nanoStw::TKLs */
#define TKLs_nanoStw 4

/* nanoStw::trdp::emptyTrainRelLimitLocations */
extern const TrainRelLimitLocations_nanoStw_trdp emptyTrainRelLimitLocations_nanoStw_trdp;

/* SpeedSupervision_UnitTest_Pkg::NVnull */
extern const P3_NationalValues_T_Packet_Types_Pkg NVnull_SpeedSupervision_UnitTest_Pkg;

/* SpeedSupervision_UnitTest_Pkg::cOdoDiversion */
#define cOdoDiversion_SpeedSupervision_UnitTest_Pkg 2

/* SpeedSupervision_UnitTest_Pkg::cNoTrackCondition */
extern const DMI_trackCondition_T_DMI_Types_Pkg cNoTrackCondition_SpeedSupervision_UnitTest_Pkg;

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::DEFAULT_MRSP_reduction_acc */
extern const MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI DEFAULT_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI;

/* TA_Export::DEFAULT_MRSP_section */
extern const MRSP_section_t_TrackAtlasTypes DEFAULT_MRSP_section_TA_Export;

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::DEFAULT_MRSP_section */
extern const DMI_speedProfileElement_T_DMI_Types_Pkg DEFAULT_MRSP_section_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI;

/* TA_Export::END_OF_SSP */
#define END_OF_SSP_TA_Export 635

/* TA_Export::END_OF_SSP_encoding */
#define END_OF_SSP_encoding_TA_Export (- 1)

/* SpeedSupervision_UnitTest_Pkg::IVcmd_apply */
#define IVcmd_apply_SpeedSupervision_UnitTest_Pkg 1

/* SpeedSupervision_UnitTest_Pkg::IVcmd_release */
#define IVcmd_release_SpeedSupervision_UnitTest_Pkg 2

/* SpeedSupervision_UnitTest_Pkg::IVcmd_not_defined */
#define IVcmd_not_defined_SpeedSupervision_UnitTest_Pkg 0

/* SpeedSupervision_UnitTest_Pkg::DEFAULT_overlapLength */
#define DEFAULT_overlapLength_SpeedSupervision_UnitTest_Pkg 2000

/* TrackAtlasTypes::DEFAULT_MA_level_23 */
#define DEFAULT_MA_level_23_TrackAtlasTypes MA_L23_TrackAtlasTypes

/* TrackAtlasTypes::DEFAULT_Endtimer */
extern const Endtimer_t_TrackAtlasTypes DEFAULT_Endtimer_TrackAtlasTypes;

/* TrackAtlasTypes::DEFAULT_DP */
extern const DP_or_OL_t_TrackAtlasTypes DEFAULT_DP_TrackAtlasTypes;

/* TrackAtlasTypes::DEFAULT_MA_sectionlist */
extern const MovementAuthoritySectionlist_t_TrackAtlasTypes DEFAULT_MA_sectionlist_TrackAtlasTypes;

/* SDM_Commands_Pkg::cSupervisionStatus */
#define cSupervisionStatus_SDM_Commands_Pkg Undefined_Supervision_SDM_Types_Pkg

/* SDM_Commands_Pkg::cSDM_Types */
#define cSDM_Types_SDM_Commands_Pkg No_SDM_Type_SDM_Types_Pkg

/* DMI_Types_Pkg::cDMIUnknownSpeed */
#define cDMIUnknownSpeed_DMI_Types_Pkg (- 1)

/* TargetManagement_pkg::emptyMRSPSection */
extern const MRSP_internal_section_T_TargetManagement_types emptyMRSPSection_TargetManagement_pkg;

/* TargetManagement_pkg::emptyTarget */
extern const Target_real_T_TargetManagement_types emptyTarget_TargetManagement_pkg;

/* SDM_Types_Pkg::V_ebi_min */
#define V_ebi_min_SDM_Types_Pkg 3056

/* SDM_Types_Pkg::dV_ebi_min */
#define dV_ebi_min_SDM_Types_Pkg 208

/* SDM_Types_Pkg::dV_ebi_max */
#define dV_ebi_max_SDM_Types_Pkg 417

/* SDM_Types_Pkg::V_ebi_max */
#define V_ebi_max_SDM_Types_Pkg 5833

/* SDM_Types_Pkg::V_warning_min */
#define V_warning_min_SDM_Types_Pkg 3056

/* SDM_Types_Pkg::dV_warning_min */
#define dV_warning_min_SDM_Types_Pkg 111

/* SDM_Types_Pkg::dV_warning_max */
#define dV_warning_max_SDM_Types_Pkg 139

/* SDM_Types_Pkg::V_warning_max */
#define V_warning_max_SDM_Types_Pkg 3889

/* SDM_Types_Pkg::V_sbi_min */
#define V_sbi_min_SDM_Types_Pkg 3056

/* SDM_Types_Pkg::dV_sbi_min */
#define dV_sbi_min_SDM_Types_Pkg 153

/* SDM_Types_Pkg::dV_sbi_max */
#define dV_sbi_max_SDM_Types_Pkg 278

/* SDM_Types_Pkg::V_sbi_max */
#define V_sbi_max_SDM_Types_Pkg 5833

/* SDM_Types_Pkg::T_preindication */
#define T_preindication_SDM_Types_Pkg 7.0

/* TargetLimits_Pkg::cMAX_Onboard_V_Release_Iterations */
#define cMAX_Onboard_V_Release_Iterations_TargetLimits_Pkg 10

/* SDM_Types_Pkg::T_warning */
#define T_warning_SDM_Types_Pkg 2.0

/* SDM_Types_Pkg::T_driver */
#define T_driver_SDM_Types_Pkg 4.0

/* SDMConversionModelPkg::cBrakePositionGCt */
#define cBrakePositionGCt_SDMConversionModelPkg 0.16

/* SDMConversionModelPkg::cBrakePositionPCt */
#define cBrakePositionPCt_SDMConversionModelPkg 0.2

/* SDMConversionModelPkg::cBrakePositionLengthShort */
#define cBrakePositionLengthShort_SDMConversionModelPkg 900.0

/* SDMConversionModelPkg::cBrakePositionGECoeff */
extern const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionGECoeff_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePositionFreightLongECoeff */
extern const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionFreightLongECoeff_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePositionFreightLongSCoeff */
extern const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionFreightLongSCoeff_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePositionPassengerSCoeff */
extern const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionPassengerSCoeff_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePositionFreightShortSCoeff */
extern const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionFreightShortSCoeff_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePositionPECoeff */
extern const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionPECoeff_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePositionLengthMin */
#define cBrakePositionLengthMin_SDMConversionModelPkg 400.0

/* SDMConversionModelPkg::cLKrIntLookUp */
extern const LKrIntLookUp_t_SDMConversionModelPkg cLKrIntLookUp_SDMConversionModelPkg;

/* SDMConversionModelPkg::cKvIntSetLength */
#define cKvIntSetLength_SDMConversionModelPkg cNIterMax_Packet_Types_Pkg

/* SDMConversionModelPkg::cEmptyKvIntSet */
extern const nvkvintset_T_Packet_Types_Pkg cEmptyKvIntSet_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePercentServiceMax */
#define cBrakePercentServiceMax_SDMConversionModelPkg 135

/* SDMConversionModelPkg::cBrakePercentALookup */
extern const array_int_6_221 cBrakePercentALookup_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePercentV_lim */
extern const array_int_4 cBrakePercentV_lim_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePercentSpeedLookup */
extern const array_int_221 cBrakePercentSpeedLookup_SDMConversionModelPkg;

/* SDMConversionModelPkg::cBrakePercentSteps */
#define cBrakePercentSteps_SDMConversionModelPkg (cBrakePercentStepsMax_SDMConversionModelPkg - cBrakePercentStepsMin_SDMConversionModelPkg + 1)

/* SDMConversionModelPkg::cBrakePercentStepsMax */
#define cBrakePercentStepsMax_SDMConversionModelPkg 250

/* SDMConversionModelPkg::cBrakePercentStepsMin */
#define cBrakePercentStepsMin_SDMConversionModelPkg 30

/* SDMConversionModelPkg::cKvMergedLength */
#define cKvMergedLength_SDMConversionModelPkg (cBrakePercentSpeedSteps_SDMConversionModelPkg + cKvIntLength_SDMConversionModelPkg)

/* SDMConversionModelPkg::cKvIntLength */
#define cKvIntLength_SDMConversionModelPkg cNIterMax_Packet_Types_Pkg

/* SDMConversionModelPkg::cBrakePercentSpeedSteps */
#define cBrakePercentSpeedSteps_SDMConversionModelPkg 6

/* SDM_Types_Pkg::A_gravity */
#define A_gravity_SDM_Types_Pkg 981

/* SDM_Types_Pkg::M_rotating_max */
#define M_rotating_max_SDM_Types_Pkg 15

/* SDM_Types_Pkg::M_rotating_min */
#define M_rotating_min_SDM_Types_Pkg 2

/* CalcBrakingCurves_types::cMAX_BC_ARCS */
#define cMAX_BC_ARCS_CalcBrakingCurves_types (cMAX_DISTANCE_STEPS_CalcBrakingCurves_types + cMAX_SPEED_VALUE_STEP_CalcBrakingCurves_types)

/* CalcBrakingCurves_types::cMAX_DISTANCE_STEPS */
#define cMAX_DISTANCE_STEPS_CalcBrakingCurves_types GradientMaxSectionsTrainlengthCompensated_SDM_GradientAcceleration_types

/* SDM_GradientAcceleration_types::GradientMaxSectionsTrainlengthCompensated */
#define GradientMaxSectionsTrainlengthCompensated_SDM_GradientAcceleration_types (GradientMaxSections_TrackAtlasTypes * 2)

/* CalcBrakingCurves_types::cMAX_SPEED_VALUE_STEP */
#define cMAX_SPEED_VALUE_STEP_CalcBrakingCurves_types (cNIterMax_Packet_Types_Pkg + 6 + 1)

/* SDM_Types_Pkg::_cMinBaliseAccuracy */
#define _cMinBaliseAccuracy_SDM_Types_Pkg 100

/* CalculateTrainPosition_Pkg::cNoPositionedBG */
extern const positionedBG_T_TrainPosition_Types_Pck cNoPositionedBG_CalculateTrainPosition_Pkg;

/* BG_Types_Pkg::cMaxNoOfLinkedBGs */
#define cMaxNoOfLinkedBGs_BG_Types_Pkg 33

/* Packet_TrainTypes_Pkg::cMaxNationalSystem */
#define cMaxNationalSystem_Packet_TrainTypes_Pkg 5

/* Packet_TrainTypes_Pkg::cMaxTractionIdentity */
#define cMaxTractionIdentity_Packet_TrainTypes_Pkg 4

/* Packet_Types_Pkg::cNIterMax */
#define cNIterMax_Packet_Types_Pkg 7

/* TrackAtlasTypes::DEFAULT_GradientProfile */
extern const GradientProfile_t_TrackAtlasTypes DEFAULT_GradientProfile_TrackAtlasTypes;

/* TrackAtlasTypes::MAsMaxSections */
#define MAsMaxSections_TrackAtlasTypes 2

/* TrackAtlasTypes::MRSPMaxSections */
#define MRSPMaxSections_TrackAtlasTypes 35

/* TrackAtlasTypes::GradientMaxSections */
#define GradientMaxSections_TrackAtlasTypes DIM_GP_TrackAtlasTypes

/* TrackAtlasTypes::DIM_GP */
#define DIM_GP_TrackAtlasTypes 2

/* nanoStw::trdp::PRstrlen */
#define PRstrlen_nanoStw_trdp 10

/* DMI_Types_Pkg::cDMI_maxSpeedProfile */
#define cDMI_maxSpeedProfile_DMI_Types_Pkg 32

/* DMI_Types_Pkg::cDMI_maxGradientProfile */
#define cDMI_maxGradientProfile_DMI_Types_Pkg 32

/* DMI_Types_Pkg::cDMI_maxTrackCondProfile */
#define cDMI_maxTrackCondProfile_DMI_Types_Pkg 32

#endif /* _KCG_CONSTS_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** kcg_consts.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

