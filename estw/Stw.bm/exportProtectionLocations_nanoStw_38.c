/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "exportProtectionLocations_nanoStw_38.h"

/* nanoStw::exportProtectionLocations */
void exportProtectionLocations_nanoStw_38(
  /* nanoStw::exportProtectionLocations::Tlocs */ ProtectionLocations_SpeedSupervision_UnitTest_Pkg *Tlocs,
  /* nanoStw::exportProtectionLocations::train */ simpleTrainPos_nanoStw *train,
  /* nanoStw::exportProtectionLocations::EBintegrity */ kcg_bool EBintegrity,
  /* nanoStw::exportProtectionLocations::tracks */ array_21324 *tracks,
  /* nanoStw::exportProtectionLocations::out */ ProtectionLocations_nanoStw_trdp *out)
{
  /* nanoStw::exportProtectionLocations */ TrackLocation_nanoStw acc1;
  /* nanoStw::exportProtectionLocations */ kcg_int tmp;
  /* nanoStw::exportProtectionLocations */ TrackLocation_nanoStw acc;
  /* nanoStw::exportProtectionLocations */ kcg_bool cond_iterw;
  kcg_int i;
  /* nanoStw::normalizeTrackLocation::enable */ kcg_bool enable_3;
  /* nanoStw::exportProtectionLocations::_L71 */ TrackLocation_nanoStw _L71;
  /* nanoStw::exportProtectionLocations::_L86 */ kcg_int _L86;
  
  (*out).tid = (*train).TrainId;
  (*out).velocity = (*train).velocity;
  (*out).acc = (*train).acc;
  kcg_copy_TrackLocation_nanoStw(&(*out).antenna, &(*train).antenna);
  (*out).sizeTrainType8 = PRstrlen_nanoStw_trdp;
  kcg_copy_array_char_10(
    &(*out).trainType8,
    (array_char_10 *) &PRstr8null_nanoStw_trdp);
  kcg_copy_array_int_8(
    &(*out).trainType16,
    (array_int_8 *) &PRstr16null_nanoStw_trdp);
  /* ck__L81 */ if ((*train).flag.active) {
    /* 1 */
    projectLocation_nanoStw(
      (*Tlocs).ProtectedFront,
      &(*train).frontEnd,
      (*train).flag.CW,
      &_L71);
  }
  else {
    kcg_copy_TrackLocation_nanoStw(&_L71, &(*train).frontEnd);
  }
  _L86 = - (*train).length;
  enable_3 = (*train).BrPc > 0;
  kcg_copy_TrackLocation_nanoStw(&(*out).protectedFrontEnd, &_L71);
  /* 3_2 */ if (enable_3) {
    /* 3_2 */ for (i = 0; i < 20; i++) {
      kcg_copy_TrackLocation_nanoStw(&acc1, &(*out).protectedFrontEnd);
      /* 3_1 */
      normalizeTrackLocation_it_nanoStw_38(
        &acc1,
        tracks,
        &cond_iterw,
        &(*out).protectedFrontEnd);
      /* 3_2 */ if (!cond_iterw) {
        break;
      }
    }
  }
  /* 1 */ if (EBintegrity) {
    (*out).protectedRearEnd.sid = _L71.sid;
    (*out).protectedRearEnd.location = _L86 + _L71.location;
    /* 1 */ if ((*train).flag.CW != (_L71.sid < 0)) {
      tmp = - _L86;
    }
    else {
      tmp = _L86;
    }
    (*out).protectedRearEnd.distance = tmp + _L71.distance;
  }
  else {
    kcg_copy_TrackLocation_nanoStw(&(*out).protectedRearEnd, &(*train).rearEnd);
  }
  /* 2_2 */ if (enable_3) {
    /* 2_2 */ for (i = 0; i < 20; i++) {
      kcg_copy_TrackLocation_nanoStw(&acc, &(*out).protectedRearEnd);
      /* 2_1 */
      normalizeTrackLocation_it_nanoStw_38(
        &acc,
        tracks,
        &cond_iterw,
        &(*out).protectedRearEnd);
      /* 2_2 */ if (!cond_iterw) {
        break;
      }
    }
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** exportProtectionLocations_nanoStw_38.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

