/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapEStwSections_SpeedSupervision_UnitTest_Pkg.h"

/* SpeedSupervision_UnitTest_Pkg::mapEStwSections */
void mapEStwSections_SpeedSupervision_UnitTest_Pkg(
  /* SpeedSupervision_UnitTest_Pkg::mapEStwSections::sect */ TrackSection_SpeedSupervision_UnitTest_Pkg *sect,
  /* SpeedSupervision_UnitTest_Pkg::mapEStwSections::Profile */ MRSP_section_t_TrackAtlasTypes *Profile)
{
  (*Profile).Loc_Abs = (*sect).length;
  (*Profile).Loc_LRBG = (*Profile).Loc_Abs;
  (*Profile).valid = 0 != (*sect).speed;
  (*Profile).MRS = (*sect).speed;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** mapEStwSections_SpeedSupervision_UnitTest_Pkg.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

