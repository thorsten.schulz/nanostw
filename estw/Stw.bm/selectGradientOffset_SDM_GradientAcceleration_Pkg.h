/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _selectGradientOffset_SDM_GradientAcceleration_Pkg_H_
#define _selectGradientOffset_SDM_GradientAcceleration_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDM_GradientAcceleration_Pkg::selectGradientOffset */
extern void selectGradientOffset_SDM_GradientAcceleration_Pkg(
  /* SDM_GradientAcceleration_Pkg::selectGradientOffset::GradientProfile */ GradientProfile_real_t_SDM_GradientAcceleration_types *GradientProfile,
  /* SDM_GradientAcceleration_Pkg::selectGradientOffset::Index */ kcg_int Index,
  /* SDM_GradientAcceleration_Pkg::selectGradientOffset::Offset */ kcg_int Offset,
  /* SDM_GradientAcceleration_Pkg::selectGradientOffset::GradientSect */ Gradient_section_real_t_SDM_GradientAcceleration_types *GradientSect);

#endif /* _selectGradientOffset_SDM_GradientAcceleration_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** selectGradientOffset_SDM_GradientAcceleration_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

