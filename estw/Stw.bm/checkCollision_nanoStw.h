/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _checkCollision_nanoStw_H_
#define _checkCollision_nanoStw_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::checkCollision */
extern void checkCollision_nanoStw(
  /* nanoStw::checkCollision::own */ TrackLocation_nanoStw *own,
  /* nanoStw::checkCollision::runsCW */ kcg_bool runsCW,
  /* nanoStw::checkCollision::otherTrain */ otherTrainPos_nanoStw *otherTrain,
  /* nanoStw::checkCollision::nloc */ TrackLocation_nanoStw *nloc);

#endif /* _checkCollision_nanoStw_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** checkCollision_nanoStw.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

