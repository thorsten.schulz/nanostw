/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _onBoardStw_configured_nanoSim_H_
#define _onBoardStw_configured_nanoSim_H_

#include "kcg_types.h"
#include "exportTrainEstate_nanoStw_35_38.h"
#include "exportProtectionLocations_nanoStw_38.h"
#include "exportTrackSide_nanoStw_35.h"
#include "makeTS_nanoStw_35_3_38.h"
#include "importTAdata_nanoStw_3_38.h"
#include "nanoSDM_SpeedSupervision_UnitTest_Pkg_35.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -------------------- initialization variables  ------------------ */
  kcg_bool init;
  /* ----------------------- local memories  ------------------------- */
  kcg_bool /* nanoStw::onBoardStw::isActive */ rem_isActive_1;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35 /* 1_1 */ _1_Context_1_1;
  outC_importTAdata_nanoStw_3_38 /* 1_1 */ Context_1_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_onBoardStw_configured_nanoSim;

/* ===========  node initialization and cycle functions  =========== */
/* nanoSim::onBoardStw_configured */
extern void onBoardStw_configured_nanoSim(
  /* nanoSim::onBoardStw_configured::XPR */ allPR_nanoSim *XPR,
  /* nanoSim::onBoardStw_configured::XTracks */ TrackPlan_nanoSim *XTracks,
  /* nanoSim::onBoardStw_configured::XReport */ TrainReport_nanoStw_trdp *XReport,
  /* nanoSim::onBoardStw_configured::correctionPoint */ TrackLocation_nanoStw *correctionPoint,
  /* nanoSim::onBoardStw_configured::haveData */ kcg_bool *haveData,
  /* nanoSim::onBoardStw_configured::XTrainRelLimitLoc */ TrackDMI_nanoSim *XTrainRelLimitLoc,
  /* nanoSim::onBoardStw_configured::PRtoOther */ ProtectionLocations_nanoStw_trdp *PRtoOther,
  /* nanoSim::onBoardStw_configured::InterventionCmd */ InterventionCommand_SpeedSupervision_UnitTest_Pkg *InterventionCmd,
  /* nanoSim::onBoardStw_configured::pkgDMI */ SDM_DMI_wrapper_T_SDM_Types_Pkg *pkgDMI,
  outC_onBoardStw_configured_nanoSim *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void onBoardStw_configured_reset_nanoSim(
  outC_onBoardStw_configured_nanoSim *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void onBoardStw_configured_init_nanoSim(
  outC_onBoardStw_configured_nanoSim *outC);
#endif /* KCG_USER_DEFINED_INIT */

#endif /* _onBoardStw_configured_nanoSim_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** onBoardStw_configured_nanoSim.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

