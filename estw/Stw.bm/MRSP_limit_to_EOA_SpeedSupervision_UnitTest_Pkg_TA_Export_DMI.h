/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_
#define _MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA */
extern void MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::i */ kcg_int i,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::MRSP */ DMI_SpeedProfileArray_T_DMI_Types_Pkg *MRSP,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::limited */ DMI_speedProfileElement_T_DMI_Types_Pkg *limited);

#endif /* _MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

