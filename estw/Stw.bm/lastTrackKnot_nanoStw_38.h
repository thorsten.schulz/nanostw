/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _lastTrackKnot_nanoStw_38_H_
#define _lastTrackKnot_nanoStw_38_H_

#include "kcg_types.h"
#include "selectTrackBySid_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::lastTrackKnot */
extern void lastTrackKnot_nanoStw_38(
  /* nanoStw::lastTrackKnot::track */ TrackKnot_nanoStw *track,
  /* nanoStw::lastTrackKnot::runsCW */ kcg_bool runsCW,
  /* nanoStw::lastTrackKnot::tracks */ array_21324 *tracks,
  /* nanoStw::lastTrackKnot::ptrack */ TrackKnot_nanoStw *ptrack,
  /* nanoStw::lastTrackKnot::isOn */ kcg_bool *isOn,
  /* nanoStw::lastTrackKnot::isLastTracksLineBlunt */ kcg_bool *isLastTracksLineBlunt,
  /* nanoStw::lastTrackKnot::isBackwardsGoingLineBlunt */ kcg_bool *isBackwardsGoingLineBlunt);

#endif /* _lastTrackKnot_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** lastTrackKnot_nanoStw_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

