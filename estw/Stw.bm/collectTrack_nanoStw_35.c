/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "collectTrack_nanoStw_35.h"

/* nanoStw::collectTrack */
void collectTrack_nanoStw_35(
  /* nanoStw::collectTrack::i */ kcg_int i1,
  /* nanoStw::collectTrack::sections */ array_21021 *sections,
  /* nanoStw::collectTrack::locs */ ProtectionLocations_SpeedSupervision_UnitTest_Pkg *locs,
  /* nanoStw::collectTrack::runsCW */ kcg_bool runsCW,
  /* nanoStw::collectTrack::nonMerged */ TrainRelLimitLocations_nanoStw_trdp *nonMerged)
{
  /* nanoStw::collectTrack */ array_int_8 tmp;
  kcg_int i;
  /* nanoStw::collectTrack::IfBlock1::then::_L22 */ array_int_8 _L22_IfBlock1;
  /* nanoStw::collectTrack::first */ TrackSection_nanoStw first;
  /* nanoStw::collectTrack::_L27 */ kcg_bool _L27;
  
  /* 1 */ selectSectionByASId_nanoStw_35(1 + i1, sections, &first, &_L27);
  /* ck__L27 */ if (_L27) {
    tmp[0] = (*locs).Rear;
    tmp[1] = (*locs).Front;
    tmp[2] = (*locs).PI;
    tmp[3] = (*locs).Indication;
    tmp[4] = (*locs).Permitted;
    tmp[5] = (*locs).FLOI;
    tmp[6] = (*locs).Target;
    tmp[7] = (*locs).ProtectedFront;
    /* 3 */ for (i = 0; i < 8; i++) {
      _L22_IfBlock1[i] = /* 2 */
        locateWithinSection_nanoStw(&first, tmp[i], runsCW);
    }
    (*nonMerged).Rear = _L22_IfBlock1[0];
    (*nonMerged).Front = _L22_IfBlock1[1];
    (*nonMerged).PI = _L22_IfBlock1[2];
    (*nonMerged).Indication = _L22_IfBlock1[3];
    (*nonMerged).Permitted = _L22_IfBlock1[4];
    (*nonMerged).FLOI = _L22_IfBlock1[5];
    (*nonMerged).Target = _L22_IfBlock1[6];
    (*nonMerged).ProtectedFront = _L22_IfBlock1[7];
  }
  else {
    kcg_copy_TrainRelLimitLocations_nanoStw_trdp(
      nonMerged,
      (TrainRelLimitLocations_nanoStw_trdp *)
        &emptyTrainRelLimitLocations_nanoStw_trdp);
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** collectTrack_nanoStw_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

