/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _fakeOdometricTrainPosition_SDM_Types_Pkg_H_
#define _fakeOdometricTrainPosition_SDM_Types_Pkg_H_

#include "kcg_types.h"
#include "SnapLRBG_SDM_Types_Pkg.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* ---------------------------  outputs  --------------------------- */
  trainPosition_T_TrainPosition_Types_Pck /* SDM_Types_Pkg::fakeOdometricTrainPosition::tpos */ tpos;
  /* -----------------------  no local probes  ----------------------- */
  /* -------------------- initialization variables  ------------------ */
  kcg_bool init;
  /* ----------------------- local memories  ------------------------- */
  positionedBG_T_TrainPosition_Types_Pck /* SDM_Types_Pkg::fakeOdometricTrainPosition::_L134 */ _L134;
  positionedBG_T_TrainPosition_Types_Pck /* SDM_Types_Pkg::fakeOdometricTrainPosition::_L131 */ _L131;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_SnapLRBG_SDM_Types_Pkg /* 1 */ Context_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_fakeOdometricTrainPosition_SDM_Types_Pkg;

/* ===========  node initialization and cycle functions  =========== */
/* SDM_Types_Pkg::fakeOdometricTrainPosition */
extern void fakeOdometricTrainPosition_SDM_Types_Pkg(
  /* SDM_Types_Pkg::fakeOdometricTrainPosition::odo */ odometry_T_Obu_BasicTypes_Pkg *odo,
  /* SDM_Types_Pkg::fakeOdometricTrainPosition::trainData */ trainData_T_TIU_Types_Pkg *trainData,
  outC_fakeOdometricTrainPosition_SDM_Types_Pkg *outC);

extern void fakeOdometricTrainPosition_reset_SDM_Types_Pkg(
  outC_fakeOdometricTrainPosition_SDM_Types_Pkg *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void fakeOdometricTrainPosition_init_SDM_Types_Pkg(
  outC_fakeOdometricTrainPosition_SDM_Types_Pkg *outC);
#endif /* KCG_USER_DEFINED_INIT */

#endif /* _fakeOdometricTrainPosition_SDM_Types_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** fakeOdometricTrainPosition_SDM_Types_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

