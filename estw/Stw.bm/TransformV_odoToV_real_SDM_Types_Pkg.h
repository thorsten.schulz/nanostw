/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _TransformV_odoToV_real_SDM_Types_Pkg_H_
#define _TransformV_odoToV_real_SDM_Types_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDM_Types_Pkg::TransformV_odoToV_real */
extern V_internal_real_Type_SDM_Types_Pkg TransformV_odoToV_real_SDM_Types_Pkg(
  /* SDM_Types_Pkg::TransformV_odoToV_real::v_odo */ V_odometry_Type_Obu_BasicTypes_Pkg v_odo);

#endif /* _TransformV_odoToV_real_SDM_Types_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** TransformV_odoToV_real_SDM_Types_Pkg.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

