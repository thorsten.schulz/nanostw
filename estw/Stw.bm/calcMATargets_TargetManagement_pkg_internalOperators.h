/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _calcMATargets_TargetManagement_pkg_internalOperators_H_
#define _calcMATargets_TargetManagement_pkg_internalOperators_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* TargetManagement_pkg::internalOperators::calcMATargets */
extern void calcMATargets_TargetManagement_pkg_internalOperators(
  /* TargetManagement_pkg::internalOperators::calcMATargets::MA_section */ MA_section_real_T_TargetManagement_types *MA_section,
  /* TargetManagement_pkg::internalOperators::calcMATargets::EOA_Target */ Target_real_T_TargetManagement_types *EOA_Target,
  /* TargetManagement_pkg::internalOperators::calcMATargets::SvL_LoA_Target */ Target_real_T_TargetManagement_types *SvL_LoA_Target);

#endif /* _calcMATargets_TargetManagement_pkg_internalOperators_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** calcMATargets_TargetManagement_pkg_internalOperators.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

