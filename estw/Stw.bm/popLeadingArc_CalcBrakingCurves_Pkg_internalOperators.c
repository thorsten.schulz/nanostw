/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "popLeadingArc_CalcBrakingCurves_Pkg_internalOperators.h"

/* CalcBrakingCurves_Pkg::internalOperators::popLeadingArc */
void popLeadingArc_CalcBrakingCurves_Pkg_internalOperators(
  /* CalcBrakingCurves_Pkg::internalOperators::popLeadingArc::oldBC */ ParabolaCurve_T_CalcBrakingCurves_types *oldBC,
  /* CalcBrakingCurves_Pkg::internalOperators::popLeadingArc::newBC */ ParabolaCurve_T_CalcBrakingCurves_types *newBC)
{
  kcg_copy_array_real_17(
    &(*newBC).distances[0],
    (array_real_17 *) &(*oldBC).distances[1]);
  (&(*newBC).distances[17])[0] = 0.0;
  kcg_copy_array_real_17(
    &(*newBC).speeds[0],
    (array_real_17 *) &(*oldBC).speeds[1]);
  (&(*newBC).speeds[17])[0] = 0.0;
  kcg_copy_array_real_17(
    &(*newBC).accelerations[0],
    (array_real_17 *) &(*oldBC).accelerations[1]);
  (&(*newBC).accelerations[17])[0] = 0.0;
  kcg_copy_array_bool_17(
    &(*newBC).valid[0],
    (array_bool_17 *) &(*oldBC).valid[1]);
  (&(*newBC).valid[17])[0] = kcg_false;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** popLeadingArc_CalcBrakingCurves_Pkg_internalOperators.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

