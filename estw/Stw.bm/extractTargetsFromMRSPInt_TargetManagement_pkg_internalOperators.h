/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _extractTargetsFromMRSPInt_TargetManagement_pkg_internalOperators_H_
#define _extractTargetsFromMRSPInt_TargetManagement_pkg_internalOperators_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* TargetManagement_pkg::internalOperators::extractTargetsFromMRSPInt */
extern void extractTargetsFromMRSPInt_TargetManagement_pkg_internalOperators(
  /* TargetManagement_pkg::internalOperators::extractTargetsFromMRSPInt::index */ kcg_int index,
  /* TargetManagement_pkg::internalOperators::extractTargetsFromMRSPInt::ACCU */ extractTargetsMRSPACC_TargetManagement_pkg *ACCU,
  /* TargetManagement_pkg::internalOperators::extractTargetsFromMRSPInt::MRSP */ MRSP_internal_T_TargetManagement_types *MRSP,
  /* TargetManagement_pkg::internalOperators::extractTargetsFromMRSPInt::endCondition */ kcg_bool *endCondition,
  /* TargetManagement_pkg::internalOperators::extractTargetsFromMRSPInt::ACCUout */ extractTargetsMRSPACC_TargetManagement_pkg *ACCUout);

#endif /* _extractTargetsFromMRSPInt_TargetManagement_pkg_internalOperators_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** extractTargetsFromMRSPInt_TargetManagement_pkg_internalOperators.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

