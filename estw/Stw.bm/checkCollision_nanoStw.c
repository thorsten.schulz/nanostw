/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "checkCollision_nanoStw.h"

/* nanoStw::checkCollision */
void checkCollision_nanoStw(
  /* nanoStw::checkCollision::own */ TrackLocation_nanoStw *own,
  /* nanoStw::checkCollision::runsCW */ kcg_bool runsCW,
  /* nanoStw::checkCollision::otherTrain */ otherTrainPos_nanoStw *otherTrain,
  /* nanoStw::checkCollision::nloc */ TrackLocation_nanoStw *nloc)
{
  /* nanoStw::checkCollision */ LengthUnit tmp3;
  /* nanoStw::checkCollision */ LengthUnit tmp2;
  /* nanoStw::checkCollision */ LengthUnit tmp1;
  /* nanoStw::checkCollision */ LengthUnit tmp;
  /* nanoStw::checkCollision::IfBlock1::else::overlapA */ LengthUnit overlapA_IfBlock1;
  /* nanoStw::checkCollision::IfBlock1::else::overlapB */ LengthUnit overlapB_IfBlock1;
  /* nanoStw::checkCollision::IfBlock1::else::IfBlock2 */ kcg_bool IfBlock2_clock_IfBlock1;
  /* nanoStw::checkCollision::IfBlock1::else::IfBlock2::else */ kcg_bool else_clock_IfBlock1_IfBlock2;
  /* nanoStw::checkCollision::IfBlock1 */ kcg_bool IfBlock1_clock;
  /* nanoStw::checkCollision::nonFlip */ kcg_bool nonFlip;
  
  IfBlock1_clock = ((*own).sid == UnknownSid_nanoStw) |
    (((*otherTrain).protectedLocationFront.sid != (*own).sid) &
      ((*otherTrain).protectedLocationRear.sid != (*own).sid));
  nonFlip = runsCW == ((*own).sid < 0);
  /* ck_IfBlock1 */ if (IfBlock1_clock) {
    kcg_copy_TrackLocation_nanoStw(nloc, own);
  }
  else {
    /* 6 */ if (nonFlip) {
      tmp3 = (*own).distance;
      tmp2 = (*otherTrain).protectedLocationFront.distance;
      tmp1 = (*own).distance;
      tmp = (*otherTrain).protectedLocationRear.distance;
    }
    else {
      tmp3 = (*otherTrain).protectedLocationFront.distance;
      tmp2 = (*own).distance;
      tmp1 = (*otherTrain).protectedLocationRear.distance;
      tmp = (*own).distance;
    }
    overlapA_IfBlock1 = tmp3 - tmp2;
    overlapB_IfBlock1 = tmp1 - tmp;
    IfBlock2_clock_IfBlock1 = ((*otherTrain).protectedLocationFront.sid ==
        (*own).sid) & (((*otherTrain).protectedLocationRear.sid != (*own).sid) |
        (overlapA_IfBlock1 > overlapB_IfBlock1)) & (overlapA_IfBlock1 >= 0);
    /* ck_IfBlock2 */ if (IfBlock2_clock_IfBlock1) {
      kcg_copy_TrackLocation_nanoStw(
        nloc,
        &(*otherTrain).protectedLocationFront);
      (*nloc).location = (*own).location - overlapA_IfBlock1;
    }
    else {
      else_clock_IfBlock1_IfBlock2 =
        (((*otherTrain).protectedLocationFront.sid != (*own).sid) |
          (overlapB_IfBlock1 > overlapA_IfBlock1)) &
        ((*otherTrain).protectedLocationRear.sid == (*own).sid) &
        (overlapB_IfBlock1 >= 0);
      /* ck_anon_activ */ if (else_clock_IfBlock1_IfBlock2) {
        kcg_copy_TrackLocation_nanoStw(
          nloc,
          &(*otherTrain).protectedLocationRear);
        (*nloc).location = (*own).location - overlapB_IfBlock1;
      }
      else {
        kcg_copy_TrackLocation_nanoStw(nloc, own);
      }
    }
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** checkCollision_nanoStw.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

