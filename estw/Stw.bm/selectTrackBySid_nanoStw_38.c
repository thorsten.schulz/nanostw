/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "selectTrackBySid_nanoStw_38.h"

/* nanoStw::selectTrackBySid */
void selectTrackBySid_nanoStw_38(
  /* nanoStw::selectTrackBySid::sid */ TrackId_nanoStw sid,
  /* nanoStw::selectTrackBySid::tracks */ array_21324 *tracks,
  /* nanoStw::selectTrackBySid::track */ TrackKnot_nanoStw *track,
  /* nanoStw::selectTrackBySid::found */ kcg_bool *found)
{
  /* nanoStw::selectTrackBySid */ TrackId_nanoStw acc;
  kcg_int i;
  /* nanoStw::selectTrackBySid::_L8 */ kcg_int _L8;
  /* nanoStw::selectTrackBySid::_L9 */ kcg_bool _L9;
  
  _L9 = UnknownSid_nanoStw != sid;
  /* 1 */ if (_L9) {
    /* 1 */ for (i = 0; i < 38; i++) {
      acc = sid;
      _L8 = i + 1;
      /* 1 */ if (!(acc != (*tracks)[i].sid)) {
        break;
      }
    }
    if ((0 <= _L8 - 1) & (_L8 - 1 < 38)) {
      kcg_copy_TrackKnot_nanoStw(track, &(*tracks)[_L8 - 1]);
    }
    else {
      kcg_copy_TrackKnot_nanoStw(
        track,
        (TrackKnot_nanoStw *) &emptyTrack_nanoStw);
    }
  }
  else {
    kcg_copy_TrackKnot_nanoStw(
      track,
      (TrackKnot_nanoStw *) &emptyTrack_nanoStw);
  }
  *found = _L9 & ((*track).sid == sid);
}

/* $**************** KCG Version 6.4 (build i21) ****************
** selectTrackBySid_nanoStw_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

