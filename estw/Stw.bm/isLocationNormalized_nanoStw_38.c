/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "isLocationNormalized_nanoStw_38.h"

/* nanoStw::isLocationNormalized */
kcg_bool isLocationNormalized_nanoStw_38(
  /* nanoStw::isLocationNormalized::loc */ TrackLocation_nanoStw *loc,
  /* nanoStw::isLocationNormalized::tracks */ array_21324 *tracks)
{
  /* nanoStw::isLocationNormalized::_L7 */ TrackKnot_nanoStw _L7;
  /* nanoStw::isLocationNormalized::_L8 */ kcg_bool _L8;
  /* nanoStw::isLocationNormalized::normalized */ kcg_bool normalized;
  
  /* 1 */ selectTrackBySid_nanoStw_38((*loc).sid, tracks, &_L7, &_L8);
  normalized = (_L7.l >= (*loc).distance) & ((*loc).distance >= 0) & _L8 &
    ((*loc).location >= 0);
  return normalized;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** isLocationNormalized_nanoStw_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

