/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_
#define _GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_

#include "kcg_types.h"
#include "GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI */
extern void GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::fresh */ kcg_bool fresh,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::Gradient_Profile_in */ GradientProfile_t_TrackAtlasTypes *Gradient_Profile_in,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::Gradient_Profile_for_DMI_out */ DMI_gradientProfile_T_DMI_Types_Pkg *Gradient_Profile_for_DMI_out);

#endif /* _GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

