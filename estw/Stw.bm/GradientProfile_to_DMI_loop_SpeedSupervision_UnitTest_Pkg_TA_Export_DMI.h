/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_
#define _GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop */
extern void GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::Gradient_section_in */ Gradient_section_t_TrackAtlasTypes *Gradient_section_in,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::Gradient_section_for_DMI_out */ GradientProfile_for_DMI_section_t_TrackAtlasTypes *Gradient_section_for_DMI_out);

#endif /* _GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

