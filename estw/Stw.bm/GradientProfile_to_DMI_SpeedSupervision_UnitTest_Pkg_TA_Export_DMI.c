/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h"

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI */
void GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::fresh */ kcg_bool fresh,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::Gradient_Profile_in */ GradientProfile_t_TrackAtlasTypes *Gradient_Profile_in,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI::Gradient_Profile_for_DMI_out */ DMI_gradientProfile_T_DMI_Types_Pkg *Gradient_Profile_for_DMI_out)
{
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI */ DMI_gradientProfileArray_T_DMI_Types_Pkg tmp1;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI */ array_21578 tmp;
  kcg_int i;
  
  (*Gradient_Profile_for_DMI_out).profileChanged = fresh;
  kcg_copy_GradientProfile_t_TrackAtlasTypes(
    &tmp[0],
    (GradientProfile_t_TrackAtlasTypes *) &(*Gradient_Profile_in)[0]);
  for (i = 0; i < 30; i++) {
    kcg_copy_Gradient_section_t_TrackAtlasTypes(
      &(&tmp[2])[i],
      &(*Gradient_Profile_in)[1]);
  }
  /* 1 */ for (i = 0; i < 32; i++) {
    /* 1 */
    GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
      &tmp[i],
      EoA,
      &tmp1[i]);
  }
  kcg_copy_DMI_gradientProfileArray_T_DMI_Types_Pkg(
    &(*Gradient_Profile_for_DMI_out).gradientProfiles,
    &tmp1);
}

/* $**************** KCG Version 6.4 (build i21) ****************
** GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

