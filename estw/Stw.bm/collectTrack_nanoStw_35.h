/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _collectTrack_nanoStw_35_H_
#define _collectTrack_nanoStw_35_H_

#include "kcg_types.h"
#include "locateWithinSection_nanoStw.h"
#include "selectSectionByASId_nanoStw_35.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::collectTrack */
extern void collectTrack_nanoStw_35(
  /* nanoStw::collectTrack::i */ kcg_int i1,
  /* nanoStw::collectTrack::sections */ array_21021 *sections,
  /* nanoStw::collectTrack::locs */ ProtectionLocations_SpeedSupervision_UnitTest_Pkg *locs,
  /* nanoStw::collectTrack::runsCW */ kcg_bool runsCW,
  /* nanoStw::collectTrack::nonMerged */ TrainRelLimitLocations_nanoStw_trdp *nonMerged);

#endif /* _collectTrack_nanoStw_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** collectTrack_nanoStw_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

