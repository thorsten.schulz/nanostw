/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _DefaultNationalValues_SpeedSupervision_UnitTest_Pkg_H_
#define _DefaultNationalValues_SpeedSupervision_UnitTest_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::DefaultNationalValues */
extern void DefaultNationalValues_SpeedSupervision_UnitTest_Pkg(
  /* SpeedSupervision_UnitTest_Pkg::DefaultNationalValues::__ */ P3_NationalValues_T_Packet_Types_Pkg *__);

#endif /* _DefaultNationalValues_SpeedSupervision_UnitTest_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** DefaultNationalValues_SpeedSupervision_UnitTest_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

