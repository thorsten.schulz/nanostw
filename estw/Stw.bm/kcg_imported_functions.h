/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _KCG_IMPORTED_FUNCTIONS_H_
#define _KCG_IMPORTED_FUNCTIONS_H_

#include "kcg_types.h"

#ifndef SqrtR_mathext
/* mathext::SqrtR */
extern kcg_real SqrtR_mathext(/* mathext::SqrtR::Input1 */ kcg_real Input1);
#endif /* SqrtR_mathext */

#endif /* _KCG_IMPORTED_FUNCTIONS_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** kcg_imported_functions.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

