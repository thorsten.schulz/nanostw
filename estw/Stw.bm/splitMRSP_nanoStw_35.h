/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _splitMRSP_nanoStw_35_H_
#define _splitMRSP_nanoStw_35_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::splitMRSP */
extern void splitMRSP_nanoStw_35(
  /* nanoStw::splitMRSP::i */ kcg_int i,
  /* nanoStw::splitMRSP::TS_MRSP */ array_21021 *TS_MRSP,
  /* nanoStw::splitMRSP::TSS */ TrainSpeedSegment_nanoStw *TSS,
  /* nanoStw::splitMRSP::frontSectionOffset */ kcg_int frontSectionOffset,
  /* nanoStw::splitMRSP::section */ TrackSection_SpeedSupervision_UnitTest_Pkg *section,
  /* nanoStw::splitMRSP::sid */ TrackId_nanoStw *sid);

#endif /* _splitMRSP_nanoStw_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** splitMRSP_nanoStw_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

