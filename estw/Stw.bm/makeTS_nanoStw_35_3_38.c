/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "makeTS_nanoStw_35_3_38.h"

/* nanoStw::makeTS */
void makeTS_nanoStw_35_3_38(
  /* nanoStw::makeTS::thisTrain */ simpleTrainPos_nanoStw *thisTrain,
  /* nanoStw::makeTS::trains */ array_21564 *trains,
  /* nanoStw::makeTS::tracks */ array_21324 *tracks,
  /* nanoStw::makeTS::MA */ TrackAtlasData_SpeedSupervision_UnitTest_Pkg *MA,
  /* nanoStw::makeTS::sections */ array_21021 *sections)
{
  /* nanoStw::makeTS */ TrackLocation_nanoStw acc;
  kcg_int i;
  /* nanoStw::makeTS::_L77 */ TrackLocation_nanoStw _L77;
  
  (*MA).vRelease = (*thisTrain).vRelease;
  (*MA).fresh = kcg_true;
  kcg_copy_TrackLocation_nanoStw(&_L77, &(*thisTrain).rearEnd);
  /* 1 */ for (i = 0; i < 35; i++) {
    kcg_copy_TrackLocation_nanoStw(&acc, &_L77);
    /* 1 */
    iterateMA_nanoStw_3_38(
      &acc,
      (*thisTrain).flag.CW,
      (*thisTrain).vMax,
      (*thisTrain).vOnSight,
      tracks,
      trains,
      &_L77,
      &(*sections)[i]);
  }
  /* 1 */ if ((*thisTrain).flag.active) {
    (*MA).EoA = _L77.location;
  }
  else {
    (*MA).EoA = (*thisTrain).frontEnd.location;
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** makeTS_nanoStw_35_3_38.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

