/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _importTAdata_nanoStw_3_38_H_
#define _importTAdata_nanoStw_3_38_H_

#include "kcg_types.h"
#include "newOwnPosition_nanoStw_38.h"
#include "newPR_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* ---------------------------  outputs  --------------------------- */
  TrainData_SpeedSupervision_UnitTest_Pkg /* nanoStw::importTAdata::TD */ TD;
  simpleTrainPos_nanoStw /* nanoStw::importTAdata::thisTrain */ thisTrain;
  array_21564 /* nanoStw::importTAdata::otherTrains */ otherTrains;
  array_21324 /* nanoStw::importTAdata::tracks */ tracks;
  kcg_bool /* nanoStw::importTAdata::dataOk */ dataOk;
  TrackLocation_nanoStw /* nanoStw::importTAdata::correctionPoint */ correctionPoint;
  /* -----------------------  no local probes  ----------------------- */
  /* -------------------- initialization variables  ------------------ */
  kcg_bool init;
  /* -----------------------  no local memory  ----------------------- */
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_importTAdata_nanoStw_3_38;

/* ===========  node initialization and cycle functions  =========== */
/* nanoStw::importTAdata */
extern void importTAdata_nanoStw_3_38(
  /* nanoStw::importTAdata::tracksIn */ array_21324 *tracksIn,
  /* nanoStw::importTAdata::tracksLen */ kcg_int tracksLen,
  /* nanoStw::importTAdata::allPR */ array_21308 *allPR,
  /* nanoStw::importTAdata::PRs */ kcg_int PRs,
  /* nanoStw::importTAdata::trainReport */ TrainReport_nanoStw_trdp *trainReport,
  outC_importTAdata_nanoStw_3_38 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void importTAdata_reset_nanoStw_3_38(
  outC_importTAdata_nanoStw_3_38 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void importTAdata_init_nanoStw_3_38(
  outC_importTAdata_nanoStw_3_38 *outC);
#endif /* KCG_USER_DEFINED_INIT */

#endif /* _importTAdata_nanoStw_3_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** importTAdata_nanoStw_3_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

