/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.h"

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction */
void MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::i */ kcg_int i,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::acc */ MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI *acc,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::MRSP */ MRSP_Profile_t_TrackAtlasTypes *MRSP,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::cont */ kcg_bool *cont,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::reduced */ MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI *reduced)
{
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction */ MRSP_section_t_TrackAtlasTypes tmp;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::_L19 */ MRSP_section_t_TrackAtlasTypes _L19;
  
  if ((0 <= i) & (i < 35)) {
    kcg_copy_MRSP_section_t_TrackAtlasTypes(&_L19, &(*MRSP)[i]);
  }
  else {
    kcg_copy_MRSP_section_t_TrackAtlasTypes(
      &_L19,
      (MRSP_section_t_TrackAtlasTypes *) &DEFAULT_MRSP_section_TA_Export);
  }
  *cont = _L19.valid;
  if ((0 <= i - 1) & (i - 1 < 35)) {
    kcg_copy_MRSP_section_t_TrackAtlasTypes(&tmp, &(*MRSP)[i - 1]);
  }
  else {
    kcg_copy_MRSP_section_t_TrackAtlasTypes(
      &tmp,
      (MRSP_section_t_TrackAtlasTypes *) &DEFAULT_MRSP_section_TA_Export);
  }
  /* 1 */ if (_L19.MRS == tmp.MRS) {
    (*reduced).write_index = (*acc).write_index;
  }
  else {
    (*reduced).write_index = (*acc).write_index + 1;
  }
  kcg_copy_DMI_SpeedProfileArray_T_DMI_Types_Pkg(
    &(*reduced).MRSP,
    &(*acc).MRSP);
  if ((0 <= (*acc).write_index) & ((*acc).write_index < 32)) {
    (*reduced).MRSP[(*acc).write_index].valid = *cont;
    (*reduced).MRSP[(*acc).write_index].Loc_Abs = _L19.Loc_Abs;
    (*reduced).MRSP[(*acc).write_index].Loc_LRBG = _L19.Loc_LRBG;
    (*reduced).MRSP[(*acc).write_index].MRS = _L19.MRS;
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

