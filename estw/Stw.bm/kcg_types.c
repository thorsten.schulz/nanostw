/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

#include "kcg_types.h"

#ifdef kcg_use_MyArray_SDM_Commands_Pkg
kcg_bool kcg_comp_MyArray_SDM_Commands_Pkg(
  MyArray_SDM_Commands_Pkg *kcg_c1,
  MyArray_SDM_Commands_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 14; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_MyArray_SDM_Commands_Pkg */

#ifdef kcg_use_array_int_6_221
kcg_bool kcg_comp_array_int_6_221(
  array_int_6_221 *kcg_c1,
  array_int_6_221 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 221; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_array_int_6(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_6_221 */

#ifdef kcg_use_ASafeSpeedDefinition_T_CalcBrakingCurves_types
kcg_bool kcg_comp_ASafeSpeedDefinition_T_CalcBrakingCurves_types(
  ASafeSpeedDefinition_T_CalcBrakingCurves_types *kcg_c1,
  ASafeSpeedDefinition_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 14; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_ASafeSpeedDefinition_T_CalcBrakingCurves_types */

#ifdef kcg_use_array_20997
kcg_bool kcg_comp_array_20997(array_20997 *kcg_c1, array_20997 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 38; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_TrainRelLimitLocations_nanoStw_trdp(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_20997 */

#ifdef kcg_use_array_int_6
kcg_bool kcg_comp_array_int_6(array_int_6 *kcg_c1, array_int_6 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 6; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_6 */

#ifdef kcg_use_array_real_6
kcg_bool kcg_comp_array_real_6(array_real_6 *kcg_c1, array_real_6 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 6; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_6 */

#ifdef kcg_use_array_21021
kcg_bool kcg_comp_array_21021(array_21021 *kcg_c1, array_21021 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 35; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_TrainSpeedSegment_nanoStw(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21021 */

#ifdef kcg_use_array_int_13
kcg_bool kcg_comp_array_int_13(array_int_13 *kcg_c1, array_int_13 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 13; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_13 */

#ifdef kcg_use_array_real_13
kcg_bool kcg_comp_array_real_13(array_real_13 *kcg_c1, array_real_13 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 13; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_13 */

#ifdef kcg_use_LinkedBGs_T_BG_Types_Pkg
kcg_bool kcg_comp_LinkedBGs_T_BG_Types_Pkg(
  LinkedBGs_T_BG_Types_Pkg *kcg_c1,
  LinkedBGs_T_BG_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 33; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_LinkedBG_T_BG_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_LinkedBGs_T_BG_Types_Pkg */

#ifdef kcg_use_array_real_4_8
kcg_bool kcg_comp_array_real_4_8(array_real_4_8 *kcg_c1, array_real_4_8 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 8; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_ASafeRow_T_CalcBrakingCurves_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_4_8 */

#ifdef kcg_use_nvkvintsetArray_T_Packet_Types_Pkg
kcg_bool kcg_comp_nvkvintsetArray_T_Packet_Types_Pkg(
  nvkvintsetArray_T_Packet_Types_Pkg *kcg_c1,
  nvkvintsetArray_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 7; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_nvkvintset_T_Packet_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_nvkvintsetArray_T_Packet_Types_Pkg */

#ifdef kcg_use_array_21057
kcg_bool kcg_comp_array_21057(array_21057 *kcg_c1, array_21057 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_TrackKnotLine_nanoStw(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21057 */

#ifdef kcg_use_GradientProfile_real_t_SDM_GradientAcceleration_types
kcg_bool kcg_comp_GradientProfile_real_t_SDM_GradientAcceleration_types(
  GradientProfile_real_t_SDM_GradientAcceleration_types *kcg_c1,
  GradientProfile_real_t_SDM_GradientAcceleration_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ &
      kcg_comp_Gradient_section_real_t_SDM_GradientAcceleration_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_GradientProfile_real_t_SDM_GradientAcceleration_types */

#ifdef kcg_use_aNID_NTC_T_Packet_TrainTypes_Pkg
kcg_bool kcg_comp_aNID_NTC_T_Packet_TrainTypes_Pkg(
  aNID_NTC_T_Packet_TrainTypes_Pkg *kcg_c1,
  aNID_NTC_T_Packet_TrainTypes_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 5; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_aNID_NTC_T_Packet_TrainTypes_Pkg */

#ifdef kcg_use_nvkrintArray_T_Packet_Types_Pkg
kcg_bool kcg_comp_nvkrintArray_T_Packet_Types_Pkg(
  nvkrintArray_T_Packet_Types_Pkg *kcg_c1,
  nvkrintArray_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 7; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_nvkrint_T_Packet_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_nvkrintArray_T_Packet_Types_Pkg */

#ifdef kcg_use_array_int_35
kcg_bool kcg_comp_array_int_35(array_int_35 *kcg_c1, array_int_35 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 35; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_35 */

#ifdef kcg_use_MRSP_internal_T_TargetManagement_types
kcg_bool kcg_comp_MRSP_internal_T_TargetManagement_types(
  MRSP_internal_T_TargetManagement_types *kcg_c1,
  MRSP_internal_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 35; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_MRSP_internal_section_T_TargetManagement_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_MRSP_internal_T_TargetManagement_types */

#ifdef kcg_use_array_real_4_6
kcg_bool kcg_comp_array_real_4_6(array_real_4_6 *kcg_c1, array_real_4_6 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 6; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_ASafeRow_T_CalcBrakingCurves_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_4_6 */

#ifdef kcg_use_array_int_4
kcg_bool kcg_comp_array_int_4(array_int_4 *kcg_c1, array_int_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_4 */

#ifdef kcg_use_array_real_4
kcg_bool kcg_comp_array_real_4(array_real_4 *kcg_c1, array_real_4 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_4 */

#ifdef kcg_use_array_21157
kcg_bool kcg_comp_array_21157(array_21157 *kcg_c1, array_21157 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 35; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_TrackSection_SpeedSupervision_UnitTest_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21157 */

#ifdef kcg_use_MRSP_Profile_t_TrackAtlasTypes
kcg_bool kcg_comp_MRSP_Profile_t_TrackAtlasTypes(
  MRSP_Profile_t_TrackAtlasTypes *kcg_c1,
  MRSP_Profile_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 35; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_MRSP_section_t_TrackAtlasTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_MRSP_Profile_t_TrackAtlasTypes */

#ifdef kcg_use_ASafe_Data_T_CalcBrakingCurves_types
kcg_bool kcg_comp_ASafe_Data_T_CalcBrakingCurves_types(
  ASafe_Data_T_CalcBrakingCurves_types *kcg_c1,
  ASafe_Data_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 14; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_ASafeRow_T_CalcBrakingCurves_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_ASafe_Data_T_CalcBrakingCurves_types */

#ifdef kcg_use_array_real_3
kcg_bool kcg_comp_array_real_3(array_real_3 *kcg_c1, array_real_3 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 3; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_3 */

#ifdef kcg_use_ParabolaCurveValid_T_CalcBrakingCurves_types
kcg_bool kcg_comp_ParabolaCurveValid_T_CalcBrakingCurves_types(
  ParabolaCurveValid_T_CalcBrakingCurves_types *kcg_c1,
  ParabolaCurveValid_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 18; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_ParabolaCurveValid_T_CalcBrakingCurves_types */

#ifdef kcg_use_nidCArray_T_Packet_Types_Pkg
kcg_bool kcg_comp_nidCArray_T_Packet_Types_Pkg(
  nidCArray_T_Packet_Types_Pkg *kcg_c1,
  nidCArray_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 7; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_nidC_T_Packet_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_nidCArray_T_Packet_Types_Pkg */

#ifdef kcg_use_array_real_18
kcg_bool kcg_comp_array_real_18(array_real_18 *kcg_c1, array_real_18 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 18; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_18 */

#ifdef kcg_use_array_21308
kcg_bool kcg_comp_array_21308(array_21308 *kcg_c1, array_21308 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 3; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_ProtectionLocations_nanoStw_trdp(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21308 */

#ifdef kcg_use_array_21324
kcg_bool kcg_comp_array_21324(array_21324 *kcg_c1, array_21324 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 38; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_TrackKnot_nanoStw(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21324 */

#ifdef kcg_use_array_char_10
kcg_bool kcg_comp_array_char_10(array_char_10 *kcg_c1, array_char_10 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 10; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_char_10 */

#ifdef kcg_use_array_int_2
kcg_bool kcg_comp_array_int_2(array_int_2 *kcg_c1, array_int_2 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_2 */

#ifdef kcg_use_array_bool_17
kcg_bool kcg_comp_array_bool_17(array_bool_17 *kcg_c1, array_bool_17 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 17; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_bool_17 */

#ifdef kcg_use_t_BrakeVt_t_SDMModelPkg
kcg_bool kcg_comp_t_BrakeVt_t_SDMModelPkg(
  t_BrakeVt_t_SDMModelPkg *kcg_c1,
  t_BrakeVt_t_SDMModelPkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_t_BrakeVt_t_SDMModelPkg */

#ifdef kcg_use_array_real_17
kcg_bool kcg_comp_array_real_17(array_real_17 *kcg_c1, array_real_17 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 17; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_17 */

#ifdef kcg_use_array_21394
kcg_bool kcg_comp_array_21394(array_21394 *kcg_c1, array_21394 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 30; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_Gradient_section_t_TrackAtlasTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21394 */

#ifdef kcg_use_LKrIntLookUp_t_SDMConversionModelPkg
kcg_bool kcg_comp_LKrIntLookUp_t_SDMConversionModelPkg(
  LKrIntLookUp_t_SDMConversionModelPkg *kcg_c1,
  LKrIntLookUp_t_SDMConversionModelPkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 32; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_LKrIntLookUp_t_SDMConversionModelPkg */

#ifdef kcg_use_DMI_SpeedProfileArray_T_DMI_Types_Pkg
kcg_bool kcg_comp_DMI_SpeedProfileArray_T_DMI_Types_Pkg(
  DMI_SpeedProfileArray_T_DMI_Types_Pkg *kcg_c1,
  DMI_SpeedProfileArray_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 32; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_DMI_speedProfileElement_T_DMI_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_DMI_SpeedProfileArray_T_DMI_Types_Pkg */

#ifdef kcg_use_DMI_gradientProfileArray_T_DMI_Types_Pkg
kcg_bool kcg_comp_DMI_gradientProfileArray_T_DMI_Types_Pkg(
  DMI_gradientProfileArray_T_DMI_Types_Pkg *kcg_c1,
  DMI_gradientProfileArray_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 32; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_DMI_gradientProfileElement_T_DMI_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_DMI_gradientProfileArray_T_DMI_Types_Pkg */

#ifdef kcg_use_array_real_4_1
kcg_bool kcg_comp_array_real_4_1(array_real_4_1 *kcg_c1, array_real_4_1 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 1; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_ASafeRow_T_CalcBrakingCurves_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_4_1 */

#ifdef kcg_use_array_21456
kcg_bool kcg_comp_array_21456(array_21456 *kcg_c1, array_21456 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 1; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_Target_real_T_TargetManagement_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21456 */

#ifdef kcg_use_A_gradient_t_SDM_GradientAcceleration_types
kcg_bool kcg_comp_A_gradient_t_SDM_GradientAcceleration_types(
  A_gradient_t_SDM_GradientAcceleration_types *kcg_c1,
  A_gradient_t_SDM_GradientAcceleration_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ &
      kcg_comp_A_gradient_element_t_SDM_GradientAcceleration_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_A_gradient_t_SDM_GradientAcceleration_types */

#ifdef kcg_use_GradientProfile_real_compensated_t_SDM_GradientAcceleration_types
kcg_bool kcg_comp_GradientProfile_real_compensated_t_SDM_GradientAcceleration_types(
  GradientProfile_real_compensated_t_SDM_GradientAcceleration_types *kcg_c1,
  GradientProfile_real_compensated_t_SDM_GradientAcceleration_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ &
      kcg_comp_Gradient_section_real_t_SDM_GradientAcceleration_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_GradientProfile_real_compensated_t_SDM_GradientAcceleration_types */

#ifdef kcg_use_aTractionIdentity_T_Packet_TrainTypes_Pkg
kcg_bool kcg_comp_aTractionIdentity_T_Packet_TrainTypes_Pkg(
  aTractionIdentity_T_Packet_TrainTypes_Pkg *kcg_c1,
  aTractionIdentity_T_Packet_TrainTypes_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 4; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_sTractionIdentity_T_Packet_TrainTypes_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_aTractionIdentity_T_Packet_TrainTypes_Pkg */

#ifdef kcg_use_array_bool_1
kcg_bool kcg_comp_array_bool_1(array_bool_1 *kcg_c1, array_bool_1 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 1; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_bool_1 */

#ifdef kcg_use_array_int_221
kcg_bool kcg_comp_array_int_221(array_int_221 *kcg_c1, array_int_221 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 221; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_221 */

#ifdef kcg_use_array_real_1
kcg_bool kcg_comp_array_real_1(array_real_1 *kcg_c1, array_real_1 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 1; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_1 */

#ifdef kcg_use_Target_list_MRSP_real_T_TargetManagement_types
kcg_bool kcg_comp_Target_list_MRSP_real_T_TargetManagement_types(
  Target_list_MRSP_real_T_TargetManagement_types *kcg_c1,
  Target_list_MRSP_real_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 35; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_Target_real_T_TargetManagement_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_Target_list_MRSP_real_T_TargetManagement_types */

#ifdef kcg_use_ParabolaCurve_list_MRSP_T_CalcBrakingCurves_types
kcg_bool kcg_comp_ParabolaCurve_list_MRSP_T_CalcBrakingCurves_types(
  ParabolaCurve_list_MRSP_T_CalcBrakingCurves_types *kcg_c1,
  ParabolaCurve_list_MRSP_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 35; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_ParabolaCurve_T_CalcBrakingCurves_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_ParabolaCurve_list_MRSP_T_CalcBrakingCurves_types */

#ifdef kcg_use_array_21516
kcg_bool kcg_comp_array_21516(array_21516 *kcg_c1, array_21516 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 34; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_Target_real_T_TargetManagement_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21516 */

#ifdef kcg_use_array_real_4_13
kcg_bool kcg_comp_array_real_4_13(
  array_real_4_13 *kcg_c1,
  array_real_4_13 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 13; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_ASafeRow_T_CalcBrakingCurves_types(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_4_13 */

#ifdef kcg_use_array_int_8
kcg_bool kcg_comp_array_int_8(array_int_8 *kcg_c1, array_int_8 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 8; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_8 */

#ifdef kcg_use_DMI_trackConditionArray_T_DMI_Types_Pkg
kcg_bool kcg_comp_DMI_trackConditionArray_T_DMI_Types_Pkg(
  DMI_trackConditionArray_T_DMI_Types_Pkg *kcg_c1,
  DMI_trackConditionArray_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 32; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_DMI_trackConditionElement_T_DMI_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_DMI_trackConditionArray_T_DMI_Types_Pkg */

#ifdef kcg_use_array_real_8
kcg_bool kcg_comp_array_real_8(array_real_8 *kcg_c1, array_real_8 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 8; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_real_8 */

#ifdef kcg_use_MovementAuthoritySectionlist_t_TrackAtlasTypes
kcg_bool kcg_comp_MovementAuthoritySectionlist_t_TrackAtlasTypes(
  MovementAuthoritySectionlist_t_TrackAtlasTypes *kcg_c1,
  MovementAuthoritySectionlist_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_MovementAuthoritySection_t_TrackAtlasTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_MovementAuthoritySectionlist_t_TrackAtlasTypes */

#ifdef kcg_use_GradientProfile_t_TrackAtlasTypes
kcg_bool kcg_comp_GradientProfile_t_TrackAtlasTypes(
  GradientProfile_t_TrackAtlasTypes *kcg_c1,
  GradientProfile_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 2; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_Gradient_section_t_TrackAtlasTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_GradientProfile_t_TrackAtlasTypes */

#ifdef kcg_use_array_21564
kcg_bool kcg_comp_array_21564(array_21564 *kcg_c1, array_21564 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 3; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_otherTrainPos_nanoStw(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21564 */

#ifdef kcg_use_array_21578
kcg_bool kcg_comp_array_21578(array_21578 *kcg_c1, array_21578 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 32; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_Gradient_section_t_TrackAtlasTypes(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_21578 */

#ifdef kcg_use_array_int_7
kcg_bool kcg_comp_array_int_7(array_int_7 *kcg_c1, array_int_7 *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 7; kcg_ci++) {
    kcg_equ = kcg_equ & ((*kcg_c1)[kcg_ci] == (*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_array_int_7 */

#ifdef kcg_use_nvkvintArray_T_Packet_Types_Pkg
kcg_bool kcg_comp_nvkvintArray_T_Packet_Types_Pkg(
  nvkvintArray_T_Packet_Types_Pkg *kcg_c1,
  nvkvintArray_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  kcg_int kcg_ci;
  
  kcg_equ = kcg_true;
  for (kcg_ci = 0; kcg_ci < 7; kcg_ci++) {
    kcg_equ = kcg_equ & kcg_comp_nvkvint_T_Packet_Types_Pkg(
        &(*kcg_c1)[kcg_ci],
        &(*kcg_c2)[kcg_ci]);
  }
  return kcg_equ;
}
#endif /* kcg_use_nvkvintArray_T_Packet_Types_Pkg */

#ifdef kcg_use_struct_20671
kcg_bool kcg_comp_struct_20671(struct_20671 *kcg_c1, struct_20671 *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->gradient == kcg_c2->gradient);
  kcg_equ = kcg_equ & (kcg_c1->end_section == kcg_c2->end_section);
  kcg_equ = kcg_equ & (kcg_c1->begin_section == kcg_c2->begin_section);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_struct_20671 */

#ifdef kcg_use_simpleTrainPos_nanoStw
kcg_bool kcg_comp_simpleTrainPos_nanoStw(
  simpleTrainPos_nanoStw *kcg_c1,
  simpleTrainPos_nanoStw *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->TrainId == kcg_c2->TrainId);
  kcg_equ = kcg_equ & (kcg_c1->Ant2Cab == kcg_c2->Ant2Cab);
  kcg_equ = kcg_equ & (kcg_c1->length == kcg_c2->length);
  kcg_equ = kcg_equ & (kcg_c1->vOnSight == kcg_c2->vOnSight);
  kcg_equ = kcg_equ & (kcg_c1->vRelease == kcg_c2->vRelease);
  kcg_equ = kcg_equ & (kcg_c1->vMax == kcg_c2->vMax);
  kcg_equ = kcg_equ & (kcg_c1->BrPc == kcg_c2->BrPc);
  kcg_equ = kcg_equ & kcg_comp_StatusWord_SpeedSupervision_UnitTest_Pkg(
      &kcg_c1->flag,
      &kcg_c2->flag);
  kcg_equ = kcg_equ & (kcg_c1->acc == kcg_c2->acc);
  kcg_equ = kcg_equ & (kcg_c1->velocity == kcg_c2->velocity);
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->rearEnd,
      &kcg_c2->rearEnd);
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->antenna,
      &kcg_c2->antenna);
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->frontEnd,
      &kcg_c2->frontEnd);
  return kcg_equ;
}
#endif /* kcg_use_simpleTrainPos_nanoStw */

#ifdef kcg_use_TSM_revokeCond_T_SDM_Commands_Pkg
kcg_bool kcg_comp_TSM_revokeCond_T_SDM_Commands_Pkg(
  TSM_revokeCond_T_SDM_Commands_Pkg *kcg_c1,
  TSM_revokeCond_T_SDM_Commands_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->r3 == kcg_c2->r3);
  kcg_equ = kcg_equ & (kcg_c1->r2 == kcg_c2->r2);
  kcg_equ = kcg_equ & (kcg_c1->r1 == kcg_c2->r1);
  kcg_equ = kcg_equ & (kcg_c1->r0 == kcg_c2->r0);
  return kcg_equ;
}
#endif /* kcg_use_TSM_revokeCond_T_SDM_Commands_Pkg */

#ifdef kcg_use_TSM_triggerCond_T_SDM_Commands_Pkg
kcg_bool kcg_comp_TSM_triggerCond_T_SDM_Commands_Pkg(
  TSM_triggerCond_T_SDM_Commands_Pkg *kcg_c1,
  TSM_triggerCond_T_SDM_Commands_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->t14 == kcg_c2->t14);
  kcg_equ = kcg_equ & (kcg_c1->t13 == kcg_c2->t13);
  kcg_equ = kcg_equ & (kcg_c1->t12 == kcg_c2->t12);
  kcg_equ = kcg_equ & (kcg_c1->t11 == kcg_c2->t11);
  kcg_equ = kcg_equ & (kcg_c1->t10 == kcg_c2->t10);
  kcg_equ = kcg_equ & (kcg_c1->t9 == kcg_c2->t9);
  kcg_equ = kcg_equ & (kcg_c1->t8 == kcg_c2->t8);
  kcg_equ = kcg_equ & (kcg_c1->t7 == kcg_c2->t7);
  kcg_equ = kcg_equ & (kcg_c1->t6 == kcg_c2->t6);
  kcg_equ = kcg_equ & (kcg_c1->t5 == kcg_c2->t5);
  kcg_equ = kcg_equ & (kcg_c1->t4 == kcg_c2->t4);
  kcg_equ = kcg_equ & (kcg_c1->t3 == kcg_c2->t3);
  kcg_equ = kcg_equ & (kcg_c1->t2 == kcg_c2->t2);
  kcg_equ = kcg_equ & (kcg_c1->t1 == kcg_c2->t1);
  return kcg_equ;
}
#endif /* kcg_use_TSM_triggerCond_T_SDM_Commands_Pkg */

#ifdef kcg_use_Speeds_T_SDM_Types_Pkg
kcg_bool kcg_comp_Speeds_T_SDM_Types_Pkg(
  Speeds_T_SDM_Types_Pkg *kcg_c1,
  Speeds_T_SDM_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->OdoStandStill == kcg_c2->OdoStandStill);
  kcg_equ = kcg_equ & (kcg_c1->dV_ebi_V_target == kcg_c2->dV_ebi_V_target);
  kcg_equ = kcg_equ & (kcg_c1->dV_ebi_V_MRSP == kcg_c2->dV_ebi_V_MRSP);
  kcg_equ = kcg_equ & (kcg_c1->dV_sbi_V_target == kcg_c2->dV_sbi_V_target);
  kcg_equ = kcg_equ & (kcg_c1->dV_sbi_V_MRSP == kcg_c2->dV_sbi_V_MRSP);
  kcg_equ = kcg_equ & (kcg_c1->dV_warning_V_target ==
      kcg_c2->dV_warning_V_target);
  kcg_equ = kcg_equ & (kcg_c1->dV_warning_V_MRSP == kcg_c2->dV_warning_V_MRSP);
  kcg_equ = kcg_equ & (kcg_c1->v_FLOI_dmi == kcg_c2->v_FLOI_dmi);
  kcg_equ = kcg_equ & (kcg_c1->v_sbi_mrdt == kcg_c2->v_sbi_mrdt);
  kcg_equ = kcg_equ & (kcg_c1->v_p_dmi == kcg_c2->v_p_dmi);
  kcg_equ = kcg_equ & (kcg_c1->v_p_mrdt == kcg_c2->v_p_mrdt);
  kcg_equ = kcg_equ & (kcg_c1->V_target == kcg_c2->V_target);
  kcg_equ = kcg_equ & (kcg_c1->V_release == kcg_c2->V_release);
  kcg_equ = kcg_equ & (kcg_c1->V_MRSP == kcg_c2->V_MRSP);
  kcg_equ = kcg_equ & (kcg_c1->V_est == kcg_c2->V_est);
  return kcg_equ;
}
#endif /* kcg_use_Speeds_T_SDM_Types_Pkg */

#ifdef kcg_use_MA_section_real_T_TargetManagement_types
kcg_bool kcg_comp_MA_section_real_T_TargetManagement_types(
  MA_section_real_T_TargetManagement_types *kcg_c1,
  MA_section_real_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->level == kcg_c2->level);
  kcg_equ = kcg_equ & (kcg_c1->ReleaseSpeed == kcg_c2->ReleaseSpeed);
  kcg_equ = kcg_equ & (kcg_c1->q_calculate_release ==
      kcg_c2->q_calculate_release);
  kcg_equ = kcg_equ & (kcg_c1->Overlap == kcg_c2->Overlap);
  kcg_equ = kcg_equ & (kcg_c1->OL_valid == kcg_c2->OL_valid);
  kcg_equ = kcg_equ & (kcg_c1->DangerPoint == kcg_c2->DangerPoint);
  kcg_equ = kcg_equ & (kcg_c1->DP_valid == kcg_c2->DP_valid);
  kcg_equ = kcg_equ & kcg_comp_EOA_real_T_TargetManagement_types(
      &kcg_c1->EOA,
      &kcg_c2->EOA);
  kcg_equ = kcg_equ & (kcg_c1->v_main == kcg_c2->v_main);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_MA_section_real_T_TargetManagement_types */

#ifdef kcg_use_t_Brake_t_SDMModelPkg
kcg_bool kcg_comp_t_Brake_t_SDMModelPkg(
  t_Brake_t_SDMModelPkg *kcg_c1,
  t_Brake_t_SDMModelPkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_t_BrakeVt_t_SDMModelPkg(
      &kcg_c1->service,
      &kcg_c2->service);
  kcg_equ = kcg_equ & kcg_comp_t_BrakeVt_t_SDMModelPkg(
      &kcg_c1->emergency,
      &kcg_c2->emergency);
  return kcg_equ;
}
#endif /* kcg_use_t_Brake_t_SDMModelPkg */

#ifdef kcg_use_SDM_Locations_T_SDM_Types_Pkg
kcg_bool kcg_comp_SDM_Locations_T_SDM_Types_Pkg(
  SDM_Locations_T_SDM_Types_Pkg *kcg_c1,
  SDM_Locations_T_SDM_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->RSM_start_location_SBD_valid ==
      kcg_c2->RSM_start_location_SBD_valid);
  kcg_equ = kcg_equ & (kcg_c1->RSM_start_location_EBD_valid ==
      kcg_c2->RSM_start_location_EBD_valid);
  kcg_equ = kcg_equ & (kcg_c1->Preindication_SBD_location_valid ==
      kcg_c2->Preindication_SBD_location_valid);
  kcg_equ = kcg_equ & (kcg_c1->Preindication_EBD_location_valid ==
      kcg_c2->Preindication_EBD_location_valid);
  kcg_equ = kcg_equ & (kcg_c1->d_svl == kcg_c2->d_svl);
  kcg_equ = kcg_equ & (kcg_c1->d_eoa == kcg_c2->d_eoa);
  kcg_equ = kcg_equ & (kcg_c1->d_target == kcg_c2->d_target);
  kcg_equ = kcg_equ & (kcg_c1->SBI2_of_V_est == kcg_c2->SBI2_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->SBI1_of_V_est == kcg_c2->SBI1_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->FLOI_of_V_est == kcg_c2->FLOI_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->d_EBI_of_V_est == kcg_c2->d_EBI_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->d_FLOI_of_V_est == kcg_c2->d_FLOI_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->d_W_of_V_est == kcg_c2->d_W_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->d_P_of_V_est == kcg_c2->d_P_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->d_I_of_V_MRSP == kcg_c2->d_I_of_V_MRSP);
  kcg_equ = kcg_equ & (kcg_c1->d_I_of_V_est == kcg_c2->d_I_of_V_est);
  kcg_equ = kcg_equ & (kcg_c1->SBD_RSM_start_location ==
      kcg_c2->SBD_RSM_start_location);
  kcg_equ = kcg_equ & (kcg_c1->EBD_RSM_start_location ==
      kcg_c2->EBD_RSM_start_location);
  kcg_equ = kcg_equ & (kcg_c1->EBD_preindication_location ==
      kcg_c2->EBD_preindication_location);
  kcg_equ = kcg_equ & (kcg_c1->SBD_preindication_location ==
      kcg_c2->SBD_preindication_location);
  return kcg_equ;
}
#endif /* kcg_use_SDM_Locations_T_SDM_Types_Pkg */

#ifdef kcg_use_SDM_Commands_T_SDM_Types_Pkg
kcg_bool kcg_comp_SDM_Commands_T_SDM_Types_Pkg(
  SDM_Commands_T_SDM_Types_Pkg *kcg_c1,
  SDM_Commands_T_SDM_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->ebCmd == kcg_c2->ebCmd);
  kcg_equ = kcg_equ & (kcg_c1->triggeredTCO == kcg_c2->triggeredTCO);
  kcg_equ = kcg_equ & (kcg_c1->revokedTCO == kcg_c2->revokedTCO);
  kcg_equ = kcg_equ & (kcg_c1->triggeredEB == kcg_c2->triggeredEB);
  kcg_equ = kcg_equ & (kcg_c1->revokedEB == kcg_c2->revokedEB);
  kcg_equ = kcg_equ & (kcg_c1->triggeredSB == kcg_c2->triggeredSB);
  kcg_equ = kcg_equ & (kcg_c1->revokedSB == kcg_c2->revokedSB);
  kcg_equ = kcg_equ & (kcg_c1->triggeredSupervisionStatus ==
      kcg_c2->triggeredSupervisionStatus);
  kcg_equ = kcg_equ & (kcg_c1->revokedSupervisionStatus ==
      kcg_c2->revokedSupervisionStatus);
  kcg_equ = kcg_equ & (kcg_c1->sdmType == kcg_c2->sdmType);
  kcg_equ = kcg_equ & (kcg_c1->supervisionStatus == kcg_c2->supervisionStatus);
  kcg_equ = kcg_equ & (kcg_c1->valid_targetDistance ==
      kcg_c2->valid_targetDistance);
  kcg_equ = kcg_equ & (kcg_c1->targetDistance == kcg_c2->targetDistance);
  kcg_equ = kcg_equ & (kcg_c1->valid_v_sbi == kcg_c2->valid_v_sbi);
  kcg_equ = kcg_equ & (kcg_c1->sbiSpeed == kcg_c2->sbiSpeed);
  kcg_equ = kcg_equ & (kcg_c1->valid_v_mrdt == kcg_c2->valid_v_mrdt);
  kcg_equ = kcg_equ & (kcg_c1->mrdtSpeed == kcg_c2->mrdtSpeed);
  kcg_equ = kcg_equ & (kcg_c1->valid_v_release == kcg_c2->valid_v_release);
  kcg_equ = kcg_equ & (kcg_c1->releaseSpeed == kcg_c2->releaseSpeed);
  kcg_equ = kcg_equ & (kcg_c1->valid_v_permitted == kcg_c2->valid_v_permitted);
  kcg_equ = kcg_equ & (kcg_c1->permittedSpeed == kcg_c2->permittedSpeed);
  kcg_equ = kcg_equ & (kcg_c1->valid_v_est == kcg_c2->valid_v_est);
  kcg_equ = kcg_equ & (kcg_c1->estimatedSpeed == kcg_c2->estimatedSpeed);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_SDM_Commands_T_SDM_Types_Pkg */

#ifdef kcg_use_trainPosition_T_TrainPosition_Types_Pck
kcg_bool kcg_comp_trainPosition_T_TrainPosition_Types_Pck(
  trainPosition_T_TrainPosition_Types_Pck *kcg_c1,
  trainPosition_T_TrainPosition_Types_Pck *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->maxSafeRearEndPosition ==
      kcg_c2->maxSafeRearEndPosition);
  kcg_equ = kcg_equ & (kcg_c1->minSafeRearEndPosition ==
      kcg_c2->minSafeRearEndPosition);
  kcg_equ = kcg_equ & (kcg_c1->estimatedRearEndPosition ==
      kcg_c2->estimatedRearEndPosition);
  kcg_equ = kcg_equ & (kcg_c1->linkingIsUsedOnboard ==
      kcg_c2->linkingIsUsedOnboard);
  kcg_equ = kcg_equ & (kcg_c1->trainRunningDirectionToLRBG ==
      kcg_c2->trainRunningDirectionToLRBG);
  kcg_equ = kcg_equ & (kcg_c1->trainOrientationToLRBG ==
      kcg_c2->trainOrientationToLRBG);
  kcg_equ = kcg_equ & (kcg_c1->nominalOrReverseToLRBG ==
      kcg_c2->nominalOrReverseToLRBG);
  kcg_equ = kcg_equ & kcg_comp_positionedBG_T_TrainPosition_Types_Pck(
      &kcg_c1->prvLRBG,
      &kcg_c2->prvLRBG);
  kcg_equ = kcg_equ & kcg_comp_positionedBG_T_TrainPosition_Types_Pck(
      &kcg_c1->LRBG,
      &kcg_c2->LRBG);
  kcg_equ = kcg_equ & (kcg_c1->maxSafeFrontEndPostion ==
      kcg_c2->maxSafeFrontEndPostion);
  kcg_equ = kcg_equ & (kcg_c1->minSafeFrontEndPosition ==
      kcg_c2->minSafeFrontEndPosition);
  kcg_equ = kcg_equ & (kcg_c1->estimatedFrontEndPosition ==
      kcg_c2->estimatedFrontEndPosition);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->trainPosition,
      &kcg_c2->trainPosition);
  kcg_equ = kcg_equ & (kcg_c1->noCoordinateSystemHasBeenAssigned ==
      kcg_c2->noCoordinateSystemHasBeenAssigned);
  kcg_equ = kcg_equ & (kcg_c1->trainPositionIsUnknown ==
      kcg_c2->trainPositionIsUnknown);
  kcg_equ = kcg_equ & (kcg_c1->timestamp == kcg_c2->timestamp);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_trainPosition_T_TrainPosition_Types_Pck */

#ifdef kcg_use_BG_Header_T_BG_Types_Pkg
kcg_bool kcg_comp_BG_Header_T_BG_Types_Pkg(
  BG_Header_T_BG_Types_Pkg *kcg_c1,
  BG_Header_T_BG_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->trainRunningDirectionToBG ==
      kcg_c2->trainRunningDirectionToBG);
  kcg_equ = kcg_equ & (kcg_c1->trainOrientationToBG ==
      kcg_c2->trainOrientationToBG);
  kcg_equ = kcg_equ & (kcg_c1->noCoordinateSystemHasBeenAssigned ==
      kcg_c2->noCoordinateSystemHasBeenAssigned);
  kcg_equ = kcg_equ & (kcg_c1->q_nvlocacc == kcg_c2->q_nvlocacc);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->BG_centerDetectionInaccuraccuracies,
      &kcg_c2->BG_centerDetectionInaccuraccuracies);
  kcg_equ = kcg_equ & kcg_comp_odometry_T_Obu_BasicTypes_Pkg(
      &kcg_c1->bgPosition,
      &kcg_c2->bgPosition);
  kcg_equ = kcg_equ & (kcg_c1->q_link == kcg_c2->q_link);
  kcg_equ = kcg_equ & (kcg_c1->nid_bg == kcg_c2->nid_bg);
  kcg_equ = kcg_equ & (kcg_c1->nid_c == kcg_c2->nid_c);
  kcg_equ = kcg_equ & (kcg_c1->m_mcount == kcg_c2->m_mcount);
  kcg_equ = kcg_equ & (kcg_c1->n_total == kcg_c2->n_total);
  kcg_equ = kcg_equ & (kcg_c1->q_media == kcg_c2->q_media);
  kcg_equ = kcg_equ & (kcg_c1->m_version == kcg_c2->m_version);
  kcg_equ = kcg_equ & (kcg_c1->q_updown == kcg_c2->q_updown);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_BG_Header_T_BG_Types_Pkg */

#ifdef kcg_use_LinkedBG_T_BG_Types_Pkg
kcg_bool kcg_comp_LinkedBG_T_BG_Types_Pkg(
  LinkedBG_T_BG_Types_Pkg *kcg_c1,
  LinkedBG_T_BG_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->q_locacc == kcg_c2->q_locacc);
  kcg_equ = kcg_equ & (kcg_c1->q_linkreaction == kcg_c2->q_linkreaction);
  kcg_equ = kcg_equ & (kcg_c1->q_linkorientation == kcg_c2->q_linkorientation);
  kcg_equ = kcg_equ & (kcg_c1->nid_bg == kcg_c2->nid_bg);
  kcg_equ = kcg_equ & (kcg_c1->nid_c == kcg_c2->nid_c);
  kcg_equ = kcg_equ & (kcg_c1->q_newcountry == kcg_c2->q_newcountry);
  kcg_equ = kcg_equ & (kcg_c1->d_link == kcg_c2->d_link);
  kcg_equ = kcg_equ & (kcg_c1->q_scale == kcg_c2->q_scale);
  kcg_equ = kcg_equ & (kcg_c1->q_dir == kcg_c2->q_dir);
  kcg_equ = kcg_equ & (kcg_c1->nid_LRBG == kcg_c2->nid_LRBG);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_LinkedBG_T_BG_Types_Pkg */

#ifdef kcg_use_trainData_T_TIU_Types_Pkg
kcg_bool kcg_comp_trainData_T_TIU_Types_Pkg(
  trainData_T_TIU_Types_Pkg *kcg_c1,
  trainData_T_TIU_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_aTractionIdentity_T_Packet_TrainTypes_Pkg(
      &kcg_c1->tractionSystem,
      &kcg_c2->tractionSystem);
  kcg_equ = kcg_equ & (kcg_c1->numberTractionSystems ==
      kcg_c2->numberTractionSystems);
  kcg_equ = kcg_equ & kcg_comp_aNID_NTC_T_Packet_TrainTypes_Pkg(
      &kcg_c1->nationSystems,
      &kcg_c2->nationSystems);
  kcg_equ = kcg_equ & (kcg_c1->numberNationalSystems ==
      kcg_c2->numberNationalSystems);
  kcg_equ = kcg_equ & (kcg_c1->axleNumber == kcg_c2->axleNumber);
  kcg_equ = kcg_equ & (kcg_c1->airtightSystem == kcg_c2->airtightSystem);
  kcg_equ = kcg_equ & (kcg_c1->axleLoadCategory == kcg_c2->axleLoadCategory);
  kcg_equ = kcg_equ & (kcg_c1->loadingGauge == kcg_c2->loadingGauge);
  kcg_equ = kcg_equ & (kcg_c1->maxTrainSpeed == kcg_c2->maxTrainSpeed);
  kcg_equ = kcg_equ & (kcg_c1->brakePerctage == kcg_c2->brakePerctage);
  kcg_equ = kcg_equ & (kcg_c1->trainLength == kcg_c2->trainLength);
  kcg_equ = kcg_equ & (kcg_c1->cantDeficientcy == kcg_c2->cantDeficientcy);
  kcg_equ = kcg_equ & (kcg_c1->trainCategory == kcg_c2->trainCategory);
  kcg_equ = kcg_equ & (kcg_c1->acknowledgedByDriver ==
      kcg_c2->acknowledgedByDriver);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_trainData_T_TIU_Types_Pkg */

#ifdef kcg_use_P3_NationalValues_T_Packet_Types_Pkg
kcg_bool kcg_comp_P3_NationalValues_T_Packet_Types_Pkg(
  P3_NationalValues_T_Packet_Types_Pkg *kcg_c1,
  P3_NationalValues_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->m_nvktint == kcg_c2->m_nvktint);
  kcg_equ = kcg_equ & kcg_comp_nvkrintArray_T_Packet_Types_Pkg(
      &kcg_c1->nvkrintArray,
      &kcg_c2->nvkrintArray);
  kcg_equ = kcg_equ & kcg_comp_nvkvintsetArray_T_Packet_Types_Pkg(
      &kcg_c1->nvkvintsetArray,
      &kcg_c2->nvkvintsetArray);
  kcg_equ = kcg_equ & (kcg_c1->q_nvkint == kcg_c2->q_nvkint);
  kcg_equ = kcg_equ & (kcg_c1->m_nvebcl == kcg_c2->m_nvebcl);
  kcg_equ = kcg_equ & (kcg_c1->m_nvavadh == kcg_c2->m_nvavadh);
  kcg_equ = kcg_equ & (kcg_c1->q_nvlocacc == kcg_c2->q_nvlocacc);
  kcg_equ = kcg_equ & (kcg_c1->a_nvmaxredadh3 == kcg_c2->a_nvmaxredadh3);
  kcg_equ = kcg_equ & (kcg_c1->a_nvmaxredadh2 == kcg_c2->a_nvmaxredadh2);
  kcg_equ = kcg_equ & (kcg_c1->a_nvmaxredadh1 == kcg_c2->a_nvmaxredadh1);
  kcg_equ = kcg_equ & (kcg_c1->q_nvdriver_adhes == kcg_c2->q_nvdriver_adhes);
  kcg_equ = kcg_equ & (kcg_c1->d_nvstff == kcg_c2->d_nvstff);
  kcg_equ = kcg_equ & (kcg_c1->m_nvderun == kcg_c2->m_nvderun);
  kcg_equ = kcg_equ & (kcg_c1->t_nvcontact == kcg_c2->t_nvcontact);
  kcg_equ = kcg_equ & (kcg_c1->m_nvcontact == kcg_c2->m_nvcontact);
  kcg_equ = kcg_equ & (kcg_c1->d_nvpotrp == kcg_c2->d_nvpotrp);
  kcg_equ = kcg_equ & (kcg_c1->t_nvovtrp == kcg_c2->t_nvovtrp);
  kcg_equ = kcg_equ & (kcg_c1->d_nvovtrp == kcg_c2->d_nvovtrp);
  kcg_equ = kcg_equ & (kcg_c1->v_nvsupovtrp == kcg_c2->v_nvsupovtrp);
  kcg_equ = kcg_equ & (kcg_c1->v_nvallowovtrp == kcg_c2->v_nvallowovtrp);
  kcg_equ = kcg_equ & (kcg_c1->q_nvinhsmicperm == kcg_c2->q_nvinhsmicperm);
  kcg_equ = kcg_equ & (kcg_c1->q_nvsbfbperm == kcg_c2->q_nvsbfbperm);
  kcg_equ = kcg_equ & (kcg_c1->q_nvguiperm == kcg_c2->q_nvguiperm);
  kcg_equ = kcg_equ & (kcg_c1->q_nvemrrls == kcg_c2->q_nvemrrls);
  kcg_equ = kcg_equ & (kcg_c1->q_nvsbtsmperm == kcg_c2->q_nvsbtsmperm);
  kcg_equ = kcg_equ & (kcg_c1->d_nvroll == kcg_c2->d_nvroll);
  kcg_equ = kcg_equ & (kcg_c1->v_nvrel == kcg_c2->v_nvrel);
  kcg_equ = kcg_equ & (kcg_c1->v_nvunfit == kcg_c2->v_nvunfit);
  kcg_equ = kcg_equ & (kcg_c1->v_nvlimsuperv == kcg_c2->v_nvlimsuperv);
  kcg_equ = kcg_equ & (kcg_c1->v_nvonsight == kcg_c2->v_nvonsight);
  kcg_equ = kcg_equ & (kcg_c1->v_nvstff == kcg_c2->v_nvstff);
  kcg_equ = kcg_equ & (kcg_c1->v_nvshunt == kcg_c2->v_nvshunt);
  kcg_equ = kcg_equ & kcg_comp_nidCArray_T_Packet_Types_Pkg(
      &kcg_c1->nid_cArray,
      &kcg_c2->nid_cArray);
  kcg_equ = kcg_equ & (kcg_c1->d_validnv == kcg_c2->d_validnv);
  kcg_equ = kcg_equ & (kcg_c1->q_dir == kcg_c2->q_dir);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_P3_NationalValues_T_Packet_Types_Pkg */

#ifdef kcg_use_MovementAuthority_t_TrackAtlasTypes
kcg_bool kcg_comp_MovementAuthority_t_TrackAtlasTypes(
  MovementAuthority_t_TrackAtlasTypes *kcg_c1,
  MovementAuthority_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_Endtimer_t_TrackAtlasTypes(
      &kcg_c1->endtimer_t,
      &kcg_c2->endtimer_t);
  kcg_equ = kcg_equ & (kcg_c1->q_endtimer == kcg_c2->q_endtimer);
  kcg_equ = kcg_equ & kcg_comp_DP_or_OL_t_TrackAtlasTypes(
      &kcg_c1->overlap,
      &kcg_c2->overlap);
  kcg_equ = kcg_equ & (kcg_c1->q_overlap == kcg_c2->q_overlap);
  kcg_equ = kcg_equ & kcg_comp_DP_or_OL_t_TrackAtlasTypes(
      &kcg_c1->dangerpoint,
      &kcg_c2->dangerpoint);
  kcg_equ = kcg_equ & (kcg_c1->q_dangerpoint == kcg_c2->q_dangerpoint);
  kcg_equ = kcg_equ & kcg_comp_MovementAuthoritySectionlist_t_TrackAtlasTypes(
      &kcg_c1->sections,
      &kcg_c2->sections);
  kcg_equ = kcg_equ & (kcg_c1->n_iter == kcg_c2->n_iter);
  kcg_equ = kcg_equ & (kcg_c1->t_loa == kcg_c2->t_loa);
  kcg_equ = kcg_equ & (kcg_c1->t_loa_unlimited == kcg_c2->t_loa_unlimited);
  kcg_equ = kcg_equ & (kcg_c1->v_loa == kcg_c2->v_loa);
  kcg_equ = kcg_equ & (kcg_c1->v_main == kcg_c2->v_main);
  kcg_equ = kcg_equ & (kcg_c1->q_dir == kcg_c2->q_dir);
  kcg_equ = kcg_equ & (kcg_c1->Level == kcg_c2->Level);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_MovementAuthority_t_TrackAtlasTypes */

#ifdef kcg_use_struct_20936
kcg_bool kcg_comp_struct_20936(struct_20936 *kcg_c1, struct_20936 *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->TrainId == kcg_c2->TrainId);
  kcg_equ = kcg_equ & (kcg_c1->Ant2Cab == kcg_c2->Ant2Cab);
  kcg_equ = kcg_equ & (kcg_c1->length == kcg_c2->length);
  kcg_equ = kcg_equ & (kcg_c1->vOnSight == kcg_c2->vOnSight);
  kcg_equ = kcg_equ & (kcg_c1->vRelease == kcg_c2->vRelease);
  kcg_equ = kcg_equ & (kcg_c1->vMax == kcg_c2->vMax);
  kcg_equ = kcg_equ & (kcg_c1->BrPc == kcg_c2->BrPc);
  kcg_equ = kcg_equ & kcg_comp_StatusWord_SpeedSupervision_UnitTest_Pkg(
      &kcg_c1->flag,
      &kcg_c2->flag);
  kcg_equ = kcg_equ & (kcg_c1->time == kcg_c2->time);
  kcg_equ = kcg_equ & (kcg_c1->acc == kcg_c2->acc);
  kcg_equ = kcg_equ & (kcg_c1->velocity == kcg_c2->velocity);
  kcg_equ = kcg_equ & (kcg_c1->location == kcg_c2->location);
  return kcg_equ;
}
#endif /* kcg_use_struct_20936 */

#ifdef kcg_use_StatusWord_SpeedSupervision_UnitTest_Pkg
kcg_bool kcg_comp_StatusWord_SpeedSupervision_UnitTest_Pkg(
  StatusWord_SpeedSupervision_UnitTest_Pkg *kcg_c1,
  StatusWord_SpeedSupervision_UnitTest_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->stopped == kcg_c2->stopped);
  kcg_equ = kcg_equ & (kcg_c1->tripped == kcg_c2->tripped);
  kcg_equ = kcg_equ & (kcg_c1->CW == kcg_c2->CW);
  kcg_equ = kcg_equ & (kcg_c1->active == kcg_c2->active);
  return kcg_equ;
}
#endif /* kcg_use_StatusWord_SpeedSupervision_UnitTest_Pkg */

#ifdef kcg_use_speedSupervisionForDMI_T_DMI_Types_Pkg
kcg_bool kcg_comp_speedSupervisionForDMI_T_DMI_Types_Pkg(
  speedSupervisionForDMI_T_DMI_Types_Pkg *kcg_c1,
  speedSupervisionForDMI_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->distanceIndicationPoint ==
      kcg_c2->distanceIndicationPoint);
  kcg_equ = kcg_equ & (kcg_c1->supervisionDisplay ==
      kcg_c2->supervisionDisplay);
  kcg_equ = kcg_equ & (kcg_c1->sup_status == kcg_c2->sup_status);
  kcg_equ = kcg_equ & (kcg_c1->interventionSpeed == kcg_c2->interventionSpeed);
  kcg_equ = kcg_equ & (kcg_c1->location_brake_curve_starting_point ==
      kcg_c2->location_brake_curve_starting_point);
  kcg_equ = kcg_equ & (kcg_c1->locationBrakeTarget ==
      kcg_c2->locationBrakeTarget);
  kcg_equ = kcg_equ & (kcg_c1->releaseSpeed == kcg_c2->releaseSpeed);
  kcg_equ = kcg_equ & (kcg_c1->permittedSpeed == kcg_c2->permittedSpeed);
  kcg_equ = kcg_equ & (kcg_c1->targetSpeed == kcg_c2->targetSpeed);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_speedSupervisionForDMI_T_DMI_Types_Pkg */

#ifdef kcg_use_struct_20972
kcg_bool kcg_comp_struct_20972(struct_20972 *kcg_c1, struct_20972 *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->MRS == kcg_c2->MRS);
  kcg_equ = kcg_equ & (kcg_c1->Loc_LRBG == kcg_c2->Loc_LRBG);
  kcg_equ = kcg_equ & (kcg_c1->Loc_Abs == kcg_c2->Loc_Abs);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_struct_20972 */

#ifdef kcg_use_bec_t_TargetLimits_Pkg
kcg_bool kcg_comp_bec_t_TargetLimits_Pkg(
  bec_t_TargetLimits_Pkg *kcg_c1,
  bec_t_TargetLimits_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->d == kcg_c2->d);
  kcg_equ = kcg_equ & (kcg_c1->v == kcg_c2->v);
  return kcg_equ;
}
#endif /* kcg_use_bec_t_TargetLimits_Pkg */

#ifdef kcg_use_DataForSupervision_nextGen_t_TrackAtlasTypes
kcg_bool kcg_comp_DataForSupervision_nextGen_t_TrackAtlasTypes(
  DataForSupervision_nextGen_t_TrackAtlasTypes *kcg_c1,
  DataForSupervision_nextGen_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_MRSP_Profile_t_TrackAtlasTypes(
      &kcg_c1->MRSP,
      &kcg_c2->MRSP);
  kcg_equ = kcg_equ & (kcg_c1->freshMRSP == kcg_c2->freshMRSP);
  kcg_equ = kcg_equ & kcg_comp_GradientProfile_t_TrackAtlasTypes(
      &kcg_c1->GradientProfile,
      &kcg_c2->GradientProfile);
  kcg_equ = kcg_equ & (kcg_c1->freshGradientProfile ==
      kcg_c2->freshGradientProfile);
  kcg_equ = kcg_equ & kcg_comp_MovementAuthority_t_TrackAtlasTypes(
      &kcg_c1->MA,
      &kcg_c2->MA);
  kcg_equ = kcg_equ & (kcg_c1->freshMA == kcg_c2->freshMA);
  return kcg_equ;
}
#endif /* kcg_use_DataForSupervision_nextGen_t_TrackAtlasTypes */

#ifdef kcg_use_LocWithInAcc_T_Obu_BasicTypes_Pkg
kcg_bool kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
  LocWithInAcc_T_Obu_BasicTypes_Pkg *kcg_c1,
  LocWithInAcc_T_Obu_BasicTypes_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->d_max == kcg_c2->d_max);
  kcg_equ = kcg_equ & (kcg_c1->d_min == kcg_c2->d_min);
  kcg_equ = kcg_equ & (kcg_c1->nominal == kcg_c2->nominal);
  return kcg_equ;
}
#endif /* kcg_use_LocWithInAcc_T_Obu_BasicTypes_Pkg */

#ifdef kcg_use_DP_or_OL_t_TrackAtlasTypes
kcg_bool kcg_comp_DP_or_OL_t_TrackAtlasTypes(
  DP_or_OL_t_TrackAtlasTypes *kcg_c1,
  DP_or_OL_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->calc_v_release_onboard ==
      kcg_c2->calc_v_release_onboard);
  kcg_equ = kcg_equ & (kcg_c1->v_release == kcg_c2->v_release);
  kcg_equ = kcg_equ & (kcg_c1->d_DP_or_OL == kcg_c2->d_DP_or_OL);
  return kcg_equ;
}
#endif /* kcg_use_DP_or_OL_t_TrackAtlasTypes */

#ifdef kcg_use_TargetCollection_T_TargetManagement_types
kcg_bool kcg_comp_TargetCollection_T_TargetManagement_types(
  TargetCollection_T_TargetManagement_types *kcg_c1,
  TargetCollection_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_Target_real_T_TargetManagement_types(
      &kcg_c1->SvL_LoA_target,
      &kcg_c2->SvL_LoA_target);
  kcg_equ = kcg_equ & kcg_comp_Target_real_T_TargetManagement_types(
      &kcg_c1->EOA_target,
      &kcg_c2->EOA_target);
  kcg_equ = kcg_equ & kcg_comp_Target_list_MRSP_real_T_TargetManagement_types(
      &kcg_c1->MRSP_targetList,
      &kcg_c2->MRSP_targetList);
  kcg_equ = kcg_equ & (kcg_c1->updatedTargetList == kcg_c2->updatedTargetList);
  return kcg_equ;
}
#endif /* kcg_use_TargetCollection_T_TargetManagement_types */

#ifdef kcg_use_SDM_DMI_wrapper_T_SDM_Types_Pkg
kcg_bool kcg_comp_SDM_DMI_wrapper_T_SDM_Types_Pkg(
  SDM_DMI_wrapper_T_SDM_Types_Pkg *kcg_c1,
  SDM_DMI_wrapper_T_SDM_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ &
    kcg_comp_DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg(
      &kcg_c1->ta,
      &kcg_c2->ta);
  kcg_equ = kcg_equ & kcg_comp_Brake_command_T_TIU_Types_Pkg(
      &kcg_c1->brake,
      &kcg_c2->brake);
  kcg_equ = kcg_equ & kcg_comp_odometry_T_Obu_BasicTypes_Pkg(
      &kcg_c1->odo,
      &kcg_c2->odo);
  kcg_equ = kcg_equ & kcg_comp_speedSupervisionForDMI_T_DMI_Types_Pkg(
      &kcg_c1->sdm,
      &kcg_c2->sdm);
  return kcg_equ;
}
#endif /* kcg_use_SDM_DMI_wrapper_T_SDM_Types_Pkg */

#ifdef kcg_use_TrackDMI_nanoSim
kcg_bool kcg_comp_TrackDMI_nanoSim(
  TrackDMI_nanoSim *kcg_c1,
  TrackDMI_nanoSim *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_array_20997(
      &kcg_c1->TrackRelLocs,
      &kcg_c2->TrackRelLocs);
  kcg_equ = kcg_equ & (kcg_c1->sizeTracks == kcg_c2->sizeTracks);
  kcg_equ = kcg_equ & (kcg_c1->TrainID == kcg_c2->TrainID);
  return kcg_equ;
}
#endif /* kcg_use_TrackDMI_nanoSim */

#ifdef kcg_use_ASafe_T_CalcBrakingCurves_types
kcg_bool kcg_comp_ASafe_T_CalcBrakingCurves_types(
  ASafe_T_CalcBrakingCurves_types *kcg_c1,
  ASafe_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_ASafe_Data_T_CalcBrakingCurves_types(
      &kcg_c1->data,
      &kcg_c2->data);
  kcg_equ = kcg_equ & kcg_comp_ASafeSpeedDefinition_T_CalcBrakingCurves_types(
      &kcg_c1->speed_definition,
      &kcg_c2->speed_definition);
  kcg_equ = kcg_equ &
    kcg_comp_ASafeDistanceDefinition_T_CalcBrakingCurves_types(
      &kcg_c1->distance_definition,
      &kcg_c2->distance_definition);
  return kcg_equ;
}
#endif /* kcg_use_ASafe_T_CalcBrakingCurves_types */

#ifdef kcg_use_sTractionIdentity_T_Packet_TrainTypes_Pkg
kcg_bool kcg_comp_sTractionIdentity_T_Packet_TrainTypes_Pkg(
  sTractionIdentity_T_Packet_TrainTypes_Pkg *kcg_c1,
  sTractionIdentity_T_Packet_TrainTypes_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->nid_ctraction == kcg_c2->nid_ctraction);
  kcg_equ = kcg_equ & (kcg_c1->m_voltage == kcg_c2->m_voltage);
  return kcg_equ;
}
#endif /* kcg_use_sTractionIdentity_T_Packet_TrainTypes_Pkg */

#ifdef kcg_use_T_trac_t_TargetLimits_Pkg
kcg_bool kcg_comp_T_trac_t_TargetLimits_Pkg(
  T_trac_t_TargetLimits_Pkg *kcg_c1,
  T_trac_t_TargetLimits_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->indication == kcg_c2->indication);
  kcg_equ = kcg_equ & (kcg_c1->inhComp == kcg_c2->inhComp);
  kcg_equ = kcg_equ & (kcg_c1->bs2 == kcg_c2->bs2);
  kcg_equ = kcg_equ & (kcg_c1->bs1 == kcg_c2->bs1);
  kcg_equ = kcg_equ & (kcg_c1->bs == kcg_c2->bs);
  kcg_equ = kcg_equ & (kcg_c1->berem == kcg_c2->berem);
  kcg_equ = kcg_equ & (kcg_c1->Traction == kcg_c2->Traction);
  return kcg_equ;
}
#endif /* kcg_use_T_trac_t_TargetLimits_Pkg */

#ifdef kcg_use_Endtimer_t_TrackAtlasTypes
kcg_bool kcg_comp_Endtimer_t_TrackAtlasTypes(
  Endtimer_t_TrackAtlasTypes *kcg_c1,
  Endtimer_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->d_endtimerstoploc == kcg_c2->d_endtimerstoploc);
  kcg_equ = kcg_equ & (kcg_c1->t_endtimer == kcg_c2->t_endtimer);
  return kcg_equ;
}
#endif /* kcg_use_Endtimer_t_TrackAtlasTypes */

#ifdef kcg_use_BCAccumulator_type_CalcBrakingCurves_Pkg
kcg_bool kcg_comp_BCAccumulator_type_CalcBrakingCurves_Pkg(
  BCAccumulator_type_CalcBrakingCurves_Pkg *kcg_c1,
  BCAccumulator_type_CalcBrakingCurves_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_ParabolaCurve_T_CalcBrakingCurves_types(
      &kcg_c1->BC,
      &kcg_c2->BC);
  kcg_equ = kcg_equ & (kcg_c1->speedIndex == kcg_c2->speedIndex);
  kcg_equ = kcg_equ & (kcg_c1->distanceIndex == kcg_c2->distanceIndex);
  kcg_equ = kcg_equ & (kcg_c1->speed == kcg_c2->speed);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  return kcg_equ;
}
#endif /* kcg_use_BCAccumulator_type_CalcBrakingCurves_Pkg */

#ifdef kcg_use_DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg
kcg_bool kcg_comp_DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg(
  DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg *kcg_c1,
  DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_DMI_trackCondition_T_DMI_Types_Pkg(
      &kcg_c1->trackConditions,
      &kcg_c2->trackConditions);
  kcg_equ = kcg_equ & kcg_comp_DMI_gradientProfile_T_DMI_Types_Pkg(
      &kcg_c1->gradientProfiles,
      &kcg_c2->gradientProfiles);
  kcg_equ = kcg_equ & kcg_comp_DMI_speedProfile_T_DMI_Types_Pkg(
      &kcg_c1->speedProfiles,
      &kcg_c2->speedProfiles);
  kcg_equ = kcg_equ & (kcg_c1->system_clock == kcg_c2->system_clock);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg */

#ifdef kcg_use_trainData_internal_t_SDM_Types_Pkg
kcg_bool kcg_comp_trainData_internal_t_SDM_Types_Pkg(
  trainData_internal_t_SDM_Types_Pkg *kcg_c1,
  trainData_internal_t_SDM_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->T_b_limit == kcg_c2->T_b_limit);
  kcg_equ = kcg_equ & (kcg_c1->offsetAntennaL1 == kcg_c2->offsetAntennaL1);
  kcg_equ = kcg_equ & (kcg_c1->T_traction_cut_off ==
      kcg_c2->T_traction_cut_off);
  kcg_equ = kcg_equ & (kcg_c1->isTCOAvailable == kcg_c2->isTCOAvailable);
  kcg_equ = kcg_equ & (kcg_c1->isSB_CmdAvailable == kcg_c2->isSB_CmdAvailable);
  kcg_equ = kcg_equ & (kcg_c1->isSB_FBAvailable == kcg_c2->isSB_FBAvailable);
  return kcg_equ;
}
#endif /* kcg_use_trainData_internal_t_SDM_Types_Pkg */

#ifdef kcg_use_av_MergedMap_t_SDMConversionModelPkg
kcg_bool kcg_comp_av_MergedMap_t_SDMConversionModelPkg(
  av_MergedMap_t_SDMConversionModelPkg *kcg_c1,
  av_MergedMap_t_SDMConversionModelPkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_v_MergedSteps_t_SDMConversionModelPkg(
      &kcg_c1->v,
      &kcg_c2->v);
  kcg_equ = kcg_equ & kcg_comp_a_MergedSteps_t_SDMConversionModelPkg(
      &kcg_c1->a,
      &kcg_c2->a);
  return kcg_equ;
}
#endif /* kcg_use_av_MergedMap_t_SDMConversionModelPkg */

#ifdef kcg_use_A_gradient_element_t_SDM_GradientAcceleration_types
kcg_bool kcg_comp_A_gradient_element_t_SDM_GradientAcceleration_types(
  A_gradient_element_t_SDM_GradientAcceleration_types *kcg_c1,
  A_gradient_element_t_SDM_GradientAcceleration_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  kcg_equ = kcg_equ & (kcg_c1->gradient_acceleration ==
      kcg_c2->gradient_acceleration);
  kcg_equ = kcg_equ & (kcg_c1->position == kcg_c2->position);
  return kcg_equ;
}
#endif /* kcg_use_A_gradient_element_t_SDM_GradientAcceleration_types */

#ifdef kcg_use_TargetIteratorAkku_TargetLimits_Pkg
kcg_bool kcg_comp_TargetIteratorAkku_TargetLimits_Pkg(
  TargetIteratorAkku_TargetLimits_Pkg *kcg_c1,
  TargetIteratorAkku_TargetLimits_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->V_P_MRDT_index == kcg_c2->V_P_MRDT_index);
  kcg_equ = kcg_equ & (kcg_c1->V_P_MRDT_ebd == kcg_c2->V_P_MRDT_ebd);
  kcg_equ = kcg_equ & (kcg_c1->V_ura == kcg_c2->V_ura);
  kcg_equ = kcg_equ & kcg_comp_T_trac_t_TargetLimits_Pkg(
      &kcg_c1->T,
      &kcg_c2->T);
  kcg_equ = kcg_equ & (kcg_c1->V_est == kcg_c2->V_est);
  kcg_equ = kcg_equ & kcg_comp_bec_t_TargetLimits_Pkg(
      &kcg_c1->bec,
      &kcg_c2->bec);
  kcg_equ = kcg_equ & (kcg_c1->ttype == kcg_c2->ttype);
  kcg_equ = kcg_equ & (kcg_c1->index == kcg_c2->index);
  kcg_equ = kcg_equ & (kcg_c1->SBI2 == kcg_c2->SBI2);
  return kcg_equ;
}
#endif /* kcg_use_TargetIteratorAkku_TargetLimits_Pkg */

#ifdef kcg_use_positionedBG_T_TrainPosition_Types_Pck
kcg_bool kcg_comp_positionedBG_T_TrainPosition_Types_Pck(
  positionedBG_T_TrainPosition_Types_Pck *kcg_c1,
  positionedBG_T_TrainPosition_Types_Pck *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->missed == kcg_c2->missed);
  kcg_equ = kcg_equ & kcg_comp_passedBG_T_BG_Types_Pkg(
      &kcg_c1->infoFromPassing,
      &kcg_c2->infoFromPassing);
  kcg_equ = kcg_equ & kcg_comp_infoFromLinking_T_TrainPosition_Types_Pck(
      &kcg_c1->infoFromLinking,
      &kcg_c2->infoFromLinking);
  kcg_equ = kcg_equ & (kcg_c1->seqNoOnTrack == kcg_c2->seqNoOnTrack);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->location,
      &kcg_c2->location);
  kcg_equ = kcg_equ & (kcg_c1->q_link == kcg_c2->q_link);
  kcg_equ = kcg_equ & (kcg_c1->nid_bg == kcg_c2->nid_bg);
  kcg_equ = kcg_equ & (kcg_c1->nid_c == kcg_c2->nid_c);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_positionedBG_T_TrainPosition_Types_Pck */

#ifdef kcg_use_struct_21191
kcg_bool kcg_comp_struct_21191(struct_21191 *kcg_c1, struct_21191 *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->ProtectedFront == kcg_c2->ProtectedFront);
  kcg_equ = kcg_equ & (kcg_c1->Target == kcg_c2->Target);
  kcg_equ = kcg_equ & (kcg_c1->FLOI == kcg_c2->FLOI);
  kcg_equ = kcg_equ & (kcg_c1->Permitted == kcg_c2->Permitted);
  kcg_equ = kcg_equ & (kcg_c1->Indication == kcg_c2->Indication);
  kcg_equ = kcg_equ & (kcg_c1->PI == kcg_c2->PI);
  kcg_equ = kcg_equ & (kcg_c1->Front == kcg_c2->Front);
  kcg_equ = kcg_equ & (kcg_c1->Rear == kcg_c2->Rear);
  return kcg_equ;
}
#endif /* kcg_use_struct_21191 */

#ifdef kcg_use_ProtectionLocations_nanoStw_trdp
kcg_bool kcg_comp_ProtectionLocations_nanoStw_trdp(
  ProtectionLocations_nanoStw_trdp *kcg_c1,
  ProtectionLocations_nanoStw_trdp *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_array_int_8(
      &kcg_c1->trainType16,
      &kcg_c2->trainType16);
  kcg_equ = kcg_equ & kcg_comp_array_char_10(
      &kcg_c1->trainType8,
      &kcg_c2->trainType8);
  kcg_equ = kcg_equ & (kcg_c1->sizeTrainType8 == kcg_c2->sizeTrainType8);
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->protectedRearEnd,
      &kcg_c2->protectedRearEnd);
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->protectedFrontEnd,
      &kcg_c2->protectedFrontEnd);
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->antenna,
      &kcg_c2->antenna);
  kcg_equ = kcg_equ & (kcg_c1->acc == kcg_c2->acc);
  kcg_equ = kcg_equ & (kcg_c1->velocity == kcg_c2->velocity);
  kcg_equ = kcg_equ & (kcg_c1->tid == kcg_c2->tid);
  return kcg_equ;
}
#endif /* kcg_use_ProtectionLocations_nanoStw_trdp */

#ifdef kcg_use_odometry_T_Obu_BasicTypes_Pkg
kcg_bool kcg_comp_odometry_T_Obu_BasicTypes_Pkg(
  odometry_T_Obu_BasicTypes_Pkg *kcg_c1,
  odometry_T_Obu_BasicTypes_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->motionDirection == kcg_c2->motionDirection);
  kcg_equ = kcg_equ & (kcg_c1->motionState == kcg_c2->motionState);
  kcg_equ = kcg_equ & (kcg_c1->acceleration == kcg_c2->acceleration);
  kcg_equ = kcg_equ & kcg_comp_OdometrySpeeds_T_Obu_BasicTypes_Pkg(
      &kcg_c1->speed,
      &kcg_c2->speed);
  kcg_equ = kcg_equ & kcg_comp_OdometryLocations_T_Obu_BasicTypes_Pkg(
      &kcg_c1->odo,
      &kcg_c2->odo);
  kcg_equ = kcg_equ & (kcg_c1->timestamp == kcg_c2->timestamp);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_odometry_T_Obu_BasicTypes_Pkg */

#ifdef kcg_use_OdometrySpeeds_T_Obu_BasicTypes_Pkg
kcg_bool kcg_comp_OdometrySpeeds_T_Obu_BasicTypes_Pkg(
  OdometrySpeeds_T_Obu_BasicTypes_Pkg *kcg_c1,
  OdometrySpeeds_T_Obu_BasicTypes_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->v_upper == kcg_c2->v_upper);
  kcg_equ = kcg_equ & (kcg_c1->v_lower == kcg_c2->v_lower);
  kcg_equ = kcg_equ & (kcg_c1->v_rawNominal == kcg_c2->v_rawNominal);
  kcg_equ = kcg_equ & (kcg_c1->v_safeNominal == kcg_c2->v_safeNominal);
  return kcg_equ;
}
#endif /* kcg_use_OdometrySpeeds_T_Obu_BasicTypes_Pkg */

#ifdef kcg_use_passedBG_T_BG_Types_Pkg
kcg_bool kcg_comp_passedBG_T_BG_Types_Pkg(
  passedBG_T_BG_Types_Pkg *kcg_c1,
  passedBG_T_BG_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_LinkedBGs_T_BG_Types_Pkg(
      &kcg_c1->linkedBGs,
      &kcg_c2->linkedBGs);
  kcg_equ = kcg_equ & kcg_comp_BG_Header_T_BG_Types_Pkg(
      &kcg_c1->BG_Header,
      &kcg_c2->BG_Header);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_passedBG_T_BG_Types_Pkg */

#ifdef kcg_use_trainProperties_T_TrainPosition_Types_Pck
kcg_bool kcg_comp_trainProperties_T_TrainPosition_Types_Pck(
  trainProperties_T_TrainPosition_Types_Pck *kcg_c1,
  trainProperties_T_TrainPosition_Types_Pck *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->centerDetectionAcc_DefaultValue,
      &kcg_c2->centerDetectionAcc_DefaultValue);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->locationAccuracy_DefaultValue,
      &kcg_c2->locationAccuracy_DefaultValue);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->d_frontend_2_rearend,
      &kcg_c2->d_frontend_2_rearend);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->d_baliseAntenna_2_frontend,
      &kcg_c2->d_baliseAntenna_2_frontend);
  kcg_equ = kcg_equ & (kcg_c1->l_train == kcg_c2->l_train);
  kcg_equ = kcg_equ & (kcg_c1->nid_operational == kcg_c2->nid_operational);
  kcg_equ = kcg_equ & (kcg_c1->nid_engine == kcg_c2->nid_engine);
  return kcg_equ;
}
#endif /* kcg_use_trainProperties_T_TrainPosition_Types_Pck */

#ifdef kcg_use_TrackLocation_nanoStw
kcg_bool kcg_comp_TrackLocation_nanoStw(
  TrackLocation_nanoStw *kcg_c1,
  TrackLocation_nanoStw *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->location == kcg_c2->location);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  kcg_equ = kcg_equ & (kcg_c1->sid == kcg_c2->sid);
  return kcg_equ;
}
#endif /* kcg_use_TrackLocation_nanoStw */

#ifdef kcg_use_Gradient_section_real_t_SDM_GradientAcceleration_types
kcg_bool kcg_comp_Gradient_section_real_t_SDM_GradientAcceleration_types(
  Gradient_section_real_t_SDM_GradientAcceleration_types *kcg_c1,
  Gradient_section_real_t_SDM_GradientAcceleration_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  kcg_equ = kcg_equ & (kcg_c1->gradient == kcg_c2->gradient);
  kcg_equ = kcg_equ & (kcg_c1->location == kcg_c2->location);
  return kcg_equ;
}
#endif /* kcg_use_Gradient_section_real_t_SDM_GradientAcceleration_types */

#ifdef kcg_use_TrackKnotLine_nanoStw
kcg_bool kcg_comp_TrackKnotLine_nanoStw(
  TrackKnotLine_nanoStw *kcg_c1,
  TrackKnotLine_nanoStw *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->on == kcg_c2->on);
  kcg_equ = kcg_equ & (kcg_c1->rLine == kcg_c2->rLine);
  kcg_equ = kcg_equ & (kcg_c1->to == kcg_c2->to);
  return kcg_equ;
}
#endif /* kcg_use_TrackKnotLine_nanoStw */

#ifdef kcg_use_Gradient_section_t_TrackAtlasTypes
kcg_bool kcg_comp_Gradient_section_t_TrackAtlasTypes(
  Gradient_section_t_TrackAtlasTypes *kcg_c1,
  Gradient_section_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->L_Gradient == kcg_c2->L_Gradient);
  kcg_equ = kcg_equ & (kcg_c1->Gradient == kcg_c2->Gradient);
  kcg_equ = kcg_equ & (kcg_c1->Loc_LRBG == kcg_c2->Loc_LRBG);
  kcg_equ = kcg_equ & (kcg_c1->Loc_Absolute == kcg_c2->Loc_Absolute);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_Gradient_section_t_TrackAtlasTypes */

#ifdef kcg_use_extractTargetsMRSPACC_TargetManagement_pkg
kcg_bool kcg_comp_extractTargetsMRSPACC_TargetManagement_pkg(
  extractTargetsMRSPACC_TargetManagement_pkg *kcg_c1,
  extractTargetsMRSPACC_TargetManagement_pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->lastInsertedTargetIndex ==
      kcg_c2->lastInsertedTargetIndex);
  kcg_equ = kcg_equ & kcg_comp_Target_list_MRSP_real_T_TargetManagement_types(
      &kcg_c1->targetList,
      &kcg_c2->targetList);
  return kcg_equ;
}
#endif /* kcg_use_extractTargetsMRSPACC_TargetManagement_pkg */

#ifdef kcg_use_ParabolaArc_T_CalcBrakingCurves_types
kcg_bool kcg_comp_ParabolaArc_T_CalcBrakingCurves_types(
  ParabolaArc_T_CalcBrakingCurves_types *kcg_c1,
  ParabolaArc_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->acceleration == kcg_c2->acceleration);
  kcg_equ = kcg_equ & (kcg_c1->speed == kcg_c2->speed);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  return kcg_equ;
}
#endif /* kcg_use_ParabolaArc_T_CalcBrakingCurves_types */

#ifdef kcg_use_MovementAuthoritySection_t_TrackAtlasTypes
kcg_bool kcg_comp_MovementAuthoritySection_t_TrackAtlasTypes(
  MovementAuthoritySection_t_TrackAtlasTypes *kcg_c1,
  MovementAuthoritySection_t_TrackAtlasTypes *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->d_sectiontimerstoploc ==
      kcg_c2->d_sectiontimerstoploc);
  kcg_equ = kcg_equ & (kcg_c1->t_sectiontimer == kcg_c2->t_sectiontimer);
  kcg_equ = kcg_equ & (kcg_c1->q_sectiontimer == kcg_c2->q_sectiontimer);
  kcg_equ = kcg_equ & (kcg_c1->l_section == kcg_c2->l_section);
  kcg_equ = kcg_equ & (kcg_c1->q_endsection == kcg_c2->q_endsection);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_MovementAuthoritySection_t_TrackAtlasTypes */

#ifdef kcg_use_OdometryLocations_T_Obu_BasicTypes_Pkg
kcg_bool kcg_comp_OdometryLocations_T_Obu_BasicTypes_Pkg(
  OdometryLocations_T_Obu_BasicTypes_Pkg *kcg_c1,
  OdometryLocations_T_Obu_BasicTypes_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->o_max == kcg_c2->o_max);
  kcg_equ = kcg_equ & (kcg_c1->o_min == kcg_c2->o_min);
  kcg_equ = kcg_equ & (kcg_c1->o_nominal == kcg_c2->o_nominal);
  return kcg_equ;
}
#endif /* kcg_use_OdometryLocations_T_Obu_BasicTypes_Pkg */

#ifdef kcg_use_Brake_command_T_TIU_Types_Pkg
kcg_bool kcg_comp_Brake_command_T_TIU_Types_Pkg(
  Brake_command_T_TIU_Types_Pkg *kcg_c1,
  Brake_command_T_TIU_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->m_emergencybrake_cm ==
      kcg_c2->m_emergencybrake_cm);
  kcg_equ = kcg_equ & (kcg_c1->m_servicebrake_cm == kcg_c2->m_servicebrake_cm);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_Brake_command_T_TIU_Types_Pkg */

#ifdef kcg_use_TrainSpeedSegment_nanoStw
kcg_bool kcg_comp_TrainSpeedSegment_nanoStw(
  TrainSpeedSegment_nanoStw *kcg_c1,
  TrainSpeedSegment_nanoStw *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_TrackSection_nanoStw(
      &kcg_c1->first,
      &kcg_c2->first);
  kcg_equ = kcg_equ & (kcg_c1->speed == kcg_c2->speed);
  kcg_equ = kcg_equ & (kcg_c1->location == kcg_c2->location);
  return kcg_equ;
}
#endif /* kcg_use_TrainSpeedSegment_nanoStw */

#ifdef kcg_use_otherTrainPos_nanoStw
kcg_bool kcg_comp_otherTrainPos_nanoStw(
  otherTrainPos_nanoStw *kcg_c1,
  otherTrainPos_nanoStw *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->protectedLocationRear,
      &kcg_c2->protectedLocationRear);
  kcg_equ = kcg_equ & kcg_comp_TrackLocation_nanoStw(
      &kcg_c1->protectedLocationFront,
      &kcg_c2->protectedLocationFront);
  kcg_equ = kcg_equ & (kcg_c1->tid == kcg_c2->tid);
  return kcg_equ;
}
#endif /* kcg_use_otherTrainPos_nanoStw */

#ifdef kcg_use_av_Map_t_SDMConversionModelPkg
kcg_bool kcg_comp_av_Map_t_SDMConversionModelPkg(
  av_Map_t_SDMConversionModelPkg *kcg_c1,
  av_Map_t_SDMConversionModelPkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_v_BrakeSteps_t_SDMConversionModelPkg(
      &kcg_c1->v,
      &kcg_c2->v);
  kcg_equ = kcg_equ & kcg_comp_a_BrakeSteps_t_SDMConversionModelPkg(
      &kcg_c1->a,
      &kcg_c2->a);
  return kcg_equ;
}
#endif /* kcg_use_av_Map_t_SDMConversionModelPkg */

#ifdef kcg_use_nvkvint_T_Packet_Types_Pkg
kcg_bool kcg_comp_nvkvint_T_Packet_Types_Pkg(
  nvkvint_T_Packet_Types_Pkg *kcg_c1,
  nvkvint_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->m_nvkvint23 == kcg_c2->m_nvkvint23);
  kcg_equ = kcg_equ & (kcg_c1->m_nvkvint12 == kcg_c2->m_nvkvint12);
  kcg_equ = kcg_equ & (kcg_c1->v_nvkvint == kcg_c2->v_nvkvint);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_nvkvint_T_Packet_Types_Pkg */

#ifdef kcg_use_DMI_trackConditionElement_T_DMI_Types_Pkg
kcg_bool kcg_comp_DMI_trackConditionElement_T_DMI_Types_Pkg(
  DMI_trackConditionElement_T_DMI_Types_Pkg *kcg_c1,
  DMI_trackConditionElement_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->m_trackcond == kcg_c2->m_trackcond);
  kcg_equ = kcg_equ & (kcg_c1->d_trackcond == kcg_c2->d_trackcond);
  return kcg_equ;
}
#endif /* kcg_use_DMI_trackConditionElement_T_DMI_Types_Pkg */

#ifdef kcg_use_TrackSection_nanoStw
kcg_bool kcg_comp_TrackSection_nanoStw(
  TrackSection_nanoStw *kcg_c1,
  TrackSection_nanoStw *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->startloc == kcg_c2->startloc);
  kcg_equ = kcg_equ & (kcg_c1->length == kcg_c2->length);
  kcg_equ = kcg_equ & (kcg_c1->sid == kcg_c2->sid);
  return kcg_equ;
}
#endif /* kcg_use_TrackSection_nanoStw */

#ifdef kcg_use_MRSP_internal_section_T_TargetManagement_types
kcg_bool kcg_comp_MRSP_internal_section_T_TargetManagement_types(
  MRSP_internal_section_T_TargetManagement_types *kcg_c1,
  MRSP_internal_section_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  kcg_equ = kcg_equ & (kcg_c1->MRS == kcg_c2->MRS);
  kcg_equ = kcg_equ & (kcg_c1->Loc_Abs == kcg_c2->Loc_Abs);
  return kcg_equ;
}
#endif /* kcg_use_MRSP_internal_section_T_TargetManagement_types */

#ifdef kcg_use_TrainReport_nanoStw_trdp
kcg_bool kcg_comp_TrainReport_nanoStw_trdp(
  TrainReport_nanoStw_trdp *kcg_c1,
  TrainReport_nanoStw_trdp *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_TrainData_nanoStw_trdp(
      &kcg_c1->trainData,
      &kcg_c2->trainData);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  kcg_equ = kcg_equ & (kcg_c1->sid == kcg_c2->sid);
  return kcg_equ;
}
#endif /* kcg_use_TrainReport_nanoStw_trdp */

#ifdef kcg_use_EOA_real_T_TargetManagement_types
kcg_bool kcg_comp_EOA_real_T_TargetManagement_types(
  EOA_real_T_TargetManagement_types *kcg_c1,
  EOA_real_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->TargetSpeed == kcg_c2->TargetSpeed);
  kcg_equ = kcg_equ & (kcg_c1->Location == kcg_c2->Location);
  return kcg_equ;
}
#endif /* kcg_use_EOA_real_T_TargetManagement_types */

#ifdef kcg_use_a_Brake_t_SDMConversionModelPkg
kcg_bool kcg_comp_a_Brake_t_SDMConversionModelPkg(
  a_Brake_t_SDMConversionModelPkg *kcg_c1,
  a_Brake_t_SDMConversionModelPkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_av_Map_t_SDMConversionModelPkg(
      &kcg_c1->service,
      &kcg_c2->service);
  kcg_equ = kcg_equ & kcg_comp_av_Map_t_SDMConversionModelPkg(
      &kcg_c1->emergency,
      &kcg_c2->emergency);
  return kcg_equ;
}
#endif /* kcg_use_a_Brake_t_SDMConversionModelPkg */

#ifdef kcg_use_InterventionCommand_SpeedSupervision_UnitTest_Pkg
kcg_bool kcg_comp_InterventionCommand_SpeedSupervision_UnitTest_Pkg(
  InterventionCommand_SpeedSupervision_UnitTest_Pkg *kcg_c1,
  InterventionCommand_SpeedSupervision_UnitTest_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->tco == kcg_c2->tco);
  kcg_equ = kcg_equ & (kcg_c1->sb == kcg_c2->sb);
  kcg_equ = kcg_equ & (kcg_c1->eb == kcg_c2->eb);
  kcg_equ = kcg_equ & (kcg_c1->interventionSpeed == kcg_c2->interventionSpeed);
  kcg_equ = kcg_equ & (kcg_c1->permittedSpeed == kcg_c2->permittedSpeed);
  kcg_equ = kcg_equ & (kcg_c1->targetDistance == kcg_c2->targetDistance);
  return kcg_equ;
}
#endif /* kcg_use_InterventionCommand_SpeedSupervision_UnitTest_Pkg */

#ifdef kcg_use_Target_real_T_TargetManagement_types
kcg_bool kcg_comp_Target_real_T_TargetManagement_types(
  Target_real_T_TargetManagement_types *kcg_c1,
  Target_real_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->speed == kcg_c2->speed);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  kcg_equ = kcg_equ & (kcg_c1->targetType == kcg_c2->targetType);
  return kcg_equ;
}
#endif /* kcg_use_Target_real_T_TargetManagement_types */

#ifdef kcg_use_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI
kcg_bool kcg_comp_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
  MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI *kcg_c1,
  MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_DMI_SpeedProfileArray_T_DMI_Types_Pkg(
      &kcg_c1->MRSP,
      &kcg_c2->MRSP);
  kcg_equ = kcg_equ & (kcg_c1->write_index == kcg_c2->write_index);
  return kcg_equ;
}
#endif /* kcg_use_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI */

#ifdef kcg_use_DMI_speedProfile_T_DMI_Types_Pkg
kcg_bool kcg_comp_DMI_speedProfile_T_DMI_Types_Pkg(
  DMI_speedProfile_T_DMI_Types_Pkg *kcg_c1,
  DMI_speedProfile_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_DMI_SpeedProfileArray_T_DMI_Types_Pkg(
      &kcg_c1->speedProfiles,
      &kcg_c2->speedProfiles);
  kcg_equ = kcg_equ & (kcg_c1->profileChanged == kcg_c2->profileChanged);
  return kcg_equ;
}
#endif /* kcg_use_DMI_speedProfile_T_DMI_Types_Pkg */

#ifdef kcg_use_DMI_gradientProfile_T_DMI_Types_Pkg
kcg_bool kcg_comp_DMI_gradientProfile_T_DMI_Types_Pkg(
  DMI_gradientProfile_T_DMI_Types_Pkg *kcg_c1,
  DMI_gradientProfile_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_DMI_gradientProfileArray_T_DMI_Types_Pkg(
      &kcg_c1->gradientProfiles,
      &kcg_c2->gradientProfiles);
  kcg_equ = kcg_equ & (kcg_c1->profileChanged == kcg_c2->profileChanged);
  return kcg_equ;
}
#endif /* kcg_use_DMI_gradientProfile_T_DMI_Types_Pkg */

#ifdef kcg_use_DMI_trackCondition_T_DMI_Types_Pkg
kcg_bool kcg_comp_DMI_trackCondition_T_DMI_Types_Pkg(
  DMI_trackCondition_T_DMI_Types_Pkg *kcg_c1,
  DMI_trackCondition_T_DMI_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_DMI_trackConditionArray_T_DMI_Types_Pkg(
      &kcg_c1->trackCondition,
      &kcg_c2->trackCondition);
  kcg_equ = kcg_equ & (kcg_c1->nIter == kcg_c2->nIter);
  return kcg_equ;
}
#endif /* kcg_use_DMI_trackCondition_T_DMI_Types_Pkg */

#ifdef kcg_use_ParabolaCurve_T_CalcBrakingCurves_types
kcg_bool kcg_comp_ParabolaCurve_T_CalcBrakingCurves_types(
  ParabolaCurve_T_CalcBrakingCurves_types *kcg_c1,
  ParabolaCurve_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_ParabolaCurveValid_T_CalcBrakingCurves_types(
      &kcg_c1->valid,
      &kcg_c2->valid);
  kcg_equ = kcg_equ &
    kcg_comp_ParabolaCurveAccelerations_T_CalcBrakingCurves_types(
      &kcg_c1->accelerations,
      &kcg_c2->accelerations);
  kcg_equ = kcg_equ & kcg_comp_ParabolaCurveSpeeds_T_CalcBrakingCurves_types(
      &kcg_c1->speeds,
      &kcg_c2->speeds);
  kcg_equ = kcg_equ & kcg_comp_ParabolaCurveDistances_T_CalcBrakingCurves_types(
      &kcg_c1->distances,
      &kcg_c2->distances);
  return kcg_equ;
}
#endif /* kcg_use_ParabolaCurve_T_CalcBrakingCurves_types */

#ifdef kcg_use_nvkrint_T_Packet_Types_Pkg
kcg_bool kcg_comp_nvkrint_T_Packet_Types_Pkg(
  nvkrint_T_Packet_Types_Pkg *kcg_c1,
  nvkrint_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->m_nvkrint == kcg_c2->m_nvkrint);
  kcg_equ = kcg_equ & (kcg_c1->l_nvkrint == kcg_c2->l_nvkrint);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_nvkrint_T_Packet_Types_Pkg */

#ifdef kcg_use_TrackKnot_nanoStw
kcg_bool kcg_comp_TrackKnot_nanoStw(
  TrackKnot_nanoStw *kcg_c1,
  TrackKnot_nanoStw *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_array_21057(&kcg_c1->line, &kcg_c2->line);
  kcg_equ = kcg_equ & (kcg_c1->vTurn == kcg_c2->vTurn);
  kcg_equ = kcg_equ & (kcg_c1->vStraight == kcg_c2->vStraight);
  kcg_equ = kcg_equ & (kcg_c1->l == kcg_c2->l);
  kcg_equ = kcg_equ & (kcg_c1->sid == kcg_c2->sid);
  return kcg_equ;
}
#endif /* kcg_use_TrackKnot_nanoStw */

#ifdef kcg_use_ACC_SDM_GradientAcceleration_Pkg
kcg_bool kcg_comp_ACC_SDM_GradientAcceleration_Pkg(
  ACC_SDM_GradientAcceleration_Pkg *kcg_c1,
  ACC_SDM_GradientAcceleration_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ &
    kcg_comp_GradientProfile_real_compensated_t_SDM_GradientAcceleration_types(
      &kcg_c1->compensatedGradientProfile,
      &kcg_c2->compensatedGradientProfile);
  kcg_equ = kcg_equ & (kcg_c1->rearIndex == kcg_c2->rearIndex);
  kcg_equ = kcg_equ & (kcg_c1->rearPos == kcg_c2->rearPos);
  kcg_equ = kcg_equ & (kcg_c1->frontIndex == kcg_c2->frontIndex);
  kcg_equ = kcg_equ & (kcg_c1->frontPos == kcg_c2->frontPos);
  return kcg_equ;
}
#endif /* kcg_use_ACC_SDM_GradientAcceleration_Pkg */

#ifdef kcg_use_nvkvintset_T_Packet_Types_Pkg
kcg_bool kcg_comp_nvkvintset_T_Packet_Types_Pkg(
  nvkvintset_T_Packet_Types_Pkg *kcg_c1,
  nvkvintset_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_nvkvintArray_T_Packet_Types_Pkg(
      &kcg_c1->nvkintArray,
      &kcg_c2->nvkintArray);
  kcg_equ = kcg_equ & (kcg_c1->a_nvp23 == kcg_c2->a_nvp23);
  kcg_equ = kcg_equ & (kcg_c1->a_nvp12 == kcg_c2->a_nvp12);
  kcg_equ = kcg_equ & (kcg_c1->q_nvkvintset == kcg_c2->q_nvkvintset);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_nvkvintset_T_Packet_Types_Pkg */

#ifdef kcg_use_infoFromLinking_T_TrainPosition_Types_Pck
kcg_bool kcg_comp_infoFromLinking_T_TrainPosition_Types_Pck(
  infoFromLinking_T_TrainPosition_Types_Pck *kcg_c1,
  infoFromLinking_T_TrainPosition_Types_Pck *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_LinkedBG_T_BG_Types_Pkg(
      &kcg_c1->linkingInfo,
      &kcg_c2->linkingInfo);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->d_link,
      &kcg_c2->d_link);
  kcg_equ = kcg_equ & kcg_comp_LocWithInAcc_T_Obu_BasicTypes_Pkg(
      &kcg_c1->expectedLocation,
      &kcg_c2->expectedLocation);
  kcg_equ = kcg_equ & (kcg_c1->nid_c_fromLinkingBG ==
      kcg_c2->nid_c_fromLinkingBG);
  kcg_equ = kcg_equ & (kcg_c1->nid_bg_fromLinkingBG ==
      kcg_c2->nid_bg_fromLinkingBG);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_infoFromLinking_T_TrainPosition_Types_Pck */

#ifdef kcg_use_TrackAtlasData_SpeedSupervision_UnitTest_Pkg
kcg_bool kcg_comp_TrackAtlasData_SpeedSupervision_UnitTest_Pkg(
  TrackAtlasData_SpeedSupervision_UnitTest_Pkg *kcg_c1,
  TrackAtlasData_SpeedSupervision_UnitTest_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->fresh == kcg_c2->fresh);
  kcg_equ = kcg_equ & (kcg_c1->vRelease == kcg_c2->vRelease);
  kcg_equ = kcg_equ & (kcg_c1->EoA == kcg_c2->EoA);
  return kcg_equ;
}
#endif /* kcg_use_TrackAtlasData_SpeedSupervision_UnitTest_Pkg */

#ifdef kcg_use_TrackSection_SpeedSupervision_UnitTest_Pkg
kcg_bool kcg_comp_TrackSection_SpeedSupervision_UnitTest_Pkg(
  TrackSection_SpeedSupervision_UnitTest_Pkg *kcg_c1,
  TrackSection_SpeedSupervision_UnitTest_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->speed == kcg_c2->speed);
  kcg_equ = kcg_equ & (kcg_c1->length == kcg_c2->length);
  return kcg_equ;
}
#endif /* kcg_use_TrackSection_SpeedSupervision_UnitTest_Pkg */

#ifdef kcg_use_allPR_nanoSim
kcg_bool kcg_comp_allPR_nanoSim(allPR_nanoSim *kcg_c1, allPR_nanoSim *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_array_21308(&kcg_c1->PR, &kcg_c2->PR);
  kcg_equ = kcg_equ & (kcg_c1->sizeTrains == kcg_c2->sizeTrains);
  return kcg_equ;
}
#endif /* kcg_use_allPR_nanoSim */

#ifdef kcg_use_Target_T_TargetManagement_types
kcg_bool kcg_comp_Target_T_TargetManagement_types(
  Target_T_TargetManagement_types *kcg_c1,
  Target_T_TargetManagement_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  kcg_equ = kcg_equ & (kcg_c1->speed == kcg_c2->speed);
  kcg_equ = kcg_equ & (kcg_c1->distance == kcg_c2->distance);
  kcg_equ = kcg_equ & (kcg_c1->targetType == kcg_c2->targetType);
  return kcg_equ;
}
#endif /* kcg_use_Target_T_TargetManagement_types */

#ifdef kcg_use_TrainLocations_real_T_SDM_Types_Pkg
kcg_bool kcg_comp_TrainLocations_real_T_SDM_Types_Pkg(
  TrainLocations_real_T_SDM_Types_Pkg *kcg_c1,
  TrainLocations_real_T_SDM_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->d_accLRBG == kcg_c2->d_accLRBG);
  kcg_equ = kcg_equ & (kcg_c1->d_LRBG == kcg_c2->d_LRBG);
  kcg_equ = kcg_equ & (kcg_c1->d_maxSafeFrontEndPos ==
      kcg_c2->d_maxSafeFrontEndPos);
  kcg_equ = kcg_equ & (kcg_c1->d_minSafeFrontEndPos ==
      kcg_c2->d_minSafeFrontEndPos);
  kcg_equ = kcg_equ & (kcg_c1->d_est_frontendPos == kcg_c2->d_est_frontendPos);
  kcg_equ = kcg_equ & (kcg_c1->trainPositionIsValid ==
      kcg_c2->trainPositionIsValid);
  return kcg_equ;
}
#endif /* kcg_use_TrainLocations_real_T_SDM_Types_Pkg */

#ifdef kcg_use_CurveCollection_T_CalcBrakingCurves_types
kcg_bool kcg_comp_CurveCollection_T_CalcBrakingCurves_types(
  CurveCollection_T_CalcBrakingCurves_types *kcg_c1,
  CurveCollection_T_CalcBrakingCurves_types *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->GUI_curve_enabled == kcg_c2->GUI_curve_enabled);
  kcg_equ = kcg_equ & kcg_comp_ParabolaCurve_T_CalcBrakingCurves_types(
      &kcg_c1->GUI_curve,
      &kcg_c2->GUI_curve);
  kcg_equ = kcg_equ & kcg_comp_ParabolaCurve_T_CalcBrakingCurves_types(
      &kcg_c1->SvL_LoA_EBD_curve,
      &kcg_c2->SvL_LoA_EBD_curve);
  kcg_equ = kcg_equ & kcg_comp_ParabolaCurve_T_CalcBrakingCurves_types(
      &kcg_c1->EOA_SBD_curve,
      &kcg_c2->EOA_SBD_curve);
  kcg_equ = kcg_equ &
    kcg_comp_ParabolaCurve_list_MRSP_T_CalcBrakingCurves_types(
      &kcg_c1->MRSP_EBD_curves,
      &kcg_c2->MRSP_EBD_curves);
  return kcg_equ;
}
#endif /* kcg_use_CurveCollection_T_CalcBrakingCurves_types */

#ifdef kcg_use_TrackPlan_nanoSim
kcg_bool kcg_comp_TrackPlan_nanoSim(
  TrackPlan_nanoSim *kcg_c1,
  TrackPlan_nanoSim *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_array_21324(&kcg_c1->tracks, &kcg_c2->tracks);
  kcg_equ = kcg_equ & (kcg_c1->sizeTracks == kcg_c2->sizeTracks);
  return kcg_equ;
}
#endif /* kcg_use_TrackPlan_nanoSim */

#ifdef kcg_use_nidC_T_Packet_Types_Pkg
kcg_bool kcg_comp_nidC_T_Packet_Types_Pkg(
  nidC_T_Packet_Types_Pkg *kcg_c1,
  nidC_T_Packet_Types_Pkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & (kcg_c1->nid_c == kcg_c2->nid_c);
  kcg_equ = kcg_equ & (kcg_c1->valid == kcg_c2->valid);
  return kcg_equ;
}
#endif /* kcg_use_nidC_T_Packet_Types_Pkg */

#ifdef kcg_use_mv_Map_t_SDMConversionModelPkg
kcg_bool kcg_comp_mv_Map_t_SDMConversionModelPkg(
  mv_Map_t_SDMConversionModelPkg *kcg_c1,
  mv_Map_t_SDMConversionModelPkg *kcg_c2)
{
  kcg_bool kcg_equ;
  
  kcg_equ = kcg_true;
  kcg_equ = kcg_equ & kcg_comp_v_KvSteps_t_SDMConversionModelPkg(
      &kcg_c1->v,
      &kcg_c2->v);
  kcg_equ = kcg_equ & kcg_comp_m_KvSteps_t_SDMConversionModelPkg(
      &kcg_c1->m,
      &kcg_c2->m);
  return kcg_equ;
}
#endif /* kcg_use_mv_Map_t_SDMConversionModelPkg */

/* $**************** KCG Version 6.4 (build i21) ****************
** kcg_types.c
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

