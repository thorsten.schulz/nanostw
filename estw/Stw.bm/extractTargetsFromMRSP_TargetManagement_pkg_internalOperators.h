/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _extractTargetsFromMRSP_TargetManagement_pkg_internalOperators_H_
#define _extractTargetsFromMRSP_TargetManagement_pkg_internalOperators_H_

#include "kcg_types.h"
#include "extractTargetsFromMRSPInt_TargetManagement_pkg_internalOperators.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* TargetManagement_pkg::internalOperators::extractTargetsFromMRSP */
extern void extractTargetsFromMRSP_TargetManagement_pkg_internalOperators(
  /* TargetManagement_pkg::internalOperators::extractTargetsFromMRSP::MRSP */ MRSP_internal_T_TargetManagement_types *MRSP,
  /* TargetManagement_pkg::internalOperators::extractTargetsFromMRSP::TargetsOfMRSP */ extractTargetsMRSPACC_TargetManagement_pkg *TargetsOfMRSP);

#endif /* _extractTargetsFromMRSP_TargetManagement_pkg_internalOperators_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** extractTargetsFromMRSP_TargetManagement_pkg_internalOperators.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

