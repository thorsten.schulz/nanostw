/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35_H_
#define _MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction */
extern void MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::i */ kcg_int i,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::acc */ MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI *acc,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::MRSP */ MRSP_Profile_t_TrackAtlasTypes *MRSP,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::cont */ kcg_bool *cont,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_reduction::reduced */ MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI *reduced);

#endif /* _MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

