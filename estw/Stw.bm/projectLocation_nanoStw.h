/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _projectLocation_nanoStw_H_
#define _projectLocation_nanoStw_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::projectLocation */
extern void projectLocation_nanoStw(
  /* nanoStw::projectLocation::to */ LengthUnit to,
  /* nanoStw::projectLocation::base */ TrackLocation_nanoStw *base,
  /* nanoStw::projectLocation::runsCW */ kcg_bool runsCW,
  /* nanoStw::projectLocation::projected */ TrackLocation_nanoStw *projected);

#endif /* _projectLocation_nanoStw_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** projectLocation_nanoStw.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

