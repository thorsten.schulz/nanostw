/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _mapEStwSections_SpeedSupervision_UnitTest_Pkg_H_
#define _mapEStwSections_SpeedSupervision_UnitTest_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SpeedSupervision_UnitTest_Pkg::mapEStwSections */
extern void mapEStwSections_SpeedSupervision_UnitTest_Pkg(
  /* SpeedSupervision_UnitTest_Pkg::mapEStwSections::sect */ TrackSection_SpeedSupervision_UnitTest_Pkg *sect,
  /* SpeedSupervision_UnitTest_Pkg::mapEStwSections::Profile */ MRSP_section_t_TrackAtlasTypes *Profile);

#endif /* _mapEStwSections_SpeedSupervision_UnitTest_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** mapEStwSections_SpeedSupervision_UnitTest_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

