/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _InflateABrakeSafe_SDMConversionModelPkg_H_
#define _InflateABrakeSafe_SDMConversionModelPkg_H_

#include "kcg_types.h"
#include "InflateABrakeSpeeds_SDMConversionModelPkg.h"
#include "InflateABrakeRow_SDMConversionModelPkg.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDMConversionModelPkg::InflateABrakeSafe */
extern void InflateABrakeSafe_SDMConversionModelPkg(
  /* SDMConversionModelPkg::InflateABrakeSafe::aBrakeSafe_cm */ av_MergedMap_t_SDMConversionModelPkg *aBrakeSafe_cm,
  /* SDMConversionModelPkg::InflateABrakeSafe::aBrakeSafe */ ASafe_T_CalcBrakingCurves_types *aBrakeSafe);

#endif /* _InflateABrakeSafe_SDMConversionModelPkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** InflateABrakeSafe_SDMConversionModelPkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

