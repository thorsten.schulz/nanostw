/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "newOwnPosition_nanoStw_38.h"

/* nanoStw::newOwnPosition */
void newOwnPosition_nanoStw_38(
  /* nanoStw::newOwnPosition::data */ TrainReport_nanoStw_trdp *data,
  /* nanoStw::newOwnPosition::tracks */ array_21324 *tracks,
  /* nanoStw::newOwnPosition::validatedTD */ TrainData_SpeedSupervision_UnitTest_Pkg *validatedTD,
  /* nanoStw::newOwnPosition::position */ simpleTrainPos_nanoStw *position,
  /* nanoStw::newOwnPosition::correctionPoint */ TrackLocation_nanoStw *correctionPoint)
{
  /* nanoStw::newOwnPosition */ TrackLocation_nanoStw acc1;
  /* nanoStw::newOwnPosition */ kcg_int tmp;
  /* nanoStw::newOwnPosition */ TrackLocation_nanoStw acc;
  kcg_int i;
  /* nanoStw::moveLocationToLastBlunt::previousBluntIsClose */ kcg_bool previousBluntIsClose_1;
  /* nanoStw::moveLocationToLastBlunt::goingWithTracksDirection */ kcg_bool goingWithTracksDirection_1;
  /* nanoStw::moveLocationToLastBlunt::previousTrack */ TrackKnot_nanoStw previousTrack_1;
  /* nanoStw::moveLocationToLastBlunt::seenTrack */ TrackKnot_nanoStw seenTrack_1;
  /* nanoStw::moveLocationToLastBlunt::IfBlock1 */ kcg_bool IfBlock1_clock_1;
  kcg_bool noname_1;
  kcg_bool _2_noname_1;
  /* nanoStw::moveLocationToLastBlunt::IfBlock1::else */ kcg_bool else_clock_1_IfBlock1;
  /* nanoStw::moveLocation::_L6 */ kcg_bool _L6_1;
  /* nanoStw::newOwnPosition::newPos */ simpleTrainPos_nanoStw newPos;
  /* nanoStw::newOwnPosition::reportedLocation */ TrackLocation_nanoStw reportedLocation;
  /* nanoStw::newOwnPosition::normalizedAnt */ TrackLocation_nanoStw normalizedAnt;
  /* nanoStw::newOwnPosition::_L360 */ LengthUnit _L360;
  /* nanoStw::newOwnPosition::_L382 */ TrackLocation_nanoStw _L382;
  
  newPos.velocity = (*data).trainData.velocity;
  newPos.acc = (*data).trainData.acc;
  kcg_copy_StatusWord_SpeedSupervision_UnitTest_Pkg(
    &newPos.flag,
    &(*data).trainData.flag);
  newPos.BrPc = (*data).trainData.BrPc;
  newPos.vMax = (*data).trainData.vMax;
  newPos.vRelease = (*data).trainData.vRelease;
  newPos.vOnSight = (*data).trainData.vOnSight;
  newPos.length = (*data).trainData.length;
  newPos.Ant2Cab = (*data).trainData.Ant2Cab;
  newPos.TrainId = (*data).trainData.TrainId;
  reportedLocation.sid = (*data).sid;
  reportedLocation.distance = (*data).distance;
  reportedLocation.location = (*data).trainData.location;
  goingWithTracksDirection_1 = ((*data).trainData.TrainId !=
      invalidTid_nanoStw) & ((*data).trainData.BrPc > 0);
  kcg_copy_TrackLocation_nanoStw(&normalizedAnt, &reportedLocation);
  /* 1_2 */ if (goingWithTracksDirection_1) {
    /* 1_2 */ for (i = 0; i < 20; i++) {
      kcg_copy_TrackLocation_nanoStw(&_L382, &normalizedAnt);
      /* 1_1 */
      normalizeTrackLocation_it_nanoStw_38(
        &_L382,
        tracks,
        &_L6_1,
        &normalizedAnt);
      /* 1_2 */ if (!_L6_1) {
        break;
      }
    }
  }
  kcg_copy_TrackLocation_nanoStw(&newPos.antenna, &normalizedAnt);
  _L382.sid = normalizedAnt.sid;
  newPos.frontEnd.sid = normalizedAnt.sid;
  newPos.frontEnd.location = (*data).trainData.Ant2Cab + normalizedAnt.location;
  _L6_1 = normalizedAnt.sid < 0;
  /* 1 */ if ((*data).trainData.flag.CW != _L6_1) {
    tmp = - (*data).trainData.Ant2Cab;
  }
  else {
    tmp = (*data).trainData.Ant2Cab;
  }
  newPos.frontEnd.distance = tmp + normalizedAnt.distance;
  _L360 = (*data).trainData.Ant2Cab - (*data).trainData.length;
  _L382.location = _L360 + normalizedAnt.location;
  /* 1 */ if ((*data).trainData.flag.CW != _L6_1) {
    tmp = - _L360;
  }
  else {
    tmp = _L360;
  }
  _L382.distance = tmp + normalizedAnt.distance;
  /* 4_2 */ if (goingWithTracksDirection_1) {
    /* 2_2 */ for (i = 0; i < 20; i++) {
      kcg_copy_TrackLocation_nanoStw(&acc1, &newPos.frontEnd);
      /* 2_1 */
      normalizeTrackLocation_it_nanoStw_38(
        &acc1,
        tracks,
        &previousBluntIsClose_1,
        &newPos.frontEnd);
      /* 2_2 */ if (!previousBluntIsClose_1) {
        break;
      }
    }
    /* 4_2 */ for (i = 0; i < 20; i++) {
      kcg_copy_TrackLocation_nanoStw(&acc, &_L382);
      /* 4_1 */
      normalizeTrackLocation_it_nanoStw_38(&acc, tracks, &_L6_1, &_L382);
      /* 4_2 */ if (!_L6_1) {
        break;
      }
    }
  }
  kcg_copy_TrackLocation_nanoStw(&newPos.rearEnd, &_L382);
  _L6_1 = (38 >= newPos.antenna.sid) & (newPos.antenna.sid >= - 38) & ((20000 >=
        newPos.velocity) & (newPos.velocity >= 0)) & ((500 >= newPos.acc) &
      (newPos.acc >= - 500)) & ((250 >= newPos.BrPc) & (newPos.BrPc >= 30)) &
    ((newPos.vMax >= newPos.vRelease) & (newPos.vRelease >= 20)) &
    ((newPos.vMax >= newPos.vOnSight) & (newPos.vOnSight >= 20)) & ((500 >=
        newPos.vMax) & (newPos.vMax >= 20)) & ((newPos.length >=
        newPos.Ant2Cab) & (newPos.Ant2Cab >= 0)) & ((200000 >= newPos.length) &
      (newPos.length >= 200)) & ((999999 >= newPos.antenna.distance) &
      (newPos.antenna.distance >= - 999999)) & (newPos.TrainId !=
      invalidTid_nanoStw);
  /* ck_valid */ if (_L6_1) {
    kcg_copy_TrainData_SpeedSupervision_UnitTest_Pkg(
      validatedTD,
      &(*data).trainData);
    kcg_copy_simpleTrainPos_nanoStw(position, &newPos);
    /* 1_1 */
    selectTrackBySid_nanoStw_38(
      normalizedAnt.sid,
      tracks,
      &seenTrack_1,
      &_2_noname_1);
    /* 1_1 */
    lastTrackKnot_nanoStw_38(
      &seenTrack_1,
      (*data).trainData.flag.CW,
      tracks,
      &previousTrack_1,
      &noname_1,
      &previousBluntIsClose_1,
      &goingWithTracksDirection_1);
    IfBlock1_clock_1 = goingWithTracksDirection_1 & (reportedLocation.sid !=
        normalizedAnt.sid);
    /* 1_ck_IfBlock1 */ if (IfBlock1_clock_1) {
      (*correctionPoint).sid = normalizedAnt.sid;
      (*correctionPoint).distance = 0;
      (*correctionPoint).location = normalizedAnt.location -
        normalizedAnt.distance;
    }
    else {
      else_clock_1_IfBlock1 = !goingWithTracksDirection_1 &
        (reportedLocation.sid != previousTrack_1.sid);
      /* 1_ck_anon_activ */ if (else_clock_1_IfBlock1) {
        (*correctionPoint).sid = previousTrack_1.sid;
        (*correctionPoint).distance = 0;
        /* 2 */ if (previousBluntIsClose_1) {
          tmp = 0;
        }
        else {
          tmp = previousTrack_1.l;
        }
        (*correctionPoint).location = normalizedAnt.location - (tmp +
            (seenTrack_1.l - normalizedAnt.distance));
      }
      else {
        kcg_copy_TrackLocation_nanoStw(
          correctionPoint,
          (TrackLocation_nanoStw *) &invalidLocation_nanoStw);
      }
    }
  }
  else {
    kcg_copy_TrainData_SpeedSupervision_UnitTest_Pkg(
      validatedTD,
      (TrainData_SpeedSupervision_UnitTest_Pkg *) &emptyTrainData_nanoStw);
    (*validatedTD).flag.tripped = kcg_true;
    kcg_copy_simpleTrainPos_nanoStw(
      position,
      (simpleTrainPos_nanoStw *) &emptySimplePos_nanoStw);
    (*position).flag.tripped = kcg_true;
    (*position).TrainId = invalidTid_nanoStw;
    kcg_copy_TrackLocation_nanoStw(
      correctionPoint,
      (TrackLocation_nanoStw *) &invalidLocation_nanoStw);
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** newOwnPosition_nanoStw_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

