/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "normalizeTrackLocation_it_nanoStw_38.h"

/* nanoStw::normalizeTrackLocation_it */
void normalizeTrackLocation_it_nanoStw_38(
  /* nanoStw::normalizeTrackLocation_it::loc */ TrackLocation_nanoStw *loc,
  /* nanoStw::normalizeTrackLocation_it::tracks */ array_21324 *tracks,
  /* nanoStw::normalizeTrackLocation_it::again */ kcg_bool *again,
  /* nanoStw::normalizeTrackLocation_it::nloc */ TrackLocation_nanoStw *nloc)
{
  /* nanoStw::normalizeTrackLocation_it */ kcg_bool tmp;
  /* nanoStw::normalizeTrackLocation_it */ kcg_bool op_call;
  /* nanoStw::normalizeTrackLocation_it::jumpedToNext */ kcg_bool jumpedToNext;
  /* nanoStw::normalizeTrackLocation_it::seenTrack */ TrackKnot_nanoStw seenTrack;
  /* nanoStw::normalizeTrackLocation_it::nextTrack */ TrackKnot_nanoStw nextTrack;
  /* nanoStw::normalizeTrackLocation_it::overlap */ LengthUnit overlap;
  /* nanoStw::normalizeTrackLocation_it::_L12 */ kcg_bool _L12;
  
  /* 1 */ selectTrackBySid_nanoStw_38((*loc).sid, tracks, &seenTrack, &op_call);
  tmp = (*loc).distance < 0;
  _L12 = op_call & ((seenTrack.l <= (*loc).distance) | tmp);
  /* ck__L12 */ if (_L12) {
    /* 1 */
    nextTrackKnot_nanoStw_38(
      &seenTrack,
      (kcg_bool) (tmp != (seenTrack.sid < UnknownSid_nanoStw)),
      tracks,
      &nextTrack,
      &jumpedToNext,
      &op_call);
  }
  else {
    kcg_copy_TrackKnot_nanoStw(&nextTrack, &seenTrack);
  }
  jumpedToNext = _L12 & (nextTrack.sid != UnknownSid_nanoStw);
  /* 2 */ if (tmp) {
    overlap = - (*loc).distance;
  }
  else {
    overlap = (*loc).distance - seenTrack.l;
  }
  kcg_copy_TrackLocation_nanoStw(nloc, loc);
  (*nloc).sid = nextTrack.sid;
  /* 1 */ if (jumpedToNext) {
    /* ck__L12 */ if (_L12) {
      tmp = op_call;
    }
    else {
      tmp = kcg_false;
    }
    /* 3 */ if (tmp) {
      (*nloc).distance = overlap;
    }
    else {
      (*nloc).distance = nextTrack.l - overlap;
    }
  }
  else {
    (*nloc).distance = (*loc).distance;
  }
  *again = jumpedToNext & (overlap >= nextTrack.l);
}

/* $**************** KCG Version 6.4 (build i21) ****************
** normalizeTrackLocation_it_nanoStw_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

