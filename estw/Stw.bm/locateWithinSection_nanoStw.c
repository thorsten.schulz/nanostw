/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "locateWithinSection_nanoStw.h"

/* nanoStw::locateWithinSection */
kcg_int locateWithinSection_nanoStw(
  /* nanoStw::locateWithinSection::section */ TrackSection_nanoStw *section,
  /* nanoStw::locateWithinSection::location */ LengthUnit location,
  /* nanoStw::locateWithinSection::runsCW */ kcg_bool runsCW)
{
  /* nanoStw::locateWithinSection::_L11 */ kcg_int _L11;
  /* nanoStw::locateWithinSection::_L12 */ kcg_bool _L12;
  /* nanoStw::locateWithinSection::_L16 */ kcg_int _L16;
  /* nanoStw::locateWithinSection::rloc */ kcg_int rloc;
  
  _L11 = location - (*section).startloc;
  _L12 = _L11 >= 0;
  /* 1 */ if ((_L11 <= (*section).length) & _L12) {
    _L16 = rlocScale_nanoStw * _L11 / (*section).length;
  }
  else /* 2 */ if (_L12) {
    _L16 = rlocScale_nanoStw;
  }
  else {
    _L16 = 0;
  }
  /* 3 */ if (((*section).sid < 0) ^ runsCW) {
    rloc = rlocScale_nanoStw - _L16;
  }
  else {
    rloc = _L16;
  }
  return rloc;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** locateWithinSection_nanoStw.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

