/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _isLocationNormalized_nanoStw_38_H_
#define _isLocationNormalized_nanoStw_38_H_

#include "kcg_types.h"
#include "selectTrackBySid_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::isLocationNormalized */
extern kcg_bool isLocationNormalized_nanoStw_38(
  /* nanoStw::isLocationNormalized::loc */ TrackLocation_nanoStw *loc,
  /* nanoStw::isLocationNormalized::tracks */ array_21324 *tracks);

#endif /* _isLocationNormalized_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** isLocationNormalized_nanoStw_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

