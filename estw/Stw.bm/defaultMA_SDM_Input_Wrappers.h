/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _defaultMA_SDM_Input_Wrappers_H_
#define _defaultMA_SDM_Input_Wrappers_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDM_Input_Wrappers::defaultMA */
extern void defaultMA_SDM_Input_Wrappers(
  /* SDM_Input_Wrappers::defaultMA::Ma_out */ MA_section_real_T_TargetManagement_types *Ma_out);

#endif /* _defaultMA_SDM_Input_Wrappers_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** defaultMA_SDM_Input_Wrappers.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

