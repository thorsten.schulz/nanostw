/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _nextTrackKnot_nanoStw_38_H_
#define _nextTrackKnot_nanoStw_38_H_

#include "kcg_types.h"
#include "selectTrackBySid_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::nextTrackKnot */
extern void nextTrackKnot_nanoStw_38(
  /* nanoStw::nextTrackKnot::track */ TrackKnot_nanoStw *track,
  /* nanoStw::nextTrackKnot::goCW */ kcg_bool goCW,
  /* nanoStw::nextTrackKnot::tracks */ array_21324 *tracks,
  /* nanoStw::nextTrackKnot::ntrack */ TrackKnot_nanoStw *ntrack,
  /* nanoStw::nextTrackKnot::isOn */ kcg_bool *isOn,
  /* nanoStw::nextTrackKnot::isNLineBlunt */ kcg_bool *isNLineBlunt);

#endif /* _nextTrackKnot_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** nextTrackKnot_nanoStw_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

