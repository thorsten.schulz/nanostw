/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "DefaultNationalValues_SpeedSupervision_UnitTest_Pkg.h"

/* SpeedSupervision_UnitTest_Pkg::DefaultNationalValues */
void DefaultNationalValues_SpeedSupervision_UnitTest_Pkg(
  /* SpeedSupervision_UnitTest_Pkg::DefaultNationalValues::__ */ P3_NationalValues_T_Packet_Types_Pkg *__)
{
  kcg_copy_P3_NationalValues_T_Packet_Types_Pkg(
    __,
    (P3_NationalValues_T_Packet_Types_Pkg *)
      &NVnull_SpeedSupervision_UnitTest_Pkg);
  (*__).valid = kcg_true;
  (*__).q_nvemrrls = Q_NVEMRRLS_Revoke_emergency_brake_command_at_standstill;
  (*__).m_nvktint = 0.2;
  (*__).q_nvsbtsmperm = Q_NVSBTSMPERM_Yes;
  (*__).q_nvinhsmicperm = Q_NVINHSMICPERM_No;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** DefaultNationalValues_SpeedSupervision_UnitTest_Pkg.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

