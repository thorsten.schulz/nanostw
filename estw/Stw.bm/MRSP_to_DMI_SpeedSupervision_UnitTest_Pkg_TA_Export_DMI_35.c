/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.h"

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI */
void MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::fresh */ kcg_bool fresh,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::MRSP_in */ MRSP_Profile_t_TrackAtlasTypes *MRSP_in,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::MRSP_to_DMI */ DMI_speedProfile_T_DMI_Types_Pkg *MRSP_to_DMI)
{
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI */ MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI acc;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI */ kcg_bool cond_iterw;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI */ DMI_SpeedProfileArray_T_DMI_Types_Pkg tmp;
  kcg_int i;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_to_DMI::_L665 */ MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI _L665;
  
  (*MRSP_to_DMI).profileChanged = fresh;
  kcg_copy_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
    &_L665,
    (MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI *)
      &DEFAULT_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI);
  /* 1 */ for (i = 0; i < 32; i++) {
    kcg_copy_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
      &acc,
      &_L665);
    /* 1 */
    MRSP_reduction_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35(
      i,
      &acc,
      MRSP_in,
      &cond_iterw,
      &_L665);
    /* 1 */ if (!cond_iterw) {
      break;
    }
  }
  /* 2 */ for (i = 0; i < 32; i++) {
    /* 1 */
    MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
      i,
      &_L665.MRSP,
      EoA,
      &tmp[i]);
  }
  kcg_copy_DMI_SpeedProfileArray_T_DMI_Types_Pkg(
    &(*MRSP_to_DMI).speedProfiles,
    &tmp);
}

/* $**************** KCG Version 6.4 (build i21) ****************
** MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

