/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg.h"

/* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition */
void CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg(
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::PR */ TrainData_SpeedSupervision_UnitTest_Pkg *PR,
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::odo_abs_diversion */ L_internal_Type_Obu_BasicTypes_Pkg odo_abs_diversion,
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::odo */ odometry_T_Obu_BasicTypes_Pkg *odo)
{
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::_L147 */ kcg_int _L147;
  /* SpeedSupervision_UnitTest_Pkg::CapturedTrainPosition::_L159 */ kcg_int _L159;
  
  (*odo).valid = kcg_true;
  (*odo).timestamp = (*PR).time;
  (*odo).acceleration = (*PR).acc;
  (*odo).motionDirection = cabAFirst_Obu_BasicTypes_Pkg;
  _L159 = (*PR).location + (*PR).Ant2Cab;
  (*odo).odo.o_nominal = _L159;
  (*odo).odo.o_min = _L159 - odo_abs_diversion;
  (*odo).odo.o_max = odo_abs_diversion + _L159;
  (*odo).speed.v_rawNominal = (*PR).velocity;
  _L159 = 100 / cOdoDiversion_SpeedSupervision_UnitTest_Pkg;
  _L147 = ((*PR).velocity + _L159 /
      cOdoDiversion_SpeedSupervision_UnitTest_Pkg) / _L159;
  (*odo).speed.v_lower = (*PR).velocity - _L147;
  (*odo).speed.v_safeNominal = (*PR).velocity + _L147;
  (*odo).speed.v_upper = (*odo).speed.v_safeNominal;
  /* 1 */ if ((*PR).flag.stopped) {
    (*odo).motionState = noMotion_Obu_BasicTypes_Pkg;
  }
  else {
    (*odo).motionState = Motion_Obu_BasicTypes_Pkg;
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

