/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "fakeOdometricTrainPosition_SDM_Types_Pkg.h"

#ifndef KCG_USER_DEFINED_INIT
void fakeOdometricTrainPosition_init_SDM_Types_Pkg(
  outC_fakeOdometricTrainPosition_SDM_Types_Pkg *outC)
{
  kcg_int i;
  
  outC->init = kcg_true;
  outC->_L131.valid = kcg_true;
  outC->_L131.nid_c = 0;
  outC->_L131.nid_bg = 0;
  outC->_L131.q_link = Q_LINK_Unlinked;
  outC->_L131.location.nominal = 0;
  outC->_L131.location.d_min = 0;
  outC->_L131.location.d_max = 0;
  outC->_L131.seqNoOnTrack = 0;
  outC->_L131.infoFromLinking.valid = kcg_true;
  outC->_L131.infoFromLinking.nid_bg_fromLinkingBG = 0;
  outC->_L131.infoFromLinking.nid_c_fromLinkingBG = 0;
  outC->_L131.infoFromLinking.expectedLocation.nominal = 0;
  outC->_L131.infoFromLinking.expectedLocation.d_min = 0;
  outC->_L131.infoFromLinking.expectedLocation.d_max = 0;
  outC->_L131.infoFromLinking.d_link.nominal = 0;
  outC->_L131.infoFromLinking.d_link.d_min = 0;
  outC->_L131.infoFromLinking.d_link.d_max = 0;
  outC->_L131.infoFromLinking.linkingInfo.valid = kcg_true;
  outC->_L131.infoFromLinking.linkingInfo.nid_LRBG = 0;
  outC->_L131.infoFromLinking.linkingInfo.q_dir = Q_DIR_Reverse;
  outC->_L131.infoFromLinking.linkingInfo.q_scale = Q_SCALE_10_cm_scale;
  outC->_L131.infoFromLinking.linkingInfo.d_link = 0;
  outC->_L131.infoFromLinking.linkingInfo.q_newcountry =
    Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
  outC->_L131.infoFromLinking.linkingInfo.nid_c = 0;
  outC->_L131.infoFromLinking.linkingInfo.nid_bg = 0;
  outC->_L131.infoFromLinking.linkingInfo.q_linkorientation =
    Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
  outC->_L131.infoFromLinking.linkingInfo.q_linkreaction =
    Q_LINKREACTION_Train_trip;
  outC->_L131.infoFromLinking.linkingInfo.q_locacc = 0;
  outC->_L131.infoFromPassing.valid = kcg_true;
  outC->_L131.infoFromPassing.BG_Header.valid = kcg_true;
  outC->_L131.infoFromPassing.BG_Header.q_updown = Q_UPDOWN_Down_link_telegram;
  outC->_L131.infoFromPassing.BG_Header.m_version =
    M_VERSION_Previous_versions_according_to_e_g_EEIG_SRS_and_UIC_A200_SRS;
  outC->_L131.infoFromPassing.BG_Header.q_media = Q_MEDIA_Balise;
  outC->_L131.infoFromPassing.BG_Header.n_total = N_TOTAL_1_balise_in_the_group;
  outC->_L131.infoFromPassing.BG_Header.m_mcount = 0;
  outC->_L131.infoFromPassing.BG_Header.nid_c = 0;
  outC->_L131.infoFromPassing.BG_Header.nid_bg = 0;
  outC->_L131.infoFromPassing.BG_Header.q_link = Q_LINK_Unlinked;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.valid = kcg_true;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.timestamp = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.odo.o_nominal = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.odo.o_min = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.odo.o_max = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.speed.v_safeNominal = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.speed.v_rawNominal = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.speed.v_lower = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.speed.v_upper = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.acceleration = 0;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.motionState =
    noMotion_Obu_BasicTypes_Pkg;
  outC->_L131.infoFromPassing.BG_Header.bgPosition.motionDirection =
    unknownDirection_Obu_BasicTypes_Pkg;
  outC->_L131.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.nominal =
    0;
  outC->_L131.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_min =
    0;
  outC->_L131.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_max =
    0;
  outC->_L131.infoFromPassing.BG_Header.q_nvlocacc = 0;
  outC->_L131.infoFromPassing.BG_Header.noCoordinateSystemHasBeenAssigned =
    kcg_true;
  outC->_L131.infoFromPassing.BG_Header.trainOrientationToBG =
    Q_DIRLRBG_Reverse;
  outC->_L131.infoFromPassing.BG_Header.trainRunningDirectionToBG =
    Q_DIRTRAIN_Reverse;
  for (i = 0; i < 33; i++) {
    outC->_L131.infoFromPassing.linkedBGs[i].valid = kcg_true;
    outC->_L131.infoFromPassing.linkedBGs[i].nid_LRBG = 0;
    outC->_L131.infoFromPassing.linkedBGs[i].q_dir = Q_DIR_Reverse;
    outC->_L131.infoFromPassing.linkedBGs[i].q_scale = Q_SCALE_10_cm_scale;
    outC->_L131.infoFromPassing.linkedBGs[i].d_link = 0;
    outC->_L131.infoFromPassing.linkedBGs[i].q_newcountry =
      Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
    outC->_L131.infoFromPassing.linkedBGs[i].nid_c = 0;
    outC->_L131.infoFromPassing.linkedBGs[i].nid_bg = 0;
    outC->_L131.infoFromPassing.linkedBGs[i].q_linkorientation =
      Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
    outC->_L131.infoFromPassing.linkedBGs[i].q_linkreaction =
      Q_LINKREACTION_Train_trip;
    outC->_L131.infoFromPassing.linkedBGs[i].q_locacc = 0;
  }
  outC->_L131.missed = kcg_true;
  outC->_L134.valid = kcg_true;
  outC->_L134.nid_c = 0;
  outC->_L134.nid_bg = 0;
  outC->_L134.q_link = Q_LINK_Unlinked;
  outC->_L134.location.nominal = 0;
  outC->_L134.location.d_min = 0;
  outC->_L134.location.d_max = 0;
  outC->_L134.seqNoOnTrack = 0;
  outC->_L134.infoFromLinking.valid = kcg_true;
  outC->_L134.infoFromLinking.nid_bg_fromLinkingBG = 0;
  outC->_L134.infoFromLinking.nid_c_fromLinkingBG = 0;
  outC->_L134.infoFromLinking.expectedLocation.nominal = 0;
  outC->_L134.infoFromLinking.expectedLocation.d_min = 0;
  outC->_L134.infoFromLinking.expectedLocation.d_max = 0;
  outC->_L134.infoFromLinking.d_link.nominal = 0;
  outC->_L134.infoFromLinking.d_link.d_min = 0;
  outC->_L134.infoFromLinking.d_link.d_max = 0;
  outC->_L134.infoFromLinking.linkingInfo.valid = kcg_true;
  outC->_L134.infoFromLinking.linkingInfo.nid_LRBG = 0;
  outC->_L134.infoFromLinking.linkingInfo.q_dir = Q_DIR_Reverse;
  outC->_L134.infoFromLinking.linkingInfo.q_scale = Q_SCALE_10_cm_scale;
  outC->_L134.infoFromLinking.linkingInfo.d_link = 0;
  outC->_L134.infoFromLinking.linkingInfo.q_newcountry =
    Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
  outC->_L134.infoFromLinking.linkingInfo.nid_c = 0;
  outC->_L134.infoFromLinking.linkingInfo.nid_bg = 0;
  outC->_L134.infoFromLinking.linkingInfo.q_linkorientation =
    Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
  outC->_L134.infoFromLinking.linkingInfo.q_linkreaction =
    Q_LINKREACTION_Train_trip;
  outC->_L134.infoFromLinking.linkingInfo.q_locacc = 0;
  outC->_L134.infoFromPassing.valid = kcg_true;
  outC->_L134.infoFromPassing.BG_Header.valid = kcg_true;
  outC->_L134.infoFromPassing.BG_Header.q_updown = Q_UPDOWN_Down_link_telegram;
  outC->_L134.infoFromPassing.BG_Header.m_version =
    M_VERSION_Previous_versions_according_to_e_g_EEIG_SRS_and_UIC_A200_SRS;
  outC->_L134.infoFromPassing.BG_Header.q_media = Q_MEDIA_Balise;
  outC->_L134.infoFromPassing.BG_Header.n_total = N_TOTAL_1_balise_in_the_group;
  outC->_L134.infoFromPassing.BG_Header.m_mcount = 0;
  outC->_L134.infoFromPassing.BG_Header.nid_c = 0;
  outC->_L134.infoFromPassing.BG_Header.nid_bg = 0;
  outC->_L134.infoFromPassing.BG_Header.q_link = Q_LINK_Unlinked;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.valid = kcg_true;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.timestamp = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.odo.o_nominal = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.odo.o_min = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.odo.o_max = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.speed.v_safeNominal = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.speed.v_rawNominal = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.speed.v_lower = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.speed.v_upper = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.acceleration = 0;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.motionState =
    noMotion_Obu_BasicTypes_Pkg;
  outC->_L134.infoFromPassing.BG_Header.bgPosition.motionDirection =
    unknownDirection_Obu_BasicTypes_Pkg;
  outC->_L134.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.nominal =
    0;
  outC->_L134.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_min =
    0;
  outC->_L134.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_max =
    0;
  outC->_L134.infoFromPassing.BG_Header.q_nvlocacc = 0;
  outC->_L134.infoFromPassing.BG_Header.noCoordinateSystemHasBeenAssigned =
    kcg_true;
  outC->_L134.infoFromPassing.BG_Header.trainOrientationToBG =
    Q_DIRLRBG_Reverse;
  outC->_L134.infoFromPassing.BG_Header.trainRunningDirectionToBG =
    Q_DIRTRAIN_Reverse;
  for (i = 0; i < 33; i++) {
    outC->_L134.infoFromPassing.linkedBGs[i].valid = kcg_true;
    outC->_L134.infoFromPassing.linkedBGs[i].nid_LRBG = 0;
    outC->_L134.infoFromPassing.linkedBGs[i].q_dir = Q_DIR_Reverse;
    outC->_L134.infoFromPassing.linkedBGs[i].q_scale = Q_SCALE_10_cm_scale;
    outC->_L134.infoFromPassing.linkedBGs[i].d_link = 0;
    outC->_L134.infoFromPassing.linkedBGs[i].q_newcountry =
      Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
    outC->_L134.infoFromPassing.linkedBGs[i].nid_c = 0;
    outC->_L134.infoFromPassing.linkedBGs[i].nid_bg = 0;
    outC->_L134.infoFromPassing.linkedBGs[i].q_linkorientation =
      Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
    outC->_L134.infoFromPassing.linkedBGs[i].q_linkreaction =
      Q_LINKREACTION_Train_trip;
    outC->_L134.infoFromPassing.linkedBGs[i].q_locacc = 0;
  }
  outC->_L134.missed = kcg_true;
  outC->tpos.valid = kcg_true;
  outC->tpos.timestamp = 0;
  outC->tpos.trainPositionIsUnknown = kcg_true;
  outC->tpos.noCoordinateSystemHasBeenAssigned = kcg_true;
  outC->tpos.trainPosition.nominal = 0;
  outC->tpos.trainPosition.d_min = 0;
  outC->tpos.trainPosition.d_max = 0;
  outC->tpos.estimatedFrontEndPosition = 0;
  outC->tpos.minSafeFrontEndPosition = 0;
  outC->tpos.maxSafeFrontEndPostion = 0;
  outC->tpos.LRBG.valid = kcg_true;
  outC->tpos.LRBG.nid_c = 0;
  outC->tpos.LRBG.nid_bg = 0;
  outC->tpos.LRBG.q_link = Q_LINK_Unlinked;
  outC->tpos.LRBG.location.nominal = 0;
  outC->tpos.LRBG.location.d_min = 0;
  outC->tpos.LRBG.location.d_max = 0;
  outC->tpos.LRBG.seqNoOnTrack = 0;
  outC->tpos.LRBG.infoFromLinking.valid = kcg_true;
  outC->tpos.LRBG.infoFromLinking.nid_bg_fromLinkingBG = 0;
  outC->tpos.LRBG.infoFromLinking.nid_c_fromLinkingBG = 0;
  outC->tpos.LRBG.infoFromLinking.expectedLocation.nominal = 0;
  outC->tpos.LRBG.infoFromLinking.expectedLocation.d_min = 0;
  outC->tpos.LRBG.infoFromLinking.expectedLocation.d_max = 0;
  outC->tpos.LRBG.infoFromLinking.d_link.nominal = 0;
  outC->tpos.LRBG.infoFromLinking.d_link.d_min = 0;
  outC->tpos.LRBG.infoFromLinking.d_link.d_max = 0;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.valid = kcg_true;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.nid_LRBG = 0;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.q_dir = Q_DIR_Reverse;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.q_scale = Q_SCALE_10_cm_scale;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.d_link = 0;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.q_newcountry =
    Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.nid_c = 0;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.nid_bg = 0;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.q_linkorientation =
    Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.q_linkreaction =
    Q_LINKREACTION_Train_trip;
  outC->tpos.LRBG.infoFromLinking.linkingInfo.q_locacc = 0;
  outC->tpos.LRBG.infoFromPassing.valid = kcg_true;
  outC->tpos.LRBG.infoFromPassing.BG_Header.valid = kcg_true;
  outC->tpos.LRBG.infoFromPassing.BG_Header.q_updown =
    Q_UPDOWN_Down_link_telegram;
  outC->tpos.LRBG.infoFromPassing.BG_Header.m_version =
    M_VERSION_Previous_versions_according_to_e_g_EEIG_SRS_and_UIC_A200_SRS;
  outC->tpos.LRBG.infoFromPassing.BG_Header.q_media = Q_MEDIA_Balise;
  outC->tpos.LRBG.infoFromPassing.BG_Header.n_total =
    N_TOTAL_1_balise_in_the_group;
  outC->tpos.LRBG.infoFromPassing.BG_Header.m_mcount = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.nid_c = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.nid_bg = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.q_link = Q_LINK_Unlinked;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.valid = kcg_true;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.timestamp = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.odo.o_nominal = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.odo.o_min = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.odo.o_max = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_safeNominal = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_rawNominal = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_lower = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_upper = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.acceleration = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.motionState =
    noMotion_Obu_BasicTypes_Pkg;
  outC->tpos.LRBG.infoFromPassing.BG_Header.bgPosition.motionDirection =
    unknownDirection_Obu_BasicTypes_Pkg;
  outC->tpos.LRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.nominal =
    0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_min =
    0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_max =
    0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.q_nvlocacc = 0;
  outC->tpos.LRBG.infoFromPassing.BG_Header.noCoordinateSystemHasBeenAssigned =
    kcg_true;
  outC->tpos.LRBG.infoFromPassing.BG_Header.trainOrientationToBG =
    Q_DIRLRBG_Reverse;
  outC->tpos.LRBG.infoFromPassing.BG_Header.trainRunningDirectionToBG =
    Q_DIRTRAIN_Reverse;
  for (i = 0; i < 33; i++) {
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].valid = kcg_true;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].nid_LRBG = 0;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].q_dir = Q_DIR_Reverse;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].q_scale = Q_SCALE_10_cm_scale;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].d_link = 0;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].q_newcountry =
      Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].nid_c = 0;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].nid_bg = 0;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].q_linkorientation =
      Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].q_linkreaction =
      Q_LINKREACTION_Train_trip;
    outC->tpos.LRBG.infoFromPassing.linkedBGs[i].q_locacc = 0;
  }
  outC->tpos.LRBG.missed = kcg_true;
  outC->tpos.prvLRBG.valid = kcg_true;
  outC->tpos.prvLRBG.nid_c = 0;
  outC->tpos.prvLRBG.nid_bg = 0;
  outC->tpos.prvLRBG.q_link = Q_LINK_Unlinked;
  outC->tpos.prvLRBG.location.nominal = 0;
  outC->tpos.prvLRBG.location.d_min = 0;
  outC->tpos.prvLRBG.location.d_max = 0;
  outC->tpos.prvLRBG.seqNoOnTrack = 0;
  outC->tpos.prvLRBG.infoFromLinking.valid = kcg_true;
  outC->tpos.prvLRBG.infoFromLinking.nid_bg_fromLinkingBG = 0;
  outC->tpos.prvLRBG.infoFromLinking.nid_c_fromLinkingBG = 0;
  outC->tpos.prvLRBG.infoFromLinking.expectedLocation.nominal = 0;
  outC->tpos.prvLRBG.infoFromLinking.expectedLocation.d_min = 0;
  outC->tpos.prvLRBG.infoFromLinking.expectedLocation.d_max = 0;
  outC->tpos.prvLRBG.infoFromLinking.d_link.nominal = 0;
  outC->tpos.prvLRBG.infoFromLinking.d_link.d_min = 0;
  outC->tpos.prvLRBG.infoFromLinking.d_link.d_max = 0;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.valid = kcg_true;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.nid_LRBG = 0;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.q_dir = Q_DIR_Reverse;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.q_scale = Q_SCALE_10_cm_scale;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.d_link = 0;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.q_newcountry =
    Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.nid_c = 0;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.nid_bg = 0;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.q_linkorientation =
    Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.q_linkreaction =
    Q_LINKREACTION_Train_trip;
  outC->tpos.prvLRBG.infoFromLinking.linkingInfo.q_locacc = 0;
  outC->tpos.prvLRBG.infoFromPassing.valid = kcg_true;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.valid = kcg_true;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.q_updown =
    Q_UPDOWN_Down_link_telegram;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.m_version =
    M_VERSION_Previous_versions_according_to_e_g_EEIG_SRS_and_UIC_A200_SRS;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.q_media = Q_MEDIA_Balise;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.n_total =
    N_TOTAL_1_balise_in_the_group;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.m_mcount = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.nid_c = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.nid_bg = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.q_link = Q_LINK_Unlinked;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.valid = kcg_true;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.timestamp = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.odo.o_nominal = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.odo.o_min = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.odo.o_max = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_safeNominal =
    0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_rawNominal =
    0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_lower = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_upper = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.acceleration = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.motionState =
    noMotion_Obu_BasicTypes_Pkg;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.bgPosition.motionDirection =
    unknownDirection_Obu_BasicTypes_Pkg;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.nominal =
    0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_min =
    0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_max =
    0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.q_nvlocacc = 0;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.noCoordinateSystemHasBeenAssigned =
    kcg_true;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.trainOrientationToBG =
    Q_DIRLRBG_Reverse;
  outC->tpos.prvLRBG.infoFromPassing.BG_Header.trainRunningDirectionToBG =
    Q_DIRTRAIN_Reverse;
  for (i = 0; i < 33; i++) {
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].valid = kcg_true;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].nid_LRBG = 0;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].q_dir = Q_DIR_Reverse;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].q_scale =
      Q_SCALE_10_cm_scale;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].d_link = 0;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].q_newcountry =
      Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].nid_c = 0;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].nid_bg = 0;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].q_linkorientation =
      Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].q_linkreaction =
      Q_LINKREACTION_Train_trip;
    outC->tpos.prvLRBG.infoFromPassing.linkedBGs[i].q_locacc = 0;
  }
  outC->tpos.prvLRBG.missed = kcg_true;
  outC->tpos.nominalOrReverseToLRBG = Q_DLRBG_Reverse;
  outC->tpos.trainOrientationToLRBG = Q_DIRLRBG_Reverse;
  outC->tpos.trainRunningDirectionToLRBG = Q_DIRTRAIN_Reverse;
  outC->tpos.linkingIsUsedOnboard = kcg_true;
  outC->tpos.estimatedRearEndPosition = 0;
  outC->tpos.minSafeRearEndPosition = 0;
  outC->tpos.maxSafeRearEndPosition = 0;
  /* 1 */ SnapLRBG_init_SDM_Types_Pkg(&outC->Context_1);
}
#endif /* KCG_USER_DEFINED_INIT */


void fakeOdometricTrainPosition_reset_SDM_Types_Pkg(
  outC_fakeOdometricTrainPosition_SDM_Types_Pkg *outC)
{
  outC->init = kcg_true;
  /* 1 */ SnapLRBG_reset_SDM_Types_Pkg(&outC->Context_1);
}

/* SDM_Types_Pkg::fakeOdometricTrainPosition */
void fakeOdometricTrainPosition_SDM_Types_Pkg(
  /* SDM_Types_Pkg::fakeOdometricTrainPosition::odo */ odometry_T_Obu_BasicTypes_Pkg *odo,
  /* SDM_Types_Pkg::fakeOdometricTrainPosition::trainData */ trainData_T_TIU_Types_Pkg *trainData,
  outC_fakeOdometricTrainPosition_SDM_Types_Pkg *outC)
{
  /* SDM_Types_Pkg::fakeOdometricTrainPosition::_L137 */ kcg_bool _L137;
  /* SDM_Types_Pkg::fakeOdometricTrainPosition::_L90 */ kcg_int _L90;
  
  outC->tpos.valid = kcg_true;
  outC->tpos.timestamp = (*odo).timestamp;
  outC->tpos.trainPositionIsUnknown = kcg_false;
  outC->tpos.noCoordinateSystemHasBeenAssigned = kcg_false;
  outC->tpos.estimatedFrontEndPosition = (*odo).odo.o_nominal;
  outC->tpos.minSafeFrontEndPosition = (*odo).odo.o_min;
  outC->tpos.maxSafeFrontEndPostion = (*odo).odo.o_max;
  outC->tpos.nominalOrReverseToLRBG = Q_DLRBG_Nominal;
  outC->tpos.trainOrientationToLRBG = Q_DIRLRBG_Nominal;
  outC->tpos.trainRunningDirectionToLRBG = Q_DIRTRAIN_Nominal;
  outC->tpos.linkingIsUsedOnboard = kcg_false;
  outC->tpos.estimatedRearEndPosition = (*odo).odo.o_nominal -
    (*trainData).trainLength;
  outC->tpos.minSafeRearEndPosition = (*odo).odo.o_min -
    (*trainData).trainLength;
  outC->tpos.maxSafeRearEndPosition = (*odo).odo.o_max -
    (*trainData).trainLength;
  outC->tpos.trainPosition.nominal = (*odo).odo.o_nominal;
  outC->tpos.trainPosition.d_min = (*odo).odo.o_min - (*odo).odo.o_nominal;
  _L90 = (*odo).odo.o_max - (*odo).odo.o_nominal;
  outC->tpos.trainPosition.d_max = _L90;
  _L137 = _L90 <= _cMinBaliseAccuracy_SDM_Types_Pkg;
  /* ck__L137 */ if (_L137) {
    /* 1 */ SnapLRBG_SDM_Types_Pkg((*odo).odo.o_nominal, &outC->Context_1);
    kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
      &outC->_L131,
      &outC->Context_1.LRBG);
    kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
      &outC->_L134,
      &outC->Context_1.prvLRBG);
  }
  else if (outC->init) {
    kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
      &outC->_L131,
      (positionedBG_T_TrainPosition_Types_Pck *)
        &cNoPositionedBG_CalculateTrainPosition_Pkg);
    kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
      &outC->_L134,
      (positionedBG_T_TrainPosition_Types_Pck *)
        &cNoPositionedBG_CalculateTrainPosition_Pkg);
  }
  outC->init = kcg_false;
  kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
    &outC->tpos.LRBG,
    &outC->_L131);
  kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
    &outC->tpos.prvLRBG,
    &outC->_L134);
}

/* $**************** KCG Version 6.4 (build i21) ****************
** fakeOdometricTrainPosition_SDM_Types_Pkg.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

