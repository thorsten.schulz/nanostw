/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "GenerateTrack_SpeedSupervision_UnitTest_Pkg_35.h"

#ifndef KCG_USER_DEFINED_INIT
void GenerateTrack_init_SpeedSupervision_UnitTest_Pkg_35(
  outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35 *outC)
{
  kcg_int i;
  
  outC->init = kcg_true;
  outC->rem_eoa = 0;
  outC->TA.freshMA = kcg_true;
  outC->TA.MA.valid = kcg_true;
  outC->TA.MA.Level = MA_L1_TrackAtlasTypes;
  outC->TA.MA.q_dir = Q_DIR_Reverse;
  outC->TA.MA.v_main = 0;
  outC->TA.MA.v_loa = 0;
  outC->TA.MA.t_loa_unlimited = kcg_true;
  outC->TA.MA.t_loa = 0;
  outC->TA.MA.n_iter = 0;
  for (i = 0; i < 2; i++) {
    outC->TA.MA.sections[i].valid = kcg_true;
    outC->TA.MA.sections[i].q_endsection = kcg_true;
    outC->TA.MA.sections[i].l_section = 0;
    outC->TA.MA.sections[i].q_sectiontimer = kcg_true;
    outC->TA.MA.sections[i].t_sectiontimer = 0;
    outC->TA.MA.sections[i].d_sectiontimerstoploc = 0;
  }
  outC->TA.MA.q_dangerpoint = kcg_true;
  outC->TA.MA.dangerpoint.d_DP_or_OL = 0;
  outC->TA.MA.dangerpoint.v_release = 0;
  outC->TA.MA.dangerpoint.calc_v_release_onboard = kcg_true;
  outC->TA.MA.q_overlap = kcg_true;
  outC->TA.MA.overlap.d_DP_or_OL = 0;
  outC->TA.MA.overlap.v_release = 0;
  outC->TA.MA.overlap.calc_v_release_onboard = kcg_true;
  outC->TA.MA.q_endtimer = kcg_true;
  outC->TA.MA.endtimer_t.t_endtimer = 0;
  outC->TA.MA.endtimer_t.d_endtimerstoploc = 0;
  outC->TA.freshGradientProfile = kcg_true;
  for (i = 0; i < 2; i++) {
    outC->TA.GradientProfile[i].valid = kcg_true;
    outC->TA.GradientProfile[i].Loc_Absolute = 0;
    outC->TA.GradientProfile[i].Loc_LRBG = 0;
    outC->TA.GradientProfile[i].Gradient = 0;
    outC->TA.GradientProfile[i].L_Gradient = 0;
  }
  outC->TA.freshMRSP = kcg_true;
  for (i = 0; i < 35; i++) {
    outC->TA.MRSP[i].valid = kcg_true;
    outC->TA.MRSP[i].Loc_Abs = 0;
    outC->TA.MRSP[i].Loc_LRBG = 0;
    outC->TA.MRSP[i].MRS = 0;
  }
}
#endif /* KCG_USER_DEFINED_INIT */


void GenerateTrack_reset_SpeedSupervision_UnitTest_Pkg_35(
  outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35 *outC)
{
  outC->init = kcg_true;
}

/* SpeedSupervision_UnitTest_Pkg::GenerateTrack */
void GenerateTrack_SpeedSupervision_UnitTest_Pkg_35(
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::eoa */ L_internal_Type_Obu_BasicTypes_Pkg eoa,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::v_release */ V_internal_Type_Obu_BasicTypes_Pkg v_release,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::freshGrad */ kcg_bool freshGrad,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::Grad */ GradientProfile_t_TrackAtlasTypes *Grad,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::freshMRSP */ kcg_bool freshMRSP,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::MRSP */ MRSP_Profile_t_TrackAtlasTypes *MRSP,
  outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35 *outC)
{
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack */ kcg_int tmp;
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::_L117 */ DP_or_OL_t_TrackAtlasTypes _L117;
  
  outC->TA.freshGradientProfile = freshGrad;
  kcg_copy_GradientProfile_t_TrackAtlasTypes(&outC->TA.GradientProfile, Grad);
  outC->TA.freshMRSP = freshMRSP;
  kcg_copy_MRSP_Profile_t_TrackAtlasTypes(&outC->TA.MRSP, MRSP);
  _L117.d_DP_or_OL = DEFAULT_overlapLength_SpeedSupervision_UnitTest_Pkg + eoa;
  _L117.v_release = v_release;
  _L117.calc_v_release_onboard = kcg_false;
  /* last_init_ck_eoa */ if (outC->init) {
    outC->init = kcg_false;
    tmp = - 1;
  }
  else {
    tmp = outC->rem_eoa;
  }
  outC->TA.freshMA = eoa != tmp;
  outC->TA.MA.valid = kcg_true;
  outC->TA.MA.Level = DEFAULT_MA_level_23_TrackAtlasTypes;
  outC->TA.MA.q_dir = Q_DIR_Nominal;
  outC->TA.MA.v_main = 0;
  outC->TA.MA.v_loa = 0;
  outC->TA.MA.t_loa_unlimited = kcg_true;
  outC->TA.MA.t_loa = 0;
  outC->TA.MA.n_iter = 1;
  outC->TA.MA.q_dangerpoint = _L117.d_DP_or_OL > eoa;
  kcg_copy_DP_or_OL_t_TrackAtlasTypes(&outC->TA.MA.dangerpoint, &_L117);
  outC->TA.MA.q_overlap = kcg_false;
  kcg_copy_DP_or_OL_t_TrackAtlasTypes(
    &outC->TA.MA.overlap,
    (DP_or_OL_t_TrackAtlasTypes *) &DEFAULT_DP_TrackAtlasTypes);
  outC->TA.MA.q_endtimer = kcg_false;
  kcg_copy_Endtimer_t_TrackAtlasTypes(
    &outC->TA.MA.endtimer_t,
    (Endtimer_t_TrackAtlasTypes *) &DEFAULT_Endtimer_TrackAtlasTypes);
  kcg_copy_MovementAuthoritySectionlist_t_TrackAtlasTypes(
    &outC->TA.MA.sections,
    (MovementAuthoritySectionlist_t_TrackAtlasTypes *)
      &DEFAULT_MA_sectionlist_TrackAtlasTypes);
  outC->TA.MA.sections[0].valid = kcg_true;
  outC->TA.MA.sections[0].q_endsection = kcg_true;
  outC->TA.MA.sections[0].l_section = eoa;
  outC->TA.MA.sections[0].q_sectiontimer = kcg_false;
  outC->TA.MA.sections[0].t_sectiontimer = 0;
  outC->TA.MA.sections[0].d_sectiontimerstoploc = 0;
  outC->rem_eoa = eoa;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** GenerateTrack_SpeedSupervision_UnitTest_Pkg_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

