/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _brakePosition_SDMConversionModelPkg_H_
#define _brakePosition_SDMConversionModelPkg_H_

#include "kcg_types.h"
#include "TransformL_intToL_real_SDM_Types_Pkg.h"
#include "T_BrakeBasic_SDMConversionModelPkg.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDMConversionModelPkg::brakePosition */
extern void brakePosition_SDMConversionModelPkg(
  /* SDMConversionModelPkg::brakePosition::trainData */ trainData_T_TIU_Types_Pkg *trainData,
  /* SDMConversionModelPkg::brakePosition::T_brake_cm */ t_Brake_t_SDMModelPkg *T_brake_cm);

#endif /* _brakePosition_SDMConversionModelPkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** brakePosition_SDMConversionModelPkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

