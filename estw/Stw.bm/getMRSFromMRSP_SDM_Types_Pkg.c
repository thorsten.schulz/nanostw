/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "getMRSFromMRSP_SDM_Types_Pkg.h"

/* SDM_Types_Pkg::getMRSFromMRSP */
V_internal_real_Type_SDM_Types_Pkg getMRSFromMRSP_SDM_Types_Pkg(
  /* SDM_Types_Pkg::getMRSFromMRSP::MRSP */ MRSP_internal_T_TargetManagement_types *MRSP,
  /* SDM_Types_Pkg::getMRSFromMRSP::d_max_safe_front */ L_internal_real_Type_SDM_Types_Pkg d_max_safe_front)
{
  /* SDM_Types_Pkg::getMRSFromMRSP */ MRSP_internal_section_T_TargetManagement_types tmp;
  /* SDM_Types_Pkg::getMRSFromMRSP */ MRSP_internal_section_T_TargetManagement_types acc;
  kcg_int i;
  /* SDM_Types_Pkg::getMRSFromMRSP::V_MRSP */ V_internal_real_Type_SDM_Types_Pkg V_MRSP;
  
  kcg_copy_MRSP_internal_section_T_TargetManagement_types(&tmp, &(*MRSP)[0]);
  for (i = 0; i < 35; i++) {
    kcg_copy_MRSP_internal_section_T_TargetManagement_types(&acc, &tmp);
    /* 1 */ if ((*MRSP)[i].valid & ((*MRSP)[i].Loc_Abs <= d_max_safe_front)) {
      kcg_copy_MRSP_internal_section_T_TargetManagement_types(
        &tmp,
        &(*MRSP)[i]);
    }
    else {
      kcg_copy_MRSP_internal_section_T_TargetManagement_types(&tmp, &acc);
    }
  }
  V_MRSP = tmp.MRS;
  return V_MRSP;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** getMRSFromMRSP_SDM_Types_Pkg.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

