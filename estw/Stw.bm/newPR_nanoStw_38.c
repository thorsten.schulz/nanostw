/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "newPR_nanoStw_38.h"

/* nanoStw::newPR */
void newPR_nanoStw_38(
  /* nanoStw::newPR::i */ kcg_int i,
  /* nanoStw::newPR::data */ ProtectionLocations_nanoStw_trdp *data,
  /* nanoStw::newPR::length */ kcg_int length,
  /* nanoStw::newPR::IdToBlank */ TrackId_nanoStw IdToBlank,
  /* nanoStw::newPR::tracks */ array_21324 *tracks,
  /* nanoStw::newPR::goOn */ kcg_bool *goOn,
  /* nanoStw::newPR::trains */ otherTrainPos_nanoStw *trains)
{
  /* nanoStw::newPR */ kcg_bool tmp1;
  /* nanoStw::newPR */ kcg_bool tmp;
  
  tmp1 = /* 1 */
    isLocationNormalized_nanoStw_38(&(*data).protectedFrontEnd, tracks);
  tmp = /* 2 */
    isLocationNormalized_nanoStw_38(&(*data).protectedRearEnd, tracks);
  /* 1 */ if (((*data).tid != IdToBlank) & tmp1 & tmp) {
    (*trains).tid = (*data).tid;
    kcg_copy_TrackLocation_nanoStw(
      &(*trains).protectedLocationFront,
      &(*data).protectedFrontEnd);
    kcg_copy_TrackLocation_nanoStw(
      &(*trains).protectedLocationRear,
      &(*data).protectedRearEnd);
  }
  else {
    kcg_copy_otherTrainPos_nanoStw(
      trains,
      (otherTrainPos_nanoStw *) &emptyOtherPos_nanoStw);
  }
  *goOn = i + 1 < length;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** newPR_nanoStw_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

