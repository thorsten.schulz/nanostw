/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _InflateABrakeService_SDMConversionModelPkg_H_
#define _InflateABrakeService_SDMConversionModelPkg_H_

#include "kcg_types.h"
#include "InflateABrakeSpeeds_SDMConversionModelPkg.h"
#include "InflateABrakeRow_SDMConversionModelPkg.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDMConversionModelPkg::InflateABrakeService */
extern void InflateABrakeService_SDMConversionModelPkg(
  /* SDMConversionModelPkg::InflateABrakeService::aBrake */ a_Brake_t_SDMConversionModelPkg *aBrake,
  /* SDMConversionModelPkg::InflateABrakeService::aBrakeService */ ASafe_T_CalcBrakingCurves_types *aBrakeService);

#endif /* _InflateABrakeService_SDMConversionModelPkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** InflateABrakeService_SDMConversionModelPkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

