/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _TransformA_intToA_real_SDM_Types_Pkg_H_
#define _TransformA_intToA_real_SDM_Types_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDM_Types_Pkg::TransformA_intToA_real */
extern A_internal_real_Type_SDM_Types_Pkg TransformA_intToA_real_SDM_Types_Pkg(
  /* SDM_Types_Pkg::TransformA_intToA_real::acc_int */ A_internal_Type_Obu_BasicTypes_Pkg acc_int);

#endif /* _TransformA_intToA_real_SDM_Types_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** TransformA_intToA_real_SDM_Types_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

