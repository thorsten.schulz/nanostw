/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _brakePercentToABrake_SDMConversionModelPkg_H_
#define _brakePercentToABrake_SDMConversionModelPkg_H_

#include "kcg_types.h"
#include "brakePercentLookUp_SDMConversionModelPkg.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDMConversionModelPkg::brakePercentToABrake */
extern void brakePercentToABrake_SDMConversionModelPkg(
  /* SDMConversionModelPkg::brakePercentToABrake::trainData */ trainData_T_TIU_Types_Pkg *trainData,
  /* SDMConversionModelPkg::brakePercentToABrake::aBrake */ a_Brake_t_SDMConversionModelPkg *aBrake);

#endif /* _brakePercentToABrake_SDMConversionModelPkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** brakePercentToABrake_SDMConversionModelPkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

