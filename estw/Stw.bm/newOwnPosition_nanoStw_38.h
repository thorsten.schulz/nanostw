/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _newOwnPosition_nanoStw_38_H_
#define _newOwnPosition_nanoStw_38_H_

#include "kcg_types.h"
#include "selectTrackBySid_nanoStw_38.h"
#include "lastTrackKnot_nanoStw_38.h"
#include "normalizeTrackLocation_it_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::newOwnPosition */
extern void newOwnPosition_nanoStw_38(
  /* nanoStw::newOwnPosition::data */ TrainReport_nanoStw_trdp *data,
  /* nanoStw::newOwnPosition::tracks */ array_21324 *tracks,
  /* nanoStw::newOwnPosition::validatedTD */ TrainData_SpeedSupervision_UnitTest_Pkg *validatedTD,
  /* nanoStw::newOwnPosition::position */ simpleTrainPos_nanoStw *position,
  /* nanoStw::newOwnPosition::correctionPoint */ TrackLocation_nanoStw *correctionPoint);

#endif /* _newOwnPosition_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** newOwnPosition_nanoStw_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

