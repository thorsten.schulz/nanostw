/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "projectLocation_nanoStw.h"

/* nanoStw::projectLocation */
void projectLocation_nanoStw(
  /* nanoStw::projectLocation::to */ LengthUnit to,
  /* nanoStw::projectLocation::base */ TrackLocation_nanoStw *base,
  /* nanoStw::projectLocation::runsCW */ kcg_bool runsCW,
  /* nanoStw::projectLocation::projected */ TrackLocation_nanoStw *projected)
{
  /* nanoStw::projectLocation */ kcg_int tmp;
  /* nanoStw::projectLocation::_L4 */ kcg_int _L4;
  
  (*projected).sid = (*base).sid;
  _L4 = to - (*base).location;
  (*projected).location = _L4 + (*base).location;
  /* 1 */ if (runsCW != ((*base).sid < 0)) {
    tmp = - _L4;
  }
  else {
    tmp = _L4;
  }
  (*projected).distance = tmp + (*base).distance;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** projectLocation_nanoStw.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

