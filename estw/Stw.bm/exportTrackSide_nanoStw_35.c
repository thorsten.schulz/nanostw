/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "exportTrackSide_nanoStw_35.h"

/* nanoStw::exportTrackSide */
void exportTrackSide_nanoStw_35(
  /* nanoStw::exportTrackSide::sections */ array_21021 *sections,
  /* nanoStw::exportTrackSide::frontSid */ TrackId_nanoStw frontSid,
  /* nanoStw::exportTrackSide::eStwToSDM_sections */ array_21157 *eStwToSDM_sections,
  /* nanoStw::exportTrackSide::MAsids */ array_int_35 *MAsids)
{
  /* nanoStw::exportTrackSide */ TrackId_nanoStw acc;
  /* nanoStw::iterateSegmentById */ TrackId_nanoStw tmp_1_1;
  kcg_int i;
  /* nanoStw::findSidInMA::_L8 */ kcg_bool _L8_1;
  /* nanoStw::helper::dec::i */ kcg_int i_1_1;
  /* nanoStw::exportTrackSide::_L19 */ kcg_int _L19;
  /* nanoStw::exportTrackSide::_L34 */ TrackId_nanoStw _L34;
  
  /* 2 */ if (0 <= frontSid) {
    _L34 = frontSid;
  }
  else {
    _L34 = - frontSid;
  }
  _L8_1 = UnknownSid_nanoStw != _L34;
  /* 1 */ if (_L8_1) {
    /* 1_1 */ for (i = 0; i < 35; i++) {
      acc = _L34;
      /* 2 */ if (0 <= (*sections)[i].first.sid) {
        tmp_1_1 = (*sections)[i].first.sid;
      }
      else {
        tmp_1_1 = - (*sections)[i].first.sid;
      }
      i_1_1 = i + 1;
      /* 1_1 */ if (!(acc != tmp_1_1)) {
        break;
      }
    }
    _L19 = - 1 + i_1_1;
  }
  else {
    _L19 = 35;
  }
  /* 1 */ for (i = 0; i < 35; i++) {
    /* 1 */
    splitMRSP_nanoStw_35(
      i,
      sections,
      &(*sections)[i],
      _L19,
      &(*eStwToSDM_sections)[i],
      &(*MAsids)[i]);
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** exportTrackSide_nanoStw_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

