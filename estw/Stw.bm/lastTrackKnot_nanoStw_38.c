/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "lastTrackKnot_nanoStw_38.h"

/* nanoStw::lastTrackKnot */
void lastTrackKnot_nanoStw_38(
  /* nanoStw::lastTrackKnot::track */ TrackKnot_nanoStw *track,
  /* nanoStw::lastTrackKnot::runsCW */ kcg_bool runsCW,
  /* nanoStw::lastTrackKnot::tracks */ array_21324 *tracks,
  /* nanoStw::lastTrackKnot::ptrack */ TrackKnot_nanoStw *ptrack,
  /* nanoStw::lastTrackKnot::isOn */ kcg_bool *isOn,
  /* nanoStw::lastTrackKnot::isLastTracksLineBlunt */ kcg_bool *isLastTracksLineBlunt,
  /* nanoStw::lastTrackKnot::isBackwardsGoingLineBlunt */ kcg_bool *isBackwardsGoingLineBlunt)
{
  /* nanoStw::lastTrackKnot */ TrackKnotLine_nanoStw tmp;
  /* nanoStw::lastTrackKnot::line_going_to_last_TK */ TrackKnotLine_nanoStw line_going_to_last_TK;
  /* nanoStw::lastTrackKnot::_L24 */ kcg_bool _L24;
  
  *isBackwardsGoingLineBlunt = ((*track).sid < UnknownSid_nanoStw) == runsCW;
  /* 1 */ if (*isBackwardsGoingLineBlunt) {
    kcg_copy_TrackKnotLine_nanoStw(&line_going_to_last_TK, &(*track).line[0]);
  }
  else /* 2 */ if ((*track).line[2].on) {
    kcg_copy_TrackKnotLine_nanoStw(&line_going_to_last_TK, &(*track).line[2]);
  }
  else /* 3 */ if ((*track).line[3].on) {
    kcg_copy_TrackKnotLine_nanoStw(&line_going_to_last_TK, &(*track).line[3]);
  }
  else {
    kcg_copy_TrackKnotLine_nanoStw(&line_going_to_last_TK, &(*track).line[1]);
  }
  /* 1 */
  selectTrackBySid_nanoStw_38(line_going_to_last_TK.to, tracks, ptrack, &_L24);
  if ((0 <= line_going_to_last_TK.rLine) & (line_going_to_last_TK.rLine < 4)) {
    kcg_copy_TrackKnotLine_nanoStw(
      &tmp,
      &(*ptrack).line[line_going_to_last_TK.rLine]);
  }
  else {
    kcg_copy_TrackKnotLine_nanoStw(
      &tmp,
      (TrackKnotLine_nanoStw *) &emptyTrackLine_nanoStw);
  }
  *isOn = tmp.on;
  *isLastTracksLineBlunt = line_going_to_last_TK.rLine == TKL_BLUNT_nanoStw;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** lastTrackKnot_nanoStw_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

