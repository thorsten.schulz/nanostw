/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _makeTS_nanoStw_35_3_38_H_
#define _makeTS_nanoStw_35_3_38_H_

#include "kcg_types.h"
#include "iterateMA_nanoStw_3_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::makeTS */
extern void makeTS_nanoStw_35_3_38(
  /* nanoStw::makeTS::thisTrain */ simpleTrainPos_nanoStw *thisTrain,
  /* nanoStw::makeTS::trains */ array_21564 *trains,
  /* nanoStw::makeTS::tracks */ array_21324 *tracks,
  /* nanoStw::makeTS::MA */ TrackAtlasData_SpeedSupervision_UnitTest_Pkg *MA,
  /* nanoStw::makeTS::sections */ array_21021 *sections);

#endif /* _makeTS_nanoStw_35_3_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** makeTS_nanoStw_35_3_38.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

