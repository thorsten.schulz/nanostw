/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _exportTrainEstate_nanoStw_35_38_H_
#define _exportTrainEstate_nanoStw_35_38_H_

#include "kcg_types.h"
#include "collectTrack_nanoStw_35.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::exportTrainEstate */
extern void exportTrainEstate_nanoStw_35_38(
  /* nanoStw::exportTrainEstate::Tlocs */ ProtectionLocations_SpeedSupervision_UnitTest_Pkg *Tlocs,
  /* nanoStw::exportTrainEstate::sections */ array_21021 *sections,
  /* nanoStw::exportTrainEstate::runsCW */ kcg_bool runsCW,
  /* nanoStw::exportTrainEstate::TrainRelLimitLoc */ array_20997 *TrainRelLimitLoc);

#endif /* _exportTrainEstate_nanoStw_35_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** exportTrainEstate_nanoStw_35_38.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

