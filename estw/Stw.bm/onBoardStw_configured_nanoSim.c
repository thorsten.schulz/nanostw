/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "onBoardStw_configured_nanoSim.h"

#ifndef KCG_USER_DEFINED_INIT
void onBoardStw_configured_init_nanoSim(
  outC_onBoardStw_configured_nanoSim *outC)
{
  outC->rem_isActive_1 = kcg_true;
  outC->init = kcg_true;
  /* 1_1 */
  nanoSDM_init_SpeedSupervision_UnitTest_Pkg_35(&outC->_1_Context_1_1);
  /* 1_1 */ importTAdata_init_nanoStw_3_38(&outC->Context_1_1);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void onBoardStw_configured_reset_nanoSim(
  outC_onBoardStw_configured_nanoSim *outC)
{
  outC->init = kcg_true;
  /* 1_1 */
  nanoSDM_reset_SpeedSupervision_UnitTest_Pkg_35(&outC->_1_Context_1_1);
  /* 1_1 */ importTAdata_reset_nanoStw_3_38(&outC->Context_1_1);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/* nanoSim::onBoardStw_configured */
void onBoardStw_configured_nanoSim(
  /* nanoSim::onBoardStw_configured::XPR */ allPR_nanoSim *XPR,
  /* nanoSim::onBoardStw_configured::XTracks */ TrackPlan_nanoSim *XTracks,
  /* nanoSim::onBoardStw_configured::XReport */ TrainReport_nanoStw_trdp *XReport,
  /* nanoSim::onBoardStw_configured::correctionPoint */ TrackLocation_nanoStw *correctionPoint,
  /* nanoSim::onBoardStw_configured::haveData */ kcg_bool *haveData,
  /* nanoSim::onBoardStw_configured::XTrainRelLimitLoc */ TrackDMI_nanoSim *XTrainRelLimitLoc,
  /* nanoSim::onBoardStw_configured::PRtoOther */ ProtectionLocations_nanoStw_trdp *PRtoOther,
  /* nanoSim::onBoardStw_configured::InterventionCmd */ InterventionCommand_SpeedSupervision_UnitTest_Pkg *InterventionCmd,
  /* nanoSim::onBoardStw_configured::pkgDMI */ SDM_DMI_wrapper_T_SDM_Types_Pkg *pkgDMI,
  outC_onBoardStw_configured_nanoSim *outC)
{
  /* nanoSim::onBoardStw_configured */ kcg_bool tmp;
  /* nanoStw::onBoardStw::_L132 */ TrackAtlasData_SpeedSupervision_UnitTest_Pkg _L132_1;
  /* nanoStw::onBoardStw::_L131 */ array_21021 _L131_1;
  /* nanoStw::onBoardStw::_L118 */ kcg_bool _L118_1;
  /* nanoStw::onBoardStw::_L114 */ array_21564 _L114_1;
  /* nanoStw::onBoardStw::_L11 */ ProtectionLocations_SpeedSupervision_UnitTest_Pkg _L11_1;
  /* nanoStw::onBoardStw::Train */ simpleTrainPos_nanoStw Train_1;
  /* nanoStw::onBoardStw::Tracks */ array_21324 Tracks_1;
  /* nanoSim::onBoardStw_configured::_L8 */ array_21157 _L8;
  /* nanoSim::onBoardStw_configured::_L9 */ array_int_35 _L9;
  kcg_int i;
  
  (*XTrainRelLimitLoc).sizeTracks = TRACKS_nanoSim;
  /* 1_1 */
  importTAdata_nanoStw_3_38(
    &(*XTracks).tracks,
    (*XTracks).sizeTracks,
    &(*XPR).PR,
    (*XPR).sizeTrains,
    XReport,
    &outC->Context_1_1);
  kcg_copy_simpleTrainPos_nanoStw(&Train_1, &outC->Context_1_1.thisTrain);
  kcg_copy_array_21564(&_L114_1, &outC->Context_1_1.otherTrains);
  kcg_copy_array_21324(&Tracks_1, &outC->Context_1_1.tracks);
  *haveData = outC->Context_1_1.dataOk;
  kcg_copy_TrackLocation_nanoStw(
    correctionPoint,
    &outC->Context_1_1.correctionPoint);
  /* 1_1 */
  makeTS_nanoStw_35_3_38(&Train_1, &_L114_1, &Tracks_1, &_L132_1, &_L131_1);
  /* 1_1 */
  exportTrackSide_nanoStw_35(&_L131_1, Train_1.frontEnd.sid, &_L8, &_L9);
  /* 1_last_init_ck_isActive */ if (outC->init) {
    outC->init = kcg_false;
    tmp = kcg_true;
  }
  else {
    tmp = !outC->rem_isActive_1;
  }
  _L118_1 = tmp & Train_1.flag.active;
  if (_L118_1) {
    /* 1_1 */
    nanoSDM_reset_SpeedSupervision_UnitTest_Pkg_35(&outC->_1_Context_1_1);
  }
  /* 1_1 */
  nanoSDM_SpeedSupervision_UnitTest_Pkg_35(
    &outC->Context_1_1.TD,
    &_L132_1,
    &_L8,
    &outC->_1_Context_1_1);
  kcg_copy_SDM_DMI_wrapper_T_SDM_Types_Pkg(
    pkgDMI,
    &outC->_1_Context_1_1.sdmToDMI);
  kcg_copy_InterventionCommand_SpeedSupervision_UnitTest_Pkg(
    InterventionCmd,
    &outC->_1_Context_1_1.InterventionCmd);
  kcg_copy_ProtectionLocations_SpeedSupervision_UnitTest_Pkg(
    &_L11_1,
    &outC->_1_Context_1_1.toIX);
  /* 1_1 */
  exportProtectionLocations_nanoStw_38(
    &_L11_1,
    &Train_1,
    GlobalEBintegrity_nanoStw,
    &Tracks_1,
    PRtoOther);
  (*XTrainRelLimitLoc).TrainID = (*PRtoOther).tid;
  /* ck_haveData */ if (*haveData) {
    /* 1_1 */
    exportTrainEstate_nanoStw_35_38(
      &_L11_1,
      &_L131_1,
      Train_1.flag.CW,
      &(*XTrainRelLimitLoc).TrackRelLocs);
  }
  else {
    for (i = 0; i < 38; i++) {
      kcg_copy_TrainRelLimitLocations_nanoStw_trdp(
        &(*XTrainRelLimitLoc).TrackRelLocs[i],
        (TrainRelLimitLocations_nanoStw_trdp *)
          &emptyTrainRelLimitLocations_nanoStw_trdp);
    }
  }
  outC->rem_isActive_1 = Train_1.flag.active;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** onBoardStw_configured_nanoSim.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

