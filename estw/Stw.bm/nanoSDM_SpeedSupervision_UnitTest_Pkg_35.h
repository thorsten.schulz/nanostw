/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _nanoSDM_SpeedSupervision_UnitTest_Pkg_35_H_
#define _nanoSDM_SpeedSupervision_UnitTest_Pkg_35_H_

#include "kcg_types.h"
#include "DefaultNationalValues_SpeedSupervision_UnitTest_Pkg.h"
#include "CapturedTrainPosition_SpeedSupervision_UnitTest_Pkg.h"
#include "mapEStwSections_SpeedSupervision_UnitTest_Pkg.h"
#include "transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35.h"
#include "SpeedSupervision_Integration_SpeedSupervision_Integration_Pkg.h"
#include "fakeOdometricTrainPosition_SDM_Types_Pkg.h"
#include "GenerateTrack_SpeedSupervision_UnitTest_Pkg_35.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* ---------------------------  outputs  --------------------------- */
  SDM_DMI_wrapper_T_SDM_Types_Pkg /* SpeedSupervision_UnitTest_Pkg::nanoSDM::sdmToDMI */ sdmToDMI;
  InterventionCommand_SpeedSupervision_UnitTest_Pkg /* SpeedSupervision_UnitTest_Pkg::nanoSDM::InterventionCmd */ InterventionCmd;
  ProtectionLocations_SpeedSupervision_UnitTest_Pkg /* SpeedSupervision_UnitTest_Pkg::nanoSDM::toIX */ toIX;
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------  no initialization variables  ----------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_SpeedSupervision_Integration_SpeedSupervision_Integration_Pkg /* 1 */ _2_Context_1;
  outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35 /* 1 */ _1_Context_1;
  outC_fakeOdometricTrainPosition_SDM_Types_Pkg /* 1 */ Context_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35;

/* ===========  node initialization and cycle functions  =========== */
/* SpeedSupervision_UnitTest_Pkg::nanoSDM */
extern void nanoSDM_SpeedSupervision_UnitTest_Pkg_35(
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::PR_TrainData */ TrainData_SpeedSupervision_UnitTest_Pkg *PR_TrainData,
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::TA_data */ TrackAtlasData_SpeedSupervision_UnitTest_Pkg *TA_data,
  /* SpeedSupervision_UnitTest_Pkg::nanoSDM::TA_sections */ array_21157 *TA_sections,
  outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35 *outC);

extern void nanoSDM_reset_SpeedSupervision_UnitTest_Pkg_35(
  outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void nanoSDM_init_SpeedSupervision_UnitTest_Pkg_35(
  outC_nanoSDM_SpeedSupervision_UnitTest_Pkg_35 *outC);
#endif /* KCG_USER_DEFINED_INIT */

#endif /* _nanoSDM_SpeedSupervision_UnitTest_Pkg_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** nanoSDM_SpeedSupervision_UnitTest_Pkg_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

