/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _nextIndex_CalcBrakingCurves_Pkg_internalOperators_H_
#define _nextIndex_CalcBrakingCurves_Pkg_internalOperators_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* CalcBrakingCurves_Pkg::internalOperators::nextIndex */
extern kcg_int nextIndex_CalcBrakingCurves_Pkg_internalOperators(
  /* CalcBrakingCurves_Pkg::internalOperators::nextIndex::upwards */ kcg_bool upwards,
  /* CalcBrakingCurves_Pkg::internalOperators::nextIndex::currentIndex */ kcg_int currentIndex,
  /* CalcBrakingCurves_Pkg::internalOperators::nextIndex::limit */ kcg_int limit);

#endif /* _nextIndex_CalcBrakingCurves_Pkg_internalOperators_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** nextIndex_CalcBrakingCurves_Pkg_internalOperators.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

