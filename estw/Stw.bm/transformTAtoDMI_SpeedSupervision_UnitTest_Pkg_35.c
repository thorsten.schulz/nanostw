/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35.h"

/* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI */
void transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35(
  /* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI::to_Supervision */ DataForSupervision_nextGen_t_TrackAtlasTypes *to_Supervision,
  /* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI::eoa */ L_internal_Type_Obu_BasicTypes_Pkg eoa,
  /* SpeedSupervision_UnitTest_Pkg::transformTAtoDMI::to_DMI */ DMI_Track_Description_T_DMI_Messages_EVC_to_DMI_Pkg *to_DMI)
{
  (*to_DMI).valid = (*to_Supervision).freshGradientProfile |
    (*to_Supervision).freshMRSP;
  (*to_DMI).system_clock = 0;
  kcg_copy_DMI_trackCondition_T_DMI_Types_Pkg(
    &(*to_DMI).trackConditions,
    (DMI_trackCondition_T_DMI_Types_Pkg *)
      &cNoTrackCondition_SpeedSupervision_UnitTest_Pkg);
  /* 1 */
  MRSP_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI_35(
    (*to_Supervision).freshMRSP,
    &(*to_Supervision).MRSP,
    eoa,
    &(*to_DMI).speedProfiles);
  /* 1 */
  GradientProfile_to_DMI_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
    (*to_Supervision).freshGradientProfile,
    &(*to_Supervision).GradientProfile,
    eoa,
    &(*to_DMI).gradientProfiles);
}

/* $**************** KCG Version 6.4 (build i21) ****************
** transformTAtoDMI_SpeedSupervision_UnitTest_Pkg_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

