/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _limitPositive_SDM_Types_Pkg_H_
#define _limitPositive_SDM_Types_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDM_Types_Pkg::limitPositive */
extern kcg_real limitPositive_SDM_Types_Pkg(
  /* SDM_Types_Pkg::limitPositive::in */ kcg_real in);

#endif /* _limitPositive_SDM_Types_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** limitPositive_SDM_Types_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

