/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h"

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop */
void GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::Gradient_section_in */ Gradient_section_t_TrackAtlasTypes *Gradient_section_in,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::Gradient_section_for_DMI_out */ GradientProfile_for_DMI_section_t_TrackAtlasTypes *Gradient_section_for_DMI_out)
{
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop */ L_internal_Type_Obu_BasicTypes_Pkg tmp1;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop */ kcg_int tmp;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::_L28 */ kcg_int _L28;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::GradientProfile_to_DMI_loop::_L50 */ kcg_bool _L50;
  
  _L28 = (*Gradient_section_in).Loc_Absolute +
    (*Gradient_section_in).L_Gradient;
  _L50 = (EoA > (*Gradient_section_in).Loc_Absolute) &
    (*Gradient_section_in).valid;
  (*Gradient_section_for_DMI_out).valid = _L50;
  /* 5 */ if (_L50) {
    (*Gradient_section_for_DMI_out).begin_section =
      (*Gradient_section_in).Loc_Absolute;
    /* 2 */ if (_L28 > EoA) {
      tmp1 = EoA;
    }
    else {
      tmp1 = _L28;
    }
    /* 1 */ if ((*Gradient_section_in).Gradient == 255) {
      tmp = 0;
    }
    else {
      tmp = (*Gradient_section_in).Gradient;
    }
  }
  else {
    (*Gradient_section_for_DMI_out).begin_section = 0;
    tmp1 = 0;
    tmp = 0;
  }
  (*Gradient_section_for_DMI_out).end_section = tmp1;
  (*Gradient_section_for_DMI_out).gradient = tmp;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** GradientProfile_to_DMI_loop_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

