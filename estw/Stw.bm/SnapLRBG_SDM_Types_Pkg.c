/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "SnapLRBG_SDM_Types_Pkg.h"

#ifndef KCG_USER_DEFINED_INIT
void SnapLRBG_init_SDM_Types_Pkg(outC_SnapLRBG_SDM_Types_Pkg *outC)
{
  kcg_int i;
  
  outC->init = kcg_true;
  outC->prvLRBG.valid = kcg_true;
  outC->prvLRBG.nid_c = 0;
  outC->prvLRBG.nid_bg = 0;
  outC->prvLRBG.q_link = Q_LINK_Unlinked;
  outC->prvLRBG.location.nominal = 0;
  outC->prvLRBG.location.d_min = 0;
  outC->prvLRBG.location.d_max = 0;
  outC->prvLRBG.seqNoOnTrack = 0;
  outC->prvLRBG.infoFromLinking.valid = kcg_true;
  outC->prvLRBG.infoFromLinking.nid_bg_fromLinkingBG = 0;
  outC->prvLRBG.infoFromLinking.nid_c_fromLinkingBG = 0;
  outC->prvLRBG.infoFromLinking.expectedLocation.nominal = 0;
  outC->prvLRBG.infoFromLinking.expectedLocation.d_min = 0;
  outC->prvLRBG.infoFromLinking.expectedLocation.d_max = 0;
  outC->prvLRBG.infoFromLinking.d_link.nominal = 0;
  outC->prvLRBG.infoFromLinking.d_link.d_min = 0;
  outC->prvLRBG.infoFromLinking.d_link.d_max = 0;
  outC->prvLRBG.infoFromLinking.linkingInfo.valid = kcg_true;
  outC->prvLRBG.infoFromLinking.linkingInfo.nid_LRBG = 0;
  outC->prvLRBG.infoFromLinking.linkingInfo.q_dir = Q_DIR_Reverse;
  outC->prvLRBG.infoFromLinking.linkingInfo.q_scale = Q_SCALE_10_cm_scale;
  outC->prvLRBG.infoFromLinking.linkingInfo.d_link = 0;
  outC->prvLRBG.infoFromLinking.linkingInfo.q_newcountry =
    Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
  outC->prvLRBG.infoFromLinking.linkingInfo.nid_c = 0;
  outC->prvLRBG.infoFromLinking.linkingInfo.nid_bg = 0;
  outC->prvLRBG.infoFromLinking.linkingInfo.q_linkorientation =
    Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
  outC->prvLRBG.infoFromLinking.linkingInfo.q_linkreaction =
    Q_LINKREACTION_Train_trip;
  outC->prvLRBG.infoFromLinking.linkingInfo.q_locacc = 0;
  outC->prvLRBG.infoFromPassing.valid = kcg_true;
  outC->prvLRBG.infoFromPassing.BG_Header.valid = kcg_true;
  outC->prvLRBG.infoFromPassing.BG_Header.q_updown =
    Q_UPDOWN_Down_link_telegram;
  outC->prvLRBG.infoFromPassing.BG_Header.m_version =
    M_VERSION_Previous_versions_according_to_e_g_EEIG_SRS_and_UIC_A200_SRS;
  outC->prvLRBG.infoFromPassing.BG_Header.q_media = Q_MEDIA_Balise;
  outC->prvLRBG.infoFromPassing.BG_Header.n_total =
    N_TOTAL_1_balise_in_the_group;
  outC->prvLRBG.infoFromPassing.BG_Header.m_mcount = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.nid_c = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.nid_bg = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.q_link = Q_LINK_Unlinked;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.valid = kcg_true;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.timestamp = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.odo.o_nominal = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.odo.o_min = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.odo.o_max = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_safeNominal = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_rawNominal = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_lower = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.speed.v_upper = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.acceleration = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.motionState =
    noMotion_Obu_BasicTypes_Pkg;
  outC->prvLRBG.infoFromPassing.BG_Header.bgPosition.motionDirection =
    unknownDirection_Obu_BasicTypes_Pkg;
  outC->prvLRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.nominal =
    0;
  outC->prvLRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_min =
    0;
  outC->prvLRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_max =
    0;
  outC->prvLRBG.infoFromPassing.BG_Header.q_nvlocacc = 0;
  outC->prvLRBG.infoFromPassing.BG_Header.noCoordinateSystemHasBeenAssigned =
    kcg_true;
  outC->prvLRBG.infoFromPassing.BG_Header.trainOrientationToBG =
    Q_DIRLRBG_Reverse;
  outC->prvLRBG.infoFromPassing.BG_Header.trainRunningDirectionToBG =
    Q_DIRTRAIN_Reverse;
  for (i = 0; i < 33; i++) {
    outC->prvLRBG.infoFromPassing.linkedBGs[i].valid = kcg_true;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].nid_LRBG = 0;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].q_dir = Q_DIR_Reverse;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].q_scale = Q_SCALE_10_cm_scale;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].d_link = 0;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].q_newcountry =
      Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].nid_c = 0;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].nid_bg = 0;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].q_linkorientation =
      Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].q_linkreaction =
      Q_LINKREACTION_Train_trip;
    outC->prvLRBG.infoFromPassing.linkedBGs[i].q_locacc = 0;
  }
  outC->prvLRBG.missed = kcg_true;
  outC->LRBG.valid = kcg_true;
  outC->LRBG.nid_c = 0;
  outC->LRBG.nid_bg = 0;
  outC->LRBG.q_link = Q_LINK_Unlinked;
  outC->LRBG.location.nominal = 0;
  outC->LRBG.location.d_min = 0;
  outC->LRBG.location.d_max = 0;
  outC->LRBG.seqNoOnTrack = 0;
  outC->LRBG.infoFromLinking.valid = kcg_true;
  outC->LRBG.infoFromLinking.nid_bg_fromLinkingBG = 0;
  outC->LRBG.infoFromLinking.nid_c_fromLinkingBG = 0;
  outC->LRBG.infoFromLinking.expectedLocation.nominal = 0;
  outC->LRBG.infoFromLinking.expectedLocation.d_min = 0;
  outC->LRBG.infoFromLinking.expectedLocation.d_max = 0;
  outC->LRBG.infoFromLinking.d_link.nominal = 0;
  outC->LRBG.infoFromLinking.d_link.d_min = 0;
  outC->LRBG.infoFromLinking.d_link.d_max = 0;
  outC->LRBG.infoFromLinking.linkingInfo.valid = kcg_true;
  outC->LRBG.infoFromLinking.linkingInfo.nid_LRBG = 0;
  outC->LRBG.infoFromLinking.linkingInfo.q_dir = Q_DIR_Reverse;
  outC->LRBG.infoFromLinking.linkingInfo.q_scale = Q_SCALE_10_cm_scale;
  outC->LRBG.infoFromLinking.linkingInfo.d_link = 0;
  outC->LRBG.infoFromLinking.linkingInfo.q_newcountry =
    Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
  outC->LRBG.infoFromLinking.linkingInfo.nid_c = 0;
  outC->LRBG.infoFromLinking.linkingInfo.nid_bg = 0;
  outC->LRBG.infoFromLinking.linkingInfo.q_linkorientation =
    Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
  outC->LRBG.infoFromLinking.linkingInfo.q_linkreaction =
    Q_LINKREACTION_Train_trip;
  outC->LRBG.infoFromLinking.linkingInfo.q_locacc = 0;
  outC->LRBG.infoFromPassing.valid = kcg_true;
  outC->LRBG.infoFromPassing.BG_Header.valid = kcg_true;
  outC->LRBG.infoFromPassing.BG_Header.q_updown = Q_UPDOWN_Down_link_telegram;
  outC->LRBG.infoFromPassing.BG_Header.m_version =
    M_VERSION_Previous_versions_according_to_e_g_EEIG_SRS_and_UIC_A200_SRS;
  outC->LRBG.infoFromPassing.BG_Header.q_media = Q_MEDIA_Balise;
  outC->LRBG.infoFromPassing.BG_Header.n_total = N_TOTAL_1_balise_in_the_group;
  outC->LRBG.infoFromPassing.BG_Header.m_mcount = 0;
  outC->LRBG.infoFromPassing.BG_Header.nid_c = 0;
  outC->LRBG.infoFromPassing.BG_Header.nid_bg = 0;
  outC->LRBG.infoFromPassing.BG_Header.q_link = Q_LINK_Unlinked;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.valid = kcg_true;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.timestamp = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.odo.o_nominal = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.odo.o_min = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.odo.o_max = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_safeNominal = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_rawNominal = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_lower = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.speed.v_upper = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.acceleration = 0;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.motionState =
    noMotion_Obu_BasicTypes_Pkg;
  outC->LRBG.infoFromPassing.BG_Header.bgPosition.motionDirection =
    unknownDirection_Obu_BasicTypes_Pkg;
  outC->LRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.nominal =
    0;
  outC->LRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_min =
    0;
  outC->LRBG.infoFromPassing.BG_Header.BG_centerDetectionInaccuraccuracies.d_max =
    0;
  outC->LRBG.infoFromPassing.BG_Header.q_nvlocacc = 0;
  outC->LRBG.infoFromPassing.BG_Header.noCoordinateSystemHasBeenAssigned =
    kcg_true;
  outC->LRBG.infoFromPassing.BG_Header.trainOrientationToBG = Q_DIRLRBG_Reverse;
  outC->LRBG.infoFromPassing.BG_Header.trainRunningDirectionToBG =
    Q_DIRTRAIN_Reverse;
  for (i = 0; i < 33; i++) {
    outC->LRBG.infoFromPassing.linkedBGs[i].valid = kcg_true;
    outC->LRBG.infoFromPassing.linkedBGs[i].nid_LRBG = 0;
    outC->LRBG.infoFromPassing.linkedBGs[i].q_dir = Q_DIR_Reverse;
    outC->LRBG.infoFromPassing.linkedBGs[i].q_scale = Q_SCALE_10_cm_scale;
    outC->LRBG.infoFromPassing.linkedBGs[i].d_link = 0;
    outC->LRBG.infoFromPassing.linkedBGs[i].q_newcountry =
      Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows;
    outC->LRBG.infoFromPassing.linkedBGs[i].nid_c = 0;
    outC->LRBG.infoFromPassing.linkedBGs[i].nid_bg = 0;
    outC->LRBG.infoFromPassing.linkedBGs[i].q_linkorientation =
      Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction;
    outC->LRBG.infoFromPassing.linkedBGs[i].q_linkreaction =
      Q_LINKREACTION_Train_trip;
    outC->LRBG.infoFromPassing.linkedBGs[i].q_locacc = 0;
  }
  outC->LRBG.missed = kcg_true;
}
#endif /* KCG_USER_DEFINED_INIT */


void SnapLRBG_reset_SDM_Types_Pkg(outC_SnapLRBG_SDM_Types_Pkg *outC)
{
  outC->init = kcg_true;
}

/* SDM_Types_Pkg::SnapLRBG */
void SnapLRBG_SDM_Types_Pkg(
  /* SDM_Types_Pkg::SnapLRBG::o_nom */ L_internal_Type_Obu_BasicTypes_Pkg o_nom,
  outC_SnapLRBG_SDM_Types_Pkg *outC)
{
  /* fby_1_init_1 */ if (outC->init) {
    outC->init = kcg_false;
    kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
      &outC->prvLRBG,
      (positionedBG_T_TrainPosition_Types_Pck *)
        &cNoPositionedBG_CalculateTrainPosition_Pkg);
  }
  else {
    kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
      &outC->prvLRBG,
      &outC->LRBG);
  }
  kcg_copy_positionedBG_T_TrainPosition_Types_Pck(
    &outC->LRBG,
    (positionedBG_T_TrainPosition_Types_Pck *)
      &cNoPositionedBG_CalculateTrainPosition_Pkg);
  outC->LRBG.valid = kcg_true;
  outC->LRBG.location.nominal = o_nom;
  outC->LRBG.location.d_min = - _cMinBaliseAccuracy_SDM_Types_Pkg;
  outC->LRBG.location.d_max = _cMinBaliseAccuracy_SDM_Types_Pkg;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** SnapLRBG_SDM_Types_Pkg.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

