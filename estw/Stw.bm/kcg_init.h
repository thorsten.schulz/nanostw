/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _KCG_INIT_H_
#define _KCG_INIT_H_

#define ProbeSDM_init_EnvSim ProbeSDM_reset_EnvSim

#endif /* _KCG_INIT_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** kcg_init.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

