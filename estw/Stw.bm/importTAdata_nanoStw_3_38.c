/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "importTAdata_nanoStw_3_38.h"

#ifndef KCG_USER_DEFINED_INIT
void importTAdata_init_nanoStw_3_38(outC_importTAdata_nanoStw_3_38 *outC)
{
  kcg_int i1;
  kcg_int i;
  
  outC->dataOk = kcg_true;
  outC->init = kcg_true;
  outC->correctionPoint.sid = 0;
  outC->correctionPoint.distance = 0;
  outC->correctionPoint.location = 0;
  for (i1 = 0; i1 < 38; i1++) {
    outC->tracks[i1].sid = 0;
    outC->tracks[i1].l = 0;
    outC->tracks[i1].vStraight = 0;
    outC->tracks[i1].vTurn = 0;
    for (i = 0; i < 4; i++) {
      outC->tracks[i1].line[i].to = 0;
      outC->tracks[i1].line[i].rLine = 0;
      outC->tracks[i1].line[i].on = kcg_true;
    }
  }
  for (i1 = 0; i1 < 3; i1++) {
    outC->otherTrains[i1].tid = 0;
    outC->otherTrains[i1].protectedLocationFront.sid = 0;
    outC->otherTrains[i1].protectedLocationFront.distance = 0;
    outC->otherTrains[i1].protectedLocationFront.location = 0;
    outC->otherTrains[i1].protectedLocationRear.sid = 0;
    outC->otherTrains[i1].protectedLocationRear.distance = 0;
    outC->otherTrains[i1].protectedLocationRear.location = 0;
  }
  outC->thisTrain.frontEnd.sid = 0;
  outC->thisTrain.frontEnd.distance = 0;
  outC->thisTrain.frontEnd.location = 0;
  outC->thisTrain.antenna.sid = 0;
  outC->thisTrain.antenna.distance = 0;
  outC->thisTrain.antenna.location = 0;
  outC->thisTrain.rearEnd.sid = 0;
  outC->thisTrain.rearEnd.distance = 0;
  outC->thisTrain.rearEnd.location = 0;
  outC->thisTrain.velocity = 0;
  outC->thisTrain.acc = 0;
  outC->thisTrain.flag.active = kcg_true;
  outC->thisTrain.flag.CW = kcg_true;
  outC->thisTrain.flag.tripped = kcg_true;
  outC->thisTrain.flag.stopped = kcg_true;
  outC->thisTrain.BrPc = 0;
  outC->thisTrain.vMax = 0;
  outC->thisTrain.vRelease = 0;
  outC->thisTrain.vOnSight = 0;
  outC->thisTrain.length = 0;
  outC->thisTrain.Ant2Cab = 0;
  outC->thisTrain.TrainId = 0;
  outC->TD.location = 0;
  outC->TD.velocity = 0;
  outC->TD.acc = 0;
  outC->TD.time = 0;
  outC->TD.flag.active = kcg_true;
  outC->TD.flag.CW = kcg_true;
  outC->TD.flag.tripped = kcg_true;
  outC->TD.flag.stopped = kcg_true;
  outC->TD.BrPc = 0;
  outC->TD.vMax = 0;
  outC->TD.vRelease = 0;
  outC->TD.vOnSight = 0;
  outC->TD.length = 0;
  outC->TD.Ant2Cab = 0;
  outC->TD.TrainId = 0;
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void importTAdata_reset_nanoStw_3_38(outC_importTAdata_nanoStw_3_38 *outC)
{
  outC->init = kcg_true;
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/* nanoStw::importTAdata */
void importTAdata_nanoStw_3_38(
  /* nanoStw::importTAdata::tracksIn */ array_21324 *tracksIn,
  /* nanoStw::importTAdata::tracksLen */ kcg_int tracksLen,
  /* nanoStw::importTAdata::allPR */ array_21308 *allPR,
  /* nanoStw::importTAdata::PRs */ kcg_int PRs,
  /* nanoStw::importTAdata::trainReport */ TrainReport_nanoStw_trdp *trainReport,
  outC_importTAdata_nanoStw_3_38 *outC)
{
  /* nanoStw::importTAdata */ kcg_bool cond_iterw;
  /* nanoStw::importTAdata::_L60 */ kcg_int _L60;
  /* nanoStw::importTAdata::_L73 */ kcg_bool _L73;
  kcg_int i;
  
  /* 1 */ if ((38 >= tracksLen) & (tracksLen > 0)) {
    kcg_copy_array_21324(&outC->tracks, tracksIn);
  }
  else /* last_init_ck_Tracks */ if (outC->init) {
    for (i = 0; i < 38; i++) {
      kcg_copy_TrackKnot_nanoStw(
        &outC->tracks[i],
        (TrackKnot_nanoStw *) &emptyTrack_nanoStw);
    }
  }
  outC->init = kcg_false;
  /* 1 */
  newOwnPosition_nanoStw_38(
    trainReport,
    &outC->tracks,
    &outC->TD,
    &outC->thisTrain,
    &outC->correctionPoint);
  _L73 = (3 >= PRs) & (PRs > 0);
  /* 2 */ if (_L73) {
    /* 2 */ for (i = 0; i < 3; i++) {
      /* 1 */
      newPR_nanoStw_38(
        i,
        &(*allPR)[i],
        PRs,
        outC->TD.TrainId,
        &outC->tracks,
        &cond_iterw,
        &outC->otherTrains[i]);
      _L60 = i + 1;
      /* 2 */ if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    _L60 = 0;
  }
#ifdef KCG_MAPW_CPY
  
  /* 2 */ for (i = _L60; i < 3; i++) {
    kcg_copy_otherTrainPos_nanoStw(
      &outC->otherTrains[i],
      (otherTrainPos_nanoStw *) &emptyOtherPos_nanoStw);
  }
#endif /* KCG_MAPW_CPY */
  
  outC->dataOk = (outC->tracks[0].sid != UnknownSid_nanoStw) & _L73;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** importTAdata_nanoStw_3_38.c
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

