/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "splitMRSP_nanoStw_35.h"

/* nanoStw::splitMRSP */
void splitMRSP_nanoStw_35(
  /* nanoStw::splitMRSP::i */ kcg_int i,
  /* nanoStw::splitMRSP::TS_MRSP */ array_21021 *TS_MRSP,
  /* nanoStw::splitMRSP::TSS */ TrainSpeedSegment_nanoStw *TSS,
  /* nanoStw::splitMRSP::frontSectionOffset */ kcg_int frontSectionOffset,
  /* nanoStw::splitMRSP::section */ TrackSection_SpeedSupervision_UnitTest_Pkg *section,
  /* nanoStw::splitMRSP::sid */ TrackId_nanoStw *sid)
{
  /* nanoStw::splitMRSP::_L10 */ TrainSpeedSegment_nanoStw _L10;
  
  if ((0 <= i + frontSectionOffset) & (i + frontSectionOffset < 35)) {
    kcg_copy_TrainSpeedSegment_nanoStw(
      &_L10,
      &(*TS_MRSP)[i + frontSectionOffset]);
  }
  else {
    kcg_copy_TrainSpeedSegment_nanoStw(
      &_L10,
      (TrainSpeedSegment_nanoStw *) &emptySpeedSegment_nanoStw);
  }
  (*section).length = _L10.location;
  (*section).speed = _L10.speed;
  *sid = (*TSS).first.sid;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** splitMRSP_nanoStw_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

