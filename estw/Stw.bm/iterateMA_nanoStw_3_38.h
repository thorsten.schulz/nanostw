/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _iterateMA_nanoStw_3_38_H_
#define _iterateMA_nanoStw_3_38_H_

#include "kcg_types.h"
#include "checkCollision_nanoStw.h"
#include "nextTrackKnot_nanoStw_38.h"
#include "guessTrackKnotLine_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::iterateMA */
extern void iterateMA_nanoStw_3_38(
  /* nanoStw::iterateMA::pos */ TrackLocation_nanoStw *pos,
  /* nanoStw::iterateMA::runsCW */ kcg_bool runsCW,
  /* nanoStw::iterateMA::vMax */ VelocityUnit vMax,
  /* nanoStw::iterateMA::vOS */ VelocityUnit vOS,
  /* nanoStw::iterateMA::tracks */ array_21324 *tracks,
  /* nanoStw::iterateMA::otherTrains */ array_21564 *otherTrains,
  /* nanoStw::iterateMA::nloc */ TrackLocation_nanoStw *nloc,
  /* nanoStw::iterateMA::SpeedSegment */ TrainSpeedSegment_nanoStw *SpeedSegment);

#endif /* _iterateMA_nanoStw_3_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** iterateMA_nanoStw_3_38.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

