/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _GenerateTrack_SpeedSupervision_UnitTest_Pkg_35_H_
#define _GenerateTrack_SpeedSupervision_UnitTest_Pkg_35_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* ---------------------------  outputs  --------------------------- */
  DataForSupervision_nextGen_t_TrackAtlasTypes /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::TA */ TA;
  /* -----------------------  no local probes  ----------------------- */
  /* -------------------- initialization variables  ------------------ */
  kcg_bool init;
  /* ----------------------- local memories  ------------------------- */
  L_internal_Type_Obu_BasicTypes_Pkg /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::eoa */ rem_eoa;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35;

/* ===========  node initialization and cycle functions  =========== */
/* SpeedSupervision_UnitTest_Pkg::GenerateTrack */
extern void GenerateTrack_SpeedSupervision_UnitTest_Pkg_35(
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::eoa */ L_internal_Type_Obu_BasicTypes_Pkg eoa,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::v_release */ V_internal_Type_Obu_BasicTypes_Pkg v_release,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::freshGrad */ kcg_bool freshGrad,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::Grad */ GradientProfile_t_TrackAtlasTypes *Grad,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::freshMRSP */ kcg_bool freshMRSP,
  /* SpeedSupervision_UnitTest_Pkg::GenerateTrack::MRSP */ MRSP_Profile_t_TrackAtlasTypes *MRSP,
  outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35 *outC);

extern void GenerateTrack_reset_SpeedSupervision_UnitTest_Pkg_35(
  outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void GenerateTrack_init_SpeedSupervision_UnitTest_Pkg_35(
  outC_GenerateTrack_SpeedSupervision_UnitTest_Pkg_35 *outC);
#endif /* KCG_USER_DEFINED_INIT */

#endif /* _GenerateTrack_SpeedSupervision_UnitTest_Pkg_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** GenerateTrack_SpeedSupervision_UnitTest_Pkg_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

