/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _exportTrackSide_nanoStw_35_H_
#define _exportTrackSide_nanoStw_35_H_

#include "kcg_types.h"
#include "splitMRSP_nanoStw_35.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::exportTrackSide */
extern void exportTrackSide_nanoStw_35(
  /* nanoStw::exportTrackSide::sections */ array_21021 *sections,
  /* nanoStw::exportTrackSide::frontSid */ TrackId_nanoStw frontSid,
  /* nanoStw::exportTrackSide::eStwToSDM_sections */ array_21157 *eStwToSDM_sections,
  /* nanoStw::exportTrackSide::MAsids */ array_int_35 *MAsids);

#endif /* _exportTrackSide_nanoStw_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** exportTrackSide_nanoStw_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

