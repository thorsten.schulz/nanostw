/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _normalizeTrackLocation_it_nanoStw_38_H_
#define _normalizeTrackLocation_it_nanoStw_38_H_

#include "kcg_types.h"
#include "selectTrackBySid_nanoStw_38.h"
#include "nextTrackKnot_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::normalizeTrackLocation_it */
extern void normalizeTrackLocation_it_nanoStw_38(
  /* nanoStw::normalizeTrackLocation_it::loc */ TrackLocation_nanoStw *loc,
  /* nanoStw::normalizeTrackLocation_it::tracks */ array_21324 *tracks,
  /* nanoStw::normalizeTrackLocation_it::again */ kcg_bool *again,
  /* nanoStw::normalizeTrackLocation_it::nloc */ TrackLocation_nanoStw *nloc);

#endif /* _normalizeTrackLocation_it_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** normalizeTrackLocation_it_nanoStw_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

