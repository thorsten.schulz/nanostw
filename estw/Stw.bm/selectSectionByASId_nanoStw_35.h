/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _selectSectionByASId_nanoStw_35_H_
#define _selectSectionByASId_nanoStw_35_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::selectSectionByASId */
extern void selectSectionByASId_nanoStw_35(
  /* nanoStw::selectSectionByASId::absId */ TrackId_nanoStw absId,
  /* nanoStw::selectSectionByASId::speedSegments */ array_21021 *speedSegments,
  /* nanoStw::selectSectionByASId::sectionLocation */ TrackSection_nanoStw *sectionLocation,
  /* nanoStw::selectSectionByASId::found */ kcg_bool *found);

#endif /* _selectSectionByASId_nanoStw_35_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** selectSectionByASId_nanoStw_35.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

