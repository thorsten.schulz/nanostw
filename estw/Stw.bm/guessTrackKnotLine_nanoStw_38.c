/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "guessTrackKnotLine_nanoStw_38.h"

/* nanoStw::guessTrackKnotLine */
void guessTrackKnotLine_nanoStw_38(
  /* nanoStw::guessTrackKnotLine::loc */ TrackLocation_nanoStw *loc,
  /* nanoStw::guessTrackKnotLine::tracks */ array_21324 *tracks,
  /* nanoStw::guessTrackKnotLine::vOnSight */ VelocityUnit vOnSight,
  /* nanoStw::guessTrackKnotLine::vMax */ VelocityUnit *vMax,
  /* nanoStw::guessTrackKnotLine::track */ TrackKnot_nanoStw *track)
{
  /* nanoStw::guessTrackKnotLine::_L13 */ TrackKnot_nanoStw _L13;
  /* nanoStw::guessTrackKnotLine::_L28 */ kcg_bool _L28;
  
  /* 1 */ selectTrackBySid_nanoStw_38((*loc).sid, tracks, &_L13, &_L28);
  _L28 = (*loc).sid != UnknownSid_nanoStw;
  /* 2 */ if (_L28) {
    /* 1 */ if (_L13.line[2].on | _L13.line[3].on) {
      *vMax = _L13.vTurn;
    }
    else {
      *vMax = _L13.vStraight;
    }
    kcg_copy_TrackKnot_nanoStw(track, &_L13);
  }
  else {
    *vMax = vOnSight;
    kcg_copy_TrackKnot_nanoStw(
      track,
      (TrackKnot_nanoStw *) &emptyTrack_nanoStw);
    (*track).l = DEFAULT_OS_MA_nanoStw;
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** guessTrackKnotLine_nanoStw_38.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

