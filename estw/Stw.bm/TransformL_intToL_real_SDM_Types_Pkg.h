/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _TransformL_intToL_real_SDM_Types_Pkg_H_
#define _TransformL_intToL_real_SDM_Types_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* SDM_Types_Pkg::TransformL_intToL_real */
extern L_internal_real_Type_SDM_Types_Pkg TransformL_intToL_real_SDM_Types_Pkg(
  /* SDM_Types_Pkg::TransformL_intToL_real::loc_int */ L_internal_Type_Obu_BasicTypes_Pkg loc_int);

#endif /* _TransformL_intToL_real_SDM_Types_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** TransformL_intToL_real_SDM_Types_Pkg.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

