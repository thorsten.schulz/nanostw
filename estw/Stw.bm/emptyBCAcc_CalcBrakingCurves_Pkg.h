/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _emptyBCAcc_CalcBrakingCurves_Pkg_H_
#define _emptyBCAcc_CalcBrakingCurves_Pkg_H_

#include "kcg_types.h"
#include "makeEmptyParabolaCurve_CalcBrakingCurves_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* CalcBrakingCurves_Pkg::emptyBCAcc */
extern void emptyBCAcc_CalcBrakingCurves_Pkg(
  /* CalcBrakingCurves_Pkg::emptyBCAcc::Output1 */ BCAccumulator_type_CalcBrakingCurves_Pkg *Output1);

#endif /* _emptyBCAcc_CalcBrakingCurves_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** emptyBCAcc_CalcBrakingCurves_Pkg.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

