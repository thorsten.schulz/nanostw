/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _exportProtectionLocations_nanoStw_38_H_
#define _exportProtectionLocations_nanoStw_38_H_

#include "kcg_types.h"
#include "projectLocation_nanoStw.h"
#include "normalizeTrackLocation_it_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::exportProtectionLocations */
extern void exportProtectionLocations_nanoStw_38(
  /* nanoStw::exportProtectionLocations::Tlocs */ ProtectionLocations_SpeedSupervision_UnitTest_Pkg *Tlocs,
  /* nanoStw::exportProtectionLocations::train */ simpleTrainPos_nanoStw *train,
  /* nanoStw::exportProtectionLocations::EBintegrity */ kcg_bool EBintegrity,
  /* nanoStw::exportProtectionLocations::tracks */ array_21324 *tracks,
  /* nanoStw::exportProtectionLocations::out */ ProtectionLocations_nanoStw_trdp *out);

#endif /* _exportProtectionLocations_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** exportProtectionLocations_nanoStw_38.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

