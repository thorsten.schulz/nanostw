/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.h"

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA */
void MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI(
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::i */ kcg_int i,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::MRSP */ DMI_SpeedProfileArray_T_DMI_Types_Pkg *MRSP,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::EoA */ L_internal_Type_Obu_BasicTypes_Pkg EoA,
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::limited */ DMI_speedProfileElement_T_DMI_Types_Pkg *limited)
{
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA */ DMI_speedProfileElement_T_DMI_Types_Pkg tmp;
  /* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::MRSP_limit_to_EOA::MRSP_section0 */ DMI_speedProfileElement_T_DMI_Types_Pkg MRSP_section0;
  
  if ((0 <= i) & (i < 32)) {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(&tmp, &(*MRSP)[i]);
  }
  else {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(
      &tmp,
      (DMI_speedProfileElement_T_DMI_Types_Pkg *)
        &DEFAULT_MRSP_section_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI);
  }
  /* 1 */ if (tmp.Loc_Abs > EoA) {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(&MRSP_section0, &tmp);
    MRSP_section0.Loc_Abs = EoA;
  }
  else {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(&MRSP_section0, &tmp);
  }
  if ((0 <= i - 1) & (i - 1 < 32)) {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(&tmp, &(*MRSP)[i - 1]);
  }
  else {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(
      &tmp,
      (DMI_speedProfileElement_T_DMI_Types_Pkg *)
        &DEFAULT_MRSP_section_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI);
  }
  /* 3 */ if (tmp.Loc_Abs == END_OF_SSP_encoding_TA_Export) {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(
      limited,
      (DMI_speedProfileElement_T_DMI_Types_Pkg *)
        &DEFAULT_MRSP_section_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI);
  }
  else {
    kcg_copy_DMI_speedProfileElement_T_DMI_Types_Pkg(limited, &MRSP_section0);
    /* 2 */ if ((EoA == MRSP_section0.Loc_Abs) | (END_OF_SSP_TA_Export ==
        MRSP_section0.MRS)) {
      (*limited).MRS = END_OF_SSP_encoding_TA_Export;
    }
    else {
      (*limited).MRS = MRSP_section0.MRS;
    }
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** MRSP_limit_to_EOA_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

