/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _newPR_nanoStw_38_H_
#define _newPR_nanoStw_38_H_

#include "kcg_types.h"
#include "isLocationNormalized_nanoStw_38.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::newPR */
extern void newPR_nanoStw_38(
  /* nanoStw::newPR::i */ kcg_int i,
  /* nanoStw::newPR::data */ ProtectionLocations_nanoStw_trdp *data,
  /* nanoStw::newPR::length */ kcg_int length,
  /* nanoStw::newPR::IdToBlank */ TrackId_nanoStw IdToBlank,
  /* nanoStw::newPR::tracks */ array_21324 *tracks,
  /* nanoStw::newPR::goOn */ kcg_bool *goOn,
  /* nanoStw::newPR::trains */ otherTrainPos_nanoStw *trains);

#endif /* _newPR_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** newPR_nanoStw_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

