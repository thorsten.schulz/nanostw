/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _selectTrackBySid_nanoStw_38_H_
#define _selectTrackBySid_nanoStw_38_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::selectTrackBySid */
extern void selectTrackBySid_nanoStw_38(
  /* nanoStw::selectTrackBySid::sid */ TrackId_nanoStw sid,
  /* nanoStw::selectTrackBySid::tracks */ array_21324 *tracks,
  /* nanoStw::selectTrackBySid::track */ TrackKnot_nanoStw *track,
  /* nanoStw::selectTrackBySid::found */ kcg_bool *found);

#endif /* _selectTrackBySid_nanoStw_38_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** selectTrackBySid_nanoStw_38.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

