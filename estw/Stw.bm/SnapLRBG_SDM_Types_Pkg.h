/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _SnapLRBG_SDM_Types_Pkg_H_
#define _SnapLRBG_SDM_Types_Pkg_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* ---------------------------  outputs  --------------------------- */
  positionedBG_T_TrainPosition_Types_Pck /* SDM_Types_Pkg::SnapLRBG::LRBG */ LRBG;
  positionedBG_T_TrainPosition_Types_Pck /* SDM_Types_Pkg::SnapLRBG::prvLRBG */ prvLRBG;
  /* -----------------------  no local probes  ----------------------- */
  /* -------------------- initialization variables  ------------------ */
  kcg_bool init;
  /* -----------------------  no local memory  ----------------------- */
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_SnapLRBG_SDM_Types_Pkg;

/* ===========  node initialization and cycle functions  =========== */
/* SDM_Types_Pkg::SnapLRBG */
extern void SnapLRBG_SDM_Types_Pkg(
  /* SDM_Types_Pkg::SnapLRBG::o_nom */ L_internal_Type_Obu_BasicTypes_Pkg o_nom,
  outC_SnapLRBG_SDM_Types_Pkg *outC);

extern void SnapLRBG_reset_SDM_Types_Pkg(outC_SnapLRBG_SDM_Types_Pkg *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void SnapLRBG_init_SDM_Types_Pkg(outC_SnapLRBG_SDM_Types_Pkg *outC);
#endif /* KCG_USER_DEFINED_INIT */

#endif /* _SnapLRBG_SDM_Types_Pkg_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** SnapLRBG_SDM_Types_Pkg.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

