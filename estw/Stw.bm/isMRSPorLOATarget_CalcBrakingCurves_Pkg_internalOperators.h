/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */
#ifndef _isMRSPorLOATarget_CalcBrakingCurves_Pkg_internalOperators_H_
#define _isMRSPorLOATarget_CalcBrakingCurves_Pkg_internalOperators_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* CalcBrakingCurves_Pkg::internalOperators::isMRSPorLOATarget */
extern kcg_bool isMRSPorLOATarget_CalcBrakingCurves_Pkg_internalOperators(
  /* CalcBrakingCurves_Pkg::internalOperators::isMRSPorLOATarget::Target */ Target_real_T_TargetManagement_types *Target);

#endif /* _isMRSPorLOATarget_CalcBrakingCurves_Pkg_internalOperators_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** isMRSPorLOATarget_CalcBrakingCurves_Pkg_internalOperators.h
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

