/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "selectSectionByASId_nanoStw_35.h"

/* nanoStw::selectSectionByASId */
void selectSectionByASId_nanoStw_35(
  /* nanoStw::selectSectionByASId::absId */ TrackId_nanoStw absId,
  /* nanoStw::selectSectionByASId::speedSegments */ array_21021 *speedSegments,
  /* nanoStw::selectSectionByASId::sectionLocation */ TrackSection_nanoStw *sectionLocation,
  /* nanoStw::selectSectionByASId::found */ kcg_bool *found)
{
  /* nanoStw::selectSectionByASId */ TrackId_nanoStw acc;
  kcg_int i;
  /* nanoStw::selectSectionByASId */ TrainSpeedSegment_nanoStw tmp2;
  /* nanoStw::selectSectionByASId */ kcg_int tmp1;
  /* nanoStw::selectSectionByASId */ TrackId_nanoStw tmp;
  /* nanoStw::findSidInMA::_L8 */ kcg_bool _L8_2;
  /* nanoStw::helper::dec::i */ kcg_int i_1_2;
  
  _L8_2 = UnknownSid_nanoStw != absId;
  /* 1 */ if (_L8_2) {
    /* 2_1 */ for (i = 0; i < 35; i++) {
      acc = absId;
      /* 2 */ if (0 <= (*speedSegments)[i].first.sid) {
        tmp = (*speedSegments)[i].first.sid;
      }
      else {
        tmp = - (*speedSegments)[i].first.sid;
      }
      i_1_2 = i + 1;
      /* 2_1 */ if (!(acc != tmp)) {
        break;
      }
    }
    tmp1 = - 1 + i_1_2;
  }
  else {
    tmp1 = 35;
  }
  if ((0 <= tmp1) & (tmp1 < 35)) {
    kcg_copy_TrainSpeedSegment_nanoStw(&tmp2, &(*speedSegments)[tmp1]);
  }
  else {
    kcg_copy_TrainSpeedSegment_nanoStw(
      &tmp2,
      (TrainSpeedSegment_nanoStw *) &emptySpeedSegment_nanoStw);
  }
  kcg_copy_TrackSection_nanoStw(sectionLocation, &tmp2.first);
  /* 2 */ if (0 <= (*sectionLocation).sid) {
    tmp = (*sectionLocation).sid;
  }
  else {
    tmp = - (*sectionLocation).sid;
  }
  *found = tmp == absId;
}

/* $**************** KCG Version 6.4 (build i21) ****************
** selectSectionByASId_nanoStw_35.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

