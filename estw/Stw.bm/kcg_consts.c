/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

#include "kcg_consts.h"

/* nanoStw::emptyTrainData */
const TrainData_SpeedSupervision_UnitTest_Pkg emptyTrainData_nanoStw = { 0, 0,
  0, 0, { kcg_false, kcg_false, kcg_false, kcg_false }, 0, 0, 0, 0, 0, 0,
  invalidTid_nanoStw };

/* nanoStw::emptySimplePos */
const simpleTrainPos_nanoStw emptySimplePos_nanoStw = { { 0, 0, 0 }, { 0, 0,
    0 }, { 0, 0, 0 }, 0, 0, { kcg_false, kcg_false, kcg_false, kcg_false }, 0,
  0, 0, 0, 0, 0, invalidTid_nanoStw };

/* nanoStw::invalidLocation */
const TrackLocation_nanoStw invalidLocation_nanoStw = { 0, 0, 0 };

/* nanoStw::emptyOtherPos */
const otherTrainPos_nanoStw emptyOtherPos_nanoStw = { nil_nanoStw, { 0, 0, 0 },
  { 0, 0, 0 } };

/* nanoStw::trdp::PRstr8null */
const array_char_10 PRstr8null_nanoStw_trdp = { 'F', 'l', 'i', 'r', 't', 'O',
  'K', '\xc3', '\xbc', '\x00' };

/* nanoStw::trdp::PRstr16null */
const array_int_8 PRstr16null_nanoStw_trdp = { 0x46, 0x6c, 0x69, 0x72, 0x74,
  0x4F, 0x4B, 0xFC };

/* nanoStw::emptySpeedSegment */
const TrainSpeedSegment_nanoStw emptySpeedSegment_nanoStw = { 0, 0, { 0, 0,
    0 } };

/* nanoStw::emptyTrackLine */
const TrackKnotLine_nanoStw emptyTrackLine_nanoStw = { 0, 0, kcg_false };

/* nanoStw::emptyTrack */
const TrackKnot_nanoStw emptyTrack_nanoStw = { 0, 0, 0, 0, { { 0, 0,
      kcg_false }, { 0, 0, kcg_false }, { 0, 0, kcg_false }, { 0, 0,
      kcg_false } } };

/* nanoStw::trdp::emptyTrainRelLimitLocations */
const TrainRelLimitLocations_nanoStw_trdp emptyTrainRelLimitLocations_nanoStw_trdp = {
  0, 0, 0, 0, 0, 0, 0, 0 };

/* SpeedSupervision_UnitTest_Pkg::NVnull */
const P3_NationalValues_T_Packet_Types_Pkg NVnull_SpeedSupervision_UnitTest_Pkg = {
  kcg_false, Q_DIR_Reverse, 0, { { kcg_false, 0 }, { kcg_false, 0 }, {
      kcg_false, 0 }, { kcg_false, 0 }, { kcg_false, 0 }, { kcg_false, 0 }, {
      kcg_false, 0 } }, 0, 0, 0, 0, 0, 0, 0, Q_NVSBTSMPERM_No,
  Q_NVEMRRLS_Revoke_emergency_brake_command_at_standstill, Q_NVGUIPERM_No,
  Q_NVSBFBPERM_No, Q_NVINHSMICPERM_No, 0, 0, 0, 0, 0, M_NVCONTACT_Train_trip, 0,
  M_NVDERUN_No, 0, Q_NVDRIVER_ADHES_Not_allowed, 0.0, 0.0, 0.0, 0, 0.0,
  M_NVEBCL_Confidence_level_50,
  Q_NVKINT_No_integrated_correction_factors_follow, { { kcg_false,
      Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0,
          0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 } } }, { kcg_false,
      Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0,
          0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 } } }, { kcg_false,
      Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0,
          0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 } } }, { kcg_false,
      Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0,
          0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 } } }, { kcg_false,
      Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0,
          0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 } } }, { kcg_false,
      Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0,
          0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 } } }, { kcg_false,
      Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0,
          0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
          kcg_false, 0, 0.0, 0.0 } } } }, { { kcg_false, L_NVKRINT_0m, 0.0 }, {
      kcg_false, L_NVKRINT_0m, 0.0 }, { kcg_false, L_NVKRINT_0m, 0.0 }, {
      kcg_false, L_NVKRINT_0m, 0.0 }, { kcg_false, L_NVKRINT_0m, 0.0 }, {
      kcg_false, L_NVKRINT_0m, 0.0 }, { kcg_false, L_NVKRINT_0m, 0.0 } }, 0.0 };

/* SpeedSupervision_UnitTest_Pkg::cNoTrackCondition */
const DMI_trackCondition_T_DMI_Types_Pkg cNoTrackCondition_SpeedSupervision_UnitTest_Pkg = {
  0, { { 0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted },
    { 0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0, M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted }, {
      0,
      M_TRACKCOND_Non_stopping_area_Initial_state_is_stopping_permitted } } };

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::DEFAULT_MRSP_reduction_acc */
const MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI DEFAULT_MRSP_reduction_acc_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI = {
  0, { { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 }, {
      kcg_false, 0, 0, 0 }, { kcg_false, 0, 0, 0 } } };

/* TA_Export::DEFAULT_MRSP_section */
const MRSP_section_t_TrackAtlasTypes DEFAULT_MRSP_section_TA_Export = {
  kcg_false, 0, 0, 0 };

/* SpeedSupervision_UnitTest_Pkg::TA_Export_DMI::DEFAULT_MRSP_section */
const DMI_speedProfileElement_T_DMI_Types_Pkg DEFAULT_MRSP_section_SpeedSupervision_UnitTest_Pkg_TA_Export_DMI = {
  kcg_false, 0, 0, 0 };

/* TrackAtlasTypes::DEFAULT_Endtimer */
const Endtimer_t_TrackAtlasTypes DEFAULT_Endtimer_TrackAtlasTypes = { 0, 0 };

/* TrackAtlasTypes::DEFAULT_DP */
const DP_or_OL_t_TrackAtlasTypes DEFAULT_DP_TrackAtlasTypes = { 0, 0,
  kcg_false };

/* TrackAtlasTypes::DEFAULT_MA_sectionlist */
const MovementAuthoritySectionlist_t_TrackAtlasTypes DEFAULT_MA_sectionlist_TrackAtlasTypes = {
  { kcg_false, kcg_false, 0, kcg_false, 0, 0 }, { kcg_false, kcg_false, 0,
    kcg_false, 0, 0 } };

/* TargetManagement_pkg::emptyMRSPSection */
const MRSP_internal_section_T_TargetManagement_types emptyMRSPSection_TargetManagement_pkg = {
  0.0, 0.0, kcg_false };

/* TargetManagement_pkg::emptyTarget */
const Target_real_T_TargetManagement_types emptyTarget_TargetManagement_pkg = {
  invalid_TargetManagement_types, 0.0, 0.0 };

/* SDMConversionModelPkg::cBrakePositionGECoeff */
const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionGECoeff_SDMConversionModelPkg = {
  12.0, 0.0, 0.05 };

/* SDMConversionModelPkg::cBrakePositionFreightLongECoeff */
const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionFreightLongECoeff_SDMConversionModelPkg = {
  - 0.4, 1.6, 0.03 };

/* SDMConversionModelPkg::cBrakePositionFreightLongSCoeff */
const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionFreightLongSCoeff_SDMConversionModelPkg = {
  10.5, 0.32, 0.18 };

/* SDMConversionModelPkg::cBrakePositionPassengerSCoeff */
const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionPassengerSCoeff_SDMConversionModelPkg = {
  3.0, 1.5, 0.10 };

/* SDMConversionModelPkg::cBrakePositionFreightShortSCoeff */
const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionFreightShortSCoeff_SDMConversionModelPkg = {
  3.0, 2.77, 0.0 };

/* SDMConversionModelPkg::cBrakePositionPECoeff */
const coeff_BrakeBasic_t_SDMConversionModelPkg cBrakePositionPECoeff_SDMConversionModelPkg = {
  2.3, 0.0, 0.17 };

/* SDMConversionModelPkg::cLKrIntLookUp */
const LKrIntLookUp_t_SDMConversionModelPkg cLKrIntLookUp_SDMConversionModelPkg = {
  0, 25, 50, 75, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100,
  1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400,
  2500, 2600, 2700 };

/* SDMConversionModelPkg::cEmptyKvIntSet */
const nvkvintset_T_Packet_Types_Pkg cEmptyKvIntSet_SDMConversionModelPkg = {
  kcg_false, Q_NVKVINTSET_Freight_trains, 0.0, 0.0, { { kcg_false, 0, 0.0,
      0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, {
      kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0, 0.0 }, { kcg_false, 0, 0.0,
      0.0 }, { kcg_false, 0, 0.0, 0.0 } } };

/* SDMConversionModelPkg::cBrakePercentALookup */
const array_int_6_221 cBrakePercentALookup_SDMConversionModelPkg = { { 30, 25,
    29, 22, 21, 21 }, { 31, 25, 29, 22, 22, 21 }, { 32, 26, 30, 23, 22, 22 }, {
    32, 27, 30, 23, 23, 22 }, { 33, 27, 31, 24, 23, 23 }, { 34, 28, 32, 25, 24,
    24 }, { 35, 29, 32, 25, 24, 24 }, { 35, 29, 33, 26, 25, 25 }, { 36, 30, 33,
    26, 25, 25 }, { 37, 31, 34, 27, 26, 26 }, { 38, 31, 35, 27, 26, 26 }, { 38,
    32, 35, 28, 27, 27 }, { 39, 33, 36, 28, 28, 27 }, { 40, 33, 36, 29, 28,
    28 }, { 41, 34, 37, 30, 29, 28 }, { 41, 34, 38, 30, 29, 29 }, { 42, 35, 38,
    31, 30, 29 }, { 43, 36, 39, 31, 30, 30 }, { 44, 36, 40, 32, 31, 30 }, { 44,
    37, 40, 32, 31, 31 }, { 45, 38, 41, 33, 32, 31 }, { 46, 38, 42, 33, 32,
    32 }, { 47, 39, 42, 34, 33, 32 }, { 47, 39, 43, 35, 33, 33 }, { 48, 40, 44,
    35, 34, 33 }, { 49, 41, 44, 36, 34, 34 }, { 50, 41, 45, 36, 35, 35 }, { 50,
    42, 46, 37, 36, 35 }, { 51, 42, 47, 37, 36, 36 }, { 52, 43, 47, 38, 37,
    36 }, { 53, 43, 48, 38, 37, 37 }, { 53, 44, 49, 39, 38, 37 }, { 54, 44, 50,
    40, 38, 38 }, { 55, 45, 50, 40, 39, 38 }, { 56, 45, 51, 41, 39, 39 }, { 56,
    52, 41, 40, 39, 0 }, { 57, 53, 42, 40, 40, 0 }, { 58, 54, 42, 41, 40, 0 }, {
    59, 54, 43, 41, 41, 0 }, { 59, 55, 43, 42, 41, 0 }, { 60, 56, 44, 43, 42,
    0 }, { 61, 57, 45, 43, 42, 0 }, { 62, 58, 45, 44, 43, 0 }, { 62, 59, 46, 44,
    44, 0 }, { 63, 60, 46, 45, 44, 0 }, { 64, 61, 47, 45, 45, 0 }, { 65, 61, 47,
    46, 45, 0 }, { 65, 62, 48, 46, 46, 0 }, { 66, 63, 49, 47, 46, 0 }, { 67, 64,
    49, 47, 47, 0 }, { 68, 65, 50, 48, 47, 0 }, { 68, 66, 50, 49, 48, 0 }, { 69,
    67, 51, 49, 48, 0 }, { 70, 68, 52, 50, 49, 0 }, { 71, 69, 52, 50, 49, 0 }, {
    71, 70, 53, 51, 50, 0 }, { 72, 71, 53, 51, 51, 0 }, { 73, 72, 54, 52, 51,
    0 }, { 74, 73, 54, 52, 52, 0 }, { 74, 74, 55, 53, 52, 0 }, { 75, 75, 56, 54,
    53, 0 }, { 76, 77, 56, 54, 53, 0 }, { 77, 78, 57, 55, 54, 0 }, { 77, 79, 57,
    55, 54, 0 }, { 78, 80, 58, 56, 55, 0 }, { 79, 81, 59, 56, 55, 0 }, { 80, 82,
    59, 57, 56, 0 }, { 80, 84, 60, 57, 57, 0 }, { 81, 85, 60, 58, 57, 0 }, { 82,
    86, 61, 59, 58, 0 }, { 83, 62, 59, 58, 0, 0 }, { 83, 62, 60, 59, 0, 0 }, {
    84, 63, 60, 59, 0, 0 }, { 85, 64, 61, 60, 0, 0 }, { 86, 64, 61, 60, 0, 0 },
  { 86, 65, 62, 61, 0, 0 }, { 87, 65, 63, 61, 0, 0 }, { 88, 66, 63, 62, 0, 0 },
  { 89, 67, 64, 63, 0, 0 }, { 89, 67, 64, 63, 0, 0 }, { 90, 68, 65, 64, 0, 0 },
  { 91, 69, 65, 64, 0, 0 }, { 92, 69, 66, 65, 0, 0 }, { 92, 70, 67, 65, 0, 0 },
  { 93, 71, 67, 66, 0, 0 }, { 94, 71, 68, 66, 0, 0 }, { 95, 72, 68, 67, 0, 0 },
  { 95, 72, 69, 68, 0, 0 }, { 96, 73, 70, 68, 0, 0 }, { 97, 74, 70, 69, 0, 0 },
  { 98, 74, 71, 69, 0, 0 }, { 98, 75, 71, 70, 0, 0 }, { 99, 76, 72, 70, 0, 0 },
  { 100, 76, 72, 71, 0, 0 }, { 101, 77, 73, 72, 0, 0 }, { 101, 78, 74, 72, 0,
    0 }, { 102, 78, 74, 73, 0, 0 }, { 103, 79, 75, 73, 0, 0 }, { 104, 80, 75,
    74, 0, 0 }, { 104, 80, 76, 74, 0, 0 }, { 105, 81, 77, 75, 0, 0 }, { 106, 82,
    77, 75, 0, 0 }, { 107, 83, 78, 76, 0, 0 }, { 107, 83, 78, 77, 0, 0 }, { 108,
    84, 79, 77, 0, 0 }, { 109, 85, 80, 78, 0, 0 }, { 110, 85, 80, 78, 0, 0 }, {
    110, 86, 81, 79, 0, 0 }, { 111, 87, 82, 79, 0, 0 }, { 112, 87, 82, 80, 0,
    0 }, { 113, 88, 83, 81, 0, 0 }, { 113, 89, 83, 81, 0, 0 }, { 114, 90, 84,
    82, 0, 0 }, { 115, 90, 85, 82, 0, 0 }, { 116, 91, 85, 83, 0, 0 }, { 116, 92,
    86, 83, 0, 0 }, { 117, 93, 87, 84, 0, 0 }, { 118, 93, 87, 85, 0, 0 }, { 119,
    94, 88, 85, 0, 0 }, { 119, 95, 88, 86, 0, 0 }, { 120, 96, 89, 86, 0, 0 }, {
    121, 96, 90, 87, 0, 0 }, { 122, 97, 90, 87, 0, 0 }, { 122, 98, 91, 88, 0,
    0 }, { 123, 99, 92, 89, 0, 0 }, { 124, 99, 92, 89, 0, 0 }, { 125, 100, 93,
    90, 0, 0 }, { 125, 101, 94, 90, 0, 0 }, { 126, 102, 94, 91, 0, 0 }, { 127,
    103, 95, 92, 0, 0 }, { 128, 103, 96, 92, 0, 0 }, { 128, 104, 96, 93, 0, 0 },
  { 129, 105, 97, 93, 0, 0 }, { 130, 106, 98, 94, 0, 0 }, { 131, 107, 98, 94, 0,
    0 }, { 131, 107, 99, 95, 0, 0 }, { 132, 108, 100, 96, 0, 0 }, { 133, 100,
    96, 0, 0, 0 }, { 134, 101, 97, 0, 0, 0 }, { 134, 102, 97, 0, 0, 0 }, { 135,
    102, 98, 0, 0, 0 }, { 136, 103, 99, 0, 0, 0 }, { 137, 104, 99, 0, 0, 0 }, {
    137, 104, 100, 0, 0, 0 }, { 138, 105, 100, 0, 0, 0 }, { 139, 106, 101, 0, 0,
    0 }, { 140, 106, 102, 0, 0, 0 }, { 140, 107, 102, 0, 0, 0 }, { 141, 108,
    103, 0, 0, 0 }, { 142, 108, 103, 0, 0, 0 }, { 143, 109, 104, 0, 0, 0 }, {
    143, 110, 105, 0, 0, 0 }, { 144, 111, 105, 0, 0, 0 }, { 145, 111, 106, 0, 0,
    0 }, { 146, 112, 106, 0, 0, 0 }, { 146, 113, 107, 0, 0, 0 }, { 147, 113,
    108, 0, 0, 0 }, { 148, 114, 108, 0, 0, 0 }, { 149, 115, 109, 0, 0, 0 }, {
    149, 116, 109, 0, 0, 0 }, { 150, 116, 110, 0, 0, 0 }, { 151, 117, 111, 0, 0,
    0 }, { 152, 118, 111, 0, 0, 0 }, { 152, 119, 112, 0, 0, 0 }, { 153, 119,
    112, 0, 0, 0 }, { 154, 120, 113, 0, 0, 0 }, { 155, 121, 114, 0, 0, 0 }, {
    155, 122, 114, 0, 0, 0 }, { 156, 122, 115, 0, 0, 0 }, { 157, 123, 115, 0, 0,
    0 }, { 158, 124, 116, 0, 0, 0 }, { 158, 125, 117, 0, 0, 0 }, { 159, 125,
    117, 0, 0, 0 }, { 160, 126, 118, 0, 0, 0 }, { 161, 127, 118, 0, 0, 0 }, {
    161, 128, 119, 0, 0, 0 }, { 162, 128, 120, 0, 0, 0 }, { 163, 129, 120, 0, 0,
    0 }, { 164, 130, 121, 0, 0, 0 }, { 164, 131, 122, 0, 0, 0 }, { 165, 132,
    122, 0, 0, 0 }, { 166, 132, 123, 0, 0, 0 }, { 167, 133, 123, 0, 0, 0 }, {
    167, 134, 124, 0, 0, 0 }, { 168, 135, 125, 0, 0, 0 }, { 169, 135, 125, 0, 0,
    0 }, { 170, 136, 126, 0, 0, 0 }, { 170, 137, 127, 0, 0, 0 }, { 171, 138,
    127, 0, 0, 0 }, { 172, 139, 128, 0, 0, 0 }, { 173, 140, 128, 0, 0, 0 }, {
    173, 140, 129, 0, 0, 0 }, { 174, 141, 130, 0, 0, 0 }, { 175, 142, 130, 0, 0,
    0 }, { 176, 143, 131, 0, 0, 0 }, { 176, 144, 132, 0, 0, 0 }, { 177, 145,
    132, 0, 0, 0 }, { 178, 145, 133, 0, 0, 0 }, { 179, 146, 133, 0, 0, 0 }, {
    179, 147, 134, 0, 0, 0 }, { 180, 148, 135, 0, 0, 0 }, { 181, 149, 135, 0, 0,
    0 }, { 182, 150, 136, 0, 0, 0 }, { 182, 150, 137, 0, 0, 0 }, { 183, 151,
    137, 0, 0, 0 }, { 184, 152, 138, 0, 0, 0 }, { 185, 153, 138, 0, 0, 0 }, {
    185, 154, 139, 0, 0, 0 }, { 186, 155, 140, 0, 0, 0 }, { 187, 156, 140, 0, 0,
    0 }, { 188, 157, 141, 0, 0, 0 }, { 188, 157, 142, 0, 0, 0 }, { 189, 158,
    142, 0, 0, 0 }, { 190, 159, 143, 0, 0, 0 }, { 191, 160, 144, 0, 0, 0 }, {
    191, 161, 144, 0, 0, 0 }, { 192, 162, 145, 0, 0, 0 }, { 193, 163, 146, 0, 0,
    0 }, { 194, 164, 146, 0, 0, 0 }, { 194, 165, 147, 0, 0, 0 }, { 195, 166,
    148, 0, 0, 0 } };

/* SDMConversionModelPkg::cBrakePercentV_lim */
const array_int_4 cBrakePercentV_lim_SDMConversionModelPkg = { 100, 120, 150,
  180 };

/* SDMConversionModelPkg::cBrakePercentSpeedLookup */
const array_int_221 cBrakePercentSpeedLookup_SDMConversionModelPkg = { 72, 73,
  74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 84, 85, 86, 87, 88, 88, 89, 90,
  91, 91, 92, 93, 94, 94, 95, 96, 96, 97, 98, 99, 99, 100, 101, 101, 102, 103,
  103, 104, 104, 105, 106, 106, 107, 108, 108, 109, 109, 110, 111, 111, 112,
  112, 113, 113, 114, 115, 115, 116, 116, 117, 117, 118, 118, 119, 119, 120,
  120, 121, 121, 122, 122, 123, 124, 124, 125, 125, 125, 126, 126, 127, 127,
  128, 128, 129, 129, 130, 130, 131, 131, 132, 132, 133, 133, 134, 134, 134,
  135, 135, 136, 136, 137, 137, 138, 138, 138, 139, 139, 140, 140, 141, 141,
  141, 142, 142, 143, 143, 143, 144, 144, 145, 145, 145, 146, 146, 147, 147,
  148, 148, 148, 149, 149, 149, 150, 150, 151, 151, 151, 152, 152, 153, 153,
  153, 154, 154, 154, 155, 155, 156, 156, 156, 157, 157, 157, 158, 158, 158,
  159, 159, 160, 160, 160, 161, 161, 161, 162, 162, 162, 163, 163, 163, 164,
  164, 164, 165, 165, 165, 166, 166, 166, 167, 167, 168, 168, 168, 169, 169,
  169, 169, 170, 170, 170, 171, 171, 171, 172, 172, 172, 173, 173, 173, 174,
  174, 174, 175, 175, 175, 176, 176, 176, 177, 177, 177, 177, 178, 178, 178,
  179, 179 };

/* CalculateTrainPosition_Pkg::cNoPositionedBG */
const positionedBG_T_TrainPosition_Types_Pck cNoPositionedBG_CalculateTrainPosition_Pkg = {
  kcg_false, 0, 0, Q_LINK_Unlinked, { 0, 0, 0 }, 0, { kcg_false, 0, 0, { 0, 0,
      0 }, { 0, 0, 0 }, { kcg_false, 0, Q_DIR_Reverse, Q_SCALE_10_cm_scale, 0,
      Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows, 0,
      0,
      Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
      Q_LINKREACTION_Train_trip, 0 } }, { kcg_false, { kcg_false,
      Q_UPDOWN_Down_link_telegram,
      M_VERSION_Previous_versions_according_to_e_g_EEIG_SRS_and_UIC_A200_SRS,
      Q_MEDIA_Balise, N_TOTAL_1_balise_in_the_group, 0, 0, 0, Q_LINK_Unlinked, {
        kcg_false, 0, { 0, 0, 0 }, { 0, 0, 0, 0 }, 0,
        noMotion_Obu_BasicTypes_Pkg, unknownDirection_Obu_BasicTypes_Pkg }, { 0,
        0, 0 }, 0, kcg_false, Q_DIRLRBG_Reverse, Q_DIRTRAIN_Reverse }, { {
        kcg_false, 0, Q_DIR_Reverse, Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 }, { kcg_false, 0, Q_DIR_Reverse,
        Q_SCALE_10_cm_scale, 0,
        Q_NEWCOUNTRY_Same_country__or__railway_administration_no_NID_C_follows,
        0, 0,
        Q_LINKORIENTATION_The_balise_group_is_seen_by_the_train_in_reverse_direction,
        Q_LINKREACTION_Train_trip, 0 } } }, kcg_false };

/* TrackAtlasTypes::DEFAULT_GradientProfile */
const GradientProfile_t_TrackAtlasTypes DEFAULT_GradientProfile_TrackAtlasTypes = {
  { kcg_false, 0, 0, 0, 0 }, { kcg_false, 0, 0, 0, 0 } };

/* $**************** KCG Version 6.4 (build i21) ****************
** kcg_consts.c
** Generation date: 2019-05-22T11:26:05
*************************************************************$ */

