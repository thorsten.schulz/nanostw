/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */
#ifndef _locateWithinSection_nanoStw_H_
#define _locateWithinSection_nanoStw_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */


/* nanoStw::locateWithinSection */
extern kcg_int locateWithinSection_nanoStw(
  /* nanoStw::locateWithinSection::section */ TrackSection_nanoStw *section,
  /* nanoStw::locateWithinSection::location */ LengthUnit location,
  /* nanoStw::locateWithinSection::runsCW */ kcg_bool runsCW);

#endif /* _locateWithinSection_nanoStw_H_ */
/* $**************** KCG Version 6.4 (build i21) ****************
** locateWithinSection_nanoStw.h
** Generation date: 2019-05-22T11:26:06
*************************************************************$ */

