/* $**************** KCG Version 6.4 (build i21) ****************
** Command: kcg64.exe -config P:/EW/ESTW/Stw.bm/config.txt
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "iterateMA_nanoStw_3_38.h"

/* nanoStw::iterateMA */
void iterateMA_nanoStw_3_38(
  /* nanoStw::iterateMA::pos */ TrackLocation_nanoStw *pos,
  /* nanoStw::iterateMA::runsCW */ kcg_bool runsCW,
  /* nanoStw::iterateMA::vMax */ VelocityUnit vMax,
  /* nanoStw::iterateMA::vOS */ VelocityUnit vOS,
  /* nanoStw::iterateMA::tracks */ array_21324 *tracks,
  /* nanoStw::iterateMA::otherTrains */ array_21564 *otherTrains,
  /* nanoStw::iterateMA::nloc */ TrackLocation_nanoStw *nloc,
  /* nanoStw::iterateMA::SpeedSegment */ TrainSpeedSegment_nanoStw *SpeedSegment)
{
  /* nanoStw::iterateMA */ kcg_int tmp;
  /* nanoStw::iterateMA */ TrackLocation_nanoStw acc;
  kcg_int i;
  /* nanoStw::iterateMA::ifFinalizing::else::_L20 */ TrackLocation_nanoStw _L20_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::_L29 */ VelocityUnit _L29_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::_L51 */ kcg_bool _L51_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::thisTrack */ TrackKnot_nanoStw thisTrack_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::nTrack */ TrackKnot_nanoStw nTrack_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::sectionStart */ LengthUnit sectionStart_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::thisTrackisOpposite */ kcg_bool thisTrackisOpposite_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::railEnd */ TrackLocation_nanoStw railEnd_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::overlapIntoThisTrack */ LengthUnit overlapIntoThisTrack_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::connected */ kcg_bool connected_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing::else::beginningOfNext */ TrackLocation_nanoStw beginningOfNext_ifFinalizing;
  /* nanoStw::iterateMA::ifFinalizing */ kcg_bool ifFinalizing_clock;
  
  ifFinalizing_clock = (*pos).sid == EndOfTrackSid_nanoStw;
  /* ck_ifFinalizing */ if (ifFinalizing_clock) {
    kcg_copy_TrackLocation_nanoStw(nloc, pos);
    kcg_copy_TrainSpeedSegment_nanoStw(
      SpeedSegment,
      (TrainSpeedSegment_nanoStw *) &emptySpeedSegment_nanoStw);
  }
  else {
    (*SpeedSegment).location = (*pos).location;
    /* 2 */
    guessTrackKnotLine_nanoStw_38(
      pos,
      tracks,
      vOS,
      &_L29_ifFinalizing,
      &thisTrack_ifFinalizing);
    (*SpeedSegment).first.sid = thisTrack_ifFinalizing.sid;
    (*SpeedSegment).first.length = thisTrack_ifFinalizing.l;
    railEnd_ifFinalizing.sid = thisTrack_ifFinalizing.sid;
    /* 2 */
    nextTrackKnot_nanoStw_38(
      &thisTrack_ifFinalizing,
      runsCW,
      tracks,
      &nTrack_ifFinalizing,
      &connected_ifFinalizing,
      &_L51_ifFinalizing);
    beginningOfNext_ifFinalizing.sid = nTrack_ifFinalizing.sid;
    thisTrackisOpposite_ifFinalizing = (thisTrack_ifFinalizing.sid <
        UnknownSid_nanoStw) != runsCW;
    /* 8 */ if (connected_ifFinalizing) {
      overlapIntoThisTrack_ifFinalizing = 0;
    }
    else {
      overlapIntoThisTrack_ifFinalizing = nTrack_ifFinalizing.l *
        (FlankenOverlap_nanoStw - 100) / 100;
    }
    /* 13 */ if (thisTrackisOpposite_ifFinalizing) {
      tmp = thisTrack_ifFinalizing.l - (*pos).distance;
    }
    else {
      tmp = (*pos).distance;
    }
    sectionStart_ifFinalizing = (*pos).location - tmp;
    (*SpeedSegment).first.startloc = sectionStart_ifFinalizing;
    beginningOfNext_ifFinalizing.location = thisTrack_ifFinalizing.l +
      sectionStart_ifFinalizing;
    /* 10 */ if (_L51_ifFinalizing) {
      beginningOfNext_ifFinalizing.distance = 0;
    }
    else {
      beginningOfNext_ifFinalizing.distance = nTrack_ifFinalizing.l;
    }
    railEnd_ifFinalizing.location = beginningOfNext_ifFinalizing.location -
      overlapIntoThisTrack_ifFinalizing;
    /* 12 */ if (thisTrackisOpposite_ifFinalizing) {
      railEnd_ifFinalizing.distance = overlapIntoThisTrack_ifFinalizing;
    }
    else {
      railEnd_ifFinalizing.distance = thisTrack_ifFinalizing.l -
        overlapIntoThisTrack_ifFinalizing;
    }
    kcg_copy_TrackLocation_nanoStw(&_L20_ifFinalizing, &railEnd_ifFinalizing);
    /* 3 */ for (i = 0; i < 3; i++) {
      kcg_copy_TrackLocation_nanoStw(&acc, &_L20_ifFinalizing);
      /* 2 */
      checkCollision_nanoStw(
        &acc,
        runsCW,
        &(*otherTrains)[i],
        &_L20_ifFinalizing);
    }
    /* 9 */ if (connected_ifFinalizing & (railEnd_ifFinalizing.location ==
        _L20_ifFinalizing.location)) {
      kcg_copy_TrackLocation_nanoStw(nloc, &beginningOfNext_ifFinalizing);
    }
    else {
      kcg_copy_TrackLocation_nanoStw(nloc, &_L20_ifFinalizing);
      (*nloc).sid = EndOfTrackSid_nanoStw;
    }
    /* 11 */ if ((vMax >= _L29_ifFinalizing) & (_L29_ifFinalizing >= 0)) {
      (*SpeedSegment).speed = _L29_ifFinalizing;
    }
    else {
      (*SpeedSegment).speed = vMax;
    }
  }
}

/* $**************** KCG Version 6.4 (build i21) ****************
** iterateMA_nanoStw_3_38.c
** Generation date: 2019-05-22T11:26:07
*************************************************************$ */

