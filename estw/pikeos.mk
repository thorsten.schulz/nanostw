#
# A Makefile-include for building the nanoStw project partially on PikeOS
#
# Use this from the Master-Makefile
#
# Copyright (c) Universität Rostock, 2020
#
# Authors:
#  Thorsten Schulz <thorsten.schulz@uni-rostock.de>
#
# SPDX-License-Identifier: Apache-2.0
#

#-------------------------------------------------------------------------------
# define in top-Makefile
export PIKEOS_PREFIX
export ELINOS_PREFIX
# Set correct JDK path -> PikeOS 4.2 -> i386 & PikeOS 5.0 -> amd64
export JAVA_HOME := /usr/lib/jvm/java-8-openjdk-i386
# export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH := $(JAVA_HOME)/bin:$(PATH)

#-------------------------------------------------------------------------------
# Set the architecture and processor
# {x86, arm, ppc, sparc}
P_ARCH = x86
# {amd64, i686, v7hf, v8hf, ...}
P_PROC = amd64
# Set the DOM file identifier for the board
P_BDOM = qemu-x86-64.bsp.dom
#-------------------------------------------------------------------------------

RANDOM_KDEV = random.dev
RTC_KDEV    = rtc.dev
RFUSION     = rfusion.kernel
LWIP_LIB    = trdpLwIP.lib
TRDP_LIB    = trdpPosix.lib
TSVC_APP    = trdpPosixSvc.app
STW_APP     = trdpStw.app
TSM_APP     = slide-marks.app
NANOSTW_INT = nanoStw_native_posix.Qint
TG_APP      = tindyguard.app
NANOSTWG_INT= nanoStw_native_posix_tindy.Qint

ESVC_SYS    = trdpElinosSvc.sys

#TINDY_SKILLS= 4_4_19_3_4_4
#TINDY_SKILLS= 4_8_23_4_4_4
TINDY_SKILLS= 8_8_23_12_16_16

# some features used for JH, apply them also here
QEMU=qemu-system-x86_64

QEMU_EXTRA_ARGS= \
	-cpu host,-kvm-pv-eoi,-kvm-pv-ipi,-kvm-asyncpf,-kvm-steal-time,-kvmclock \
	-smp 4 \
	-enable-kvm -machine q35,kernel_irqchip=split


# Build the target identifier
P_TARGET= $(P_ARCH)_$(P_PROC)
# Build the board path
# note, not used, preconfigured board available
P_BOARD = $(PIKEOS_PREFIX)/target/$(P_ARCH)/$(P_PROC)/board/$(P_BDOM)
# Build the build directory path
P_SRC   = pikeos
P_BUILD = $(BUILDTMP)
P_POOL  = $(PWD)/$(P_BUILD)/pool
# Shorten PikeOS clone-tool path
P_CLONE = $(PIKEOS_PREFIX)/bin/pikeos-cloneproject
E_CLONE = $(ELINOS_PREFIX)/bin/elinos-cloneproject
# Where to find the demo projects
P_DEMOS = $(PIKEOS_PREFIX)/demo
# MUXA-PID file depends on configuration, though 1500 is the standard
P_MUXAP = /tmp/muxa-1500
P_KMUXA = $(PIKEOS_PREFIX)/bin/killmuxa
P_MUXA  = $(PIKEOS_PREFIX)/bin/muxa

P_PROJECTS  = $(RANDOM_KDEV) $(RTC_KDEV) $(RFUSION) $(LWIP_LIB) $(TRDP_LIB) $(TSVC_APP) $(STW_APP) $(NANOSTW_INT) $(TG_APP) $(NANOSTWG_INT) $(TSM_APP)
E_PROJECTS  = $(ESVC_SYS)

.PHONY:  pikeos-network-qemu pikeos-clean pikeos-run pikeos-grab-tindy pikeos-all pikeos-prepare

$(P_BUILD):
	mkdir -p "$@"

$(P_BUILD)/%/project.xml : $(P_SRC)/%/project.xml | $(P_BUILD)
	$(RM) -r "$(@D)"
	$(P_CLONE) "$(<D)" "$(@D)" --target=$(P_TARGET) --pool=$(P_POOL)

$(P_BUILD)/$(LWIP_LIB)/project.xml: $(P_DEMOS)/posix/lwip-build | $(P_BUILD)
	$(RM) -r "$(@D)"
	$(P_CLONE) "$<" "$(@D)" --target=$(P_TARGET) --pool=$(P_POOL)
	patch -d "$(@D)" -p1 < patches/lwip_recvfromdest.patch

$(P_BUILD)/%/.elinosproject: $(P_SRC)/%/.elinosproject $(P_SRC)/%/.features $(P_SRC)/%/Makefile $(P_SRC)/%/project.mkefs $(P_SRC)/%/src | $(P_BUILD)
	$(RM) -r "$(@D)"
	$(E_CLONE) "$(<D)" "$(@D)"

$(P_BUILD)/$(NANOSTW_INT)/project.xml: $(P_SRC)/$(NANOSTW_INT)/project.xml | $(P_BUILD)
	$(RM) -r "$(@D)"
	#$(P_CLONE) "$(<D)" "$(@D)" --target=$(P_TARGET) --pool=$(P_POOL) --board=$(P_BOARD)
	#cp $(P_SRC)/$(NANOSTW_INT)/project2.xml "$@"
	mkdir -p "$(@D)"
	cp $(P_SRC)/$(NANOSTW_INT)/project.xml $(P_SRC)/$(NANOSTW_INT)/makefile.defs "$(@D)"
	#do not inflate the project quite yet

# see above
$(P_BUILD)/$(NANOSTWG_INT)/project.xml: $(P_SRC)/$(NANOSTWG_INT)/project.xml | $(P_BUILD)
	$(RM) -r "$(@D)"
	mkdir -p "$(@D)"
	cp $(P_SRC)/$(NANOSTWG_INT)/project.xml $(P_SRC)/$(NANOSTWG_INT)/makefile.defs "$(@D)"

ifeq ($(TRDP_SRC),)
$(warning Cannot build $(TRDP_LIB) without local TRDP library source.)
else

$(P_BUILD)/$(TRDP_LIB)/src: $(TRDP_SRC) $(P_BUILD)/$(TRDP_LIB)/makefile.defs
	cp -r "$<" "$@"

pikeos-prepare: $(addprefix $(P_BUILD),$(addsuffix /project.xml,   $(P_PROJECTS)) )

ifneq ($(realpath $(ELINOS_PREFIX)),)

pikeos-prepare: $(addprefix $(P_BUILD),$(addsuffix /.elinosproject,$(E_PROJECTS)) )
pikeos-all:     $(P_BUILD)/$(ESVC_SYS)/src/app/trdpElinosSvc

endif


endif

$(P_BUILD)/%/makefile.defs : $(P_SRC)/%/makefile.defs $(P_BUILD)/%/project.xml
	cp "$<" "$@"

$(P_BUILD)/$(NANOSTW_INT)/config_%: ../config/% $(P_BUILD)/$(NANOSTW_INT)/project.xml
	cp "$<" "$@"

$(P_BUILD)/$(NANOSTWG_INT)/config_%: ../config/% $(P_BUILD)/$(NANOSTWG_INT)/project.xml
	cp "$<" "$@"

#	cp -r user_macros.h Stw.bm app/{ivshmem.h,ivshmem-pikeos.c,stw.c,tau_xservice_client.c,trdpSvc.h} ../../p4/trdpStw.app/
$(P_BUILD)/$(STW_APP)/% : app/% $(P_BUILD)/$(STW_APP)/project.xml
	cp -rL "$<" "$@"

#	cp    /usr/include/trdp/{tau_xsession_defaults.h,tau_xmarshall_map.h} ../../p4/trdpStw.app/
$(P_BUILD)/$(STW_APP)/tau_x%.h : $(TRDP_HDR_COMMON)/tau_x%.h $(P_BUILD)/$(STW_APP)/project.xml
	cp "$<" "$@"

$(P_BUILD)/$(STW_APP)/$(C_SRC_MODEL_STW) : $(C_SRC_MODEL_STW)/kcg_files.txt $(P_BUILD)/$(STW_APP)/project.xml
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@cp "$<" $(<D)/*.c $(<D)/*.h "$@"

$(P_BUILD)/$(TSM_APP)/% : app/% $(P_BUILD)/$(TSM_APP)/project.xml
	cp -rL "$<" "$@"

$(P_BUILD)/$(TSM_APP)/utils.bm : utils.bm
	cp -r "$<" "$@"

$(P_BUILD)/$(TSM_APP)/slide-marks.bm : $(C_SRC_MODEL_TSM)/kcg_files.txt $(P_BUILD)/$(TSM_APP)/project.xml
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@cp "$<" $(<D)/*.c $(<D)/*.h "$@"

#	cp tindyguard-filter.c crypt/
$(P_BUILD)/$(TG_APP)/% : app/%
	cp -rL "$<" "$@"

$(P_BUILD)/$(TG_APP)/utils.bm : utils.bm
	cp -r "$<" "$@"

$(P_BUILD)/$(TG_APP)/tindyguard.% : tindyguard/tindyguard.%/kcg_files.txt $(P_BUILD)/$(TG_APP)/project.xml
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@cp "$<" $(<D)/*.c $(<D)/*.h "$@"

$(P_POOL)/pikeos-native/object/trdpStw.elf: $(P_BUILD)/$(STW_APP)/trdpStw.elf
	make -C "$(<D)" install

$(P_POOL)/pikeos-native/object/slide-marks.elf: $(P_BUILD)/$(TSM_APP)/slide-marks.elf
	make -C "$(<D)" install

$(P_POOL)/pikeos-native/object/tindyguard-filter.elf: $(P_BUILD)/$(TG_APP)/tindyguard-filter.elf
	make -C "$(<D)" install

$(P_BUILD)/$(STW_APP)/trdpStw.elf: $(addprefix $(P_BUILD)/$(STW_APP)/, makefile.defs tau_xsession_defaults.h tau_xmarshall_map.h user_macros.h ivshmem.h ivshmem-pikeos.c ivshmem-common.c stw.c tau_xservice_client.c trdpSvc.h $(C_SRC_MODEL_STW))
	make -C "$(<D)" all

$(P_BUILD)/$(TSM_APP)/slide-marks.elf: $(addprefix $(P_BUILD)/$(TSM_APP)/, makefile.defs slide-marks.c curve25519-donna.c donna slide-marks.bm utils.bm user_macros.h)
	make -C "$(<D)" all

$(P_BUILD)/$(TG_APP)/tindyguard-filter.elf: $(addprefix $(P_BUILD)/$(TG_APP)/,makefile.defs tindyguard-filter.c curve25519-donna.c donna tindyguard.$(TINDY_SKILLS) utils.bm  user_macros.h)
	make -C "$(<D)" all

#	cp -r user_macros.h Stw.bm app/{ivshmem.h,ivshmem-posix.c,ivshmem-common.c,trdpSvc.c,trdpSvc.h} ../../p4/trdpPosixSvc.app/
$(P_BUILD)/$(TSVC_APP)/% : app/% $(P_BUILD)/$(TSVC_APP)/project.xml
	cp -rL "$<" "$@"

$(P_POOL)/posix/object/trdpSvc.elf: $(P_BUILD)/$(TSVC_APP)/trdpSvc.elf
	make -C "$(<D)" install

$(P_BUILD)/$(TSVC_APP)/trdpSvc.elf: $(P_BUILD)/$(TSVC_APP)/makefile.defs $(C_SRC_SVC:app/%=$(P_BUILD)/$(TSVC_APP)/%) $(C_HDR_SVC:app/%=$(P_BUILD)/$(TSVC_APP)/%) $(P_SRC)/$(TSVC_APP) $(P_POOL)/posix/lwip/lib/liblwip.a $(P_POOL)/posix/lib/libtrdpap.a
	make -C "$(<D)" all

$(P_BUILD)/$(ESVC_SYS)/src/app/% : app/% $(P_BUILD)/$(ESVC_SYS)/.elinosproject
	cp -rL "$<" "$@"

$(P_BUILD)/$(ESVC_SYS)/src/app/trdp.mk : trdp.mk
	cp "$<" "$@"

$(P_BUILD)/$(ESVC_SYS)/src/app/trdpElinosSvc: $(P_BUILD)/$(ESVC_SYS)/.elinosproject $(P_BUILD)/$(ESVC_SYS)/src/app/trdp.mk $(C_SRC_SVC:app/%=$(P_BUILD)/$(ESVC_SYS)/src/app/%) $(C_HDR_SVC:app/%=$(P_BUILD)/$(ESVC_SYS)/src/app/%) $(P_SRC)/$(ESVC_SYS) $(TRDP_SRC)
	. $(P_BUILD)/$(ESVC_SYS)/ELINOS.sh ; TRDP="$(abspath $(LOCAL_TRDP))" make -C "$(<D)" boot 

#If you do changes to the kernel drivers, you must clean and rebuild (no dependency tracking).
$(P_POOL)/fusion-kernel/object/kerneldriver/devrandom.kdev: $(P_BUILD)/$(RANDOM_KDEV)/devrandom.kdev
	make -C "$(<D)" install

$(P_BUILD)/$(RANDOM_KDEV)/devrandom.kdev: $(P_BUILD)/$(RANDOM_KDEV)/makefile.defs
	make -C "$(<D)" all

$(P_POOL)/fusion-kernel/object/kerneldriver/devrtc.kdev: $(P_BUILD)/$(RTC_KDEV)/devrtc.kdev
	make -C "$(<D)" install

$(P_BUILD)/$(RTC_KDEV)/devrtc.kdev: $(P_BUILD)/$(RTC_KDEV)/makefile.defs
	make -C "$(<D)" all

$(P_POOL)/object/x86-64+R: $(P_BUILD)/$(RFUSION)/*.elf
	make -C "$(<D)" install

$(P_BUILD)/rfusion.kernel/*.elf : $(P_BUILD)/$(RFUSION)/project.xml $(P_POOL)/fusion-kernel/object/kerneldriver/devrtc.kdev $(P_POOL)/fusion-kernel/object/kerneldriver/devrandom.kdev 
	make -C "$(<D)" all

ifneq ($(TRDP_SRC),)

# newer Make 4.3 supports grouped targets "&:", for backwards compat, use pattern rule
$(P_POOL)/posix/lib/%.a $(P_POOL)/posix/lib/%ap.a : $(P_BUILD)/$(TRDP_LIB)/%.a $(P_BUILD)/$(TRDP_LIB)/%ap.a
	make -C "$(<D)" install

$(P_BUILD)/$(TRDP_LIB)/%.a $(P_BUILD)/$(TRDP_LIB)/%ap.a : $(P_BUILD)/$(TRDP_LIB)/src
	make -C "$(<D)" all

endif

$(P_POOL)/posix/lwip/lib/liblwip.a: $(P_BUILD)/$(LWIP_LIB)/project.xml
	make -C "$(<D)" install

$(P_BUILD)/$(NANOSTW_INT)/project.mk : $(P_BUILD)/$(NANOSTW_INT)/project.xml
	cd "$(@D)" && $(PIKEOS_PREFIX)/bin/pikeos-share-project import --pool=$(P_POOL)

$(P_BUILD)/$(NANOSTWG_INT)/project.mk : $(P_BUILD)/$(NANOSTWG_INT)/project.xml
	cd "$(@D)" && $(PIKEOS_PREFIX)/bin/pikeos-share-project import --pool=$(P_POOL)

$(P_BUILD)/$(NANOSTW_INT)/muxa.xml $(P_BUILD)/$(NANOSTW_INT)/Makefile: $(P_BUILD)/$(NANOSTW_INT)/project.mk

$(P_BUILD)/$(NANOSTWG_INT)/muxa.xml $(P_BUILD)/$(NANOSTWG_INT)/Makefile: $(P_BUILD)/$(NANOSTWG_INT)/project.mk

$(P_BUILD)/$(NANOSTW_INT)/boot/% : $(addprefix $(P_BUILD)/$(NANOSTW_INT)/,project.xml config_db.xml config_sbb.xml config_ice.xml Makefile)  $(P_POOL)/pikeos-native/object/trdpStw.elf $(P_POOL)/posix/object/trdpSvc.elf $(P_POOL)/object/x86-64+R $(P_POOL)/pikeos-native/object/slide-marks.elf
	make -C "$(<D)" clean validate-xml all
	@$(RM) "$(P_BUILD)/$(NANOSTW_INT)/boot/qemu_cmdline"

$(P_BUILD)/$(NANOSTWG_INT)/boot/% : $(addprefix $(P_BUILD)/$(NANOSTWG_INT)/,project.xml config_db.xml config_sbb.xml config_ice.xml Makefile config_wg-rno.conf) $(P_POOL)/pikeos-native/object/trdpStw.elf $(P_POOL)/posix/object/trdpSvc.elf $(P_POOL)/pikeos-native/object/tindyguard-filter.elf $(P_POOL)/object/x86-64+R
	make -C "$(<D)" clean validate-xml all
	@$(RM) "$(P_BUILD)/$(NANOSTWG_INT)/boot/qemu_cmdline"

pikeos-all: $(P_BUILD)/$(NANOSTW_INT)/boot/nanoStw-qemu-x86-64-qemu $(P_BUILD)/$(NANOSTWG_INT)/boot/nanoStWG-qemu-x86-64-qemu 
	@echo "ALL done."

$(P_MUXAP): $(P_BUILD)/$(NANOSTW_INT)/muxa.xml
	#{ $(P_MUXA) -f "$<" > /dev/null & echo $$! > "$@"; }
	# use MUXA's own pid file 
	@test -r "$(P_MUXAP)" && $(P_KMUXA) || echo "Starting ..."
	$(P_MUXA) -f "$<" > /dev/null &

pikeos-run: $(P_BUILD)/$(NANOSTW_INT)/boot/nanoStw-qemu-x86-64-qemu $(P_MUXAP)
	@echo "You can exit QEMU by pressing CTRL-a then c then q then ENTER"
	#this needs separate terminal processes
	#make -C ~/Projekte/ew/estw run-train
	#make -C ~/Projekte/eisenbahn/gleis-gui run
	#make -C ~/Projekte/ew/dmi run
	#telnet localhost 1504
	sudo $(QEMU) -kernel "$<" -nographic -serial mon:stdio -m 256 \
		$(QEMU_EXTRA_ARGS) \
		-netdev bridge,id=eth0,br=br-qemu -device e1000,netdev=eth0 \
		-netdev bridge,id=eth1,br=br-up   -device e1000,netdev=eth1 \
		-netdev bridge,id=eth2,br=br-ecn2 -device e1000,netdev=eth2
	#potentially start a second train in Linux
	@kill -2 `cat $(P_MUXAP)` && $(RM) $(P_MUXAP)
	@echo "---"

pikeos-grab-tindy: $(P_BUILD)/$(NANOSTWG_INT)/boot/nanoStWG-qemu-x86-64-qemu $(P_MUXAP)
	@echo "You can exit QEMU by pressing CTRL-a then c then q then ENTER"
	sudo $(QEMU) -kernel "$<" -nographic -serial mon:stdio -m 256 \
		$(QEMU_EXTRA_ARGS) \
		-netdev bridge,id=eth0,br=br-qemu -device e1000,netdev=eth0 \
		-netdev bridge,id=eth1,br=br-up   -device e1000,netdev=eth1 \
		-netdev bridge,id=eth2,br=br-ecn2 -device e1000,netdev=eth2
	#potentially start a second train in Linux
	@kill -2 `cat $(P_MUXAP)` && $(RM) $(P_MUXAP)
	@echo "---"

pikeos-clean:
	@test -r "$(P_MUXAP)" && $(P_KMUXA) || echo 
	$(RM) -r $(addprefix $(P_BUILD)/,$(P_PROJECTS) $(E_PROJECTS))

pikeos-network-qemu:
	sudo ./network-qemu.sh
	echo "Also run network-qemu-wg.sh if you want WG / tindguard"

distclean: pikeos-clean
