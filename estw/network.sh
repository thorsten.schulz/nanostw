#!/bin/bash

del_if () {
	ip link show $1 2> /dev/null >/dev/null && ip link del $1 || echo -n .
}

fix_bridge_allow () {
	mkdir -p /etc/qemu ; [[ -r /etc/qemu/bridge ]] || ( touch /etc/qemu/bridge.conf ; chgrp kvm /etc/qemu/bridge.conf ) ; [[ $(grep -w "$1" /etc/qemu/bridge.conf) ]] || echo "allow $1" | cat - /etc/qemu/bridge.conf |  tee /etc/qemu/bridge.conf > /dev/null
}
# this is meant for the Linux branch

if [[ $UID != 0 ]]; then
	echo "Please run this network script as root!"
	exit 1
fi

# allow qemu bridge access
fix_bridge_allow br-qemu
fix_bridge_allow br-up
fix_bridge_allow br-ecn1
fix_bridge_allow br-ecn2
fix_bridge_allow br-ecn3

#clear interfaces
echo -n "Resetting: "
del_if ve1-ecn3
del_if br-ecn3
del_if ve1-ecn2
del_if br-ecn2
del_if ve1-ecn1
del_if br-ecn1
del_if br-qemu
del_if wg-up
del_if br-up
echo

#create interfaces
#ip link add br-qemu type bridge
ip link add br-up type bridge
ip link add br-ecn1 type bridge
ip link add ve1-ecn1 type veth peer name ve2-ecn1
ip link set ve1-ecn1 master br-ecn1
ip link add br-ecn2 type bridge
ip link add ve1-ecn2 type veth peer name ve2-ecn2
ip link set ve1-ecn2 master br-ecn2
ip link add br-ecn3 type bridge
ip link add ve1-ecn3 type veth peer name ve2-ecn3
ip link set ve1-ecn3 master br-ecn3
#ip link add wg-up type wireguard

#hand out addresses
#ip addr add 192.168.201.1/24 dev br-qemu
#ip addr add 172.22.0.1/24 dev br-up      #some router on the Uplink at the OCC
ip addr add 172.21.0.10/20 dev br-up     #core GG instance
ip addr add 172.21.1.10/20 dev ve2-ecn1    #uplink channel on stw#1
ip addr add 172.21.2.10/20 dev ve2-ecn2    #uplink channel on stw#2
ip addr add 172.21.3.10/20 dev ve2-ecn3    #uplink channel on stw#3

#train-1
ip addr add 10.0.9.1/24 dev ve2-ecn1    #some router on ECN
ip addr add 10.0.9.10/32 dev br-ecn1    #stw
ip addr add 10.0.9.11/32 dev br-ecn1    #Ctrl
ip addr add 10.0.9.18/32 dev br-ecn1    #DMI-A
ip addr add 10.0.9.19/32 dev br-ecn1    #UI-A
#ip addr add 10.0.10.18/32 dev br-ecn2    #DMI-B
#ip addr add 10.0.10.19/32 dev br-ecn2    #UI-B

#train-2
ip addr add 10.0.17.1/24 dev ve2-ecn2    #some router on ECN
ip addr add 10.0.17.10/32 dev br-ecn2    #stw
ip addr add 10.0.17.11/32 dev br-ecn2    #Ctrl
ip addr add 10.0.17.18/32 dev br-ecn2    #DMI-A
ip addr add 10.0.17.19/32 dev br-ecn2    #UI-A
#ip addr add 10.0.18.18/32 dev br-ecn2    #DMI-B
#ip addr add 10.0.18.19/32 dev br-ecn2    #UI-B

#train-3
ip addr add 10.0.25.1/24 dev ve2-ecn3    #some router on ECN
ip addr add 10.0.25.10/32 dev br-ecn3    #stw
ip addr add 10.0.25.11/32 dev br-ecn3    #Ctrl
ip addr add 10.0.25.18/32 dev br-ecn3    #DMI-A
ip addr add 10.0.25.19/32 dev br-ecn3    #UI-A
#ip addr add 10.0.26.18/32 dev br-ecn3    #DMI-B
#ip addr add 10.0.26.19/32 dev br-ecn3    #UI-B

#wg addconf wg-up recipes-core/non-root-initramfs/files/overlay/etc/trdp/wg?gg.conf

ip link set br-up up
#ip link set br-qemu up
ip link set ve1-ecn1 up
ip link set ve2-ecn1 up
ip link set  br-ecn1 up
ip link set ve1-ecn2 up
ip link set ve2-ecn2 up
ip link set  br-ecn2 up
ip link set ve1-ecn3 up
ip link set ve2-ecn3 up
ip link set  br-ecn3 up
#ip link set wg-up up

#tell the kernel where to find our peers
#ip route add 172.21.2.0/24 via 172.21.0.102 dev wg-up    #route to STW of DB train via its router (instead of NATing)
#ip route add 172.21.1.0/24 via 172.21.0.101 dev wg-up
#ip route add 172.21.3.0/24 via 172.21.0.103 dev wg-up


