/* -*- Mode: C -*- */
/* Based on PikeOS example "/dev/zero device" to read produces infinite stream of 0 bytes.
 * Modified to output the (x86) systems's Real-Time-Clock.
 *
 * Most parts of the RTC implementation are stolen from Linux's kernel and QEMU's sources.
 *
 *   Thorsten Schulz, July 2018, <thorsten.schulz@uni-rostock.de>
 */

#include <p4_vmit.h>
#include <stand/stdlib.h>
#include <stand/string.h>
#include <kdev/assert.h>
#include <kdev/callback.h>
#include <kdev/service.h>
#include <kdev/core/callback_api.h>

#include "psp/portio.h"

#include "time_devrtc.h"

static time64_t mktime64(
		const unsigned int year0, const unsigned int mon0,
		const unsigned int day,   const unsigned int hour,
		const unsigned int min,   const unsigned int sec);

/*take from QEMU and Linux rtc driver */
#define RTC_PORT_ADDR	     0x70
#define RTC_PORT_DATA	     0x71
#define RTC_SECONDS             0
#define RTC_MINUTES             2
#define RTC_HOURS               4
#define RTC_DAY_OF_WEEK         6
#define RTC_DAY_OF_MONTH        7
#define RTC_MONTH               8
#define RTC_YEAR                9
#define RTC_CENTURY            50

#define RTC_REG_A              10
#define RTC_REG_B              11
#define RTC_REG_C              12
#define RTC_REG_D              13
/* For two digit years assume time is always after that */
#define CMOS_YEARS_OFFS 2000

#define REG_A_UIP 0x80

#define REG_B_SET  0x80
#define REG_B_PIE  0x40
#define REG_B_AIE  0x20
#define REG_B_UIE  0x10
#define REG_B_SQWE 0x08
#define REG_B_DM   0x04
#define REG_B_24H  0x02

#define REG_C_UF   0x10
#define REG_C_IRQF 0x80
#define REG_C_PF   0x40
#define REG_C_AF   0x20
#define REG_C_MASK 0x70


/* Routines for accessing the CMOS RAM/RTC. */
static unsigned char rtc_cmos_read(unsigned char addr) {

	psp_ioport_write8(RTC_PORT_ADDR, addr);
	return psp_ioport_read8(RTC_PORT_DATA);
}

static inline int rtc_from_bcd(int a, int reg_b) {
    if ((a & 0xc0) == 0xc0) return -1;
    return (reg_b & REG_B_DM) ? a : ((a >> 4) * 10) + (a & 0x0f);
}

static time64_t rtc_hw_read(P4_bool_t dbgdump) {
	unsigned int year, mon, day, hour, min, sec, cent = 0;
	int reg_b;

	/*
	 * If UIP is clear, then we have >= 244 microseconds before
	 * RTC registers will be updated.  Spec sheet says that this
	 * is the reliable way to read RTC - registers. If UIP is set
	 * then the register access might be invalid.
	 */

	// TODO This is not a safe sleep loop

	while ((rtc_cmos_read(RTC_REG_A) & REG_A_UIP)) ;

	reg_b = rtc_cmos_read(RTC_REG_B);

	sec  = rtc_from_bcd(rtc_cmos_read(RTC_SECONDS), reg_b);
	min  = rtc_from_bcd(rtc_cmos_read(RTC_MINUTES), reg_b);
	hour = rtc_from_bcd(rtc_cmos_read(RTC_HOURS), reg_b);
	day  = rtc_from_bcd(rtc_cmos_read(RTC_DAY_OF_MONTH), reg_b);
	mon  = rtc_from_bcd(rtc_cmos_read(RTC_MONTH), reg_b);
	year = rtc_from_bcd(rtc_cmos_read(RTC_YEAR), reg_b);
	cent = rtc_from_bcd(rtc_cmos_read(RTC_CENTURY), reg_b);

	if (cent) {
		year += cent * 100;
	} else
		year += CMOS_YEARS_OFFS;

	if (dbgdump) {
		drv_put_s("RTC is: ");
		drv_put_d(year);
		drv_put_s("-");
		drv_put_d(mon);
		drv_put_s("-");
		drv_put_d(day);
		drv_put_s(" ");
		drv_put_d(hour);
		drv_put_s(":");
		drv_put_d(min);
		drv_put_s(":");
		drv_put_d(sec);
		drv_put_s("\n");
	}
	return mktime64(year, mon, day, hour, min, sec);
}



/* declare driver */

static P4_e_t rtc_init_prov( drv_prov_t *prov, drv_config_header_t *config_header __unused, P4_size_t config_sz __unused);
static P4_e_t rtc_init_gate( drv_gate_t *gate);
static P4_e_t rtc_gd_init( drv_gd_t *gd, drv_glock_t *glock, drv_qorder_t qorder);
static P4_e_t rtc_read( drv_gd_t *gd, void * u_buff, P4_size_t buff_sz, void * u_ctrl, void * u_meta, P4_size_t *read_sz);
static P4_e_t rtc_sync( drv_gd_t *gd );
extern P4_time_t rtc_get_time( void );

static drv_prov_ops_t prov_ops = {
	.init_drv = NULL,
	.init_prov = rtc_init_prov,
    .init_gate = rtc_init_gate,
};

static drv_gate_ops_t gate_ops = {
    .gd_init = rtc_gd_init,
    .read    = rtc_read,
	.psync   = rtc_sync,
};

DRV_DECLARE_DRV(devrtc, &prov_ops);

static struct timespec *boot_ref;

/* implementation */
P4_e_t rtc_init_prov( drv_prov_t *prov, drv_config_header_t *config_header __unused, P4_size_t config_sz __unused) {

	boot_ref = drv_prov_get_priv(prov, sizeof(struct timespec));
    boot_ref->tv_sec  =  rtc_hw_read(1);
    boot_ref->tv_nsec =  -p4_get_time(); /* get_time will always increase, so no need to normalize right now */

	return P4_E_OK;
}


P4_e_t rtc_init_gate(drv_gate_t *gate) {
    drv_gate_set_ops (gate, &gate_ops);
    return P4_E_OK;
}

P4_e_t rtc_gd_init( drv_gd_t *gd, drv_glock_t *glock __unused, drv_qorder_t qorder __unused) {

	//struct timespec *boot_ref = drv_prov_get_priv( drv_gate_get_prov( drv_gd_get_gate(gd) ), sizeof(struct timespec) );
	struct timespec *this_ref = drv_gd_get_priv(gd, sizeof(struct timespec));  /* get stash for time reference of this descriptor */

   	*this_ref = *boot_ref;

    return P4_E_OK;
}

/* on fsync call, we reload the RTC's value. */

P4_e_t rtc_sync( drv_gd_t *gd ) {
	struct timespec *this_ref = drv_gd_get_priv(gd, sizeof(struct timespec));  /* get stash for time reference of this descriptor */
    this_ref->tv_sec  = rtc_hw_read(1);
    this_ref->tv_nsec = -p4_get_time(); /* get_time will always increase, so no need to normalize right now */

	return P4_E_OK;
}

P4_time_t rtc_get_time( void ) {

	P4_time_t t = p4_get_time();
	if (boot_ref) { /* this should normally be the case */
		t += boot_ref->tv_nsec;
		t += boot_ref->tv_sec*1000000000LL;
	} else { /* however */
		time64_t rtc = rtc_hw_read(1);
		if (rtc > 0) {
			t %=         1000000000LL; /* leave some sub-second noise, but chop the rest */
			t += (rtc-1)*1000000000LL; /* round down by one second, so we don't accidentally end up in the future */
		}
	}

	return t;
}

/* Do not use from kernel space. Dark segmentation clouds will rise upon you! */

P4_e_t rtc_read( drv_gd_t *gd, void * u_buff, P4_size_t buff_sz, void * u_ctrl __unused, void * u_meta __unused, P4_size_t *read_sz) {

	if (!u_buff || buff_sz < sizeof(struct timespec) || !read_sz) return P4_E_SIZE;

	struct timespec *wall_init = drv_gd_get_priv(gd, sizeof(struct timespec)); /* retrieve offset to boot time */
	if (!wall_init) return P4_E_STATE;

    struct timespec wall = *wall_init;
    P4_time_t ut   = p4_get_time();                /* get time since boot in nano->micro seconds */
    wall.tv_nsec += ut;                            /* sum up to current wall time */
    wall.tv_sec  += wall.tv_nsec / 1000000000LL;   /* and normalize the struct values */
    wall.tv_nsec  = wall.tv_nsec % 1000000000LL;

    /* copy the time structure back to user space */
    P4_e_t rc;
    P4_size_t sz = sizeof(struct timespec);
    rc = drv_memcpy_out(read_sz, &sz, sizeof(sz));
    if (!rc) rc = drv_memcpy_out(u_buff, &wall, sizeof(struct timespec));
   	return rc;
}

/* mktime -- plain from Linux kernel, may not be publishable */

/*
 * mktime64 - Converts date to seconds.
 * Converts Gregorian date to seconds since 1970-01-01 00:00:00.
 * Assumes input in normal date format, i.e. 1980-12-31 23:59:59
 * => year=1980, mon=12, day=31, hour=23, min=59, sec=59.
 *
 * [For the Julian calendar (which was used in Russia before 1917,
 * Britain & colonies before 1752, anywhere else before 1582,
 * and is still in use by some communities) leave out the
 * -year/100+year/400 terms, and add 10.]
 *
 * This algorithm was first published by Gauss (I think).
 *
 * A leap second can be indicated by calling this function with sec as
 * 60 (allowable under ISO 8601).  The leap second is treated the same
 * as the following second since they don't exist in UNIX time.
 *
 * An encoding of midnight at the end of the day as 24:00:00 - ie. midnight
 * tomorrow - (allowable under ISO 8601) is supported.
 */

static time64_t mktime64(
		const unsigned int year0, const unsigned int mon0,
		const unsigned int day,   const unsigned int hour,
		const unsigned int min,   const unsigned int sec) {

	unsigned int mon = mon0, year = year0;

	/* 1..12 -> 11,12,1..10 */
	if (0 >= (int) (mon -= 2)) {
		mon += 12;	/* Puts Feb last since it has leap day */
		year -= 1;
	}

	return ((((time64_t)
		  (year/4 - year/100 + year/400 + 367*mon/12 + day) +
		  year*365 - 719499
	    )*24 + hour /* now have hours - midnight tomorrow handled here */
	  )*60 + min /* now have minutes */
	)*60 + sec; /* finally seconds */
}

/* EOF */
