/*
 * time.h
 *
 * Header for the partition helper functions of the RTC dev.
 *
 * No need to use it, you can always implement your own. The structures are almost a plain copy from Linux.
 * Also include "gettimeofday.c" from the custom pool
 *
 *  Created on: 30.07.2018
 *      Author: thorsten
 */

#ifndef TIME_DEVRTC_H_
#define TIME_DEVRTC_H_

#include <p4.h>


#define	CLOCK_REALTIME           0  /* high res gettimeofday */
#define	CLOCK_MONOTONIC          1  /* == p4_get_time() */
//#define	CLOCK_PROCESS_CPUTIME_ID 2  /* not implemented, but should be available through the Thread API. */
//#define	CLOCK_THREAD_CPUTIME_ID  3  /* not implemented, but should be available through the Thread API */
#define	CLOCK_MONOTONIC_RAW      4  /* This clock has many dubious side-effects and may not work at all */
//#define	CLOCK_REALTIME_COARSE    5  /* n.a. */
//#define	CLOCK_MONOTONIC_COARSE   6  /* n.a. */
	/* there are a few more though ... */

typedef P4_sint32_t clockid_t;

typedef P4_sint64_t time64_t;

/* both types must have same member types */

struct timeval {
	P4_sint64_t tv_sec;     /* seconds */
	P4_sint64_t tv_usec;    /* microseconds */
};

struct timespec {
	P4_sint64_t tv_sec;     /* seconds */
	P4_sint64_t tv_nsec;    /* nanoseconds */
};

typedef time64_t time_t;

struct tm {
	P4_sint32_t tm_sec;    /* Seconds (0-60) */
	P4_sint32_t tm_min;    /* Minutes (0-59) */
	P4_sint32_t tm_hour;   /* Hours (0-23) */
	P4_sint32_t tm_mday;   /* Day of the month (1-31) */
	P4_sint32_t tm_mon;    /* Month (0-11) */
	P4_sint32_t tm_year;   /* Year - 1900 */
	P4_sint32_t tm_wday;   /* Day of the week (0-6, Sunday = 0) */
	P4_sint32_t tm_yday;   /* Day in the year (0-365, 1 Jan = 0) */
	P4_sint32_t tm_isdst;  /* Daylight saving time */
};

P4_e_t gettimeofday(struct timeval *tv, void *tz);
P4_e_t gettm(struct tm *tm);
struct tm *gmtime_r (const time_t *__restrict tim_p, struct tm *__restrict res);

/** an equivalent to the POSIX function
 *  However, a P4-error is returned on failure. All clock-types source from RTC
 *  as a basis, continuing with a monotonic clock from the P4 kernel.
 */
extern P4_e_t clock_gettime (clockid_t __clock_id, struct timespec *__tp);

#endif /* TIME_DEVRTC_H_ */
