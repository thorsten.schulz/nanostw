#ifndef USER_MACROS_H
#define USER_MACROS_H

/* HINT:
 * if doing clock'ed runs, set cpu governor first:
 * echo performance | sudo tee /sys/devices/system/cpu/cpu?/cpufreq/scaling_governor
 */

#include <stdint.h>

#ifdef __COMPCERT__
#define kcg_assign(a,b,c) __builtin_memcpy_aligned(a, b, c, _Alignof(*(a)))
#define SqrtR_mathext(In) ((kcg_real) __builtin_fsqrt(In))

#else
#include <string.h>
#define kcg_assign memcpy

#ifdef JAILHOUSE
#include <inmate.h>
#define SqrtR_mathext(Out, In) (sqrtp(Out, In))

/* looks odd, but returning a double would incur something with SSE which is not supported in the bare-metal case */
/* occurances of SqrtR_mathext(In) must be patched */

static inline void sqrtp(double *dst, double src) {
	asm volatile ( "sqrtsd %0,%0" : "+x" (src) );
	*dst = src;
}

#else

#include <math.h>
#define SqrtR_mathext(In) ((kcg_real) sqrt(In))
#define LogR_mathext(In)  ((kcg_real) log10(In))
#define PowerI_mathext(In, Power)  ((kcg_real)  pow(In, Power))

#endif

#endif /* COMPCERT */

#define ASSERT_SIZE(x,y) typedef char x ## _assert_size_t[-1+10*(sizeof(x) == (y))]

#define kcg_int kcg_int
typedef int32_t kcg_int;

#define kcg_uint64 kcg_uint64
typedef uint64_t kcg_uint64;

#define kcg_uint32 kcg_uint32
typedef uint32_t kcg_uint32;

#define kcg_uint16 kcg_uint16
typedef uint16_t kcg_uint16;

#define kcg_uint8 kcg_uint8
typedef uint8_t kcg_uint8;

#define kcg_int64 kcg_int64
typedef int64_t kcg_int64;

#define kcg_int32 kcg_int32
typedef int32_t kcg_int32;

#define kcg_int16 kcg_int16
typedef int16_t kcg_int16;

#define kcg_int8 kcg_int8
typedef int8_t kcg_int8;

#define kcg_size kcg_size
typedef int32_t kcg_size;

#endif /*USER_MACROS_H*/
