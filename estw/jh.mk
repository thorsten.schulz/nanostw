#
# A Makefile-include for building the nanoStw project partially on Jailhouse
#
# Use this from the Master-Makefile. However, this will only trial-compile
# the sources, w/o building an image. It requires a checkout repo of jailhouse
# with the path to be defined in the top-makefile.
#
# Copyright (c) Universität Rostock, 2020
#
# Authors:
#  Thorsten Schulz <thorsten.schulz@uni-rostock.de>
#
# SPDX-License-Identifier: Apache-2.0
#

#TODO take a peek at https://www.gnu.org/software/make/manual/html_node/Canned-Recipes.html#Canned-Recipes

C_DST_MODEL_STW := $(notdir $(C_SRC_MODEL_STW))
C_DST_MODEL_TSM := $(notdir $(C_SRC_MODEL_TSM))

# after changing any of the two lists, you need to touch the jailhouse*.bbappend file to trigger the rule
JH_CELLSRC = stw.c nollie.c slide-marks.c curve25519-donna.c sys.c ivshmem-jh.c ivshmem-common.c tau_xservice_client.c
JIJ_DEPS = $(JH_CELLSRC) \
 Makefile user_macros.h ivshmem.h trdpSvc.h \
 $(C_DST_MODEL_STW) $(C_DST_MODEL_STW)/Makefile tau_xsession_defaults.h tau_xmarshall_map.h \
 $(C_DST_MODEL_TSM) $(C_DST_MODEL_TSM)/Makefile donna \
 gptp_ipcdef.h ptptypes.hpp \

COMMON_CELLS := stw trdp wg
COMMON_CONFIGS := $(addsuffix .c,$(COMMON_CELLS))

# if you add another system-config, you also have to add manual checking rules to the far end of this make-inlcude file
J_CELLS_fitlet2:= $(addprefix fitlet2.,$(COMMON_CONFIGS))
J_CELLS += fitlet2.c $(J_CELLS_fitlet2)
J_CELLS_fitlet4:= $(addprefix fitlet4.,$(COMMON_CONFIGS))
J_CELLS += fitlet4.c $(J_CELLS_fitlet4)
J_CELLS_edge4  := $(addprefix edge4.,$(COMMON_CONFIGS))
J_CELLS += edge4.c $(J_CELLS_edge4)
J_CELLS_2cards := $(addprefix 2cards.,$(COMMON_CONFIGS) e1000.c)
J_CELLS += 2cards.c $(J_CELLS_2cards)

# patches are only applied to a virgin submodule - you may need to make jailhouse-clean first on changes
J_BASE_PATCHES = \
 0010-inmates-general-add-scade-stub-to-make.patch \
 0014-inmates-lib-increase-default-stack.patch \
 0015-hypervisor-efifb-small-lcd.patch \
 0016-inmates-lib-add-tsc-tune.patch \
 0017-python3.patch \

# 0011-inmates-lib-add-sqrt-without-sse-return.patch
JNR_DEPS = build-jh-trdpsvc.sh Makefile trdp.mk $(C_SRC_USVC)  $(C_SRC_TSVC) $(C_HDR_SVC) $(LOCAL_TRDP) \
 $(C_SRC_MODEL_TSM) $(C_SRC_MODEL_TSM)/../user_macros.h app/slide-marks.c app/curve25519-donna.c app/donna app/user_macros.h utils.bm/sys.c user_macros.h

JNR_ODEPS= etc etc/trdp root/.ssh/known_hosts
JRC_DEPS = estw_bash_history chrony.root.conf


# part one, inject all patches, sources, config, what-not into the jailhouse-images build-system

ifeq ($(realpath $(JI)),)
$(and $(JI),$(warning JI path set to $(JI), but it is inaccessible.))
else

JIJ = $(JI)/recipes-jailhouse/jailhouse
JNR = $(JI)/recipes-core/non-root-initramfs
JRC = $(JI)/recipes-core/customizations
JIJ_DST = $(JIJ)/files/estw
JNR_DST = $(JNR)/files/estw
JNR_OVL = $(JNR)/files/overlay
JRC_DST = $(JRC)/files

JIJ_APND = jailhouse_0.12.bbappend jailhouse_latest.bbappend
JNR_APND = non-root-initramfs_2020.05.1.bbappend
JRC_APND = local.inc

#create basic sub-folders for my stuff and push BB-appends
$(JIJ_DST) :
	@echo "  SETUP    $@"
	@mkdir -p "$@"

$(JNR_DST) : patches/0100-br2-config-estw.patch
	@echo "  SETUP    $@"
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@patch -d $(JNR)/files -p1 <"$<"

$(JIJ)/%.bbappend : jh/%.bbappend
	@echo "  CP       $(<F)"
	@cp -rL "$<" "$@"
	@echo "EXTRA_JAILHOUSE_CONFIGS_amd64 += \" $(addprefix cells/,$(J_CELLS)) \"" >> "$@"
	@echo "SRC_URI += \" $(addprefix file://estw/,$(JIJ_DEPS)) $(addprefix file://,$(J_BASE_PATCHES)) \"" >> "$@"

$(JNR)/$(JNR_APND) : jh/$(JNR_APND)
	@echo "  CP       $(<F)"
	@cp "$^" "$@"

$(JRC)/$(JRC_APND) : jh/$(JRC_APND)
	@echo "  CP       $(<F)"
	@cp "$^" "$@"

$(JIJ)/files/%.patch : patches/%.patch
	@echo "  CP       $(<F)"
	@cp "$<" "$@"

$(JNR)/files/%.patch : patches/%.patch
	@echo "  CP       $(<F)"
	@cp "$<" "$@"

#move files for the root-linux customizations
$(JRC_DST)/% : jh/%
	@echo "  CP       $(<F)"
	@cp "$<" "$@"

#config files within the non-root Linux
$(JNR_OVL)/etc : jh/etc-nr/*
	@echo "  SETUP    $@"
	@cp -rLu $^ "$@"
	@touch "$@"

$(JNR_OVL)/etc/trdp : ../config
	@echo "  SETUP    $@"
	@$(RM) -r "$@"
	@cp -ru "$<" "$@"

$(JNR_OVL)/root/.ssh/known_hosts: jh/etc-nr/known_hosts
	@echo "  CP       $(<F)"
	@mkdir -p "$(@D)"
	@cp "$<" "$@"

#Cell-configs for the Hypervisor
$(JIJ)/files/cells/%.c : jh/cells/%.c
	@echo "  CP       $(<F)"
	@mkdir -p $(@D)
	@cp "$<" "$@"

#application sources for the bare-metal cell (here, the Stw)
$(JIJ_DST)/% : app/% | $(JIJ_DST)
	@echo "  CP       $(<F)"
	@cp -rL "$<" "$@"

$(JIJ_DST)/sys.c : utils.bm/sys.c | $(JIJ_DST)
	@echo "  CP       $(<F)"
	@cp -rL "$<" "$@"

$(JIJ_DST)/Makefile : app/makefile.jailhouse | $(JIJ_DST)
	@echo "  CP       $(<F)"
	@cp "$<" "$@"

$(JIJ_DST)/tau_x%.h : $(TRDP_HDR_COMMON)/tau_x%.h | $(JIJ_DST)
	@echo "  CP       $(<F)"
	@cp "$<" "$@"

$(JIJ_DST)/$(C_DST_MODEL_STW) : $(C_SRC_MODEL_STW)/kcg_files.txt | $(JIJ_DST)
	@echo "  CP       $(<D)"
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@cp "$<" $(<D)/*.c $(<D)/*.h "$@"
	@patch -d "$@" -p4 <patches/0013-patch-different-sqrt-interface-into-KCG-code.patch

$(JIJ_DST)/$(C_DST_MODEL_STW)/Makefile : $(C_SRC_MODEL_STW)/makefile.jailhouse | $(JIJ_DST)/$(C_DST_MODEL_STW)
	@echo "  CP       $<"
	@cp "$<" "$@"

$(JIJ_DST)/$(C_DST_MODEL_TSM) : $(C_SRC_MODEL_TSM)/kcg_files.txt | $(JIJ_DST)
	@echo "  CP       $(<D)"
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@cp "$<" $(<D)/*.c $(<D)/*.h "$@"

$(JIJ_DST)/$(C_DST_MODEL_TSM)/Makefile : $(C_SRC_MODEL_TSM)/makefile.jailhouse | $(JIJ_DST)/$(C_DST_MODEL_TSM)
	@echo "  CP       $<"
	@cp "$<" "$@"

#move the other non-root dependencies for the trdpUioSvc app and TRDP
#TODO this also copies the build-dir of trdp
#TODO this rule is really broken
$(addprefix $(JNR)/files/%/,$(JNR_DEPS)) : $(JNR_DEPS) | $(JNR_DST)
	@echo "  CP       $(<D)"
	@cp -rLu --parents $^ "$(JNR_DST)"


jail-inject: \
 $(addprefix $(JIJ_DST)/,$(JIJ_DEPS)) $(addprefix $(JIJ)/files/,$(J_BASE_PATCHES)) $(addprefix $(JIJ)/files/cells/,$(J_CELLS)) $(addprefix $(JIJ)/,$(JIJ_APND)) \
 $(addprefix $(JRC_DST)/,$(JRC_DEPS))  $(JRC)/$(JRC_APND) \
 $(addprefix $(JNR_DST)/,$(JNR_DEPS)) $(addprefix $(JNR_OVL)/,$(JNR_ODEPS)) $(JNR)/$(JNR_APND) \

	@echo "  TIP      You can now run 'cd $(JI) && ./build-images.sh --rt --latest' and choose the fitlet2 config."

# clean-up all untracked modifications
# and undo the build-root patch
jail-reject:
	@cd $(JI) && git clean -xfd -e "/build/"
	@patch -R -d $(JNR)/files -p1 <"patches/0100-br2-config-estw.patch" || echo

wg-network-qemu:
	sudo ./network-qemu-wg.sh

.FORCE: $(JNR_OVL)/etc
.PHONY: jail-inject jail-reject wg-network-qemu
endif

# so, here is the second half to try out cell configs and inmate binaries without having to build the image
ifeq ($(realpath $(JH)),)
$(and $(JH),$(warning JH path set to $(JH), but it is inaccessible.))
else

JH_DST = $(JH)/inmates/estw
JH_CELLBINS = $(addprefix $(JH_DST)/,$(JH_CELLSRC:%.c=%.o))

$(JH_DST):
	mkdir -p "$@"
	cat $(addprefix patches/,$(J_BASE_PATCHES)) | patch -d "$(JH)" -p1

#Cell-configs for the Hypervisor
$(JH)/configs/x86/%.c : jh/cells/%.c
	cp -rL "$<" "$@"

$(JH_DST)/% : app/%  | $(JH_DST)
	cp -rL "$<" "$@"

$(JH_DST)/sys.c : utils.bm/sys.c  | $(JH_DST)
	cp -rL "$<" "$@"

$(JH_DST)/Makefile : app/makefile.jailhouse | $(JH_DST)
	cp "$<" "$@"

$(JH_DST)/tau_x%.h : $(TRDP_HDR_COMMON)/tau_x%.h | $(JH_DST)
	cp "$<" "$@"

$(JH_DST)/$(C_DST_MODEL_STW) : $(C_SRC_MODEL_STW)/kcg_files.txt | $(JH_DST)
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@cp "$<" $(<D)/*.c $(<D)/*.h "$@"
	patch -d "$@" -p4 <patches/0013-patch-different-sqrt-interface-into-KCG-code.patch

$(JH_DST)/$(C_DST_MODEL_STW)/Makefile : $(C_SRC_MODEL_STW)/makefile.jailhouse | $(JH_DST)/$(C_DST_MODEL_STW)
	cp "$<" "$@"

$(JH_DST)/$(C_DST_MODEL_TSM) : $(C_SRC_MODEL_TSM)/kcg_files.txt | $(JH_DST)
	@$(RM) -r "$@"
	@mkdir -p "$@"
	@cp "$<" $(<D)/*.c $(<D)/*.h "$@"

$(JH_DST)/$(C_DST_MODEL_TSM)/Makefile : $(C_SRC_MODEL_TSM)/makefile.jailhouse | $(JH_DST)/$(C_DST_MODEL_TSM)
	cp "$<" "$@"

$(JH_CELLBINS): $(addprefix $(JH_DST)/,$(JIJ_DEPS))
	make -C $(JH) $(JH_KDIRVAR)

$(JH)/configs/%.cell: $(JH)/configs/%.c | $(JH_DST)
	make -C $(JH) $(JH_KDIRVAR)

$(BUILDTMP)/check-%.log: $(addprefix jh/cells/,%.c)
	@rm -f $(@:.log=.err)
	@$(JH)/tools/jailhouse config check $(^:jh/cells/%.c=$(JH)/configs/x86/%.cell) > $@ && echo "  CHECK    $* ok." || ( mv $@ $(@:.log=.err) ; echo "  CHECK--> $^ contain bugs, check \"$(@:.log=.err)\"" )


jailhouse: $(JH_CELLBINS) $(addprefix $(JH)/configs/x86/,$(J_CELLS:%.c=%.cell)) 

#new configs must be added manually for checking
$(BUILDTMP)/check-2cards.log: $(addprefix jh/cells/,$(J_CELLS_2cards))
jailhouse: $(BUILDTMP)/check-2cards.log

$(BUILDTMP)/check-edge4.log : $(addprefix jh/cells/,$(J_CELLS_edge4))
jailhouse: $(BUILDTMP)/check-edge4.log

$(BUILDTMP)/check-fitlet4.log : $(addprefix jh/cells/,$(J_CELLS_fitlet4))
jailhouse: $(BUILDTMP)/check-fitlet4.log

$(BUILDTMP)/check-fitlet2.log : $(addprefix jh/cells/,$(J_CELLS_fitlet2))
jailhouse: $(BUILDTMP)/check-fitlet2.log

jailhouse-clean:
	-make -C $(JH) clean
	@test -d "$(JH_DST)" && cat $(addprefix patches/,$(J_BASE_PATCHES)) | patch -R -d "$(JH)" -p1 || echo
	$(RM) $(addprefix $(JH)/configs/x86/,$(J_CELLS:%.c=%.cell))
	$(RM) -r $(JH_DST)

distclean: jailhouse-clean

.PHONY: jailhouse jailhouse-clean
endif
