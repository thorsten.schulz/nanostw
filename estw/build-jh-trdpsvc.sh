#!/bin/sh

eval $(grep ^BR2_ARCH= ${BR2_CONFIG})
if [ ${BR2_ARCH} = arm ]; then
    ABI="gnueabihf"
fi

make -C ../estw install CC=${BR2_ARCH}-buildroot-linux-uclibc${ABI}-gcc DEB_HOST_GNU_TYPE=${BR2_ARCH}-buildroot-linux-uclibc${ABI} DEB_HOST_MULTIARCH=${BR2_ARCH} prefix=$1
