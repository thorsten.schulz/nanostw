/*
 ============================================================================
 Name        : trdpSvc.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Copyright   : (c) 2017-2020 Universität Rostock
 Description : TRDP-service that takes application data via IPC and interacts
               with the network accordingly, abstracting all this network 
               stuff away from the core application.
.
 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include <errno.h>
#ifndef PIKEOS_POSIX
#include <error.h>
#else

#endif

#define CONST_PAGE_SIZE 0x1000

#include "trdpSvc.h"

#include <tau_xmarshall.h>
#include <tau_xsession.h>


static void dprint(const char* lead, const char* msg, int putNL) {
	if ( lead) fputs(lead, stderr);
	if (  msg) fputs(msg,  stderr);
	if (putNL) fputc('\n', stderr);
}

static TRDP_ERR_T init_svc_shm(
		char *arg,
		TAU_XSESSION_T **trdp, struct ivshmem_dev *dev, uint32_t *client_id, INT32 *IDs,
		const struct trdpSvcRec **_itemI, int *_len_in,  UINT8 **_in,
		const struct trdpSvcRec **_itemO, int *_len_out, const UINT8 **_out) {


	TRDP_ERR_T result = TRDP_NO_ERR;
	const struct shm_struct *shm;
	const char *xml;
	size_t xlength;
	uint32_t remote_state=0, ifaces;
	int devid[2] = { SHM_DEFAULT_NUMBER, ESTW_DEFAULT_SHM_CLIENT_NUMBER};

	if (!trdp || !dev || 0 > ivshmem_parse_ids( arg, devid, 2, '.')) return TRDP_PARAM_ERR;
	*client_id = devid[1];

	printf("SHM-INIT[%d.?-%d]: Blocking for signal paths.\n", devid[0], devid[1]);
	ivshmem_open( dev, devid[0], /* struct pollfd */ NULL);

	if (*client_id == dev->id || *client_id >= dev->peers)
		error(TRDP_MEM_ERR, EINVAL, "Invalid TRDP-peer ID (%d) detected (self = %d/%d)", *client_id, dev->id, dev->peers);

	remote_state = dev->lstate[*client_id];
	if (0 == (remote_state & 0xF)) { /* only wait, if we have no lock token */
		toSt ivst = ivshmem_wait_for_state( dev, IVSHMEM_BLOCK, *client_id, &remote_state );
		if ( ivst != STATE_CHANGED_OK )
			error(TRDP_MEM_ERR, ENOENT, "Could not block for shm-state[%d]: err=%d state=%d\n", *client_id,  ivst, remote_state);
	}

	shm = (const struct shm_struct *)(dev->in+(*client_id * dev->out_size));

	if ( shm->magic != TRDP_XSVC_MAGIC )
		error(TRDP_MEM_ERR, EINVAL, "Mapping shm-%d@%08" PRIX64 " brought no real magic (0x%08" PRIX64 ").\n",
				devid[0], (u64)shm, shm->magic);

	/* note, the sizeIf parameter has to be identical. It is (currently) not meant for dynamic configuration, but at
	 * this time more of a safety check. Fail if it mismatches.
	 */
	if ( shm->sizeIf != MAX_INTERFACES )
		error(TRDP_MEM_ERR, EINVAL, "Mapping shm-%d brought unexpected no. of interfaces (%d != %" PRId64 ").\n",
				devid[0], MAX_INTERFACES, shm->sizeIf);

	/* The incomming data from the application ('out') is right behind the shm header. */
	/* the payload block of the SHM contains an array of service records. */
	const struct trdpSvcRec *item = (const struct trdpSvcRec *)(shm+1);
	
	/*  The last record always exists and contains information about this instance's xml-config-(file) */
	const struct trdpSvcRec *xmlitem= item + shm->sizeIn + shm->sizeOut;
	if ( !xmlitem->offset ) return TRDP_PARAM_ERR;
	xml = (const char *)(shm+1) + xmlitem->offset;
	/* if xlength is >MAX_XML_FILENAME_LENGTH, we can load the config directly from SHM: */
	xlength = (xmlitem->size <= MAX_XML_FILENAME_LENGTH) ? 0 : xmlitem->size;

	printf("Loading: \"%s\"\n", xlength ? "<from SHM>" : xml);
#if defined(PIKEOS_ELINOS)
	char filedev[32];
	if (!xlength) {
		int fdx;
		if (0 > (fdx = open_p4_resource(xml, O_RDONLY, EVM_FILE_CONFIG, EVM_FILE_FILENAME, 0)))
			error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Opening \"%s\" failed.\n", xml);
		unsigned int size;
		if (0 == (result = ioctl(fd, VMDRV_GET_FILE_SIZE, &size))) {
			xlength = size;
			xml = mmap(NULL, xlength, PROT_READ, MAP_SHARED, fdx, 0);
			close(fdx);
			result = tau_xsession_load( xml , xlength, dprint, shm->xtype_map);
			munmap(xml, xlength);
		}
		if (result) return result;
	} else
		if (TRDP_NO_ERR != (result = tau_xsession_load( xml , xlength, dprint, shm->xtype_map)))
			return result;
#else
	if (TRDP_NO_ERR != (result = tau_xsession_load( xml , xlength, dprint, shm->xtype_map)))
		return result;
#endif

	/* vos_printLog is available from 'ere onwards */
	for (ifaces=0; ifaces<MAX_INTERFACES && *shm->xnetif[ifaces]; ifaces++) {
		vos_printLog(VOS_LOG_INFO, "INTERFACE[%d/%d]: \"%s\"\n", ifaces, MAX_INTERFACES, shm->xnetif[ifaces]);
		if (TRDP_NO_ERR != (result = tau_xsession_init( &trdp[ifaces], shm->xnetif[ifaces], 4000, -1, NULL )))
			return result;
	}

	/* now, subscribe / publish the I/Os of the peer application */
	for (uint64_t i=0; i<shm->sizeIn; i++, item++) if (item->iface < ifaces) {
		if (item->ComId) {
			result = tau_xsession_subscribe( trdp[item->iface], item->ComId, &IDs[i], 1, NULL );
			if (TRDP_NO_ERR != result) return result;
			vos_printLog(VOS_LOG_INFO, "SUBSCRIBED[%d]: <%d>%d[%d]\n", IDs[i], item->iface, item->ComId, item->size);
		}
	} else {
		vos_printLog(VOS_LOG_ERROR, "Service item %" PRIu64 "/%" PRIu64 " referenced an out-of-index interface %d/%d.\n",
				i, shm->sizeIn, item->iface, ifaces);
		return TRDP_MEM_ERR;
	}
	/* item-pointer is NOT reset, since out-items follow the in-items */
	for (uint64_t i=0; i<shm->sizeOut; i++, item++) if (item->iface < ifaces) {
		if (item->ComId) {
			result = tau_xsession_publish( trdp[item->iface], item->ComId, &IDs[shm->sizeIn+i], 1,
					(const UINT8 *)shm + item->offset, item->size, NULL );
			if (TRDP_NO_ERR != result) return result;
			vos_printLog(VOS_LOG_INFO, "PUBLISHED[%d]: <%d>%d[%d]\n", IDs[shm->sizeIn+i], item->iface, item->ComId, item->size);
		}
	} else {
		vos_printLog(VOS_LOG_ERROR, "Service item %" PRIu64 "/%" PRIu64 " referenced an out-of-index interface %d/%d.\n",
				i, shm->sizeOut, item->iface, ifaces);
		return TRDP_MEM_ERR;
	}
	/* return the init-token, so the other side knows, we are not yet in sync. */
	ivshmem_set_state( dev, TRDP_XSVC_ALIVE );

	/* only set "return-values" at the end, when we are successful */
	*_itemI  = (const struct trdpSvcRec *)(shm+1);
	*_itemO  = *_itemI + shm->sizeIn;
	*_len_in = shm->sizeIn;
	*_len_out= shm->sizeOut;
	*_out    = (const UINT8 *)(shm+1);
	*_in     = dev->out;
	return result;
}

static struct ivshmem_dev dev; /* make this global for cleanup */

static void cleanup_state( void ) {
	printf(__FILE__ " finalizing.\n");
	ivshmem_finalize( &dev );
}

int main(int argc, char **argv) {
	static TAU_XSESSION_T *trdp[MAX_INTERFACES];

	fprintf(stderr, "[stderr] %s\n", BUILD_DATE);
	fprintf(stdout, "[stdout] %s\n", BUILD_DATE);

	char *arg = argc == 2 ? argv[1] : NULL;
	TRDP_ERR_T result;
	const struct trdpSvcRec *itemIn = NULL;
	const struct trdpSvcRec *itemOut = NULL;
	int sizeIn=0, sizeOut=0;
	UINT8 *payloadI = NULL;
	const UINT8 *payloadO =NULL; /* from APP's perspective */
	uint32_t client_id;
	INT32 IDs[MAX_TELEGRAMS];
	memset( IDs, -1, sizeof(IDs) );

	/* after this, I need to clean up */
	if (TRDP_NO_ERR == (result = init_svc_shm(
									arg, trdp, &dev, &client_id, IDs,
									&itemIn, &sizeIn, &payloadI,
									&itemOut, &sizeOut, &payloadO ))) {

		uint32_t state = TRDP_XSVC_ALIVE;
		toSt ivst = STATE_TIMEOUT;
		INT32 * inID = IDs;
		INT32 *outID = IDs+sizeIn;
		TRDP_PD_INFO_T info[sizeIn]; /* have a stash for meta-data from received packets */
		atexit( cleanup_state );

		u64 tv1, tv2;
		tv1 = tsc_read_ns();
		do {
			static long long unsigned t_getcom=0, t_setcom=0, t=ROUNDS_PER_STAT-1;

			if (ivst == STATE_TIMEOUT || !(state & TRDP_XSVC_GETCOM)) {
			// fprintf(stderr, "SVC Getcom ?\n");
				ivst = ivshmem_wait_for_state( &dev, tv1+IVSHMEM_MSEC(100)/*IVSHMEM_BLOCK*/, client_id, &state);
				if (ivst != STATE_TIMEOUT && ivst != STATE_CHANGED_OK) break; /* svc cannot recover from gone-state and must restart */
			// fprintf(stderr, "SVC Getcom(%d) <<%0x<<\n", ivst, state);
				tv1 = tsc_read_ns();
			}
			tv2 = tsc_read_ns();
			if (ivst == STATE_CHANGED_OK && (state & TRDP_XSVC_GETCOM)) {

				/* iterate all inputs */
				for (int i=0; i<sizeIn; i++) if (itemIn[i].ComId && inID[i] >= 0) {
					UINT32 temp;
					tau_xsession_getCom( trdp[itemIn[i].iface], inID[i],
							payloadI + itemIn[i].offset, itemIn[i].size, &temp, info+i);
				}

				/* return the lock */
			// fprintf(stderr, "SVC Getcom >>%0x>>\n", state);
				ivshmem_set_state( &dev, state);
				state = TRDP_XSVC_ALIVE;
				t_getcom += tsc_read_ns() - tv2;

				/* next read blocks for the Scade model thread to do its work */
				/* before we can (possibly) lock the send data and push it out */
			// fprintf(stderr, "SVC Setcom ?\n");
				ivst = ivshmem_wait_for_state( &dev, IVSHMEM_BLOCK, client_id, &state);
			// fprintf(stderr, "SVC Setcom <<%0x<<\n", state);
				tv2 = tsc_read_ns();
			}

			if (ivst == STATE_CHANGED_OK && (state & TRDP_XSVC_SETCOM)) {
			//	fprintf(stderr, "SVC <<=*=>>\n");

				/* export data of model to TRDP */
				for (int i=0; i<sizeOut; i++) if (itemOut[i].ComId && outID[i] >= 0) {
					tau_xsession_setCom( trdp[itemOut[i].iface], outID[i],
							payloadO + itemOut[i].offset, itemOut[i].size);
				}
				if (!(state & TRDP_XSVC_GETCOM)) {
			//		fprintf(stderr, "SVC Setcom >>%0x>>\n", state);
					ivshmem_set_state( &dev, state);
					state = TRDP_XSVC_ALIVE;
				}

				/* iterate all requests for input data */
				for (int i=0; i<sizeIn; i++) {
					if (itemIn[i].request && itemIn[i].iface < MAX_INTERFACES)
						tau_xsession_request( trdp[itemIn[i].iface], inID[i]);
				}
			} else {
				printf("TrdpSvc: What is happening? Communication with App seems broken.\n");
				//usleep(10000); /* give it minimal sleep in odd conditions */
			}

			/* in Linux, sys-start-based, however, PikeOS-Posix does not provide a CLOCK_MONOTONIC,
			 * see _POSIX_MONOTONIC_CLOCK. However, CLOCK_REALTIME is based on the system time, as such, is also based
			 * on the system-uptime. */
			{
				long long unsigned tv, _tv1 = tv1;
				t_setcom += tsc_read_ns() - tv2;

				tau_xsession_cycle_all();
				tv1 = tsc_read_ns();

				if (t+1==ROUNDS_PER_STAT) {
				/* we'd have to sort out, when the last packets came in to do some stats. */
					for (int i=0; i<sizeIn; i++) /*if (itemIn[i].request)*/ {
						VOS_TIMEVAL_T tvr;
						tau_xsession_getRxTime( trdp[itemIn[i].iface], inID[i], &tvr);
						tv = tv1/NS_PER_USEC - (tvr.tv_sec*1000000ULL+tvr.tv_usec);
						if (tv < (tv1-_tv1)/NS_PER_USEC) printf("<%d>:%4lld ", itemIn[i].ComId, tv/100ULL);
					}
				}
			}

			if (++t==ROUNDS_PER_STAT) {
				printf("get=%5llu us / get+set=%5llu us\r\n",
						t_getcom/t/NS_PER_USEC, (t_getcom+t_setcom)/t/NS_PER_USEC
						); fflush(stdout);
				t=0; t_getcom=0; t_setcom=0;
			}
		} while (ivst == STATE_CHANGED_OK || ivst == STATE_TIMEOUT); /* exit on abnormal comm state */
	} else
		vos_printLog(VOS_LOG_ERROR, "%s\n", tau_getResultString(result));

	tau_xsession_delete( NULL );

	return result;
}
