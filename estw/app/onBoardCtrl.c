/*
 ============================================================================
 Name        : onBoardCtrl.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Version     :
 Copyright   : (c) 2017-2020 Universität Rostock
 Description : TRDP-stub to wire up Scade models
 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <ctype.h>

#include "../onTrain.bm/onBoard_train.h"
#include "../onTrain.bm/kcg_consts.h"

#include "keypoll.h"
#include <tau_xmarshall.h>
#include <time.h>
#include <tau_xsession.h>
#include "trdpSvc.h"

typedef struct DMI_ctrl {
	char key;
} DMI_ctrl;

/* Scade-model I/O to be handled locally */

struct scade_local {
	struct scade_in_local {
		TrainId_TIU self;
		kcg_bool turnOff;
		kcg_bool trip;
		AccelerationUnit_TIU ctrl;
		M_directioncontroller_command_T_TIU selectDir;
		TimeUnit_TIU dT;
	} in;
	struct scade_out_local {
		VelocityUnit_TIU modelPWM;
		kcg_bool send_report;
		kcg_uint8 packedFlags;
		kcg_bool paramChanged;
		kcg_bool ivState[5];
	} out;
};

/* Scade-model I/O to be sent across the network */
struct scade_in_ext {
	DMI_ctrl                dky;
	InterventionCommand_TIU iv;
	TrackLocation_TIU       correctionPoint;
};

struct scade_out_ext {
	TrainReport_TIU report;
};

struct shm_out {
	struct trdpSvcRec item[5]; /* scade-i/o-items +1 */
    struct scade_out_ext out;
};

struct shm_in {
	struct scade_in_ext  in;
};

/* This is some const data for the Scade model */

static const TrainParam_TIU trainData_Flirt = {
		.modelFactor = 1, .modelOffset = 30,
		.BrPc = 196, .vMax = 170, .vRelease = 30, .vOnSight = 50, .length = 10000,
		.Ant2CabA = 5000, /* .Ant2CabB is calc as fallback to CabA's antenna */
};

static const char* fncfg = NULL;
static TrainId_TIU self = -1;

static TAU_XSESSION_T *trdp;
static TRDP_PD_INFO_T info0, info1, info2;

static  INT32 subDKyID = ~0;
static UINT32 subDKyLen=  0;

static  INT32 subTLID  = ~0;
static UINT32 subTLLen =  0;

static  INT32 subIVID  = ~0;
static UINT32 subIVLen =  0;

static  INT32 pubRptID = ~0;

DEFINE_TAU_XMARSHALL_MAP( xmap,
		kcg_bool, kcg_char, kcg_int16,
		kcg_int8, kcg_int16, kcg_int32, kcg_int64,
		kcg_uint8, kcg_uint16, kcg_uint32, kcg_uint64,
		kcg_float32, kcg_float64,
		kcg_int32, kcg_int32, kcg_int32
	);

static void dump_key_help() {
	printf( "\r"
			"=================================================================================\n"
			"  8  Switch ON and direction Cab A\n"
			"  2  Switch ON and direction Cab B\n"
			"  +  Increase traction power\n"
			"  0  Set power to zero (mid-position)\n"
			"  -  Decrease power up to braking\n"
			" 1 , Switch OFF; when it is stopped a 2nd press will quit the program\n"
			"  7  +two digits: fire reception of that balise, ie. position\n"
			"=================================================================================\n"
			"To get going, you have to switch on (8/2) and press +. "
			"If you have Trackplan connected, you can set initial position by pressing 717 (position 17 is in the outer ring).\n"
			"The train will throw a fault after 1000m of unsupervised driving. You can turn it off to reset this counter.\n"
			"\n"
	);
}

static void handleInput(struct scade_local *local, const struct scade_out_ext *out, const struct scade_in_ext *in, TrackLocation_TIU *cP) {
	static int num_mode = 0;
	static char num_buf[3] = {0,};

	/* reset first */
	local->in.selectDir = directioncontroller_command_not_defined_TIU;
	local->in.trip = kcg_false;
	local->in.turnOff = kcg_false;

	/* import */
	if (!num_mode) *cP = in->correctionPoint;

	char c = keypoll_get();

	if (c) switch (num_mode) {
	case 0:
		switch (c) {
		case '8':
			local->in.selectDir = directioncontroller_command_cabA_TIU;
			local->in.ctrl = 0;
			break;
		case '2':
			local->in.selectDir = directioncontroller_command_cabB_TIU;
			local->in.ctrl = 0;
			break;
		case '1':
		case ',':
		case 'q':
			local->in.turnOff = kcg_true;
			break;
		case '-':
			if (local->out.send_report && local->in.ctrl > -CtrlRange_train) local->in.ctrl--;
			break;
		case '+':
			if (local->out.send_report && local->in.ctrl <  CtrlRange_train) local->in.ctrl++;
			break;
		case '0':
			if (local->out.send_report) local->in.ctrl = 0;
			break;
		case '7':
			if (local->out.send_report) {
				num_mode++;
				printf( "\r                                                                        \n"
						"Enter 2-digit TrackID for absolute position.\n");
			}
			break;
		case 'h':
		case '?':
			dump_key_help();
			break;
		}
		break;
	case 1:
		num_buf[0] = c;
		num_mode++;
		break;
	case 2:
		num_buf[1] = c;
		num_mode = 0;
		cP->sid = atoi(num_buf);
		if (cP->sid) {
			cP->location = out->report.trainData.location;
			printf( "\r                                                                        \n"
					"Sending %02d as absolute position.\n", cP->sid);
		} else {
			printf( "\r                                                                        \n"
					"Non-valid digits entered. Positioning aborted.\n");
		}
		break;
	}
}

static void stderr_print(const char *lead, const char *str, int nl) {
	if (lead) fputs(lead, stderr);
	if (str) fputs(str,  stderr);
	if (nl) fputc('\n', stderr);
}

static const char *handleIV(InterventionCommand_TIU i, int val, kcg_bool *ivS) {
	static char dots[12] = "...";
	char *c = dots;
	switch (i.eb) {
	case cmd_apply_brake_TIU:   *c = 'E'; break;
	case cmd_release_brake_TIU: *c = '_'; break;
	default:
		*c = (*c=='E' || *c=='e') ? 'e' : '.';
	}
	c++;
	switch (i.sb) {
	case cmd_apply_brake_TIU:   *c = 'S'; break;
	case cmd_release_brake_TIU: *c = '_'; break;
	default:
		*c = (*c=='S' || *c=='s') ? 's' : '.';
	}
	c++;
	switch (i.tco) {
	case cmd_apply_brake_TIU:   *c = 'C'; break;
	case cmd_release_brake_TIU: *c = '_'; break;
	default:
		*c = (*c=='C' || *c=='c') ? 'c' : '.';
	}
	c++;
	*c++ = 0xe2;
	if (val ==0) { *c++ = 0x95; *c++ = 0xb3; }
	if (val > 0) { *c++ = 0x96; *c++ = 0x81 + (val&0xF)/2; }
	if (val < 0) { *c++ = 0x96; *c++ = 0x88 + (val&0xF)/2; }

	*c++ = ivS[0] ? 'e':('-'+ivS[4]);
	*c++ = ivS[1] ? 's':('-'+ivS[4]);
	*c++ = ivS[2] ? 'c':('-'+ivS[4]);
	*c++ = ivS[3] ? 'o':('-'+ivS[4]);

	// 🚈🚃🚋
	return dots;
}

static int parse_train_no(FILE *f) {
	char buffer[2048]; /* expect our train number in the first few bytes */
	char *p = NULL;
	const char *attrib = "host-name";
	size_t end;
	end = fread(buffer, 1, sizeof(buffer)-1, f);
	buffer[end] = '\0';
	p = strstr(buffer, attrib);
	if (p) {
		p += strlen(attrib);
		while (isspace(*p)) p++;
		if (*p++ == '=') { /* skip the '=' */
			while (isspace(*p)) p++;
			if (*p++ == '"') {
				while (*p && *p != '"') {
					if (isdigit(*p)) return *p - '0'; /* take any first digit in the host-name */
					p++;
				}
			}
		}
	}
	return -1;
}

static int parse_args(int argc, char **argv, const char **other) {
	const char *help = "  Usage: /path/config.xml " XSERVICE_OPTARG_USAGE "\n";
	if (other && argc >= 3) *other = argv[2]; /* 1:config 2:shmem-id, otherwise below */
	for (int i=1; i<argc; i++) {
		FILE *f;
		if (((0 == strstr(argv[i], ".xml")) || (0 == strstr(argv[i], ".XML"))) && (f = fopen(argv[i], "r"))) { /* require it to be appended xml */
			self = parse_train_no(f);
			if (self < 0) stderr_print("Cannot read Train-ID from host-name attribute in ", argv[i], 1);
			fclose(f);
			fncfg = argv[i];
			break;
		} else 
			if (other) *other = argv[i];
	}
	
	if (!fncfg || self == -1) {
		fprintf( stderr, help, argv[0]);
		return 1;
	} else {
		fprintf( stdout, "Loading: %s @ train #%d\n", fncfg, self);
		return 0;
	}
}

char *icons[] = { /* active CW tripped stopped */
		"o⎓⎓o",
		"⎏-oA",
		"o⎓⎓o",
		"Bo-⎏",

		"X⎓⎓X", /*           tripped         */
		"⎏-XA", /* active    tripped         */
		"X⎓⎓X", /*        CW tripped         */
		"BX-⎏", /* active CW tripped         */

		"-⎓⎓-", /*                   stopped */
		"⎏--A", /* active            stopped */
		"-⎓⎓-", /*        CW         stopped */
		"B--⎏", /* active CW         stopped */

		"x⎓⎓x", /*           tripped stopped */
		"⎏-xA", /* active    tripped stopped */
		"x⎓⎓x", /*        CW tripped stopped */
		"Bx-⎏", /* active CW tripped stopped */
};

int main(int argc, char **argv) {
	printf(__FILE__ " @ " __TIME__ "\n");

	/* take care of command line arguments */
	const char *xservice_arg = NULL;
	if (parse_args(argc, argv, &xservice_arg) != 0) exit(EXIT_FAILURE);

	/* initialize TRDP session */
	TRDP_ERR_T result = 0;
	static struct shm_data sdd, *sd = &sdd;	
	const char *ctrl_if = "Ctrl.nano.ecn";
	const char *ifaces[] = { ctrl_if, NULL};
	size_t msize = MAX(sizeof(struct shm_struct) + sizeof(struct shm_out) + MAX_XML_FILENAME_LENGTH, sizeof(struct shm_in));

	if ((sd->mem = calloc(1, msize)) && (sd->in  = calloc(1, msize))) sd->out = (char *)(sd->mem+1 /* struct shm_struct */);

		/* initialize Scade model context */
		struct shm_out *extw = (struct shm_out *)sd->out;
		struct scade_local local = { .in = { .self = self }};
		const struct shm_in *extr = (const struct shm_in *)sd->in;
		static const struct shm_in sd_in_zero; /* provide a zero buffer, for when we are unable to lock the shm */
		TrackLocation_TIU correctionPoint; /* I need a copy, as I may modify inputs based on key-strokes */
		/* initialize Scade model context */
		outC_onBoard_train internalCtx;
		onBoard_init_train(&internalCtx);
		extw->out.report.trainData.TrainId = ~0;

	/* initialize TRDP session */
	if (TRDP_NO_ERR == (result = tau_xsession_load(     fncfg, 0, stderr_print, xmap ))
	 && TRDP_NO_ERR == (result = tau_xsession_init(     &trdp, ifaces[0], 10000, -1, NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_subscribe( trdp, 1021, &subDKyID, 1,  NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_subscribe( trdp, 1132, &subIVID, 1,  NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_subscribe( trdp, 1143, &subTLID, 1,  NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_publish(   trdp, 1131, &pubRptID, 1, (UINT8 *)&extw->out.report, sizeof(extw->out.report), NULL ))
	) {

		sd->ts1 = tsc_read_ns();
		sd->tproc = sd->ts1;
		
		dump_key_help();
		keypoll_setmode(1);
		while ( result != EXIT_FAILURE && (local.out.send_report || !local.in.turnOff) ) {
			static uint64_t t=ROUNDS_PER_STAT-1;
			u64 tv1 = tsc_read_ns();

			/* Fetch data from TRDP routines. */
			tau_xsession_getCom( trdp, subDKyID, (UINT8 *)&((struct shm_in *)sd->in)->in.dky, sizeof(DMI_ctrl),  &subDKyLen, &info0);
			if (result != TRDP_NO_ERR && result != TRDP_NODATA_ERR && result != TRDP_TIMEOUT_ERR) break;

		/* TRDP zeros the packet on error, which turns permitted speed to 0 */
			tau_xsession_getCom( trdp, subIVID, (UINT8 *)&((struct shm_in *)sd->in)->in.iv, sizeof(InterventionCommand_TIU),  &subIVLen, &info1);
			if (result != TRDP_NO_ERR && result != TRDP_NODATA_ERR && result != TRDP_TIMEOUT_ERR) break;

			tau_xsession_getCom( trdp, subTLID, (UINT8 *)&((struct shm_in *)sd->in)->in.correctionPoint, sizeof(TrackLocation_TIU), &subTLLen, &info2);
			if (result != TRDP_NO_ERR && result != TRDP_NODATA_ERR && result != TRDP_TIMEOUT_ERR) break;
			extr = (result == TRDP_TIMEOUT_ERR) ? &sd_in_zero : (const struct shm_in *)sd->in;

		/* minimalistic UI, input part */
			handleInput(&local, &extw->out, &extr->in, &correctionPoint);

			local.in.dT = tau_xservice_take_time_for_app( sd );

		/* implementation of root operator of Scade model */
		/* the to-non-const-casts are acceptable, since Scade operators do not change their inputs, the KCG just does not declare it. */
			onBoard_train(
					local.in.self, local.in.turnOff, local.in.trip, local.in.ctrl, local.in.selectDir, local.in.dT, 0, 0,
					(InterventionCommand_TIU *)&extr->in.iv, &correctionPoint, (TrainParam_TIU*)&trainData_Flirt,
					&local.out.modelPWM, &extw->out.report,
					&local.out.send_report, &local.out.packedFlags, &local.out.paramChanged, &local.out.ivState,
					&internalCtx);

		/* this input must be reset after it's been latched */
			correctionPoint.sid = UnknownSid_TIU;

		/* calc CPU time for full cycle, excludes sleeping, but includes some of the sys-call time */
			tau_xservice_time_after_app( sd );

		/* minimalistic UI, output part */
			if (++t==ROUNDS_PER_STAT) {
				printf("%+03d.%s %s%+1.1f>%3d/%3d(%3d) ->| %6d %6d:%+2d:%+3d %5llu us / %5llu us / %4d ",
					correctionPoint.sid,
					icons[local.out.packedFlags],
					handleIV(extr->in.iv, local.in.ctrl, local.out.ivState),
					extw->out.report.trainData.acc/100.0,
					extw->out.report.trainData.velocity/28,
					extr->in.iv.permittedSpeed/28,
					extr->in.iv.interventionSpeed/28,
					extr->in.iv.targetDistance/100,
					extw->out.report.trainData.location/100, extw->out.report.sid, extw->out.report.distance/100,
					sd->mem->app/NS_PER_USEC /*process time Scade-Op*/, sd->mem->round/NS_PER_USEC /*process time complete round*/, local.in.dT
					);
				t=0;
			/* we'd have to sort out, when the last packets came in to do some stats. */
				VOS_TIMEVAL_T tvr;
				u64 tvr1;
				tau_xsession_getRxTime( trdp, subDKyID, &tvr);
				tvr1 = tv1/NS_PER_USEC-(tvr.tv_sec*1000000ULL+tvr.tv_usec);
				if (tvr1 < local.in.dT*1100) printf("<%d>:%4lld ", 1021, tvr1/100ULL);
				tau_xsession_getRxTime( trdp, subIVID, &tvr);
				tvr1 = tv1/NS_PER_USEC-(tvr.tv_sec*1000000ULL+tvr.tv_usec);
				if (tvr1 < local.in.dT*1100) printf("<%d>:%4lld ", 1132, tvr1/100ULL);
				tau_xsession_getRxTime( trdp, subTLID, &tvr);
				tvr1 = tv1/NS_PER_USEC-(tvr.tv_sec*1000000ULL+tvr.tv_usec);
				if (tvr1 < local.in.dT*1100) printf("<%d>:%4lld ", 1143, tvr1/100ULL);
				putchar('\r');
				fflush(stdout);
			}
			
		/* export data of model to TRDP */
			if ((result = tau_xsession_setCom( trdp, pubRptID, (UINT8 *)&extw->out.report, sizeof(extw->out.report)))
				|| (result = tau_xsession_cycle_all())) break;
		}
		keypoll_setmode(0);
		printf("\n");
	} else {
		stderr_print(__FILE__"-init failed: ", tau_getResultString(result), 1);
	}
	tau_xsession_delete( NULL );
	return result;
}
