/*
 ============================================================================
 Name        : keyCtrl.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Version     :
 Copyright   : (c) 2017-2020 Universität Rostock
 Description : TRDP-stub to wire up Scade models
 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <ctype.h>
#include <time.h>
#include <string.h>

#include "keypoll.h"
#include <tau_xmarshall_map.h>
#include <tau_xsession.h>

typedef struct DMI_ctrl {
	char key;
} DMI_ctrl;


static const char* fncfg = NULL;
static int self = -1;

static TAU_XSESSION_T *trdp;

static  INT32 pubDKyID = ~0;

DEFINE_TAU_XMARSHALL_MAP( xmap,
		char, char, int16_t,
		int8_t, int16_t, int32_t, int64_t,
		uint8_t, uint16_t, uint32_t, uint64_t,
		float, double,
		int32_t, int32_t, int32_t
	);

static void dump_key_help() {
	printf( "\r"
			"=================================================================================\n"
			"   8   Switch ON and direction Cab A\n"
			"   2   Switch ON and direction Cab B\n"
			"   +   Increase traction power\n"
			"   0   Set power to zero (mid-position)\n"
			"   -   Decrease power up to braking\n"
			" 1 , q Switch OFF; when it is stopped\n"
			"   x   Quit this program\n"
//			"   7   +two digits: fire reception of that balise, ie. position\n"
			"=================================================================================\n"
			"To get going, you have to switch on (8/2) and press +. "
//			"If you have Trackplan connected, you can set initial position by pressing 717 (position 17 is in the outer ring).\n"
			"The train will throw a fault after 1000m of unsupervised driving. You can turn it off to reset this counter.\n"
			"\n"
	);
}

static void stderr_print(const char *lead, const char *str, int nl) {
	if (lead) fputs(lead, stderr);
	if (str) fputs(str,  stderr);
	if (nl) fputc('\n', stderr);
}

static int parse_train_no(FILE *f) {
	char buffer[2048]; /* expect our train number in the first few bytes */
	char *p = NULL;
	const char *attrib = "host-name";
	size_t end;
	end = fread(buffer, 1, sizeof(buffer)-1, f);
	buffer[end] = '\0';
	p = strstr(buffer, attrib);
	if (p) {
		p += strlen(attrib);
		while (isspace(*p)) p++;
		if (*p++ == '=') { /* skip the '=' */
			while (isspace(*p)) p++;
			if (*p++ == '"') {
				while (*p && *p != '"') {
					if (isdigit(*p)) return *p - '0'; /* take any first digit in the host-name */
					p++;
				}
			}
		}
	}
	return -1;
}

static int parse_args(int argc, char **argv, const char **other) {
	const char *help = "  Usage: /path/config.xml " "\n";
	if (other && argc >= 3) *other = argv[2]; /* 1:config 2:shmem-id, otherwise below */
	for (int i=1; i<argc; i++) {
		FILE *f;
		if (((0 == strstr(argv[i], ".xml")) || (0 == strstr(argv[i], ".XML"))) && (f = fopen(argv[i], "r"))) { /* require it to be appended xml */
			self = parse_train_no(f);
			if (self < 0) stderr_print("Cannot read Train-ID from host-name attribute in ", argv[i], 1);
			fclose(f);
			fncfg = argv[i];
			break;
		} else 
			if (other) *other = argv[i];
	}
	
	if (!fncfg || self == -1) {
		fprintf( stderr, help, argv[0]);
		return 1;
	} else {
		fprintf( stdout, "Loading: %s @ train #%d\n", fncfg, self);
		return 0;
	}
}

int main(int argc, char **argv) {
	printf(__FILE__ " @ " __TIME__ "\n");

	/* take care of command line arguments */
	const char *xservice_arg = NULL;
	if (parse_args(argc, argv, &xservice_arg) != 0) exit(EXIT_FAILURE);

	/* initialize TRDP session */
	TRDP_ERR_T result = 0;
	const char *ctrl_if = "keys.nano.ecn";
	const char *ifaces[] = { ctrl_if, NULL};
	DMI_ctrl dmi_ctrl;

	/* initialize TRDP session */
	if (TRDP_NO_ERR == (result = tau_xsession_load(     fncfg, 0, stderr_print, xmap ))
	 && TRDP_NO_ERR == (result = tau_xsession_init(     &trdp, ifaces[0], 5000, -1, NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_publish(   trdp, 1021, &pubDKyID, 1, (UINT8 *)&dmi_ctrl, sizeof(dmi_ctrl), NULL ))
	) {

		dump_key_help();
		keypoll_setmode(1);
		while ( result != EXIT_FAILURE && dmi_ctrl.key != 'x' ) {
			dmi_ctrl.key = keypoll_get();
			if ((result = tau_xsession_setCom( trdp, pubDKyID, (UINT8 *)&dmi_ctrl, sizeof(dmi_ctrl)))
				|| (result = tau_xsession_cycle_all())) break;
		}
		keypoll_setmode(0);
		printf("\n");
	} else {
		stderr_print(__FILE__"-init failed: ", tau_getResultString(result), 1);
	}
	tau_xsession_delete( NULL );
	return result;
}
