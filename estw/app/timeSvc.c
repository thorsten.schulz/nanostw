/*
 ============================================================================
 Name        : timeSvc.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Copyright   : (c) 2020 Universität Rostock
 Description : simple time-service to teleport Linux's system clock to a
               bare-metal jailhouse cell for synchronization.

 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>
#include <sys/wait.h>

#include <errno.h>
#include <error.h>
/* inhibit inclusion of outdated libc struct sched_param definition.
 * TODO This should be fixed, once libc gets this right. See also inline definition of sched_setattr. */
#define _BITS_TYPES_STRUCT_SCHED_PARAM 1
#define __need_schedparam    1
#define __defined_schedparam 1
#include <sys/syscall.h>
#include <linux/sched/types.h>
#include <linux/sched.h>
#include <pthread.h>
#undef sched_priority /* defined by uclib and it is misleading */



#define CONST_PAGE_SIZE    0x1000
#define SHM_DEFAULT_NUMBER      0
#define ROUNDS_PER_STAT 16
#define MAXSIZE_FILENAME 32         /* size of static buffers for filename construction */

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#endif

#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif


#include "ivshmem.h"
#include <unistd.h>
static struct ivshmem_dev dev; /* make this global for cleanup */

static void cleanup_state( void ) {
	printf(__FILE__ " finalizing.\n");
	ivshmem_finalize( &dev );
}

/* Once libc defines these two functions, they can be removed here */
int sched_setattr (pid_t pid, const struct sched_attr  *attr, unsigned int flags) {
    return syscall(SYS_sched_setattr, pid, attr, flags);
}

int sched_getattr (pid_t pid, struct sched_attr * const attr, unsigned int size, unsigned int flags) {
    return syscall(SYS_sched_getattr, pid, attr, size, flags);
}

static int get_freq_offset_from_chrony( long int *drift) {
	/* in the following lines, we will fork a process with the chronyc command, querying the chrony service for its
	 * current parameters and extract the frequency deviation. */
	int err = 1;
	/* boiler plate code borrowed from internet */
	int filedes[2];
	char *argv[] = { "chronyc", "-c", "tracking", NULL };
	int chrony_exit;
	if (pipe(filedes) == -1) {
		perror("pipe");
		exit(1);
	}

	pid_t pid = fork();
	if (pid == -1) {
		perror("fork");
		exit(1);
	} else if (pid == 0) {
		while ((dup2(filedes[1], STDOUT_FILENO) == -1) && (errno == EINTR)) {}
		close(filedes[1]);
		close(filedes[0]);
		execve("/usr/bin/chronyc", argv, NULL);
		perror("execve");
		_exit(1);
	}
	close(filedes[1]);

	char buffer[4096];
	while (1) {
		/* I am assuming I get the whole line at once */
		ssize_t count = read(filedes[0], buffer, sizeof(buffer)-1);
		if (count == -1) {
			if (errno == EINTR) {
				continue;
			} else {
				perror("read");
				exit(1);
			}
		} else if (count == 0) {
			break;
		} else {
			char *pstart = buffer;
			buffer[count] = '\0';
			if (!strstr(pstart, "Normal")) {
				fprintf(stderr, "chrony did not signal sync-status. (%s)\n", buffer);
			} else {
				for (int i=0; i<7 && pstart && *pstart; i++) pstart = strchr(pstart+1, ',');
				if (*pstart == ',' && *++pstart) {
					char *pend;
					double df = strtod(pstart, &pend);
					if (*pend == ',') {
						*drift = df * 1000L;
						err = 0;
					}
					break;
				}
				fprintf(stderr, "chronyc returned something I don't understand: \"%s\"\n", buffer);
			}
			break;
		}
	}
	close(filedes[0]);
	wait(&chrony_exit);
	if (WIFEXITED(chrony_exit)) {
		if (WEXITSTATUS(chrony_exit))
			fprintf(stderr, "chronyc exited with error %d.\n", WEXITSTATUS(chrony_exit));
	} else {
		fprintf(stderr, "chronyc exited abnormally.\n");
	}
	return err;
}

/* provide memory barriers against compiler optimizations */
static inline void cpu_relax(void)
{
	asm volatile("rep; nop" : : : "memory");
}

static inline void memory_barrier(void)
{
	asm volatile("mfence" : : : "memory");
}

void switch_rt_prio(int prio) {
	struct sched_attr rt_attribs;
	memset(&rt_attribs, 0, sizeof(rt_attribs));
	rt_attribs.size             = sizeof(struct sched_attr); /* Size of this structure */
	rt_attribs.sched_policy     = SCHED_FIFO;
	rt_attribs.sched_flags      = SCHED_FLAG_RESET_ON_FORK;
	/* Remaining fields are for SCHED_DEADLINE only */
	rt_attribs.sched_priority   = prio;

	if (0 != sched_setattr(0, &rt_attribs, 0))
		error(2, errno, "sched_setattr failed");


}

int main(int argc, char **argv) {
	fprintf(stderr, "[stderr] %s\n", BUILD_DATE);
	fprintf(stdout, "[stdout] %s\n", BUILD_DATE);

	char *arg = argc == 2 ? argv[1] : NULL;

	int devid = SHM_DEFAULT_NUMBER;
	if (arg && 0 > ivshmem_parse_ids( arg, &devid, 1, '.')) {
		fprintf(stderr, "Could not parse arg \"%s\" for %s.\n", argv[1], argv[0]);
	} else {
		switch_rt_prio(99);
		/* extract the initial f-drift from chrony. */
		/* wait for chrony to stabilize before startig this program */
		long int drift;
		for (; get_freq_offset_from_chrony( &drift); sleep(1)) ;
		fprintf(stdout, "Chrony announced a drift of %ld ppm.\n", drift);
		/* after this, I need to clean up */
		ivshmem_open( &dev, devid, NULL); /* initialize ivshmem /dev/uio0, or whatever comes in devid */
		atexit( cleanup_state );          /* register the helper to cleanup the ivshmem at application exit */
		if ( dev.rw_size && dev.rw ) {    /* if the ivshmem is available */
			volatile u64 *shm = (u64 *)dev.out;                 /* simplify access */
			volatile int *lock = (int *)dev.rw;
			struct timespec tv, tv2;
			long int lag_min = 1000000000L, lag_max=0;
			long int dlv, dlv_min = 1000000000L, dlv_max = 0;
			shm[0] = 0;
			shm[1] = 0;
			shm[2] = drift;
			shm[3] = 15000ULL; /* start with an experienced value */
			memory_barrier();
			printf("Blocking for nollie's first acknowledge.\n");

			clock_gettime(CLOCK_REALTIME, &tv);       /* get time from OS */
			do {                                      /* and enter application loop */
				int err;
				tv.tv_sec++;                          /* round up to next second */
				tv.tv_nsec = 0;

				err = clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &tv, NULL); /* schedule to wake this thread at that next even second */
				if (err == EINTR) continue;           /* try again on stray signals */
				if (err) break;                       /* exit on any other error */
				clock_gettime(CLOCK_REALTIME, &tv);   /* despite the actual cost of this call, get the real time and
					see how long it took to wakeup the thread after nanosleep. */
				shm[0] = tv.tv_sec;                   /* copy this current second to the ivshmem, the RW second at offset 0 */
				shm[1] = tv.tv_nsec;
				ivshmem_set_blocking( &dev, tv.tv_sec, lock);  /* send out the state */
				clock_gettime(CLOCK_REALTIME, &tv2);  /* take the current time again to check for ISR-delivery time */
				dlv = tv2.tv_nsec - tv.tv_nsec;       /* take the diff and do stats */
				if (dlv < dlv_min) dlv_min = dlv;
				if (dlv > dlv_max) dlv_max = dlv;
				shm[3] = dlv_min;                     /* also make the min delivery time available, however, too late for initial values */
				if (tv.tv_nsec < lag_min) lag_min = tv.tv_nsec;
				if (tv.tv_nsec > lag_max) lag_max = tv.tv_nsec;
				if (0 == (tv.tv_sec % 600)) {         /* every 600s, reassure the drift */
					if ( 0 == get_freq_offset_from_chrony(&drift) ) {
						shm[2] = drift;
					}
				}
				printf("%ld:%ld) chrony-drift: %ld  Wakeup-lag: %ld [%ld..%ld] Delivery: %ld [%ld..%ld]\n",
					   (tv.tv_sec/60)%60, tv.tv_sec%60, drift, tv.tv_nsec, lag_min, lag_max, dlv, dlv_min, dlv_max);
				/* note: due to leap seconds, the seconds calculated this way are not the same as calendar time */
			} while (1);                              /* can only be interrupted  exited by CTRL-C signal */
		} else
			fprintf(stderr, "Could not find RW section in uio%d.\n", devid);
	}
	return 0;
}
