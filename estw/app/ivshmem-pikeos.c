/*
 * IVSHMEM emulator extensions for use in PikeOS-Native environments
 *
 * Uses my interface from the Jailhouse's ivshmem model.
 *
 * Copyright (c) Universität Rostock, 2020
 *
 * Authors:
 *  Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* PikeOS Personality Extensions API */
#include <p4ext/p4ext_vmem.h>
#include <p4ext/p4ext_assert.h>

#include "ivshmem.h"
#include <stdlib.h>
/*       ^^^^^^^^^^ would normally also define EXIT_FAILURE, but it lacks. */
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#define FIFO_PREFIX   ""
#define SHM_PREFIX    "shm:/"  /* mmapped shm / file */

/* TODO, should have two SHMs, one per direction */
#define SHM_OPEN_FLAGS (VM_O_RD | VM_O_WR | VM_O_MAP)

static size_t _msize;

void ivshmem_reserve(size_t msize) {
	_msize = msize;
}

static void ivshmem_init( struct ivshmem_dev *dev, int bdf, u8 *vmem );

void ivshmem_open( struct ivshmem_dev *dev, int iv_no, struct ivshmem_cb *cb ) {
	ivshmem_init( dev, iv_no, cb?cb->vmem:NULL );

	ivshmem_sti ( dev );
}

static void ivshmem_init( struct ivshmem_dev *dev, int bdf, u8 *vmem ) {
	char fnw[64], fnr[64], fnshm[64];
	vm_port_desc_t fd;
	vm_file_desc_t fdm;
	int svc = (_msize == 0);
//	struct stat statbuf;
	P4_e_t rc;

	snprintf(fnw,   sizeof(fnw),   "%s%s%d", FIFO_PREFIX, SVCW_FILENAME, bdf);
	snprintf(fnr,   sizeof(fnr),   "%s%s%d", FIFO_PREFIX, SVCR_FILENAME, bdf);
	snprintf(fnshm, sizeof(fnshm), "%s%s%d",  SHM_PREFIX,  SHM_FILENAME, bdf);

	memset(dev, 0, sizeof(*dev));
	dev->id    = svc?0:1;
	dev->peers = 2;
	dev->state_size = dev->peers * sizeof(*dev->state);  /* real ivshmem has this capped to PAGESIZE */
	dev->regs  = calloc( 1, sizeof( struct ivshm_regs ));
	dev->state = calloc( 1, dev->state_size);
	dev->lstate= calloc( 1, dev->state_size);

	if (P4_E_OK != (rc = vm_open(fnshm, SHM_OPEN_FLAGS, &fdm)))
		error(IVSHMEM_ERROR_EXIT_VALUE, rc, "Opening \"%s\" failed.\n", fnshm);

	/* Allocate virtual memory to map the shared memory
	* Enforce the PAGE alignment
	*/
	if (!svc) {
		/* this part is not aware of the app-specific struct shm*, so hide
		 * p4ext_assert( (sizeof( struct shm_struct )+sizeof( struct shm_data ) + 256) <= 2*_msize );
		 */
		dev->out_size = _msize;
	}

	if (P4_E_OK != (rc = vm_qport_open(fnr, svc?VM_PORT_DESTINATION:VM_PORT_SOURCE, &fd)))
		error(IVSHMEM_ERROR_EXIT_VALUE, rc, "vm_qport_open(%s)", fnr);

	if (svc) dev->device = fd; else dev->send = fd;

	if (P4_E_OK != (rc = vm_qport_open(fnw, svc?VM_PORT_SOURCE:VM_PORT_DESTINATION, &fd)))
		error(IVSHMEM_ERROR_EXIT_VALUE, rc, "vm_qport_open(%s)", fnw);

	if (svc) dev->send = fd; else dev->device = fd;

	if (svc) {
		/* find out shm size and set dev->out_size */
		/* dev->out_size = statbuf.st_size/dev->peers; */
	}

	/* The memory is already reserved through VMIT
	mem = p4ext_vmem_alloc_aligned(2*_msize, (P4_phys_addr_t)SHM_ADDR);
	if (!sync) {
		vm_cprintf("Partition %d: Error in p4ext_vmem_alloc_aligned()\n", p4_my_respart());
		return 2;
	}
	*/

	/* Map the shared memory segment */
	if (dev->out_size) {
		if (P4_E_OK != (rc = vm_map(&fdm, 0, 2*_msize, VM_MEM_ACCESS_RD_WR, 0, (P4_address_t)vmem)))
			error(EXIT_FAILURE, rc, "vm_map()");

		dev->in = vmem;
		dev->out= vmem + dev->id * dev->out_size;
		memset(dev->out, 0, dev->out_size);
	}

	ivshmem_tell( dev );
}

void ivshmem_sti( struct ivshmem_dev *dev __unused) {
	/* nop */
}

toSt ivshmem_set_state( struct ivshmem_dev *dev, u32 state ) {
	P4_e_t rc;
	if (0 != (rc = vm_qport_write(&dev->send, &state, sizeof(state), P4_TIMEOUT_INFINITE)))
		error(IVSHMEM_ERROR_EXIT_VALUE, rc, "vm_qport_write(toSvc)\n");
	return STATE_CHANGED_OK;
}

/* send out an INT to target.
   However, the Linux UIO driver cannot distinguish between INT_no when notifying. */

void ivshmem_signal( struct ivshmem_dev *dev __unused, u32 intno __unused, u32 target __unused) {
	/* not implemented here, feature not used in actual apps */
	/* see ivshmem-uio.c for more */
}

int ivshmem_poll( struct ivshmem_dev *dev, u64 timeout ) {
	P4_e_t rc;

	uint32_t state;
	size_t red;
	int src = dev->id==1 ? 0 : 1;
	rc = vm_qport_read(&dev->device, &state, sizeof(state), 
						timeout==IVSHMEM_BLOCK?P4_TIMEOUT_INFINITE:P4_NSEC(timeout), &red);
	if ( rc == P4_E_TIMEOUT ) return 0;
	if ( rc != P4_E_OK || red != sizeof(state)) 
		error(IVSHMEM_ERROR_EXIT_VALUE, rc, "vm_qport_read(device)");

	((volatile u32 *)dev->state)[src] = state;
	dev->int_count[0] += 1;
	return 1;
}

u32 ivshmem_read_device( struct ivshmem_dev *dev ) {
	return 1;
}

void ivshmem_finalize( struct ivshmem_dev *dev ) {
	ivshmem_set_state( dev, 0 );
	vm_qport_close( &dev->device );
	vm_qport_close( &dev->send );
	free((void *)dev->regs);
	free((void *)dev->state);
	free((void *)dev->lstate);
}

unsigned long tsc_read_ns(void) {
	return p4_get_time();
}

unsigned long process_clock_ns(void) {
	return p4_get_time();
}

/* could do better, if the rtc.dev is present */
unsigned long rtc_read_ns(void) {
	return p4_get_time();
}

