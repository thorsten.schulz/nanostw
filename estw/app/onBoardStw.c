/*
 ============================================================================
 Name        : onBoardStw.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Version     :
 Copyright   : (c) 2017-2020 Universität Rostock
 Description : TRDP-stub to wire up Scade models
 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <tau_xmarshall.h>
#include <tau_xsession.h>
#include "trdpSvc.h"

#include "../Stw.bm/onBoardStw_configured_nanoSim.h"

struct scade_in {
	allPR_nanoSim XPR;
	TrackPlan_nanoSim XTracks;
	TrainReport_nanoStw_trdp XReport;
};

struct scade_out{
	TrackLocation_nanoStw correctionPoint;
	kcg_bool haveData;
	TrackDMI_nanoSim XTrainRelLimitLoc;
	ProtectionLocations_nanoStw_trdp PRtoOther;
	InterventionCommand_SpeedSupervision_UnitTest_Pkg InterventionCmd;
	SDM_DMI_wrapper_T_SDM_Types_Pkg pkgDMI;
};

struct scade_ext {
	struct scade_in  in;
	struct scade_out out;
};

static TAU_XSESSION_T *s_int, *s_ext;
static TRDP_PD_INFO_T info1, info2, info3;
static  INT32 pubLocID = ~0;
static  INT32 pubPRID  = ~0;
static  INT32 pubTLID  = ~0;
static  INT32 subTrkID = ~0;
static UINT32 subTrkLen = 0;
static  INT32 subPRID  = ~0;
static UINT32 subPRLen =  0;
static  INT32 pubDmiID = ~0;
static  INT32 pubIVID  = ~0;
static  INT32 subRptID = ~0;
static UINT32 subRptLen=  0;

static
struct TimeTrials {
	TIMEDATE32 timeA;
	TIMEDATE48 timeB[2];
	TIMEDATE64 timeC[3];
} tt;
//static UINT32 pubTTID  = ~0;

DEFINE_TAU_XMARSHALL_MAP( xmap,
		kcg_bool, kcg_char, kcg_int,
		kcg_int, kcg_int, kcg_int, kcg_int,
		kcg_int, kcg_int, kcg_int, kcg_int,
		kcg_real, kcg_real,
		kcg_int, kcg_int, kcg_int);

void stderr_print(const char *lead, const char *str, int nl) {
	if (lead) fputs(lead, stderr);
	if (str) fputs(str,  stderr);
	if (nl) fputc('\n', stderr);
}

void handleIV(InterventionCommand_SpeedSupervision_UnitTest_Pkg i, speedSupervisionForDMI_T_DMI_Types_Pkg s, char *c) {
	switch (i.eb) {
	case 1: *c = 'E'; break;
	case 2: *c = '_'; break;
	default:
		*c = (*c=='E' || *c=='e') ? 'e' : '.';
	}
	c++;
	switch (i.sb) {
	case 1: *c = 'S'; break;
	case 2: *c = '_'; break;
	default:
		*c = (*c=='S' || *c=='s') ? 's' : '.';
	}
	c++;
	switch (i.tco) {
	case 1: *c = 'C'; break;
	case 2: *c = '_'; break;
	default:
		*c = (*c=='C' || *c=='c') ? 'c' : '.';
	}
	c++;
	c++;
	switch (s.sup_status) {
	case CSM_DMI_Types_Pkg: *c='C'; break;
	case PIM_DMI_Types_Pkg: *c='P'; break;
	case TSM_DMI_Types_Pkg: *c='T'; break;
	case RSM_DMI_Types_Pkg: *c='R'; break;
	default: *c='?';
	}
	c++;
	c++;
	switch (s.supervisionDisplay) {
	case supDis_normal_DMI_Types_Pkg: *c='n'; break;
	case supDis_indication_DMI_Types_Pkg: *c='i'; break;
	case supDis_overspeed_DMI_Types_Pkg: *c='o'; break;
	case supDis_warning_DMI_Types_Pkg: *c='w'; break;
	case supDis_intervention_DMI_Types_Pkg: *c='v'; break;
	default: *c='?'; break;
	}
}


int main(int argc, char **argv) {

	stderr_print(__FILE__, __TIME__, 1);
	if (argc != 2) {
		stderr_print("onBoardStw-init: ", "Please pass trdp-XML configurations file as first and only parameter.", 1);
	} else {
		stderr_print("Loading: ", argv[1], 1);
	}

/* initialize Scade model context */
	struct scade_ext _ext;
	memset(&_ext, 0, sizeof(_ext));
	const char *ext_if = "Stw.nano.uplink";
	const char *int_if = "Stw.nano.ecn";
	const char *ifaces[] = { ext_if, int_if, NULL};
	struct scade_ext *ext = &_ext;
	outC_onBoardStw_configured_nanoSim internalCtx;
	memset(&internalCtx, 0, sizeof(internalCtx));
	onBoardStw_configured_init_nanoSim(&internalCtx);
	ext->out.XTrainRelLimitLoc.TrainID = ~0;
	ext->out.PRtoOther.tid = ~0;

	TRDP_ERR_T result;

	/* after this, I need to clean up */
	if (TRDP_NO_ERR == (result = tau_xsession_load(          argv[1], 0, stderr_print, xmap ))
	 && TRDP_NO_ERR == (result = tau_xsession_init(     &s_ext, ifaces[0], 4000, -1, NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_init(     &s_int, ifaces[1], 4000, -1, NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_subscribe( s_ext, 1111, &subTrkID, 1,  NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_subscribe( s_ext, 1141, &subPRID,  1,  NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_subscribe( s_int, 1131, &subRptID, 1,  NULL ))
	 && TRDP_NO_ERR == (result = tau_xsession_publish(   s_ext, 1112, &pubLocID, 1, (UINT8 *)&ext->out.XTrainRelLimitLoc, sizeof(ext->out.XTrainRelLimitLoc), NULL))
	 && TRDP_NO_ERR == (result = tau_xsession_publish(   s_ext, 1142, &pubPRID,  1, (UINT8 *)&ext->out.PRtoOther,         sizeof(ext->out.PRtoOther),         NULL))
	 && TRDP_NO_ERR == (result = tau_xsession_publish(   s_int, 1143, &pubTLID,  1, (UINT8 *)&ext->out.correctionPoint,   sizeof(ext->out.correctionPoint),   NULL))
	 && TRDP_NO_ERR == (result = tau_xsession_publish(   s_int, 1022, &pubDmiID, 1, (UINT8 *)&ext->out.pkgDMI,            sizeof(ext->out.pkgDMI),            NULL))
	 && TRDP_NO_ERR == (result = tau_xsession_publish(   s_int, 1132, &pubIVID,  1, (UINT8 *)&ext->out.InterventionCmd,   sizeof(ext->out.InterventionCmd),   NULL))
//	 && TRDP_NO_ERR == (result = tau_xsession_publish(   s_ext, 1301, &pubTTID,  1, (UINT8 *)&tt,                    sizeof(tt),                    NULL))
	) {

		u64 tv0, tv1;
		tv1 = tsc_read_ns();
		tv0 = tv1 - IVSHMEM_MSEC(1);
		long long unsigned t1, t2, t3 = process_clock_ns();
		char iv[] = "... . .";

		while (result == TRDP_NO_ERR) {
			static uint64_t t=ROUNDS_PER_STAT-1;
			tv0 = tv1;
			tv1 = tsc_read_ns();

			result = tau_xsession_getCom( s_ext, subTrkID, (UINT8 *)&ext->in.XTracks, sizeof(ext->in.XTracks), &subTrkLen, &info1);
			if (result != TRDP_NO_ERR && result != TRDP_NODATA_ERR && result != TRDP_TIMEOUT_ERR) break;
			result = tau_xsession_getCom( s_ext, subPRID,  (UINT8 *)&ext->in.XPR,     sizeof(ext->in.XPR),     &subPRLen,  &info2);
			if (result != TRDP_NO_ERR && result != TRDP_NODATA_ERR && result != TRDP_TIMEOUT_ERR) break;
			result = tau_xsession_getCom( s_int, subRptID, (UINT8 *)&ext->in.XReport, sizeof(ext->in.XReport), &subRptLen, &info3);
			if (result != TRDP_NO_ERR && result != TRDP_NODATA_ERR && result != TRDP_TIMEOUT_ERR) break;


			t1 = process_clock_ns();
			int dT = (tv1-tv0+NS_PER_MSEC/2-1)/NS_PER_MSEC;

		/* implementation of root operator of Scade model */
			onBoardStw_configured_nanoSim(
					&ext->in.XPR, &ext->in.XTracks, &ext->in.XReport,
					&ext->out.correctionPoint, &ext->out.haveData, &ext->out.XTrainRelLimitLoc, &ext->out.PRtoOther, &ext->out.InterventionCmd, &ext->out.pkgDMI,
					&internalCtx);

		/* pure console state stuff */
			handleIV(ext->out.InterventionCmd, ext->out.pkgDMI.sdm, iv);

		/* calc CPU time for full cycle, excludes sleeping, but includes some of the sys-call time */
			t2 = t3;
			t3 = process_clock_ns();
		/* console line */
			if (++t==ROUNDS_PER_STAT) {
				printf("%c %s %3d/%3d(%3d) ->| %6d %6d:%+2d:%+3d %5llu us / %5llu us / %4d, %s ",
					ext->out.haveData? 'T':'?',
					iv,
					ext->out.PRtoOther.velocity/28,
					ext->out.InterventionCmd.permittedSpeed/28,
					ext->out.InterventionCmd.interventionSpeed/28,
					ext->out.InterventionCmd.targetDistance/100,
					ext->out.PRtoOther.antenna.location/100, ext->out.PRtoOther.antenna.sid, ext->out.PRtoOther.antenna.distance/100,
					(t3-t1)/NS_PER_USEC, (t3-t2)/NS_PER_USEC, dT,
					ext->out.PRtoOther.trainType8
				  );
				t=0;
				VOS_TIMEVAL_T tvr;
				u64 tvr1;
				tau_xsession_getRxTime( s_ext, subTrkID, &tvr);
				tvr1 = tv1/NS_PER_USEC-(tvr.tv_sec*1000000ULL+tvr.tv_usec);
				if (tvr1 < dT*1100) printf("<%d>:%4lld ", 1111, tvr1/100ULL);
				tau_xsession_getRxTime( s_ext, subPRID, &tvr);
				tvr1 = tv1/NS_PER_USEC-(tvr.tv_sec*1000000ULL+tvr.tv_usec);
				if (tvr1 < dT*1100) printf("<%d>:%4lld ", 1141, tvr1/100ULL);
				tau_xsession_getRxTime( s_int, subRptID, &tvr);
				tvr1 = tv1/NS_PER_USEC-(tvr.tv_sec*1000000ULL+tvr.tv_usec);
				if (tvr1 < dT*1100) printf("<%d>:%4lld ", 1131, tvr1/100ULL);
				putchar('\r');
				fflush(stdout);
			}

		/* some content for testing time-structs stuff */
			time_t ttemp;
			time(&ttemp);
			tt.timeA = ttemp;
			struct timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			struct tm tm = { .tm_sec = 56, .tm_min = 34, .tm_hour = 12, .tm_mday = 7, .tm_mon = 7 /*zero based*/, .tm_year = 90, .tm_isdst = -1 };
			tt.timeB[0].sec   = ts.tv_sec;
			tt.timeB[0].ticks = (ts.tv_nsec*256ULL)/(1000000000ULL/256ULL);
			tt.timeB[1].sec   = mktime(&tm);
			tt.timeB[1].ticks = 0xFFFF;
			tt.timeC[0].tv_sec  = ts.tv_sec;
			tt.timeC[0].tv_usec = ts.tv_nsec/1000;
			tt.timeC[1].tv_sec  = mktime(&tm);
			tt.timeC[1].tv_usec = 999999;

		/* export data of model to TRDP, not yet sending it off */
			if (
				TRDP_NO_ERR != (result = tau_xsession_setCom( s_ext, pubLocID, (UINT8 *)&ext->out.XTrainRelLimitLoc, sizeof(ext->out.XTrainRelLimitLoc))) ||
				TRDP_NO_ERR != (result = tau_xsession_setCom( s_ext, pubPRID,  (UINT8 *)&ext->out.PRtoOther,         sizeof(ext->out.PRtoOther))) ||
				TRDP_NO_ERR != (result = tau_xsession_setCom( s_int, pubTLID,  (UINT8 *)&ext->out.correctionPoint,   sizeof(ext->out.correctionPoint))) ||
			/* valgrind noticed that pkgDMI's data may not be correctly initialized */
				TRDP_NO_ERR != (result = tau_xsession_setCom( s_int, pubDmiID, (UINT8 *)&ext->out.pkgDMI,            sizeof(ext->out.pkgDMI))) ||
				TRDP_NO_ERR != (result = tau_xsession_setCom( s_int, pubIVID,  (UINT8 *)&ext->out.InterventionCmd,   sizeof(ext->out.InterventionCmd))) ||
//				TRDP_NO_ERR != (result = tau_xsession_setCom( s_ext, pubTTID,  (UINT8 *)&tt,                         sizeof(tt))) ||
				TRDP_NO_ERR != (result = tau_xsession_request( s_ext, subTrkID)) ||
				TRDP_NO_ERR != (result = tau_xsession_request( s_ext, subPRID)) ||

				TRDP_NO_ERR != (result = tau_xsession_cycle_all())
				) break;
		}
		printf("\n");
	} else
		stderr_print("onBoardStw initialization failed: ", tau_getResultString(result), 1);

	tau_xsession_delete( NULL );

	return result;
}
