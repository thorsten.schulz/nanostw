/*
 ============================================================================
 Name        : nollie.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Copyright   : (c) 2020 Universität Rostock
 Description : "simple" jailhouse inmate to toggle an LED on a synchronized
               time-base.

 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */

/* for other Apollo-Lake systems using other pads, you can find out the address by running a kernel with PINCTRL+GPIO
 * driver support; going to /sys/class/gpio, echo $your_pin_no > /sys/class/gpio/export, run jailhouse enable $your_cfg,
 * echo 1 > /sys/class/gpio/gpio$(your_pin_no)/value and find the r/w address of the PadCfg0 register in the jailhouse
 * console dump due the illegal mmio access.
 */

#define COMPULAB_FITLET_LED_G1 0xd0c50508
#define COMPULAB_FITLET_LED_Y1 0xd0c50510
#define COMPULAB_FITLET_LED_G2 0xd0c50518
#define COMPULAB_FITLET_LED_Y2 0xd0c50520

#define DEFAULT_CHRONY_AVG_RNDS 4
#define DEFAULT_GPTP_AVG_RNDS 32

#include "ivshmem.h"

/* Minimal Apollo Lake Pad register accessor */
typedef struct aplpadcfg {
	u32 pad;
	u32 reserved;
} apl_padcfg;

static apl_padcfg *led_regs;
static struct ivshmem_dev dev;
static int avg_cycles = 1;

/* these need cli-protection */
// tsc_read_ns() is not tread/IRQ-safe
static s64 tsc_ns_offset, chrony_offset;
static s64 iv_comp_ns, iv_comp_fill, iv_comp_reject, iv_comp_ns_max;
static unsigned long tsc_frq_initial, tsc_frq, tsc_frq_fill;
/* for CHRONY_VARIANT */
static s64 jitter_min, jitter_max, jitter_last, jitter_drift;
static s64 drift_last;

static enum {
	CHRONY_VARIANT,
	GPTP_VARIANT,
} app_mode;



static inline void memory_barrier(void)
{
	asm volatile("mfence" : : : "memory");
}

#include "gptp_ipcdef.h"

/* Interrupt Service Routine
 * Will be called for all IRQs with its number appended. May interrupt the main routine at any possible time.
 */
static void jailhouse_irq_handler(unsigned int irq) {
/* for CHRONY_VARIANT */
	static u64 rtc_s_last, tsc_ns_initial, rtc_ns_initial, rtc_s_init; /* a few local variables keeping its state between the calls */
/* for GPTP_VARIANT */
	static u64 xtsc_last, rtc_ns_last;

	volatile int *lock = (int *)dev.rw;
	*lock = 0;                                 /* acknowledge, that we are in */
	//u64 tsc = rdtsc();
	memory_barrier();                          /* and tell the compiler/CPU to really do it, hopefully */
	s64 stamp_ns = tsc_read_ns();              /* take the entry timestamp. */
	if ( ivshmem_irq_handler( &dev, irq)       /* check if it is the ivshmem IRQ and let it handle its internal states */
		&& dev.state[0] )                      /* and make sure the peer is actually active */
	switch (app_mode) {
		case CHRONY_VARIANT: {
			const volatile u64 *shm = (const u64 *)dev.in;
			s64 _iv_comp_ns;
			u64 rtc_s  = shm[0];               /* The ivshmem RW segment contains the latest Unix-UTC timestamp at offset 0 */
			s64 rtc_ns = rtc_s * NS_PER_SEC    /* the timestamp is in seconds, so convert to nano seconds for further use */
					+ shm[1];
			chrony_offset = shm[4];
			if (drift_last != (s64)shm[2]) {       /* if the f-drift was updated */
				drift_last = (s64)shm[2];
				tsc_frq = tsc_tune( drift_last );  /* patched into the inmate-library. The actual update will be currently
					postponed to the next internal overflow, to avoid race-conditions. This, however, causes other,
					slightly smaller problems, due to unaligned time-quanitzations. */
			}
			if (rtc_s != rtc_s_last) {             /* only act, if the timestamp was updated */
				rtc_s_last = rtc_s;
				if (!rtc_ns_initial) {                /* on the first run initialize the base variables */
					rtc_s_init  = rtc_s;
					rtc_ns_initial = rtc_ns;
					tsc_ns_initial = stamp_ns;
				} else {                           /* on later runs, calculate the difference between the external RTC and the internally expired time. */
					jitter_last = (s64)(rtc_ns - rtc_ns_initial) - (s64)(stamp_ns - tsc_ns_initial);
					jitter_drift = jitter_last/(s64)(rtc_s - rtc_s_init);
					if (jitter_last < jitter_min) jitter_min = jitter_last;
					if (jitter_last > jitter_max) jitter_max = jitter_last;
				}
			}
			while (!*lock) memory_barrier(); /* chrony must flip the lock back to 1, once it has written to shm[3] */
			_iv_comp_ns = shm[3];
			if (_iv_comp_ns < 100000 /*ns*/) { /* reject outliers */
				if (iv_comp_fill < avg_cycles) {
					iv_comp_fill++;
				} else {
					iv_comp_ns -= iv_comp_ns / avg_cycles;
				}
				iv_comp_ns += _iv_comp_ns;
			} else { /* but add stats of outliers */
				iv_comp_reject++;
				if (iv_comp_ns_max < _iv_comp_ns) iv_comp_ns_max = _iv_comp_ns;
			}
			tsc_ns_offset = rtc_ns + (iv_comp_fill ? iv_comp_ns/iv_comp_fill : 0) - stamp_ns;
		}
		break;
		case GPTP_VARIANT: {
			const volatile gPtpTimeData *ptp = (const gPtpTimeData *)dev.in;
			u64 rtc_ns;
	//		u64 d_xtsc, d_ns;

			rtc_ns = ptp->local_time + ptp->ml_phoffset; /* local is the time on the network card / the _local_ PTP clock */
			if (xtsc_last && rtc_ns_last) {
	//			d_xtsc = ptp->x_tsc - xtsc_last;
	//			d_ns = rtc_ns - rtc_ns_last;
	//			tsc_frq = (d_xtsc*NS_PER_MSEC) / (d_ns/1000ULL);  /* this only works for updates <=18000s apart */
				if (tsc_frq_fill < avg_cycles) {
					tsc_frq_fill++;
				} else {
					tsc_frq -= tsc_frq / avg_cycles;
				}
				tsc_frq += (double)tsc_frq_initial * (double)ptp->ls_freqoffset;
	//			tsc_step_freq( tsc_frq/tsc_frq_fill );
				if (iv_comp_fill < avg_cycles) {
					iv_comp_fill++;
				} else {
					iv_comp_ns -= iv_comp_ns / avg_cycles;
				}
				iv_comp_ns += ptp->xiv_tsc_offset * NS_PER_SEC / (tsc_frq/tsc_frq_fill);
			}

			tsc_ns_offset = rtc_ns + (iv_comp_fill ? iv_comp_ns/iv_comp_fill : 0) - stamp_ns;

			xtsc_last = ptp->x_tsc;
			rtc_ns_last = rtc_ns;
		}
		break;
		default:
		break;
	}
}


/* Main function for jailhouse cells */
void inmate_main() {
	int result = 0;
	bool terminate = false;
	int val[4] = {0,};


	print(__FILE__ " @ " __TIME__ "\n");

	comm_region->cell_state = JAILHOUSE_CELL_RUNNING_LOCKED;

	tsc_frq = tsc_init();                     /* initialize the TSC routines and get the TSC-freq const from the hypervisor */
	tsc_frq_initial = tsc_frq;
	tsc_frq_fill = 1;
	printk("TSC frequency: %lu.%03lu kHz\n", tsc_frq_initial / 1000, tsc_frq_initial % 1000);

	led_regs = (apl_padcfg *)(unsigned long)cmdline_parse_int("led-reg", COMPULAB_FITLET_LED_G1); /* read paramter from cmdline */
	app_mode = cmdline_parse_int("mode", CHRONY_VARIANT);
	avg_cycles = cmdline_parse_int("avg", app_mode==CHRONY_VARIANT?DEFAULT_CHRONY_AVG_RNDS:DEFAULT_GPTP_AVG_RNDS);
	
	if (led_regs) {
		map_range(led_regs, sizeof(apl_padcfg[4]), MAP_UNCACHED); /* that register address needs to be mapped manually */
		
		/* map 4 GPIO-pads (connected to 4 LEDs on Compu-Lab Fitlets) requiring 2 32bit registers PadCfg0 each on Intel Apollo-Lake */

		ivshmem_open(&dev, -1, NULL);     /* initialize the first found ivshmem with its mem references in dev */
		irq_init(jailhouse_irq_handler);  /* register this cell's IRQ hadler */
		enable_irqs();                    /* and enable the handler (it is an empty operation on x86) */
		ivshmem_set_state(&dev, 1);       /* signal being alive */
	}

	comm_region->cell_state = JAILHOUSE_CELL_RUNNING; /* no need to lock this binary */
	while (led_regs && !terminate) {  /* run the main loop until the termination signal is received from the HV */
		u64 tsc_ns;
		s64 rtc, rtc_ns, rtc_sec, tsc_frq_off, _iv_comp_ns, _drift_last;

		cpu_relax();                  /* asm( rep nop ) */

		disable_irqs();               /* disable irq signals (defer to afterwards) to protect the next lines from interruption */
			tsc_ns = tsc_read_ns();      /* read timestamp and derive realtime */
			rtc = tsc_ns+tsc_ns_offset;
			tsc_frq_off = tsc_frq/tsc_frq_fill-tsc_frq_initial;
			_iv_comp_ns = iv_comp_fill ? iv_comp_ns/iv_comp_fill : 0;
		enable_irqs();                /* reenable IRQs again */
		rtc_ns = rtc % IVSHMEM_SEC(1);
		rtc_sec= rtc / IVSHMEM_SEC(1);

		/* +0.5s by convention, so the ISR does not overlap with the pin toggle */
		int level_up = rtc_ns < IVSHMEM_MSEC(600) && rtc_ns >= IVSHMEM_MSEC(500);
		if (level_up && !val[1]) { /* if the LED should be on, but is not yet */
			/* always switch both colours of one LED (due to short-cut wire) */
			mmio_write32(&led_regs[0].pad, mmio_read32(&led_regs[0].pad) |  (1 << 0)); /* read-modify-write the LED-pad-register */
			mmio_write32(&led_regs[1].pad, mmio_read32(&led_regs[1].pad) |  (1 << 0));
			val[1] = 1;               /* save the new LED state */
			print("RTC is %02lld:%02lld:%02lld | f_off=%6lld + %6lld"
			      "| [%5lld]->%5lld <| %5lld, %5lld | %lld"
			      "\n",
			      (rtc_sec/3600) % 24, (rtc_sec/60) % 60, rtc_sec % 60, tsc_frq_off, _iv_comp_ns,
			      jitter_min, jitter_last, jitter_max, jitter_drift,
			      chrony_offset
				);
		} else if (!level_up && val[1]) { /* turn off the LED otherwise */
			mmio_write32(&led_regs[1].pad, mmio_read32(&led_regs[1].pad) & ~(1 << 0));
			mmio_write32(&led_regs[0].pad, mmio_read32(&led_regs[0].pad) & ~(1 << 0));
			val[1] = 0;
			disable_irqs();
				if (_drift_last != drift_last) {
					_drift_last = drift_last;
					enable_irqs();
					print("Got new drift: %lld -> %lu.%03lu kHz\n", _drift_last, tsc_frq / 1000, tsc_frq % 1000);
				} else
					enable_irqs();
		}

		/* communication pattern with HV */
		/* comm-region communication must be implemented, as the other binaries use it and it is configured */
		switch (comm_region->msg_to_cell) {
		case JAILHOUSE_MSG_SHUTDOWN_REQUEST:
				terminate = true;
				/* Don't actually send a reply in this case but exit the cell straight away */
			break;
		case JAILHOUSE_MSG_NONE:
			break;
		default:
			jailhouse_send_reply_from_cell(comm_region, JAILHOUSE_MSG_UNKNOWN);
			break;
		}

	}
	print("Cell finished (code=%d). So will the whole inmate: You must reload the cell.\n", result);
	comm_region->cell_state = JAILHOUSE_CELL_SHUT_DOWN;
}
