/*
 * IVSHMEM emulator extensions for use in POSIX environments
 *
 * Uses my interface from the Jailhouse's ivshmem code.
 *
 * Copyright (c) Universität Rostock, 2020
 *
 * Authors:
 *  Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 *
 * SPDX-License-Identifier: GPL-2 OR Apache-2.0
 */

/* only applies to the Linux variants */
#define _GNU_SOURCE

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>


#include "ivshmem.h"

#define SHM_PREFIX    "/"

#ifdef PIKEOS_POSIX
#include <sys/qport.h>
#define mkfifo(a,b) (-1) /* cannot make fifos in PIKEOS_POSIX. They are pre-allocated.*/
#define SHM_OPEN_FLAGS O_RDWR, 0
#define FIFO_PREFIX    "/qport/"

#else

#ifdef PIKEOS_ELINOS
#undef SHM_PREFIX
#define SHM_PREFIX    "shm:"
#define SHM_OPEN_FLAGS  O_RDWR
#define FIFO_PREFIX "/dev/vmport/"

#else

/* TODO, should have two SHMs, one per direction */
#define SHM_OPEN_FLAGS (O_RDWR|O_CREAT), S_IRUSR|S_IWUSR
#define FIFO_PREFIX "/tmp/"
#endif

#endif

static size_t _msize;

void ivshmem_reserve(size_t msize) {
	_msize = msize;
}

static void ivshmem_init( struct ivshmem_dev *dev, int bdf);

void ivshmem_open( struct ivshmem_dev *dev, int uio_no, struct ivshmem_cb *cb ) {
	ivshmem_init( dev, uio_no );
	if (cb) {
#ifndef PIKEOS_POSIX
		cb->fds.fd = dev->device;
		cb->fds.events = POLLIN;
#endif
	}
	ivshmem_sti ( dev );
}

int open_p4_resource(const char *p4_res, int oflags, const char *configfn, const char *cdevfn, int idx) {
	char fndev[64];
	struct stat statbuf;
	snprintf(fndev, sizeof(fndev), "%s%d", cdevfn, idx);
	if (0 > stat(fndev, &statbuf)) {
		FILE *f;
		if (NULL == (f = fopen(configfn, "w")))
			error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Opening \"%s\" failed.\n", configfn);
		if (0 > fprintf(f, "%d %s", idx, p4_res))
			error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Writing to \"%s\" failed.\n", configfn);
		fclose(f);
	}
	return open(fndev, oflags);
}

static void ivshmem_init( struct ivshmem_dev *dev, int bdf) {
	char fnw[64], fnr[64], fnshm[64];
	int fd, fdm;
	int svc = (_msize == 0);
	struct stat statbuf;

	snprintf(fnw,   sizeof(fnw),   "%s%s%d", FIFO_PREFIX, SVCW_FILENAME, bdf);
	snprintf(fnr,   sizeof(fnr),   "%s%s%d", FIFO_PREFIX, SVCR_FILENAME, bdf);
	snprintf(fnshm, sizeof(fnshm), "%s%s%d",  SHM_PREFIX,  SHM_FILENAME, bdf);

	memset(dev, 0, sizeof(*dev));
	dev->id    = svc?0:1;
	dev->peers = 2;
	dev->state_size = dev->peers * sizeof(*dev->state);  /* real ivshmem has this capped to PAGESIZE */
	dev->regs  = calloc( 1, sizeof( struct ivshm_regs ));
	dev->state = calloc( 1, dev->state_size);
	dev->lstate= calloc( 1, dev->state_size);

#ifndef PIKEOS_ELINOS
	if (0 > (fdm = shm_open(fnshm, SHM_OPEN_FLAGS)))
#else
	if (0 > (fdm = open_p4_resource(fnshm, SHM_OPEN_FLAGS, EVM_SHM_CONFIG, EVM_SHM_FILENAME, bdf)))
#endif
		error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Opening \"%s\" failed.\n", fnshm);


	if (!svc) { /* The SHM must be inflated before the queue handshake */
#ifndef PIKEOS_ELINOS
		if (0 > ftruncate( fdm, 2*_msize))
			error(IVSHMEM_ERROR_EXIT_VALUE, EINVAL, "Inflating SHM to %ld failed.\n", 2*_msize);
#endif
		dev->out_size = _msize;
	}

#ifndef PIKEOS_ELINOS
	if (0 > stat(fnr, &statbuf) && (0 > mkfifo(fnr, S_IRUSR|S_IWUSR)))
		error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Creating \"%s\"\n", fnr);
#endif

#ifndef PIKEOS_ELINOS
	if (0 > (fd = open(fnr, svc?O_RDONLY:O_WRONLY)))
#else
	if (0 > (fd = open_p4_resource(fnr, svc?O_RDONLY:O_WRONLY, EVM_SHM_CONFIG, EVM_SHM_FILENAME, bdf)))
#endif
		error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Opening \"%s\"\n", fnr);

	if (svc) dev->device = fd; else dev->send = fd;

#ifndef PIKEOS_ELINOS
	if (0 > stat(fnw, &statbuf) && (0 > mkfifo(fnw, S_IRUSR|S_IWUSR)))
		error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Creating \"%s\"\n", fnw);
#endif

	if (0 > (fd = open(fnw, svc?O_WRONLY:O_RDONLY)))
		error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Opening \"%s\"\n", fnr);

	if (svc) dev->send = fd; else dev->device = fd;

	if (svc) {
		if (0 > fstat( fdm, &statbuf ) )
			error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Stat for \"%s\" failed.\n", fnshm);

		if (statbuf.st_size % CONST_PAGE_SIZE || (statbuf.st_size / CONST_PAGE_SIZE > 255))
			error(IVSHMEM_ERROR_EXIT_VALUE, EINVAL, "Stat returned odd size-property=%lld.\n", (long long)statbuf.st_size);

		dev->out_size = statbuf.st_size/dev->peers;
	}

	if (dev->out_size) {
		u8 *mem = mmap(NULL, dev->out_size*dev->peers, PROT_READ | PROT_WRITE, MAP_SHARED, fdm, 0);
		close(fdm);  /* once mapped, the fd becomes redundant */
		if (!mem)
			error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Mapping \"%s\" failed.\n", fnshm);
		dev->in = mem;
		dev->out= mem + dev->id * dev->out_size;
		memset(dev->out, 0, dev->out_size); /* wipe output for safety. */
	}
	ivshmem_tell( dev );
}

void ivshmem_sti( struct ivshmem_dev *dev __unused) {
	/* nop */
}

toSt ivshmem_set_state( struct ivshmem_dev *dev, u32 state ) {
	if (dev->send > 0) {
		/* should catch SIGPIPE somewhere here and replace state as 0 accordingly */
		if (/*sizeof(state)*/4 > write(dev->send, &state, sizeof(state)))
			error(IVSHMEM_ERROR_EXIT_VALUE, errno, "Writing state 0x%0x to <%d> failed.\n", state, dev->send);
		return STATE_CHANGED_OK;
	} else
		return STATE_CHANGED_GONE;
}

/* send out an INT to target.
   However, the Linux UIO driver cannot distinguish between INT_no when notifying. */

void ivshmem_signal( struct ivshmem_dev *dev __unused, u32 intno __unused, u32 target __unused) {
	/* not implemented here, feature not used in actual apps */
	/* see ivshmem-uio.c for more */
}

int ivshmem_poll( struct ivshmem_dev *dev, u64 timeout ) {
	int fds_changed;
	
	struct timespec tv = { .tv_nsec = timeout, };
	if (timeout >= NS_PER_SEC) {
		tv.tv_nsec = timeout % NS_PER_SEC;
		tv.tv_sec  = timeout / NS_PER_SEC;
	}
#ifdef PIKEOS_POSIX
	fds_changed = 1;
	ioctl(dev->device, QPORT_STOUT, timeout == IVSHMEM_BLOCK ? NULL : &tv);
#else
	struct pollfd fds[1];
	fds[0].fd = dev->device;
	fds[0].events = POLLIN;

	fds_changed = ppoll(fds, (dev->device > 0), timeout == IVSHMEM_BLOCK ? NULL : &tv, NULL);
	if (fds_changed < 0)
		error(IVSHMEM_ERROR_EXIT_VALUE, errno, "poll()->%d", fds_changed);
#endif

	dev->int_count[0] += fds_changed;
	return fds_changed;
}

u32 ivshmem_read_device( struct ivshmem_dev *dev ) {
	u32 state = 0;
	ssize_t r;
	int src = dev->id==0 ? 1 : 0;
	r = read(dev->device, &state, sizeof(state));
		
	if (sizeof(state) == r) {
		((volatile u32 *)dev->state)[src] = state;
		return 1;
	} else if ( 0 == r ) {
		/* reading nothing after ppoll's indication means, the pipe is broken */
		/* TODO this is, however, different in the PikeOS variants, as those pipes are concrete-based */
		((volatile u32 *)dev->state)[src] = 0;
		return 1;
	} else
		/* reading any other size in Linux really is an error */
		error(IVSHMEM_ERROR_EXIT_VALUE, errno,  "\nread(%d:fifo)->%ld", dev->device, r);
	return 0;
}

void ivshmem_finalize( struct ivshmem_dev *dev ) {
	if (!dev) return;
	ivshmem_set_state( dev, 0 );
	if (dev->in)     munmap((void *)dev->in, dev->out_size*dev->peers);
	if (dev->device) close(dev->device);
	if (dev->send)   close(dev->send);
	free(dev->regs);
	free((void *)dev->state);
	free(dev->lstate);
	memset(dev, 0, sizeof(*dev));
}

unsigned long tsc_read_ns(void) {
	struct timespec tv;
	clock_gettime(CLOCK_MONOTONIC, &tv);
	return tv.tv_sec*NS_PER_SEC + tv.tv_nsec;
}

unsigned long process_clock_ns(void) {
	struct timespec tv;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tv);
	return tv.tv_sec*NS_PER_SEC + tv.tv_nsec;
}

unsigned long rtc_read_ns(void) {
	struct timespec tv;
	clock_gettime(CLOCK_REALTIME, &tv);
	return tv.tv_sec*NS_PER_SEC + tv.tv_nsec;
}

