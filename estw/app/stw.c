/*
 ============================================================================
 Name        : stw.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Version     :
 Copyright   : (c) 2017-2020 Universität Rostock
 Description : TRDP-stub to wire up Scade models
 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */
#if defined( PIKEOS_NATIVE )
/* PSSW API */
#include <vm.h>
#ifdef PIKEOSDEBUG
/* init_gdbstub(), gdb_breakpoint(). */
#include <vm_debug.h>
#endif

/* Enable assertions */
#undef NDEBUG

/* PikeOS Personality Extensions API */
#include <p4ext/p4ext_assert.h>

#define SHM_PAGES 20 /* == 80k */
#define SHM_ADDR  (void *)0x60000000
#define printf vm_cprintf
#define stdout
#define fflush(x)

#elif defined( JAILHOUSE )

#include <inmate.h>

/* I dunno, why the JH build-system explicitly blocks the definition of these attributes, so I redefine them locally. */
#ifndef __maybe_unused
#define __maybe_unused		__attribute__((unused))
#define __always_unused		__attribute__((unused))
#endif

#define fflush(x) /* smoother than ifdef block */

# if defined( __x86_64__ ) || __WORDSIZE == 64
#  define __PRI64_PREFIX        "l"
#  define __PRIPTR_PREFIX       "l"
# else
#  define __PRI64_PREFIX        "ll"
#  define __PRIPTR_PREFIX
# endif

#else

#include <unistd.h>
#define SHM_ADDR NULL
#endif

#include "../Stw.bm/onBoardStw_configured_nanoSim.h"
#include "../Stw.bm/kcg_consts.h"

#include "trdpSvc.h"

/* Scade-model I/O to be handled locally */

struct scade_local {
	struct scade_in_local {
		kcg_int dT;
	} in;
};

struct scade_in_ext {
	TrackPlan_nanoSim XTracks;
	allPR_nanoSim XPR;
	TrainReport_nanoStw_trdp XReport;
};

struct scade_out_ext {
	kcg_bool haveData;
	TrackDMI_nanoSim XTrainRelLimitLoc;
	ProtectionLocations_nanoStw_trdp PRtoOther;
	TrackLocation_nanoStw correctionPoint;
	SDM_DMI_wrapper_T_SDM_Types_Pkg pkgDMI;
	InterventionCommand_SpeedSupervision_UnitTest_Pkg InterventionCmd;
};

/* This definition overlaps the unspecified payload in shm_struct.
 * For Scade applications, you should have the description records for the above items plus one, for the config filename
 * and the items themselves.
 */

struct shm_out {
	struct trdpSvcRec item[10];
	struct scade_out_ext out;
};

struct shm_in {
	struct scade_in_ext  in;
};

static const char* fncfg = NULL;

static const char * const ext_if = "Stw.nano.uplink";
static const char * const int_if = "Stw.nano.ecn";
#if __GNUC__ < 8
static const char *ifaces[] = { "Stw.nano.uplink", "Stw.nano.ecn", NULL};
#else
static const char *ifaces[] = { ext_if, int_if, NULL};
#endif

/* SCADE likes to map kcg_int to int, which may differ, depending on what you need. I usually
 * force it to 32bit -> int32_t in my user_macros.h.
 * HOWEVER (2), this issue is even more confusing on newer Scade-Versions with fine-grained types. For whatever odd
 * reasons, the default mapping of int32 is to 'signed long', instead of standardized <stdint.h>'s int32_t, or just
 * int. So, different again depending on compiler / environment.
 * End of story: ALWAYS do you own type-mapping in user_macros.h, do not rely on the given defaults!
 */

DEFINE_TAU_XMARSHALL_MAP( xmap,
		kcg_bool, kcg_char, kcg_int,
		kcg_int, kcg_int, kcg_int, kcg_int,
		kcg_int, kcg_int, kcg_int, kcg_int,
		kcg_real, kcg_real,
		kcg_int, kcg_int, kcg_int
	);

static const char *handleIV(InterventionCommand_SpeedSupervision_UnitTest_Pkg i, speedSupervisionForDMI_T_DMI_Types_Pkg s);

static const char *handleIV(InterventionCommand_SpeedSupervision_UnitTest_Pkg i, speedSupervisionForDMI_T_DMI_Types_Pkg s) {
	static char dots[] = "... . .";
	char *c = dots;
	switch (i.eb) {
	case 1: *c = 'E'; break;
	case 2: *c = '_'; break;
	default:
		*c = (*c=='E' || *c=='e') ? 'e' : '.';
	}
	c++;
	switch (i.sb) {
	case 1: *c = 'S'; break;
	case 2: *c = '_'; break;
	default:
		*c = (*c=='S' || *c=='s') ? 's' : '.';
	}
	c++;
	switch (i.tco) {
	case 1: *c = 'C'; break;
	case 2: *c = '_'; break;
	default:
		*c = (*c=='C' || *c=='c') ? 'c' : '.';
	}
	c++;
	c++;
	switch (s.sup_status) {
	case CSM_DMI_Types_Pkg: *c='C'; break;
	case PIM_DMI_Types_Pkg: *c='P'; break;
	case TSM_DMI_Types_Pkg: *c='T'; break;
	case RSM_DMI_Types_Pkg: *c='R'; break;
	default: *c='?';
	}
	c++;
	c++;
	switch (s.supervisionDisplay) {
	case supDis_normal_DMI_Types_Pkg: *c='n'; break;
	case supDis_indication_DMI_Types_Pkg: *c='i'; break;
	case supDis_overspeed_DMI_Types_Pkg: *c='o'; break;
	case supDis_warning_DMI_Types_Pkg: *c='w'; break;
	case supDis_intervention_DMI_Types_Pkg: *c='v'; break;
	default: *c='?'; break;
	}
	return dots;
}

#ifdef JAILHOUSE
static int handle_signals(int model_locked) {
	switch (comm_region->msg_to_cell) {
	case JAILHOUSE_MSG_SHUTDOWN_REQUEST:
		/* this request should be handled by the Scade-Model, as it is the case for the ctrl model. */
		/* handle it here for now */
		if (model_locked) {
			print("Ctrl-Input signal ACTIVE state. Shutdown forbidden\n");
			jailhouse_send_reply_from_cell(comm_region, JAILHOUSE_MSG_REQUEST_DENIED);
		} else {
			/* ok, we can exit */
			/* TODO should tell TRDP about this. For now resetting the state through exiting will notify TRDP. */
			comm_region->cell_state = JAILHOUSE_CELL_RUNNING;
			jailhouse_send_reply_from_cell(comm_region, JAILHOUSE_MSG_REQUEST_APPROVED);
			return 0;
		}
		break;
	case JAILHOUSE_MSG_NONE:
		/* no message, no problem */
		break;
	case JAILHOUSE_MSG_RECONFIG_COMPLETED:
		/* this means, cell layout has changed, by destroying / creating a cell */
		/* it is a MSG_INFORMATION, so we can acknowledge it with JAILHOUSE_MSG_RECEIVED for a positive ack. */
		/* However, for this msg, jh doesn't bother at all. We could even let it time-out. */
		jailhouse_send_reply_from_cell(comm_region, JAILHOUSE_MSG_RECEIVED);
		break;
	default:
		/* this basically leads to a negative reply or nack o be safe on new messages */
		jailhouse_send_reply_from_cell(comm_region, JAILHOUSE_MSG_UNKNOWN);
		break;
	}
	return 1;
}

#elif defined( PIKEOS_NATIVE )
/* there are also messages from the health monitor, however, noone know */
static int handle_signals(int model_locked) {
	/* something like signals */
	return 1;
}

#else
static int handle_signals(int model_locked) {
	/* something like signals */
	return 1;
}
#endif

#ifdef JAILHOUSE
static int parse_args(int argc, char **argv, const char **other) {
	const char *help = "-- Params: config=/path/config.xml ivshmem=" XSERVICE_OPTARG_USAGE "\n";
	static char fncfg_buffer[MAX_XML_FILENAME_LENGTH];
	static char ivshmem_buffer[32];
	/* in Jailhouse, cmdline params are space-separated key-'='-value-pairs. Keys nor values can be escaped, thus, must
	   not contain spaces. Boolean params must either be 'false' or 'true'. Int values can be decimal or hex, though
	   the latter must start with '0x'. */
	/* log long = *_int bool = *_bool const char = *_str */
	*other = cmdline_parse_str("ivshmem", ivshmem_buffer, sizeof(ivshmem_buffer), NULL);
	fncfg = cmdline_parse_str("config", fncfg_buffer, sizeof(fncfg_buffer), NULL);
	print( "Forwarded Configuration: %s\n", fncfg?fncfg:help);
	return fncfg?0:1;
}

#else

static int parse_args(int argc, char **argv, const char **other) {
	const char *help = "--  Usage: /path/config.xml " XSERVICE_OPTARG_USAGE "\n";
	if (argc >= 3) *other = argv[2]; /* 1:config 2:shmem-id, otherwise below */
	for (int i=1; i<argc; i++) {
		if ((0 == strstr(argv[i], ".xml")) || (0 == strstr(argv[i], ".XML"))) { /* require it to be appended xml */
			fncfg = argv[i];
			break;
		} else 
			*other = argv[i];
	}
	
	print( "Forwarded Configuration: %s\n", fncfg?fncfg:help);
	return fncfg?0:1;
}

#endif

static int app_main( struct shm_data *sd ) {
	toSt result = 0;
	
	/* further initialize the memory with data */
	struct shm_out *extw = (struct shm_out *)sd->out;
	const struct shm_in  *extr = (const struct shm_in  *)sd->in;
	static const struct shm_in sd_in_zero; /* provide a zero buffer, for when we are unable to lock the shm */
	struct scade_local local = { 0, };
	/* initialize Scade model context */
	outC_onBoardStw_configured_nanoSim internalCtx;
	onBoardStw_configured_init_nanoSim(&internalCtx);
	extw->out.XTrainRelLimitLoc.TrainID = ~0;
	extw->out.PRtoOther.tid = ~0;

/* These MUST be in the same order, as they appear in the struct */
	TRDP_DEFINE_REQ (sd, extr->in.XTracks, 1111, 0);
	TRDP_DEFINE_REQ (sd, extr->in.XPR,     1141, 0);
	TRDP_DEFINE_IN  (sd, extr->in.XReport, 1131, 1);

	TRDP_DEFINE_OUT (sd, extw->out.XTrainRelLimitLoc, 1112, 0);
	TRDP_DEFINE_OUT (sd, extw->out.PRtoOther,         1142, 0);
	TRDP_DEFINE_OUT (sd, extw->out.correctionPoint,   1143, 1);
	TRDP_DEFINE_OUT (sd, extw->out.pkgDMI,            1022, 1);
	TRDP_DEFINE_OUT (sd, extw->out.InterventionCmd,   1132, 1);

	TRDP_DEFINE_CONF(sd, extw->out, fncfg, MAX_XML_FILENAME_LENGTH);

	while ( result != STATE_CHANGED_BAD && result != STATE_INVALID ) {
		static u64 t=ROUNDS_PER_STAT-1;
	/* sync with trdpSvc here */
		result = tau_xservice_sync_getcom( sd, tsc_read_ns()+IVSHMEM_MSEC(50) );
			/* did not aquire the shm lock in time, must not access that shm-region, as it may change unexpectedly */
		if (!( result == STATE_CHANGED_OK || result == STATE_CHANGED_GONE || result == STATE_TIMEOUT )) break;
		extr = result != STATE_CHANGED_OK ? &sd_in_zero : (const struct shm_in *)sd->in;

		local.in.dT = tau_xservice_take_time_for_app(sd);

	/* implementation of root operator of Scade model */
		onBoardStw_configured_nanoSim(
				(allPR_nanoSim *)&extr->in.XPR, (TrackPlan_nanoSim *)&extr->in.XTracks, (TrainReport_nanoStw_trdp *)&extr->in.XReport,
				&extw->out.correctionPoint, &extw->out.haveData, &extw->out.XTrainRelLimitLoc,
				&extw->out.PRtoOther, &extw->out.InterventionCmd, &extw->out.pkgDMI,
				&internalCtx);

		if (0 == handle_signals( extr->in.XReport.trainData.flag.active ))
			break;

	/* calc CPU time for full cycle, excludes sleeping, but includes some of the sys-call time */
		tau_xservice_time_after_app(sd);

	/* minimalistic UI, output part */
		if (++t==ROUNDS_PER_STAT) {
			print("%c:%d %s %3d/%3d(%3d) ->| %6d %6d:%3d:%4d %5llu us / %5llu us / %4d -%c-\n",
				extw->out.haveData? 'T':'?',
				extr->in.XReport.trainData.TrainId,
				handleIV(extw->out.InterventionCmd, extw->out.pkgDMI.sdm),
				extw->out.PRtoOther.velocity/28,
				extw->out.InterventionCmd.permittedSpeed/28,
				extw->out.InterventionCmd.interventionSpeed/28,
				extw->out.InterventionCmd.targetDistance/100,
				extw->out.PRtoOther.antenna.location/100, extw->out.PRtoOther.antenna.sid,
				extw->out.PRtoOther.antenna.distance/100,
				sd->mem->app/NS_PER_USEC, sd->mem->round/NS_PER_USEC, local.in.dT,
				extr == &sd_in_zero ? 'X' : '='
			  );
			t=0;
		}

	/* export data of model to TRDP */
		if ( result == STATE_CHANGED_OK )
			result = tau_xservice_sync_cycle( sd, 1 /* also block for getcom */, IVSHMEM_NONBLOCKING /* there is non in "both_locks-mode" */ );
	}
	ivshmem_finalize( &sd->dev );
	print("\n"); /* clean up the former CR */
	return result;
}

/* keep shm-size to a maximum of 255 pages / <1MB */
/* declare its min size here. 256 is the max size for a path name.
 * Take a different value, if posting the whole x-cfg into SHM. */

/* the differences are quite lightly sprinkled, so hard to merge, yet. */
#ifdef JAILHOUSE
void inmate_main() {
	struct shm_data sd; /* is cleared by connect */
	int result;
	const char *xsession_arg = NULL;
	size_t msize = MAX(sizeof(struct shm_struct) + sizeof(struct shm_out) + MAX_XML_FILENAME_LENGTH, sizeof(struct shm_in));
	print(__FILE__ " @ " __TIME__ "\n");

	/* take care of command line arguments */
	if (0 == (result = parse_args(0, NULL, &xsession_arg))) {
		/* initialize TRDP session */
		if ( 0 == (result = tau_xservice_connect( xsession_arg, xmap, ifaces, msize, NULL, &sd )) ) {

			comm_region->cell_state = JAILHOUSE_CELL_RUNNING_LOCKED;
				result = app_main( &sd );
			comm_region->cell_state = JAILHOUSE_CELL_RUNNING;

			if (result)	print("The XSession subsystem failed (code=%d).\n", result);
		} else {
			if (result)	print("The XService subsystem failed (code=%d).\n", result);
		}
	}
	print("XSession finished (code=%d). So will the whole inmate: You must restart the cell.\n", result);
	comm_region->cell_state = JAILHOUSE_CELL_SHUT_DOWN;
}

#else

int main(int argc, char **argv) {
#ifdef PIKEOSDEBUG
	gdb_breakpoint();
#endif
	static struct shm_data sd;
	int result;
	const char *xsession_arg = NULL;
	size_t msize = MAX(sizeof(struct shm_struct) + sizeof(struct shm_out) + MAX_XML_FILENAME_LENGTH, sizeof(struct shm_in));
	print(__FILE__ " @ " __TIME__ "\n");

	/* take care of command line arguments */
	if (0 == (result = parse_args(argc, argv, &xsession_arg))) {
		/* initialize TRDP session */
		if ( 0 == (result = tau_xservice_connect( xsession_arg, xmap, ifaces, msize, SHM_ADDR, &sd )) ) {
			result = app_main( &sd );
			if (result)	print("The XSession subsystem failed (code=%d).\n", result);
		} else {
			if (result)	print("The XService subsystem failed (code=%d).\n", result);
		}
	}
	return result;
}

#endif

