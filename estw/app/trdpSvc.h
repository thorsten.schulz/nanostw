/*
 * trdpSvc.h
 *
 * Copyright (c) Universität Rostock, 2019-2020
 *
 * Authors:
 *  Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef TRDPSVC_H_
#define TRDPSVC_H_

#include <stdint.h>

/* these headers are needed for some definitions, however, linking of libtrdp.a is NOT required in the client apps. */
#include <tau_xmarshall_map.h>
#include <tau_xsession_defaults.h>

#define MAX_INTERFACE_LENGTH TRDP_DEFAULT_MAX_LABEL_LEN
#define MAX_XML_FILENAME_LENGTH 256 /* this is just a security / santity limit */

#include "ivshmem.h"

#define SHM_DEFAULT_NUMBER                0
#define ESTW_DEFAULT_SHM_CLIENT_NUMBER    1
#define TRDPSVC_DEFAULT_SHM_CLIENT_NUMBER 0

#define ROUNDS_PER_STAT 16

#define MAXSIZE_FILENAME 32         /* size of static buffers for filename construction */
#define XSERVICE_OPTARG_USAGE " <shm-number[.target_id]>"

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#endif

#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#if defined( PIKEOS_POSIX )

//#include <vm.h>
//#include <sys/qport.h>

#elif defined( PIKEOS_NATIVE )

#include <vm.h>

#elif defined( JAILHOUSE) 

#include <inmate.h>

#undef  XSERVICE_OPTARG_USAGE
#define XSERVICE_OPTARG_USAGE "0xBDF.n irq_base=32" /* bus-device-function */
#undef  TRDPSVC_DEFAULT_SHM_CLIENT_NUMBER
#define TRDPSVC_DEFAULT_SHM_CLIENT_NUMBER 2
#undef  SHM_DEFAULT_NUMBER /* overwrite w/ autodetect value */
#define SHM_DEFAULT_NUMBER (-1)

/* the pm timer overflows frequently (24bit @ ~3.9MHz) and it only uses its SW-64bit when called within these cycles, ie, <1s */
#define clock() (pm_timer_read()/1000)

#endif

#define TRDP_XSVC_MAGIC 0x00AFFE00 /* identifier token for initial synchronization */
#define TRDP_XSVC_SETCOM 1 /* sending this through the pipe passes trdpSvc the token for <setcom> functions */
#define TRDP_XSVC_GETCOM 2 /* same for <getcom> functions */
#define TRDP_XSVC_CYCLE  3 /* only with this token, trdpSvc will <setcom>, block until deadline, and <getcom> */
#define TRDP_XSVC_ALIVE  4 /* set a bit to differentiate from dead-state (0) */

struct trdpSvcRec {
	uint32_t ComId;  /* the comId to use */
	uint32_t iface;  /* index to xnetif / claimed interfaces, zero-based, 0 = first interface */
	 int32_t __ID;     /* deprecated MUST not be used and removed. */
	uint32_t offset; /* offset_of( scade_type_instance in shm_struct ) */
	uint32_t size;   /* sizeof( scade_type ) */
	uint32_t request;
};

/* @purpose
*   Communication data structure created in shared memory segment
*/

struct shm_struct { /* currently 27*8 => 216 */
	uint64_t magic;     /* a sanity-check flag */
	int64_t  app;       /* some timing information for TRDP statistics */
	int64_t  round;
	/* configured deadline is preferred */
	/*  int64_t  deadline;*//* absolute point of time, until to receive packets, in nanoseconds since the system start
	....                (CLOCK_MONOTONIC) */
	uint64_t sizeIf;    /* count of interface name-fields (may be zero) in xnetif, must match MAX_INTERFACES */
	uint64_t sizeIn;    /* number of following incoming / subscription items */
	uint64_t sizeOut;   /* number of following output items */
	char     xnetif[MAX_INTERFACES][MAX_INTERFACE_LENGTH]; /* currently [8][16] */
	uint8_t  xtype_map[TAU_XTYPE_MAP_SIZE]; /* currently 40 */

	/* this shm_struct should directly be followed by the out-items and the out-payload */
};

/* Helper to wrap data related to communication, ie., container object for xservice functions. */
struct shm_data {
	struct ivshmem_dev dev;
	uint32_t state;   /* the local state, being passed back and forth */
	uint32_t target;  /* ivshmem needs to know to id of the peer to identify it in a multi-peer setup. */

	uint64_t ts0, ts1;
	uint64_t tproc;   /* process timers */

	struct shm_struct *mem;
	char *out;  /* currently pointing to mem+1; the ouput payload, which should be the first descriptor */
	const char *in;   /* this may be an extra page, but may be part of mem->payload; the input payload */ /* and it should be read-only!! */
};

/* shortcut */
#define TRDP_DEFINE_L(sd, itm, comid, interface, req, idx, start) do { \
		struct trdpSvcRec *tsr = ((struct trdpSvcRec *)sd->out)+(sd->mem->sizeIn + sd->mem->sizeOut); \
		tsr->ComId = comid;                  \
		tsr->iface = interface;              \
		tsr->offset = (char *)&itm - (char *)sd->start; \
		tsr->size = sizeof(itm);             \
		tsr->request = req;                  \
		sd->mem->idx++; \
	} while (0)

#define TRDP_DEFINE_LC(sd, itm, src, len, start) do { \
		struct trdpSvcRec *tsr = ((struct trdpSvcRec *)sd->out)+(sd->mem->sizeIn + sd->mem->sizeOut); \
		char *dst = (char *)((&itm)+1); \
		strncpy(dst, src, len);                  \
		tsr->ComId   = 0;                        \
		tsr->iface   = 0;                        \
		tsr->offset  = dst - (char *)sd->start;  \
		tsr->size    = len;                      \
		tsr->request = 0;                        \
	} while (0)

#define TRDP_DEFINE_IN( sd, itm, comid, iface) \
		TRDP_DEFINE_L(  sd, itm, comid, iface, 0, sizeIn, in)
#define TRDP_DEFINE_REQ(sd, itm, comid, iface) \
		TRDP_DEFINE_L(  sd, itm, comid, iface, 1, sizeIn, in)
#define TRDP_DEFINE_OUT(sd, itm, comid, iface) \
		TRDP_DEFINE_L(  sd, itm, comid, iface, 0, sizeOut, out)
#define TRDP_DEFINE_CONF(sd,itm, src, len) \
		TRDP_DEFINE_LC(  sd,itm, src, len, out)

const char *snprint_bdf(char *bdfb, int l, int bdf);

int tau_xservice_connect(
		const char *arg, /* pass some params for the IPC */
		const uint8_t *xTypeMap, /* pass the typemap array for xmarshall */
		const char * const *iface, /* give a list of interface names to initialize */
		size_t msize,    /* provide the min size of any of the two input and output struct shm_struct with app-specific members */
		u8   *shm_addr,  /* provide an address for the shm here, typically provide NULL */
		struct shm_data *sd); /* instance's data */

int64_t tau_xservice_take_time_for_app( struct shm_data *sd );
void    tau_xservice_time_after_app(    struct shm_data *sd );

toSt tau_xservice_sync_setup(  struct shm_data *sd );
toSt tau_xservice_sync_getcom( struct shm_data *sd, int64_t before_ns);
toSt tau_xservice_sync_cycle(  struct shm_data *sd, int both_locks, int64_t before_ns);

#endif /* TRDPSVC_H_ */
