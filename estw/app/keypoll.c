/*
 * key-helper.c
 */

/* KEY-HELPER */

#include <unistd.h> /* for stdin_fileno */
#include <termios.h>
#include <sys/time.h> /* struct timeval */
#include <stdio.h> /* getchar() */

#include "keypoll.h"

void keypoll_setmode(int want_key)
{
	static struct termios old, new;
	if (!want_key) {
		tcsetattr(STDIN_FILENO, TCSANOW, &old);
		return;
	}

	tcgetattr(STDIN_FILENO, &old);
	new = old;
	new.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &new);
}

int keypoll_get()
{
	int c = 0;
	struct timeval tv = {0,0};
	fd_set fs;

	FD_ZERO(&fs);
	FD_SET(STDIN_FILENO, &fs);
	select(STDIN_FILENO + 1, &fs, 0, 0, &tv);

	if (FD_ISSET(STDIN_FILENO, &fs)) {
		c = getchar();
	}
	return c;
}

