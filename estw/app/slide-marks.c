/*
 ============================================================================
 Name        : slide-benchruns.c
 Author      : Thorsten Schulz <thorsten.schulz@uni-rostock.de>
 Version     :
 Copyright   : (c) 2020 Universität Rostock
 Description : Run Slide trials 
 SPDX-License-Identifier: Apache-2.0
 ============================================================================
 */

#if defined( JAILHOUSE )

#include <inmate.h>

#define print printk

/* I dunno, why the JH build-system explicitly blocks the definition of these attributes, so I redefine them locally. */
#ifndef __maybe_unused
#define __maybe_unused		__attribute__((unused))
#define __always_unused		__attribute__((unused))
#endif

#elif defined( PIKEOS_NATIVE )
#include <vm.h>

#define print       vm_cprintf
#define tsc_read_ns p4_get_time

#define error(E,errno, fstr, ...) { \
			print( "\n" __FILE__ ": " fstr, ##__VA_ARGS__);\
			if (errno) print(":%s",p4_strerror(errno));\
			print("\n");\
			if (E) vm_shutdown(VM_RESPART_MYSELF); \
		}


#else

#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <error.h>
#include <errno.h>

#include <stdio.h>
#define print printf

#endif

#include "pullAll_test.h"
#include "ProtocolDryRun_tindyguard_test.h"
#include "kcg_consts.h"

#if defined( JAILHOUSE )

#elif defined( PIKEOS_NATIVE )

typedef unsigned long long u64;
typedef unsigned int u32;

#else

#define STACKSIZE 0x7FC00L
#define STUFF 0xCAFEBEEFCAFEBEEF
uint64_t *stck;

#define SYSFS_TSC_FREQ_KHZ "/sys/devices/system/cpu/cpu0/tsc_freq_khz"
#define NS_PER_SEC 1000000000LL
typedef uint64_t u64;
typedef uint32_t u32;
static unsigned long tsc_freq, tsc_overflow;
static unsigned long tsc_last[1];
static unsigned long tsc_overflows[1];



static void fill(uint64_t **p) {
	volatile uint64_t stack[STACKSIZE/sizeof(uint64_t)];
	*p = (uint64_t *)stack;
	for(int i=0; i<(sizeof(stack)/sizeof(stack[0])); i++) stack[i] = STUFF;
}

static size_t check(uint64_t *stack) {
	size_t used = STACKSIZE;
	uint64_t *end = stack+(STACKSIZE/sizeof(uint64_t));
	while (stack < end && *stack++ == STUFF) used -= sizeof(uint64_t);
	return used;
}

static int ReadLongFromFile(const char *fn, unsigned long *val) {
	char buf[64];
	FILE *f = fopen(fn, "r");
	if ( f ) {
		if ( fgets(buf, sizeof(buf), f) ) {
			*val = strtoul(buf, NULL, 0);
			fclose(f);
			return 0;
		}
		fclose(f);
	}
	return 1;	
}

static unsigned long tsc_init(int argc, char **argv) {
	if (0 != ReadLongFromFile(SYSFS_TSC_FREQ_KHZ, &tsc_freq)) {
		error(0, errno, "Oops, reading '%s' failed. Have you loaded the 'tsc_freq_khz.ko' through 'git clone https://github.com/trailofbits/tsc_freq_khz.git' ?\n", SYSFS_TSC_FREQ_KHZ);
		if (argc > 1) tsc_freq = strtoul(argv[1], NULL, 0);
		if (tsc_freq < 200000)
			tsc_freq = 2400000;
	}
	tsc_freq *= 1000L;
	tsc_overflow = (0x100000000L * NS_PER_SEC) / tsc_freq;
	return tsc_freq;
}

/* TSC code from Jailhouse inmate library */
static u64 rdtsc(void) {
	u32 lo, hi;
	asm volatile("rdtsc" : "=a" (lo), "=d" (hi));
	return (u64)lo | (((u64)hi) << 32);
}

unsigned long tsc_read_ns(void)
{
	unsigned int cpu = 0;
	unsigned long tmr;

	tmr = ((rdtsc() & 0xffffffffLL) * NS_PER_SEC) / tsc_freq;
	if (tmr < tsc_last[cpu])
		tsc_overflows[cpu] += tsc_overflow;
	tsc_last[cpu] = tmr;
	return tmr + tsc_overflows[cpu];
}
#endif

#define TDIV 100ull
#define RND(x) ((((unsigned long long)x)+TDIV/2)/TDIV)

#define run_testarray( func, n, typecast, data ) { \
	kcg_bool ret;                              \
	u64 idx, min, nmin, max, nmax, rnd, overall; \
	for (overall=0,ret=0,idx=0,min=0,nmin=0,max=0,nmax=0; idx<n; idx++) { \
		rnd=tsc_read_ns();                     \
		ret = func( ret, typecast &data[idx]); /* discard const */\
		rnd = tsc_read_ns() -rnd;              \
		if (!min || rnd < min) { min = rnd; nmin = idx; } \
		if (max < rnd) { max = rnd; nmax=idx; }  \
		overall += rnd;                        \
	}                                          \
	print("%6lld %6lld %6lld %4lld %4lld %4lld " #func "\n", RND(min), RND(overall/n), RND(max), (unsigned long long)nmin, (unsigned long long)n, (unsigned long long)nmax); \
}

#define run_testmore( func, n, typecast, data ) { \
	kcg_bool ret;                              \
	u64 idx, min, nmin, max, nmax, rnd, overall; \
	for (overall=0,ret=0,idx=0,min=0,nmin=0,max=0,nmax=0; idx<n; idx++) { \
		rnd=tsc_read_ns();                     \
		ret = func( ret, typecast &data); /* discard const */\
		rnd = tsc_read_ns() -rnd;              \
		if (!min || rnd < min) { min = rnd; nmin = idx; } \
		if (max < rnd) { max = rnd; nmax=idx; }  \
		overall += rnd;                        \
	}                                          \
	print("%6lld %6lld %6lld %4lld %4lld %4lld " #func "\n", RND(min), RND(overall/n), RND(max), (unsigned long long)nmin, (unsigned long long)n, (unsigned long long)nmax); \
}

#define run_aeadmore( func, n ) { \
	u64 idx, min, nmin, max, nmax, rnd, overall; \
	for (overall=0,idx=0,min=0,nmin=0,max=0,nmax=0; idx<n; idx++) { \
		rnd=tsc_read_ns();                     \
		func( ); \
		rnd = tsc_read_ns() -rnd;              \
		if (!min || rnd < min) { min = rnd; nmin = idx; } \
		if (max < rnd) { max = rnd; nmax=idx; }  \
		overall += rnd;                        \
	}                                          \
	print("%6lld %6lld %6lld %4lld %4lld %4lld " #func "\n", RND(min), RND(overall/n), RND(max), (unsigned long long)nmin, (unsigned long long)n, (unsigned long long)nmax); \
}

#define run_x25519( func, sr, n, data) {              \
	kcg_bool ret;                              \
	u64 idx, min, nmin, max, nmax, rnd, overall; \
	for (overall=0,ret=1,idx=0,min=0,nmin=0,max=0,nmax=0; idx<n; idx++) { \
		rnd=tsc_read_ns();                     \
		for(int i=0;i<sr; i++) func( &ret, &data); func( &ret, &data);          \
		rnd = tsc_read_ns() -rnd;              \
		if (!min || rnd < min) { min = rnd; nmin = idx; } \
		if (max < rnd) { max = rnd; nmax=idx; }  \
		overall += rnd;                        \
	}                                          \
	print("%6lld %6lld %6lld %4lld %4lld %4lld " #func "~" #sr "\n", RND(min), RND(overall/n), RND(max), (unsigned long long)nmin, (unsigned long long)n, (unsigned long long)nmax); \
}

#define run_testmore_noret( func, n, typecast, data ) { \
	kcg_bool ret;                              \
	u64 idx, min, nmin, max, nmax, rnd, overall; \
	for (overall=0,ret=0,idx=0,min=0,nmin=0,max=0,nmax=0; idx<n; idx++) { \
		rnd=tsc_read_ns();                     \
		func( &ret, typecast data); /* discard const */\
		rnd = tsc_read_ns() -rnd;              \
		if (!min || rnd < min) { min = rnd; nmin = idx; } \
		if (max < rnd) { max = rnd; nmax=idx; }  \
		overall += rnd;                        \
	}                                          \
	print("%6lld %6lld %6lld %4lld %4lld %4lld " #func " ->%dk\n", RND(min), RND(overall/n), RND(max), (unsigned long long)nmin, (unsigned long long)n, (unsigned long long)nmax, ret); \
}

/* shortened version of tindyguard::test::ProtocolDryRun/ */
void HandshakeDryRun_tindyguard_test(kcg_bool *failed,outC_ProtocolDryRun_tindyguard_test *outC);
void HandshakeDryRun_tindyguard_test(
  /* _L69/, fail/, failed/ */
  kcg_bool *failed,
  outC_ProtocolDryRun_tindyguard_test *outC)
{
  static kcg_bool skip_init;
  static size_tindyguardTypes acc;
  static kcg_size idx;
  static Peer_tindyguardTypes tmp;
  static array_uint32_16_4 tmp1;
  static array_uint32_16_4 tmp2;
  static Session_tindyguardTypes _3_noname;
  static length_t_udp _4_noname;
  /* BigZ/, _L87/ */
  static KeySalted_tindyguardTypes BigZ;
  /* @1/_L17/, @1/knownPeer/, _L155/, _L70/, initialPeers/ */
  static _6_array initialPeers;
  /* ChickenJoe/, _L5/ */
  static KeySalted_tindyguardTypes _L5;
  /* @2/_L20/, @3/_L10/, @3/o/, _L6/ */
  static kcg_bool _L6;
  /* _L61/, _L86/ */
  static kcg_bool _L61;
  /* _L63/ */
  static Session_tindyguardTypes _L63;
  /* _L4/, _L81/ */
  static kcg_bool _L81;
  /* _L88/ */
  static kcg_bool _L88;
  /* _L126/, _L158/ */
  static kcg_bool _L126;
  /* _L125/ */
  static Session_tindyguardTypes _L125;
  /* @4/_L1/, @4/i/, @5/_L1/, @5/i/, _L134/, _L79/ */
  static array_uint32_16_4 _L134;
  /* _L138/, _L147/ */
  static kcg_bool _L138;
  /* _L132/, _L136/ */
  static length_t_udp _L136;
  /* @1/_L4/, @1/pid/, _L153/ */
  static size_tindyguardTypes _L153;

if (!skip_init) {
  skip_init = 1;
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[3],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[4],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[5],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[6],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[7],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  /* _L49=(tindyguard::initPeer#1)/ */
  initPeer_tindyguard(
    (pub_tindyguardTypes *) &BigZ_pub_tindyguard_test,
    (secret_tindyguardTypes *) &preshared_tindyguard_test,
    (peer_t_udp *) &BigZ_endpoint_tindyguard_test,
    (range_t_udp *) &BigZ_allowed_tindyguard_test,
    &initialPeers[0]);
  /* _L104=(tindyguard::initPeer#2)/ */
  initPeer_tindyguard(
    (pub_tindyguardTypes *) &ChickenJoe_pub_tindyguard_test,
    (secret_tindyguardTypes *) &preshared_tindyguard_test,
    (peer_t_udp *) &ChickenJoe_endpoint_tindyguard_test,
    (range_t_udp *) &ChickenJoe_allowed_tindyguard_test,
    &initialPeers[1]);
  /* _L111=(tindyguard::initPeer#3)/ */
  initPeer_tindyguard(
    (pub_tindyguardTypes *) &Lani_pub_tindyguard_test,
    (secret_tindyguardTypes *) &preshared_tindyguard_test,
    (peer_t_udp *) &Lani_endpoint_tindyguard_test,
    (range_t_udp *) &net10_allowed_tindyguard_test,
    &initialPeers[2]);
  /* _L4=(tindyguard::initOur#2)/ */
  initOur_tindyguard(
    (Key32_slideTypes *) &ChickenJoe_priv_tindyguard_test,
    &_L81,
    &_L5,
    &_L6,
    &outC->Context_initOur_2);
  /* _L86=(tindyguard::initOur#3)/ */
  initOur_tindyguard(
    (Key32_slideTypes *) &BigZ_priv_tindyguard_test,
    &_L61,
    &BigZ,
    &_L88,
    &outC->Context_initOur_3);
}
  /* @1/_L3= */
  _L153 = InvalidPeer_tindyguardTypes;
  for (idx = 0; idx < 8; idx++) {
    acc = _L153;
    _L6 = BigZ_ip_tindyguard_test.addr != kcg_lit_uint32(0) && kcg_lit_uint32(
        0) == ((BigZ_ip_tindyguard_test.addr ^
          initialPeers[idx].allowed.net.addr) &
        initialPeers[idx].allowed.mask.addr) && kcg_lit_uint32(0) !=
      initialPeers[idx].allowed.net.addr;
    /* @2/_L9= */
    if (_L6) {
      _L153 = /* @1/_L3= */(kcg_int32) idx;
    }
    else {
      _L153 = acc;
    }
    /* @1/_L3= */
    if (!!_L6) {
      break;
    }
  }
  if (kcg_lit_int32(0) <= _L153 && _L153 < kcg_lit_int32(8)) {
    kcg_copy_Peer_tindyguardTypes(&tmp, &initialPeers[_L153]);
  }
  else {
    kcg_copy_Peer_tindyguardTypes(
      &tmp,
      (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  }
  /* _L132=(tindyguard::handshake::init#1)/ */
  init_tindyguard_handshake(
    &tmp,
    _L153,
    &_L5,
    kcg_lit_int64(1234),
    &_L136,
    &_L134,
    &_3_noname,
    &_L81,
    &outC->Context_init_1);
  for (idx = 0; idx < 12; idx++) {
    tmp1[0][idx] = kcg_lit_uint32(0);
    tmp2[0][idx] = kcg_lit_uint32(0);
  }
  kcg_copy_array_uint32_4(&tmp1[0][12], (array_uint32_4 *) &_L134[0][0]);
  kcg_copy_array_uint32_12(&tmp1[1][0], (array_uint32_12 *) &_L134[0][4]);
  kcg_copy_array_uint32_4(&tmp1[1][12], (array_uint32_4 *) &_L134[1][0]);
  kcg_copy_array_uint32_12(&tmp1[2][0], (array_uint32_12 *) &_L134[1][4]);
  kcg_copy_array_uint32_4(&tmp1[2][12], (array_uint32_4 *) &_L134[2][0]);
  kcg_copy_array_uint32_12(&tmp1[3][0], (array_uint32_12 *) &_L134[2][4]);
  kcg_copy_array_uint32_4(&tmp1[3][12], (array_uint32_4 *) &_L134[3][0]);
  /* _L133=(tindyguard::handshake::respond#2)/ */
  respond_tindyguard_handshake_8(
    _L136,
    &tmp1,
    (peer_t_udp *) &emptyPeer_udp,
    &BigZ,
    (Session_tindyguardTypes *) &EmptySession_tindyguardTypes,
    &initialPeers,
    kcg_lit_int64(1357),
    &_4_noname,
    &_L134,
    &_L63,
    &_L61,
    &_L138,
    &_L126,
    &outC->Context_respond_2);
  kcg_copy_array_uint32_4(&tmp2[0][12], (array_uint32_4 *) &_L134[0][0]);
  kcg_copy_array_uint32_12(&tmp2[1][0], (array_uint32_12 *) &_L134[0][4]);
  kcg_copy_array_uint32_4(&tmp2[1][12], (array_uint32_4 *) &_L134[1][0]);
  kcg_copy_array_uint32_12(&tmp2[2][0], (array_uint32_12 *) &_L134[1][4]);
  kcg_copy_array_uint32_4(&tmp2[2][12], (array_uint32_4 *) &_L134[2][0]);
  kcg_copy_array_uint32_12(&tmp2[3][0], (array_uint32_12 *) &_L134[2][4]);
  kcg_copy_array_uint32_4(&tmp2[3][12], (array_uint32_4 *) &_L134[3][0]);
  /* _L125=(tindyguard::handshake::eval_response#1)/ */
  eval_response_tindyguard_handshake_3(
    &_3_noname,
    _L61,
    _4_noname,
    &tmp2,
    (peer_t_udp *) &emptyPeer_udp,
    &_L5,
    &_L125,
    &_L126);
}


static void my_tests_test_ietf_RFC8032( void ) {
//		tmp =  test_ed25519_test_ietf_RFC8032    (tmp, (testcase_test_ietf_RFC8032 *) &tc_test_ietf_RFC8032[idx]); /* 2 */
	run_testarray(  test_ed25519_test_ietf_RFC8032,    10ul, (testcase_test_ietf_RFC8032 *), tc_test_ietf_RFC8032); /* 2 */
	run_testarray( bench_ed25520_test_ietf_RFC8032_16, 10ul, (testcase_test_ietf_RFC8032 *), tc_test_ietf_RFC8032);
	run_testarray( bench_ed25520_test_ietf_RFC8032_64, 10ul, (testcase_test_ietf_RFC8032 *), tc_test_ietf_RFC8032);
	run_testarray( bench_ed25520_test_ietf_RFC8032_256,10ul, (testcase_test_ietf_RFC8032 *), tc_test_ietf_RFC8032);
}

static void my_test_test_ietf_RFC7693( void ) {
	run_testarray( bench_hash_test_ietf_RFC7693_16,  10ul, (simpleTC_test *), stc_test );
	run_testarray( bench_hash_test_ietf_RFC7693_64,  10ul, (simpleTC_test *), stc_test );
	run_testarray( bench_hash_test_ietf_RFC7693_256, 10ul, (simpleTC_test *), stc_test );
	
	run_testmore( test_BLAKE2s_it_test_ietf_RFC7693,     10ul, (testcase_test_ietf_RFC7693 *), test_blake2s_test_ietf_RFC7693 );
	run_testmore( test_BLAKE2s_key_it_test_ietf_RFC7693, 10ul, (testcase_test_ietf_RFC7693 *), test_blake2s_keyed_test_ietf_RFC7693 );
	
	run_testmore( test_hkdf_it_test_ietf_RFC7693, 10ul, (testcase_hkdf_test_ietf_RFC7693 *), test_hkdf_test_ietf_RFC7693);
}

static void my_test_test_ietf_RFC6234( void ) {
	run_testarray( test_SHA256_it_test_ietf_RFC6234,  9ul, (testcase_test_ietf_RFC6234 *), test_sha256_test_ietf_RFC6234 );
	run_testarray( test_HMAC256_it_test_ietf_RFC6234, 4ul, (testcase_test_ietf_RFC6234 *), test_hmac256_test_ietf_RFC6234 );
	run_testarray( test_SHA512_it_test_ietf_RFC6234,  9ul, (testcase_test_ietf_RFC6234 *), test_sha512_test_ietf_RFC6234 );
	run_testarray( test_HMAC512_it_test_ietf_RFC6234, 4ul, (testcase_test_ietf_RFC6234 *), test_hmac512_test_ietf_RFC6234 );
	run_testarray( test_HKDF256_it_test_ietf_RFC6234, 3ul, (testcaseHKDF_test_ietf_RFC6234 *), test_hkdf256_test_ietf_RFC6234 );
	
	run_testarray( bench_hash256_test_ietf_RFC6234_16,  10ul, (simpleTC_test *), stc_test );
	run_testarray( bench_hash256_test_ietf_RFC6234_64,  10ul, (simpleTC_test *), stc_test );
	run_testarray( bench_hash256_test_ietf_RFC6234_256, 10ul, (simpleTC_test *), stc_test );
	run_testarray( bench_hash512_test_ietf_RFC6234_16,  10ul, (simpleTC_test *), stc_test );
	run_testarray( bench_hash512_test_ietf_RFC6234_64,  10ul, (simpleTC_test *), stc_test );
	run_testarray( bench_hash512_test_ietf_RFC6234_256, 10ul, (simpleTC_test *), stc_test );
	
	run_testmore ( bench_HKDF256_test_ietf_RFC6234, 10ul, (testcase_hkdf_test_ietf_RFC7693 *), test_hkdf_test_ietf_RFC7693 );
}

static void my_bench_test_ietf_RFC7539( void ) {
	run_aeadmore( bench_aead_test_ietf_RFC7539_2   , 10ul );
	run_aeadmore( bench_xaead_test_ietf_RFC7539_2  , 10ul );
	run_aeadmore( bench_aead_test_ietf_RFC7539_23  , 10ul );
	run_aeadmore( bench_xaead_test_ietf_RFC7539_23 , 10ul );
	run_aeadmore( bench_aead_test_ietf_RFC7539_64  , 10ul );
	run_aeadmore( bench_xaead_test_ietf_RFC7539_64 , 10ul );
	run_aeadmore( bench_aead_test_ietf_RFC7539_256 , 10ul );
	run_aeadmore( bench_xaead_test_ietf_RFC7539_256, 10ul );
}

static void my_test_x25519_algos_test_ietf_RFC7748( outC_test_x25519_algos_test_ietf_RFC7748 *outC) {

	run_x25519( bench_x25519_test_ietf_RFC7748_8 , 5, 1000ul, outC->Context_bench_x25519_2);
	run_x25519( bench_x25519_test_ietf_RFC7748_19, 2, 1000ul, outC->Context_bench_x25519_3);
	run_x25519( bench_x25519_test_ietf_RFC7748_21, 1, 1000ul, outC->Context_bench_x25519_4);

	run_x25519( bench_x25519_test_ietf_RFC7748_20, 1, 1000ul, outC->Context_bench_x25519_5 );
}

static void my_bench_ProtocolDryRun_tindyguard_test( outC_ProtocolDryRun_tindyguard_test *outC ) {
	run_testmore_noret( ProtocolDryRun_tindyguard_test,  10ul, (outC_ProtocolDryRun_tindyguard_test *), outC);
	ProtocolDryRun_init_tindyguard_test( outC );
	run_testmore_noret( HandshakeDryRun_tindyguard_test, 10ul, (outC_ProtocolDryRun_tindyguard_test *), outC);
}

static void actual_main( unsigned long tsc_freq ) {
	outC_pullAll_test outC;  /* is a little more than 4k (4160) */
	outC_ProtocolDryRun_tindyguard_test outT;
	print("State-Size:%ld @ 0x%08lx , %ld @ 0x%08lx \n", sizeof(outC), (long int)&outC, sizeof(outT), (long int)&outT);
	print("Time-res:%lld ns\n", TDIV);
	print("TSC-freq:%ld Hz\n", tsc_freq);
	pullAll_init_test( &outC );
	ProtocolDryRun_init_tindyguard_test( &outT );
	print("Done initializing.\n");
	
	print("__min_ __avg_ __max_ i_min n_avg i_max ____________________func______\n");
	my_tests_test_ietf_RFC8032();
	my_tests_test_ietf_RFC8032();
	my_test_test_ietf_RFC7693();
	my_test_test_ietf_RFC6234();
	my_bench_test_ietf_RFC7539();

	my_test_x25519_algos_test_ietf_RFC7748( &outC.Context_test_x25519_algos_1 );
	my_bench_ProtocolDryRun_tindyguard_test( &outT );
}

#if defined( JAILHOUSE )
/* using the patch for nanostw, we get about 0.5MB stack */
void inmate_main() {
	//excp_reporting_init();
	print(__FILE__ " @ " __TIME__ "\n");
	unsigned long tf = tsc_init();

	actual_main(tf); /* seems to require ~56k stack mem plain before instrumentation */

	print("===========================================================\nYou must destroy this cell now.\n");
	comm_region->cell_state = JAILHOUSE_CELL_SHUT_DOWN;
	stop();
}

#elif defined( PIKEOS_NATIVE )

int main(int argc, char **argv) {
	actual_main(1593600000ull); /* PikeOS delivers ns anyways */
	print("===========================================================\nYou must reboot now.\n");
}

#else

int main(int argc, char **argv) {
	unsigned long tf = tsc_init(argc, argv);
	fill(&stck);

	actual_main(tf); /* seems to require ~56k stack mem plain before instrumentation */
	
	print("Stack:%ldk / %ldk\n", (check(stck)+0xfff)/0x1000*4, STACKSIZE/0x400);
	
	return 0;
}

#endif
