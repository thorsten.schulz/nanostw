/* File to ease the interface with Scade-Generated-Code and the execution environment. */

/* make memcpy() available. */
#ifdef JAILHOUSE
#include <inmate.h>
#else
#include <string.h>
#endif

#ifndef __WORDSIZE
#if __SIZEOF_POINTER__ == 8
#define __WORDSIZE 64
#else
#define __WORDSIZE 32
#endif
#endif
/* Choose the version of curve25519-donna based on the word size */
#if __WORDSIZE == 64 && defined(__GNUC__)
#include "donna/curve25519-donna-c64.c"
#else
#include "donna/curve25519-donna.c"
#endif

/* Could include kcg_imported_functions.h to correctly replicate this, however, the locations in the fs change depending
   on the targeted environment. Easier to hard-define it here - no dynamism expected. */
typedef int Key32_slideTypes[8];
void curve25519_donna_nacl_box( Key32_slideTypes *ourPriv, Key32_slideTypes *their, Key32_slideTypes *key);

void curve25519_donna_nacl_box( Key32_slideTypes *ourPriv, Key32_slideTypes *their, Key32_slideTypes *key) {
	curve25519_donna((uint8_t *)key, (uint8_t *)ourPriv, (uint8_t *)their);
}
