/*
 SPDX-License-Identifier: GPL-2.0
*/

#include "ivshmem.h"

static size_t _msize;

void ivshmem_reserve(size_t msize) {
	_msize = msize;
}

/* inmates-lib does not have one, this is inspired by the one from the GNU man page of strncpy */
char *strncpy(char *dest, const char *src, unsigned n) {
	if (n && src && dest) {
		unsigned i;
		n--;
		for (i = 0; i < n && src[i]; i++)	dest[i] = src[i];
		for ( ; i < n; i++)					dest[i] = '\0';
		dest[n] = '\0'; /* always terminate. If this is not your intention, use memcpy! */
	}
	return dest;
}

static u64 pci_cfg_read64(u16 bdf, unsigned int addr) {
	return pci_read_config(bdf, addr, 4) | ((u64)pci_read_config(bdf, addr + 4, 4) << 32);
}

static void ivshmem_init(struct ivshmem_dev *dev, int bdf, int irqbase, int oneshot) {
	unsigned long baseaddr, addr;
	int vndr_cap, n;
	int vectors;
	unsigned int class_rev;

	if (bdf == -1) { /* try auto-detect (the first IVSHMEM) */
		if (0 > (bdf = pci_find_device(VENDORID, DEVICEID, 0)))
			error(IVSHMEM_ERROR_EXIT_VALUE, 0, "IVSHMEM: No PCI devices found .. nothing to do.\n");
	}

	class_rev = pci_read_config(bdf, 0x8, 4);
	if (class_rev != (PCI_DEV_CLASS_OTHER << 24 | JAILHOUSE_SHMEM_PROTO_UNDEFINED << 8))
		error(IVSHMEM_ERROR_EXIT_VALUE, 0, "IVSHMEM: class/revision %08x, not supported\n", class_rev);

	if (0 > (vndr_cap = pci_find_cap(bdf, PCI_CAP_VENDOR)))
		error(IVSHMEM_ERROR_EXIT_VALUE, 0, "IVSHMEM ERROR: missing vendor capability\n");

	memset(dev, 0, sizeof(struct ivshmem_dev));
	dev->irq_base = irqbase;

	dev->regs = (struct ivshm_regs *)BAR_BASE;
	pci_write_config(bdf, PCI_CFG_BAR, (unsigned long)dev->regs, 4);

	dev->msix = (u32 *)(BAR_BASE + PAGE_SIZE);
	pci_write_config(bdf, PCI_CFG_BAR + 4, (unsigned long)dev->msix, 4);

	pci_write_config(bdf, PCI_CFG_COMMAND, (PCI_CMD_MEM | PCI_CMD_MASTER), 2);

	if (oneshot) 
		pci_write_config(bdf, vndr_cap + IVSHM_CFG_PRIV_CNTL, IVSHM_PRIV_CNTL_ONESHOT_INT, 1);
		
	map_range((void *)BAR_BASE, 2 * PAGE_SIZE, MAP_UNCACHED);

	dev->id    = mmio_read32(&dev->regs->id);
	dev->peers = mmio_read32(&dev->regs->max_peers);

	dev->state_size = pci_read_config(bdf, vndr_cap + IVSHMEM_CFG_STATE_TAB_SZ, 4);
	dev->rw_size =    pci_cfg_read64 (bdf, vndr_cap + IVSHMEM_CFG_RW_SECTION_SZ);
	dev->out_size =   pci_cfg_read64 (bdf, vndr_cap + IVSHMEM_CFG_OUT_SECTION_SZ);
	baseaddr =        pci_cfg_read64 (bdf, vndr_cap + IVSHMEM_CFG_ADDRESS);

	addr = baseaddr;
	dev->state = (u32 *)addr;
	addr += dev->state_size;
	
	if (dev->rw_size > 0) {
		dev->rw = (u8 *)addr;
		addr += dev->rw_size;
	} else
		dev->rw = NULL;
	
	if (dev->out_size > 0) {
		dev->in  = (u8 *)addr;
		addr    += dev->id * dev->out_size;
		dev->out = (u8 *)addr;
	}

	map_range(
		(void *)baseaddr,
		dev->state_size + dev->rw_size + dev->peers*dev->out_size,
		MAP_CACHED);

	dev->has_msix = pci_find_cap(bdf, PCI_CAP_MSIX) > 0 ? true : false;
	vectors = dev->has_msix ? IVSHMEM_MAX_VECTORS : 1;

	for (n = 0; n < vectors; n++) {
		if (dev->has_msix) pci_msix_set_vector(bdf, irqbase + n, n);
		irq_enable(irqbase + n);
	}
	dev->lstate = alloc(dev->peers*sizeof(*dev->state), sizeof(*dev->state));
	dev->device = bdf;
	memset(dev->out, 0, dev->out_size); /* wipe output for safety. */
	ivshmem_tell( dev );
}

void ivshmem_open( struct ivshmem_dev *dev, int devid, struct ivshmem_cb *cb ) {
	static char bdfb[] = "*detect";
	int irq_base;
/* in Jailhouse, we do this all via one PCI-IVSHMEM device. You can specify it via the ivshmem=0x... paramter */
/* if you need a second IVSHMEM, no worries, go and and extend the code :) */
	if (devid!=-1) {
		devid &= 0xffff;
		snprint_bdf(bdfb, sizeof(bdfb), devid);
	}

	irq_base = (!cb) ? cmdline_parse_int("irq_base", DEFAULT_IRQ_BASE) : cb->irq_base;
	print("PCI: %s IRQ: %d\n", bdfb, irq_base);

	tsc_init();
	pci_init();
	ivshmem_init( dev, devid, irq_base, 0 /* oneshot */);
	ivshmem_sti ( dev );
}

void ivshmem_sti( struct ivshmem_dev *dev ) {
	mmio_write32(&dev->regs->int_control, 1);
}

/* Do not pass state=0, unless the app goes away. This is a special value in JH-IVSHMEM */
toSt ivshmem_set_state( struct ivshmem_dev *dev, u32 state ) {
	mmio_write32(&dev->regs->state, state);
	return STATE_CHANGED_OK;
}

void ivshmem_signal( struct ivshmem_dev *dev, u32 intno, u32 target ) {
	u32 bell_no = dev->has_msix ? (intno & 0xFFFF) : 0;
	bell_no |= target << 16;
	mmio_write32(&dev->regs->doorbell, bell_no);
}

/* poll only cares about the state-change vector 0 */
int ivshmem_poll( struct ivshmem_dev *dev, u64 timeout ) {
	u64 now = tsc_read_ns();
	timeout += now;
	for (; now<timeout && !(dev->irq_mask&1); now=tsc_read_ns())
		cpu_relax();

	return !!dev->irq_mask; /* event_happened_within_timeout?1:0 */
}

u32 ivshmem_read_device( struct ivshmem_dev *dev ) {
	/* return the new count of signal-0 events */
	if ( dev->irq_mask & (1 << 0) ) {
		dev->irq_mask &= ~(1 << 0);
		return 1;
	} else
		return 0;
}

bool ivshmem_irq_handler( struct ivshmem_dev *dev, unsigned int irq ) {
	if (irq >= (dev->irq_base) && irq < dev->irq_base + (dev->has_msix ? IVSHMEM_MAX_VECTORS : 1)) {
		unsigned int n = irq - dev->irq_base;
		dev->int_count[n]++;
	//	printk("IVSHMEM-IRQ: %d -> %08llx /%d\n",n, dev->state_change_mask,dev->peers);
		dev->irq_mask |= 1 << n;
		return true;
	} else
		return false;
}

void ivshmem_finalize( struct ivshmem_dev *dev ) {
	/* all cleanup is done by cell destruction */
	//ivshmem_set_state( dev, 0 );
}

unsigned long process_clock_ns( void ) {
	return tsc_read_ns();
}

unsigned long rtc_read_ns(void) {
	return tsc_read_ns();
}

