/*
 * tau_xservice_client.c
 *
 *  Created on: 10.09.2019
 *      Author: thorsten
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "trdpSvc.h"

#ifdef JAILHOUSE

/* TODO this needs better wrapping */
static struct ivshmem_dev *shmdev;

static volatile int tick = 0;
static int apic_vector;

static u64 expected_time;
static u64 min = -1, max, delta;
static u64 period = 50 * NS_PER_MSEC;


static void jailhouse_irq_handler(unsigned int irq) {
//	printk("IRQ: %d\n", irq);
	if (irq == apic_vector) { /* APIC */

		delta = tsc_read_ns() - expected_time;
		if (delta < min) min = delta;
		if (delta > max) max = delta;

		expected_time += period;
		apic_timer_set(expected_time - tsc_read_ns());
		tick++;
		
	} else 
		ivshmem_irq_handler( shmdev, irq);

}
#define CONST_PAGE_SIZE PAGE_SIZE
#else
#define CONST_PAGE_SIZE 0x1000
#endif

int tau_xservice_connect(
		const char *arg,
		const uint8_t *xTypeMap, const char * const *iface,
		size_t msize,  u8 *shm_addr, struct shm_data *sd) {

	struct ivshmem_cb _cb, *cb = &_cb;
	int devid[2] = { SHM_DEFAULT_NUMBER, TRDPSVC_DEFAULT_SHM_CLIENT_NUMBER };

	if ( !iface || !sd || !msize || 0>	ivshmem_parse_ids(arg, devid, 2, '.')) return 1;
	memset(sd, 0, sizeof(*sd));

	msize += CONST_PAGE_SIZE-1;
	msize &= ~(CONST_PAGE_SIZE-1);
	sd->target = devid[1];

#ifdef JAILHOUSE
	cb->irq_base = cmdline_parse_int("irq_base", DEFAULT_IRQ_BASE);
/* this is a bit of an interwoven cross-dependency, missing a better approach. I need to reserve an IRQ-line for the
   APIC timer that we also require somehow. */
	apic_vector = cb->irq_base++;
	shmdev = &sd->dev;
	irq_init(jailhouse_irq_handler);
	enable_irqs();
#else

#ifdef PIKEOS_NATIVE
	cb->vmem = shm_addr;
#else
	cb = NULL;
#endif

	ivshmem_reserve( msize );
#endif
	/* if there is a mis-match in size required by tau_xsession and the JH pre-configured shm-size, this will crash */

	ivshmem_open( &sd->dev, devid[0], cb);

	if (sd->target == sd->dev.id || sd->target >= sd->dev.peers)
		error(-1, 22 /*EINVAL*/, "Invalid TRDP-peer ID (%d) detected (self = %d/%d)", sd->target, sd->dev.id, sd->dev.peers);

	sd->in  =((const char *)(sd->dev.in))+(sd->dev.out_size * sd->target);
	sd->mem = (struct shm_struct *)sd->dev.out;
	sd->out = (char *)(sd->mem+1);

	sd->mem->magic = TRDP_XSVC_MAGIC;
	sd->mem->app = 0;
	sd->mem->round = 0;
	sd->mem->sizeIf  = MAX_INTERFACES;
	sd->mem->sizeIn  = 0;
	sd->mem->sizeOut = 0;
	for (int i=0; i<MAX_INTERFACES && *iface; i++, iface++) {
		memcpy(sd->mem->xnetif[i], *iface, MAX_INTERFACE_LENGTH);
	}
	memcpy(sd->mem->xtype_map, xTypeMap, TAU_XTYPE_MAP_SIZE);

	print("TAU_XService: SHM prepared for TRDP service at peer %d.\n", sd->target);

	sd->ts1 = tsc_read_ns();
	sd->tproc = sd->ts1;
	return 0;
}

/* deprecated */
toSt tau_xservice_sync_setup( struct shm_data *sd ) {
	toSt ivst;
	do {
		ivshmem_set_state( &sd->dev, sd->state );
	} while ( STATE_CHANGED_OK != (ivst = ivshmem_expect_state( &sd->dev, IVSHMEM_MSEC(200), sd->target, sd->state, &sd->state )) );

	sd->state = 0;
	sd->ts1 = tsc_read_ns();
	sd->tproc = sd->ts1;

	return STATE_CHANGED_OK;
}

toSt tau_xservice_sync_getcom( struct shm_data *sd, int64_t before_ns ) {
	/* if we did NOT send the getCom()-token in the last write, do it now. */
	if (!(sd->state & TRDP_XSVC_GETCOM)) {
		sd->state &=~TRDP_XSVC_SETCOM; /* erase it */
		sd->state |= TRDP_XSVC_GETCOM; /* pass the read-token to trdp-svc */
		sd->dev.state_change_mask &=~(1 << sd->target); /* clear the mask first of old notifications */
		ivshmem_set_state( &sd->dev, sd->state );
	}

	toSt ivst = ivshmem_expect_state( &sd->dev, before_ns, sd->target, sd->state, &sd->state);
	if (ivst == STATE_CHANGED_BAD && ((sd->state & 0xF) == TRDP_XSVC_ALIVE)) {
		/* the other side just initialized, and is not in sync, be a little more generous in that case: */
		ivst = STATE_TIMEOUT;
		sd->state &=~TRDP_XSVC_ALIVE;
		print("tau_xsvc.getcom():: Svc announced init.\n");
		sd->state += 0x100; /* since we got something back */
	} else if (ivst == STATE_CHANGED_OK) {
		sd->state += 0x100; /* inc counter, if we got the token back. */
//	} else {
//		sd->state &=~TRDP_XSVC_GETCOM;
	}
	return ivst;
}

int64_t tau_xservice_take_time_for_app( struct shm_data *sd ) {
	sd->mem->app = -process_clock_ns();
	sd->ts0 = sd->ts1;
	sd->ts1 = tsc_read_ns();
	return (sd->ts1 - sd->ts0)/NS_PER_MSEC;
}

void tau_xservice_time_after_app( struct shm_data *sd ) {
	sd->mem->round = -sd->tproc;
	sd->tproc = process_clock_ns();
	sd->mem->round += sd->tproc;
	sd->mem->app   += sd->tproc;
}

toSt tau_xservice_sync_cycle( struct shm_data *sd, int both_locks, int64_t before_ns) {
	/* pass the send and the rcv -token to trdp-svc */
	sd->state &=~TRDP_XSVC_CYCLE;
	sd->state |= both_locks ? TRDP_XSVC_CYCLE : TRDP_XSVC_SETCOM;

//	print("sync-cycle: >>%x\n", sd->state);
	ivshmem_set_state( &sd->dev, sd->state );

	/* if we were for setCom() only , this would also return immediately */
	toSt ivst = ((sd->state & TRDP_XSVC_CYCLE) == TRDP_XSVC_SETCOM) ?
		  ivshmem_expect_state( &sd->dev, before_ns, sd->target, sd->state, &sd->state)
		/* do not wait for the return of the token -- it is helt until sync_getcom */
		: STATE_CHANGED_OK;
	if (ivst == STATE_CHANGED_BAD && ((sd->state & 0xF) == TRDP_XSVC_ALIVE)) {
		/* the other side just initialized, and is not in sync, be a little more generous in that case: */
		ivst = STATE_TIMEOUT;
		sd->state &=~TRDP_XSVC_ALIVE;
		print("tau_xsvc.cycle():: Svc announced init.\n");
		sd->state += 0x100; /* since we got something back */
	} else if (ivst == STATE_CHANGED_OK && ((sd->state & TRDP_XSVC_CYCLE) == TRDP_XSVC_SETCOM)) {
		sd->state += 0x100; /* inc counter, if we got the token back. */
//	} else {
//		sd->state &=~TRDP_XSVC_SETCOM;
	}
	return ivst;
}
