DEB_HOST_MULTIARCH ?= x86_64-linux-gnu
ifdef ELINOS_TARGET
ARCH = $(ELINOS_TARGET)
else
ARCH = $(DEB_HOST_MULTIARCH)
endif
TRDP_BUILD = $(TRDP_BUILD_BASE)/$(ARCH)-rel
LDEPS += $(TRDP_BUILD)/libtrdpap.a
LDLIBS_TRDP     = -ltrdpap -luuid -lrt
TRDP_LDPATHS    = $(TRDP_BUILD)

TRDP_SRC        = $(LOCAL_TRDP)/src
TRDP_HDR_API    = $(TRDP_SRC)/vos/api $(TRDP_SRC)/api
TRDP_HDR_COMMON = $(TRDP_SRC)/common

$(TRDP_BUILD)/libtrdpap.a: libtrdp-clean
	make -C $(LOCAL_TRDP) DEB_config BUILD="$(abspath $(TRDP_BUILD_BASE))"
	make -C $(LOCAL_TRDP) libtrdpap  BUILD="$(abspath $(TRDP_BUILD_BASE))"

#nevertheless build the lib-package for gleis-gui and the wireshark dissector, optional
libtrdp-deb-pkg:
	make -C $(LOCAL_TRDP) deb-pkg BUILD="$(abspath $(TRDP_BUILD_BASE))"

libtrdp-clean:
	make -C $(LOCAL_TRDP) clean BUILD="$(abspath $(TRDP_BUILD_BASE))"

libtrdp-distclean:
	$(RM) -r "$(TRDP_BUILD_BASE)"

distclean: libtrdp-distclean

libtrdp: $(TRDP_BUILD)/libtrdpap.a

.PHONY: libtrdp libtrdp-distclean libtrdp-clean libtrdp-deb-pkg

