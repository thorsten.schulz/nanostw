/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sign_finalize_nacl_sign_H_
#define _sign_finalize_nacl_sign_H_

#include "kcg_types.h"
#include "sign_finalize_it1_nacl_sign.h"
#include "modL_nacl_sign.h"
#include "finalize_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::sign_finalize/ */
extern void sign_finalize_nacl_sign(
  /* _L31/, lastPart/ */
  StreamChunk_slideTypes *lastPart,
  /* _L28/, signin/ */
  prependAkku_nacl_sign *signin,
  /* _L37/, r/ */
  array_uint32_8 *r,
  /* _L33/, signature/ */
  Signature_nacl_sign *signature);



#endif /* _sign_finalize_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_finalize_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

