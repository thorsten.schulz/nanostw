/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10_H_
#define _block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10_H_

#include "kcg_types.h"
#include "block32_it2_hash_libsha_2_13_22_6_11_25.h"
#include "block32_it3_hash_libsha_7_18_3_17_19_10.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::block32_it1/ */
extern void block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10(
  /* _L2/, ain/ */
  array_uint32_8 *ain_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L3/, w/ */
  array_uint32_16 *w_2_13_22_6_11_25_7_18_3_17_19_10,
  /* K/, _L17/ */
  array_uint32_16 *K_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L6/, aout/ */
  array_uint32_8 *aout_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L5/, woo/ */
  array_uint32_16 *woo_2_13_22_6_11_25_7_18_3_17_19_10);



#endif /* _block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

