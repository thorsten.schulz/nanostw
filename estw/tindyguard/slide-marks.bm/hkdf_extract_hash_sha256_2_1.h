/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hkdf_extract_hash_sha256_2_1_H_
#define _hkdf_extract_hash_sha256_2_1_H_

#include "kcg_types.h"
#include "HMAC_hash_sha256_2_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::hkdf_extract/ */
extern void hkdf_extract_hash_sha256_2_1(
  /* _L9/, ikmlen/ */
  size_slideTypes ikmlen_2_1,
  /* _L8/, ikm/ */
  array_uint32_16_2 *ikm_2_1,
  /* _L1/, _L10/, saltlen/ */
  size_slideTypes saltlen_2_1,
  /* _L5/, salt/ */
  array_uint32_16_1 *salt_2_1,
  /* _L7/, prk/ */
  StreamChunk_slideTypes *prk_2_1);



#endif /* _hkdf_extract_hash_sha256_2_1_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_extract_hash_sha256_2_1.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

