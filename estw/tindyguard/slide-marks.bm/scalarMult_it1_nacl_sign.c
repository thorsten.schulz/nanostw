/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarMult_it1_nacl_sign.h"

/* nacl::sign::scalarMult_it1/ */
void scalarMult_it1_nacl_sign(
  /* _L2/, akku/ */
  pq_pair_nacl_sign *akku,
  /* _L3/, s/ */
  kcg_uint8 s,
  /* _L1/, akkoo/ */
  pq_pair_nacl_sign *akkoo)
{
  pq_pair_nacl_sign acc;
  kcg_size idx;

  kcg_copy_pq_pair_nacl_sign(akkoo, akku);
  /* _L1= */
  for (idx = 0; idx < 8; idx++) {
    kcg_copy_pq_pair_nacl_sign(&acc, akkoo);
    /* _L1=(nacl::sign::scalarMult_it2#1)/ */
    scalarMult_it2_nacl_sign(/* _L1= */(kcg_uint8) idx, &acc, s, akkoo);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarMult_it1_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

