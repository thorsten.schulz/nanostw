/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "stream_it_hash_sha512.h"

/* hash::sha512::stream_it/ */
void stream_it_hash_sha512(
  /* i/ */
  size_slideTypes i,
  /* zin/ */
  hashAkkuWithChunk_hash_sha512 *zin,
  /* m/ */
  StreamChunk_slideTypes *m,
  /* goOn/ */
  kcg_bool *goOn,
  /* zout/ */
  hashAkkuWithChunk_hash_sha512 *zout)
{
  array_uint32_32 tmp;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock;

  IfBlock1_clock = i % kcg_lit_int32(2) == kcg_lit_int32(0);
  /* IfBlock1: */
  if (IfBlock1_clock) {
    *goOn = (*zin).length != i * StreamChunkBytes_slideTypes +
      StreamChunkBytes_slideTypes + StreamChunkBytes_slideTypes;
    kcg_copy_hashAkkuWithChunk_hash_sha512(zout, zin);
    kcg_copy_StreamChunk_slideTypes(&tmp[0], &(*zin).porch);
    kcg_copy_StreamChunk_slideTypes(&tmp[16], m);
    /* IfBlock1:then:_L1=(hash::sha512::block_refine#1)/ */
    block_refine_hash_sha512(&(*zin).z, &tmp, &(*zout).z);
    kcg_copy_StreamChunk_slideTypes(
      &(*zout).porch,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  else {
    *goOn = (*zin).length / hBlockBytes_hash_sha512 > i / kcg_lit_int32(2) +
      kcg_lit_int32(1);
    kcg_copy_hashAkkuWithChunk_hash_sha512(zout, zin);
    kcg_copy_StreamChunk_slideTypes(&(*zout).porch, m);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** stream_it_hash_sha512.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

