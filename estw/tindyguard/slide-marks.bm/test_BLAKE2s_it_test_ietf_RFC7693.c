/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_BLAKE2s_it_test_ietf_RFC7693.h"

/* test::ietf_RFC7693::test_BLAKE2s_it/ */
kcg_bool test_BLAKE2s_it_test_ietf_RFC7693(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L4/, tc/ */
  testcase_test_ietf_RFC7693 *tc)
{
  HashChunk_hash_blake2s tmp;
  /* _L3/, failed/ */
  kcg_bool failed;

  /* _L1=(hash::blake2s::hashChunk#1)/ */
  hashChunk_hash_blake2s_2(&(*tc).msg, (*tc).mlen, (*tc).klen, &tmp);
  failed = failing || !kcg_comp_HashChunk_hash_blake2s(&tmp, &(*tc).exp);
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_BLAKE2s_it_test_ietf_RFC7693.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

