/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _Ch_hash_libsha_uint32_H_
#define _Ch_hash_libsha_uint32_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::Ch/ */
extern kcg_uint32 Ch_hash_libsha_uint32(
  /* _L1/, x/ */
  kcg_uint32 x_uint32,
  /* _L6/, y/ */
  kcg_uint32 y_uint32,
  /* _L2/, z/ */
  kcg_uint32 z_uint32);



#endif /* _Ch_hash_libsha_uint32_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Ch_hash_libsha_uint32.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

