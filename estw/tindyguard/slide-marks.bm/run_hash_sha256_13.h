/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _run_hash_sha256_13_H_
#define _run_hash_sha256_13_H_

#include "kcg_types.h"
#include "finalize_hash_sha256.h"
#include "stream_it_hash_sha256.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::run/ */
extern void run_hash_sha256_13(
  /* _L13/, _L24/, msg/ */
  array_uint32_16_13 *msg_13,
  /* _L10/, _L14/, _L28/, len/ */
  size_slideTypes len_13,
  /* finalbits/ */
  kcg_uint32 finalbits_13,
  /* finalbitsLen/ */
  size_slideTypes finalbitsLen_13,
  /* _L3/, hash/ */
  StreamChunk_slideTypes *hash_13);



#endif /* _run_hash_sha256_13_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** run_hash_sha256_13.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

