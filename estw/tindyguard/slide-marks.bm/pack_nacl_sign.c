/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pack_nacl_sign.h"

/* nacl::sign::pack/ */
void pack_nacl_sign(
  /* _L2/, p/ */
  cpoint_nacl_op *p,
  /* _L14/, t/ */
  array_uint32_8 *t)
{
  gf_nacl_op tmp;
  /* _L1/ */
  gf_nacl_op _L1;
  /* _L13/ */
  array_uint32_8 _L13;

  /* _L1=(nacl::op::inv25519#1)/ */ inv25519_nacl_op(&(*p)[2], &_L1);
  /* _L12=(nacl::op::M#2)/ */ M_nacl_op(&(*p)[1], &_L1, &tmp);
  /* _L13=(nacl::op::pack25519#1)/ */ pack25519_nacl_op(&tmp, &_L13);
  kcg_copy_array_uint32_8(t, &_L13);
  /* _L11=(nacl::op::M#1)/ */ M_nacl_op(&(*p)[0], &_L1, &tmp);
  (*t)[7] = kcg_lsl_uint32(
      /* _L15=(nacl::op::par25519#1)/ */ par25519_nacl_op(&tmp),
      kcg_lit_uint32(31)) ^ _L13[7];
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

