/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "block32_it2_hash_libsha_2_13_22_6_11_25.h"

/* hash::libsha::block32_it2/ */
void block32_it2_hash_libsha_2_13_22_6_11_25(
  /* _L1/, _L37/, ain/ */
  array_uint32_8 *ain_2_13_22_6_11_25,
  /* _L13/, w/ */
  kcg_uint32 w_2_13_22_6_11_25,
  /* _L12/, k/ */
  kcg_uint32 k_2_13_22_6_11_25,
  /* _L36/, aout/ */
  array_uint32_8 *aout_2_13_22_6_11_25)
{
  /* _L3/ */
  kcg_uint32 _L3_2_13_22_6_11_25;

  (*aout_2_13_22_6_11_25)[1] = (*ain_2_13_22_6_11_25)[0];
  (*aout_2_13_22_6_11_25)[2] = (*ain_2_13_22_6_11_25)[1];
  (*aout_2_13_22_6_11_25)[3] = (*ain_2_13_22_6_11_25)[2];
  (*aout_2_13_22_6_11_25)[5] = (*ain_2_13_22_6_11_25)[4];
  (*aout_2_13_22_6_11_25)[6] = (*ain_2_13_22_6_11_25)[5];
  (*aout_2_13_22_6_11_25)[7] = (*ain_2_13_22_6_11_25)[6];
  _L3_2_13_22_6_11_25 = w_2_13_22_6_11_25 + k_2_13_22_6_11_25 +
    (*ain_2_13_22_6_11_25)[7] + /* _L4=(hash::libsha::SIGMA32#2)/ */
    SIGMA32_hash_libsha_6_11_25((*ain_2_13_22_6_11_25)[4]) +
    /* _L6=(hash::libsha::Ch#1)/ */
    Ch_hash_libsha_uint32(
      (*ain_2_13_22_6_11_25)[4],
      (*ain_2_13_22_6_11_25)[5],
      (*ain_2_13_22_6_11_25)[6]);
  (*aout_2_13_22_6_11_25)[0] = _L3_2_13_22_6_11_25 +
    /* _L26=(hash::libsha::SIGMA32#3)/ */
    SIGMA32_hash_libsha_2_13_22((*ain_2_13_22_6_11_25)[0]) +
    /* _L22=(hash::libsha::Maj#1)/ */
    Maj_hash_libsha_uint32(
      (*ain_2_13_22_6_11_25)[0],
      (*ain_2_13_22_6_11_25)[1],
      (*ain_2_13_22_6_11_25)[2]);
  (*aout_2_13_22_6_11_25)[4] = _L3_2_13_22_6_11_25 + (*ain_2_13_22_6_11_25)[3];
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block32_it2_hash_libsha_2_13_22_6_11_25.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

