/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sigma32_hash_libsha_7_18_3.h"

/* hash::libsha::sigma32/ */
kcg_uint32 sigma32_hash_libsha_7_18_3(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, _L1/, x/ */
  kcg_uint32 x_7_18_3)
{
  /* _L6/, ret/ */
  kcg_uint32 ret_7_18_3;

  ret_7_18_3 = ((x_7_18_3 >> kcg_lit_uint32(7)) | kcg_lsl_uint32(
        x_7_18_3,
        kcg_lit_uint32(25))) ^ ((x_7_18_3 >> kcg_lit_uint32(18)) |
      kcg_lsl_uint32(x_7_18_3, kcg_lit_uint32(14))) ^ (x_7_18_3 >>
      kcg_lit_uint32(3));
  return ret_7_18_3;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sigma32_hash_libsha_7_18_3.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

