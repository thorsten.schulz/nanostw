/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _modL_nacl_sign_H_
#define _modL_nacl_sign_H_

#include "kcg_types.h"
#include "modL_it3_nacl_sign.h"
#include "modL_it1_nacl_sign.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::modL/ */
extern void modL_nacl_sign(
  /* _L1/, x/ */
  array_int64_64 *x,
  /* @1/_L24/, @1/u32/, _L25/, rout/ */
  array_uint32_8 *rout);

/*
  Expanded instances for: nacl::sign::modL/
  @1: (slideTypes::ld32x8#2)
*/

#endif /* _modL_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

