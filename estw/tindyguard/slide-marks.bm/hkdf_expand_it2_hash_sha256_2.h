/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hkdf_expand_it2_hash_sha256_2_H_
#define _hkdf_expand_it2_hash_sha256_2_H_

#include "kcg_types.h"
#include "HMAC_hash_sha256_2_1.h"
#include "truncate_slideTypes.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::hkdf_expand_it2/ */
extern void hkdf_expand_it2_hash_sha256_2(
  /* @1/_L1/, @1/i/, _L48/, _L66/, _L7/, i/ */
  size_slideTypes i_2,
  /* _L26/, hin/ */
  array_uint32_8 *hin_2,
  /* _L3/, prk/ */
  StreamChunk_slideTypes *prk_2,
  /* _L51/, m0/ */
  array_uint32_16_2 *m0_2,
  /* _L21/, _L36/, m/ */
  array_uint32_16_2 *m_2,
  /* _L29/, _L43/, infolen/ */
  size_slideTypes infolen_2,
  /* _L13/, okmlen/ */
  size_slideTypes okmlen_2,
  /* _L54/, orig/ */
  kcg_uint32 orig_2,
  /* x/ */
  size_slideTypes x_2,
  /* y/ */
  size_slideTypes y_2,
  /* _L58/, _L60/, z/ */
  kcg_uint32 z_2,
  /* _L12/, goOn/ */
  kcg_bool *goOn_2,
  /* _L20/, hoo/ */
  array_uint32_8 *hoo_2,
  /* _L16/, okm/ */
  StreamChunk_slideTypes *okm_2);

/*
  Expanded instances for: hash::sha256::hkdf_expand_it2/
  @1: (M::inc#1)
*/

#endif /* _hkdf_expand_it2_hash_sha256_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_expand_it2_hash_sha256_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

