/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _HMAC_hash_sha256_3_1_H_
#define _HMAC_hash_sha256_3_1_H_

#include "kcg_types.h"
#include "run_hash_sha256_4.h"
#include "run_hash_sha256_2.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::HMAC/ */
extern void HMAC_hash_sha256_3_1(
  /* _L8/, msg/ */
  array_uint32_16_3 *msg_3_1,
  /* _L9/, mlen/ */
  int_slideTypes mlen_3_1,
  /* IfBlock1:else:_L1/, key/ */
  array_uint32_16_1 *key_3_1,
  /* klen/ */
  size_slideTypes klen_3_1,
  /* _L20/, hmac/ */
  StreamChunk_slideTypes *hmac_3_1);



#endif /* _HMAC_hash_sha256_3_1_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** HMAC_hash_sha256_3_1.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

