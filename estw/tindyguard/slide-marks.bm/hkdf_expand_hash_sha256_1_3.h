/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hkdf_expand_hash_sha256_1_3_H_
#define _hkdf_expand_hash_sha256_1_3_H_

#include "kcg_types.h"
#include "hkdf_expand_it2_hash_sha256_2.h"
#include "prependData_slideTypes_8.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::hkdf_expand/ */
extern void hkdf_expand_hash_sha256_1_3(
  /* _L24/, prk/ */
  StreamChunk_slideTypes *prk_1_3,
  /* _L26/, _L40/, _L72/, infolen/ */
  size_slideTypes infolen_1_3,
  /* _L39/, info/ */
  array_uint32_16_1 *info_1_3,
  /* _L1/, _L11/, okmlen/ */
  size_slideTypes okmlen_1_3,
  /* _L33/, okm/ */
  array_uint32_16_3 *okm_1_3,
  /* _L9/, fail/ */
  kcg_bool *fail_1_3);



#endif /* _hkdf_expand_hash_sha256_1_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_expand_hash_sha256_1_3.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

