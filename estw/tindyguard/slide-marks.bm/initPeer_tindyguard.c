/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "initPeer_tindyguard.h"

/* tindyguard::initPeer/ */
void initPeer_tindyguard(
  /* @1/_L24/, @1/pub/, _L14/, _L26/, pub/ */
  pub_tindyguardTypes *pub,
  /* _L15/, preshared/ */
  secret_tindyguardTypes *preshared,
  /* _L16/, endpoint/ */
  peer_t_udp *endpoint,
  /* _L17/, allowed/ */
  range_t_udp *allowed,
  /* _L12/, p/ */
  Peer_tindyguardTypes *p)
{
  array_uint32_16 tmp;
  array_uint32_8 tmp1;
  /* @1/_L19/ */
  XNonce_nacl_core_chacha _L19_initHashes_2;
  /* @2/_L46/, @3/_L46/ */
  array_uint32_7 _L46_single_2_initHashes_2;
  kcg_size idx;

  kcg_copy_pub_tindyguardTypes(&(*p).tpub, pub);
  kcg_copy_psk_tindyguardTypes(&(*p).preshared, preshared);
  kcg_copy_range_t_udp(&(*p).allowed, allowed);
  kcg_copy_peer_t_udp(&(*p).endpoint, endpoint);
  (*p).timedOut = kcg_false;
  (*p).bestSession = nil_tindyguard_session;
  (*p).inhibitInit = kcg_false;
  kcg_copy_CookieJar_tindyguardTypes(
    &(*p).cookie,
    (CookieJar_tindyguardTypes *) &EmptyJar_tindyguardTypes);
  for (idx = 0; idx < 6; idx++) {
    _L19_initHashes_2[idx] = kcg_lit_uint32(0);
  }
  kcg_copy_array_uint32_7(
    &_L46_single_2_initHashes_2,
    (array_uint32_7 *) &IV_hash_blake2s[1]);
  kcg_copy_array_uint32_2(&tmp[0], (array_uint32_2 *) &MAC1_label_tindyguard);
  kcg_copy_pub_tindyguardTypes(&tmp[2], pub);
  kcg_copy_XNonce_nacl_core_chacha(&tmp[10], &_L19_initHashes_2);
  /* @1/_L18=(hash::blake2s::singleChunk#2)/ */
  singleChunk_hash_blake2s(&tmp, kcg_lit_int32(40), &(*p).hcache.salt);
  tmp1[0] = kcg_lit_uint32(1795745351);
  kcg_copy_array_uint32_7(&tmp1[1], &_L46_single_2_initHashes_2);
  kcg_copy_array_uint32_8(
    &tmp[0],
    (array_uint32_8 *) &CONSTRUCTION_IDENTIFIER_hash_tindyguard);
  kcg_copy_pub_tindyguardTypes(&tmp[8], pub);
  /* @4/_L2=(hash::blake2s::block_refine#1)/ */
  block_refine_hash_blake2s(
    &tmp1,
    kcg_lit_int32(64),
    &tmp,
    kcg_false,
    &(*p).hcache.hash1);
  tmp1[0] = kcg_lit_uint32(1795745351);
  kcg_copy_array_uint32_7(&tmp1[1], &_L46_single_2_initHashes_2);
  kcg_copy_array_uint32_2(&tmp[0], (array_uint32_2 *) &COOKIE_label_tindyguard);
  kcg_copy_pub_tindyguardTypes(&tmp[2], pub);
  kcg_copy_XNonce_nacl_core_chacha(&tmp[10], &_L19_initHashes_2);
  /* @5/_L2=(hash::blake2s::block_refine#1)/ */
  block_refine_hash_blake2s(
    &tmp1,
    kcg_lit_int32(40),
    &tmp,
    kcg_false,
    &(*p).hcache.cookieHKey);
  for (idx = 0; idx < 3; idx++) {
    (*p).tai[idx] = kcg_lit_uint32(0);
  }
}

/*
  Expanded instances for: tindyguard::initPeer/
  @1: (tindyguard::initHashes#2)
  @2: @1/(hash::blake2s::single#2)
  @3: @1/(hash::blake2s::single#3)
  @4: @2/(hash::blake2s::stream_it#1)
  @5: @3/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initPeer_tindyguard.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

