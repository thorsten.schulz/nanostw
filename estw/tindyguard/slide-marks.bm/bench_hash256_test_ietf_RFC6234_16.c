/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_hash256_test_ietf_RFC6234_16.h"

/* test::ietf_RFC6234::bench_hash256/ */
kcg_bool bench_hash256_test_ietf_RFC6234_16(
  /* _L1/, failing/ */
  kcg_bool failing_16,
  /* tc/ */
  simpleTC_test *tc_16)
{
  StreamChunk_slideTypes tmp;
  /* _L10/ */
  array_uint32_16_16 _L10_16;
  kcg_size idx;
  /* _L2/, failed/ */
  kcg_bool failed_16;

  for (idx = 0; idx < 16; idx++) {
    kcg_copy_StreamChunk_slideTypes(&_L10_16[idx], &(*tc_16).m[0]);
  }
  /* _L19=(hash::sha256::run#1)/ */
  run_hash_sha256_16(
    &_L10_16,
    (*tc_16).mlen + kcg_lit_int32(960),
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &tmp);
  failed_16 = failing_16 || kcg_comp_StreamChunk_slideTypes(
      &tmp,
      (array_uint32_16 *) &ZeroChunk_slideTypes);
  return failed_16;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_hash256_test_ietf_RFC6234_16.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

