/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _HMAC_hash_sha256_12_2_H_
#define _HMAC_hash_sha256_12_2_H_

#include "kcg_types.h"
#include "run_hash_sha256_2.h"
#include "run_hash_sha256_13.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::HMAC/ */
extern void HMAC_hash_sha256_12_2(
  /* _L8/, msg/ */
  array_uint32_16_12 *msg_12_2,
  /* _L9/, mlen/ */
  int_slideTypes mlen_12_2,
  /* key/ */
  array_uint32_16_2 *key_12_2,
  /* klen/ */
  size_slideTypes klen_12_2,
  /* _L20/, hmac/ */
  StreamChunk_slideTypes *hmac_12_2);



#endif /* _HMAC_hash_sha256_12_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** HMAC_hash_sha256_12_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

