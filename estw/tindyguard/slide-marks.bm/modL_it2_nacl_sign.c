/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "modL_it2_nacl_sign.h"

/* nacl::sign::modL_it2/ */
void modL_it2_nacl_sign(
  /* @1/IRIO_Input/, @1/_L3/, _L22/, _L8/, it/ */
  int_slideTypes it,
  /* _L26/, _L9/, carry/ */
  kcg_int64 carry,
  /* _L10/, _L28/, x/ */
  kcg_int64 x,
  /* _L24/, xi/ */
  kcg_int64 xi,
  /* _L12/, i/ */
  int_slideTypes i,
  /* _L18/, carryo/ */
  kcg_int64 *carryo,
  /* _L19/, xo/ */
  kcg_int64 *xo)
{
  /* _L38/ */
  kcg_int32 _L38;
  /* @2/_L1/, @2/i/, _L30/ */
  kcg_int64 _L30;
  /* _L29/ */
  kcg_int64 _L29;
  /* @1/A/, @1/_L12/, _L14/ */
  kcg_int32 _L14;
  /* @1/IRIO_Output/, @1/_L10/, _L13/ */
  kcg_bool _L13;
  kcg_bool every_srs8_1;
  /* @2/_L4/ */
  kcg_uint64 _L4_srs8_1;
  /* @2/_L8/ */
  kcg_uint64 _L8_srs8_1;

  _L14 = i - kcg_lit_int32(32);
  _L13 = i - kcg_lit_int32(12) > it && it >= _L14;
  _L38 = it - _L14;
  if (kcg_lit_int32(0) <= _L38 && _L38 < kcg_lit_int32(32)) {
    _L30 = L_nacl_sign[_L38];
  }
  else {
    _L30 = kcg_lit_int64(0);
  }
  _L29 = x + carry + kcg_lit_int64(-16) * xi * _L30;
  _L30 = kcg_lit_int64(128) + _L29;
  _L4_srs8_1 = /* @2/_L2= */(kcg_uint64) _L30 >> kcg_lit_uint64(8);
  every_srs8_1 = _L30 < kcg_lit_int64(0);
  if (every_srs8_1) {
    _L8_srs8_1 = kcg_lit_uint64(0xFF00000000000000) | _L4_srs8_1;
  }
  else {
    _L8_srs8_1 = _L4_srs8_1;
  }
  /* _L18= */
  if (_L13) {
    *carryo = /* @2/_L3= */(kcg_int64) _L8_srs8_1;
    *xo = _L29 - /* _L45= */(kcg_int64)
        kcg_lsl_uint64(
          /* _L44= */(kcg_uint64) /* @2/_L3= */(kcg_int64) _L8_srs8_1,
          kcg_lit_uint64(8));
  }
  else {
    *carryo = carry;
    *xo = x;
  }
}

/*
  Expanded instances for: nacl::sign::modL_it2/
  @1: (math::InRangeInOut#1)
  @2: (nacl::op::srs8#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_it2_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

