/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdfChunk1_hash_blake2s_1.h"

/* hash::blake2s::hkdfChunk1/ */
void hkdfChunk1_hash_blake2s_1(
  /* @1/_L4/, @1/ikm/, _L4/, ikm/ */
  array_uint32_16_1 *ikm_1,
  /* @1/_L5/, @1/ikmlen/, _L5/, ikmlen/ */
  size_slideTypes ikmlen_1,
  /* @1/_L3/, @1/chainingKey/, _L3/, chainingKey/ */
  HashChunk_hash_blake2s *chainingKey_1,
  /* _L22/, cKout/ */
  HashChunk_hash_blake2s *cKout_1)
{
  /* @2/IfBlock1: */
  kcg_bool IfBlock1_clock_hkdf_expand_it_chunk_2_hkdfChunk_1;
  /* @2/IfBlock1:else:_L9/ */
  array_uint32_16_1 _L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1;
  /* @2/IfBlock1:then:_L10/ */
  array_uint32_16_1 _L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1;
  /* @1/_L14/, @1/okm/, _L21/ */
  array_uint32_16_1 _L21_1;
  /* @1/_L18/ */
  HashChunk_hash_blake2s _L18_hkdfChunk_1_1_1;

  /* @1/_L18=(hash::blake2s::hmacChunk#1)/ */
  hmacChunk_hash_blake2s_1(
    ikm_1,
    ikmlen_1,
    chainingKey_1,
    &_L18_hkdfChunk_1_1_1);
  IfBlock1_clock_hkdf_expand_it_chunk_2_hkdfChunk_1 = kcg_lit_uint32(0) ==
    kcg_lit_uint32(0);
  /* @2/IfBlock1: */
  if (IfBlock1_clock_hkdf_expand_it_chunk_2_hkdfChunk_1) {
    kcg_copy_StreamChunk_slideTypes(
      &_L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    _L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1[0][0] = kcg_lit_uint32(1);
    /* @2/IfBlock1:then:_L5=(hash::blake2s::hmacChunk#2)/ */
    hmacChunk_hash_blake2s_1(
      &_L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1,
      kcg_lit_int32(1),
      &_L18_hkdfChunk_1_1_1,
      &_L21_1[0]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &_L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    _L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1[0][8] = kcg_lit_uint32(
        0) + kcg_lit_uint32(1);
    /* @2/IfBlock1:else:_L5=(hash::blake2s::hmacChunk#3)/ */
    hmacChunk_hash_blake2s_1(
      &_L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1,
      kcg_lit_int32(33),
      &_L18_hkdfChunk_1_1_1,
      &_L21_1[0]);
  }
  kcg_copy_HashChunk_hash_blake2s(cKout_1, &_L21_1[0]);
}

/*
  Expanded instances for: hash::blake2s::hkdfChunk1/
  @1: (hash::blake2s::hkdfChunk#1)
  @2: @1/(hash::blake2s::hkdf_expand_it_chunk#2)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdfChunk1_hash_blake2s_1.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

