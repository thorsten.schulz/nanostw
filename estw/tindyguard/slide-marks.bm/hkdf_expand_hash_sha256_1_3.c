/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdf_expand_hash_sha256_1_3.h"

/* hash::sha256::hkdf_expand/ */
void hkdf_expand_hash_sha256_1_3(
  /* _L24/, prk/ */
  StreamChunk_slideTypes *prk_1_3,
  /* _L26/, _L40/, _L72/, infolen/ */
  size_slideTypes infolen_1_3,
  /* _L39/, info/ */
  array_uint32_16_1 *info_1_3,
  /* _L1/, _L11/, okmlen/ */
  size_slideTypes okmlen_1_3,
  /* _L33/, okm/ */
  array_uint32_16_3 *okm_1_3,
  /* _L9/, fail/ */
  kcg_bool *fail_1_3)
{
  kcg_size idx;
  kcg_bool cond_iterw;
  array_uint32_8 acc;
  array_uint32_8 noname;
  size_slideTypes _1_noname;
  /* _L50/ */
  kcg_int32 _L50_1_3;
  /* _L49/ */
  kcg_int32 _L49_1_3;
  /* _L48/ */
  kcg_uint32 _L48_1_3;
  /* @1/_L1/, @1/i/, _L43/ */
  kcg_int32 _L43_1_3;
  /* _L42/ */
  kcg_uint32 _L42_1_3;
  /* _L64/, _L75/, m/ */
  array_uint32_16_2 _L75_1_3;
  /* _L65/, _L80/, _L81/, m0/ */
  array_uint32_16_2 _L80_1_3;
  /* _L83/ */
  kcg_int32 _L83_1_3;
  /* _L85/ */
  kcg_int32 _L85_1_3;
  /* _L89/ */
  kcg_int32 _L89_1_3;

  _L43_1_3 = infolen_1_3 / StreamChunkBytes_slideTypes;
  _L50_1_3 = infolen_1_3 % StreamChunkBytes_slideTypes;
  _L49_1_3 = _L50_1_3 / kcg_lit_int32(4);
  _L83_1_3 = _L49_1_3 + Length_hash_sha256;
  /* _L89= */
  if (_L83_1_3 >= StreamChunkLength_slideTypes) {
    _L89_1_3 = _L43_1_3 + kcg_lit_int32(1);
  }
  else {
    _L89_1_3 = _L43_1_3;
  }
  _L85_1_3 = _L83_1_3 % StreamChunkLength_slideTypes;
  /* _L42= */
  switch (_L50_1_3 % kcg_lit_int32(4)) {
    case kcg_lit_int32(0) :
      _L42_1_3 = kcg_lit_uint32(1);
      break;
    case kcg_lit_int32(1) :
      _L42_1_3 = kcg_lit_uint32(256);
      break;
    case kcg_lit_int32(2) :
      _L42_1_3 = kcg_lit_uint32(65536);
      break;
    default :
      _L42_1_3 = kcg_lit_uint32(16777216);
      break;
  }
  kcg_copy_StreamChunk_slideTypes(&_L80_1_3[0], &(*info_1_3)[0]);
  kcg_copy_StreamChunk_slideTypes(
    &_L80_1_3[1],
    (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  if (kcg_lit_int32(0) <= _L43_1_3 && _L43_1_3 < kcg_lit_int32(2) &&
    (kcg_lit_int32(0) <= _L49_1_3 && _L49_1_3 < kcg_lit_int32(16))) {
    _L48_1_3 = _L80_1_3[_L43_1_3][_L49_1_3];
    _L80_1_3[_L43_1_3][_L49_1_3] = _L48_1_3 + _L42_1_3;
  }
  else {
    _L48_1_3 = kcg_lit_uint32(0);
  }
  for (idx = 0; idx < 8; idx++) {
    noname[idx] = kcg_lit_uint32(0);
  }
  /* _L63= */
  for (idx = 0; idx < 2; idx++) {
    kcg_copy_array_uint32_8(&acc, &noname);
    /* _L63=(slideTypes::prependData#1)/ */
    prependData_slideTypes_8(&acc, &_L80_1_3[idx], &noname, &_L75_1_3[idx]);
  }
  *fail_1_3 = okmlen_1_3 <= kcg_lit_int32(0) || okmlen_1_3 > kcg_lit_int32(
      8160) || okmlen_1_3 > kcg_lit_int32(192) || infolen_1_3 < kcg_lit_int32(
      0);
  for (idx = 0; idx < 8; idx++) {
    noname[idx] = kcg_lit_uint32(0);
  }
  /* _L19= */
  if (*fail_1_3) {
    _1_noname = kcg_lit_int32(0);
  }
  else {
    /* _L19= */
    for (idx = 0; idx < 3; idx++) {
      kcg_copy_array_uint32_8(&acc, &noname);
      /* _L19=(hash::sha256::hkdf_expand_it2#1)/ */
      hkdf_expand_it2_hash_sha256_2(
        /* _L19= */(kcg_int32) idx,
        &acc,
        prk_1_3,
        &_L80_1_3,
        &_L75_1_3,
        infolen_1_3,
        okmlen_1_3,
        _L48_1_3,
        _L85_1_3,
        _L89_1_3,
        _L42_1_3,
        &cond_iterw,
        &noname,
        &(*okm_1_3)[idx]);
      _1_noname = /* _L19= */(kcg_int32) (idx + 1);
      /* _L19= */
      if (!cond_iterw) {
        break;
      }
    }
  }
#ifdef KCG_MAPW_CPY

  /* _L19= */
  for (idx = /* _L19= */(kcg_size) _1_noname; idx < 3; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &(*okm_1_3)[idx],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
#endif /* KCG_MAPW_CPY */

}

/*
  Expanded instances for: hash::sha256::hkdf_expand/
  @1: (M::inc#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_expand_hash_sha256_1_3.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

