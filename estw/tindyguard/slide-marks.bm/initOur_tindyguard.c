/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "initOur_tindyguard.h"

/* tindyguard::initOur/ */
void initOur_tindyguard(
  /* priv/ */
  Key32_slideTypes *priv,
  /* again/ */
  kcg_bool *again,
  /* salted/ */
  KeySalted_tindyguardTypes *salted,
  /* _L40/, failed/, failure/ */
  kcg_bool *failed,
  outC_initOur_tindyguard *outC)
{
  kcg_bool tmp;
  kcg_bool tmp1;
  array_uint32_16 tmp2;
  array_uint32_8 tmp3;
  array_uint32_16 tmp4;
  array_uint32_8 tmp5;
  array_uint32_16 tmp6;
  /* @2/_L19/ */
  XNonce_nacl_core_chacha _L19_initHashes_2;
  /* key_retry:makeKeys:_L1/, oneMore/ */
  kcg_bool oneMore_partial;
  /* @1/_/c/, key_retry:makeKeys:<2>, key_retry:makeKeys:_L3/, weak/ */
  kcg_bool _L3_makeKeys_key_retry;
  /* @1/_/o/, finalizer:, key_retry:makeKeys:<1> */
  kcg_bool finalizer_clock;
  /* key_retry: */
  SSM_ST_key_retry key_retry_state_act;
  /* tmp_key/ */
  KeyPair32_slideTypes tmp_key;
  kcg_size idx;

  key_retry_state_act = outC->key_retry_state_nxt;
  /* key_retry: */
  switch (key_retry_state_act) {
    case SSM_st_permanentWeakKey_key_retry :
      tmp1 = kcg_true;
      tmp = kcg_false;
      kcg_copy_KeyPair32_slideTypes(
        &tmp_key,
        (KeyPair32_slideTypes *) &ZeroKeyPair_slideTypes);
      *failed = kcg_true;
      outC->key_retry_reset_nxt = kcg_false;
      *again = kcg_false;
      outC->key_retry_state_nxt = SSM_st_permanentWeakKey_key_retry;
      break;
    case SSM_st_makeKeys_key_retry :
      if (outC->key_retry_reset_nxt) {
        outC->init = kcg_true;
        /* key_retry:makeKeys:_L1=(nacl::box::keyPair#2)/ */
        keyPair_reset_nacl_box_20(&outC->Context_keyPair_2);
      }
      /* @1/_/v4= */
      if (outC->init) {
        idx = 3;
      }
      else {
        idx = outC->v3_times_3_size;
      }
      /* key_retry:makeKeys:_L1=(nacl::box::keyPair#2)/ */
      keyPair_nacl_box_20(
        priv,
        &oneMore_partial,
        &tmp_key,
        &_L3_makeKeys_key_retry,
        &outC->Context_keyPair_2);
      /* @1/_/v3= */
      if (idx < 0) {
        outC->v3_times_3_size = idx;
      }
      else /* @1/_/v3= */
      if (_L3_makeKeys_key_retry) {
        outC->v3_times_3_size = idx - 1;
      }
      else {
        outC->v3_times_3_size = idx;
      }
      finalizer_clock = _L3_makeKeys_key_retry && outC->v3_times_3_size == 0;
      tmp = oneMore_partial;
      tmp1 = _L3_makeKeys_key_retry;
      outC->init = kcg_false;
      *again = oneMore_partial || _L3_makeKeys_key_retry;
      outC->key_retry_reset_nxt = finalizer_clock || _L3_makeKeys_key_retry;
      /* key_retry:makeKeys:<1> */
      if (finalizer_clock) {
        outC->key_retry_state_nxt = SSM_st_permanentWeakKey_key_retry;
        *failed = kcg_true;
      }
      else {
        outC->key_retry_state_nxt = SSM_st_makeKeys_key_retry;
        *failed = kcg_false;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  finalizer_clock = tmp || tmp1;
  /* finalizer: */
  if (finalizer_clock) {
    kcg_copy_KeySalted_tindyguardTypes(
      salted,
      (KeySalted_tindyguardTypes *) &EmptyKey_tindyguardTypes);
  }
  else {
    for (idx = 0; idx < 6; idx++) {
      _L19_initHashes_2[idx] = kcg_lit_uint32(0);
    }
    kcg_copy_KeyPair32_slideTypes(&(*salted).key, &tmp_key);
    kcg_copy_array_uint32_2(&tmp2[0], (array_uint32_2 *) &MAC1_label_tindyguard);
    kcg_copy_Key32_slideTypes(&tmp2[2], &tmp_key.pk_y);
    kcg_copy_XNonce_nacl_core_chacha(&tmp2[10], &_L19_initHashes_2);
    /* @2/_L18=(hash::blake2s::singleChunk#2)/ */
    singleChunk_hash_blake2s(&tmp2, kcg_lit_int32(40), &(*salted).hcache.salt);
    tmp3[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp3[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    kcg_copy_array_uint32_8(
      &tmp4[0],
      (array_uint32_8 *) &CONSTRUCTION_IDENTIFIER_hash_tindyguard);
    kcg_copy_Key32_slideTypes(&tmp4[8], &tmp_key.pk_y);
    /* @4/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp3,
      kcg_lit_int32(64),
      &tmp4,
      kcg_false,
      &(*salted).hcache.hash1);
    tmp5[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp5[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    kcg_copy_array_uint32_2(&tmp6[0], (array_uint32_2 *) &COOKIE_label_tindyguard);
    kcg_copy_Key32_slideTypes(&tmp6[2], &tmp_key.pk_y);
    kcg_copy_XNonce_nacl_core_chacha(&tmp6[10], &_L19_initHashes_2);
    /* @6/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp5,
      kcg_lit_int32(40),
      &tmp6,
      kcg_false,
      &(*salted).hcache.cookieHKey);
    for (idx = 0; idx < 2; idx++) {
      kcg_copy_CookieJar_tindyguardTypes(
        &(*salted).cookies[idx],
        (CookieJar_tindyguardTypes *) &EmptyJar_tindyguardTypes);
    }
  }
}

#ifndef KCG_USER_DEFINED_INIT
void initOur_init_tindyguard(outC_initOur_tindyguard *outC)
{
  outC->key_retry_reset_nxt = kcg_false;
  outC->init = kcg_true;
  outC->v3_times_3_size = 0;
  /* key_retry:makeKeys:_L1=(nacl::box::keyPair#2)/ */
  keyPair_init_nacl_box_20(&outC->Context_keyPair_2);
  outC->key_retry_state_nxt = SSM_st_makeKeys_key_retry;
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void initOur_reset_tindyguard(outC_initOur_tindyguard *outC)
{
  outC->key_retry_reset_nxt = kcg_false;
  outC->init = kcg_true;
  /* key_retry:makeKeys:_L1=(nacl::box::keyPair#2)/ */
  keyPair_reset_nacl_box_20(&outC->Context_keyPair_2);
  outC->key_retry_state_nxt = SSM_st_makeKeys_key_retry;
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/*
  Expanded instances for: tindyguard::initOur/
  @1: (times#3)
  @2: (tindyguard::initHashes#2)
  @3: @2/(hash::blake2s::single#2)
  @4: @3/(hash::blake2s::stream_it#1)
  @5: @2/(hash::blake2s::single#3)
  @6: @5/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initOur_tindyguard.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

