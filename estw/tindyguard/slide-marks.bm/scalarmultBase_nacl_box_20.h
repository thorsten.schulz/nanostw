/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _scalarmultBase_nacl_box_20_H_
#define _scalarmultBase_nacl_box_20_H_

#include "kcg_types.h"
#include "scalarmult_nacl_box_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_scalarmult_nacl_box_20 /* _L5=(nacl::box::scalarmult#2)/ */ Context_scalarmult_2;
  /* ----------------- no clocks of observable data ------------------ */
} outC_scalarmultBase_nacl_box_20;

/* ===========  node initialization and cycle functions  =========== */
/* nacl::box::scalarmultBase/ */
extern void scalarmultBase_nacl_box_20(
  /* _L2/, our/ */
  KeyPair32_slideTypes *our_20,
  /* _L3/, q/ */
  array_uint32_8 *q_20,
  /* _L5/, again/ */
  kcg_bool *again_20,
  /* _L6/, failed/ */
  kcg_bool *failed_20,
  outC_scalarmultBase_nacl_box_20 *outC);

extern void scalarmultBase_reset_nacl_box_20(
  outC_scalarmultBase_nacl_box_20 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void scalarmultBase_init_nacl_box_20(
  outC_scalarmultBase_nacl_box_20 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _scalarmultBase_nacl_box_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmultBase_nacl_box_20.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

