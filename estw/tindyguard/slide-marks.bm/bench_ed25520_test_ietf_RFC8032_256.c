/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_ed25520_test_ietf_RFC8032_256.h"

/* test::ietf_RFC8032::bench_ed25520/ */
kcg_bool bench_ed25520_test_ietf_RFC8032_256(
  /* _L17/, failing/ */
  kcg_bool failing_256,
  /* tc/ */
  testcase_test_ietf_RFC8032 *tc_256)
{
  KeyPair32_slideTypes tmp_str;
  Signature_nacl_sign tmp;
  /* _L36/ */
  array_uint32_16_256 _L36_256;
  /* _L46/ */
  kcg_int32 _L46_256;
  kcg_size idx;
  /* _L4/, failed/ */
  kcg_bool failed_256;

  _L46_256 = (*tc_256).mlen + kcg_lit_int32(16320);
  for (idx = 0; idx < 256; idx++) {
    kcg_copy_StreamChunk_slideTypes(&_L36_256[idx], &(*tc_256).m[0]);
  }
  kcg_copy_array_uint32_8(&tmp_str.sk_x, (array_uint32_8 *) &(*tc_256).key[0]);
  kcg_copy_array_uint32_8(&tmp_str.pk_y, (array_uint32_8 *) &(*tc_256).key[8]);
  /* _L1=(nacl::sign::sign#1)/ */
  sign_nacl_sign_256(&_L36_256, _L46_256, &tmp_str, &tmp);
  failed_256 = /* _L2=(nacl::sign::verify#1)/ */
    verify_nacl_sign_256(
      &_L36_256,
      _L46_256,
      (Key32_slideTypes *) &(*tc_256).key[8],
      &tmp) || failing_256;
  return failed_256;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_ed25520_test_ietf_RFC8032_256.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

