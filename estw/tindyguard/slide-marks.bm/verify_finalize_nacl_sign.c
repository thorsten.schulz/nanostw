/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "verify_finalize_nacl_sign.h"

/* nacl::sign::verify_finalize/ */
kcg_bool verify_finalize_nacl_sign(
  /* _L34/, _L37/, signature/ */
  Signature_nacl_sign *signature,
  /* _L32/, publicKey/ */
  Key32_slideTypes *publicKey,
  /* _L23/, lastPart/ */
  StreamChunk_slideTypes *lastPart,
  /* _L12/, hashin/ */
  hashAkkuWithChunk_hash_sha512 *hashin)
{
  StreamChunk_slideTypes tmp;
  kcg_size idx;
  /* @2/_L15/ */
  kcg_uint32 _L15_st32_casti64_1_reduce_1;
  /* @2/_L14/ */
  kcg_uint32 _L14_st32_casti64_1_reduce_1;
  array_uint32_8 tmp1;
  cpoint_nacl_op tmp2;
  cpoint_nacl_op tmp3;
  cpoint_nacl_op tmp4;
  array_int64_64 tmp5;
  /* @1/_L6/ */
  i64416_nacl_sign _L6_reduce_1;
  /* _L1/ */
  kcg_bool _L1;
  /* _L9/, failed/ */
  kcg_bool failed;

  /* _L21=(hash::sha512::finalize#1)/ */
  finalize_hash_sha512(
    hashin,
    lastPart,
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &tmp);
  /* @1/_L6= */
  for (idx = 0; idx < 16; idx++) {
    _L6_reduce_1[idx][0] = /* @2/_L17= */(kcg_int64)
        /* @2/_L8= */(kcg_uint8) tmp[idx];
    _L14_st32_casti64_1_reduce_1 = tmp[idx] >> kcg_lit_uint32(8);
    _L6_reduce_1[idx][1] = /* @2/_L18= */(kcg_int64)
        /* @2/_L7= */(kcg_uint8) _L14_st32_casti64_1_reduce_1;
    _L15_st32_casti64_1_reduce_1 = _L14_st32_casti64_1_reduce_1 >>
      kcg_lit_uint32(8);
    _L6_reduce_1[idx][2] = /* @2/_L19= */(kcg_int64)
        /* @2/_L6= */(kcg_uint8) _L15_st32_casti64_1_reduce_1;
    _L6_reduce_1[idx][3] = /* @2/_L20= */(kcg_int64)
        /* @2/_L5= */(kcg_uint8) (_L15_st32_casti64_1_reduce_1 >> kcg_lit_uint32(8));
  }
  /* _L1=(nacl::sign::unpackneg#1)/ */
  unpackneg_nacl_sign(publicKey, &_L1, &tmp2);
  /* _L16=(nacl::sign::scalarBase#1)/ */
  scalarBase_nacl_sign((array_uint32_8 *) &(*signature)[8], &tmp3);
  kcg_copy_array_int64_4(&tmp5[0], &_L6_reduce_1[0]);
  kcg_copy_array_int64_4(&tmp5[4], &_L6_reduce_1[1]);
  kcg_copy_array_int64_4(&tmp5[8], &_L6_reduce_1[2]);
  kcg_copy_array_int64_4(&tmp5[12], &_L6_reduce_1[3]);
  kcg_copy_array_int64_4(&tmp5[16], &_L6_reduce_1[4]);
  kcg_copy_array_int64_4(&tmp5[20], &_L6_reduce_1[5]);
  kcg_copy_array_int64_4(&tmp5[24], &_L6_reduce_1[6]);
  kcg_copy_array_int64_4(&tmp5[28], &_L6_reduce_1[7]);
  kcg_copy_array_int64_4(&tmp5[32], &_L6_reduce_1[8]);
  kcg_copy_array_int64_4(&tmp5[36], &_L6_reduce_1[9]);
  kcg_copy_array_int64_4(&tmp5[40], &_L6_reduce_1[10]);
  kcg_copy_array_int64_4(&tmp5[44], &_L6_reduce_1[11]);
  kcg_copy_array_int64_4(&tmp5[48], &_L6_reduce_1[12]);
  kcg_copy_array_int64_4(&tmp5[52], &_L6_reduce_1[13]);
  kcg_copy_array_int64_4(&tmp5[56], &_L6_reduce_1[14]);
  kcg_copy_array_int64_4(&tmp5[60], &_L6_reduce_1[15]);
  /* @1/_L3=(nacl::sign::modL#1)/ */ modL_nacl_sign(&tmp5, &tmp1);
  /* _L13=(nacl::sign::scalarMult#1)/ */
  scalarMult_nacl_sign(&tmp2, &tmp1, &tmp4);
  /* _L19=(nacl::sign::add#1)/ */ add_nacl_sign(&tmp3, &tmp4, &tmp2);
  /* _L20=(nacl::sign::pack#1)/ */ pack_nacl_sign(&tmp2, &tmp1);
  failed = _L1 || kcg_lit_int32(0) != /* _L5=(M::cmp#1)/ */
    cmp_M_uint32_8(&tmp1, (array_uint32_8 *) &(*signature)[0]);
  return failed;
}

/*
  Expanded instances for: nacl::sign::verify_finalize/
  @1: (nacl::sign::reduce#1)
  @2: @1/(slideTypes::st32_casti64#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** verify_finalize_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

