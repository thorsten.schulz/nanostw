/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10_H_
#define _process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10_H_

#include "kcg_types.h"
#include "block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10.h"
#include "block32_it2_hash_libsha_2_13_22_6_11_25.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::process32/ */
extern void process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10(
  /* _L2/, _L4/, zin/ */
  array_uint32_8 *zin_4_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L13/, m/ */
  array_uint32_16 *m_4_2_13_22_6_11_25_7_18_3_17_19_10,
  /* K/, _L26/, _L27/ */
  array_uint32_16_4 *K_4_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L3/, zout/ */
  array_uint32_8 *zout_4_2_13_22_6_11_25_7_18_3_17_19_10);



#endif /* _process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

