/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_nacl_sign_2.h"

/* nacl::sign::sign/ */
void sign_nacl_sign_2(
  /* _L23/, _L59/, _L62/, _L71/, msg/ */
  array_uint32_16_2 *msg_2,
  /* _L24/, len/ */
  kcg_int32 len_2,
  /* _L19/, _L40/, key/ */
  KeyPair32_slideTypes *key_2,
  /* _L41/, signature/ */
  Signature_nacl_sign *signature_2)
{
  prependAkku_nacl_sign acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  StreamChunk_slideTypes tmp;
  /* _L39/ */
  array_uint32_8 _L39_2;
  /* _L30/, _L36/ */
  kcg_bool _L36_2;
  /* _L64/ */
  array_uint32_16_1 _L64_2;
  /* _L60/, _L66/ */
  size_slideTypes _L66_2;
  /* _L67/ */
  prependAkku_nacl_sign _L67_2;

  /* _L30=(nacl::sign::sign_init#1)/ */
  sign_init_nacl_sign(key_2, len_2, &(*msg_2)[0], &_L36_2, &acc);
  kcg_copy_array_uint32_16_1(&_L64_2, (array_uint32_16_1 *) &(*msg_2)[1]);
  /* _L60= */
  if (_L36_2) {
    kcg_copy_prependAkku_nacl_sign(&_L67_2, &acc);
    /* _L60=(nacl::sign::sign_block1#1)/ */
    sign_block1_nacl_sign(
      kcg_lit_int32(0),
      &_L67_2,
      &_L64_2[0],
      &cond_iterw,
      &acc);
    _L66_2 = /* _L60= */(kcg_int32) (0 + 1);
  }
  else {
    _L66_2 = kcg_lit_int32(0);
  }
  if (kcg_lit_int32(0) <= _L66_2 && _L66_2 < kcg_lit_int32(1)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &_L64_2[_L66_2]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* _L36=(nacl::sign::sign_halftime#1)/ */
  sign_halftime_nacl_sign(key_2, &tmp, &acc, &_L36_2, &_L67_2, &_L39_2);
  /* _L66= */
  if (_L36_2) {
    /* _L66= */
    for (idx = 0; idx < 2; idx++) {
      kcg_copy_prependAkku_nacl_sign(&acc, &_L67_2);
      /* _L66=(nacl::sign::sign_block2#2)/ */
      sign_block2_nacl_sign(
        /* _L66= */(kcg_int32) idx,
        &acc,
        &(*msg_2)[idx],
        &cond_iterw,
        &_L67_2);
      _L66_2 = /* _L66= */(kcg_int32) (idx + 1);
      /* _L66= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    _L66_2 = kcg_lit_int32(0);
  }
  if (kcg_lit_int32(0) <= _L66_2 && _L66_2 < kcg_lit_int32(2)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &(*msg_2)[_L66_2]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* _L41=(nacl::sign::sign_finalize#1)/ */
  sign_finalize_nacl_sign(&tmp, &_L67_2, &_L39_2, signature_2);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_nacl_sign_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

