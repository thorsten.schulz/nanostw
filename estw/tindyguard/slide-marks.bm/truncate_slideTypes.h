/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _truncate_slideTypes_H_
#define _truncate_slideTypes_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* slideTypes::truncate/ */
extern void truncate_slideTypes(
  /* _L1/, mlast/ */
  StreamChunk_slideTypes *mlast,
  /* _L2/, mlen/ */
  size_slideTypes mlen,
  /* _L5/, mtrunc/ */
  StreamChunk_slideTypes *mtrunc);



#endif /* _truncate_slideTypes_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** truncate_slideTypes.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

