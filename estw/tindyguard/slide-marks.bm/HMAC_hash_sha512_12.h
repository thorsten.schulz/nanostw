/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _HMAC_hash_sha512_12_H_
#define _HMAC_hash_sha512_12_H_

#include "kcg_types.h"
#include "run_hash_sha512_3.h"
#include "run_hash_sha512_14.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha512::HMAC/ */
extern void HMAC_hash_sha512_12(
  /* _L8/, msg/ */
  array_uint32_16_12 *msg_12,
  /* _L9/, mlen/ */
  int_slideTypes mlen_12,
  /* _L16/, _L4/, key/ */
  array_uint32_16_2 *key_12,
  /* _L20/, hmac/ */
  StreamChunk_slideTypes *hmac_12);



#endif /* _HMAC_hash_sha512_12_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** HMAC_hash_sha512_12.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

