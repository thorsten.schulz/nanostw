/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_ed25520_test_ietf_RFC8032_16.h"

/* test::ietf_RFC8032::bench_ed25520/ */
kcg_bool bench_ed25520_test_ietf_RFC8032_16(
  /* _L17/, failing/ */
  kcg_bool failing_16,
  /* tc/ */
  testcase_test_ietf_RFC8032 *tc_16)
{
  KeyPair32_slideTypes tmp_str;
  Signature_nacl_sign tmp;
  /* _L36/ */
  array_uint32_16_16 _L36_16;
  /* _L46/ */
  kcg_int32 _L46_16;
  kcg_size idx;
  /* _L4/, failed/ */
  kcg_bool failed_16;

  _L46_16 = (*tc_16).mlen + kcg_lit_int32(960);
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_StreamChunk_slideTypes(&_L36_16[idx], &(*tc_16).m[0]);
  }
  kcg_copy_array_uint32_8(&tmp_str.sk_x, (array_uint32_8 *) &(*tc_16).key[0]);
  kcg_copy_array_uint32_8(&tmp_str.pk_y, (array_uint32_8 *) &(*tc_16).key[8]);
  /* _L1=(nacl::sign::sign#1)/ */
  sign_nacl_sign_16(&_L36_16, _L46_16, &tmp_str, &tmp);
  failed_16 = /* _L2=(nacl::sign::verify#1)/ */
    verify_nacl_sign_16(
      &_L36_16,
      _L46_16,
      (Key32_slideTypes *) &(*tc_16).key[8],
      &tmp) || failing_16;
  return failed_16;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_ed25520_test_ietf_RFC8032_16.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

