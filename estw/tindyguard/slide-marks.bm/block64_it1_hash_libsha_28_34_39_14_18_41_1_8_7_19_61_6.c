/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6.h"

/* hash::libsha::block64_it1/ */
void block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6(
  /* _L2/, ain/ */
  array_uint64_8 *ain_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L3/, w/ */
  array_uint64_16 *w_28_34_39_14_18_41_1_8_7_19_61_6,
  /* K/, _L17/ */
  array_uint64_16 *K_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L6/, aout/ */
  array_uint64_8 *aout_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L5/, woo/ */
  array_uint64_16 *woo_28_34_39_14_18_41_1_8_7_19_61_6)
{
  array_uint64_16 acc;
  kcg_size idx;
  array_uint64_8 acc1;

  kcg_copy_array_uint64_16(
    woo_28_34_39_14_18_41_1_8_7_19_61_6,
    w_28_34_39_14_18_41_1_8_7_19_61_6);
  /* _L5= */
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint64_16(&acc, woo_28_34_39_14_18_41_1_8_7_19_61_6);
    /* _L5=(hash::libsha::block64_it3#1)/ */
    block64_it3_hash_libsha_1_8_7_19_61_6(
      /* _L5= */(kcg_int32) idx,
      &acc,
      woo_28_34_39_14_18_41_1_8_7_19_61_6);
  }
  kcg_copy_array_uint64_8(
    aout_28_34_39_14_18_41_1_8_7_19_61_6,
    ain_28_34_39_14_18_41_1_8_7_19_61_6);
  /* _L6= */
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint64_8(&acc1, aout_28_34_39_14_18_41_1_8_7_19_61_6);
    /* _L6=(hash::libsha::block64_it2#1)/ */
    block64_it2_hash_libsha_28_34_39_14_18_41(
      &acc1,
      (*woo_28_34_39_14_18_41_1_8_7_19_61_6)[idx],
      (*K_28_34_39_14_18_41_1_8_7_19_61_6)[idx],
      aout_28_34_39_14_18_41_1_8_7_19_61_6);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

