/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "single_hash_sha512.h"

/* hash::sha512::single/ */
void single_hash_sha512(
  /* _L26/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L25/, len/ */
  size_slideTypes len,
  /* finalbits/ */
  kcg_uint32 finalbits,
  /* finalbitsLen/ */
  size_slideTypes finalbitsLen,
  /* _L27/, hash/ */
  StreamChunk_slideTypes *hash)
{
  hashAkkuWithChunk_hash_sha512 tmp_str;

  kcg_copy_array_uint64_8(&tmp_str.z, (array_uint64_8 *) &IV_hash_sha512);
  tmp_str.length = len;
  kcg_copy_StreamChunk_slideTypes(&tmp_str.porch, msg);
  /* _L27=(hash::sha512::finalize#1)/ */
  finalize_hash_sha512(
    &tmp_str,
    (StreamChunk_slideTypes *) &ZeroChunk_slideTypes,
    finalbits,
    finalbitsLen,
    hash);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** single_hash_sha512.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

