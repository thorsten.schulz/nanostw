/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "SIGMA64_hash_libsha_28_34_39.h"

/* hash::libsha::SIGMA64/ */
kcg_uint64 SIGMA64_hash_libsha_28_34_39(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, @3/_L11/, @3/x/, _L1/, x/ */
  kcg_uint64 x_28_34_39)
{
  /* _L6/, ret/ */
  kcg_uint64 ret_28_34_39;

  ret_28_34_39 = ((x_28_34_39 >> kcg_lit_uint64(28)) | kcg_lsl_uint64(
        x_28_34_39,
        kcg_lit_uint64(36))) ^ ((x_28_34_39 >> kcg_lit_uint64(34)) |
      kcg_lsl_uint64(x_28_34_39, kcg_lit_uint64(30))) ^ ((x_28_34_39 >>
        kcg_lit_uint64(39)) | kcg_lsl_uint64(x_28_34_39, kcg_lit_uint64(25)));
  return ret_28_34_39;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** SIGMA64_hash_libsha_28_34_39.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

