/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hashChunk_hash_blake2s_2.h"

/* hash::blake2s::hashChunk/ */
void hashChunk_hash_blake2s_2(
  /* @1/_L64/, @1/msg/, _L64/, msg/ */
  array_uint32_16_2 *msg_2,
  /* @1/_L66/, @1/len/, _L66/, len/ */
  size_slideTypes len_2,
  /* @1/_L70/, @1/keybytes/, _L70/, keybytes/ */
  size_slideTypes keybytes_2,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk_2)
{
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  /* @2/_L8/ */
  kcg_int32 _L8_stream_it_1_hash_1;
  kcg_int32 tmp_stream_it_1_hash_1;

  (*chunk_2)[0] = IV_hash_blake2s[0] ^ (kcg_lit_uint32(16842784) |
      kcg_lsl_uint32(/* @1/_L72= */(kcg_uint32) keybytes_2, kcg_lit_uint32(8)));
  kcg_copy_array_uint32_7(&(*chunk_2)[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* @1/_L60= */
  for (idx = 0; idx < 2; idx++) {
    kcg_copy_array_uint32_8(&acc, (array_uint32_8 *) &(*chunk_2)[0]);
    _L8_stream_it_1_hash_1 = /* @1/_L60= */(kcg_int32) idx *
      hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
    cond_iterw = len_2 > _L8_stream_it_1_hash_1;
    /* @2/_L14= */
    if (cond_iterw) {
      tmp_stream_it_1_hash_1 = _L8_stream_it_1_hash_1;
    }
    else {
      tmp_stream_it_1_hash_1 = len_2;
    }
    /* @2/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &acc,
      tmp_stream_it_1_hash_1,
      &(*msg_2)[idx],
      cond_iterw,
      (array_uint32_8 *) &(*chunk_2)[0]);
    /* @1/_L60= */
    if (!cond_iterw) {
      break;
    }
  }
  for (idx = 0; idx < 8; idx++) {
    (*chunk_2)[idx + 8] = kcg_lit_uint32(0);
  }
}

/*
  Expanded instances for: hash::blake2s::hashChunk/
  @1: (hash::blake2s::hash#1)
  @2: @1/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hashChunk_hash_blake2s_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

