/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "aead_nacl_box_256_1_20.h"

/* nacl::box::aead/ */
void aead_nacl_box_256_1_20(
  /* _L2/, msg/ */
  array_uint32_16_256 *msg_256_1_20,
  /* _L10/, _L4/, mlen/ */
  int_slideTypes mlen_256_1_20,
  /* _L14/, ad/ */
  array_uint32_16_1 *ad_256_1_20,
  /* _L15/, adlen/ */
  int_slideTypes adlen_256_1_20,
  /* _L5/, nonce/ */
  Nonce_nacl_core_chacha *nonce_256_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_256_1_20,
  /* _L13/, a/ */
  Mac_nacl_onetime *a_256_1_20,
  /* _L1/, cm/ */
  array_uint32_16_256 *cm_256_1_20)
{
  /* _L17/ */
  StreamChunk_slideTypes _L17_256_1_20;
  /* _L16/ */
  State_nacl_core_chacha _L16_256_1_20;

  /* _L16=(nacl::core::chacha::setup#1)/ */
  setup_nacl_core_chacha_20(
    nonce_256_1_20,
    key_256_1_20,
    &_L16_256_1_20,
    &_L17_256_1_20);
  /* _L1=(nacl::box::stream::chacha_IETF#1)/ */
  chacha_IETF_nacl_box_stream_256_20(
    msg_256_1_20,
    mlen_256_1_20,
    &_L16_256_1_20,
    cm_256_1_20);
  /* _L13=(nacl::onetime::auth2#1)/ */
  auth2_nacl_onetime_1_256(
    ad_256_1_20,
    adlen_256_1_20,
    cm_256_1_20,
    mlen_256_1_20,
    &_L17_256_1_20,
    a_256_1_20);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** aead_nacl_box_256_1_20.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

