/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _cswap_nacl_sign_H_
#define _cswap_nacl_sign_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::cswap/ */
extern void cswap_nacl_sign(
  /* _L1/, pqp/ */
  pq_pair_nacl_sign *pqp,
  /* _L8/, b/ */
  kcg_uint8 b,
  /* _L21/, o/ */
  pq_pair_nacl_sign *o);



#endif /* _cswap_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** cswap_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

