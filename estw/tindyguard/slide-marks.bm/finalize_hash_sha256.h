/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _finalize_hash_sha256_H_
#define _finalize_hash_sha256_H_

#include "kcg_types.h"
#include "block_refine_hash_sha256.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::finalize/ */
extern void finalize_hash_sha256(
  /* _L40/, zin/ */
  array_uint32_8 *zin,
  /* _L61/, m/ */
  StreamChunk_slideTypes *m,
  /* _L91/, _L93/, len/ */
  size_slideTypes len,
  /* _L73/, finalbits/ */
  kcg_uint32 finalbits,
  /* _L74/, _L89/, finalbitsLen/ */
  size_slideTypes finalbitsLen,
  /* _L94/, hash/ */
  StreamChunk_slideTypes *hash);



#endif /* _finalize_hash_sha256_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** finalize_hash_sha256.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

