/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _beHash2Chunk_slideTypes_H_
#define _beHash2Chunk_slideTypes_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* slideTypes::beHash2Chunk/ */
extern void beHash2Chunk_slideTypes(
  /* _L112/, zin/ */
  beHash_slideTypes *zin,
  /* _L111/, sha/ */
  StreamChunk_slideTypes *sha);



#endif /* _beHash2Chunk_slideTypes_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** beHash2Chunk_slideTypes.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

