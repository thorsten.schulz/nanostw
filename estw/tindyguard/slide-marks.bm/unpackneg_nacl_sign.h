/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _unpackneg_nacl_sign_H_
#define _unpackneg_nacl_sign_H_

#include "kcg_types.h"
#include "neq25519_nacl_op.h"
#include "pow2523_nacl_op.h"
#include "par25519_nacl_op.h"
#include "S_nacl_op.h"
#include "A_nacl_op.h"
#include "M_nacl_op.h"
#include "Z_nacl_op.h"
#include "unpack25519_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::unpackneg/ */
extern void unpackneg_nacl_sign(
  /* _L1/, _L55/, pk/ */
  Key32_slideTypes *pk,
  /* _L49/, failed/ */
  kcg_bool *failed,
  /* _L14/, r/ */
  cpoint_nacl_op *r);



#endif /* _unpackneg_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unpackneg_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

