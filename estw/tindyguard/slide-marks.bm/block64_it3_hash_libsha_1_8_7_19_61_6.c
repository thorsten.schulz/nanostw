/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "block64_it3_hash_libsha_1_8_7_19_61_6.h"

/* hash::libsha::block64_it3/ */
void block64_it3_hash_libsha_1_8_7_19_61_6(
  /* _L6/, i/ */
  size_slideTypes i_1_8_7_19_61_6,
  /* _L1/, w/ */
  array_uint64_16 *w_1_8_7_19_61_6,
  /* _L2/, wo/ */
  array_uint64_16 *wo_1_8_7_19_61_6)
{
  kcg_uint64 tmp;
  kcg_uint64 tmp1;
  kcg_uint64 tmp2;
  kcg_uint64 tmp3;
  /* _L5/ */
  kcg_int32 _L5_1_8_7_19_61_6;
  /* _L16/ */
  kcg_int32 _L16_1_8_7_19_61_6;
  /* _L20/ */
  kcg_int32 _L20_1_8_7_19_61_6;

  _L20_1_8_7_19_61_6 = (i_1_8_7_19_61_6 + kcg_lit_int32(14)) % kcg_lit_int32(16);
  _L16_1_8_7_19_61_6 = (i_1_8_7_19_61_6 + kcg_lit_int32(1)) % kcg_lit_int32(16);
  _L5_1_8_7_19_61_6 = (i_1_8_7_19_61_6 + kcg_lit_int32(9)) % kcg_lit_int32(16);
  kcg_copy_array_uint64_16(wo_1_8_7_19_61_6, w_1_8_7_19_61_6);
  if (kcg_lit_int32(0) <= _L5_1_8_7_19_61_6 && _L5_1_8_7_19_61_6 <
    kcg_lit_int32(16)) {
    tmp = (*w_1_8_7_19_61_6)[_L5_1_8_7_19_61_6];
  }
  else {
    tmp = kcg_lit_uint64(0);
  }
  if (kcg_lit_int32(0) <= _L16_1_8_7_19_61_6 && _L16_1_8_7_19_61_6 <
    kcg_lit_int32(16)) {
    tmp3 = (*w_1_8_7_19_61_6)[_L16_1_8_7_19_61_6];
  }
  else {
    tmp3 = kcg_lit_uint64(0);
  }
  tmp1 = /* _L17=(hash::libsha::sigma64#1)/ */ sigma64_hash_libsha_1_8_7(tmp3);
  if (kcg_lit_int32(0) <= _L20_1_8_7_19_61_6 && _L20_1_8_7_19_61_6 <
    kcg_lit_int32(16)) {
    tmp3 = (*w_1_8_7_19_61_6)[_L20_1_8_7_19_61_6];
  }
  else {
    tmp3 = kcg_lit_uint64(0);
  }
  tmp2 = /* _L23=(hash::libsha::sigma64#2)/ */ sigma64_hash_libsha_19_61_6(tmp3);
  if (kcg_lit_int32(0) <= i_1_8_7_19_61_6 && i_1_8_7_19_61_6 < kcg_lit_int32(
      16)) {
    (*wo_1_8_7_19_61_6)[i_1_8_7_19_61_6] = (*w_1_8_7_19_61_6)[i_1_8_7_19_61_6] +
      tmp + tmp1 + tmp2;
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block64_it3_hash_libsha_1_8_7_19_61_6.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

