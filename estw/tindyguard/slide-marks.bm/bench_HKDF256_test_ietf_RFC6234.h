/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _bench_HKDF256_test_ietf_RFC6234_H_
#define _bench_HKDF256_test_ietf_RFC6234_H_

#include "kcg_types.h"
#include "hkdf_expand_hash_sha256_1_3.h"
#include "hkdf_extract_hash_sha256_2_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC6234::bench_HKDF256/ */
extern kcg_bool bench_HKDF256_test_ietf_RFC6234(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* tc/ */
  testcase_hkdf_test_ietf_RFC7693 *tc);



#endif /* _bench_HKDF256_test_ietf_RFC6234_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_HKDF256_test_ietf_RFC6234.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

