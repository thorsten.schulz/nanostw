/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hkdf_expand_it2_hash_sha256_3_H_
#define _hkdf_expand_it2_hash_sha256_3_H_

#include "kcg_types.h"
#include "truncate_slideTypes.h"
#include "HMAC_hash_sha256_3_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::hkdf_expand_it2/ */
extern void hkdf_expand_it2_hash_sha256_3(
  /* @1/_L1/, @1/i/, _L48/, _L66/, _L7/, i/ */
  size_slideTypes i_3,
  /* _L26/, hin/ */
  array_uint32_8 *hin_3,
  /* _L3/, prk/ */
  StreamChunk_slideTypes *prk_3,
  /* _L51/, m0/ */
  array_uint32_16_3 *m0_3,
  /* _L21/, _L36/, m/ */
  array_uint32_16_3 *m_3,
  /* _L29/, _L43/, infolen/ */
  size_slideTypes infolen_3,
  /* _L13/, okmlen/ */
  size_slideTypes okmlen_3,
  /* _L54/, orig/ */
  kcg_uint32 orig_3,
  /* x/ */
  size_slideTypes x_3,
  /* y/ */
  size_slideTypes y_3,
  /* _L58/, _L60/, z/ */
  kcg_uint32 z_3,
  /* _L12/, goOn/ */
  kcg_bool *goOn_3,
  /* _L20/, hoo/ */
  array_uint32_8 *hoo_3,
  /* _L16/, okm/ */
  StreamChunk_slideTypes *okm_3);

/*
  Expanded instances for: hash::sha256::hkdf_expand_it2/
  @1: (M::inc#1)
*/

#endif /* _hkdf_expand_it2_hash_sha256_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_expand_it2_hash_sha256_3.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

