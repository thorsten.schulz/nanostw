/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "HMAC_hash_sha256_12_2.h"

/* hash::sha256::HMAC/ */
void HMAC_hash_sha256_12_2(
  /* _L8/, msg/ */
  array_uint32_16_12 *msg_12_2,
  /* _L9/, mlen/ */
  int_slideTypes mlen_12_2,
  /* key/ */
  array_uint32_16_2 *key_12_2,
  /* klen/ */
  size_slideTypes klen_12_2,
  /* _L20/, hmac/ */
  StreamChunk_slideTypes *hmac_12_2)
{
  array_uint32_16_2 tmp;
  kcg_size idx;
  array_uint32_16_13 tmp1;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_12_2;
  /* @1/_L1/, @1/chunk/, @2/_L1/, @2/chunk/, _L29/, sbk/ */
  StreamChunk_slideTypes sbk_12_2;

  IfBlock1_clock_12_2 = klen_12_2 > hBlockBytes_hash_sha256;
  /* IfBlock1: */
  if (IfBlock1_clock_12_2) {
    /* IfBlock1:then:_L1=(hash::sha256::run#3)/ */
    run_hash_sha256_2(
      key_12_2,
      klen_12_2,
      kcg_lit_uint32(0),
      kcg_lit_int32(0),
      &sbk_12_2);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(&sbk_12_2, &(*key_12_2)[0]);
  }
  /* @1/_L2=, @2/_L2= */
  for (idx = 0; idx < 16; idx++) {
    tmp[0][idx] = sbk_12_2[idx] ^ HMAC_outer_xor_hash_libsha;
    tmp1[0][idx] = sbk_12_2[idx] ^ HMAC_inner_xor_hash_libsha;
  }
  kcg_copy_array_uint32_16_12(&tmp1[1], msg_12_2);
  /* _L1=(hash::sha256::run#1)/ */
  run_hash_sha256_13(
    &tmp1,
    hBlockBytes_hash_sha256 + mlen_12_2,
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &tmp[1]);
  /* _L20=(hash::sha256::run#2)/ */
  run_hash_sha256_2(
    &tmp,
    kcg_lit_int32(96),
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    hmac_12_2);
}

/*
  Expanded instances for: hash::sha256::HMAC/
  @1: (slideTypes::xorChunkConst#1)
  @2: (slideTypes::xorChunkConst#3)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** HMAC_hash_sha256_12_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

