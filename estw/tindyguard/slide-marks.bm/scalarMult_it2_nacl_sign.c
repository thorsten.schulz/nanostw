/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarMult_it2_nacl_sign.h"

/* nacl::sign::scalarMult_it2/ */
void scalarMult_it2_nacl_sign(
  /* _L1/, i/ */
  kcg_uint8 i,
  /* _L2/, akku/ */
  pq_pair_nacl_sign *akku,
  /* _L3/, b/ */
  kcg_uint8 b,
  /* _L11/, akkoo/ */
  pq_pair_nacl_sign *akkoo)
{
  pq_pair_nacl_sign tmp_str;
  cpoint_nacl_op tmp;
  cpoint_nacl_op tmp1;
  /* _L7/ */
  kcg_uint8 _L7;
  /* _L9/ */
  pq_pair_nacl_sign _L9;

  _L7 = (b >> (kcg_lit_uint8(7) - i)) & kcg_lit_uint8(1);
  /* _L9=(nacl::sign::cswap#1)/ */ cswap_nacl_sign(akku, _L7, &_L9);
  /* _L13=(nacl::sign::add#2)/ */ add_nacl_sign(&_L9.p, &_L9.p, &tmp);
  /* _L12=(nacl::sign::add#1)/ */ add_nacl_sign(&_L9.p, &_L9.q, &tmp1);
  kcg_copy_cpoint_nacl_op(&tmp_str.p, &tmp);
  kcg_copy_cpoint_nacl_op(&tmp_str.q, &tmp1);
  /* _L11=(nacl::sign::cswap#3)/ */ cswap_nacl_sign(&tmp_str, _L7, akkoo);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarMult_it2_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

