/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _pow2523_it1_nacl_op_H_
#define _pow2523_it1_nacl_op_H_

#include "kcg_types.h"
#include "S_nacl_op.h"
#include "M_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::pow2523_it1/ */
extern void pow2523_it1_nacl_op(
  /* _L3/, a/ */
  int_slideTypes a,
  /* _L4/, c/ */
  gf_nacl_op *c,
  /* _L1/, i/ */
  gf_nacl_op *i,
  /* _L12/, o/ */
  gf_nacl_op *o);



#endif /* _pow2523_it1_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pow2523_it1_nacl_op.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

