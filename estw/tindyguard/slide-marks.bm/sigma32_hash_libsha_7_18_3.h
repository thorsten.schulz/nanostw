/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sigma32_hash_libsha_7_18_3_H_
#define _sigma32_hash_libsha_7_18_3_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::sigma32/ */
extern kcg_uint32 sigma32_hash_libsha_7_18_3(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, _L1/, x/ */
  kcg_uint32 x_7_18_3);

/*
  Expanded instances for: hash::libsha::sigma32/
  @1: (M::Ror32#1)
  @2: (M::Ror32#2)
*/

#endif /* _sigma32_hash_libsha_7_18_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sigma32_hash_libsha_7_18_3.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

