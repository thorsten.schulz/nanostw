/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _bench_test_ietf_RFC7539_H_
#define _bench_test_ietf_RFC7539_H_

#include "kcg_types.h"
#include "bench_xaead_test_ietf_RFC7539_256.h"
#include "bench_aead_test_ietf_RFC7539_256.h"
#include "bench_xaead_test_ietf_RFC7539_64.h"
#include "bench_aead_test_ietf_RFC7539_64.h"
#include "bench_aead_test_ietf_RFC7539_23.h"
#include "bench_xaead_test_ietf_RFC7539_23.h"
#include "bench_xaead_test_ietf_RFC7539_2.h"
#include "bench_aead_test_ietf_RFC7539_2.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC7539::bench/ */
extern kcg_bool bench_test_ietf_RFC7539(void);



#endif /* _bench_test_ietf_RFC7539_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_test_ietf_RFC7539.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

