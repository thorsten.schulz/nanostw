/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "eval_response_tindyguard_handshake_3.h"

/* tindyguard::handshake::eval_response/ */
void eval_response_tindyguard_handshake_3(
  /* s/ */
  Session_tindyguardTypes *s_3,
  /* fail_in/ */
  kcg_bool fail_in_3,
  /* rlength/ */
  length_t_udp rlength_3,
  /* responsemsg/ */
  array_uint32_16_4 *responsemsg_3,
  /* endpoint/ */
  peer_t_udp *endpoint_3,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_3,
  /* soo/ */
  Session_tindyguardTypes *soo_3,
  /* allfail/ */
  kcg_bool *allfail_3)
{
  array_uint32_16_1 tmp;
  array_uint32_16 tmp1;
  array_uint32_16 tmp2;
  array_uint32_8_2 tmp3;
  array_uint32_16 tmp4;
  array_uint32_8_2 tmp5;
  HashChunk_hash_blake2s tmp6;
  array_uint32_16_1 tmp7;
  array_uint32_16 tmp8;
  array_uint32_16 tmp9;
  array_uint8_16 tmp10;
  array_uint32_16_1 tmp11;
  array_uint32_16 tmp12;
  array_uint32_8 tmp13;
  array_uint32_16 tmp14;
  array_uint32_8 tmp15;
  array_uint32_16 tmp16;
  /* @3/_L14/ */
  array_uint32_16_1 _L14_DHDerive_6;
  /* @3/_L5/ */
  array_uint32_16 _L5_DHDerive_6;
  /* @1/_L42/ */
  kcg_uint32 _L42_stAuth_4;
  /* @1/_L46/ */
  kcg_uint32 _L46_stAuth_4;
  /* @1/_L37/ */
  kcg_uint32 _L37_stAuth_4;
  /* @1/_L38/ */
  kcg_uint32 _L38_stAuth_4;
  /* @1/_L30/ */
  kcg_uint32 _L30_stAuth_4;
  /* @1/_L32/ */
  kcg_uint32 _L32_stAuth_4;
  /* @1/_L14/ */
  kcg_uint32 _L14_stAuth_4;
  /* @1/_L15/ */
  kcg_uint32 _L15_stAuth_4;
  /* @2/_L14/ */
  array_uint32_16_1 _L14_DHDerive_5;
  /* @2/_L5/ */
  array_uint32_16 _L5_DHDerive_5;
  /* @3/_L3/, @3/fail/, IfBlock1:then:_L35/ */
  kcg_bool _L35_then_IfBlock1_3;
  /* IfBlock1:then:_L45/ */
  kcg_bool _L45_then_IfBlock1_3;
  /* @2/_L3/, @2/fail/, IfBlock1:then:_L54/ */
  kcg_bool _L54_then_IfBlock1_3;
  /* IfBlock1:then:_L58/ */
  array_uint32_3 _L58_then_IfBlock1_3;
  /* IfBlock1:then:_L69/ */
  array_uint32_16_1 _L69_then_IfBlock1_3;
  /* IfBlock1:then:_L72/ */
  array_uint32_8_2 _L72_then_IfBlock1_3;
  /* IfBlock1:then:_L87/ */
  array_uint32_8_3 _L87_then_IfBlock1_3;
  /* @2/_L10/,
     @2/their/,
     @3/_L10/,
     @3/their/,
     IfBlock1:then:_L19/,
     IfBlock1:then:_L40/,
     IfBlock1:then:_L59/,
     IfBlock1:then:_L68/,
     IfBlock1:then:_L96/,
     IfBlock1:then:their_ephemeral/ */
  array_uint32_8 _L19_then_IfBlock1_3;
  /* IfBlock1:then:_L22/ */
  array_uint32_48 _L22_then_IfBlock1_3;
  /* @1/_L25/,
     @1/u32/,
     IfBlock1:then:_L24/,
     IfBlock1:then:_L83/,
     IfBlock1:then:in_a_nothing/ */
  array_uint32_4 _L24_then_IfBlock1_3;
  /* IfBlock1:then:_L76/, IfBlock1:then:fail/ */
  kcg_bool fail_then_IfBlock1_3;
  array_uint32_16_1 noname;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_3;
  kcg_size idx;

  IfBlock1_clock_3 = rlength_3 == kcg_lit_int32(92) &&
    (*responsemsg_3)[0][12] == MsgType_Response_tindyguard;
  /* IfBlock1: */
  if (IfBlock1_clock_3) {
    kcg_copy_StreamChunk_slideTypes(
      &_L69_then_IfBlock1_3[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    kcg_copy_StreamChunk_slideTypes(&_L22_then_IfBlock1_3[0], &(*responsemsg_3)[0]);
    kcg_copy_StreamChunk_slideTypes(
      &_L22_then_IfBlock1_3[16],
      &(*responsemsg_3)[1]);
    kcg_copy_StreamChunk_slideTypes(
      &_L22_then_IfBlock1_3[32],
      &(*responsemsg_3)[2]);
    kcg_copy_array_uint32_8(
      &_L19_then_IfBlock1_3,
      (array_uint32_8 *) &_L22_then_IfBlock1_3[15]);
    /* @3/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*sks_3).key,
      &_L19_then_IfBlock1_3,
      kcg_true,
      &_L35_then_IfBlock1_3,
      (Key32_slideTypes *) &_L5_DHDerive_6[0]);
    kcg_copy_array_uint32_4(
      &_L24_then_IfBlock1_3,
      (array_uint32_4 *) &_L22_then_IfBlock1_3[23]);
    tmp10[0] = /* @1/_L8= */(kcg_uint8) _L24_then_IfBlock1_3[0];
    tmp10[4] = /* @1/_L26= */(kcg_uint8) _L24_then_IfBlock1_3[1];
    tmp10[8] = /* @1/_L34= */(kcg_uint8) _L24_then_IfBlock1_3[2];
    tmp10[12] = /* @1/_L41= */(kcg_uint8) _L24_then_IfBlock1_3[3];
    _L14_stAuth_4 = _L24_then_IfBlock1_3[0] >> kcg_lit_uint32(8);
    tmp10[1] = /* @1/_L7= */(kcg_uint8) _L14_stAuth_4;
    _L15_stAuth_4 = _L14_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[2] = /* @1/_L6= */(kcg_uint8) _L15_stAuth_4;
    tmp10[3] = /* @1/_L5= */(kcg_uint8) (_L15_stAuth_4 >> kcg_lit_uint32(8));
    _L32_stAuth_4 = _L24_then_IfBlock1_3[1] >> kcg_lit_uint32(8);
    tmp10[5] = /* @1/_L29= */(kcg_uint8) _L32_stAuth_4;
    _L30_stAuth_4 = _L32_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[6] = /* @1/_L27= */(kcg_uint8) _L30_stAuth_4;
    tmp10[7] = /* @1/_L31= */(kcg_uint8) (_L30_stAuth_4 >> kcg_lit_uint32(8));
    _L37_stAuth_4 = _L24_then_IfBlock1_3[2] >> kcg_lit_uint32(8);
    tmp10[9] = /* @1/_L36= */(kcg_uint8) _L37_stAuth_4;
    _L38_stAuth_4 = _L37_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[10] = /* @1/_L33= */(kcg_uint8) _L38_stAuth_4;
    tmp10[11] = /* @1/_L35= */(kcg_uint8) (_L38_stAuth_4 >> kcg_lit_uint32(8));
    _L46_stAuth_4 = _L24_then_IfBlock1_3[3] >> kcg_lit_uint32(8);
    tmp10[13] = /* @1/_L44= */(kcg_uint8) _L46_stAuth_4;
    _L42_stAuth_4 = _L46_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[14] = /* @1/_L43= */(kcg_uint8) _L42_stAuth_4;
    tmp10[15] = /* @1/_L45= */(kcg_uint8) (_L42_stAuth_4 >> kcg_lit_uint32(8));
    /* @2/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*s_3).handshake.ephemeral,
      &_L19_then_IfBlock1_3,
      kcg_true,
      &_L54_then_IfBlock1_3,
      (Key32_slideTypes *) &_L5_DHDerive_5[0]);
    kcg_copy_psk_tindyguardTypes(&tmp1[0], &(*s_3).peer.preshared);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_6[idx + 8] = kcg_lit_uint32(0);
      _L5_DHDerive_5[idx + 8] = kcg_lit_uint32(0);
      tmp1[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp[0], &tmp1);
    kcg_copy_array_uint32_8(&tmp8[0], &_L19_then_IfBlock1_3);
    for (idx = 0; idx < 8; idx++) {
      tmp8[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp7[0], &tmp8);
    /* IfBlock1:then:_L75=(hash::blake2s::hkdfChunk1#3)/ */
    hkdfChunk1_hash_blake2s_1(
      &tmp7,
      kcg_lit_int32(32),
      &(*s_3).handshake.chainingKey,
      &tmp6);
    kcg_copy_array_uint32_16(&_L14_DHDerive_5[0], &_L5_DHDerive_5);
    /* @2/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(&_L14_DHDerive_5, kcg_lit_int32(32), &tmp6, &tmp5);
    kcg_copy_Hash_hash_blake2s(&tmp4[0], &tmp5[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp4[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&_L14_DHDerive_6[0], &_L5_DHDerive_6);
    /* @3/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(&_L14_DHDerive_6, kcg_lit_int32(32), &tmp4, &tmp3);
    kcg_copy_Hash_hash_blake2s(&tmp2[0], &tmp3[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp2[idx + 8] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L87=(hash::blake2s::hkdf#5)/ */
    hkdf_hash_blake2s_1_3(
      &tmp,
      kcg_lit_int32(32),
      &tmp2,
      &_L87_then_IfBlock1_3);
    kcg_copy_Hash_hash_blake2s(&tmp9[0], &_L87_then_IfBlock1_3[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp9[idx + 8] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L72=(hash::blake2s::hkdf#6)/ */
    hkdf_hash_blake2s_1_2(
      &_L69_then_IfBlock1_3,
      kcg_lit_int32(0),
      &tmp9,
      &_L72_then_IfBlock1_3);
    tmp13[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp13[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    tmp15[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp15[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    kcg_copy_Hash_hash_blake2s(&tmp16[0], &(*s_3).handshake.ihash);
    kcg_copy_array_uint32_8(&tmp16[8], &_L19_then_IfBlock1_3);
    /* @5/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp15,
      kcg_lit_int32(64),
      &tmp16,
      kcg_false,
      (array_uint32_8 *) &tmp14[0]);
    kcg_copy_Hash_hash_blake2s(&tmp14[8], &_L87_then_IfBlock1_3[1]);
    /* @7/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp13,
      kcg_lit_int32(64),
      &tmp14,
      kcg_false,
      (array_uint32_8 *) &tmp12[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp12[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp11[0], &tmp12);
    for (idx = 0; idx < 3; idx++) {
      _L58_then_IfBlock1_3[idx] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L44=(nacl::box::aeadOpen#3)/ */
    aeadOpen_nacl_box_1_1_20(
      &tmp10,
      &_L69_then_IfBlock1_3,
      kcg_lit_int32(0),
      &tmp11,
      kcg_lit_int32(32),
      &_L58_then_IfBlock1_3,
      &_L87_then_IfBlock1_3[2],
      &noname,
      &_L45_then_IfBlock1_3);
    fail_then_IfBlock1_3 = _L35_then_IfBlock1_3 || _L54_then_IfBlock1_3 ||
      _L45_then_IfBlock1_3;
    /* IfBlock1:then:_L93= */
    if (fail_then_IfBlock1_3) {
      kcg_copy_Session_tindyguardTypes(soo_3, s_3);
    }
    else {
      kcg_copy_secret_tindyguardTypes(&(*soo_3).ot, &_L72_then_IfBlock1_3[0]);
      kcg_copy_secret_tindyguardTypes(&(*soo_3).to, &_L72_then_IfBlock1_3[1]);
      (*soo_3).ot_cnt = kcg_lit_uint64(0);
      (*soo_3).to_cnt = kcg_lit_uint64(0);
      (*soo_3).to_cnt_cache = kcg_lit_uint64(0);
      (*soo_3).our = (*s_3).our;
      (*soo_3).their = _L22_then_IfBlock1_3[13];
      (*soo_3).rx_bytes = kcg_lit_int64(0);
      (*soo_3).rx_cnt = kcg_lit_int32(1);
      (*soo_3).tx_cnt = kcg_lit_int32(0);
      (*soo_3).txTime = kcg_lit_int64(-1);
      (*soo_3).sTime = (*endpoint_3).mtime;
      (*soo_3).pid = (*s_3).pid;
      kcg_copy_State_tindyguard_handshake(
        &(*soo_3).handshake,
        (State_tindyguard_handshake *) &EmptyState_tindyguard_handshake);
      (*soo_3).transmissive = kcg_false;
      (*soo_3).gotKeepAlive = kcg_false;
      (*soo_3).sentKeepAlive = kcg_false;
      kcg_copy_Peer_tindyguardTypes(&(*soo_3).peer, &(*s_3).peer);
      kcg_copy_peer_t_udp(&(*soo_3).peer.endpoint, endpoint_3);
    }
    *allfail_3 = fail_in_3 && fail_then_IfBlock1_3;
  }
  else {
    kcg_copy_Session_tindyguardTypes(soo_3, s_3);
    *allfail_3 = fail_in_3;
  }
}

/*
  Expanded instances for: tindyguard::handshake::eval_response/
  @1: (slideTypes::stAuth#4)
  @2: (tindyguard::handshake::DHDerive#5)
  @3: (tindyguard::handshake::DHDerive#6)
  @4: (hash::blake2s::single#6)
  @5: @4/(hash::blake2s::stream_it#1)
  @6: (hash::blake2s::single#7)
  @7: @6/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_response_tindyguard_handshake_3.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

