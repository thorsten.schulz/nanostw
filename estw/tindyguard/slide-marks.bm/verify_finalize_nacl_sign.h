/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _verify_finalize_nacl_sign_H_
#define _verify_finalize_nacl_sign_H_

#include "kcg_types.h"
#include "unpackneg_nacl_sign.h"
#include "scalarBase_nacl_sign.h"
#include "scalarMult_nacl_sign.h"
#include "add_nacl_sign.h"
#include "pack_nacl_sign.h"
#include "modL_nacl_sign.h"
#include "finalize_hash_sha512.h"
#include "cmp_M_uint32_8.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::verify_finalize/ */
extern kcg_bool verify_finalize_nacl_sign(
  /* _L34/, _L37/, signature/ */
  Signature_nacl_sign *signature,
  /* _L32/, publicKey/ */
  Key32_slideTypes *publicKey,
  /* _L23/, lastPart/ */
  StreamChunk_slideTypes *lastPart,
  /* _L12/, hashin/ */
  hashAkkuWithChunk_hash_sha512 *hashin);



#endif /* _verify_finalize_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** verify_finalize_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

