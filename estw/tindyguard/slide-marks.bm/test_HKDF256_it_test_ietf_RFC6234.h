/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _test_HKDF256_it_test_ietf_RFC6234_H_
#define _test_HKDF256_it_test_ietf_RFC6234_H_

#include "kcg_types.h"
#include "hkdf_expand_hash_sha256_2_2.h"
#include "hkdf_extract_hash_sha256_2_2.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC6234::test_HKDF256_it/ */
extern kcg_bool test_HKDF256_it_test_ietf_RFC6234(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L36/, tc/ */
  testcaseHKDF_test_ietf_RFC6234 *tc);



#endif /* _test_HKDF256_it_test_ietf_RFC6234_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_HKDF256_it_test_ietf_RFC6234.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

