/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "unpackneg_nacl_sign.h"

/* nacl::sign::unpackneg/ */
void unpackneg_nacl_sign(
  /* _L1/, _L55/, pk/ */
  Key32_slideTypes *pk,
  /* _L49/, failed/ */
  kcg_bool *failed,
  /* _L14/, r/ */
  cpoint_nacl_op *r)
{
  gf_nacl_op tmp;
  gf_nacl_op tmp1;
  /* _L16/ */
  gf_nacl_op _L16;
  /* _L20/, _L62/, _L63/, _L64/, _L65/, num/ */
  gf_nacl_op _L20;
  /* _L21/, _L31/, _L33/, _L38/, _L46/, den/ */
  gf_nacl_op _L21;
  /* _L22/, _L35/ */
  gf_nacl_op _L22;
  /* _L40/, _L54/ */
  kcg_bool _L54;

  kcg_copy_gf_nacl_op(&(*r)[2], (gf_nacl_op *) &gf1_nacl_op);
  /* _L16=(nacl::op::unpack25519#1)/ */ unpack25519_nacl_op(pk, &_L16);
  kcg_copy_gf_nacl_op(&(*r)[1], &_L16);
  /* _L17=(nacl::op::S#1)/ */ S_nacl_op(&_L16, &tmp1);
  /* _L18=(nacl::op::M#1)/ */ M_nacl_op(&tmp1, (gf_nacl_op *) &D_nacl_op, &_L22);
  /* _L21=(nacl::op::A#1)/ */
  A_nacl_op((gf_nacl_op *) &gf1_nacl_op, &_L22, &_L21);
  /* _L22=(nacl::op::S#2)/ */ S_nacl_op(&_L21, &_L22);
  /* _L20=(nacl::op::Z#1)/ */
  Z_nacl_op(&tmp1, (gf_nacl_op *) &gf1_nacl_op, &_L20);
  /* _L23=(nacl::op::S#3)/ */ S_nacl_op(&_L22, &tmp1);
  /* _L24=(nacl::op::M#2)/ */ M_nacl_op(&tmp1, &_L22, &tmp);
  /* _L25=(nacl::op::M#3)/ */ M_nacl_op(&tmp, &_L20, &_L22);
  /* _L26=(nacl::op::M#4)/ */ M_nacl_op(&_L22, &_L21, &tmp1);
  /* _L27=(nacl::op::pow2523#1)/ */ pow2523_nacl_op(&tmp1, &tmp);
  /* _L28=(nacl::op::M#5)/ */ M_nacl_op(&tmp, &_L20, &_L22);
  /* _L29=(nacl::op::M#6)/ */ M_nacl_op(&_L22, &_L21, &tmp1);
  /* _L30=(nacl::op::M#7)/ */ M_nacl_op(&tmp1, &_L21, &tmp);
  /* _L35=(nacl::op::M#9)/ */ M_nacl_op(&tmp, &_L21, &_L22);
  /* _L36=(nacl::op::S#4)/ */ S_nacl_op(&_L22, &tmp1);
  /* _L37=(nacl::op::M#10)/ */ M_nacl_op(&tmp1, &_L21, &tmp);
  _L54 = kcg_lit_int32(0) != /* _L39=(nacl::op::neq25519#1)/ */
    neq25519_nacl_op(&tmp, &_L20);
  if (_L54) {
    /* _L42=(nacl::op::M#11)/ */ M_nacl_op(&_L22, (gf_nacl_op *) &I_nacl_op, &tmp1);
  }
  else {
    kcg_copy_gf_nacl_op(&tmp1, &_L22);
  }
  _L54 = (*pk)[7] >> kcg_lit_uint32(31) == /* _L53=(nacl::op::par25519#1)/ */
    par25519_nacl_op(&tmp1);
  if (_L54) {
    /* _L58=(nacl::op::Z#2)/ */ Z_nacl_op((gf_nacl_op *) &gf0_nacl_op, &tmp1, &tmp);
  }
  else {
    kcg_copy_gf_nacl_op(&tmp, &tmp1);
  }
  kcg_copy_gf_nacl_op(&(*r)[0], &tmp);
  /* _L44=(nacl::op::S#5)/ */ S_nacl_op(&tmp1, &_L22);
  /* _L45=(nacl::op::M#12)/ */ M_nacl_op(&_L22, &_L21, &tmp1);
  *failed = kcg_lit_int32(0) != /* _L47=(nacl::op::neq25519#2)/ */
    neq25519_nacl_op(&tmp1, &_L20);
  /* _L60=(nacl::op::M#13)/ */ M_nacl_op(&tmp, &_L16, &(*r)[3]);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unpackneg_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

