/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _KCG_TYPES_H_
#define _KCG_TYPES_H_

#include "stddef.h"

#define KCG_MAPW_CPY

#include "../user_macros.h"

#ifndef kcg_char
#define kcg_char kcg_char
typedef char kcg_char;
#endif /* kcg_char */

#ifndef kcg_bool
#define kcg_bool kcg_bool
typedef unsigned char kcg_bool;
#endif /* kcg_bool */

#ifndef kcg_float32
#define kcg_float32 kcg_float32
typedef float kcg_float32;
#endif /* kcg_float32 */

#ifndef kcg_float64
#define kcg_float64 kcg_float64
typedef double kcg_float64;
#endif /* kcg_float64 */

#ifndef kcg_size
#define kcg_size kcg_size
typedef ptrdiff_t kcg_size;
#endif /* kcg_size */

#ifndef kcg_uint64
#define kcg_uint64 kcg_uint64
typedef unsigned long long kcg_uint64;
#endif /* kcg_uint64 */

#ifndef kcg_uint32
#define kcg_uint32 kcg_uint32
typedef unsigned long kcg_uint32;
#endif /* kcg_uint32 */

#ifndef kcg_uint16
#define kcg_uint16 kcg_uint16
typedef unsigned short kcg_uint16;
#endif /* kcg_uint16 */

#ifndef kcg_uint8
#define kcg_uint8 kcg_uint8
typedef unsigned char kcg_uint8;
#endif /* kcg_uint8 */

#ifndef kcg_int64
#define kcg_int64 kcg_int64
typedef signed long long kcg_int64;
#endif /* kcg_int64 */

#ifndef kcg_int32
#define kcg_int32 kcg_int32
typedef signed long kcg_int32;
#endif /* kcg_int32 */

#ifndef kcg_int16
#define kcg_int16 kcg_int16
typedef signed short kcg_int16;
#endif /* kcg_int16 */

#ifndef kcg_int8
#define kcg_int8 kcg_int8
typedef signed char kcg_int8;
#endif /* kcg_int8 */

#ifndef kcg_lit_float32
#define kcg_lit_float32(kcg_C1) ((kcg_float32) (kcg_C1))
#endif /* kcg_lit_float32 */

#ifndef kcg_lit_float64
#define kcg_lit_float64(kcg_C1) ((kcg_float64) (kcg_C1))
#endif /* kcg_lit_float64 */

#ifndef kcg_lit_size
#define kcg_lit_size(kcg_C1) ((kcg_size) (kcg_C1))
#endif /* kcg_lit_size */

#ifndef kcg_lit_uint64
#define kcg_lit_uint64(kcg_C1) ((kcg_uint64) (kcg_C1))
#endif /* kcg_lit_uint64 */

#ifndef kcg_lit_uint32
#define kcg_lit_uint32(kcg_C1) ((kcg_uint32) (kcg_C1))
#endif /* kcg_lit_uint32 */

#ifndef kcg_lit_uint16
#define kcg_lit_uint16(kcg_C1) ((kcg_uint16) (kcg_C1))
#endif /* kcg_lit_uint16 */

#ifndef kcg_lit_uint8
#define kcg_lit_uint8(kcg_C1) ((kcg_uint8) (kcg_C1))
#endif /* kcg_lit_uint8 */

#ifndef kcg_lit_int64
#define kcg_lit_int64(kcg_C1) ((kcg_int64) (kcg_C1))
#endif /* kcg_lit_int64 */

#ifndef kcg_lit_int32
#define kcg_lit_int32(kcg_C1) ((kcg_int32) (kcg_C1))
#endif /* kcg_lit_int32 */

#ifndef kcg_lit_int16
#define kcg_lit_int16(kcg_C1) ((kcg_int16) (kcg_C1))
#endif /* kcg_lit_int16 */

#ifndef kcg_lit_int8
#define kcg_lit_int8(kcg_C1) ((kcg_int8) (kcg_C1))
#endif /* kcg_lit_int8 */

#ifndef kcg_false
#define kcg_false ((kcg_bool) 0)
#endif /* kcg_false */

#ifndef kcg_true
#define kcg_true ((kcg_bool) 1)
#endif /* kcg_true */

#ifndef kcg_lsl_uint64
#define kcg_lsl_uint64(kcg_C1, kcg_C2)                                        \
  ((kcg_uint64) ((kcg_C1) << (kcg_C2)) & 0xffffffffffffffff)
#endif /* kcg_lsl_uint64 */

#ifndef kcg_lsl_uint32
#define kcg_lsl_uint32(kcg_C1, kcg_C2)                                        \
  ((kcg_uint32) ((kcg_C1) << (kcg_C2)) & 0xffffffff)
#endif /* kcg_lsl_uint32 */

#ifndef kcg_lsl_uint16
#define kcg_lsl_uint16(kcg_C1, kcg_C2)                                        \
  ((kcg_uint16) ((kcg_C1) << (kcg_C2)) & 0xffff)
#endif /* kcg_lsl_uint16 */

#ifndef kcg_lsl_uint8
#define kcg_lsl_uint8(kcg_C1, kcg_C2)                                         \
  ((kcg_uint8) ((kcg_C1) << (kcg_C2)) & 0xff)
#endif /* kcg_lsl_uint8 */

#ifndef kcg_lnot_uint64
#define kcg_lnot_uint64(kcg_C1) ((kcg_C1) ^ 0xffffffffffffffff)
#endif /* kcg_lnot_uint64 */

#ifndef kcg_lnot_uint32
#define kcg_lnot_uint32(kcg_C1) ((kcg_C1) ^ 0xffffffff)
#endif /* kcg_lnot_uint32 */

#ifndef kcg_lnot_uint16
#define kcg_lnot_uint16(kcg_C1) ((kcg_C1) ^ 0xffff)
#endif /* kcg_lnot_uint16 */

#ifndef kcg_lnot_uint8
#define kcg_lnot_uint8(kcg_C1) ((kcg_C1) ^ 0xff)
#endif /* kcg_lnot_uint8 */

#ifndef kcg_assign
#include "kcg_assign.h"
#endif /* kcg_assign */

#ifndef kcg_assign_struct
#define kcg_assign_struct kcg_assign
#endif /* kcg_assign_struct */

#ifndef kcg_assign_array
#define kcg_assign_array kcg_assign
#endif /* kcg_assign_array */

/* nacl::box::scalarmult/SM1: */
typedef enum kcg_tag_SSM_TR_SM1 {
  SSM_TR_no_trans_SM1,
  SSM_TR_firstPart_donna_1_firstPart_SM1,
  SSM_TR_firstPart_runSingle_2_firstPart_SM1,
  SSM_TR_firstPart_runPart_3_firstPart_SM1,
  SSM_TR_finalPart_fin_1_finalPart_SM1,
  SSM_TR_runPart_finalPart_1_runPart_SM1
} SSM_TR_SM1;
/* nacl::box::scalarmult/SM1: */
typedef enum kcg_tag_SSM_ST_SM1 {
  SSM_st_firstPart_SM1,
  SSM_st_finalPart_SM1,
  SSM_st_runPart_SM1,
  SSM_st_runSingle_SM1,
  SSM_st_fin_SM1,
  SSM_st_donna_SM1
} SSM_ST_SM1;
/* tindyguard::initOur/key_retry: */
typedef enum kcg_tag_SSM_TR_key_retry {
  SSM_TR_no_trans_key_retry,
  SSM_TR_makeKeys_permanentWeakKey_1_makeKeys_key_retry,
  SSM_TR_makeKeys_makeKeys_2_makeKeys_key_retry
} SSM_TR_key_retry;
/* tindyguard::initOur/key_retry: */
typedef enum kcg_tag_SSM_ST_key_retry {
  SSM_st_makeKeys_key_retry,
  SSM_st_permanentWeakKey_key_retry
} SSM_ST_key_retry;
/* test::ietf_RFC7748::bench_x25519/SM3: */
typedef enum kcg_tag_SSM_TR_SM3 {
  SSM_TR_no_trans_SM3,
  SSM_TR_compute_ok_1_1_1_compute_SM3,
  SSM_TR_compute_fail_2_1_1_compute_SM3,
  SSM_TR_compute_more_2_1_compute_SM3,
  SSM_TR_more_compute_1_more_SM3
} SSM_TR_SM3;
/* test::ietf_RFC7748::bench_x25519/SM3: */
typedef enum kcg_tag_SSM_ST_SM3 {
  SSM_st_compute_SM3,
  SSM_st_ok_SM3,
  SSM_st_fail_SM3,
  SSM_st_more_SM3
} SSM_ST_SM3;
typedef kcg_bool array_bool_4[4];

/* hash::sha512::word/ */
typedef kcg_uint64 word_hash_sha512;

typedef kcg_uint64 array_uint64_16[16];

typedef array_uint64_16 array_uint64_16_5[5];

typedef kcg_uint64 array_uint64_8[8];

/* slideTypes::beHash/ */
typedef array_uint64_8 beHash_slideTypes;

/* hash::sha256::word/ */
typedef kcg_uint32 word_hash_sha256;

/* slideTypes::uint/ */
typedef kcg_uint32 uint_slideTypes;

typedef kcg_uint32 array_uint32_14[14];

typedef kcg_uint32 array_uint32_32[32];

typedef kcg_uint32 array_uint32_3[3];

/* nacl::core::chacha::Nonce/ */
typedef array_uint32_3 Nonce_nacl_core_chacha;

/* tindyguardTypes::TAI/ */
typedef array_uint32_3 TAI_tindyguardTypes;

/* nacl::core::chacha::XNonce/, tindyguard::initHashes/ */
typedef kcg_uint32 XNonce_nacl_core_chacha[6];

typedef kcg_uint32 array_uint32_12[12];

typedef kcg_uint32 array_uint32_30[30];

typedef kcg_uint32 array_uint32_1[1];

typedef kcg_uint32 array_uint32_33[33];

typedef kcg_uint32 array_uint32_4[4];

/* hash::blake2s::Mac/ */
typedef array_uint32_4 Mac_hash_blake2s;

/* slideTypes::u324/ */
typedef array_uint32_4 u324_slideTypes;

typedef array_uint32_4 array_uint32_4_4[4];

typedef kcg_uint32 array_uint32_16[16];

/* nacl::sign::Signature/ */
typedef array_uint32_16 Signature_nacl_sign;

/* nacl::core::chacha::State/ */
typedef array_uint32_16 State_nacl_core_chacha;

/* hash::blake2s::u32_16/ */
typedef array_uint32_16 u32_16_hash_blake2s;

/* hash::blake2s::HashChunk/ */
typedef array_uint32_16 HashChunk_hash_blake2s;

/* slideTypes::StreamChunk/ */
typedef array_uint32_16 StreamChunk_slideTypes;

typedef array_uint32_16 array_uint32_16_15[15];

typedef array_uint32_16 array_uint32_16_3[3];

typedef array_uint32_16 array_uint32_16_1[1];

typedef array_uint32_16 array_uint32_16_14[14];

typedef array_uint32_16 array_uint32_16_12[12];

typedef array_uint32_16 array_uint32_16_63[63];

typedef array_uint32_16 array_uint32_16_23[23];

typedef array_uint32_16 array_uint32_16_16[16];

typedef array_uint32_16 array_uint32_16_4[4];

typedef array_uint32_16 array_uint32_16_2[2];

typedef array_uint32_16 array_uint32_16_256[256];

typedef array_uint32_16 array_uint32_16_13[13];

typedef array_uint32_16 array_uint32_16_255[255];

typedef array_uint32_16 array_uint32_16_64[64];

typedef kcg_uint32 array_uint32_48[48];

typedef kcg_uint32 array_uint32_7[7];

typedef kcg_uint32 array_uint32_13[13];

typedef kcg_uint32 array_uint32_2[2];

typedef kcg_uint32 array_uint32_8[8];

/* nacl::core::Key/ */
typedef array_uint32_8 Key_nacl_core;

/* tindyguardTypes::psk/ */
typedef array_uint32_8 psk_tindyguardTypes;

/* tindyguardTypes::secret/ */
typedef array_uint32_8 secret_tindyguardTypes;

/* tindyguardTypes::pub/ */
typedef array_uint32_8 pub_tindyguardTypes;

/* slideTypes::Key32/ */
typedef array_uint32_8 Key32_slideTypes;

typedef Key32_slideTypes array_uint32_8_5[5];

/* slideTypes::KeyPair32/ */
typedef struct kcg_tag_KeyPair32_slideTypes {
  Key32_slideTypes sk_x;
  Key32_slideTypes pk_y;
} KeyPair32_slideTypes;

/* hash::blake2s::Hash/ */
typedef array_uint32_8 Hash_hash_blake2s;

/* tindyguard::handshake::State/ */
typedef struct kcg_tag_State_tindyguard_handshake {
  KeyPair32_slideTypes ephemeral;
  Key32_slideTypes their;
  Hash_hash_blake2s ihash;
  HashChunk_hash_blake2s chainingKey;
} State_tindyguard_handshake;

/* tindyguardTypes::PreHashes/ */
typedef struct kcg_tag_PreHashes_tindyguardTypes {
  StreamChunk_slideTypes salt;
  Hash_hash_blake2s hash1;
  Hash_hash_blake2s cookieHKey;
} PreHashes_tindyguardTypes;

typedef array_uint32_8 array_uint32_8_2[2];

typedef array_uint32_8 array_uint32_8_3[3];

/* udp::IpAddress/ */
typedef struct kcg_tag_IpAddress_udp { kcg_uint32 addr; } IpAddress_udp;

/* udp::range_t/ */
typedef struct kcg_tag_range_t_udp {
  IpAddress_udp net;
  IpAddress_udp mask;
} range_t_udp;

typedef kcg_uint32 array_uint32_17[17];

typedef kcg_uint32 array_uint32_64[64];

typedef kcg_uint8 array_uint8_4[4];

typedef array_uint8_4 array_uint8_4_4[4];

/* nacl::sign::scalarMult/, slideTypes::st32x8/, slideTypes::u848/ */
typedef array_uint8_4 u848_slideTypes[8];

typedef kcg_uint8 array_uint8_16[16];

/* nacl::onetime::Mac/ */
typedef array_uint8_16 Mac_nacl_onetime;

typedef array_uint8_16 array_uint8_16_10[10];

typedef kcg_uint8 array_uint8_13[13];

typedef kcg_uint8 array_uint8_31[31];

typedef kcg_uint8 array_uint8_11[11];

typedef kcg_uint8 array_uint8_17[17];

typedef kcg_uint8 array_uint8_32[32];

typedef kcg_uint8 array_uint8_12[12];

typedef kcg_uint8 array_uint8_24[24];

typedef kcg_int64 array_int64_4[4];

/* nacl::sign::i64416/, nacl::sign::reduce/ */
typedef array_int64_4 i64416_nacl_sign[16];

/* nacl::sign::i6448/, nacl::sign::sign_finalize/ */
typedef array_int64_4 i6448_nacl_sign[8];

/* nacl::op::car25519/, nacl::op::gf/, nacl::op::unpack25519/ */
typedef kcg_int64 gf_nacl_op[16];

/* nacl::op::cpoint/, nacl::sign::cswap/ */
typedef gf_nacl_op cpoint_nacl_op[4];

/* nacl::sign::pq_pair/ */
typedef struct kcg_tag_pq_pair_nacl_sign {
  cpoint_nacl_op p;
  cpoint_nacl_op q;
} pq_pair_nacl_sign;

/* tindyguardTypes::CookieJar/ */
typedef struct kcg_tag_CookieJar_tindyguardTypes {
  StreamChunk_slideTypes content;
  kcg_int64 opened;
} CookieJar_tindyguardTypes;

typedef CookieJar_tindyguardTypes _5_array[2];

/* tindyguardTypes::KeySalted/ */
typedef struct kcg_tag_KeySalted_tindyguardTypes {
  KeyPair32_slideTypes key;
  PreHashes_tindyguardTypes hcache;
  _5_array cookies;
} KeySalted_tindyguardTypes;

typedef kcg_int64 array_int64_31[31];

typedef kcg_int64 array_int64_64[64];

typedef kcg_int64 array_int64_32[32];

typedef kcg_int64 array_int64_15[15];

/* sys::time_t/ */
typedef kcg_int32 time_t_sys;

/* udp::port_t/ */
typedef kcg_int32 port_t_udp;

/* udp::peer_t/ */
typedef struct kcg_tag_peer_t_udp {
  IpAddress_udp addr;
  port_t_udp port;
  time_t_sys time;
  kcg_int64 mtime;
} peer_t_udp;

/* udp::length_t/ */
typedef kcg_int32 length_t_udp;

/* tindyguardTypes::size/ */
typedef kcg_int32 size_tindyguardTypes;

/* tindyguardTypes::Peer/ */
typedef struct kcg_tag_Peer_tindyguardTypes {
  pub_tindyguardTypes tpub;
  psk_tindyguardTypes preshared;
  range_t_udp allowed;
  PreHashes_tindyguardTypes hcache;
  peer_t_udp endpoint;
  TAI_tindyguardTypes tai;
  kcg_bool timedOut;
  size_tindyguardTypes bestSession;
  kcg_bool inhibitInit;
  CookieJar_tindyguardTypes cookie;
} Peer_tindyguardTypes;

typedef Peer_tindyguardTypes _6_array[8];

/* tindyguardTypes::Session/ */
typedef struct kcg_tag_Session_tindyguardTypes {
  secret_tindyguardTypes ot;
  secret_tindyguardTypes to;
  kcg_uint64 ot_cnt;
  kcg_uint64 to_cnt;
  kcg_uint64 to_cnt_cache;
  kcg_uint32 our;
  kcg_uint32 their;
  kcg_int64 rx_bytes;
  size_tindyguardTypes rx_cnt;
  size_tindyguardTypes tx_cnt;
  kcg_int64 txTime;
  kcg_int64 sTime;
  size_tindyguardTypes pid;
  State_tindyguard_handshake handshake;
  Peer_tindyguardTypes peer;
  kcg_bool transmissive;
  kcg_bool gotKeepAlive;
  kcg_bool sentKeepAlive;
} Session_tindyguardTypes;

/* tindyguardTypes::int/ */
typedef kcg_int32 int_tindyguardTypes;

/* slideTypes::size/ */
typedef kcg_int32 size_slideTypes;

/* test::ietf_RFC6234::testcase/ */
typedef struct kcg_tag_testcase_test_ietf_RFC6234 {
  array_uint32_16_12 m;
  size_slideTypes mlen;
  array_uint32_16_2 key;
  size_slideTypes klen;
  kcg_uint32 finbits;
  size_slideTypes finbitslen;
  StreamChunk_slideTypes exp;
} testcase_test_ietf_RFC6234;

typedef testcase_test_ietf_RFC6234 _7_array[4];

typedef testcase_test_ietf_RFC6234 _4_array[9];

/* test::ietf_RFC6234::testcaseHKDF/ */
typedef struct kcg_tag_testcaseHKDF_test_ietf_RFC6234 {
  size_slideTypes ikmlen;
  array_uint32_16_2 ikm;
  size_slideTypes saltlen;
  array_uint32_16_2 salt;
  size_slideTypes infolen;
  array_uint32_16_2 info;
  StreamChunk_slideTypes prk;
  size_slideTypes okmlen;
  array_uint32_16_2 okm;
} testcaseHKDF_test_ietf_RFC6234;

typedef testcaseHKDF_test_ietf_RFC6234 _3_array[3];

/* test::ietf_RFC8032::testcase/ */
typedef struct kcg_tag_testcase_test_ietf_RFC8032 {
  array_uint32_16_1 m;
  size_slideTypes mlen;
  StreamChunk_slideTypes key;
  Signature_nacl_sign signat;
} testcase_test_ietf_RFC8032;

typedef testcase_test_ietf_RFC8032 _2_array[10];

/* test::ietf_RFC7693::testcase_hkdf/ */
typedef struct kcg_tag_testcase_hkdf_test_ietf_RFC7693 {
  array_uint32_16_2 msg;
  size_slideTypes mlen;
  StreamChunk_slideTypes key;
  array_uint32_16_3 exp;
} testcase_hkdf_test_ietf_RFC7693;

/* test::ietf_RFC7693::testcase/ */
typedef struct kcg_tag_testcase_test_ietf_RFC7693 {
  array_uint32_16_2 msg;
  size_slideTypes mlen;
  StreamChunk_slideTypes key;
  size_slideTypes klen;
  StreamChunk_slideTypes exp;
} testcase_test_ietf_RFC7693;

/* test::simpleTC/ */
typedef struct kcg_tag_simpleTC_test {
  array_uint32_16_1 m;
  size_slideTypes mlen;
  StreamChunk_slideTypes key;
} simpleTC_test;

typedef simpleTC_test array[10];

/* slideTypes::int/ */
typedef kcg_int32 int_slideTypes;

/* nacl::box::scalMulAcc/ */
typedef struct kcg_tag_scalMulAcc_nacl_box {
  int_slideTypes i;
  gf_nacl_op a1;
  gf_nacl_op a2;
  gf_nacl_op a3;
  gf_nacl_op a4;
} scalMulAcc_nacl_box;

/* hash::sha512::hashAkkuWithChunk/ */
typedef struct kcg_tag_hashAkkuWithChunk_hash_sha512 {
  array_uint64_8 z;
  kcg_int32 length;
  StreamChunk_slideTypes porch;
} hashAkkuWithChunk_hash_sha512;

/* nacl::sign::prependAkku/ */
typedef struct kcg_tag_prependAkku_nacl_sign {
  hashAkkuWithChunk_hash_sha512 accu;
  array_uint32_8 d;
  array_uint32_8 stash;
} prependAkku_nacl_sign;

#ifndef kcg_copy_Session_tindyguardTypes
#define kcg_copy_Session_tindyguardTypes(kcg_C1, kcg_C2)                      \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (Session_tindyguardTypes)))
#endif /* kcg_copy_Session_tindyguardTypes */

#ifndef kcg_copy_Peer_tindyguardTypes
#define kcg_copy_Peer_tindyguardTypes(kcg_C1, kcg_C2)                         \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (Peer_tindyguardTypes)))
#endif /* kcg_copy_Peer_tindyguardTypes */

#ifndef kcg_copy_KeySalted_tindyguardTypes
#define kcg_copy_KeySalted_tindyguardTypes(kcg_C1, kcg_C2)                    \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (KeySalted_tindyguardTypes)))
#endif /* kcg_copy_KeySalted_tindyguardTypes */

#ifndef kcg_copy_prependAkku_nacl_sign
#define kcg_copy_prependAkku_nacl_sign(kcg_C1, kcg_C2)                        \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (prependAkku_nacl_sign)))
#endif /* kcg_copy_prependAkku_nacl_sign */

#ifndef kcg_copy_IpAddress_udp
#define kcg_copy_IpAddress_udp(kcg_C1, kcg_C2)                                \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (IpAddress_udp)))
#endif /* kcg_copy_IpAddress_udp */

#ifndef kcg_copy_PreHashes_tindyguardTypes
#define kcg_copy_PreHashes_tindyguardTypes(kcg_C1, kcg_C2)                    \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (PreHashes_tindyguardTypes)))
#endif /* kcg_copy_PreHashes_tindyguardTypes */

#ifndef kcg_copy_simpleTC_test
#define kcg_copy_simpleTC_test(kcg_C1, kcg_C2)                                \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (simpleTC_test)))
#endif /* kcg_copy_simpleTC_test */

#ifndef kcg_copy_hashAkkuWithChunk_hash_sha512
#define kcg_copy_hashAkkuWithChunk_hash_sha512(kcg_C1, kcg_C2)                \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (hashAkkuWithChunk_hash_sha512)))
#endif /* kcg_copy_hashAkkuWithChunk_hash_sha512 */

#ifndef kcg_copy_testcase_test_ietf_RFC7693
#define kcg_copy_testcase_test_ietf_RFC7693(kcg_C1, kcg_C2)                   \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (testcase_test_ietf_RFC7693)))
#endif /* kcg_copy_testcase_test_ietf_RFC7693 */

#ifndef kcg_copy_scalMulAcc_nacl_box
#define kcg_copy_scalMulAcc_nacl_box(kcg_C1, kcg_C2)                          \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (scalMulAcc_nacl_box)))
#endif /* kcg_copy_scalMulAcc_nacl_box */

#ifndef kcg_copy_testcase_hkdf_test_ietf_RFC7693
#define kcg_copy_testcase_hkdf_test_ietf_RFC7693(kcg_C1, kcg_C2)              \
  (kcg_assign_struct(                                                         \
      (kcg_C1),                                                               \
      (kcg_C2),                                                               \
      sizeof (testcase_hkdf_test_ietf_RFC7693)))
#endif /* kcg_copy_testcase_hkdf_test_ietf_RFC7693 */

#ifndef kcg_copy_testcase_test_ietf_RFC8032
#define kcg_copy_testcase_test_ietf_RFC8032(kcg_C1, kcg_C2)                   \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (testcase_test_ietf_RFC8032)))
#endif /* kcg_copy_testcase_test_ietf_RFC8032 */

#ifndef kcg_copy_State_tindyguard_handshake
#define kcg_copy_State_tindyguard_handshake(kcg_C1, kcg_C2)                   \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (State_tindyguard_handshake)))
#endif /* kcg_copy_State_tindyguard_handshake */

#ifndef kcg_copy_KeyPair32_slideTypes
#define kcg_copy_KeyPair32_slideTypes(kcg_C1, kcg_C2)                         \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (KeyPair32_slideTypes)))
#endif /* kcg_copy_KeyPair32_slideTypes */

#ifndef kcg_copy_pq_pair_nacl_sign
#define kcg_copy_pq_pair_nacl_sign(kcg_C1, kcg_C2)                            \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (pq_pair_nacl_sign)))
#endif /* kcg_copy_pq_pair_nacl_sign */

#ifndef kcg_copy_testcaseHKDF_test_ietf_RFC6234
#define kcg_copy_testcaseHKDF_test_ietf_RFC6234(kcg_C1, kcg_C2)               \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (testcaseHKDF_test_ietf_RFC6234)))
#endif /* kcg_copy_testcaseHKDF_test_ietf_RFC6234 */

#ifndef kcg_copy_range_t_udp
#define kcg_copy_range_t_udp(kcg_C1, kcg_C2)                                  \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (range_t_udp)))
#endif /* kcg_copy_range_t_udp */

#ifndef kcg_copy_peer_t_udp
#define kcg_copy_peer_t_udp(kcg_C1, kcg_C2)                                   \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (peer_t_udp)))
#endif /* kcg_copy_peer_t_udp */

#ifndef kcg_copy_CookieJar_tindyguardTypes
#define kcg_copy_CookieJar_tindyguardTypes(kcg_C1, kcg_C2)                    \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (CookieJar_tindyguardTypes)))
#endif /* kcg_copy_CookieJar_tindyguardTypes */

#ifndef kcg_copy_testcase_test_ietf_RFC6234
#define kcg_copy_testcase_test_ietf_RFC6234(kcg_C1, kcg_C2)                   \
  (kcg_assign_struct((kcg_C1), (kcg_C2), sizeof (testcase_test_ietf_RFC6234)))
#endif /* kcg_copy_testcase_test_ietf_RFC6234 */

#ifndef kcg_copy_array_uint8_24
#define kcg_copy_array_uint8_24(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_24)))
#endif /* kcg_copy_array_uint8_24 */

#ifndef kcg_copy_array
#define kcg_copy_array(kcg_C1, kcg_C2)                                        \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array)))
#endif /* kcg_copy_array */

#ifndef kcg_copy__2_array
#define kcg_copy__2_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_2_array)))
#endif /* kcg_copy__2_array */

#ifndef kcg_copy_array_uint32_16_64
#define kcg_copy_array_uint32_16_64(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_64)))
#endif /* kcg_copy_array_uint32_16_64 */

#ifndef kcg_copy_array_uint8_12
#define kcg_copy_array_uint8_12(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_12)))
#endif /* kcg_copy_array_uint8_12 */

#ifndef kcg_copy_array_uint32_16_255
#define kcg_copy_array_uint32_16_255(kcg_C1, kcg_C2)                          \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_255)))
#endif /* kcg_copy_array_uint32_16_255 */

#ifndef kcg_copy__3_array
#define kcg_copy__3_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_3_array)))
#endif /* kcg_copy__3_array */

#ifndef kcg_copy_array_uint32_16_13
#define kcg_copy_array_uint32_16_13(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_13)))
#endif /* kcg_copy_array_uint32_16_13 */

#ifndef kcg_copy_array_uint32_64
#define kcg_copy_array_uint32_64(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_64)))
#endif /* kcg_copy_array_uint32_64 */

#ifndef kcg_copy_array_uint32_17
#define kcg_copy_array_uint32_17(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_17)))
#endif /* kcg_copy_array_uint32_17 */

#ifndef kcg_copy_array_uint64_8
#define kcg_copy_array_uint64_8(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint64_8)))
#endif /* kcg_copy_array_uint64_8 */

#ifndef kcg_copy_array_uint32_8_3
#define kcg_copy_array_uint32_8_3(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8_3)))
#endif /* kcg_copy_array_uint32_8_3 */

#ifndef kcg_copy_array_uint8_32
#define kcg_copy_array_uint8_32(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_32)))
#endif /* kcg_copy_array_uint8_32 */

#ifndef kcg_copy_array_uint32_16_256
#define kcg_copy_array_uint32_16_256(kcg_C1, kcg_C2)                          \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_256)))
#endif /* kcg_copy_array_uint32_16_256 */

#ifndef kcg_copy_array_int64_15
#define kcg_copy_array_int64_15(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int64_15)))
#endif /* kcg_copy_array_int64_15 */

#ifndef kcg_copy_array_uint32_8
#define kcg_copy_array_uint32_8(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8)))
#endif /* kcg_copy_array_uint32_8 */

#ifndef kcg_copy_array_uint32_2
#define kcg_copy_array_uint32_2(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_2)))
#endif /* kcg_copy_array_uint32_2 */

#ifndef kcg_copy__4_array
#define kcg_copy__4_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_4_array)))
#endif /* kcg_copy__4_array */

#ifndef kcg_copy_array_uint32_16_2
#define kcg_copy_array_uint32_16_2(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_2)))
#endif /* kcg_copy_array_uint32_16_2 */

#ifndef kcg_copy_i6448_nacl_sign
#define kcg_copy_i6448_nacl_sign(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (i6448_nacl_sign)))
#endif /* kcg_copy_i6448_nacl_sign */

#ifndef kcg_copy_u848_slideTypes
#define kcg_copy_u848_slideTypes(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (u848_slideTypes)))
#endif /* kcg_copy_u848_slideTypes */

#ifndef kcg_copy_array_int64_32
#define kcg_copy_array_int64_32(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int64_32)))
#endif /* kcg_copy_array_int64_32 */

#ifndef kcg_copy_array_uint32_16_4
#define kcg_copy_array_uint32_16_4(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_4)))
#endif /* kcg_copy_array_uint32_16_4 */

#ifndef kcg_copy_cpoint_nacl_op
#define kcg_copy_cpoint_nacl_op(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (cpoint_nacl_op)))
#endif /* kcg_copy_cpoint_nacl_op */

#ifndef kcg_copy_array_uint32_16_16
#define kcg_copy_array_uint32_16_16(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_16)))
#endif /* kcg_copy_array_uint32_16_16 */

#ifndef kcg_copy_array_uint32_13
#define kcg_copy_array_uint32_13(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_13)))
#endif /* kcg_copy_array_uint32_13 */

#ifndef kcg_copy_array_uint32_7
#define kcg_copy_array_uint32_7(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_7)))
#endif /* kcg_copy_array_uint32_7 */

#ifndef kcg_copy_array_uint64_16
#define kcg_copy_array_uint64_16(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint64_16)))
#endif /* kcg_copy_array_uint64_16 */

#ifndef kcg_copy__5_array
#define kcg_copy__5_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_5_array)))
#endif /* kcg_copy__5_array */

#ifndef kcg_copy_array_uint32_16_23
#define kcg_copy_array_uint32_16_23(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_23)))
#endif /* kcg_copy_array_uint32_16_23 */

#ifndef kcg_copy_array_uint32_48
#define kcg_copy_array_uint32_48(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_48)))
#endif /* kcg_copy_array_uint32_48 */

#ifndef kcg_copy_array_uint8_17
#define kcg_copy_array_uint8_17(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_17)))
#endif /* kcg_copy_array_uint8_17 */

#ifndef kcg_copy_array_uint8_11
#define kcg_copy_array_uint8_11(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_11)))
#endif /* kcg_copy_array_uint8_11 */

#ifndef kcg_copy_array_uint32_16_63
#define kcg_copy_array_uint32_16_63(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_63)))
#endif /* kcg_copy_array_uint32_16_63 */

#ifndef kcg_copy_array_uint32_16
#define kcg_copy_array_uint32_16(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16)))
#endif /* kcg_copy_array_uint32_16 */

#ifndef kcg_copy_array_int64_64
#define kcg_copy_array_int64_64(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int64_64)))
#endif /* kcg_copy_array_int64_64 */

#ifndef kcg_copy_array_uint32_4
#define kcg_copy_array_uint32_4(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_4)))
#endif /* kcg_copy_array_uint32_4 */

#ifndef kcg_copy_array_bool_4
#define kcg_copy_array_bool_4(kcg_C1, kcg_C2)                                 \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_bool_4)))
#endif /* kcg_copy_array_bool_4 */

#ifndef kcg_copy_array_uint32_33
#define kcg_copy_array_uint32_33(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_33)))
#endif /* kcg_copy_array_uint32_33 */

#ifndef kcg_copy_array_uint64_16_5
#define kcg_copy_array_uint64_16_5(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint64_16_5)))
#endif /* kcg_copy_array_uint64_16_5 */

#ifndef kcg_copy_array_uint8_31
#define kcg_copy_array_uint8_31(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_31)))
#endif /* kcg_copy_array_uint8_31 */

#ifndef kcg_copy_array_uint32_16_12
#define kcg_copy_array_uint32_16_12(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_12)))
#endif /* kcg_copy_array_uint32_16_12 */

#ifndef kcg_copy__6_array
#define kcg_copy__6_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_6_array)))
#endif /* kcg_copy__6_array */

#ifndef kcg_copy_array_uint32_16_14
#define kcg_copy_array_uint32_16_14(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_14)))
#endif /* kcg_copy_array_uint32_16_14 */

#ifndef kcg_copy_array_uint8_13
#define kcg_copy_array_uint8_13(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_13)))
#endif /* kcg_copy_array_uint8_13 */

#ifndef kcg_copy_array_uint32_8_2
#define kcg_copy_array_uint32_8_2(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8_2)))
#endif /* kcg_copy_array_uint32_8_2 */

#ifndef kcg_copy_array_uint32_1
#define kcg_copy_array_uint32_1(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_1)))
#endif /* kcg_copy_array_uint32_1 */

#ifndef kcg_copy_array_uint32_30
#define kcg_copy_array_uint32_30(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_30)))
#endif /* kcg_copy_array_uint32_30 */

#ifndef kcg_copy_array_int64_31
#define kcg_copy_array_int64_31(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int64_31)))
#endif /* kcg_copy_array_int64_31 */

#ifndef kcg_copy_array_uint32_12
#define kcg_copy_array_uint32_12(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_12)))
#endif /* kcg_copy_array_uint32_12 */

#ifndef kcg_copy_XNonce_nacl_core_chacha
#define kcg_copy_XNonce_nacl_core_chacha(kcg_C1, kcg_C2)                      \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (XNonce_nacl_core_chacha)))
#endif /* kcg_copy_XNonce_nacl_core_chacha */

#ifndef kcg_copy_array_uint8_16
#define kcg_copy_array_uint8_16(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_16)))
#endif /* kcg_copy_array_uint8_16 */

#ifndef kcg_copy__7_array
#define kcg_copy__7_array(kcg_C1, kcg_C2)                                     \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (_7_array)))
#endif /* kcg_copy__7_array */

#ifndef kcg_copy_array_uint8_4
#define kcg_copy_array_uint8_4(kcg_C1, kcg_C2)                                \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_4)))
#endif /* kcg_copy_array_uint8_4 */

#ifndef kcg_copy_array_uint32_16_1
#define kcg_copy_array_uint32_16_1(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_1)))
#endif /* kcg_copy_array_uint32_16_1 */

#ifndef kcg_copy_array_uint32_16_3
#define kcg_copy_array_uint32_16_3(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_3)))
#endif /* kcg_copy_array_uint32_16_3 */

#ifndef kcg_copy_array_uint32_16_15
#define kcg_copy_array_uint32_16_15(kcg_C1, kcg_C2)                           \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_16_15)))
#endif /* kcg_copy_array_uint32_16_15 */

#ifndef kcg_copy_array_uint8_4_4
#define kcg_copy_array_uint8_4_4(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_4_4)))
#endif /* kcg_copy_array_uint8_4_4 */

#ifndef kcg_copy_array_uint32_4_4
#define kcg_copy_array_uint32_4_4(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_4_4)))
#endif /* kcg_copy_array_uint32_4_4 */

#ifndef kcg_copy_i64416_nacl_sign
#define kcg_copy_i64416_nacl_sign(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (i64416_nacl_sign)))
#endif /* kcg_copy_i64416_nacl_sign */

#ifndef kcg_copy_array_uint8_16_10
#define kcg_copy_array_uint8_16_10(kcg_C1, kcg_C2)                            \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint8_16_10)))
#endif /* kcg_copy_array_uint8_16_10 */

#ifndef kcg_copy_array_uint32_8_5
#define kcg_copy_array_uint32_8_5(kcg_C1, kcg_C2)                             \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_8_5)))
#endif /* kcg_copy_array_uint32_8_5 */

#ifndef kcg_copy_gf_nacl_op
#define kcg_copy_gf_nacl_op(kcg_C1, kcg_C2)                                   \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (gf_nacl_op)))
#endif /* kcg_copy_gf_nacl_op */

#ifndef kcg_copy_array_uint32_3
#define kcg_copy_array_uint32_3(kcg_C1, kcg_C2)                               \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_3)))
#endif /* kcg_copy_array_uint32_3 */

#ifndef kcg_copy_array_int64_4
#define kcg_copy_array_int64_4(kcg_C1, kcg_C2)                                \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_int64_4)))
#endif /* kcg_copy_array_int64_4 */

#ifndef kcg_copy_array_uint32_32
#define kcg_copy_array_uint32_32(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_32)))
#endif /* kcg_copy_array_uint32_32 */

#ifndef kcg_copy_array_uint32_14
#define kcg_copy_array_uint32_14(kcg_C1, kcg_C2)                              \
  (kcg_assign_array((kcg_C1), (kcg_C2), sizeof (array_uint32_14)))
#endif /* kcg_copy_array_uint32_14 */

#ifdef kcg_use_Session_tindyguardTypes
#ifndef kcg_comp_Session_tindyguardTypes
extern kcg_bool kcg_comp_Session_tindyguardTypes(
  Session_tindyguardTypes *kcg_c1,
  Session_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_Session_tindyguardTypes */
#endif /* kcg_use_Session_tindyguardTypes */

#ifdef kcg_use_Peer_tindyguardTypes
#ifndef kcg_comp_Peer_tindyguardTypes
extern kcg_bool kcg_comp_Peer_tindyguardTypes(
  Peer_tindyguardTypes *kcg_c1,
  Peer_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_Peer_tindyguardTypes */
#endif /* kcg_use_Peer_tindyguardTypes */

#ifdef kcg_use_KeySalted_tindyguardTypes
#ifndef kcg_comp_KeySalted_tindyguardTypes
extern kcg_bool kcg_comp_KeySalted_tindyguardTypes(
  KeySalted_tindyguardTypes *kcg_c1,
  KeySalted_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_KeySalted_tindyguardTypes */
#endif /* kcg_use_KeySalted_tindyguardTypes */

#ifdef kcg_use_prependAkku_nacl_sign
#ifndef kcg_comp_prependAkku_nacl_sign
extern kcg_bool kcg_comp_prependAkku_nacl_sign(
  prependAkku_nacl_sign *kcg_c1,
  prependAkku_nacl_sign *kcg_c2);
#endif /* kcg_comp_prependAkku_nacl_sign */
#endif /* kcg_use_prependAkku_nacl_sign */

#ifdef kcg_use_IpAddress_udp
#ifndef kcg_comp_IpAddress_udp
extern kcg_bool kcg_comp_IpAddress_udp(
  IpAddress_udp *kcg_c1,
  IpAddress_udp *kcg_c2);
#endif /* kcg_comp_IpAddress_udp */
#endif /* kcg_use_IpAddress_udp */

#ifdef kcg_use_PreHashes_tindyguardTypes
#ifndef kcg_comp_PreHashes_tindyguardTypes
extern kcg_bool kcg_comp_PreHashes_tindyguardTypes(
  PreHashes_tindyguardTypes *kcg_c1,
  PreHashes_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_PreHashes_tindyguardTypes */
#endif /* kcg_use_PreHashes_tindyguardTypes */

#ifdef kcg_use_simpleTC_test
#ifndef kcg_comp_simpleTC_test
extern kcg_bool kcg_comp_simpleTC_test(
  simpleTC_test *kcg_c1,
  simpleTC_test *kcg_c2);
#endif /* kcg_comp_simpleTC_test */
#endif /* kcg_use_simpleTC_test */

#ifdef kcg_use_hashAkkuWithChunk_hash_sha512
#ifndef kcg_comp_hashAkkuWithChunk_hash_sha512
extern kcg_bool kcg_comp_hashAkkuWithChunk_hash_sha512(
  hashAkkuWithChunk_hash_sha512 *kcg_c1,
  hashAkkuWithChunk_hash_sha512 *kcg_c2);
#endif /* kcg_comp_hashAkkuWithChunk_hash_sha512 */
#endif /* kcg_use_hashAkkuWithChunk_hash_sha512 */

#ifdef kcg_use_testcase_test_ietf_RFC7693
#ifndef kcg_comp_testcase_test_ietf_RFC7693
extern kcg_bool kcg_comp_testcase_test_ietf_RFC7693(
  testcase_test_ietf_RFC7693 *kcg_c1,
  testcase_test_ietf_RFC7693 *kcg_c2);
#endif /* kcg_comp_testcase_test_ietf_RFC7693 */
#endif /* kcg_use_testcase_test_ietf_RFC7693 */

#ifdef kcg_use_scalMulAcc_nacl_box
#ifndef kcg_comp_scalMulAcc_nacl_box
extern kcg_bool kcg_comp_scalMulAcc_nacl_box(
  scalMulAcc_nacl_box *kcg_c1,
  scalMulAcc_nacl_box *kcg_c2);
#endif /* kcg_comp_scalMulAcc_nacl_box */
#endif /* kcg_use_scalMulAcc_nacl_box */

#ifdef kcg_use_testcase_hkdf_test_ietf_RFC7693
#ifndef kcg_comp_testcase_hkdf_test_ietf_RFC7693
extern kcg_bool kcg_comp_testcase_hkdf_test_ietf_RFC7693(
  testcase_hkdf_test_ietf_RFC7693 *kcg_c1,
  testcase_hkdf_test_ietf_RFC7693 *kcg_c2);
#endif /* kcg_comp_testcase_hkdf_test_ietf_RFC7693 */
#endif /* kcg_use_testcase_hkdf_test_ietf_RFC7693 */

#ifdef kcg_use_testcase_test_ietf_RFC8032
#ifndef kcg_comp_testcase_test_ietf_RFC8032
extern kcg_bool kcg_comp_testcase_test_ietf_RFC8032(
  testcase_test_ietf_RFC8032 *kcg_c1,
  testcase_test_ietf_RFC8032 *kcg_c2);
#endif /* kcg_comp_testcase_test_ietf_RFC8032 */
#endif /* kcg_use_testcase_test_ietf_RFC8032 */

#ifdef kcg_use_State_tindyguard_handshake
#ifndef kcg_comp_State_tindyguard_handshake
extern kcg_bool kcg_comp_State_tindyguard_handshake(
  State_tindyguard_handshake *kcg_c1,
  State_tindyguard_handshake *kcg_c2);
#endif /* kcg_comp_State_tindyguard_handshake */
#endif /* kcg_use_State_tindyguard_handshake */

#ifdef kcg_use_KeyPair32_slideTypes
#ifndef kcg_comp_KeyPair32_slideTypes
extern kcg_bool kcg_comp_KeyPair32_slideTypes(
  KeyPair32_slideTypes *kcg_c1,
  KeyPair32_slideTypes *kcg_c2);
#endif /* kcg_comp_KeyPair32_slideTypes */
#endif /* kcg_use_KeyPair32_slideTypes */

#ifdef kcg_use_pq_pair_nacl_sign
#ifndef kcg_comp_pq_pair_nacl_sign
extern kcg_bool kcg_comp_pq_pair_nacl_sign(
  pq_pair_nacl_sign *kcg_c1,
  pq_pair_nacl_sign *kcg_c2);
#endif /* kcg_comp_pq_pair_nacl_sign */
#endif /* kcg_use_pq_pair_nacl_sign */

#ifdef kcg_use_testcaseHKDF_test_ietf_RFC6234
#ifndef kcg_comp_testcaseHKDF_test_ietf_RFC6234
extern kcg_bool kcg_comp_testcaseHKDF_test_ietf_RFC6234(
  testcaseHKDF_test_ietf_RFC6234 *kcg_c1,
  testcaseHKDF_test_ietf_RFC6234 *kcg_c2);
#endif /* kcg_comp_testcaseHKDF_test_ietf_RFC6234 */
#endif /* kcg_use_testcaseHKDF_test_ietf_RFC6234 */

#ifdef kcg_use_range_t_udp
#ifndef kcg_comp_range_t_udp
extern kcg_bool kcg_comp_range_t_udp(range_t_udp *kcg_c1, range_t_udp *kcg_c2);
#endif /* kcg_comp_range_t_udp */
#endif /* kcg_use_range_t_udp */

#ifdef kcg_use_peer_t_udp
#ifndef kcg_comp_peer_t_udp
extern kcg_bool kcg_comp_peer_t_udp(peer_t_udp *kcg_c1, peer_t_udp *kcg_c2);
#endif /* kcg_comp_peer_t_udp */
#endif /* kcg_use_peer_t_udp */

#ifdef kcg_use_CookieJar_tindyguardTypes
#ifndef kcg_comp_CookieJar_tindyguardTypes
extern kcg_bool kcg_comp_CookieJar_tindyguardTypes(
  CookieJar_tindyguardTypes *kcg_c1,
  CookieJar_tindyguardTypes *kcg_c2);
#endif /* kcg_comp_CookieJar_tindyguardTypes */
#endif /* kcg_use_CookieJar_tindyguardTypes */

#ifdef kcg_use_testcase_test_ietf_RFC6234
#ifndef kcg_comp_testcase_test_ietf_RFC6234
extern kcg_bool kcg_comp_testcase_test_ietf_RFC6234(
  testcase_test_ietf_RFC6234 *kcg_c1,
  testcase_test_ietf_RFC6234 *kcg_c2);
#endif /* kcg_comp_testcase_test_ietf_RFC6234 */
#endif /* kcg_use_testcase_test_ietf_RFC6234 */

#ifdef kcg_use_array_uint8_24
#ifndef kcg_comp_array_uint8_24
extern kcg_bool kcg_comp_array_uint8_24(
  array_uint8_24 *kcg_c1,
  array_uint8_24 *kcg_c2);
#endif /* kcg_comp_array_uint8_24 */
#endif /* kcg_use_array_uint8_24 */

#ifdef kcg_use_array
#ifndef kcg_comp_array
extern kcg_bool kcg_comp_array(array *kcg_c1, array *kcg_c2);
#endif /* kcg_comp_array */
#endif /* kcg_use_array */

#ifdef kcg_use__2_array
#ifndef kcg_comp__2_array
extern kcg_bool kcg_comp__2_array(_2_array *kcg_c1, _2_array *kcg_c2);
#endif /* kcg_comp__2_array */
#endif /* kcg_use__2_array */

#ifndef kcg_comp_array_uint32_16_64
extern kcg_bool kcg_comp_array_uint32_16_64(
  array_uint32_16_64 *kcg_c1,
  array_uint32_16_64 *kcg_c2);
#define kcg_use_array_uint32_16_64
#endif /* kcg_comp_array_uint32_16_64 */

#ifdef kcg_use_array_uint8_12
#ifndef kcg_comp_array_uint8_12
extern kcg_bool kcg_comp_array_uint8_12(
  array_uint8_12 *kcg_c1,
  array_uint8_12 *kcg_c2);
#endif /* kcg_comp_array_uint8_12 */
#endif /* kcg_use_array_uint8_12 */

#ifdef kcg_use_array_uint32_16_255
#ifndef kcg_comp_array_uint32_16_255
extern kcg_bool kcg_comp_array_uint32_16_255(
  array_uint32_16_255 *kcg_c1,
  array_uint32_16_255 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_255 */
#endif /* kcg_use_array_uint32_16_255 */

#ifdef kcg_use__3_array
#ifndef kcg_comp__3_array
extern kcg_bool kcg_comp__3_array(_3_array *kcg_c1, _3_array *kcg_c2);
#endif /* kcg_comp__3_array */
#endif /* kcg_use__3_array */

#ifdef kcg_use_array_uint32_16_13
#ifndef kcg_comp_array_uint32_16_13
extern kcg_bool kcg_comp_array_uint32_16_13(
  array_uint32_16_13 *kcg_c1,
  array_uint32_16_13 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_13 */
#endif /* kcg_use_array_uint32_16_13 */

#ifdef kcg_use_array_uint32_64
#ifndef kcg_comp_array_uint32_64
extern kcg_bool kcg_comp_array_uint32_64(
  array_uint32_64 *kcg_c1,
  array_uint32_64 *kcg_c2);
#endif /* kcg_comp_array_uint32_64 */
#endif /* kcg_use_array_uint32_64 */

#ifdef kcg_use_array_uint32_17
#ifndef kcg_comp_array_uint32_17
extern kcg_bool kcg_comp_array_uint32_17(
  array_uint32_17 *kcg_c1,
  array_uint32_17 *kcg_c2);
#endif /* kcg_comp_array_uint32_17 */
#endif /* kcg_use_array_uint32_17 */

#ifdef kcg_use_array_uint64_8
#ifndef kcg_comp_array_uint64_8
extern kcg_bool kcg_comp_array_uint64_8(
  array_uint64_8 *kcg_c1,
  array_uint64_8 *kcg_c2);
#endif /* kcg_comp_array_uint64_8 */
#endif /* kcg_use_array_uint64_8 */

#ifdef kcg_use_array_uint32_8_3
#ifndef kcg_comp_array_uint32_8_3
extern kcg_bool kcg_comp_array_uint32_8_3(
  array_uint32_8_3 *kcg_c1,
  array_uint32_8_3 *kcg_c2);
#endif /* kcg_comp_array_uint32_8_3 */
#endif /* kcg_use_array_uint32_8_3 */

#ifdef kcg_use_array_uint8_32
#ifndef kcg_comp_array_uint8_32
extern kcg_bool kcg_comp_array_uint8_32(
  array_uint8_32 *kcg_c1,
  array_uint8_32 *kcg_c2);
#endif /* kcg_comp_array_uint8_32 */
#endif /* kcg_use_array_uint8_32 */

#ifndef kcg_comp_array_uint32_16_256
extern kcg_bool kcg_comp_array_uint32_16_256(
  array_uint32_16_256 *kcg_c1,
  array_uint32_16_256 *kcg_c2);
#define kcg_use_array_uint32_16_256
#endif /* kcg_comp_array_uint32_16_256 */

#ifdef kcg_use_array_int64_15
#ifndef kcg_comp_array_int64_15
extern kcg_bool kcg_comp_array_int64_15(
  array_int64_15 *kcg_c1,
  array_int64_15 *kcg_c2);
#endif /* kcg_comp_array_int64_15 */
#endif /* kcg_use_array_int64_15 */

#ifndef kcg_comp_array_uint32_8
extern kcg_bool kcg_comp_array_uint32_8(
  array_uint32_8 *kcg_c1,
  array_uint32_8 *kcg_c2);
#define kcg_use_array_uint32_8
#endif /* kcg_comp_array_uint32_8 */

#ifdef kcg_use_array_uint32_2
#ifndef kcg_comp_array_uint32_2
extern kcg_bool kcg_comp_array_uint32_2(
  array_uint32_2 *kcg_c1,
  array_uint32_2 *kcg_c2);
#endif /* kcg_comp_array_uint32_2 */
#endif /* kcg_use_array_uint32_2 */

#ifdef kcg_use__4_array
#ifndef kcg_comp__4_array
extern kcg_bool kcg_comp__4_array(_4_array *kcg_c1, _4_array *kcg_c2);
#endif /* kcg_comp__4_array */
#endif /* kcg_use__4_array */

#ifndef kcg_comp_array_uint32_16_2
extern kcg_bool kcg_comp_array_uint32_16_2(
  array_uint32_16_2 *kcg_c1,
  array_uint32_16_2 *kcg_c2);
#define kcg_use_array_uint32_16_2
#endif /* kcg_comp_array_uint32_16_2 */

#ifdef kcg_use_i6448_nacl_sign
#ifndef kcg_comp_i6448_nacl_sign
extern kcg_bool kcg_comp_i6448_nacl_sign(
  i6448_nacl_sign *kcg_c1,
  i6448_nacl_sign *kcg_c2);
#endif /* kcg_comp_i6448_nacl_sign */
#endif /* kcg_use_i6448_nacl_sign */

#ifdef kcg_use_u848_slideTypes
#ifndef kcg_comp_u848_slideTypes
extern kcg_bool kcg_comp_u848_slideTypes(
  u848_slideTypes *kcg_c1,
  u848_slideTypes *kcg_c2);
#endif /* kcg_comp_u848_slideTypes */
#endif /* kcg_use_u848_slideTypes */

#ifdef kcg_use_array_int64_32
#ifndef kcg_comp_array_int64_32
extern kcg_bool kcg_comp_array_int64_32(
  array_int64_32 *kcg_c1,
  array_int64_32 *kcg_c2);
#endif /* kcg_comp_array_int64_32 */
#endif /* kcg_use_array_int64_32 */

#ifdef kcg_use_array_uint32_16_4
#ifndef kcg_comp_array_uint32_16_4
extern kcg_bool kcg_comp_array_uint32_16_4(
  array_uint32_16_4 *kcg_c1,
  array_uint32_16_4 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_4 */
#endif /* kcg_use_array_uint32_16_4 */

#ifdef kcg_use_cpoint_nacl_op
#ifndef kcg_comp_cpoint_nacl_op
extern kcg_bool kcg_comp_cpoint_nacl_op(
  cpoint_nacl_op *kcg_c1,
  cpoint_nacl_op *kcg_c2);
#endif /* kcg_comp_cpoint_nacl_op */
#endif /* kcg_use_cpoint_nacl_op */

#ifdef kcg_use_array_uint32_16_16
#ifndef kcg_comp_array_uint32_16_16
extern kcg_bool kcg_comp_array_uint32_16_16(
  array_uint32_16_16 *kcg_c1,
  array_uint32_16_16 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_16 */
#endif /* kcg_use_array_uint32_16_16 */

#ifdef kcg_use_array_uint32_13
#ifndef kcg_comp_array_uint32_13
extern kcg_bool kcg_comp_array_uint32_13(
  array_uint32_13 *kcg_c1,
  array_uint32_13 *kcg_c2);
#endif /* kcg_comp_array_uint32_13 */
#endif /* kcg_use_array_uint32_13 */

#ifdef kcg_use_array_uint32_7
#ifndef kcg_comp_array_uint32_7
extern kcg_bool kcg_comp_array_uint32_7(
  array_uint32_7 *kcg_c1,
  array_uint32_7 *kcg_c2);
#endif /* kcg_comp_array_uint32_7 */
#endif /* kcg_use_array_uint32_7 */

#ifdef kcg_use_array_uint64_16
#ifndef kcg_comp_array_uint64_16
extern kcg_bool kcg_comp_array_uint64_16(
  array_uint64_16 *kcg_c1,
  array_uint64_16 *kcg_c2);
#endif /* kcg_comp_array_uint64_16 */
#endif /* kcg_use_array_uint64_16 */

#ifdef kcg_use__5_array
#ifndef kcg_comp__5_array
extern kcg_bool kcg_comp__5_array(_5_array *kcg_c1, _5_array *kcg_c2);
#endif /* kcg_comp__5_array */
#endif /* kcg_use__5_array */

#ifndef kcg_comp_array_uint32_16_23
extern kcg_bool kcg_comp_array_uint32_16_23(
  array_uint32_16_23 *kcg_c1,
  array_uint32_16_23 *kcg_c2);
#define kcg_use_array_uint32_16_23
#endif /* kcg_comp_array_uint32_16_23 */

#ifdef kcg_use_array_uint32_48
#ifndef kcg_comp_array_uint32_48
extern kcg_bool kcg_comp_array_uint32_48(
  array_uint32_48 *kcg_c1,
  array_uint32_48 *kcg_c2);
#endif /* kcg_comp_array_uint32_48 */
#endif /* kcg_use_array_uint32_48 */

#ifdef kcg_use_array_uint8_17
#ifndef kcg_comp_array_uint8_17
extern kcg_bool kcg_comp_array_uint8_17(
  array_uint8_17 *kcg_c1,
  array_uint8_17 *kcg_c2);
#endif /* kcg_comp_array_uint8_17 */
#endif /* kcg_use_array_uint8_17 */

#ifdef kcg_use_array_uint8_11
#ifndef kcg_comp_array_uint8_11
extern kcg_bool kcg_comp_array_uint8_11(
  array_uint8_11 *kcg_c1,
  array_uint8_11 *kcg_c2);
#endif /* kcg_comp_array_uint8_11 */
#endif /* kcg_use_array_uint8_11 */

#ifdef kcg_use_array_uint32_16_63
#ifndef kcg_comp_array_uint32_16_63
extern kcg_bool kcg_comp_array_uint32_16_63(
  array_uint32_16_63 *kcg_c1,
  array_uint32_16_63 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_63 */
#endif /* kcg_use_array_uint32_16_63 */

#ifndef kcg_comp_array_uint32_16
extern kcg_bool kcg_comp_array_uint32_16(
  array_uint32_16 *kcg_c1,
  array_uint32_16 *kcg_c2);
#define kcg_use_array_uint32_16
#endif /* kcg_comp_array_uint32_16 */

#ifdef kcg_use_array_int64_64
#ifndef kcg_comp_array_int64_64
extern kcg_bool kcg_comp_array_int64_64(
  array_int64_64 *kcg_c1,
  array_int64_64 *kcg_c2);
#endif /* kcg_comp_array_int64_64 */
#endif /* kcg_use_array_int64_64 */

#ifndef kcg_comp_array_uint32_4
extern kcg_bool kcg_comp_array_uint32_4(
  array_uint32_4 *kcg_c1,
  array_uint32_4 *kcg_c2);
#define kcg_use_array_uint32_4
#endif /* kcg_comp_array_uint32_4 */

#ifdef kcg_use_array_bool_4
#ifndef kcg_comp_array_bool_4
extern kcg_bool kcg_comp_array_bool_4(
  array_bool_4 *kcg_c1,
  array_bool_4 *kcg_c2);
#endif /* kcg_comp_array_bool_4 */
#endif /* kcg_use_array_bool_4 */

#ifdef kcg_use_array_uint32_33
#ifndef kcg_comp_array_uint32_33
extern kcg_bool kcg_comp_array_uint32_33(
  array_uint32_33 *kcg_c1,
  array_uint32_33 *kcg_c2);
#endif /* kcg_comp_array_uint32_33 */
#endif /* kcg_use_array_uint32_33 */

#ifdef kcg_use_array_uint64_16_5
#ifndef kcg_comp_array_uint64_16_5
extern kcg_bool kcg_comp_array_uint64_16_5(
  array_uint64_16_5 *kcg_c1,
  array_uint64_16_5 *kcg_c2);
#endif /* kcg_comp_array_uint64_16_5 */
#endif /* kcg_use_array_uint64_16_5 */

#ifdef kcg_use_array_uint8_31
#ifndef kcg_comp_array_uint8_31
extern kcg_bool kcg_comp_array_uint8_31(
  array_uint8_31 *kcg_c1,
  array_uint8_31 *kcg_c2);
#endif /* kcg_comp_array_uint8_31 */
#endif /* kcg_use_array_uint8_31 */

#ifdef kcg_use_array_uint32_16_12
#ifndef kcg_comp_array_uint32_16_12
extern kcg_bool kcg_comp_array_uint32_16_12(
  array_uint32_16_12 *kcg_c1,
  array_uint32_16_12 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_12 */
#endif /* kcg_use_array_uint32_16_12 */

#ifdef kcg_use__6_array
#ifndef kcg_comp__6_array
extern kcg_bool kcg_comp__6_array(_6_array *kcg_c1, _6_array *kcg_c2);
#endif /* kcg_comp__6_array */
#endif /* kcg_use__6_array */

#ifdef kcg_use_array_uint32_16_14
#ifndef kcg_comp_array_uint32_16_14
extern kcg_bool kcg_comp_array_uint32_16_14(
  array_uint32_16_14 *kcg_c1,
  array_uint32_16_14 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_14 */
#endif /* kcg_use_array_uint32_16_14 */

#ifdef kcg_use_array_uint8_13
#ifndef kcg_comp_array_uint8_13
extern kcg_bool kcg_comp_array_uint8_13(
  array_uint8_13 *kcg_c1,
  array_uint8_13 *kcg_c2);
#endif /* kcg_comp_array_uint8_13 */
#endif /* kcg_use_array_uint8_13 */

#ifdef kcg_use_array_uint32_8_2
#ifndef kcg_comp_array_uint32_8_2
extern kcg_bool kcg_comp_array_uint32_8_2(
  array_uint32_8_2 *kcg_c1,
  array_uint32_8_2 *kcg_c2);
#endif /* kcg_comp_array_uint32_8_2 */
#endif /* kcg_use_array_uint32_8_2 */

#ifdef kcg_use_array_uint32_1
#ifndef kcg_comp_array_uint32_1
extern kcg_bool kcg_comp_array_uint32_1(
  array_uint32_1 *kcg_c1,
  array_uint32_1 *kcg_c2);
#endif /* kcg_comp_array_uint32_1 */
#endif /* kcg_use_array_uint32_1 */

#ifdef kcg_use_array_uint32_30
#ifndef kcg_comp_array_uint32_30
extern kcg_bool kcg_comp_array_uint32_30(
  array_uint32_30 *kcg_c1,
  array_uint32_30 *kcg_c2);
#endif /* kcg_comp_array_uint32_30 */
#endif /* kcg_use_array_uint32_30 */

#ifdef kcg_use_array_int64_31
#ifndef kcg_comp_array_int64_31
extern kcg_bool kcg_comp_array_int64_31(
  array_int64_31 *kcg_c1,
  array_int64_31 *kcg_c2);
#endif /* kcg_comp_array_int64_31 */
#endif /* kcg_use_array_int64_31 */

#ifdef kcg_use_array_uint32_12
#ifndef kcg_comp_array_uint32_12
extern kcg_bool kcg_comp_array_uint32_12(
  array_uint32_12 *kcg_c1,
  array_uint32_12 *kcg_c2);
#endif /* kcg_comp_array_uint32_12 */
#endif /* kcg_use_array_uint32_12 */

#ifdef kcg_use_XNonce_nacl_core_chacha
#ifndef kcg_comp_XNonce_nacl_core_chacha
extern kcg_bool kcg_comp_XNonce_nacl_core_chacha(
  XNonce_nacl_core_chacha *kcg_c1,
  XNonce_nacl_core_chacha *kcg_c2);
#endif /* kcg_comp_XNonce_nacl_core_chacha */
#endif /* kcg_use_XNonce_nacl_core_chacha */

#ifdef kcg_use_array_uint8_16
#ifndef kcg_comp_array_uint8_16
extern kcg_bool kcg_comp_array_uint8_16(
  array_uint8_16 *kcg_c1,
  array_uint8_16 *kcg_c2);
#endif /* kcg_comp_array_uint8_16 */
#endif /* kcg_use_array_uint8_16 */

#ifdef kcg_use__7_array
#ifndef kcg_comp__7_array
extern kcg_bool kcg_comp__7_array(_7_array *kcg_c1, _7_array *kcg_c2);
#endif /* kcg_comp__7_array */
#endif /* kcg_use__7_array */

#ifdef kcg_use_array_uint8_4
#ifndef kcg_comp_array_uint8_4
extern kcg_bool kcg_comp_array_uint8_4(
  array_uint8_4 *kcg_c1,
  array_uint8_4 *kcg_c2);
#endif /* kcg_comp_array_uint8_4 */
#endif /* kcg_use_array_uint8_4 */

#ifdef kcg_use_array_uint32_16_1
#ifndef kcg_comp_array_uint32_16_1
extern kcg_bool kcg_comp_array_uint32_16_1(
  array_uint32_16_1 *kcg_c1,
  array_uint32_16_1 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_1 */
#endif /* kcg_use_array_uint32_16_1 */

#ifndef kcg_comp_array_uint32_16_3
extern kcg_bool kcg_comp_array_uint32_16_3(
  array_uint32_16_3 *kcg_c1,
  array_uint32_16_3 *kcg_c2);
#define kcg_use_array_uint32_16_3
#endif /* kcg_comp_array_uint32_16_3 */

#ifdef kcg_use_array_uint32_16_15
#ifndef kcg_comp_array_uint32_16_15
extern kcg_bool kcg_comp_array_uint32_16_15(
  array_uint32_16_15 *kcg_c1,
  array_uint32_16_15 *kcg_c2);
#endif /* kcg_comp_array_uint32_16_15 */
#endif /* kcg_use_array_uint32_16_15 */

#ifdef kcg_use_array_uint8_4_4
#ifndef kcg_comp_array_uint8_4_4
extern kcg_bool kcg_comp_array_uint8_4_4(
  array_uint8_4_4 *kcg_c1,
  array_uint8_4_4 *kcg_c2);
#endif /* kcg_comp_array_uint8_4_4 */
#endif /* kcg_use_array_uint8_4_4 */

#ifdef kcg_use_array_uint32_4_4
#ifndef kcg_comp_array_uint32_4_4
extern kcg_bool kcg_comp_array_uint32_4_4(
  array_uint32_4_4 *kcg_c1,
  array_uint32_4_4 *kcg_c2);
#endif /* kcg_comp_array_uint32_4_4 */
#endif /* kcg_use_array_uint32_4_4 */

#ifdef kcg_use_i64416_nacl_sign
#ifndef kcg_comp_i64416_nacl_sign
extern kcg_bool kcg_comp_i64416_nacl_sign(
  i64416_nacl_sign *kcg_c1,
  i64416_nacl_sign *kcg_c2);
#endif /* kcg_comp_i64416_nacl_sign */
#endif /* kcg_use_i64416_nacl_sign */

#ifdef kcg_use_array_uint8_16_10
#ifndef kcg_comp_array_uint8_16_10
extern kcg_bool kcg_comp_array_uint8_16_10(
  array_uint8_16_10 *kcg_c1,
  array_uint8_16_10 *kcg_c2);
#endif /* kcg_comp_array_uint8_16_10 */
#endif /* kcg_use_array_uint8_16_10 */

#ifdef kcg_use_array_uint32_8_5
#ifndef kcg_comp_array_uint32_8_5
extern kcg_bool kcg_comp_array_uint32_8_5(
  array_uint32_8_5 *kcg_c1,
  array_uint32_8_5 *kcg_c2);
#endif /* kcg_comp_array_uint32_8_5 */
#endif /* kcg_use_array_uint32_8_5 */

#ifdef kcg_use_gf_nacl_op
#ifndef kcg_comp_gf_nacl_op
extern kcg_bool kcg_comp_gf_nacl_op(gf_nacl_op *kcg_c1, gf_nacl_op *kcg_c2);
#endif /* kcg_comp_gf_nacl_op */
#endif /* kcg_use_gf_nacl_op */

#ifdef kcg_use_array_uint32_3
#ifndef kcg_comp_array_uint32_3
extern kcg_bool kcg_comp_array_uint32_3(
  array_uint32_3 *kcg_c1,
  array_uint32_3 *kcg_c2);
#endif /* kcg_comp_array_uint32_3 */
#endif /* kcg_use_array_uint32_3 */

#ifdef kcg_use_array_int64_4
#ifndef kcg_comp_array_int64_4
extern kcg_bool kcg_comp_array_int64_4(
  array_int64_4 *kcg_c1,
  array_int64_4 *kcg_c2);
#endif /* kcg_comp_array_int64_4 */
#endif /* kcg_use_array_int64_4 */

#ifdef kcg_use_array_uint32_32
#ifndef kcg_comp_array_uint32_32
extern kcg_bool kcg_comp_array_uint32_32(
  array_uint32_32 *kcg_c1,
  array_uint32_32 *kcg_c2);
#endif /* kcg_comp_array_uint32_32 */
#endif /* kcg_use_array_uint32_32 */

#ifdef kcg_use_array_uint32_14
#ifndef kcg_comp_array_uint32_14
extern kcg_bool kcg_comp_array_uint32_14(
  array_uint32_14 *kcg_c1,
  array_uint32_14 *kcg_c2);
#endif /* kcg_comp_array_uint32_14 */
#endif /* kcg_use_array_uint32_14 */

#define kcg_comp_StreamChunk_slideTypes kcg_comp_array_uint32_16

#define kcg_copy_StreamChunk_slideTypes kcg_copy_array_uint32_16

#define kcg_comp_Hash_hash_blake2s kcg_comp_array_uint32_8

#define kcg_copy_Hash_hash_blake2s kcg_copy_array_uint32_8

#define kcg_comp_Key32_slideTypes kcg_comp_array_uint32_8

#define kcg_copy_Key32_slideTypes kcg_copy_array_uint32_8

#define kcg_comp_pub_tindyguardTypes kcg_comp_array_uint32_8

#define kcg_copy_pub_tindyguardTypes kcg_copy_array_uint32_8

#define kcg_comp_HashChunk_hash_blake2s kcg_comp_array_uint32_16

#define kcg_copy_HashChunk_hash_blake2s kcg_copy_array_uint32_16

#define kcg_comp_u32_16_hash_blake2s kcg_comp_array_uint32_16

#define kcg_copy_u32_16_hash_blake2s kcg_copy_array_uint32_16

#define kcg_comp_secret_tindyguardTypes kcg_comp_array_uint32_8

#define kcg_copy_secret_tindyguardTypes kcg_copy_array_uint32_8

#define kcg_comp_psk_tindyguardTypes kcg_comp_array_uint32_8

#define kcg_copy_psk_tindyguardTypes kcg_copy_array_uint32_8

#define kcg_comp_TAI_tindyguardTypes kcg_comp_array_uint32_3

#define kcg_copy_TAI_tindyguardTypes kcg_copy_array_uint32_3

#define kcg_comp_State_nacl_core_chacha kcg_comp_array_uint32_16

#define kcg_copy_State_nacl_core_chacha kcg_copy_array_uint32_16

#define kcg_comp_Key_nacl_core kcg_comp_array_uint32_8

#define kcg_copy_Key_nacl_core kcg_copy_array_uint32_8

#define kcg_comp_Nonce_nacl_core_chacha kcg_comp_array_uint32_3

#define kcg_copy_Nonce_nacl_core_chacha kcg_copy_array_uint32_3

#define kcg_comp_Mac_nacl_onetime kcg_comp_array_uint8_16

#define kcg_copy_Mac_nacl_onetime kcg_copy_array_uint8_16

#define kcg_comp_u324_slideTypes kcg_comp_array_uint32_4

#define kcg_copy_u324_slideTypes kcg_copy_array_uint32_4

#define kcg_comp_Mac_hash_blake2s kcg_comp_array_uint32_4

#define kcg_copy_Mac_hash_blake2s kcg_copy_array_uint32_4

#define kcg_comp_Signature_nacl_sign kcg_comp_array_uint32_16

#define kcg_copy_Signature_nacl_sign kcg_copy_array_uint32_16

#define kcg_comp_beHash_slideTypes kcg_comp_array_uint64_8

#define kcg_copy_beHash_slideTypes kcg_copy_array_uint64_8

#endif /* _KCG_TYPES_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_types.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

