/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sigma64_hash_libsha_19_61_6.h"

/* hash::libsha::sigma64/ */
kcg_uint64 sigma64_hash_libsha_19_61_6(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, _L1/, x/ */
  kcg_uint64 x_19_61_6)
{
  /* _L6/, ret/ */
  kcg_uint64 ret_19_61_6;

  ret_19_61_6 = ((x_19_61_6 >> kcg_lit_uint64(19)) | kcg_lsl_uint64(
        x_19_61_6,
        kcg_lit_uint64(45))) ^ ((x_19_61_6 >> kcg_lit_uint64(61)) |
      kcg_lsl_uint64(x_19_61_6, kcg_lit_uint64(3))) ^ (x_19_61_6 >>
      kcg_lit_uint64(6));
  return ret_19_61_6;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sigma64_hash_libsha_19_61_6.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

