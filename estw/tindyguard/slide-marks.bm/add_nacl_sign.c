/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "add_nacl_sign.h"

/* nacl::sign::add/ */
void add_nacl_sign(
  /* _L26/, _L37/, _L41/, _L5/, q/ */
  cpoint_nacl_op *q,
  /* _L19/, _L24/, _L36/, _L42/, p/ */
  cpoint_nacl_op *p,
  /* _L66/, s/ */
  cpoint_nacl_op *s)
{
  gf_nacl_op tmp;
  gf_nacl_op tmp1;
  /* _L4/ */
  gf_nacl_op _L4;
  /* _L38/ */
  gf_nacl_op _L38;
  /* _L45/ */
  gf_nacl_op _L45;
  /* _L29/, _L48/ */
  gf_nacl_op _L48;
  /* _L49/ */
  gf_nacl_op _L49;

  /* _L21=(nacl::op::A#2)/ */ A_nacl_op(&(*p)[0], &(*p)[1], &tmp);
  /* _L27=(nacl::op::A#3)/ */ A_nacl_op(&(*q)[0], &(*q)[1], &tmp1);
  /* _L29=(nacl::op::M#3)/ */ M_nacl_op(&tmp, &tmp1, &_L48);
  /* _L2=(nacl::op::Z#1)/ */ Z_nacl_op(&(*p)[1], &(*p)[0], &tmp);
  /* _L3=(nacl::op::Z#2)/ */ Z_nacl_op(&(*q)[1], &(*q)[0], &tmp1);
  /* _L4=(nacl::op::M#2)/ */ M_nacl_op(&tmp, &tmp1, &_L4);
  /* _L46=(nacl::op::Z#4)/ */ Z_nacl_op(&_L48, &_L4, &tmp);
  /* _L43=(nacl::op::M#7)/ */ M_nacl_op(&(*p)[2], &(*q)[2], &tmp1);
  /* _L45=(nacl::op::A#5)/ */ A_nacl_op(&tmp1, &tmp1, &_L45);
  /* _L34=(nacl::op::M#5)/ */ M_nacl_op(&(*p)[3], &(*q)[3], &tmp1);
  /* _L38=(nacl::op::M#6)/ */ M_nacl_op(&tmp1, (gf_nacl_op *) &D2_nacl_op, &_L38);
  /* _L47=(nacl::op::Z#5)/ */ Z_nacl_op(&_L45, &_L38, &tmp1);
  /* _L49=(nacl::op::A#7)/ */ A_nacl_op(&_L48, &_L4, &_L49);
  /* _L48=(nacl::op::A#6)/ */ A_nacl_op(&_L45, &_L38, &_L48);
  /* _L50=(nacl::op::M#8)/ */ M_nacl_op(&tmp, &tmp1, &(*s)[0]);
  /* _L51=(nacl::op::M#9)/ */ M_nacl_op(&_L49, &_L48, &(*s)[1]);
  /* _L52=(nacl::op::M#10)/ */ M_nacl_op(&_L48, &tmp1, &(*s)[2]);
  /* _L53=(nacl::op::M#11)/ */ M_nacl_op(&tmp, &_L49, &(*s)[3]);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** add_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

