/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _single_hash_sha512_H_
#define _single_hash_sha512_H_

#include "kcg_types.h"
#include "finalize_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha512::single/ */
extern void single_hash_sha512(
  /* _L26/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L25/, len/ */
  size_slideTypes len,
  /* finalbits/ */
  kcg_uint32 finalbits,
  /* finalbitsLen/ */
  size_slideTypes finalbitsLen,
  /* _L27/, hash/ */
  StreamChunk_slideTypes *hash);



#endif /* _single_hash_sha512_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** single_hash_sha512.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

