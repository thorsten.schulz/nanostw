/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _aead_nacl_box_64_1_20_H_
#define _aead_nacl_box_64_1_20_H_

#include "kcg_types.h"
#include "auth2_nacl_onetime_1_64.h"
#include "chacha_IETF_nacl_box_stream_64_20.h"
#include "setup_nacl_core_chacha_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::aead/ */
extern void aead_nacl_box_64_1_20(
  /* _L2/, msg/ */
  array_uint32_16_64 *msg_64_1_20,
  /* _L10/, _L4/, mlen/ */
  int_slideTypes mlen_64_1_20,
  /* _L14/, ad/ */
  array_uint32_16_1 *ad_64_1_20,
  /* _L15/, adlen/ */
  int_slideTypes adlen_64_1_20,
  /* _L5/, nonce/ */
  Nonce_nacl_core_chacha *nonce_64_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_64_1_20,
  /* _L13/, a/ */
  Mac_nacl_onetime *a_64_1_20,
  /* _L1/, cm/ */
  array_uint32_16_64 *cm_64_1_20);



#endif /* _aead_nacl_box_64_1_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** aead_nacl_box_64_1_20.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

