/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "xaeadOpen_nacl_box_64_1_20.h"

/* nacl::box::xaeadOpen/ */
void xaeadOpen_nacl_box_64_1_20(
  /* _L5/, a/ */
  Mac_nacl_onetime *a_64_1_20,
  /* _L7/, cm/ */
  array_uint32_16_64 *cm_64_1_20,
  /* _L6/, mlen/ */
  int_slideTypes mlen_64_1_20,
  /* _L18/, ad/ */
  array_uint32_16_1 *ad_64_1_20,
  /* _L19/, adlen/ */
  int_slideTypes adlen_64_1_20,
  /* _L29/, xnonce/ */
  XNonce_nacl_core_chacha *xnonce_64_1_20,
  /* _L24/, key/ */
  Key_nacl_core *key_64_1_20,
  /* _L20/, msg/ */
  array_uint32_16_64 *msg_64_1_20,
  /* _L21/, failed/ */
  kcg_bool *failed_64_1_20)
{
  array_uint32_3 tmp;
  Key_nacl_core tmp1;

  tmp[0] = kcg_lit_uint32(0);
  tmp[1] = (*xnonce_64_1_20)[4];
  tmp[2] = (*xnonce_64_1_20)[5];
  /* _L28=(nacl::core::chacha::half#1)/ */
  half_nacl_core_chacha_20(
    (array_uint32_4 *) &(*xnonce_64_1_20)[0],
    key_64_1_20,
    &tmp1);
  /* _L20=(nacl::box::aeadOpen#1)/ */
  aeadOpen_nacl_box_64_1_20(
    a_64_1_20,
    cm_64_1_20,
    mlen_64_1_20,
    ad_64_1_20,
    adlen_64_1_20,
    &tmp,
    &tmp1,
    msg_64_1_20,
    failed_64_1_20);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** xaeadOpen_nacl_box_64_1_20.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

