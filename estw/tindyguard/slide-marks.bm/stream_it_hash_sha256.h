/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _stream_it_hash_sha256_H_
#define _stream_it_hash_sha256_H_

#include "kcg_types.h"
#include "block_refine_hash_sha256.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::stream_it/ */
extern void stream_it_hash_sha256(
  /* _L9/, i/ */
  size_slideTypes i,
  /* _L3/, zin/ */
  array_uint32_8 *zin,
  /* _L4/, m/ */
  StreamChunk_slideTypes *m,
  /* _L5/, len/ */
  size_slideTypes len,
  /* _L10/, goOn/ */
  kcg_bool *goOn,
  /* _L2/, zout/ */
  array_uint32_8 *zout);



#endif /* _stream_it_hash_sha256_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** stream_it_hash_sha256.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

