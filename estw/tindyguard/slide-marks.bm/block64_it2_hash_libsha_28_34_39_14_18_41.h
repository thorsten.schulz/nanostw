/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block64_it2_hash_libsha_28_34_39_14_18_41_H_
#define _block64_it2_hash_libsha_28_34_39_14_18_41_H_

#include "kcg_types.h"
#include "SIGMA64_hash_libsha_28_34_39.h"
#include "Maj_hash_libsha_uint64.h"
#include "Ch_hash_libsha_uint64.h"
#include "SIGMA64_hash_libsha_14_18_41.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::block64_it2/ */
extern void block64_it2_hash_libsha_28_34_39_14_18_41(
  /* _L1/, _L37/, ain/ */
  array_uint64_8 *ain_28_34_39_14_18_41,
  /* _L13/, w/ */
  kcg_uint64 w_28_34_39_14_18_41,
  /* _L12/, k/ */
  kcg_uint64 k_28_34_39_14_18_41,
  /* _L36/, aout/ */
  array_uint64_8 *aout_28_34_39_14_18_41);



#endif /* _block64_it2_hash_libsha_28_34_39_14_18_41_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block64_it2_hash_libsha_28_34_39_14_18_41.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

