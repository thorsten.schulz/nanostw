/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _eval_response_tindyguard_handshake_3_H_
#define _eval_response_tindyguard_handshake_3_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_1_1_20.h"
#include "hkdf_hash_blake2s_1_3.h"
#include "hkdf_hash_blake2s_1_2.h"
#include "scalarmultDonna_nacl_box.h"
#include "hkdfChunk1_hash_blake2s_1.h"
#include "block_refine_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::handshake::eval_response/ */
extern void eval_response_tindyguard_handshake_3(
  /* s/ */
  Session_tindyguardTypes *s_3,
  /* fail_in/ */
  kcg_bool fail_in_3,
  /* rlength/ */
  length_t_udp rlength_3,
  /* responsemsg/ */
  array_uint32_16_4 *responsemsg_3,
  /* endpoint/ */
  peer_t_udp *endpoint_3,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_3,
  /* soo/ */
  Session_tindyguardTypes *soo_3,
  /* allfail/ */
  kcg_bool *allfail_3);



#endif /* _eval_response_tindyguard_handshake_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_response_tindyguard_handshake_3.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

