/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sign_block2_nacl_sign_H_
#define _sign_block2_nacl_sign_H_

#include "kcg_types.h"
#include "stream_it_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::sign_block2/ */
extern void sign_block2_nacl_sign(
  /* _L4/, i/ */
  size_slideTypes i,
  /* _L30/, _L5/, signin/ */
  prependAkku_nacl_sign *signin,
  /* _L8/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L2/, goOn/ */
  kcg_bool *goOn,
  /* _L29/, signoo/ */
  prependAkku_nacl_sign *signoo);



#endif /* _sign_block2_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_block2_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

