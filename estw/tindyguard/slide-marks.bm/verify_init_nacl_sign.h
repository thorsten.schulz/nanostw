/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _verify_init_nacl_sign_H_
#define _verify_init_nacl_sign_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::verify_init/ */
extern void verify_init_nacl_sign(
  /* _L2/, signature/ */
  Signature_nacl_sign *signature,
  /* _L3/, publicKey/ */
  Key32_slideTypes *publicKey,
  /* _L27/, length/ */
  kcg_int32 length,
  /* _L26/, goOnHashBlocks/ */
  kcg_bool *goOnHashBlocks,
  /* _L56/, hashoo/ */
  hashAkkuWithChunk_hash_sha512 *hashoo);



#endif /* _verify_init_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** verify_init_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

