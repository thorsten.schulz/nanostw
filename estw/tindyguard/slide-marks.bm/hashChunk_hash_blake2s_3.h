/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hashChunk_hash_blake2s_3_H_
#define _hashChunk_hash_blake2s_3_H_

#include "kcg_types.h"
#include "block_refine_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hashChunk/ */
extern void hashChunk_hash_blake2s_3(
  /* @1/_L64/, @1/msg/, _L64/, msg/ */
  array_uint32_16_3 *msg_3,
  /* @1/_L66/, @1/len/, _L66/, len/ */
  size_slideTypes len_3,
  /* @1/_L70/, @1/keybytes/, _L70/, keybytes/ */
  size_slideTypes keybytes_3,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk_3);

/*
  Expanded instances for: hash::blake2s::hashChunk/
  @1: (hash::blake2s::hash#1)
*/

#endif /* _hashChunk_hash_blake2s_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hashChunk_hash_blake2s_3.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

