/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sigma32_hash_libsha_17_19_10.h"

/* hash::libsha::sigma32/ */
kcg_uint32 sigma32_hash_libsha_17_19_10(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, _L1/, x/ */
  kcg_uint32 x_17_19_10)
{
  /* _L6/, ret/ */
  kcg_uint32 ret_17_19_10;

  ret_17_19_10 = ((x_17_19_10 >> kcg_lit_uint32(17)) | kcg_lsl_uint32(
        x_17_19_10,
        kcg_lit_uint32(15))) ^ ((x_17_19_10 >> kcg_lit_uint32(19)) |
      kcg_lsl_uint32(x_17_19_10, kcg_lit_uint32(13))) ^ (x_17_19_10 >>
      kcg_lit_uint32(10));
  return ret_17_19_10;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sigma32_hash_libsha_17_19_10.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

