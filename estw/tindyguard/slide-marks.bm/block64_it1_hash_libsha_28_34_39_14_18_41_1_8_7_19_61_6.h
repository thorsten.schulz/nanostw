/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6_H_
#define _block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6_H_

#include "kcg_types.h"
#include "block64_it2_hash_libsha_28_34_39_14_18_41.h"
#include "block64_it3_hash_libsha_1_8_7_19_61_6.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::block64_it1/ */
extern void block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6(
  /* _L2/, ain/ */
  array_uint64_8 *ain_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L3/, w/ */
  array_uint64_16 *w_28_34_39_14_18_41_1_8_7_19_61_6,
  /* K/, _L17/ */
  array_uint64_16 *K_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L6/, aout/ */
  array_uint64_8 *aout_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L5/, woo/ */
  array_uint64_16 *woo_28_34_39_14_18_41_1_8_7_19_61_6);



#endif /* _block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

