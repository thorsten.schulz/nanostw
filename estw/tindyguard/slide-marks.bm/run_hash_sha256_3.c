/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "run_hash_sha256_3.h"

/* hash::sha256::run/ */
void run_hash_sha256_3(
  /* _L13/, _L24/, msg/ */
  array_uint32_16_3 *msg_3,
  /* _L10/, _L14/, _L28/, len/ */
  size_slideTypes len_3,
  /* finalbits/ */
  kcg_uint32 finalbits_3,
  /* finalbitsLen/ */
  size_slideTypes finalbitsLen_3,
  /* _L3/, hash/ */
  StreamChunk_slideTypes *hash_3)
{
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  StreamChunk_slideTypes tmp;
  /* _L2/ */
  array_uint32_8 _L2_3;
  /* _L15/ */
  kcg_int32 _L15_3;

  _L15_3 = len_3 / hBlockBytes_hash_sha256;
  kcg_copy_array_uint32_8(&_L2_3, (array_uint32_8 *) &IV_hash_sha256);
  /* _L9= */
  if (_L15_3 > kcg_lit_int32(0)) {
    /* _L9= */
    for (idx = 0; idx < 3; idx++) {
      kcg_copy_array_uint32_8(&acc, &_L2_3);
      /* _L9=(hash::sha256::stream_it#1)/ */
      stream_it_hash_sha256(
        /* _L9= */(kcg_int32) idx,
        &acc,
        &(*msg_3)[idx],
        len_3,
        &cond_iterw,
        &_L2_3);
      /* _L9= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  if (kcg_lit_int32(0) <= _L15_3 && _L15_3 < kcg_lit_int32(3)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &(*msg_3)[_L15_3]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* _L3=(hash::sha256::finalize#1)/ */
  finalize_hash_sha256(
    &_L2_3,
    &tmp,
    len_3,
    finalbits_3,
    finalbitsLen_3,
    hash_3);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** run_hash_sha256_3.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

