/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block_refine_hash_sha256_H_
#define _block_refine_hash_sha256_H_

#include "kcg_types.h"
#include "process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::block_refine/ */
extern void block_refine_hash_sha256(
  /* _L2/, zin/ */
  array_uint32_8 *zin,
  /* _L4/, m/ */
  array_uint32_16 *m,
  /* _L1/, zout/ */
  array_uint32_8 *zout);



#endif /* _block_refine_hash_sha256_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block_refine_hash_sha256.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

