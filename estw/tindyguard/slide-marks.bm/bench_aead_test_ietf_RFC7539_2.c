/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_aead_test_ietf_RFC7539_2.h"

/* test::ietf_RFC7539::bench_aead/ */
kcg_bool bench_aead_test_ietf_RFC7539_2(void)
{
  /* _L2/ */
  array_uint32_16_2 _L2_2;
  /* _L1/ */
  Mac_nacl_onetime _L1_2;
  /* _L3/ */
  array_uint32_16_2 _L3_2;
  /* _L19/ */
  array_uint32_16_2 _L19_2;
  /* _L20/ */
  kcg_bool _L20_2;
  kcg_size idx;
  /* _L14/, failed/ */
  kcg_bool failed_2;

  for (idx = 0; idx < 2; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &_L3_2[idx],
      (StreamChunk_slideTypes *) &mdata_test_ietf_RFC7539[1]);
  }
  /* _L1=(nacl::box::aead#1)/ */
  aead_nacl_box_2_1_20(
    &_L3_2,
    kcg_lit_int32(114),
    (array_uint32_16_1 *) &addata_test_ietf_RFC7539,
    adlen_test_ietf_RFC7539,
    (Nonce_nacl_core_chacha *) &nonce_test_ietf_RFC7539,
    (Key_nacl_core *) &firstkey_test_ietf_RFC7539,
    &_L1_2,
    &_L2_2);
  /* _L19=(nacl::box::aeadOpen#1)/ */
  aeadOpen_nacl_box_2_1_20(
    &_L1_2,
    &_L2_2,
    kcg_lit_int32(114),
    (array_uint32_16_1 *) &addata_test_ietf_RFC7539,
    adlen_test_ietf_RFC7539,
    (Nonce_nacl_core_chacha *) &nonce_test_ietf_RFC7539,
    (Key_nacl_core *) &firstkey_test_ietf_RFC7539,
    &_L19_2,
    &_L20_2);
  failed_2 = !kcg_comp_array_uint32_16_2(&_L3_2, &_L19_2) || _L20_2;
  return failed_2;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_aead_test_ietf_RFC7539_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

