/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _test_test_ietf_RFC7693_H_
#define _test_test_ietf_RFC7693_H_

#include "kcg_types.h"
#include "bench_hash_test_ietf_RFC7693_256.h"
#include "bench_hash_test_ietf_RFC7693_64.h"
#include "bench_hash_test_ietf_RFC7693_16.h"
#include "test_BLAKE2s_key_it_test_ietf_RFC7693.h"
#include "test_hkdf_it_test_ietf_RFC7693.h"
#include "test_BLAKE2s_it_test_ietf_RFC7693.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC7693::test/ */
extern kcg_bool test_test_ietf_RFC7693(void);



#endif /* _test_test_ietf_RFC7693_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_test_ietf_RFC7693.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

