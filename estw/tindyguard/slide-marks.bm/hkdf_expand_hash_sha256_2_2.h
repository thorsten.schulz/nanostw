/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hkdf_expand_hash_sha256_2_2_H_
#define _hkdf_expand_hash_sha256_2_2_H_

#include "kcg_types.h"
#include "prependData_slideTypes_8.h"
#include "hkdf_expand_it2_hash_sha256_3.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha256::hkdf_expand/ */
extern void hkdf_expand_hash_sha256_2_2(
  /* _L24/, prk/ */
  StreamChunk_slideTypes *prk_2_2,
  /* _L26/, _L40/, _L72/, infolen/ */
  size_slideTypes infolen_2_2,
  /* _L39/, info/ */
  array_uint32_16_2 *info_2_2,
  /* _L1/, _L11/, okmlen/ */
  size_slideTypes okmlen_2_2,
  /* _L33/, okm/ */
  array_uint32_16_2 *okm_2_2,
  /* _L9/, fail/ */
  kcg_bool *fail_2_2);



#endif /* _hkdf_expand_hash_sha256_2_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_expand_hash_sha256_2_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

