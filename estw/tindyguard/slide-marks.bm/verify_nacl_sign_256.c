/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "verify_nacl_sign_256.h"

/* nacl::sign::verify/ */
kcg_bool verify_nacl_sign_256(
  /* _L23/, _L35/, msg/ */
  array_uint32_16_256 *msg_256,
  /* _L24/, len/ */
  size_slideTypes len_256,
  /* _L19/, _L20/, pk/ */
  Key32_slideTypes *pk_256,
  /* _L21/, _L22/, signature/ */
  Signature_nacl_sign *signature_256)
{
  hashAkkuWithChunk_hash_sha512 acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  StreamChunk_slideTypes tmp;
  /* _L30/ */
  kcg_bool _L30_256;
  /* _L32/ */
  size_slideTypes _L32_256;
  /* _L33/ */
  hashAkkuWithChunk_hash_sha512 _L33_256;
  /* _L18/, failed/ */
  kcg_bool failed_256;

  /* _L30=(nacl::sign::verify_init#1)/ */
  verify_init_nacl_sign(signature_256, pk_256, len_256, &_L30_256, &_L33_256);
  /* _L32= */
  if (_L30_256) {
    /* _L32= */
    for (idx = 0; idx < 256; idx++) {
      kcg_copy_hashAkkuWithChunk_hash_sha512(&acc, &_L33_256);
      /* _L32=(hash::sha512::stream_it#1)/ */
      stream_it_hash_sha512(
        /* _L32= */(kcg_int32) idx,
        &acc,
        &(*msg_256)[idx],
        &cond_iterw,
        &_L33_256);
      _L32_256 = /* _L32= */(kcg_int32) (idx + 1);
      /* _L32= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    _L32_256 = kcg_lit_int32(0);
  }
  if (kcg_lit_int32(0) <= _L32_256 && _L32_256 < kcg_lit_int32(256)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &(*msg_256)[_L32_256]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  failed_256 = /* _L18=(nacl::sign::verify_finalize#1)/ */
    verify_finalize_nacl_sign(signature_256, pk_256, &tmp, &_L33_256);
  return failed_256;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** verify_nacl_sign_256.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

