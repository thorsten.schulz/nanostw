/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _Ch_hash_libsha_uint64_H_
#define _Ch_hash_libsha_uint64_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::Ch/ */
extern kcg_uint64 Ch_hash_libsha_uint64(
  /* _L1/, x/ */
  kcg_uint64 x_uint64,
  /* _L6/, y/ */
  kcg_uint64 y_uint64,
  /* _L2/, z/ */
  kcg_uint64 z_uint64);



#endif /* _Ch_hash_libsha_uint64_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Ch_hash_libsha_uint64.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

