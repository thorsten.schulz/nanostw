/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block32_it3_hash_libsha_7_18_3_17_19_10_H_
#define _block32_it3_hash_libsha_7_18_3_17_19_10_H_

#include "kcg_types.h"
#include "sigma32_hash_libsha_17_19_10.h"
#include "sigma32_hash_libsha_7_18_3.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::block32_it3/ */
extern void block32_it3_hash_libsha_7_18_3_17_19_10(
  /* _L6/, i/ */
  size_slideTypes i_7_18_3_17_19_10,
  /* _L1/, w/ */
  array_uint32_16 *w_7_18_3_17_19_10,
  /* _L2/, wo/ */
  array_uint32_16 *wo_7_18_3_17_19_10);



#endif /* _block32_it3_hash_libsha_7_18_3_17_19_10_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block32_it3_hash_libsha_7_18_3_17_19_10.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

