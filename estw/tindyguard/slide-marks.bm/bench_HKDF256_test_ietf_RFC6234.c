/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_HKDF256_test_ietf_RFC6234.h"

/* test::ietf_RFC6234::bench_HKDF256/ */
kcg_bool bench_HKDF256_test_ietf_RFC6234(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* tc/ */
  testcase_hkdf_test_ietf_RFC7693 *tc)
{
  StreamChunk_slideTypes tmp;
  /* _L40/ */
  array_uint32_16_3 _L40;
  /* _L41/ */
  kcg_bool _L41;
  /* _L43/ */
  array_uint32_16_3 _L43;
  /* _L45/, _L49/ */
  array_uint32_16_1 _L45;
  kcg_size idx;
  /* _L3/, failed/ */
  kcg_bool failed;

  kcg_copy_StreamChunk_slideTypes(&_L45[0], &(*tc).key);
  /* _L39=(hash::sha256::hkdf_extract#1)/ */
  hkdf_extract_hash_sha256_2_1(
    (*tc).mlen,
    &(*tc).msg,
    kcg_lit_int32(64),
    &_L45,
    &tmp);
  kcg_copy_StreamChunk_slideTypes(
    &_L45[0],
    (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  /* _L40=(hash::sha256::hkdf_expand#1)/ */
  hkdf_expand_hash_sha256_1_3(
    &tmp,
    kcg_lit_int32(0),
    &_L45,
    kcg_lit_int32(96),
    &_L40,
    &_L41);
  for (idx = 0; idx < 3; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &_L43[idx],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  failed = failing || _L41 || kcg_comp_array_uint32_16_3(&_L40, &_L43);
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_HKDF256_test_ietf_RFC6234.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

