/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "init_tindyguard_handshake.h"

/* tindyguard::handshake::init/ */
void init_tindyguard_handshake(
  /* peer/ */
  Peer_tindyguardTypes *peer,
  /* pid/ */
  size_tindyguardTypes pid,
  /* sks/ */
  KeySalted_tindyguardTypes *sks,
  /* now/ */
  kcg_int64 now,
  /* slength/ */
  length_t_udp *slength,
  /* initmsg/ */
  array_uint32_16_4 *initmsg,
  /* soo/ */
  Session_tindyguardTypes *soo,
  /* again/ */
  kcg_bool *again,
  outC_init_tindyguard_handshake *outC)
{
  kcg_bool tmp;
  array_uint32_8 tmp1;
  array_uint32_16 tmp2;
  HashChunk_hash_blake2s tmp3;
  array_uint32_16_1 tmp4;
  array_uint32_16 tmp5;
  array_uint32_16_1 tmp6;
  array_uint32_16 tmp7;
  array_uint32_16_1 tmp8;
  array_uint32_16 tmp9;
  array_uint32_16_2 tmp10;
  array_uint32_16 tmp11;
  array_uint32_16 tmp12;
  array_uint8_4_4 tmp13;
  kcg_size idx;
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  /* @9/_L8/ */
  kcg_int32 _L8_stream_it_1_hash_1_build_init_2;
  kcg_int32 tmp_stream_it_1_hash_1_build_init_2;
  array_uint32_16 tmp14;
  array_uint32_16_1 tmp15;
  array_uint32_16 tmp16;
  array_uint32_16_1 tmp17;
  array_uint32_16 tmp18;
  array_uint8_4_4 tmp19;
  array_uint8_4_4 tmp20;
  array_uint32_16_3 tmp21;
  array_uint32_8 acc22;
  kcg_bool _23_cond_iterw;
  /* @16/_L8/ */
  kcg_int32 _L8_stream_it_1_hash128_1_build_init_2;
  kcg_int32 tmp_stream_it_1_hash128_1_build_init_2;
  array_uint32_16_4 tmp24;
  array_uint32_8 acc25;
  kcg_bool _26_cond_iterw;
  /* @18/_L8/ */
  kcg_int32 _L8_stream_it_1_hash128_2_build_init_2;
  kcg_int32 tmp_stream_it_1_hash128_2_build_init_2;
  array_uint32_8 tmp27;
  array_uint32_16 tmp28;
  array_uint8_4_4 tmp29;
  array_uint32_16 tmp30;
  /* @1/_L152/, @1/_L305/, @1/_L308/, @1/_L312/, @1/_L321/, @1/m_pt1/ */
  array_uint32_32 _L321_build_init_2;
  /* @1/_L323/, @1/m_pt2/ */
  array_uint32_16_4 _L323_build_init_2;
  /* @1/_L281/ */
  kcg_uint32 _L281_build_init_2;
  /* @1/_L282/ */
  kcg_uint32 _L282_build_init_2;
  /* @1/_L199/ */
  array_uint32_3 _L199_build_init_2;
  /* @1/_L74/ */
  array_uint32_16_1 _L74_build_init_2;
  /* @1/_L56/ */
  array_uint32_3 _L56_build_init_2;
  /* @1/_L48/ */
  array_uint32_16_1 _L48_build_init_2;
  /* @1/_L247/, @1/_L264/, @1/_L57/, @1/c_static/ */
  array_uint32_8 c_static_build_init_2;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_build_init_2;
  /* @17/_L61/ */
  array_uint32_8 _L61_hash128_2_build_init_2_4;
  /* @1/IfBlock1:then:_L6/, @17/_L75/, @17/mac/ */
  array_uint32_4 _L75_hash128_2_build_init_2_4;
  kcg_bool op_call_ephemeral_1_build_init_2;
  /* @2/IfBlock1:else:_L6/ */
  kcg_bool _L6_ephemeral_1_build_init_2_else_IfBlock1;
  /* @2/IfBlock1:else:_L1/ */
  kcg_bool _L1_ephemeral_1_build_init_2_else_IfBlock1;
  /* @2/IfBlock1:else:_L2/ */
  array_uint32_8 _L2_ephemeral_1_build_init_2_else_IfBlock1;
  /* @1/_L253/,
     @1/_L263/,
     @1/_L47/,
     @1/a_static/,
     @11/_L1/,
     @11/a/,
     @6/_L1/,
     @6/a/ */
  Mac_nacl_onetime _L1_ldAuth_2_build_init_2;
  /* @1/_L255/,
     @1/_L266/,
     @1/_L73/,
     @1/a_TAI/,
     @13/_L1/,
     @13/a/,
     @19/_L1/,
     @19/a/ */
  Mac_nacl_onetime _L1_ldAuth_1_build_init_2;
  /* @10/_L6/ */
  array_uint32_8_2 _L6_DHDerive_2_build_init_2;
  /* @10/_L5/ */
  array_uint32_16 _L5_DHDerive_2_build_init_2;
  /* @1/_L169/, @10/_L3/, @10/fail/ */
  kcg_bool _L3_DHDerive_2_build_init_2;
  /* @10/_L14/ */
  array_uint32_16_1 _L14_DHDerive_2_build_init_2;
  /* @1/_L243/,
     @1/_L268/,
     @1/ek/,
     @2/IfBlock1:else:_L4/,
     @2/kp/,
     @5/_L9/,
     @5/sk/ */
  KeyPair32_slideTypes _L9_DHDerive_1_build_init_2;
  /* @5/_L6/ */
  array_uint32_8_2 _L6_DHDerive_1_build_init_2;
  /* @5/_L5/ */
  array_uint32_16 _L5_DHDerive_1_build_init_2;
  /* @1/_L164/, @5/_L3/, @5/fail/, _L2/, fail/ */
  kcg_bool _L3_DHDerive_1_build_init_2;
  /* @5/_L14/ */
  array_uint32_16_1 _L14_DHDerive_1_build_init_2;
  /* @15/_L61/ */
  array_uint32_8 _L61_hash128_1_build_init_2_3;
  /* @1/_L155/, @1/_L316/, @1/_L320/, @1/m_mac1/, @15/_L75/, @15/mac/ */
  array_uint32_4 _L75_hash128_1_build_init_2_3;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock;
  /* _L3/ */
  array_uint32_1 _L3;

  /* _L2=(sys::random#1)/ */
  random01_sys_specialization(&_L3_DHDerive_1_build_init_2, &_L3);
  IfBlock1_clock = !_L3_DHDerive_1_build_init_2 && pid >
    InvalidPeer_tindyguardTypes;
  /* IfBlock1: */
  if (IfBlock1_clock) {
    kcg_copy_StreamChunk_slideTypes(
      &_L323_build_init_2[3],
      (array_uint32_16 *) &ZeroChunk_slideTypes);
    kcg_copy_StreamChunk_slideTypes(&tmp21[0], &(*peer).hcache.salt);
    _L321_build_init_2[0] = MsgType_Initiation_tindyguard;
    _L321_build_init_2[1] = _L3[0];
    /* @2/IfBlock1:else:_L1=(sys::random#1)/ */
    random08_sys_specialization(
      &_L1_ephemeral_1_build_init_2_else_IfBlock1,
      &_L2_ephemeral_1_build_init_2_else_IfBlock1);
    _L6_ephemeral_1_build_init_2_else_IfBlock1 =
      !_L1_ephemeral_1_build_init_2_else_IfBlock1;
    /* @10/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*sks).key,
      &(*peer).tpub,
      kcg_true,
      &_L3_DHDerive_2_build_init_2,
      (Key32_slideTypes *) &_L5_DHDerive_2_build_init_2[0]);
    if (_L6_ephemeral_1_build_init_2_else_IfBlock1) {
      /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
      keyPair_nacl_box_20(
        &_L2_ephemeral_1_build_init_2_else_IfBlock1,
        &op_call_ephemeral_1_build_init_2,
        &_L9_DHDerive_1_build_init_2,
        &tmp,
        &outC->Context_keyPair_2_ephemeral_1_build_init_2);
    }
    else {
      kcg_copy_KeyPair32_slideTypes(
        &_L9_DHDerive_1_build_init_2,
        (KeyPair32_slideTypes *) &ZeroKeyPair_slideTypes);
      tmp = kcg_true;
    }
    /* @5/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &_L9_DHDerive_1_build_init_2,
      &(*peer).tpub,
      kcg_true,
      &_L3_DHDerive_1_build_init_2,
      (Key32_slideTypes *) &_L5_DHDerive_1_build_init_2[0]);
    *again = _L3_DHDerive_1_build_init_2 || _L3_DHDerive_2_build_init_2 || tmp;
    tmp1[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp1[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    kcg_copy_Hash_hash_blake2s(&tmp2[0], &(*peer).hcache.hash1);
    kcg_copy_Key32_slideTypes(&tmp2[8], &_L9_DHDerive_1_build_init_2.pk_y);
    /* @4/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp1,
      kcg_lit_int32(64),
      &tmp2,
      kcg_false,
      (array_uint32_8 *) &tmp11[0]);
    kcg_copy_Key32_slideTypes(&tmp5[0], &_L9_DHDerive_1_build_init_2.pk_y);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_1_build_init_2[idx + 8] = kcg_lit_uint32(0);
      tmp5[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp4[0], &tmp5);
    /* @1/_L16=(hash::blake2s::hkdfChunk1#1)/ */
    hkdfChunk1_hash_blake2s_1(
      &tmp4,
      kcg_lit_int32(32),
      (HashChunk_hash_blake2s *) &CONSTRUCTION_chunk_tindyguard,
      &tmp3);
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_1_build_init_2[0],
      &_L5_DHDerive_1_build_init_2);
    /* @5/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_1_build_init_2,
      kcg_lit_int32(32),
      &tmp3,
      &_L6_DHDerive_1_build_init_2);
    kcg_copy_Key32_slideTypes(&tmp7[0], &(*sks).key.pk_y);
    for (idx = 0; idx < 8; idx++) {
      tmp7[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp6[0], &tmp7);
    kcg_copy_array_uint32_8(&tmp9[0], (array_uint32_8 *) &tmp11[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp9[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp8[0], &tmp9);
    for (idx = 0; idx < 3; idx++) {
      _L56_build_init_2[idx] = kcg_lit_uint32(0);
    }
    /* @1/_L47=(nacl::box::aead#1)/ */
    aead_nacl_box_1_1_20(
      &tmp6,
      kcg_lit_int32(32),
      &tmp8,
      kcg_lit_int32(32),
      &_L56_build_init_2,
      &_L6_DHDerive_1_build_init_2[1],
      &_L1_ldAuth_2_build_init_2,
      &_L48_build_init_2);
    kcg_copy_array_uint32_8(
      &c_static_build_init_2,
      (array_uint32_8 *) &_L48_build_init_2[0][0]);
    /* @1/_L280=(sys::tai#1)/ */
    tai_sys(kcg_true, &tmp16[0], &_L281_build_init_2, &_L282_build_init_2);
    tmp16[1] = _L281_build_init_2;
    tmp16[2] = _L282_build_init_2;
    kcg_copy_array_uint8_4(
      &tmp13[0],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[0]);
    kcg_copy_array_uint8_4(&tmp19[0], &tmp13[0]);
    kcg_copy_array_uint8_4(
      &tmp13[1],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[4]);
    kcg_copy_array_uint8_4(&tmp19[1], &tmp13[1]);
    kcg_copy_array_uint8_4(
      &tmp13[2],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[8]);
    kcg_copy_array_uint8_4(&tmp19[2], &tmp13[2]);
    kcg_copy_array_uint8_4(
      &tmp13[3],
      (array_uint8_4 *) &_L1_ldAuth_2_build_init_2[12]);
    kcg_copy_array_uint8_4(&tmp19[3], &tmp13[3]);
    tmp28[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp28[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    kcg_copy_array_uint32_8(&tmp11[8], &c_static_build_init_2);
    kcg_copy_array_uint32_16(&tmp10[0], &tmp11);
    /* @6/_L24= */
    for (idx = 0; idx < 4; idx++) {
      tmp12[idx] = /* @7/_L14= */(kcg_uint32) tmp13[idx][0] | kcg_lsl_uint32(
          /* @7/_L15= */(kcg_uint32) tmp13[idx][1],
          kcg_lit_uint32(8)) | kcg_lsl_uint32(
          /* @7/_L16= */(kcg_uint32) tmp13[idx][2],
          kcg_lit_uint32(16)) | kcg_lsl_uint32(
          /* @7/_L17= */(kcg_uint32) tmp13[idx][3],
          kcg_lit_uint32(24));
    }
    for (idx = 0; idx < 12; idx++) {
      tmp12[idx + 4] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp10[1], &tmp12);
    /* @8/_L60= */
    for (idx = 0; idx < 2; idx++) {
      kcg_copy_array_uint32_8(&acc, (array_uint32_8 *) &tmp28[0]);
      _L8_stream_it_1_hash_1_build_init_2 = /* @8/_L60= */(kcg_int32) idx *
        hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
      cond_iterw = kcg_lit_int32(80) > _L8_stream_it_1_hash_1_build_init_2;
      /* @9/_L14= */
      if (cond_iterw) {
        tmp_stream_it_1_hash_1_build_init_2 = _L8_stream_it_1_hash_1_build_init_2;
      }
      else {
        tmp_stream_it_1_hash_1_build_init_2 = kcg_lit_int32(80);
      }
      /* @9/_L2=(hash::blake2s::block_refine#1)/ */
      block_refine_hash_blake2s(
        &acc,
        tmp_stream_it_1_hash_1_build_init_2,
        &tmp10[idx],
        cond_iterw,
        (array_uint32_8 *) &tmp28[0]);
      /* @8/_L60= */
      if (!cond_iterw) {
        break;
      }
    }
    kcg_copy_Hash_hash_blake2s(&tmp14[0], &_L6_DHDerive_1_build_init_2[0]);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_2_build_init_2[idx + 8] = kcg_lit_uint32(0);
      tmp14[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_2_build_init_2[0],
      &_L5_DHDerive_2_build_init_2);
    /* @10/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_2_build_init_2,
      kcg_lit_int32(32),
      &tmp14,
      &_L6_DHDerive_2_build_init_2);
    for (idx = 0; idx < 13; idx++) {
      tmp16[idx + 3] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp15[0], &tmp16);
    kcg_copy_array_uint32_8(&tmp18[0], (array_uint32_8 *) &tmp28[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp18[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp17[0], &tmp18);
    for (idx = 0; idx < 3; idx++) {
      _L199_build_init_2[idx] = kcg_lit_uint32(0);
    }
    /* @1/_L73=(nacl::box::aead#2)/ */
    aead_nacl_box_1_1_20(
      &tmp15,
      kcg_lit_int32(12),
      &tmp17,
      kcg_lit_int32(32),
      &_L199_build_init_2,
      &_L6_DHDerive_2_build_init_2[1],
      &_L1_ldAuth_1_build_init_2,
      &_L74_build_init_2);
    kcg_copy_array_uint32_3(&tmp28[8], (array_uint32_3 *) &_L74_build_init_2[0][0]);
    kcg_copy_array_uint8_4(
      &tmp20[0],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[0]);
    kcg_copy_array_uint8_4(&tmp29[0], &tmp20[0]);
    kcg_copy_array_uint8_4(
      &tmp20[1],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[4]);
    kcg_copy_array_uint8_4(&tmp29[1], &tmp20[1]);
    kcg_copy_array_uint8_4(
      &tmp20[2],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[8]);
    kcg_copy_array_uint8_4(&tmp29[2], &tmp20[2]);
    kcg_copy_array_uint8_4(
      &tmp20[3],
      (array_uint8_4 *) &_L1_ldAuth_1_build_init_2[12]);
    kcg_copy_array_uint8_4(&tmp29[3], &tmp20[3]);
    kcg_copy_Key32_slideTypes(
      &_L321_build_init_2[2],
      &_L9_DHDerive_1_build_init_2.pk_y);
    kcg_copy_array_uint32_8(&_L321_build_init_2[10], &c_static_build_init_2);
    /* @11/_L24= */
    for (idx = 0; idx < 4; idx++) {
      _L321_build_init_2[idx + 18] = /* @12/_L14= */(kcg_uint32) tmp19[idx][0] |
        kcg_lsl_uint32(/* @12/_L15= */(kcg_uint32) tmp19[idx][1], kcg_lit_uint32(8)) |
        kcg_lsl_uint32(/* @12/_L16= */(kcg_uint32) tmp19[idx][2], kcg_lit_uint32(16)) |
        kcg_lsl_uint32(/* @12/_L17= */(kcg_uint32) tmp19[idx][3], kcg_lit_uint32(24));
    }
    kcg_copy_array_uint32_3(&_L321_build_init_2[22], (array_uint32_3 *) &tmp28[8]);
    /* @13/_L24= */
    for (idx = 0; idx < 4; idx++) {
      _L321_build_init_2[idx + 25] = /* @14/_L14= */(kcg_uint32) tmp20[idx][0] |
        kcg_lsl_uint32(/* @14/_L15= */(kcg_uint32) tmp20[idx][1], kcg_lit_uint32(8)) |
        kcg_lsl_uint32(/* @14/_L16= */(kcg_uint32) tmp20[idx][2], kcg_lit_uint32(16)) |
        kcg_lsl_uint32(/* @14/_L17= */(kcg_uint32) tmp20[idx][3], kcg_lit_uint32(24));
    }
    for (idx = 0; idx < 3; idx++) {
      _L321_build_init_2[idx + 29] = kcg_lit_uint32(0);
    }
    kcg_copy_StreamChunk_slideTypes(
      &tmp21[2],
      (StreamChunk_slideTypes *) &_L321_build_init_2[16]);
    IfBlock1_clock_build_init_2 = now < (*peer).cookie.opened +
      cookieEdible_tindyguard_conf;
    kcg_copy_StreamChunk_slideTypes(
      &tmp21[1],
      (StreamChunk_slideTypes *) &_L321_build_init_2[0]);
    kcg_copy_StreamChunk_slideTypes(&_L323_build_init_2[0], &tmp21[1]);
    _L61_hash128_1_build_init_2_3[0] = kcg_lit_uint32(1795737207);
    kcg_copy_array_uint32_7(
      &_L61_hash128_1_build_init_2_3[1],
      (array_uint32_7 *) &IV_hash_blake2s[1]);
    /* @15/_L60= */
    for (idx = 0; idx < 3; idx++) {
      kcg_copy_array_uint32_8(&acc22, &_L61_hash128_1_build_init_2_3);
      _L8_stream_it_1_hash128_1_build_init_2 = /* @15/_L60= */(kcg_int32) idx *
        hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
      _23_cond_iterw = kcg_lit_int32(180) > _L8_stream_it_1_hash128_1_build_init_2;
      /* @16/_L14= */
      if (_23_cond_iterw) {
        tmp_stream_it_1_hash128_1_build_init_2 = _L8_stream_it_1_hash128_1_build_init_2;
      }
      else {
        tmp_stream_it_1_hash128_1_build_init_2 = kcg_lit_int32(180);
      }
      /* @16/_L2=(hash::blake2s::block_refine#1)/ */
      block_refine_hash_blake2s(
        &acc22,
        tmp_stream_it_1_hash128_1_build_init_2,
        &tmp21[idx],
        _23_cond_iterw,
        &_L61_hash128_1_build_init_2_3);
      /* @15/_L60= */
      if (!_23_cond_iterw) {
        break;
      }
    }
    kcg_copy_array_uint32_4(
      &_L75_hash128_1_build_init_2_3,
      (array_uint32_4 *) &_L61_hash128_1_build_init_2_3[0]);
    kcg_copy_array_uint32_13(
      &_L323_build_init_2[1][0],
      (array_uint32_13 *) &_L321_build_init_2[16]);
    kcg_copy_array_uint32_3(
      &_L323_build_init_2[1][13],
      (array_uint32_3 *) &_L75_hash128_1_build_init_2_3[0]);
    kcg_copy_StreamChunk_slideTypes(
      &_L323_build_init_2[2],
      (array_uint32_16 *) &ZeroChunk_slideTypes);
    _L323_build_init_2[2][0] = _L75_hash128_1_build_init_2_3[3];
    /* @1/_L289= */
    if (*again) {
      *slength = kcg_lit_int32(-1);
    }
    else {
      *slength = kcg_lit_int32(148);
    }
    /* @1/IfBlock1: */
    if (IfBlock1_clock_build_init_2) {
      kcg_copy_StreamChunk_slideTypes(&tmp24[0], &(*peer).cookie.content);
      kcg_copy_StreamChunk_slideTypes(&tmp24[1], &_L323_build_init_2[0]);
      kcg_copy_StreamChunk_slideTypes(&tmp24[2], &_L323_build_init_2[1]);
      kcg_copy_StreamChunk_slideTypes(&tmp24[3], &_L323_build_init_2[2]);
      _L61_hash128_2_build_init_2_4[0] = kcg_lit_uint32(1795737207);
      kcg_copy_array_uint32_7(
        &_L61_hash128_2_build_init_2_4[1],
        (array_uint32_7 *) &IV_hash_blake2s[1]);
      /* @17/_L60= */
      for (idx = 0; idx < 4; idx++) {
        kcg_copy_array_uint32_8(&acc25, &_L61_hash128_2_build_init_2_4);
        _L8_stream_it_1_hash128_2_build_init_2 = /* @17/_L60= */(kcg_int32)
            idx * hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
        _26_cond_iterw = kcg_lit_int32(196) > _L8_stream_it_1_hash128_2_build_init_2;
        /* @18/_L14= */
        if (_26_cond_iterw) {
          tmp_stream_it_1_hash128_2_build_init_2 = _L8_stream_it_1_hash128_2_build_init_2;
        }
        else {
          tmp_stream_it_1_hash128_2_build_init_2 = kcg_lit_int32(196);
        }
        /* @18/_L2=(hash::blake2s::block_refine#1)/ */
        block_refine_hash_blake2s(
          &acc25,
          tmp_stream_it_1_hash128_2_build_init_2,
          &tmp24[idx],
          _26_cond_iterw,
          &_L61_hash128_2_build_init_2_4);
        /* @17/_L60= */
        if (!_26_cond_iterw) {
          break;
        }
      }
      kcg_copy_array_uint32_4(
        &_L75_hash128_2_build_init_2_4,
        (array_uint32_4 *) &_L61_hash128_2_build_init_2_4[0]);
      kcg_copy_array_uint32_16_4(initmsg, &_L323_build_init_2);
      (*initmsg)[2][1] = _L75_hash128_2_build_init_2_4[0];
      (*initmsg)[2][2] = _L75_hash128_2_build_init_2_4[1];
      (*initmsg)[2][3] = _L75_hash128_2_build_init_2_4[2];
      (*initmsg)[2][4] = _L75_hash128_2_build_init_2_4[3];
    }
    else {
      kcg_copy_array_uint32_16_4(initmsg, &_L323_build_init_2);
    }
    kcg_copy_Session_tindyguardTypes(
      soo,
      (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
    kcg_copy_KeyPair32_slideTypes(
      &(*soo).handshake.ephemeral,
      &_L9_DHDerive_1_build_init_2);
    tmp27[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp27[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    /* @19/_L24= */
    for (idx = 0; idx < 4; idx++) {
      tmp28[idx + 11] = /* @20/_L14= */(kcg_uint32) tmp29[idx][0] |
        kcg_lsl_uint32(/* @20/_L15= */(kcg_uint32) tmp29[idx][1], kcg_lit_uint32(8)) |
        kcg_lsl_uint32(/* @20/_L16= */(kcg_uint32) tmp29[idx][2], kcg_lit_uint32(16)) |
        kcg_lsl_uint32(/* @20/_L17= */(kcg_uint32) tmp29[idx][3], kcg_lit_uint32(24));
    }
    tmp28[15] = kcg_lit_uint32(0);
    /* @22/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp27,
      kcg_lit_int32(60),
      &tmp28,
      kcg_false,
      &(*soo).handshake.ihash);
    kcg_copy_Hash_hash_blake2s(&tmp30[0], &_L6_DHDerive_2_build_init_2[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp30[idx + 8] = kcg_lit_uint32(0);
      (*soo).handshake.their[idx] = kcg_lit_uint32(0);
    }
    kcg_copy_HashChunk_hash_blake2s(&(*soo).handshake.chainingKey, &tmp30);
    (*soo).pid = pid;
    kcg_copy_Peer_tindyguardTypes(&(*soo).peer, peer);
    (*soo).our = _L3[0];
  }
  else {
    *again = kcg_false;
    *slength = nil_udp;
    for (idx = 0; idx < 4; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &(*initmsg)[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    kcg_copy_Session_tindyguardTypes(
      soo,
      (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
  }
}

#ifndef KCG_USER_DEFINED_INIT
void init_init_tindyguard_handshake(outC_init_tindyguard_handshake *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_init_nacl_box_20(&outC->Context_keyPair_2_ephemeral_1_build_init_2);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void init_reset_tindyguard_handshake(outC_init_tindyguard_handshake *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_reset_nacl_box_20(&outC->Context_keyPair_2_ephemeral_1_build_init_2);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/*
  Expanded instances for: tindyguard::handshake::init/
  @1: (tindyguard::handshake::build_init#2)
  @3: @1/(hash::blake2s::single#2)
  @4: @3/(hash::blake2s::stream_it#1)
  @5: @1/(tindyguard::handshake::DHDerive#1)
  @6: @1/(slideTypes::ldAuth#2)
  @7: @6/(slideTypes::ld32x1#1)
  @8: @1/(hash::blake2s::hash#1)
  @9: @8/(hash::blake2s::stream_it#1)
  @10: @1/(tindyguard::handshake::DHDerive#2)
  @11: @1/(slideTypes::ldAuth#3)
  @12: @11/(slideTypes::ld32x1#1)
  @13: @1/(slideTypes::ldAuth#4)
  @14: @13/(slideTypes::ld32x1#1)
  @15: @1/(hash::blake2s::hash128#1)
  @16: @15/(hash::blake2s::stream_it#1)
  @17: @1/(hash::blake2s::hash128#2)
  @18: @17/(hash::blake2s::stream_it#1)
  @19: @1/(slideTypes::ldAuth#1)
  @20: @19/(slideTypes::ld32x1#1)
  @21: @1/(hash::blake2s::single#3)
  @22: @21/(hash::blake2s::stream_it#1)
  @2: @1/(tindyguard::handshake::ephemeral#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** init_tindyguard_handshake.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

