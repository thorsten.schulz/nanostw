/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_HMAC512_it_test_ietf_RFC6234.h"

/* test::ietf_RFC6234::test_HMAC512_it/ */
kcg_bool test_HMAC512_it_test_ietf_RFC6234(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L4/, tc/ */
  testcase_test_ietf_RFC6234 *tc)
{
  StreamChunk_slideTypes tmp;
  /* _L3/, failed/ */
  kcg_bool failed;

  /* _L18=(hash::sha512::HMAC#1)/ */
  HMAC_hash_sha512_12(&(*tc).m, (*tc).mlen, &(*tc).key, &tmp);
  failed = failing || !kcg_comp_StreamChunk_slideTypes(&tmp, &(*tc).exp);
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_HMAC512_it_test_ietf_RFC6234.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

