/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10.h"

/* hash::libsha::process32/ */
void process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10(
  /* _L2/, _L4/, zin/ */
  array_uint32_8 *zin_4_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L13/, m/ */
  array_uint32_16 *m_4_2_13_22_6_11_25_7_18_3_17_19_10,
  /* K/, _L26/, _L27/ */
  array_uint32_16_4 *K_4_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L3/, zout/ */
  array_uint32_8 *zout_4_2_13_22_6_11_25_7_18_3_17_19_10)
{
  kcg_size idx;
  array_uint32_8 acc;
  array_uint32_16 acc1;
  array_uint32_16 noname;
  /* _L11/ */
  array_uint32_8 _L11_4_2_13_22_6_11_25_7_18_3_17_19_10;

  kcg_copy_array_uint32_8(
    &_L11_4_2_13_22_6_11_25_7_18_3_17_19_10,
    zin_4_2_13_22_6_11_25_7_18_3_17_19_10);
  /* _L21= */
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint32_8(&acc, &_L11_4_2_13_22_6_11_25_7_18_3_17_19_10);
    /* _L21=(hash::libsha::block32_it2#1)/ */
    block32_it2_hash_libsha_2_13_22_6_11_25(
      &acc,
      (*m_4_2_13_22_6_11_25_7_18_3_17_19_10)[idx],
      (*K_4_2_13_22_6_11_25_7_18_3_17_19_10)[0][idx],
      &_L11_4_2_13_22_6_11_25_7_18_3_17_19_10);
  }
  kcg_copy_array_uint32_16(&noname, m_4_2_13_22_6_11_25_7_18_3_17_19_10);
  /* _L11= */
  for (idx = 0; idx < 3; idx++) {
    kcg_copy_array_uint32_8(&acc, &_L11_4_2_13_22_6_11_25_7_18_3_17_19_10);
    kcg_copy_array_uint32_16(&acc1, &noname);
    /* _L11=(hash::libsha::block32_it1#1)/ */
    block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10(
      &acc,
      &acc1,
      &(*K_4_2_13_22_6_11_25_7_18_3_17_19_10)[idx + 1],
      &_L11_4_2_13_22_6_11_25_7_18_3_17_19_10,
      &noname);
  }
  /* _L3= */
  for (idx = 0; idx < 8; idx++) {
    (*zout_4_2_13_22_6_11_25_7_18_3_17_19_10)[idx] =
      _L11_4_2_13_22_6_11_25_7_18_3_17_19_10[idx] +
      (*zin_4_2_13_22_6_11_25_7_18_3_17_19_10)[idx];
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

