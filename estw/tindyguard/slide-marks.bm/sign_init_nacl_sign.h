/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sign_init_nacl_sign_H_
#define _sign_init_nacl_sign_H_

#include "kcg_types.h"
#include "single_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::sign_init/ */
extern void sign_init_nacl_sign(
  /* SecretKey/, _L19/ */
  KeyPair32_slideTypes *SecretKey,
  /* _L28/, length/ */
  kcg_int32 length,
  /* _L62/, msg0/ */
  StreamChunk_slideTypes *msg0,
  /* _L29/, goOnHashBlocks/ */
  kcg_bool *goOnHashBlocks,
  /* _L49/, signoo/ */
  prependAkku_nacl_sign *signoo);



#endif /* _sign_init_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_init_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

