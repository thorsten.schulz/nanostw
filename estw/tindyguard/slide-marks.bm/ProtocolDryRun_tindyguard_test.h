/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _ProtocolDryRun_tindyguard_test_H_
#define _ProtocolDryRun_tindyguard_test_H_

#include "kcg_types.h"
#include "unwrap_tindyguard_data_2.h"
#include "wrap_tindyguard_data_2.h"
#include "eval_response_tindyguard_handshake_3.h"
#include "initPeer_tindyguard.h"
#include "init_tindyguard_handshake.h"
#include "respond_tindyguard_handshake_8.h"
#include "initOur_tindyguard.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_respond_tindyguard_handshake_8 /* _L133=(tindyguard::handshake::respond#2)/ */ Context_respond_2;
  outC_initOur_tindyguard /* _L86=(tindyguard::initOur#3)/ */ Context_initOur_3;
  outC_init_tindyguard_handshake /* _L132=(tindyguard::handshake::init#1)/ */ Context_init_1;
  outC_initOur_tindyguard /* _L4=(tindyguard::initOur#2)/ */ Context_initOur_2;
  /* ----------------- no clocks of observable data ------------------ */
} outC_ProtocolDryRun_tindyguard_test;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::test::ProtocolDryRun/ */
extern void ProtocolDryRun_tindyguard_test(
  /* _L69/, fail/, failed/ */
  kcg_bool *failed,
  outC_ProtocolDryRun_tindyguard_test *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void ProtocolDryRun_reset_tindyguard_test(
  outC_ProtocolDryRun_tindyguard_test *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void ProtocolDryRun_init_tindyguard_test(
  outC_ProtocolDryRun_tindyguard_test *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _ProtocolDryRun_tindyguard_test_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** ProtocolDryRun_tindyguard_test.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

