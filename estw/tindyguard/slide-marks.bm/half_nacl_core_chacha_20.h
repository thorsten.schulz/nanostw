/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _half_nacl_core_chacha_20_H_
#define _half_nacl_core_chacha_20_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::core::chacha::half/ */
extern void half_nacl_core_chacha_20(
  /* _L5/, x/ */
  array_uint32_4 *x_20,
  /* _L3/, key/ */
  Key_nacl_core *key_20,
  /* _L14/, xo/ */
  Key_nacl_core *xo_20);



#endif /* _half_nacl_core_chacha_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** half_nacl_core_chacha_20.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

