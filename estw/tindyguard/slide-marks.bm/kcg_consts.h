/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _KCG_CONSTS_H_
#define _KCG_CONSTS_H_

#include "kcg_types.h"

/* test::ietf_RFC7539::xnonce/ */
extern const XNonce_nacl_core_chacha xnonce_test_ietf_RFC7539;

/* nacl::core::chacha::XNonceBytes/ */
#define XNonceBytes_nacl_core_chacha (kcg_lit_uint32(24))

/* test::ietf_RFC7539::firstkey/ */
extern const Key_nacl_core firstkey_test_ietf_RFC7539;

/* test::ietf_RFC7539::nonce/ */
extern const Nonce_nacl_core_chacha nonce_test_ietf_RFC7539;

/* test::ietf_RFC7539::adlen/ */
#define adlen_test_ietf_RFC7539 (kcg_lit_int32(12))

/* test::ietf_RFC7539::addata/ */
extern const array_uint32_16_1 addata_test_ietf_RFC7539;

/* test::ietf_RFC7539::mlen/ */
#define mlen_test_ietf_RFC7539 (kcg_lit_int32(114))

/* test::ietf_RFC7539::mdata/ */
extern const array_uint32_16_2 mdata_test_ietf_RFC7539;

/* hash::sha256::Length/ */
#define Length_hash_sha256 (kcg_lit_int32(8))

/* hash::sha256::Bytes/ */
#define Bytes_hash_sha256 (kcg_lit_int32(32))

/* hash::sha256::hBlockLength/ */
#define hBlockLength_hash_sha256 (kcg_lit_int32(64) / kcg_lit_int32(4))

/* hash::sha256::K/ */
extern const array_uint32_16_4 K_hash_sha256;

/* hash::sha256::s13/ */
#define s13_hash_sha256 (kcg_lit_int32(10))

/* hash::sha256::r12/ */
#define r12_hash_sha256 (kcg_lit_int32(19))

/* hash::sha256::r11/ */
#define r11_hash_sha256 (kcg_lit_int32(17))

/* hash::sha256::s03/ */
#define s03_hash_sha256 (kcg_lit_int32(3))

/* hash::sha256::r02/ */
#define r02_hash_sha256 (kcg_lit_int32(18))

/* hash::sha256::r01/ */
#define r01_hash_sha256 (kcg_lit_int32(7))

/* hash::sha256::R13/ */
#define R13_hash_sha256 (kcg_lit_int32(25))

/* hash::sha256::R12/ */
#define R12_hash_sha256 (kcg_lit_int32(11))

/* hash::sha256::R11/ */
#define R11_hash_sha256 (kcg_lit_int32(6))

/* hash::sha256::R03/ */
#define R03_hash_sha256 (kcg_lit_int32(22))

/* hash::sha256::R02/ */
#define R02_hash_sha256 (kcg_lit_int32(13))

/* hash::sha256::R01/ */
#define R01_hash_sha256 (kcg_lit_int32(2))

/* hash::sha256::Kdim/ */
#define Kdim_hash_sha256 (kcg_lit_int32(4))

/* hash::sha256::IV/ */
extern const array_uint32_8 IV_hash_sha256;

/* hash::sha256::hBlockBytes/ */
#define hBlockBytes_hash_sha256 (kcg_lit_int32(64))

/* hash::sha512::Bytes/ */
#define Bytes_hash_sha512 (kcg_lit_int32(64))

/* test::ietf_RFC6234::test_hkdf256/ */
extern const _3_array test_hkdf256_test_ietf_RFC6234;

/* test::ietf_RFC6234::test_hkdfs/ */
#define test_hkdfs_test_ietf_RFC6234 (kcg_lit_int32(3))

/* test::ietf_RFC6234::test_sha256/ */
extern const _4_array test_sha256_test_ietf_RFC6234;

/* test::ietf_RFC6234::test_hmac256/ */
extern const _7_array test_hmac256_test_ietf_RFC6234;

/* test::ietf_RFC6234::test_hmac512/ */
extern const _7_array test_hmac512_test_ietf_RFC6234;

/* test::ietf_RFC6234::test_hmacs/ */
#define test_hmacs_test_ietf_RFC6234 (kcg_lit_int32(4))

/* test::ietf_RFC6234::test_sha512/ */
extern const _4_array test_sha512_test_ietf_RFC6234;

/* test::ietf_RFC6234::mblocks/ */
#define mblocks_test_ietf_RFC6234 (kcg_lit_int32(12))

/* test::ietf_RFC6234::test_shas/ */
#define test_shas_test_ietf_RFC6234 (kcg_lit_int32(9))

/* test::ietf_RFC7693::test_blake2s_keyed/ */
extern const testcase_test_ietf_RFC7693 test_blake2s_keyed_test_ietf_RFC7693;

/* test::ietf_RFC7693::test_hkdf/ */
extern const testcase_hkdf_test_ietf_RFC7693 test_hkdf_test_ietf_RFC7693;

/* test::ietf_RFC7693::hkdfoutputs/ */
#define hkdfoutputs_test_ietf_RFC7693 (kcg_lit_int32(3))

/* test::ietf_RFC7693::exp_hkdf3/ */
extern const array_uint32_16_3 exp_hkdf3_test_ietf_RFC7693;

/* test::ietf_RFC7693::test_blake2s/ */
extern const testcase_test_ietf_RFC7693 test_blake2s_test_ietf_RFC7693;

/* test::ietf_RFC7693::mblocks/ */
#define mblocks_test_ietf_RFC7693 (kcg_lit_int32(2))

/* test::stc/ */
extern const array stc_test;

/* test::stcs/ */
#define stcs_test (kcg_lit_int32(10))

/* test::ietf_RFC7748::RFC7748_exp_k1000/ */
extern const array_uint32_8 RFC7748_exp_k1000_test_ietf_RFC7748;

/* nacl::box::TURNS_scalarM_Split2/ */
#define TURNS_scalarM_Split2_nacl_box (kcg_lit_int32(19))

/* nacl::op::I/ */
extern const gf_nacl_op I_nacl_op;

/* nacl::op::D/ */
extern const gf_nacl_op D_nacl_op;

/* slideTypes::BlockBytes/ */
#define BlockBytes_slideTypes (kcg_lit_int32(128))

/* nacl::op::D2/ */
extern const gf_nacl_op D2_nacl_op;

/* nacl::op::Y/ */
extern const gf_nacl_op Y_nacl_op;

/* nacl::op::X/ */
extern const gf_nacl_op X_nacl_op;

/* nacl::sign::L/ */
extern const array_int64_32 L_nacl_sign;

/* hash::sha512::K/ */
extern const array_uint64_16_5 K_hash_sha512;

/* hash::sha512::s13/ */
#define s13_hash_sha512 (kcg_lit_int32(6))

/* hash::sha512::r12/ */
#define r12_hash_sha512 (kcg_lit_int32(61))

/* hash::sha512::r11/ */
#define r11_hash_sha512 (kcg_lit_int32(19))

/* hash::sha512::s03/ */
#define s03_hash_sha512 (kcg_lit_int32(7))

/* hash::sha512::r02/ */
#define r02_hash_sha512 (kcg_lit_int32(8))

/* hash::sha512::r01/ */
#define r01_hash_sha512 (kcg_lit_int32(1))

/* hash::sha512::R13/ */
#define R13_hash_sha512 (kcg_lit_int32(41))

/* hash::sha512::R12/ */
#define R12_hash_sha512 (kcg_lit_int32(18))

/* hash::sha512::R11/ */
#define R11_hash_sha512 (kcg_lit_int32(14))

/* hash::sha512::R03/ */
#define R03_hash_sha512 (kcg_lit_int32(39))

/* hash::sha512::R02/ */
#define R02_hash_sha512 (kcg_lit_int32(34))

/* hash::sha512::R01/ */
#define R01_hash_sha512 (kcg_lit_int32(28))

/* hash::sha512::Kdim/ */
#define Kdim_hash_sha512 (kcg_lit_int32(5))

/* hash::sha512::hBlockLength/ */
#define hBlockLength_hash_sha512 (kcg_lit_int32(128) / kcg_lit_int32(4))

/* hash::sha512::IV/ */
extern const array_uint64_8 IV_hash_sha512;

/* hash::sha512::hBlockBytes/ */
#define hBlockBytes_hash_sha512 (kcg_lit_int32(128))

/* test::ietf_RFC8032::tc/ */
extern const _2_array tc_test_ietf_RFC8032;

/* test::ietf_RFC8032::mblocks/ */
#define mblocks_test_ietf_RFC8032 (kcg_lit_int32(1))

/* nacl::sign::SignatureBytes/ */
#define SignatureBytes_nacl_sign (kcg_lit_int32(64))

/* test::ietf_RFC8032::tcs/ */
#define tcs_test_ietf_RFC8032 (kcg_lit_int32(10))

/* tindyguard::conf::signalKeepAlive/ */
#define signalKeepAlive_tindyguard_conf kcg_true

/* slideTypes::StreamChunkBytes/ */
#define StreamChunkBytes_slideTypes (kcg_lit_int32(16) * kcg_lit_int32(4))

/* tindyguard::MsgType_Data/ */
#define MsgType_Data_tindyguard (kcg_lit_uint32(4))

/* nacl::onetime::macLength/ */
#define macLength_nacl_onetime (kcg_lit_int32(16))

/* tindyguardTypes::FatalTAIage/ */
#define FatalTAIage_tindyguardTypes                                           \
  (kcg_lit_uint64(60) * kcg_lit_uint64(60) * kcg_lit_uint64(24))

/* tindyguard::CONSTRUCTION_chunk/ */
extern const StreamChunk_slideTypes CONSTRUCTION_chunk_tindyguard;

/* tindyguard::MsgType_Initiation/ */
#define MsgType_Initiation_tindyguard (kcg_lit_uint32(1))

/* tindyguardTypes::EmptySession/ */
extern const Session_tindyguardTypes EmptySession_tindyguardTypes;

/* tindyguardTypes::EmptyPeer/ */
extern const Peer_tindyguardTypes EmptyPeer_tindyguardTypes;

/* nacl::core::chacha::DEFAULT_Rounds/ */
#define DEFAULT_Rounds_nacl_core_chacha (kcg_lit_uint32(20))

/* nacl::onetime::kMask32/ */
extern const array_uint32_4 kMask32_nacl_onetime;

/* nacl::onetime::minusp/ */
extern const array_uint8_17 minusp_nacl_onetime;

/* nacl::onetime::macBytes/ */
#define macBytes_nacl_onetime (kcg_lit_uint32(16))

/* nacl::core::sigma/ */
extern const array_uint32_4 sigma_nacl_core;

/* nacl::core::chacha::NonceBytes/ */
#define NonceBytes_nacl_core_chacha (kcg_lit_uint32(12))

/* hash::libsha::HMAC_outer_xor/ */
#define HMAC_outer_xor_hash_libsha (kcg_lit_uint32(0x5c5c5c5c))

/* hash::libsha::HMAC_inner_xor/ */
#define HMAC_inner_xor_hash_libsha (kcg_lit_uint32(0x36363636))

/* hash::blake2s::Bytes/ */
#define Bytes_hash_blake2s (kcg_lit_int32(32))

/* slideTypes::ZeroChunk/ */
extern const StreamChunk_slideTypes ZeroChunk_slideTypes;

/* tindyguard::conf::fixedEphemeral/ */
extern const secret_tindyguardTypes fixedEphemeral_tindyguard_conf;

/* tindyguard::conf::isFixedE/ */
#define isFixedE_tindyguard_conf kcg_false

/* tindyguard::handshake::EmptyState/ */
extern const State_tindyguard_handshake EmptyState_tindyguard_handshake;

/* tindyguard::conf::cookieEdible/ */
#define cookieEdible_tindyguard_conf                                          \
  (kcg_lit_int64(120) * kcg_lit_int64(1000000000))

/* tindyguard::WG_Cookie_Rotten_After_Time/ */
#define WG_Cookie_Rotten_After_Time_tindyguard                                \
  (kcg_lit_int64(120) * kcg_lit_int64(1000000000))

/* tindyguard::Ticks_per_Sec/ */
#define Ticks_per_Sec_tindyguard (kcg_lit_int64(1000000000))

/* tindyguard::MsgType_Response/ */
#define MsgType_Response_tindyguard (kcg_lit_uint32(2))

/* tindyguard::handshake::dimHS/ */
#define dimHS_tindyguard_handshake (kcg_lit_int32(4))

/* tindyguard::test::Lani_pub/ */
extern const pub_tindyguardTypes Lani_pub_tindyguard_test;

/* tindyguard::test::net10_allowed/ */
extern const range_t_udp net10_allowed_tindyguard_test;

/* tindyguard::test::Lani_endpoint/ */
extern const peer_t_udp Lani_endpoint_tindyguard_test;

/* tindyguard::test::ChickenJoe_endpoint/ */
extern const peer_t_udp ChickenJoe_endpoint_tindyguard_test;

/* tindyguard::test::ChickenJoe_pub/ */
extern const pub_tindyguardTypes ChickenJoe_pub_tindyguard_test;

/* tindyguard::test::ChickenJoe_allowed/ */
extern const range_t_udp ChickenJoe_allowed_tindyguard_test;

/* tindyguard::test::ChickenJoe_ip/ */
extern const IpAddress_udp ChickenJoe_ip_tindyguard_test;

/* tindyguard::test::BigZ_priv/ */
extern const secret_tindyguardTypes BigZ_priv_tindyguard_test;

/* tindyguard::test::dimPeer/ */
#define dimPeer_tindyguard_test (kcg_lit_int32(8))

/* tindyguard::test::BigZ_pub/ */
extern const pub_tindyguardTypes BigZ_pub_tindyguard_test;

/* tindyguard::test::BigZ_allowed/ */
extern const range_t_udp BigZ_allowed_tindyguard_test;

/* tindyguard::test::BigZ_ip/ */
extern const IpAddress_udp BigZ_ip_tindyguard_test;

/* tindyguard::test::BigZ_endpoint/ */
extern const peer_t_udp BigZ_endpoint_tindyguard_test;

/* udp::emptyPeer/ */
extern const peer_t_udp emptyPeer_udp;

/* udp::ipAny/ */
extern const IpAddress_udp ipAny_udp;

/* tindyguard::test::preshared/ */
extern const secret_tindyguardTypes preshared_tindyguard_test;

/* tindyguard::test::ChickenJoe_priv/ */
extern const secret_tindyguardTypes ChickenJoe_priv_tindyguard_test;

/* tindyguard::session::nil/ */
#define nil_tindyguard_session (kcg_lit_int32(-1))

/* tindyguardTypes::InvalidPeer/ */
#define InvalidPeer_tindyguardTypes (kcg_lit_int32(-1))

/* tindyguardTypes::nil/ */
#define nil_tindyguardTypes (kcg_lit_int32(-1))

/* udp::nil/ */
#define nil_udp (kcg_lit_int32(-1))

/* tindyguardTypes::EmptyKey/ */
extern const KeySalted_tindyguardTypes EmptyKey_tindyguardTypes;

/* tindyguardTypes::EmptyJar/ */
extern const CookieJar_tindyguardTypes EmptyJar_tindyguardTypes;

/* slideTypes::ZeroKeyPair/ */
extern const KeyPair32_slideTypes ZeroKeyPair_slideTypes;

/* nacl::box::badKeys/ */
#define badKeys_nacl_box (kcg_lit_int32(5))

/* nacl::box::badKey/ */
extern const array_uint32_8_5 badKey_nacl_box;

/* nacl::box::bad2/ */
extern const Key32_slideTypes bad2_nacl_box;

/* nacl::box::bad1/ */
extern const Key32_slideTypes bad1_nacl_box;

/* nacl::box::invKey/ */
extern const Key32_slideTypes invKey_nacl_box;

/* nacl::box::oneKey/ */
extern const Key32_slideTypes oneKey_nacl_box;

/* nacl::box::zeroKey/ */
extern const Key32_slideTypes zeroKey_nacl_box;

/* nacl::op::_121665/ */
extern const gf_nacl_op _121665_nacl_op;

/* nacl::box::turnsInvEquiv/ */
#define turnsInvEquiv_nacl_box (kcg_lit_int32(6))

/* nacl::box::TURNS_scalarM_Single/ */
#define TURNS_scalarM_Single_nacl_box (kcg_lit_int32(21))

/* nacl::box::initialScalMulAcc/ */
extern const scalMulAcc_nacl_box initialScalMulAcc_nacl_box;

/* nacl::op::gf0/ */
extern const gf_nacl_op gf0_nacl_op;

/* nacl::op::gf1/ */
extern const gf_nacl_op gf1_nacl_op;

/* nacl::box::_9/ */
extern const array_uint32_8 _9_nacl_box;

/* nacl::box::TURNS_scalarM_Donna/ */
#define TURNS_scalarM_Donna_nacl_box (kcg_lit_int32(20))

/* tindyguard::conf::retryKey/ */
#define retryKey_tindyguard_conf (kcg_lit_int32(3))

/* hash::blake2s::sigma/ */
extern const array_uint8_16_10 sigma_hash_blake2s;

/* hash::blake2s::hBlockLength/ */
#define hBlockLength_hash_blake2s (kcg_lit_int32(64) / kcg_lit_int32(4))

/* hash::blake2s::hBlockBytes/ */
#define hBlockBytes_hash_blake2s (kcg_lit_int32(64))

/* hash::blake2s::IV/ */
extern const array_uint32_8 IV_hash_blake2s;

/* hash::blake2s::Length/ */
#define Length_hash_blake2s (kcg_lit_int32(8))

/* slideTypes::StreamChunkLength/ */
#define StreamChunkLength_slideTypes (kcg_lit_int32(16))

/* slideTypes::KeyLength/ */
#define KeyLength_slideTypes (kcg_lit_int32(32) / kcg_lit_int32(4))

/* nacl::core::KeyLength/ */
#define KeyLength_nacl_core (kcg_lit_int32(32) / kcg_lit_int32(4))

/* nacl::core::KeyBytes/ */
#define KeyBytes_nacl_core (kcg_lit_int32(32))

/* tindyguard::MAC1_label/ */
extern const array_uint32_2 MAC1_label_tindyguard;

/* tindyguard::CONSTRUCTION_IDENTIFIER_hash/ */
extern const array_uint32_8 CONSTRUCTION_IDENTIFIER_hash_tindyguard;

/* tindyguard::COOKIE_label/ */
extern const array_uint32_2 COOKIE_label_tindyguard;

#endif /* _KCG_CONSTS_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_consts.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

