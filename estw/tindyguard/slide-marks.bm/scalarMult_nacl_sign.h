/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _scalarMult_nacl_sign_H_
#define _scalarMult_nacl_sign_H_

#include "kcg_types.h"
#include "scalarMult_it1_nacl_sign.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::scalarMult/ */
extern void scalarMult_nacl_sign(
  /* _L1/, qin/ */
  cpoint_nacl_op *qin,
  /* _L2/, s/ */
  array_uint32_8 *s,
  /* _L29/, p/ */
  cpoint_nacl_op *p);



#endif /* _scalarMult_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarMult_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

