/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "prependData_slideTypes_8.h"

/* slideTypes::prependData/ */
void prependData_slideTypes_8(
  /* _L3/, prep/ */
  array_uint32_8 *prep_8,
  /* _L1/, in/ */
  StreamChunk_slideTypes *in_8,
  /* _L5/, prepo/ */
  array_uint32_8 *prepo_8,
  /* _L4/, out/ */
  StreamChunk_slideTypes *out_8)
{
  kcg_copy_array_uint32_8(prepo_8, (array_uint32_8 *) &(*in_8)[8]);
  kcg_copy_array_uint32_8(&(*out_8)[0], prep_8);
  kcg_copy_array_uint32_8(&(*out_8)[8], (array_uint32_8 *) &(*in_8)[0]);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** prependData_slideTypes_8.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

