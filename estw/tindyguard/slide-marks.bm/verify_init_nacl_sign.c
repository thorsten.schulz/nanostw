/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "verify_init_nacl_sign.h"

/* nacl::sign::verify_init/ */
void verify_init_nacl_sign(
  /* _L2/, signature/ */
  Signature_nacl_sign *signature,
  /* _L3/, publicKey/ */
  Key32_slideTypes *publicKey,
  /* _L27/, length/ */
  kcg_int32 length,
  /* _L26/, goOnHashBlocks/ */
  kcg_bool *goOnHashBlocks,
  /* _L56/, hashoo/ */
  hashAkkuWithChunk_hash_sha512 *hashoo)
{
  /* _L28/ */
  kcg_int32 _L28;

  kcg_copy_array_uint64_8(&(*hashoo).z, (array_uint64_8 *) &IV_hash_sha512);
  _L28 = length + kcg_lit_int32(64);
  (*hashoo).length = _L28;
  kcg_copy_array_uint32_8(
    &(*hashoo).porch[0],
    (array_uint32_8 *) &(*signature)[0]);
  kcg_copy_Key32_slideTypes(&(*hashoo).porch[8], publicKey);
  *goOnHashBlocks = _L28 >= BlockBytes_slideTypes;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** verify_init_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

