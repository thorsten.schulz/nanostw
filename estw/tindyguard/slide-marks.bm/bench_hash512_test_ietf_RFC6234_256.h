/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _bench_hash512_test_ietf_RFC6234_256_H_
#define _bench_hash512_test_ietf_RFC6234_256_H_

#include "kcg_types.h"
#include "run_hash_sha512_256.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC6234::bench_hash512/ */
extern kcg_bool bench_hash512_test_ietf_RFC6234_256(
  /* _L1/, failing/ */
  kcg_bool failing_256,
  /* tc/ */
  simpleTC_test *tc_256);



#endif /* _bench_hash512_test_ietf_RFC6234_256_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_hash512_test_ietf_RFC6234_256.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

