/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarmult_nacl_box_1.h"

/* nacl::box::scalarmult/ */
void scalarmult_nacl_box_1(
  /* our/ */
  KeyPair32_slideTypes *our_1,
  /* their/ */
  Key32_slideTypes *their_1,
  /* check/ */
  kcg_bool check_1,
  /* again/ */
  kcg_bool *again_1,
  /* _L1/, q/ */
  array_uint32_8 *q_1,
  /* failed/ */
  kcg_bool *failed_1,
  outC_scalarmult_nacl_box_1 *outC)
{
  kcg_size idx;
  /* @12/_L15/ */
  kcg_uint32 _L15_st32_1_st32x8_1;
  /* @12/_L14/ */
  kcg_uint32 _L14_st32_1_st32x8_1;
  /* @10/_L15/ */
  kcg_uint32 _L15_st32_1_st32x8_2;
  /* @10/_L14/ */
  kcg_uint32 _L14_st32_1_st32x8_2;
  array_uint8_32 tmp;
  /* @7/_L11/, @8/_L2/, @8/o/ */
  int_slideTypes _L11_scalarmult_it1_8;
  kcg_size idx_scalarmult_it1_8;
  scalMulAcc_nacl_box acc_scalarmult_it1_8;
  /* @1/_L4/ */
  scalMulAcc_nacl_box _L4_scalarmult_it1_7;
  scalMulAcc_nacl_box acc_scalarmult_it1_7;
  gf_nacl_op tmp1;
  gf_nacl_op tmp2;
  gf_nacl_op tmp3;
  gf_nacl_op tmp4;
  /* @3/_L4/ */
  scalMulAcc_nacl_box _L4_scalarmult_it1_6;
  scalMulAcc_nacl_box acc_scalarmult_it1_6;
  array_uint8_32 tmp5;
  /* @5/_L4/, SM1:runSingle:_L9/ */
  scalMulAcc_nacl_box _L4_scalarmult_it1_5;
  scalMulAcc_nacl_box acc_scalarmult_it1_5;
  /* @11/_L18/ */
  u848_slideTypes _L18_st32x8_1;
  /* @9/_L18/ */
  u848_slideTypes _L18_st32x8_2;
  /* @11/_L19/, @11/m/, SM1:firstPart:_L79/ */
  array_uint8_32 _L79_firstPart_SM1_1;
  /* @9/_L19/, @9/m/, SM1:firstPart:_L55/, SM1:runSingle:_L33/ */
  array_uint8_32 _L55_firstPart_SM1_1;
  /* SM1:runPart:_L25/, a/ */
  scalMulAcc_nacl_box a_partial_1;
  /* SM1:runSingle:_L18/ */
  gf_nacl_op _L18_runSingle_SM1_1;
  /* IfBlock1:, SM1:, SM1:finalPart:, SM1:runPart:<1> */
  kcg_bool IfBlock1_clock_1;
  /* SM1: */
  SSM_ST_SM1 SM1_state_act_1;
  /* SM1: */
  SSM_TR_SM1 SM1_fired_strong_1;
  /* z/ */
  array_uint8_32 z_1;

  kcg_copy_array_uint8_32(&z_1, &outC->z_1);
  /* SM1: */
  switch (outC->SM1_state_nxt_1) {
    case SSM_st_donna_SM1 :
      SM1_state_act_1 = SSM_st_donna_SM1;
      SM1_fired_strong_1 = SSM_TR_no_trans_SM1;
      break;
    case SSM_st_fin_SM1 :
      SM1_state_act_1 = SSM_st_fin_SM1;
      SM1_fired_strong_1 = SSM_TR_no_trans_SM1;
      break;
    case SSM_st_runSingle_SM1 :
      SM1_state_act_1 = SSM_st_runSingle_SM1;
      SM1_fired_strong_1 = SSM_TR_no_trans_SM1;
      break;
    case SSM_st_runPart_SM1 :
      IfBlock1_clock_1 = outC->a_1.i <= kcg_lit_int32(-5);
      if (IfBlock1_clock_1) {
        SM1_state_act_1 = SSM_st_finalPart_SM1;
        SM1_fired_strong_1 = SSM_TR_runPart_finalPart_1_runPart_SM1;
      }
      else {
        SM1_state_act_1 = SSM_st_runPart_SM1;
        SM1_fired_strong_1 = SSM_TR_no_trans_SM1;
      }
      break;
    case SSM_st_finalPart_SM1 :
      SM1_state_act_1 = SSM_st_finalPart_SM1;
      SM1_fired_strong_1 = SSM_TR_no_trans_SM1;
      break;
    case SSM_st_firstPart_SM1 :
      SM1_state_act_1 = SSM_st_firstPart_SM1;
      SM1_fired_strong_1 = SSM_TR_no_trans_SM1;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* SM1: */
  switch (SM1_state_act_1) {
    case SSM_st_donna_SM1 :
      *again_1 = kcg_false;
      /* SM1:donna:_L1=(nacl::box::curve25519_donna#1)/ */
      curve25519_donna_nacl_box(&(*our_1).sk_x, their_1, q_1);
      break;
    case SSM_st_fin_SM1 :
      *again_1 = kcg_false;
      kcg_copy_array_uint32_8(q_1, &outC->k_1);
      break;
    case SSM_st_runSingle_SM1 :
      *again_1 = kcg_false;
      /* @9/_L18= */
      for (idx = 0; idx < 8; idx++) {
        _L18_st32x8_2[idx][0] = /* @10/_L8= */(kcg_uint8) (*our_1).sk_x[idx];
        _L14_st32_1_st32x8_2 = (*our_1).sk_x[idx] >> kcg_lit_uint32(8);
        _L18_st32x8_2[idx][1] = /* @10/_L7= */(kcg_uint8) _L14_st32_1_st32x8_2;
        _L15_st32_1_st32x8_2 = _L14_st32_1_st32x8_2 >> kcg_lit_uint32(8);
        _L18_st32x8_2[idx][2] = /* @10/_L6= */(kcg_uint8) _L15_st32_1_st32x8_2;
        _L18_st32x8_2[idx][3] = /* @10/_L5= */(kcg_uint8)
            (_L15_st32_1_st32x8_2 >> kcg_lit_uint32(8));
      }
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[0], &_L18_st32x8_2[0]);
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[4], &_L18_st32x8_2[1]);
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[8], &_L18_st32x8_2[2]);
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[12], &_L18_st32x8_2[3]);
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[16], &_L18_st32x8_2[4]);
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[20], &_L18_st32x8_2[5]);
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[24], &_L18_st32x8_2[6]);
      kcg_copy_array_uint8_4(&_L55_firstPart_SM1_1[28], &_L18_st32x8_2[7]);
      /* SM1:runSingle:_L18=(nacl::op::unpack25519#6)/ */
      unpack25519_nacl_op(their_1, &_L18_runSingle_SM1_1);
      kcg_copy_scalMulAcc_nacl_box(
        &_L4_scalarmult_it1_5,
        (scalMulAcc_nacl_box *) &initialScalMulAcc_nacl_box);
      kcg_copy_gf_nacl_op(&_L4_scalarmult_it1_5.a2, &_L18_runSingle_SM1_1);
      kcg_copy_array_uint8_32(&tmp, &_L55_firstPart_SM1_1);
      tmp[0] = _L55_firstPart_SM1_1[0] & kcg_lit_uint8(248);
      tmp[31] = (_L55_firstPart_SM1_1[31] & kcg_lit_uint8(127)) | kcg_lit_uint8(64);
      for (idx = 0; idx < 32; idx++) {
        tmp5[idx] = tmp[31 - idx];
      }
      /* SM1:runSingle:_L8= */
      for (idx = 0; idx < 32; idx++) {
        /* @7/_L4= */
        for (
          idx_scalarmult_it1_8 = 0;
          idx_scalarmult_it1_8 < 8;
          idx_scalarmult_it1_8++) {
          kcg_copy_scalMulAcc_nacl_box(&acc_scalarmult_it1_8, &_L4_scalarmult_it1_5);
          /* @7/_L4=(nacl::box::scalarmult_it2#1)/ */
          scalarmult_it2_nacl_box(
            /* @7/_L4= */(kcg_uint8) idx_scalarmult_it1_8,
            &acc_scalarmult_it1_8,
            tmp5[idx],
            &_L18_runSingle_SM1_1,
            &_L4_scalarmult_it1_5);
        }
        _L11_scalarmult_it1_8 = _L4_scalarmult_it1_5.i - kcg_lit_int32(1);
        _L4_scalarmult_it1_5.i = _L11_scalarmult_it1_8;
        /* SM1:runSingle:_L8= */
        if (!(_L11_scalarmult_it1_8 > kcg_lit_int32(0))) {
          break;
        }
      }
      /* SM1:runSingle:_L27=(nacl::op::inv25519#3)/ */
      inv25519_nacl_op(&_L4_scalarmult_it1_5.a3, &tmp2);
      /* SM1:runSingle:_L30=(nacl::op::M#3)/ */
      M_nacl_op(&_L4_scalarmult_it1_5.a1, &tmp2, &tmp1);
      /* SM1:runSingle:_L26=(nacl::op::pack25519#3)/ */ pack25519_nacl_op(&tmp1, q_1);
      break;
    case SSM_st_runPart_SM1 :
      *again_1 = kcg_true;
      kcg_copy_array_uint32_8(q_1, &outC->k_1);
      kcg_copy_scalMulAcc_nacl_box(&a_partial_1, &outC->a_1);
      kcg_copy_scalMulAcc_nacl_box(&_L4_scalarmult_it1_6, &a_partial_1);
      /* @3/_L4= */
      for (idx = 0; idx < 8; idx++) {
        kcg_copy_scalMulAcc_nacl_box(&acc_scalarmult_it1_6, &_L4_scalarmult_it1_6);
        /* @3/_L4=(nacl::box::scalarmult_it2#1)/ */
        scalarmult_it2_nacl_box(
          /* @3/_L4= */(kcg_uint8) idx,
          &acc_scalarmult_it1_6,
          z_1[0],
          &outC->x_1,
          &_L4_scalarmult_it1_6);
      }
      kcg_copy_scalMulAcc_nacl_box(&a_partial_1, &_L4_scalarmult_it1_6);
      a_partial_1.i = _L4_scalarmult_it1_6.i - kcg_lit_int32(1);
      break;
    case SSM_st_finalPart_SM1 :
      *again_1 = kcg_false;
      kcg_copy_scalMulAcc_nacl_box(&acc_scalarmult_it1_5, &outC->a_1);
      /* SM1:finalPart:_L17= */
      if (outC->a_1.i > kcg_lit_int32(0)) {
        kcg_copy_scalMulAcc_nacl_box(&_L4_scalarmult_it1_7, &acc_scalarmult_it1_5);
        /* @1/_L4= */
        for (idx = 0; idx < 8; idx++) {
          kcg_copy_scalMulAcc_nacl_box(&acc_scalarmult_it1_7, &_L4_scalarmult_it1_7);
          /* @1/_L4=(nacl::box::scalarmult_it2#1)/ */
          scalarmult_it2_nacl_box(
            /* @1/_L4= */(kcg_uint8) idx,
            &acc_scalarmult_it1_7,
            z_1[0],
            &outC->x_1,
            &_L4_scalarmult_it1_7);
        }
        kcg_copy_scalMulAcc_nacl_box(&acc_scalarmult_it1_5, &_L4_scalarmult_it1_7);
        acc_scalarmult_it1_5.i = _L4_scalarmult_it1_7.i - kcg_lit_int32(1);
      }
      /* SM1:finalPart:_L7=(nacl::op::inv25519#2)/ */
      inv25519_nacl_op(&acc_scalarmult_it1_5.a3, &tmp4);
      /* SM1:finalPart:_L5=(nacl::op::M#2)/ */
      M_nacl_op(&acc_scalarmult_it1_5.a1, &tmp4, &tmp3);
      /* SM1:finalPart:_L6=(nacl::op::pack25519#2)/ */ pack25519_nacl_op(&tmp3, q_1);
      break;
    case SSM_st_firstPart_SM1 :
      *again_1 = kcg_true;
      /* @11/_L18= */
      for (idx = 0; idx < 8; idx++) {
        _L18_st32x8_1[idx][0] = /* @12/_L8= */(kcg_uint8) (*our_1).sk_x[idx];
        _L14_st32_1_st32x8_1 = (*our_1).sk_x[idx] >> kcg_lit_uint32(8);
        _L18_st32x8_1[idx][1] = /* @12/_L7= */(kcg_uint8) _L14_st32_1_st32x8_1;
        _L15_st32_1_st32x8_1 = _L14_st32_1_st32x8_1 >> kcg_lit_uint32(8);
        _L18_st32x8_1[idx][2] = /* @12/_L6= */(kcg_uint8) _L15_st32_1_st32x8_1;
        _L18_st32x8_1[idx][3] = /* @12/_L5= */(kcg_uint8)
            (_L15_st32_1_st32x8_1 >> kcg_lit_uint32(8));
      }
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[0], &_L18_st32x8_1[0]);
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[4], &_L18_st32x8_1[1]);
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[8], &_L18_st32x8_1[2]);
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[12], &_L18_st32x8_1[3]);
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[16], &_L18_st32x8_1[4]);
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[20], &_L18_st32x8_1[5]);
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[24], &_L18_st32x8_1[6]);
      kcg_copy_array_uint8_4(&_L79_firstPart_SM1_1[28], &_L18_st32x8_1[7]);
      kcg_copy_array_uint32_8(q_1, &outC->k_1);
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  IfBlock1_clock_1 = check_1 && !*again_1;
  /* IfBlock1: */
  if (IfBlock1_clock_1) {
    *failed_1 = /* IfBlock1:then:_L10=(nacl::box::assertGoodKey#2)/ */
      assertGoodKey_nacl_box(their_1) ||
      /* IfBlock1:then:_L8=(nacl::box::assertGoodKey#1)/ */
      assertGoodKey_nacl_box(q_1);
  }
  else {
    *failed_1 = kcg_false;
  }
  /* SM1: */
  switch (SM1_state_act_1) {
    case SSM_st_donna_SM1 :
      kcg_copy_array_uint8_32(&outC->z_1, &z_1);
      outC->SM1_state_nxt_1 = SSM_st_donna_SM1;
      break;
    case SSM_st_fin_SM1 :
      kcg_copy_array_uint8_32(&outC->z_1, &z_1);
      outC->SM1_state_nxt_1 = SSM_st_fin_SM1;
      break;
    case SSM_st_runSingle_SM1 :
      kcg_copy_array_uint8_32(&outC->z_1, &z_1);
      outC->SM1_state_nxt_1 = SSM_st_runSingle_SM1;
      break;
    case SSM_st_runPart_SM1 :
      kcg_copy_array_uint8_31(&outC->z_1[0], (array_uint8_31 *) &z_1[1]);
      outC->z_1[31] = kcg_lit_uint8(0);
      kcg_copy_scalMulAcc_nacl_box(&outC->a_1, &a_partial_1);
      outC->SM1_state_nxt_1 = SSM_st_runPart_SM1;
      break;
    case SSM_st_finalPart_SM1 :
      IfBlock1_clock_1 = SM1_fired_strong_1 != SSM_TR_no_trans_SM1;
      kcg_copy_array_uint8_32(&outC->z_1, &z_1);
      /* SM1:finalPart: */
      if (IfBlock1_clock_1) {
        outC->SM1_state_nxt_1 = SSM_st_finalPart_SM1;
      }
      else {
        outC->SM1_state_nxt_1 = SSM_st_fin_SM1;
      }
      break;
    case SSM_st_firstPart_SM1 :
      /* SM1:firstPart:_L48=(nacl::op::unpack25519#5)/ */
      unpack25519_nacl_op(their_1, &outC->x_1);
      kcg_copy_array_uint8_32(&tmp5, &_L79_firstPart_SM1_1);
      tmp5[0] = _L79_firstPart_SM1_1[0] & kcg_lit_uint8(248);
      tmp5[31] = (_L79_firstPart_SM1_1[31] & kcg_lit_uint8(127)) | kcg_lit_uint8(64);
      for (idx = 0; idx < 32; idx++) {
        _L55_firstPart_SM1_1[idx] = tmp5[31 - idx];
      }
      kcg_copy_scalMulAcc_nacl_box(
        &_L4_scalarmult_it1_5,
        (scalMulAcc_nacl_box *) &initialScalMulAcc_nacl_box);
      kcg_copy_gf_nacl_op(&_L4_scalarmult_it1_5.a2, &outC->x_1);
      /* @5/_L4= */
      for (idx = 0; idx < 8; idx++) {
        kcg_copy_scalMulAcc_nacl_box(&acc_scalarmult_it1_5, &_L4_scalarmult_it1_5);
        /* @5/_L4=(nacl::box::scalarmult_it2#1)/ */
        scalarmult_it2_nacl_box(
          /* @5/_L4= */(kcg_uint8) idx,
          &acc_scalarmult_it1_5,
          _L55_firstPart_SM1_1[0],
          &outC->x_1,
          &_L4_scalarmult_it1_5);
      }
      kcg_copy_scalMulAcc_nacl_box(&outC->a_1, &_L4_scalarmult_it1_5);
      outC->a_1.i = _L4_scalarmult_it1_5.i - kcg_lit_int32(1);
      kcg_copy_array_uint8_31(
        &outC->z_1[0],
        (array_uint8_31 *) &_L55_firstPart_SM1_1[1]);
      outC->z_1[31] = kcg_lit_uint8(0);
      outC->SM1_state_nxt_1 = SSM_st_runPart_SM1;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  kcg_copy_array_uint32_8(&outC->k_1, q_1);
}

#ifndef KCG_USER_DEFINED_INIT
void scalarmult_init_nacl_box_1(outC_scalarmult_nacl_box_1 *outC)
{
  kcg_size idx;

  for (idx = 0; idx < 8; idx++) {
    outC->k_1[idx] = kcg_lit_uint32(0);
  }
  kcg_copy_gf_nacl_op(&outC->x_1, (gf_nacl_op *) &gf0_nacl_op);
  kcg_copy_scalMulAcc_nacl_box(
    &outC->a_1,
    (scalMulAcc_nacl_box *) &initialScalMulAcc_nacl_box);
  outC->SM1_state_nxt_1 = SSM_st_firstPart_SM1;
  for (idx = 0; idx < 32; idx++) {
    outC->z_1[idx] = kcg_lit_uint8(0);
  }
}
#endif /* KCG_USER_DEFINED_INIT */


void scalarmult_reset_nacl_box_1(outC_scalarmult_nacl_box_1 *outC)
{
  kcg_size idx;

  for (idx = 0; idx < 8; idx++) {
    outC->k_1[idx] = kcg_lit_uint32(0);
  }
  kcg_copy_gf_nacl_op(&outC->x_1, (gf_nacl_op *) &gf0_nacl_op);
  kcg_copy_scalMulAcc_nacl_box(
    &outC->a_1,
    (scalMulAcc_nacl_box *) &initialScalMulAcc_nacl_box);
  outC->SM1_state_nxt_1 = SSM_st_firstPart_SM1;
  for (idx = 0; idx < 32; idx++) {
    outC->z_1[idx] = kcg_lit_uint8(0);
  }
}

/*
  Expanded instances for: nacl::box::scalarmult/
  @7: (nacl::box::scalarmult_it1#8)
  @8: @7/(M::dec#1)
  @9: (slideTypes::st32x8#2)
  @10: @9/(slideTypes::st32#1)
  @3: (nacl::box::scalarmult_it1#6)
  @1: (nacl::box::scalarmult_it1#7)
  @11: (slideTypes::st32x8#1)
  @12: @11/(slideTypes::st32#1)
  @5: (nacl::box::scalarmult_it1#5)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmult_nacl_box_1.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

