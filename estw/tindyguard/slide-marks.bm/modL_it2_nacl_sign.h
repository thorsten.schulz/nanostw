/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _modL_it2_nacl_sign_H_
#define _modL_it2_nacl_sign_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::modL_it2/ */
extern void modL_it2_nacl_sign(
  /* @1/IRIO_Input/, @1/_L3/, _L22/, _L8/, it/ */
  int_slideTypes it,
  /* _L26/, _L9/, carry/ */
  kcg_int64 carry,
  /* _L10/, _L28/, x/ */
  kcg_int64 x,
  /* _L24/, xi/ */
  kcg_int64 xi,
  /* _L12/, i/ */
  int_slideTypes i,
  /* _L18/, carryo/ */
  kcg_int64 *carryo,
  /* _L19/, xo/ */
  kcg_int64 *xo);

/*
  Expanded instances for: nacl::sign::modL_it2/
  @1: (math::InRangeInOut#1)
*/

#endif /* _modL_it2_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_it2_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

