/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _modL_it3_nacl_sign_H_
#define _modL_it3_nacl_sign_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::modL_it3/ */
extern void modL_it3_nacl_sign(
  /* _L1/, carry/ */
  kcg_int64 carry,
  /* _L2/, x/ */
  kcg_int64 x,
  /* _L3/, l/ */
  kcg_int64 l,
  /* _L4/, x31/ */
  kcg_int64 x31,
  /* @1/_L3/, @1/o/, _L18/, carryo/ */
  kcg_int64 *carryo,
  /* _L15/, xo/ */
  kcg_int64 *xo);

/*
  Expanded instances for: nacl::sign::modL_it3/
  @1: (nacl::op::srs8#1)
*/

#endif /* _modL_it3_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_it3_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

