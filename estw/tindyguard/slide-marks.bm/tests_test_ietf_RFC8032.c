/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "tests_test_ietf_RFC8032.h"

/* test::ietf_RFC8032::tests/ */
kcg_bool tests_test_ietf_RFC8032(void)
{
  kcg_bool tmp;
  kcg_size idx;
  kcg_bool tmp1;
  kcg_bool tmp2;
  kcg_bool tmp3;
  /* _L16/, failed/ */
  kcg_bool failed;

  tmp3 = kcg_false;
  tmp2 = kcg_false;
  tmp1 = kcg_false;
  tmp = kcg_false;
  /* _L10=, _L12=, _L19=, _L1= */
  for (idx = 0; idx < 10; idx++) {
    tmp = /* _L1=(test::ietf_RFC8032::bench_ed25520#1)/ */
      bench_ed25520_test_ietf_RFC8032_16(
        tmp,
        (testcase_test_ietf_RFC8032 *) &tc_test_ietf_RFC8032[idx]);
    tmp1 = /* _L10=(test::ietf_RFC8032::bench_ed25520#2)/ */
      bench_ed25520_test_ietf_RFC8032_64(
        tmp1,
        (testcase_test_ietf_RFC8032 *) &tc_test_ietf_RFC8032[idx]);
    tmp2 = /* _L12=(test::ietf_RFC8032::bench_ed25520#3)/ */
      bench_ed25520_test_ietf_RFC8032_256(
        tmp2,
        (testcase_test_ietf_RFC8032 *) &tc_test_ietf_RFC8032[idx]);
    tmp3 = /* _L19=(test::ietf_RFC8032::test_ed25519#4)/ */
      test_ed25519_test_ietf_RFC8032(
        tmp3,
        (testcase_test_ietf_RFC8032 *) &tc_test_ietf_RFC8032[idx]);
  }
  failed = tmp || tmp1 || tmp2 || tmp3;
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** tests_test_ietf_RFC8032.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

