/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_HKDF256_it_test_ietf_RFC6234.h"

/* test::ietf_RFC6234::test_HKDF256_it/ */
kcg_bool test_HKDF256_it_test_ietf_RFC6234(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L36/, tc/ */
  testcaseHKDF_test_ietf_RFC6234 *tc)
{
  /* _L40/ */
  array_uint32_16_2 _L40;
  /* _L41/ */
  kcg_bool _L41;
  /* _L39/ */
  StreamChunk_slideTypes _L39;
  /* _L3/, failed/ */
  kcg_bool failed;

  /* _L39=(hash::sha256::hkdf_extract#1)/ */
  hkdf_extract_hash_sha256_2_2(
    (*tc).ikmlen,
    &(*tc).ikm,
    (*tc).saltlen,
    &(*tc).salt,
    &_L39);
  /* _L40=(hash::sha256::hkdf_expand#1)/ */
  hkdf_expand_hash_sha256_2_2(
    &_L39,
    (*tc).infolen,
    &(*tc).info,
    (*tc).okmlen,
    &_L40,
    &_L41);
  failed = failing || !kcg_comp_StreamChunk_slideTypes(&_L39, &(*tc).prk) ||
    _L41 || !kcg_comp_array_uint32_16_2(&_L40, &(*tc).okm);
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_HKDF256_it_test_ietf_RFC6234.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

