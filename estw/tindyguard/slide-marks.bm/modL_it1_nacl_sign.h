/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _modL_it1_nacl_sign_H_
#define _modL_it1_nacl_sign_H_

#include "kcg_types.h"
#include "modL_it2_nacl_sign.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::modL_it1/ */
extern void modL_it1_nacl_sign(
  /* _L14/, it/ */
  int_slideTypes it,
  /* _L9/, x/ */
  array_int64_64 *x,
  /* _L3/, xo/ */
  array_int64_64 *xo);



#endif /* _modL_it1_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_it1_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

