/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_test_ietf_RFC6234.h"

/* test::ietf_RFC6234::test/ */
kcg_bool test_test_ietf_RFC6234(void)
{
  kcg_bool tmp;
  kcg_size idx;
  kcg_bool tmp1;
  kcg_bool tmp2;
  kcg_bool tmp3;
  kcg_bool tmp4;
  kcg_bool tmp5;
  kcg_bool tmp6;
  kcg_bool tmp7;
  kcg_bool tmp8;
  kcg_bool tmp9;
  kcg_bool tmp10;
  /* _L36/, failed/ */
  kcg_bool failed;

  tmp10 = kcg_false;
  tmp9 = kcg_false;
  tmp8 = kcg_false;
  tmp7 = kcg_false;
  tmp6 = kcg_false;
  tmp5 = kcg_false;
  tmp4 = kcg_false;
  tmp3 = kcg_false;
  tmp2 = kcg_false;
  tmp1 = kcg_false;
  tmp = kcg_false;
  /* _L19= */
  for (idx = 0; idx < 9; idx++) {
    tmp = /* _L19=(test::ietf_RFC6234::test_SHA256_it#1)/ */
      test_SHA256_it_test_ietf_RFC6234(
        tmp,
        (testcase_test_ietf_RFC6234 *) &test_sha256_test_ietf_RFC6234[idx]);
  }
  /* _L22= */
  for (idx = 0; idx < 4; idx++) {
    tmp1 = /* _L22=(test::ietf_RFC6234::test_HMAC256_it#1)/ */
      test_HMAC256_it_test_ietf_RFC6234(
        tmp1,
        (testcase_test_ietf_RFC6234 *) &test_hmac256_test_ietf_RFC6234[idx]);
  }
  /* _L10= */
  for (idx = 0; idx < 9; idx++) {
    tmp2 = /* _L10=(test::ietf_RFC6234::test_SHA512_it#1)/ */
      test_SHA512_it_test_ietf_RFC6234(
        tmp2,
        (testcase_test_ietf_RFC6234 *) &test_sha512_test_ietf_RFC6234[idx]);
  }
  /* _L13= */
  for (idx = 0; idx < 4; idx++) {
    tmp3 = /* _L13=(test::ietf_RFC6234::test_HMAC512_it#1)/ */
      test_HMAC512_it_test_ietf_RFC6234(
        tmp3,
        (testcase_test_ietf_RFC6234 *) &test_hmac512_test_ietf_RFC6234[idx]);
  }
  /* _L23= */
  for (idx = 0; idx < 3; idx++) {
    tmp4 = /* _L23=(test::ietf_RFC6234::test_HKDF256_it#1)/ */
      test_HKDF256_it_test_ietf_RFC6234(
        tmp4,
        (testcaseHKDF_test_ietf_RFC6234 *) &test_hkdf256_test_ietf_RFC6234[idx]);
  }
  /* _L26=, _L28=, _L29=, _L31=, _L33=, _L35= */
  for (idx = 0; idx < 10; idx++) {
    tmp5 = /* _L26=(test::ietf_RFC6234::bench_hash256#3)/ */
      bench_hash256_test_ietf_RFC6234_16(
        tmp5,
        (simpleTC_test *) &stc_test[idx]);
    tmp6 = /* _L28=(test::ietf_RFC6234::bench_hash256#2)/ */
      bench_hash256_test_ietf_RFC6234_64(
        tmp6,
        (simpleTC_test *) &stc_test[idx]);
    tmp7 = /* _L29=(test::ietf_RFC6234::bench_hash256#1)/ */
      bench_hash256_test_ietf_RFC6234_256(
        tmp7,
        (simpleTC_test *) &stc_test[idx]);
    tmp8 = /* _L33=(test::ietf_RFC6234::bench_hash512#1)/ */
      bench_hash512_test_ietf_RFC6234_16(
        tmp8,
        (simpleTC_test *) &stc_test[idx]);
    tmp9 = /* _L31=(test::ietf_RFC6234::bench_hash512#2)/ */
      bench_hash512_test_ietf_RFC6234_64(
        tmp9,
        (simpleTC_test *) &stc_test[idx]);
    tmp10 = /* _L35=(test::ietf_RFC6234::bench_hash512#3)/ */
      bench_hash512_test_ietf_RFC6234_256(
        tmp10,
        (simpleTC_test *) &stc_test[idx]);
  }
  failed = tmp || tmp1 || tmp2 || tmp3 || tmp4 || (tmp5 || tmp6 || tmp7) ||
    (tmp8 || tmp9 || tmp10) || /* _L37=(test::ietf_RFC6234::bench_HKDF256#1)/ */
    bench_HKDF256_test_ietf_RFC6234(
      kcg_false,
      (testcase_hkdf_test_ietf_RFC7693 *) &test_hkdf_test_ietf_RFC7693);
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_test_ietf_RFC6234.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

