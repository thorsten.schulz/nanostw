/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block32_it2_hash_libsha_2_13_22_6_11_25_H_
#define _block32_it2_hash_libsha_2_13_22_6_11_25_H_

#include "kcg_types.h"
#include "SIGMA32_hash_libsha_2_13_22.h"
#include "Maj_hash_libsha_uint32.h"
#include "Ch_hash_libsha_uint32.h"
#include "SIGMA32_hash_libsha_6_11_25.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::block32_it2/ */
extern void block32_it2_hash_libsha_2_13_22_6_11_25(
  /* _L1/, _L37/, ain/ */
  array_uint32_8 *ain_2_13_22_6_11_25,
  /* _L13/, w/ */
  kcg_uint32 w_2_13_22_6_11_25,
  /* _L12/, k/ */
  kcg_uint32 k_2_13_22_6_11_25,
  /* _L36/, aout/ */
  array_uint32_8 *aout_2_13_22_6_11_25);



#endif /* _block32_it2_hash_libsha_2_13_22_6_11_25_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block32_it2_hash_libsha_2_13_22_6_11_25.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

