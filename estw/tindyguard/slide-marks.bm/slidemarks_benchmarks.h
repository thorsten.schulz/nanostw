/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _slidemarks_benchmarks_H_
#define _slidemarks_benchmarks_H_

#include "kcg_types.h"
#include "pullAll_test.h"
#include "ProtocolDryRun_tindyguard_test.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_ProtocolDryRun_tindyguard_test /* _L1=(tindyguard::test::ProtocolDryRun#1)/ */ Context_ProtocolDryRun_1;
  outC_pullAll_test /* _L2=(test::pullAll#1)/ */ Context_pullAll_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_slidemarks_benchmarks;

/* ===========  node initialization and cycle functions  =========== */
/* benchmarks::slidemarks/ */
extern void slidemarks_benchmarks(
  /* _L4/, failed/ */
  kcg_bool *failed,
  /* _L3/, failed2/ */
  kcg_bool *failed2,
  outC_slidemarks_benchmarks *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void slidemarks_reset_benchmarks(outC_slidemarks_benchmarks *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void slidemarks_init_benchmarks(outC_slidemarks_benchmarks *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _slidemarks_benchmarks_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** slidemarks_benchmarks.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

