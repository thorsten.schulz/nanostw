/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "neq25519_nacl_op.h"

/* nacl::op::neq25519/ */
int_slideTypes neq25519_nacl_op(
  /* _L1/, a/ */
  gf_nacl_op *a,
  /* _L2/, b/ */
  gf_nacl_op *b)
{
  array_uint32_8 tmp;
  array_uint32_8 tmp1;
  /* _L5/, neq/ */
  int_slideTypes neq;

  /* _L3=(nacl::op::pack25519#1)/ */ pack25519_nacl_op(a, &tmp);
  /* _L4=(nacl::op::pack25519#2)/ */ pack25519_nacl_op(b, &tmp1);
  neq = /* _L5=(M::cmp#1)/ */ cmp_M_uint32_8(&tmp, &tmp1);
  return neq;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** neq25519_nacl_op.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

