/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _pack_nacl_sign_H_
#define _pack_nacl_sign_H_

#include "kcg_types.h"
#include "par25519_nacl_op.h"
#include "inv25519_nacl_op.h"
#include "pack25519_nacl_op.h"
#include "M_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::pack/ */
extern void pack_nacl_sign(
  /* _L2/, p/ */
  cpoint_nacl_op *p,
  /* _L14/, t/ */
  array_uint32_8 *t);



#endif /* _pack_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pack_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

