/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "xaead_nacl_box_2_1_20.h"

/* nacl::box::xaead/ */
void xaead_nacl_box_2_1_20(
  /* _L23/, msg/ */
  array_uint32_16_2 *msg_2_1_20,
  /* _L22/, mlen/ */
  int_slideTypes mlen_2_1_20,
  /* _L20/, ad/ */
  array_uint32_16_1 *ad_2_1_20,
  /* _L21/, adlen/ */
  int_slideTypes adlen_2_1_20,
  /* _L5/, xnonce/ */
  XNonce_nacl_core_chacha *xnonce_2_1_20,
  /* _L3/, key/ */
  Key_nacl_core *key_2_1_20,
  /* _L18/, a/ */
  Mac_nacl_onetime *a_2_1_20,
  /* _L19/, cm/ */
  array_uint32_16_2 *cm_2_1_20)
{
  array_uint32_3 tmp;
  Key_nacl_core tmp1;

  tmp[0] = kcg_lit_uint32(0);
  tmp[1] = (*xnonce_2_1_20)[4];
  tmp[2] = (*xnonce_2_1_20)[5];
  /* _L17=(nacl::core::chacha::half#1)/ */
  half_nacl_core_chacha_20(
    (array_uint32_4 *) &(*xnonce_2_1_20)[0],
    key_2_1_20,
    &tmp1);
  /* _L18=(nacl::box::aead#1)/ */
  aead_nacl_box_2_1_20(
    msg_2_1_20,
    mlen_2_1_20,
    ad_2_1_20,
    adlen_2_1_20,
    &tmp,
    &tmp1,
    a_2_1_20,
    cm_2_1_20);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** xaead_nacl_box_2_1_20.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

