/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "respond_tindyguard_handshake_8.h"

/* tindyguard::handshake::respond/ */
void respond_tindyguard_handshake_8(
  /* _L21/, rlength/ */
  length_t_udp rlength_8,
  /* _L12/, initmsg/ */
  array_uint32_16_4 *initmsg_8,
  /* _L32/, endpoint/ */
  peer_t_udp *endpoint_8,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8,
  /* session/ */
  Session_tindyguardTypes *session_8,
  /* knownPeer/ */
  _6_array *knownPeer_8,
  /* _L34/, now/ */
  kcg_int64 now_8,
  /* _L24/, slength/ */
  length_t_udp *slength_8,
  /* _L25/, responsemsg/ */
  array_uint32_16_4 *responsemsg_8,
  /* _L3/, soo/ */
  Session_tindyguardTypes *soo_8,
  /* _L33/, failed/ */
  kcg_bool *failed_8,
  /* _L30/, noPeer/ */
  kcg_bool *noPeer_8,
  /* _L1/, weakKey/ */
  kcg_bool *weakKey_8,
  outC_respond_tindyguard_handshake_8 *outC)
{
  kcg_bool tmp;
  array_uint32_16_1 tmp1;
  array_uint32_16 tmp2;
  array_uint32_16 tmp3;
  array_uint32_8_2 tmp4;
  array_uint32_16 tmp5;
  array_uint32_8_2 tmp6;
  HashChunk_hash_blake2s tmp7;
  array_uint32_16_1 tmp8;
  array_uint32_16 tmp9;
  array_uint32_16_1 tmp10;
  array_uint32_16 tmp11;
  array_uint32_8 tmp12;
  array_uint32_16 tmp13;
  array_uint32_8 tmp14;
  array_uint32_16 tmp15;
  array_uint32_16 tmp16;
  array_uint8_4_4 tmp17;
  kcg_size idx;
  array_uint32_16_2 tmp18;
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  /* @12/_L8/ */
  kcg_int32 _L8_stream_it_1_hash128_7_build_response_1;
  kcg_int32 tmp_stream_it_1_hash128_7_build_response_1;
  array_uint32_16_3 tmp19;
  array_uint32_8 acc20;
  kcg_bool _21_cond_iterw;
  /* @14/_L8/ */
  kcg_int32 _L8_stream_it_1_hash128_6_build_response_1;
  kcg_int32 tmp_stream_it_1_hash128_6_build_response_1;
  /* @1/_L482/ */
  array_uint32_1 _L482_build_response_1;
  /* @1/_L481/ */
  kcg_bool _L481_build_response_1;
  /* @1/_L379/ */
  array_uint32_16_1 _L379_build_response_1;
  /* @1/_L305/ */
  array_uint32_8_2 _L305_build_response_1;
  /* @1/_L294/ */
  array_uint32_3 _L294_build_response_1;
  /* @1/_L276/ */
  array_uint32_8_3 _L276_build_response_1;
  /* @1/_L506/, @1/m_rsp2/ */
  StreamChunk_slideTypes m_rsp2_build_response_1;
  /* @1/_L520/, @1/m_rsp1/ */
  StreamChunk_slideTypes m_rsp1_build_response_1;
  array_uint32_16_1 noname_build_response_1;
  /* @1/IfBlock1:else: */
  kcg_bool else_clock_build_response_1_IfBlock1;
  /* @13/_L61/ */
  array_uint32_8 _L61_hash128_6_build_response_1_3;
  /* @1/IfBlock1:else:then:_L8/, @13/_L75/, @13/mac/ */
  array_uint32_4 _L75_hash128_6_build_response_1_3;
  /* @11/_L61/ */
  array_uint32_8 _L61_hash128_7_build_response_1_2;
  /* @1/_L515/, @11/_L75/, @11/mac/ */
  array_uint32_4 _L75_hash128_7_build_response_1_2;
  /* @1/_L290/, @9/_L1/, @9/a/ */
  Mac_nacl_onetime _L1_ldAuth_6_build_response_1;
  /* @4/_L5/ */
  array_uint32_16 _L5_DHDerive_4_build_response_1;
  /* @1/_L274/, @4/_L3/, @4/fail/ */
  kcg_bool _L3_DHDerive_4_build_response_1;
  /* @4/_L14/ */
  array_uint32_16_1 _L14_DHDerive_4_build_response_1;
  /* @3/_L5/ */
  array_uint32_16 _L5_DHDerive_3_build_response_1;
  /* @1/_L269/, @3/_L3/, @3/fail/ */
  kcg_bool _L3_DHDerive_3_build_response_1;
  /* @3/_L14/ */
  array_uint32_16_1 _L14_DHDerive_3_build_response_1;
  kcg_bool op_call_ephemeral_1_build_response_1;
  /* @1/_L205/,
     @1/_L513/,
     @1/ek/,
     @2/IfBlock1:else:_L4/,
     @2/kp/,
     @3/_L9/,
     @3/sk/,
     @4/_L9/,
     @4/sk/ */
  KeyPair32_slideTypes kp_partial_ephemeral_1_build_response_1;
  /* @2/IfBlock1:else:_L6/ */
  kcg_bool _L6_ephemeral_1_build_response_1_else_IfBlock1;
  /* @2/IfBlock1:else:_L1/ */
  kcg_bool _L1_ephemeral_1_build_response_1_else_IfBlock1;
  /* @2/IfBlock1:else:_L2/ */
  array_uint32_8 _L2_ephemeral_1_build_response_1_else_IfBlock1;
  kcg_bool op_call;
  /* _L9/ */
  Session_tindyguardTypes _L9_8;
  /* _L8/ */
  kcg_bool _L8_8;
  /* _L31/ */
  kcg_bool _L31_8;

  /* _L9=(tindyguard::handshake::eval_init#1)/ */
  eval_init_tindyguard_handshake_8(
    initmsg_8,
    rlength_8,
    endpoint_8,
    sks_8,
    knownPeer_8,
    session_8,
    &_L9_8,
    &_L8_8,
    noPeer_8);
  _L31_8 = !_L8_8;
  if (_L31_8) {
    kcg_copy_StreamChunk_slideTypes(&tmp18[0], &_L9_8.peer.hcache.salt);
    m_rsp1_build_response_1[0] = MsgType_Response_tindyguard;
    m_rsp1_build_response_1[2] = _L9_8.their;
    kcg_copy_StreamChunk_slideTypes(
      &_L379_build_response_1[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    /* @1/_L481=(sys::random#1)/ */
    random01_sys_specialization(
      &_L481_build_response_1,
      &_L482_build_response_1);
    m_rsp1_build_response_1[1] = _L482_build_response_1[0];
    /* @2/IfBlock1:else:_L1=(sys::random#1)/ */
    random08_sys_specialization(
      &_L1_ephemeral_1_build_response_1_else_IfBlock1,
      &_L2_ephemeral_1_build_response_1_else_IfBlock1);
    _L6_ephemeral_1_build_response_1_else_IfBlock1 =
      !_L1_ephemeral_1_build_response_1_else_IfBlock1;
    if (_L6_ephemeral_1_build_response_1_else_IfBlock1) {
      /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
      keyPair_nacl_box_20(
        &_L2_ephemeral_1_build_response_1_else_IfBlock1,
        &op_call_ephemeral_1_build_response_1,
        &kp_partial_ephemeral_1_build_response_1,
        &tmp,
        &outC->Context_keyPair_2_ephemeral_1_build_response_1);
    }
    else {
      kcg_copy_KeyPair32_slideTypes(
        &kp_partial_ephemeral_1_build_response_1,
        (KeyPair32_slideTypes *) &ZeroKeyPair_slideTypes);
      tmp = kcg_true;
    }
    /* @4/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &kp_partial_ephemeral_1_build_response_1,
      &_L9_8.peer.tpub,
      kcg_true,
      &_L3_DHDerive_4_build_response_1,
      (Key32_slideTypes *) &_L5_DHDerive_4_build_response_1[0]);
    /* @3/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &kp_partial_ephemeral_1_build_response_1,
      &_L9_8.handshake.their,
      kcg_true,
      &_L3_DHDerive_3_build_response_1,
      (Key32_slideTypes *) &_L5_DHDerive_3_build_response_1[0]);
    op_call = tmp || _L3_DHDerive_3_build_response_1 ||
      _L3_DHDerive_4_build_response_1 || _L481_build_response_1;
    kcg_copy_psk_tindyguardTypes(&tmp2[0], &_L9_8.peer.preshared);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_4_build_response_1[idx + 8] = kcg_lit_uint32(0);
      _L5_DHDerive_3_build_response_1[idx + 8] = kcg_lit_uint32(0);
      tmp2[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp1[0], &tmp2);
    kcg_copy_Key32_slideTypes(
      &tmp9[0],
      &kp_partial_ephemeral_1_build_response_1.pk_y);
    for (idx = 0; idx < 8; idx++) {
      tmp9[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp8[0], &tmp9);
    /* @1/_L257=(hash::blake2s::hkdfChunk1#2)/ */
    hkdfChunk1_hash_blake2s_1(
      &tmp8,
      kcg_lit_int32(32),
      &_L9_8.handshake.chainingKey,
      &tmp7);
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_3_build_response_1[0],
      &_L5_DHDerive_3_build_response_1);
    /* @3/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_3_build_response_1,
      kcg_lit_int32(32),
      &tmp7,
      &tmp6);
    kcg_copy_Hash_hash_blake2s(&tmp5[0], &tmp6[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp5[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(
      &_L14_DHDerive_4_build_response_1[0],
      &_L5_DHDerive_4_build_response_1);
    /* @4/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_4_build_response_1,
      kcg_lit_int32(32),
      &tmp5,
      &tmp4);
    kcg_copy_Hash_hash_blake2s(&tmp3[0], &tmp4[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp3[idx + 8] = kcg_lit_uint32(0);
    }
    /* @1/_L276=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_3(
      &tmp1,
      kcg_lit_int32(32),
      &tmp3,
      &_L276_build_response_1);
    tmp12[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp12[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    tmp14[0] = kcg_lit_uint32(1795745351);
    kcg_copy_array_uint32_7(&tmp14[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
    kcg_copy_Hash_hash_blake2s(&tmp15[0], &_L9_8.handshake.ihash);
    kcg_copy_Key32_slideTypes(
      &tmp15[8],
      &kp_partial_ephemeral_1_build_response_1.pk_y);
    /* @6/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp14,
      kcg_lit_int32(64),
      &tmp15,
      kcg_false,
      (array_uint32_8 *) &tmp13[0]);
    kcg_copy_Hash_hash_blake2s(&tmp13[8], &_L276_build_response_1[1]);
    /* @8/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &tmp12,
      kcg_lit_int32(64),
      &tmp13,
      kcg_false,
      (array_uint32_8 *) &tmp11[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp11[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp10[0], &tmp11);
    for (idx = 0; idx < 3; idx++) {
      _L294_build_response_1[idx] = kcg_lit_uint32(0);
    }
    /* @1/_L290=(nacl::box::aead#1)/ */
    aead_nacl_box_1_1_20(
      &_L379_build_response_1,
      kcg_lit_int32(0),
      &tmp10,
      kcg_lit_int32(32),
      &_L294_build_response_1,
      &_L276_build_response_1[2],
      &_L1_ldAuth_6_build_response_1,
      &noname_build_response_1);
    kcg_copy_array_uint8_4(
      &tmp17[0],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[0]);
    kcg_copy_array_uint8_4(
      &tmp17[1],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[4]);
    kcg_copy_array_uint8_4(
      &tmp17[2],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[8]);
    kcg_copy_array_uint8_4(
      &tmp17[3],
      (array_uint8_4 *) &_L1_ldAuth_6_build_response_1[12]);
    kcg_copy_Hash_hash_blake2s(&tmp16[0], &_L276_build_response_1[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp16[idx + 8] = kcg_lit_uint32(0);
    }
    /* @1/_L305=(hash::blake2s::hkdf#2)/ */
    hkdf_hash_blake2s_1_2(
      &_L379_build_response_1,
      kcg_lit_int32(0),
      &tmp16,
      &_L305_build_response_1);
    kcg_copy_Key32_slideTypes(
      &m_rsp1_build_response_1[3],
      &kp_partial_ephemeral_1_build_response_1.pk_y);
    /* @9/_L24= */
    for (idx = 0; idx < 4; idx++) {
      m_rsp1_build_response_1[idx + 11] = /* @10/_L14= */(kcg_uint32)
          tmp17[idx][0] | kcg_lsl_uint32(
          /* @10/_L15= */(kcg_uint32) tmp17[idx][1],
          kcg_lit_uint32(8)) | kcg_lsl_uint32(
          /* @10/_L16= */(kcg_uint32) tmp17[idx][2],
          kcg_lit_uint32(16)) | kcg_lsl_uint32(
          /* @10/_L17= */(kcg_uint32) tmp17[idx][3],
          kcg_lit_uint32(24));
    }
    m_rsp1_build_response_1[15] = kcg_lit_uint32(0);
    kcg_copy_StreamChunk_slideTypes(&tmp18[1], &m_rsp1_build_response_1);
    _L61_hash128_7_build_response_1_2[0] = kcg_lit_uint32(1795737207);
    kcg_copy_array_uint32_7(
      &_L61_hash128_7_build_response_1_2[1],
      (array_uint32_7 *) &IV_hash_blake2s[1]);
    /* @11/_L60= */
    for (idx = 0; idx < 2; idx++) {
      kcg_copy_array_uint32_8(&acc, &_L61_hash128_7_build_response_1_2);
      _L8_stream_it_1_hash128_7_build_response_1 = /* @11/_L60= */(kcg_int32)
          idx * hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
      cond_iterw = kcg_lit_int32(124) > _L8_stream_it_1_hash128_7_build_response_1;
      /* @12/_L14= */
      if (cond_iterw) {
        tmp_stream_it_1_hash128_7_build_response_1 =
          _L8_stream_it_1_hash128_7_build_response_1;
      }
      else {
        tmp_stream_it_1_hash128_7_build_response_1 = kcg_lit_int32(124);
      }
      /* @12/_L2=(hash::blake2s::block_refine#1)/ */
      block_refine_hash_blake2s(
        &acc,
        tmp_stream_it_1_hash128_7_build_response_1,
        &tmp18[idx],
        cond_iterw,
        &_L61_hash128_7_build_response_1_2);
      /* @11/_L60= */
      if (!cond_iterw) {
        break;
      }
    }
    kcg_copy_array_uint32_4(
      &_L75_hash128_7_build_response_1_2,
      (array_uint32_4 *) &_L61_hash128_7_build_response_1_2[0]);
    kcg_copy_array_uint32_3(
      &m_rsp2_build_response_1[0],
      (array_uint32_3 *) &_L75_hash128_7_build_response_1_2[1]);
    for (idx = 0; idx < 13; idx++) {
      m_rsp2_build_response_1[idx + 3] = kcg_lit_uint32(0);
    }
    m_rsp1_build_response_1[15] = _L75_hash128_7_build_response_1_2[0];
    *weakKey_8 = op_call;
    /* @1/IfBlock1:, @1/_L367= */
    if (op_call) {
      for (idx = 0; idx < 4; idx++) {
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_8)[idx],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
      }
      *slength_8 = nil_udp;
      kcg_copy_Session_tindyguardTypes(
        soo_8,
        (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
    }
    else {
      else_clock_build_response_1_IfBlock1 = now_8 < _L9_8.peer.cookie.opened +
        cookieEdible_tindyguard_conf;
      kcg_copy_secret_tindyguardTypes(&(*soo_8).ot, &_L305_build_response_1[1]);
      kcg_copy_secret_tindyguardTypes(&(*soo_8).to, &_L305_build_response_1[0]);
      (*soo_8).ot_cnt = kcg_lit_uint64(0);
      (*soo_8).to_cnt = kcg_lit_uint64(0);
      (*soo_8).to_cnt_cache = kcg_lit_uint64(0);
      (*soo_8).our = _L482_build_response_1[0];
      (*soo_8).their = _L9_8.their;
      (*soo_8).rx_bytes = kcg_lit_int64(0);
      (*soo_8).rx_cnt = kcg_lit_int32(0);
      (*soo_8).tx_cnt = kcg_lit_int32(0);
      (*soo_8).txTime = kcg_lit_int64(-1);
      (*soo_8).sTime = _L9_8.peer.endpoint.mtime;
      (*soo_8).pid = _L9_8.pid;
      kcg_copy_State_tindyguard_handshake(
        &(*soo_8).handshake,
        (State_tindyguard_handshake *) &EmptyState_tindyguard_handshake);
      kcg_copy_Peer_tindyguardTypes(&(*soo_8).peer, &_L9_8.peer);
      (*soo_8).transmissive = kcg_false;
      (*soo_8).gotKeepAlive = kcg_false;
      (*soo_8).sentKeepAlive = kcg_false;
      /* @1/IfBlock1:else: */
      if (else_clock_build_response_1_IfBlock1) {
        kcg_copy_StreamChunk_slideTypes(&tmp19[0], &_L9_8.peer.cookie.content);
        kcg_copy_StreamChunk_slideTypes(&tmp19[1], &m_rsp1_build_response_1);
        kcg_copy_StreamChunk_slideTypes(&tmp19[2], &m_rsp2_build_response_1);
        _L61_hash128_6_build_response_1_3[0] = kcg_lit_uint32(1795737207);
        kcg_copy_array_uint32_7(
          &_L61_hash128_6_build_response_1_3[1],
          (array_uint32_7 *) &IV_hash_blake2s[1]);
        /* @13/_L60= */
        for (idx = 0; idx < 3; idx++) {
          kcg_copy_array_uint32_8(&acc20, &_L61_hash128_6_build_response_1_3);
          _L8_stream_it_1_hash128_6_build_response_1 =
            /* @13/_L60= */(kcg_int32) idx * hBlockBytes_hash_blake2s +
            hBlockBytes_hash_blake2s;
          _21_cond_iterw = kcg_lit_int32(140) >
            _L8_stream_it_1_hash128_6_build_response_1;
          /* @14/_L14= */
          if (_21_cond_iterw) {
            tmp_stream_it_1_hash128_6_build_response_1 =
              _L8_stream_it_1_hash128_6_build_response_1;
          }
          else {
            tmp_stream_it_1_hash128_6_build_response_1 = kcg_lit_int32(140);
          }
          /* @14/_L2=(hash::blake2s::block_refine#1)/ */
          block_refine_hash_blake2s(
            &acc20,
            tmp_stream_it_1_hash128_6_build_response_1,
            &tmp19[idx],
            _21_cond_iterw,
            &_L61_hash128_6_build_response_1_3);
          /* @13/_L60= */
          if (!_21_cond_iterw) {
            break;
          }
        }
        kcg_copy_array_uint32_4(
          &_L75_hash128_6_build_response_1_3,
          (array_uint32_4 *) &_L61_hash128_6_build_response_1_3[0]);
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_8)[0], &m_rsp1_build_response_1);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_8)[2],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_8)[3],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_8)[1], &m_rsp2_build_response_1);
        (*responsemsg_8)[1][3] = _L75_hash128_6_build_response_1_3[0];
        (*responsemsg_8)[1][4] = _L75_hash128_6_build_response_1_3[1];
        (*responsemsg_8)[1][5] = _L75_hash128_6_build_response_1_3[2];
        (*responsemsg_8)[1][6] = _L75_hash128_6_build_response_1_3[3];
        *slength_8 = kcg_lit_int32(92);
      }
      else {
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_8)[0], &m_rsp1_build_response_1);
        kcg_copy_StreamChunk_slideTypes(&(*responsemsg_8)[1], &m_rsp2_build_response_1);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_8)[2],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        kcg_copy_StreamChunk_slideTypes(
          &(*responsemsg_8)[3],
          (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
        *slength_8 = kcg_lit_int32(92);
      }
    }
  }
  else {
    *weakKey_8 = kcg_true;
    for (idx = 0; idx < 4; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &(*responsemsg_8)[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    *slength_8 = nil_udp;
    kcg_copy_Session_tindyguardTypes(soo_8, session_8);
  }
  *failed_8 = *weakKey_8 || _L8_8;
}

#ifndef KCG_USER_DEFINED_INIT
void respond_init_tindyguard_handshake_8(
  outC_respond_tindyguard_handshake_8 *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_init_nacl_box_20(
    &outC->Context_keyPair_2_ephemeral_1_build_response_1);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void respond_reset_tindyguard_handshake_8(
  outC_respond_tindyguard_handshake_8 *outC)
{
  /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */
  keyPair_reset_nacl_box_20(
    &outC->Context_keyPair_2_ephemeral_1_build_response_1);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/*
  Expanded instances for: tindyguard::handshake::respond/
  @1: (tindyguard::handshake::build_response#1)
  @3: @1/(tindyguard::handshake::DHDerive#3)
  @4: @1/(tindyguard::handshake::DHDerive#4)
  @5: @1/(hash::blake2s::single#4)
  @6: @5/(hash::blake2s::stream_it#1)
  @7: @1/(hash::blake2s::single#5)
  @8: @7/(hash::blake2s::stream_it#1)
  @9: @1/(slideTypes::ldAuth#6)
  @10: @9/(slideTypes::ld32x1#1)
  @11: @1/(hash::blake2s::hash128#7)
  @12: @11/(hash::blake2s::stream_it#1)
  @13: @1/(hash::blake2s::hash128#6)
  @14: @13/(hash::blake2s::stream_it#1)
  @2: @1/(tindyguard::handshake::ephemeral#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** respond_tindyguard_handshake_8.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

