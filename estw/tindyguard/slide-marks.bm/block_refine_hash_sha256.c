/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "block_refine_hash_sha256.h"

/* hash::sha256::block_refine/ */
void block_refine_hash_sha256(
  /* _L2/, zin/ */
  array_uint32_8 *zin,
  /* _L4/, m/ */
  array_uint32_16 *m,
  /* _L1/, zout/ */
  array_uint32_8 *zout)
{
  array_uint32_16 tmp;
  kcg_size idx;

  /* _L3= */
  for (idx = 0; idx < 16; idx++) {
    tmp[idx] = (((*m)[idx] & kcg_lit_uint32(4278190080)) >> kcg_lit_uint32(
          24)) | (((*m)[idx] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
      kcg_lsl_uint32((*m)[idx] & kcg_lit_uint32(65280), kcg_lit_uint32(8)) |
      kcg_lsl_uint32((*m)[idx] & kcg_lit_uint32(255), kcg_lit_uint32(24));
  }
  /* _L1=(hash::libsha::process32#1)/ */
  process32_hash_libsha_4_2_13_22_6_11_25_7_18_3_17_19_10(
    zin,
    &tmp,
    (array_uint32_16_4 *) &K_hash_sha256,
    zout);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block_refine_hash_sha256.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

