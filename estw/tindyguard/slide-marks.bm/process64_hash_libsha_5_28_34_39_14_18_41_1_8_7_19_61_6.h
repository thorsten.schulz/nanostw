/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6_H_
#define _process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6_H_

#include "kcg_types.h"
#include "block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6.h"
#include "block64_it2_hash_libsha_28_34_39_14_18_41.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::process64/ */
extern void process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6(
  /* _L2/, _L4/, zin/ */
  array_uint64_8 *zin_5_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L13/, m/ */
  array_uint64_16 *m_5_28_34_39_14_18_41_1_8_7_19_61_6,
  /* K/, _L26/, _L27/ */
  array_uint64_16_5 *K_5_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L3/, zout/ */
  array_uint64_8 *zout_5_28_34_39_14_18_41_1_8_7_19_61_6);



#endif /* _process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

