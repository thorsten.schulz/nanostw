/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _KCG_IMPORTED_FUNCTIONS_H_
#define _KCG_IMPORTED_FUNCTIONS_H_

#include "kcg_types.h"

#ifndef htonl_sys_specialization
/* sys::specialization::htonl/ */
extern kcg_uint32 htonl_sys_specialization(/* h/ */ kcg_uint32 h);
#endif /* htonl_sys_specialization */

#ifndef tai_sys
/* sys::tai/ */
extern void tai_sys(
  /* bigEndian/ */
  kcg_bool bigEndian,
  /* xsec/ */
  kcg_uint32 *xsec,
  /* sec/ */
  kcg_uint32 *sec,
  /* nano/ */
  kcg_uint32 *nano);
#endif /* tai_sys */

#ifndef random01_sys_specialization
/* sys::specialization::random01/ */
extern void random01_sys_specialization(
  /* fail/ */
  kcg_bool *fail,
  /* data/ */
  array_uint32_1 *data);
#endif /* random01_sys_specialization */

#ifndef random08_sys_specialization
/* sys::specialization::random08/ */
extern void random08_sys_specialization(
  /* fail/ */
  kcg_bool *fail,
  /* data/ */
  array_uint32_8 *data);
#endif /* random08_sys_specialization */

#ifndef curve25519_donna_nacl_box
/* nacl::box::curve25519_donna/ */
extern void curve25519_donna_nacl_box(
  /* ourPriv/ */
  Key32_slideTypes *ourPriv,
  /* their/ */
  Key32_slideTypes *their,
  /* key/ */
  Key32_slideTypes *key);
#endif /* curve25519_donna_nacl_box */

#endif /* _KCG_IMPORTED_FUNCTIONS_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_imported_functions.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

