/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _neq25519_nacl_op_H_
#define _neq25519_nacl_op_H_

#include "kcg_types.h"
#include "cmp_M_uint32_8.h"
#include "pack25519_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::neq25519/ */
extern int_slideTypes neq25519_nacl_op(
  /* _L1/, a/ */
  gf_nacl_op *a,
  /* _L2/, b/ */
  gf_nacl_op *b);



#endif /* _neq25519_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** neq25519_nacl_op.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

