/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_hkdf_it_test_ietf_RFC7693.h"

/* test::ietf_RFC7693::test_hkdf_it/ */
kcg_bool test_hkdf_it_test_ietf_RFC7693(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L4/, tc/ */
  testcase_hkdf_test_ietf_RFC7693 *tc)
{
  kcg_size idx;
  /* @2/IfBlock1: */
  kcg_bool IfBlock1_clock_hkdf_expand_it_chunk_2_hkdfChunk_1;
  /* @2/IfBlock1:else:_L9/ */
  array_uint32_16_1 _L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1;
  /* @2/IfBlock1:then:_L10/ */
  array_uint32_16_1 _L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1;
  /* @1/_L18/ */
  HashChunk_hash_blake2s _L18_hkdfChunk_1_2_3;
  HashChunk_hash_blake2s noname_hkdfChunk_1;
  /* @1/_L14/, @1/okm/, _L51/ */
  array_uint32_16_3 _L51;
  /* _L3/, failed/ */
  kcg_bool failed;

  /* @1/_L18=(hash::blake2s::hmacChunk#1)/ */
  hmacChunk_hash_blake2s_2(
    &(*tc).msg,
    (*tc).mlen,
    &(*tc).key,
    &_L18_hkdfChunk_1_2_3);
  kcg_copy_HashChunk_hash_blake2s(
    &noname_hkdfChunk_1,
    (HashChunk_hash_blake2s *) &ZeroChunk_slideTypes);
  /* @1/_L16= */
  for (idx = 0; idx < 3; idx++) {
    IfBlock1_clock_hkdf_expand_it_chunk_2_hkdfChunk_1 =
      /* @1/_L16= */(kcg_uint32) idx == kcg_lit_uint32(0);
    /* @2/IfBlock1: */
    if (IfBlock1_clock_hkdf_expand_it_chunk_2_hkdfChunk_1) {
      kcg_copy_StreamChunk_slideTypes(
        &_L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1[0],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
      _L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1[0][0] = kcg_lit_uint32(1);
      /* @2/IfBlock1:then:_L5=(hash::blake2s::hmacChunk#2)/ */
      hmacChunk_hash_blake2s_1(
        &_L10_hkdf_expand_it_chunk_2_hkdfChunk_1_then_IfBlock1,
        kcg_lit_int32(1),
        &_L18_hkdfChunk_1_2_3,
        &_L51[idx]);
    }
    else {
      kcg_copy_StreamChunk_slideTypes(
        &_L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1[0],
        &noname_hkdfChunk_1);
      _L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1[0][8] =
        /* @1/_L16= */(kcg_uint32) idx + kcg_lit_uint32(1);
      /* @2/IfBlock1:else:_L5=(hash::blake2s::hmacChunk#3)/ */
      hmacChunk_hash_blake2s_1(
        &_L9_hkdf_expand_it_chunk_2_hkdfChunk_1_else_IfBlock1,
        kcg_lit_int32(33),
        &_L18_hkdfChunk_1_2_3,
        &_L51[idx]);
    }
    kcg_copy_HashChunk_hash_blake2s(&noname_hkdfChunk_1, &_L51[idx]);
  }
  failed = failing || !kcg_comp_array_uint32_16_3(&_L51, &(*tc).exp);
  return failed;
}

/*
  Expanded instances for: test::ietf_RFC7693::test_hkdf_it/
  @1: (hash::blake2s::hkdfChunk#1)
  @2: @1/(hash::blake2s::hkdf_expand_it_chunk#2)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_hkdf_it_test_ietf_RFC7693.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

