/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _singleChunk_hash_blake2s_H_
#define _singleChunk_hash_blake2s_H_

#include "kcg_types.h"
#include "block_refine_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::singleChunk/ */
extern void singleChunk_hash_blake2s(
  /* @1/_L64/, @1/msg/, @2/m/, _L64/, msg/ */
  StreamChunk_slideTypes *msg,
  /* @1/_L66/, @1/len/, @2/_L5/, @2/len/, _L66/, len/ */
  size_slideTypes len,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk);

/*
  Expanded instances for: hash::blake2s::singleChunk/
  @1: (hash::blake2s::single#1)
  @2: @1/(hash::blake2s::stream_it#1)
*/

#endif /* _singleChunk_hash_blake2s_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** singleChunk_hash_blake2s.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

