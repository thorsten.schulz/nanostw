/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "block_refine_hash_sha512.h"

/* hash::sha512::block_refine/ */
void block_refine_hash_sha512(
  /* _L2/, zin/ */
  array_uint64_8 *zin,
  /* _L4/, m/ */
  array_uint32_32 *m,
  /* _L1/, zout/ */
  array_uint64_8 *zout)
{
  array_uint64_16 tmp;

  /* _L5=(slideTypes::LEtoBE64a#1)/ */ LEtoBE64a_slideTypes_16(m, &tmp);
  /* _L1=(hash::libsha::process64#1)/ */
  process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6(
    zin,
    &tmp,
    (array_uint64_16_5 *) &K_hash_sha512,
    zout);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block_refine_hash_sha512.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

