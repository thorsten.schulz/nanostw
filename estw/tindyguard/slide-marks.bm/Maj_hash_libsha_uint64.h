/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _Maj_hash_libsha_uint64_H_
#define _Maj_hash_libsha_uint64_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::Maj/ */
extern kcg_uint64 Maj_hash_libsha_uint64(
  /* _L3/, x/ */
  kcg_uint64 x_uint64,
  /* _L5/, y/ */
  kcg_uint64 y_uint64,
  /* _L7/, z/ */
  kcg_uint64 z_uint64);



#endif /* _Maj_hash_libsha_uint64_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Maj_hash_libsha_uint64.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

