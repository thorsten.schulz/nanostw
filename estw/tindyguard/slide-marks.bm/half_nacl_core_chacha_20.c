/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "half_nacl_core_chacha_20.h"

/* nacl::core::chacha::half/ */
void half_nacl_core_chacha_20(
  /* _L5/, x/ */
  array_uint32_4 *x_20,
  /* _L3/, key/ */
  Key_nacl_core *key_20,
  /* _L14/, xo/ */
  Key_nacl_core *xo_20)
{
  kcg_size idx;
  /* @1/_L95/, @17/_L11/, @17/doo/, @20/_L9/, @20/ret/ */
  kcg_uint32 _L95_dual_1;
  /* @1/_L94/, @17/_L14/, @17/coo/, @21/_L11/, @21/y/ */
  kcg_uint32 _L94_dual_1;
  /* @1/_L92/, @17/_L13/, @17/aoo/, @20/_L5/, @20/x/ */
  kcg_uint32 _L92_dual_1;
  /* @1/_L99/, @12/_L11/, @12/doo/, @15/_L9/, @15/ret/ */
  kcg_uint32 _L99_dual_1;
  /* @1/_L98/, @12/_L14/, @12/coo/, @16/_L11/, @16/y/ */
  kcg_uint32 _L98_dual_1;
  /* @1/_L96/, @12/_L13/, @12/aoo/, @15/_L5/, @15/x/ */
  kcg_uint32 _L96_dual_1;
  /* @1/_L103/, @10/_L9/, @10/ret/, @7/_L11/, @7/doo/ */
  kcg_uint32 _L103_dual_1;
  /* @1/_L102/, @11/_L11/, @11/y/, @7/_L14/, @7/coo/ */
  kcg_uint32 _L102_dual_1;
  /* @1/_L100/, @10/_L5/, @10/x/, @7/_L13/, @7/aoo/ */
  kcg_uint32 _L100_dual_1;
  /* @1/_L107/, @2/_L11/, @2/doo/, @5/_L9/, @5/ret/ */
  kcg_uint32 _L107_dual_1;
  /* @1/_L106/, @2/_L14/, @2/coo/, @6/_L11/, @6/y/ */
  kcg_uint32 _L106_dual_1;
  /* @1/_L104/, @2/_L13/, @2/aoo/, @5/_L5/, @5/x/ */
  kcg_uint32 _L104_dual_1;
  /* @1/_L91/,
     @2/_L5/,
     @2/d/,
     @22/_L11/,
     @22/doo/,
     @25/_L9/,
     @25/ret/,
     @3/_L11/,
     @3/y/ */
  kcg_uint32 _L91_dual_1;
  /* @1/_L90/, @17/_L8/, @17/c/, @22/_L14/, @22/coo/, @26/_L11/, @26/y/ */
  kcg_uint32 _L90_dual_1;
  /* @1/_L89/,
     @22/_L12/,
     @22/boo/,
     @26/_L9/,
     @26/ret/,
     @7/_L3/,
     @7/b/,
     @9/_L11/,
     @9/y/ */
  kcg_uint32 _L89_dual_1;
  /* @1/_L88/, @12/_L2/, @12/a/, @22/_L13/, @22/aoo/, @25/_L5/, @25/x/ */
  kcg_uint32 _L88_dual_1;
  /* @1/_L87/,
     @12/_L5/,
     @12/d/,
     @13/_L11/,
     @13/y/,
     @27/_L11/,
     @27/doo/,
     @30/_L9/,
     @30/ret/ */
  kcg_uint32 _L87_dual_1;
  /* @1/_L86/, @2/_L8/, @2/c/, @27/_L14/, @27/coo/, @31/_L11/, @31/y/ */
  kcg_uint32 _L86_dual_1;
  /* @1/_L85/,
     @17/_L3/,
     @17/b/,
     @19/_L11/,
     @19/y/,
     @27/_L12/,
     @27/boo/,
     @31/_L9/,
     @31/ret/ */
  kcg_uint32 _L85_dual_1;
  /* @1/_L84/, @27/_L13/, @27/aoo/, @30/_L5/, @30/x/, @7/_L2/, @7/a/ */
  kcg_uint32 _L84_dual_1;
  /* @1/_L83/,
     @32/_L11/,
     @32/doo/,
     @35/_L9/,
     @35/ret/,
     @7/_L5/,
     @7/d/,
     @8/_L11/,
     @8/y/ */
  kcg_uint32 _L83_dual_1;
  /* @1/_L82/, @12/_L8/, @12/c/, @32/_L14/, @32/coo/, @36/_L11/, @36/y/ */
  kcg_uint32 _L82_dual_1;
  /* @1/_L81/,
     @2/_L3/,
     @2/b/,
     @32/_L12/,
     @32/boo/,
     @36/_L9/,
     @36/ret/,
     @4/_L11/,
     @4/y/ */
  kcg_uint32 _L81_dual_1;
  /* @1/_L80/, @17/_L2/, @17/a/, @32/_L13/, @32/aoo/, @35/_L5/, @35/x/ */
  kcg_uint32 _L80_dual_1;
  /* @1/_L76/, @2/_L2/, @2/a/, @37/_L13/, @37/aoo/, @40/_L5/, @40/x/ */
  kcg_uint32 _L76_dual_1;
  /* @1/_L77/,
     @12/_L3/,
     @12/b/,
     @14/_L11/,
     @14/y/,
     @37/_L12/,
     @37/boo/,
     @41/_L9/,
     @41/ret/ */
  kcg_uint32 _L77_dual_1;
  /* @1/_L78/, @37/_L14/, @37/coo/, @41/_L11/, @41/y/, @7/_L8/, @7/c/ */
  kcg_uint32 _L78_dual_1;
  /* @1/_L79/,
     @17/_L5/,
     @17/d/,
     @18/_L11/,
     @18/y/,
     @37/_L11/,
     @37/doo/,
     @40/_L9/,
     @40/ret/ */
  kcg_uint32 _L79_dual_1;
  /* @3/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_8_dual_1_16;
  /* @2/_L6/, @3/_L9/, @3/ret/, @5/_L11/, @5/y/ */
  kcg_uint32 _L9_XRol32_1_QR_8_dual_1_16;
  /* @2/_L1/, @3/_L5/, @3/x/ */
  kcg_uint32 _L5_XRol32_1_QR_8_dual_1_16;
  /* @4/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_8_dual_1_12;
  /* @2/_L10/, @4/_L9/, @4/ret/, @6/_L5/, @6/x/ */
  kcg_uint32 _L9_XRol32_2_QR_8_dual_1_12;
  /* @2/_L7/, @4/_L5/, @4/x/ */
  kcg_uint32 _L5_XRol32_2_QR_8_dual_1_12;
  /* @5/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_8_dual_1_8;
  /* @6/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_8_dual_1_7;
  /* @8/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_7_dual_1_16;
  /* @10/_L11/, @10/y/, @7/_L6/, @8/_L9/, @8/ret/ */
  kcg_uint32 _L9_XRol32_1_QR_7_dual_1_16;
  /* @7/_L1/, @8/_L5/, @8/x/ */
  kcg_uint32 _L5_XRol32_1_QR_7_dual_1_16;
  /* @9/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_7_dual_1_12;
  /* @11/_L5/, @11/x/, @7/_L10/, @9/_L9/, @9/ret/ */
  kcg_uint32 _L9_XRol32_2_QR_7_dual_1_12;
  /* @7/_L7/, @9/_L5/, @9/x/ */
  kcg_uint32 _L5_XRol32_2_QR_7_dual_1_12;
  /* @10/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_7_dual_1_8;
  /* @11/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_7_dual_1_7;
  /* @13/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_6_dual_1_16;
  /* @12/_L6/, @13/_L9/, @13/ret/, @15/_L11/, @15/y/ */
  kcg_uint32 _L9_XRol32_1_QR_6_dual_1_16;
  /* @12/_L1/, @13/_L5/, @13/x/ */
  kcg_uint32 _L5_XRol32_1_QR_6_dual_1_16;
  /* @14/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_6_dual_1_12;
  /* @12/_L10/, @14/_L9/, @14/ret/, @16/_L5/, @16/x/ */
  kcg_uint32 _L9_XRol32_2_QR_6_dual_1_12;
  /* @12/_L7/, @14/_L5/, @14/x/ */
  kcg_uint32 _L5_XRol32_2_QR_6_dual_1_12;
  /* @15/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_6_dual_1_8;
  /* @16/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_6_dual_1_7;
  /* @18/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_5_dual_1_16;
  /* @17/_L6/, @18/_L9/, @18/ret/, @20/_L11/, @20/y/ */
  kcg_uint32 _L9_XRol32_1_QR_5_dual_1_16;
  /* @17/_L1/, @18/_L5/, @18/x/ */
  kcg_uint32 _L5_XRol32_1_QR_5_dual_1_16;
  /* @19/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_5_dual_1_12;
  /* @17/_L10/, @19/_L9/, @19/ret/, @21/_L5/, @21/x/ */
  kcg_uint32 _L9_XRol32_2_QR_5_dual_1_12;
  /* @17/_L7/, @19/_L5/, @19/x/ */
  kcg_uint32 _L5_XRol32_2_QR_5_dual_1_12;
  /* @20/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_5_dual_1_8;
  /* @21/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_5_dual_1_7;
  /* @23/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_4_dual_1_16;
  /* @22/_L6/, @23/_L9/, @23/ret/, @25/_L11/, @25/y/ */
  kcg_uint32 _L9_XRol32_1_QR_4_dual_1_16;
  /* @22/_L1/, @23/_L5/, @23/x/ */
  kcg_uint32 _L5_XRol32_1_QR_4_dual_1_16;
  /* @24/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_4_dual_1_12;
  /* @22/_L10/, @24/_L9/, @24/ret/, @26/_L5/, @26/x/ */
  kcg_uint32 _L9_XRol32_2_QR_4_dual_1_12;
  /* @22/_L7/, @24/_L5/, @24/x/ */
  kcg_uint32 _L5_XRol32_2_QR_4_dual_1_12;
  /* @25/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_4_dual_1_8;
  /* @26/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_4_dual_1_7;
  /* @28/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_3_dual_1_16;
  /* @27/_L6/, @28/_L9/, @28/ret/, @30/_L11/, @30/y/ */
  kcg_uint32 _L9_XRol32_1_QR_3_dual_1_16;
  /* @27/_L1/, @28/_L5/, @28/x/ */
  kcg_uint32 _L5_XRol32_1_QR_3_dual_1_16;
  /* @29/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_3_dual_1_12;
  /* @27/_L10/, @29/_L9/, @29/ret/, @31/_L5/, @31/x/ */
  kcg_uint32 _L9_XRol32_2_QR_3_dual_1_12;
  /* @27/_L7/, @29/_L5/, @29/x/ */
  kcg_uint32 _L5_XRol32_2_QR_3_dual_1_12;
  /* @30/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_3_dual_1_8;
  /* @31/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_3_dual_1_7;
  /* @33/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_2_dual_1_16;
  /* @32/_L6/, @33/_L9/, @33/ret/, @35/_L11/, @35/y/ */
  kcg_uint32 _L9_XRol32_1_QR_2_dual_1_16;
  /* @32/_L1/, @33/_L5/, @33/x/ */
  kcg_uint32 _L5_XRol32_1_QR_2_dual_1_16;
  /* @34/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_2_dual_1_12;
  /* @32/_L10/, @34/_L9/, @34/ret/, @36/_L5/, @36/x/ */
  kcg_uint32 _L9_XRol32_2_QR_2_dual_1_12;
  /* @32/_L7/, @34/_L5/, @34/x/ */
  kcg_uint32 _L5_XRol32_2_QR_2_dual_1_12;
  /* @35/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_2_dual_1_8;
  /* @36/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_2_dual_1_7;
  /* @38/_L10/ */
  kcg_uint32 _L10_XRol32_1_QR_1_dual_1_16;
  /* @37/_L6/, @38/_L9/, @38/ret/, @40/_L11/, @40/y/ */
  kcg_uint32 _L9_XRol32_1_QR_1_dual_1_16;
  /* @37/_L1/, @38/_L5/, @38/x/ */
  kcg_uint32 _L5_XRol32_1_QR_1_dual_1_16;
  /* @39/_L10/ */
  kcg_uint32 _L10_XRol32_2_QR_1_dual_1_12;
  /* @37/_L10/, @39/_L9/, @39/ret/, @41/_L5/, @41/x/ */
  kcg_uint32 _L9_XRol32_2_QR_1_dual_1_12;
  /* @37/_L7/, @39/_L5/, @39/x/ */
  kcg_uint32 _L5_XRol32_2_QR_1_dual_1_12;
  /* @40/_L10/ */
  kcg_uint32 _L10_XRol32_3_QR_1_dual_1_8;
  /* @41/_L10/ */
  kcg_uint32 _L10_XRol32_4_QR_1_dual_1_7;
  /* _L10/ */
  array_uint32_16 _L10_20;

  kcg_copy_array_uint32_4(&_L10_20[0], (array_uint32_4 *) &sigma_nacl_core);
  kcg_copy_Key_nacl_core(&_L10_20[4], key_20);
  kcg_copy_array_uint32_4(&_L10_20[12], x_20);
  /* _L10= */
  for (idx = 0; idx < 10; idx++) {
    _L5_XRol32_1_QR_1_dual_1_16 = _L10_20[0] + _L10_20[4];
    _L10_XRol32_1_QR_1_dual_1_16 = _L5_XRol32_1_QR_1_dual_1_16 ^ _L10_20[12];
    _L9_XRol32_1_QR_1_dual_1_16 = (_L10_XRol32_1_QR_1_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_1_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_1_dual_1_12 = _L10_20[8] + _L9_XRol32_1_QR_1_dual_1_16;
    _L10_XRol32_2_QR_1_dual_1_12 = _L5_XRol32_2_QR_1_dual_1_12 ^ _L10_20[4];
    _L9_XRol32_2_QR_1_dual_1_12 = (_L10_XRol32_2_QR_1_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_1_dual_1_12,
        kcg_lit_uint32(12));
    _L76_dual_1 = _L5_XRol32_1_QR_1_dual_1_16 + _L9_XRol32_2_QR_1_dual_1_12;
    _L10_XRol32_3_QR_1_dual_1_8 = _L76_dual_1 ^ _L9_XRol32_1_QR_1_dual_1_16;
    _L79_dual_1 = (_L10_XRol32_3_QR_1_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_1_dual_1_8, kcg_lit_uint32(8));
    _L78_dual_1 = _L5_XRol32_2_QR_1_dual_1_12 + _L79_dual_1;
    _L10_XRol32_4_QR_1_dual_1_7 = _L9_XRol32_2_QR_1_dual_1_12 ^ _L78_dual_1;
    _L77_dual_1 = (_L10_XRol32_4_QR_1_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_1_dual_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_2_dual_1_16 = _L10_20[1] + _L10_20[5];
    _L10_XRol32_1_QR_2_dual_1_16 = _L5_XRol32_1_QR_2_dual_1_16 ^ _L10_20[13];
    _L9_XRol32_1_QR_2_dual_1_16 = (_L10_XRol32_1_QR_2_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_2_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_2_dual_1_12 = _L10_20[9] + _L9_XRol32_1_QR_2_dual_1_16;
    _L10_XRol32_2_QR_2_dual_1_12 = _L5_XRol32_2_QR_2_dual_1_12 ^ _L10_20[5];
    _L9_XRol32_2_QR_2_dual_1_12 = (_L10_XRol32_2_QR_2_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_2_dual_1_12,
        kcg_lit_uint32(12));
    _L80_dual_1 = _L5_XRol32_1_QR_2_dual_1_16 + _L9_XRol32_2_QR_2_dual_1_12;
    _L10_XRol32_3_QR_2_dual_1_8 = _L80_dual_1 ^ _L9_XRol32_1_QR_2_dual_1_16;
    _L83_dual_1 = (_L10_XRol32_3_QR_2_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_2_dual_1_8, kcg_lit_uint32(8));
    _L82_dual_1 = _L5_XRol32_2_QR_2_dual_1_12 + _L83_dual_1;
    _L10_XRol32_4_QR_2_dual_1_7 = _L9_XRol32_2_QR_2_dual_1_12 ^ _L82_dual_1;
    _L81_dual_1 = (_L10_XRol32_4_QR_2_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_2_dual_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_3_dual_1_16 = _L10_20[2] + _L10_20[6];
    _L10_XRol32_1_QR_3_dual_1_16 = _L5_XRol32_1_QR_3_dual_1_16 ^ _L10_20[14];
    _L9_XRol32_1_QR_3_dual_1_16 = (_L10_XRol32_1_QR_3_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_3_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_3_dual_1_12 = _L10_20[10] + _L9_XRol32_1_QR_3_dual_1_16;
    _L10_XRol32_2_QR_3_dual_1_12 = _L5_XRol32_2_QR_3_dual_1_12 ^ _L10_20[6];
    _L9_XRol32_2_QR_3_dual_1_12 = (_L10_XRol32_2_QR_3_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_3_dual_1_12,
        kcg_lit_uint32(12));
    _L84_dual_1 = _L5_XRol32_1_QR_3_dual_1_16 + _L9_XRol32_2_QR_3_dual_1_12;
    _L10_XRol32_3_QR_3_dual_1_8 = _L84_dual_1 ^ _L9_XRol32_1_QR_3_dual_1_16;
    _L87_dual_1 = (_L10_XRol32_3_QR_3_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_3_dual_1_8, kcg_lit_uint32(8));
    _L86_dual_1 = _L5_XRol32_2_QR_3_dual_1_12 + _L87_dual_1;
    _L10_XRol32_4_QR_3_dual_1_7 = _L9_XRol32_2_QR_3_dual_1_12 ^ _L86_dual_1;
    _L85_dual_1 = (_L10_XRol32_4_QR_3_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_3_dual_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_4_dual_1_16 = _L10_20[3] + _L10_20[7];
    _L10_XRol32_1_QR_4_dual_1_16 = _L5_XRol32_1_QR_4_dual_1_16 ^ _L10_20[15];
    _L9_XRol32_1_QR_4_dual_1_16 = (_L10_XRol32_1_QR_4_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_4_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_4_dual_1_12 = _L10_20[11] + _L9_XRol32_1_QR_4_dual_1_16;
    _L10_XRol32_2_QR_4_dual_1_12 = _L5_XRol32_2_QR_4_dual_1_12 ^ _L10_20[7];
    _L9_XRol32_2_QR_4_dual_1_12 = (_L10_XRol32_2_QR_4_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_4_dual_1_12,
        kcg_lit_uint32(12));
    _L88_dual_1 = _L5_XRol32_1_QR_4_dual_1_16 + _L9_XRol32_2_QR_4_dual_1_12;
    _L10_XRol32_3_QR_4_dual_1_8 = _L88_dual_1 ^ _L9_XRol32_1_QR_4_dual_1_16;
    _L91_dual_1 = (_L10_XRol32_3_QR_4_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_4_dual_1_8, kcg_lit_uint32(8));
    _L90_dual_1 = _L5_XRol32_2_QR_4_dual_1_12 + _L91_dual_1;
    _L10_XRol32_4_QR_4_dual_1_7 = _L9_XRol32_2_QR_4_dual_1_12 ^ _L90_dual_1;
    _L89_dual_1 = (_L10_XRol32_4_QR_4_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_4_dual_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_5_dual_1_16 = _L80_dual_1 + _L85_dual_1;
    _L10_XRol32_1_QR_5_dual_1_16 = _L5_XRol32_1_QR_5_dual_1_16 ^ _L79_dual_1;
    _L9_XRol32_1_QR_5_dual_1_16 = (_L10_XRol32_1_QR_5_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_5_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_5_dual_1_12 = _L90_dual_1 + _L9_XRol32_1_QR_5_dual_1_16;
    _L10_XRol32_2_QR_5_dual_1_12 = _L5_XRol32_2_QR_5_dual_1_12 ^ _L85_dual_1;
    _L9_XRol32_2_QR_5_dual_1_12 = (_L10_XRol32_2_QR_5_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_5_dual_1_12,
        kcg_lit_uint32(12));
    _L92_dual_1 = _L5_XRol32_1_QR_5_dual_1_16 + _L9_XRol32_2_QR_5_dual_1_12;
    _L10_20[1] = _L92_dual_1;
    _L10_XRol32_3_QR_5_dual_1_8 = _L92_dual_1 ^ _L9_XRol32_1_QR_5_dual_1_16;
    _L95_dual_1 = (_L10_XRol32_3_QR_5_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_5_dual_1_8, kcg_lit_uint32(8));
    _L10_20[12] = _L95_dual_1;
    _L94_dual_1 = _L5_XRol32_2_QR_5_dual_1_12 + _L95_dual_1;
    _L10_20[11] = _L94_dual_1;
    _L10_XRol32_4_QR_5_dual_1_7 = _L9_XRol32_2_QR_5_dual_1_12 ^ _L94_dual_1;
    _L10_20[6] = (_L10_XRol32_4_QR_5_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_5_dual_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_6_dual_1_16 = _L88_dual_1 + _L77_dual_1;
    _L10_XRol32_1_QR_6_dual_1_16 = _L5_XRol32_1_QR_6_dual_1_16 ^ _L87_dual_1;
    _L9_XRol32_1_QR_6_dual_1_16 = (_L10_XRol32_1_QR_6_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_6_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_6_dual_1_12 = _L82_dual_1 + _L9_XRol32_1_QR_6_dual_1_16;
    _L10_XRol32_2_QR_6_dual_1_12 = _L5_XRol32_2_QR_6_dual_1_12 ^ _L77_dual_1;
    _L9_XRol32_2_QR_6_dual_1_12 = (_L10_XRol32_2_QR_6_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_6_dual_1_12,
        kcg_lit_uint32(12));
    _L96_dual_1 = _L5_XRol32_1_QR_6_dual_1_16 + _L9_XRol32_2_QR_6_dual_1_12;
    _L10_20[3] = _L96_dual_1;
    _L10_XRol32_3_QR_6_dual_1_8 = _L96_dual_1 ^ _L9_XRol32_1_QR_6_dual_1_16;
    _L99_dual_1 = (_L10_XRol32_3_QR_6_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_6_dual_1_8, kcg_lit_uint32(8));
    _L10_20[14] = _L99_dual_1;
    _L98_dual_1 = _L5_XRol32_2_QR_6_dual_1_12 + _L99_dual_1;
    _L10_20[9] = _L98_dual_1;
    _L10_XRol32_4_QR_6_dual_1_7 = _L9_XRol32_2_QR_6_dual_1_12 ^ _L98_dual_1;
    _L10_20[4] = (_L10_XRol32_4_QR_6_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_6_dual_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_7_dual_1_16 = _L84_dual_1 + _L89_dual_1;
    _L10_XRol32_1_QR_7_dual_1_16 = _L5_XRol32_1_QR_7_dual_1_16 ^ _L83_dual_1;
    _L9_XRol32_1_QR_7_dual_1_16 = (_L10_XRol32_1_QR_7_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_7_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_7_dual_1_12 = _L78_dual_1 + _L9_XRol32_1_QR_7_dual_1_16;
    _L10_XRol32_2_QR_7_dual_1_12 = _L5_XRol32_2_QR_7_dual_1_12 ^ _L89_dual_1;
    _L9_XRol32_2_QR_7_dual_1_12 = (_L10_XRol32_2_QR_7_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_7_dual_1_12,
        kcg_lit_uint32(12));
    _L100_dual_1 = _L5_XRol32_1_QR_7_dual_1_16 + _L9_XRol32_2_QR_7_dual_1_12;
    _L10_20[2] = _L100_dual_1;
    _L10_XRol32_3_QR_7_dual_1_8 = _L100_dual_1 ^ _L9_XRol32_1_QR_7_dual_1_16;
    _L103_dual_1 = (_L10_XRol32_3_QR_7_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_7_dual_1_8, kcg_lit_uint32(8));
    _L10_20[13] = _L103_dual_1;
    _L102_dual_1 = _L5_XRol32_2_QR_7_dual_1_12 + _L103_dual_1;
    _L10_20[8] = _L102_dual_1;
    _L10_XRol32_4_QR_7_dual_1_7 = _L9_XRol32_2_QR_7_dual_1_12 ^ _L102_dual_1;
    _L10_20[7] = (_L10_XRol32_4_QR_7_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_7_dual_1_7, kcg_lit_uint32(7));
    _L5_XRol32_1_QR_8_dual_1_16 = _L76_dual_1 + _L81_dual_1;
    _L10_XRol32_1_QR_8_dual_1_16 = _L5_XRol32_1_QR_8_dual_1_16 ^ _L91_dual_1;
    _L9_XRol32_1_QR_8_dual_1_16 = (_L10_XRol32_1_QR_8_dual_1_16 >>
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        _L10_XRol32_1_QR_8_dual_1_16,
        kcg_lit_uint32(16));
    _L5_XRol32_2_QR_8_dual_1_12 = _L86_dual_1 + _L9_XRol32_1_QR_8_dual_1_16;
    _L10_XRol32_2_QR_8_dual_1_12 = _L5_XRol32_2_QR_8_dual_1_12 ^ _L81_dual_1;
    _L9_XRol32_2_QR_8_dual_1_12 = (_L10_XRol32_2_QR_8_dual_1_12 >>
        kcg_lit_uint32(20)) | kcg_lsl_uint32(
        _L10_XRol32_2_QR_8_dual_1_12,
        kcg_lit_uint32(12));
    _L104_dual_1 = _L5_XRol32_1_QR_8_dual_1_16 + _L9_XRol32_2_QR_8_dual_1_12;
    _L10_20[0] = _L104_dual_1;
    _L10_XRol32_3_QR_8_dual_1_8 = _L104_dual_1 ^ _L9_XRol32_1_QR_8_dual_1_16;
    _L107_dual_1 = (_L10_XRol32_3_QR_8_dual_1_8 >> kcg_lit_uint32(24)) |
      kcg_lsl_uint32(_L10_XRol32_3_QR_8_dual_1_8, kcg_lit_uint32(8));
    _L10_20[15] = _L107_dual_1;
    _L106_dual_1 = _L5_XRol32_2_QR_8_dual_1_12 + _L107_dual_1;
    _L10_20[10] = _L106_dual_1;
    _L10_XRol32_4_QR_8_dual_1_7 = _L9_XRol32_2_QR_8_dual_1_12 ^ _L106_dual_1;
    _L10_20[5] = (_L10_XRol32_4_QR_8_dual_1_7 >> kcg_lit_uint32(25)) |
      kcg_lsl_uint32(_L10_XRol32_4_QR_8_dual_1_7, kcg_lit_uint32(7));
  }
  kcg_copy_array_uint32_4(&(*xo_20)[0], (array_uint32_4 *) &_L10_20[0]);
  kcg_copy_array_uint32_4(&(*xo_20)[4], (array_uint32_4 *) &_L10_20[12]);
}

/*
  Expanded instances for: nacl::core::chacha::half/
  @1: (nacl::core::chacha::dual#1)
  @2: @1/(nacl::core::chacha::QR#8)
  @3: @2/(nacl::op::XRol32#1)
  @4: @2/(nacl::op::XRol32#2)
  @5: @2/(nacl::op::XRol32#3)
  @6: @2/(nacl::op::XRol32#4)
  @7: @1/(nacl::core::chacha::QR#7)
  @8: @7/(nacl::op::XRol32#1)
  @9: @7/(nacl::op::XRol32#2)
  @10: @7/(nacl::op::XRol32#3)
  @11: @7/(nacl::op::XRol32#4)
  @12: @1/(nacl::core::chacha::QR#6)
  @13: @12/(nacl::op::XRol32#1)
  @14: @12/(nacl::op::XRol32#2)
  @15: @12/(nacl::op::XRol32#3)
  @16: @12/(nacl::op::XRol32#4)
  @17: @1/(nacl::core::chacha::QR#5)
  @18: @17/(nacl::op::XRol32#1)
  @19: @17/(nacl::op::XRol32#2)
  @20: @17/(nacl::op::XRol32#3)
  @21: @17/(nacl::op::XRol32#4)
  @22: @1/(nacl::core::chacha::QR#4)
  @23: @22/(nacl::op::XRol32#1)
  @24: @22/(nacl::op::XRol32#2)
  @25: @22/(nacl::op::XRol32#3)
  @26: @22/(nacl::op::XRol32#4)
  @27: @1/(nacl::core::chacha::QR#3)
  @28: @27/(nacl::op::XRol32#1)
  @29: @27/(nacl::op::XRol32#2)
  @30: @27/(nacl::op::XRol32#3)
  @31: @27/(nacl::op::XRol32#4)
  @32: @1/(nacl::core::chacha::QR#2)
  @33: @32/(nacl::op::XRol32#1)
  @34: @32/(nacl::op::XRol32#2)
  @35: @32/(nacl::op::XRol32#3)
  @36: @32/(nacl::op::XRol32#4)
  @37: @1/(nacl::core::chacha::QR#1)
  @38: @37/(nacl::op::XRol32#1)
  @39: @37/(nacl::op::XRol32#2)
  @40: @37/(nacl::op::XRol32#3)
  @41: @37/(nacl::op::XRol32#4)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** half_nacl_core_chacha_20.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

