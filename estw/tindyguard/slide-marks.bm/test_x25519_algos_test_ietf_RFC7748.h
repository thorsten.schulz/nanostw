/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _test_x25519_algos_test_ietf_RFC7748_H_
#define _test_x25519_algos_test_ietf_RFC7748_H_

#include "kcg_types.h"
#include "bench_x25519_test_ietf_RFC7748_20.h"
#include "bench_x25519_test_ietf_RFC7748_21.h"
#include "bench_x25519_test_ietf_RFC7748_19.h"
#include "bench_x25519_test_ietf_RFC7748_8.h"
#include "bench_x25519_test_ietf_RFC7748_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_bench_x25519_test_ietf_RFC7748_20 /* _L5=(test::ietf_RFC7748::bench_x25519#5)/ */ Context_bench_x25519_5;
  outC_bench_x25519_test_ietf_RFC7748_21 /* _L4=(test::ietf_RFC7748::bench_x25519#4)/ */ Context_bench_x25519_4;
  outC_bench_x25519_test_ietf_RFC7748_19 /* _L3=(test::ietf_RFC7748::bench_x25519#3)/ */ Context_bench_x25519_3;
  outC_bench_x25519_test_ietf_RFC7748_8 /* _L2=(test::ietf_RFC7748::bench_x25519#2)/ */ Context_bench_x25519_2;
  outC_bench_x25519_test_ietf_RFC7748_1 /* _L1=(test::ietf_RFC7748::bench_x25519#1)/ */ Context_bench_x25519_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_test_x25519_algos_test_ietf_RFC7748;

/* ===========  node initialization and cycle functions  =========== */
/* test::ietf_RFC7748::test_x25519_algos/ */
extern void test_x25519_algos_test_ietf_RFC7748(
  /* _L6/, failed/ */
  kcg_bool *failed,
  outC_test_x25519_algos_test_ietf_RFC7748 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void test_x25519_algos_reset_test_ietf_RFC7748(
  outC_test_x25519_algos_test_ietf_RFC7748 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void test_x25519_algos_init_test_ietf_RFC7748(
  outC_test_x25519_algos_test_ietf_RFC7748 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _test_x25519_algos_test_ietf_RFC7748_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_x25519_algos_test_ietf_RFC7748.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

