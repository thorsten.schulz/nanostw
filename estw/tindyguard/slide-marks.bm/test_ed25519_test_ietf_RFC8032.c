/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_ed25519_test_ietf_RFC8032.h"

/* test::ietf_RFC8032::test_ed25519/ */
kcg_bool test_ed25519_test_ietf_RFC8032(
  /* _L17/, failing/ */
  kcg_bool failing,
  /* tc/ */
  testcase_test_ietf_RFC8032 *tc)
{
  KeyPair32_slideTypes tmp_str;
  Signature_nacl_sign tmp;
  /* _L50/ */
  array_uint32_16_2 _L50;
  /* _L4/, failed/ */
  kcg_bool failed;

  kcg_copy_StreamChunk_slideTypes(&_L50[0], &(*tc).m[0]);
  kcg_copy_StreamChunk_slideTypes(
    &_L50[1],
    (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  kcg_copy_array_uint32_8(&tmp_str.sk_x, (array_uint32_8 *) &(*tc).key[0]);
  kcg_copy_array_uint32_8(&tmp_str.pk_y, (array_uint32_8 *) &(*tc).key[8]);
  /* _L1=(nacl::sign::sign#1)/ */
  sign_nacl_sign_2(&_L50, (*tc).mlen, &tmp_str, &tmp);
  failed = !kcg_comp_Signature_nacl_sign(&tmp, &(*tc).signat) ||
    /* _L2=(nacl::sign::verify#1)/ */
    verify_nacl_sign_2(
      &_L50,
      (*tc).mlen,
      (Key32_slideTypes *) &(*tc).key[8],
      &(*tc).signat) || failing;
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_ed25519_test_ietf_RFC8032.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

