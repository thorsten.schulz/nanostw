/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pullAll_test.h"

/* test::pullAll/ */
void pullAll_test(
  /* _L3/, failed/ */
  kcg_bool *failed,
  /* _L2/, failed2/ */
  kcg_bool *failed2,
  outC_pullAll_test *outC)
{
  *failed = /* _L1=(test::ietf_RFC8032::tests#1)/ */
    tests_test_ietf_RFC8032() || /* _L4=(test::ietf_RFC7693::test#1)/ */
    test_test_ietf_RFC7693() || /* _L5=(test::ietf_RFC6234::test#1)/ */
    test_test_ietf_RFC6234() || /* _L6=(test::ietf_RFC7539::bench#1)/ */
    bench_test_ietf_RFC7539();
  /* _L2=(test::ietf_RFC7748::test_x25519_algos#1)/ */
  test_x25519_algos_test_ietf_RFC7748(
    failed2,
    &outC->Context_test_x25519_algos_1);
}

#ifndef KCG_USER_DEFINED_INIT
void pullAll_init_test(outC_pullAll_test *outC)
{
  /* _L2=(test::ietf_RFC7748::test_x25519_algos#1)/ */
  test_x25519_algos_init_test_ietf_RFC7748(&outC->Context_test_x25519_algos_1);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void pullAll_reset_test(outC_pullAll_test *outC)
{
  /* _L2=(test::ietf_RFC7748::test_x25519_algos#1)/ */
  test_x25519_algos_reset_test_ietf_RFC7748(&outC->Context_test_x25519_algos_1);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pullAll_test.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

