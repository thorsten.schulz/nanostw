/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "block32_it3_hash_libsha_7_18_3_17_19_10.h"

/* hash::libsha::block32_it3/ */
void block32_it3_hash_libsha_7_18_3_17_19_10(
  /* _L6/, i/ */
  size_slideTypes i_7_18_3_17_19_10,
  /* _L1/, w/ */
  array_uint32_16 *w_7_18_3_17_19_10,
  /* _L2/, wo/ */
  array_uint32_16 *wo_7_18_3_17_19_10)
{
  kcg_uint32 tmp;
  kcg_uint32 tmp1;
  kcg_uint32 tmp2;
  kcg_uint32 tmp3;
  /* _L5/ */
  kcg_int32 _L5_7_18_3_17_19_10;
  /* _L16/ */
  kcg_int32 _L16_7_18_3_17_19_10;
  /* _L20/ */
  kcg_int32 _L20_7_18_3_17_19_10;

  _L20_7_18_3_17_19_10 = (i_7_18_3_17_19_10 + kcg_lit_int32(14)) %
    kcg_lit_int32(16);
  _L16_7_18_3_17_19_10 = (i_7_18_3_17_19_10 + kcg_lit_int32(1)) % kcg_lit_int32(
      16);
  _L5_7_18_3_17_19_10 = (i_7_18_3_17_19_10 + kcg_lit_int32(9)) % kcg_lit_int32(
      16);
  kcg_copy_array_uint32_16(wo_7_18_3_17_19_10, w_7_18_3_17_19_10);
  if (kcg_lit_int32(0) <= _L5_7_18_3_17_19_10 && _L5_7_18_3_17_19_10 <
    kcg_lit_int32(16)) {
    tmp = (*w_7_18_3_17_19_10)[_L5_7_18_3_17_19_10];
  }
  else {
    tmp = kcg_lit_uint32(0);
  }
  if (kcg_lit_int32(0) <= _L16_7_18_3_17_19_10 && _L16_7_18_3_17_19_10 <
    kcg_lit_int32(16)) {
    tmp3 = (*w_7_18_3_17_19_10)[_L16_7_18_3_17_19_10];
  }
  else {
    tmp3 = kcg_lit_uint32(0);
  }
  tmp1 = /* _L17=(hash::libsha::sigma32#2)/ */ sigma32_hash_libsha_7_18_3(tmp3);
  if (kcg_lit_int32(0) <= _L20_7_18_3_17_19_10 && _L20_7_18_3_17_19_10 <
    kcg_lit_int32(16)) {
    tmp3 = (*w_7_18_3_17_19_10)[_L20_7_18_3_17_19_10];
  }
  else {
    tmp3 = kcg_lit_uint32(0);
  }
  tmp2 = /* _L23=(hash::libsha::sigma32#3)/ */ sigma32_hash_libsha_17_19_10(tmp3);
  if (kcg_lit_int32(0) <= i_7_18_3_17_19_10 && i_7_18_3_17_19_10 <
    kcg_lit_int32(16)) {
    (*wo_7_18_3_17_19_10)[i_7_18_3_17_19_10] =
      (*w_7_18_3_17_19_10)[i_7_18_3_17_19_10] + tmp + tmp1 + tmp2;
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block32_it3_hash_libsha_7_18_3_17_19_10.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

