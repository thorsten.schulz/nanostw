/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _test_test_ietf_RFC6234_H_
#define _test_test_ietf_RFC6234_H_

#include "kcg_types.h"
#include "bench_HKDF256_test_ietf_RFC6234.h"
#include "bench_hash512_test_ietf_RFC6234_256.h"
#include "bench_hash512_test_ietf_RFC6234_16.h"
#include "bench_hash512_test_ietf_RFC6234_64.h"
#include "bench_hash256_test_ietf_RFC6234_256.h"
#include "bench_hash256_test_ietf_RFC6234_64.h"
#include "bench_hash256_test_ietf_RFC6234_16.h"
#include "test_HKDF256_it_test_ietf_RFC6234.h"
#include "test_HMAC256_it_test_ietf_RFC6234.h"
#include "test_SHA256_it_test_ietf_RFC6234.h"
#include "test_HMAC512_it_test_ietf_RFC6234.h"
#include "test_SHA512_it_test_ietf_RFC6234.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC6234::test/ */
extern kcg_bool test_test_ietf_RFC6234(void);



#endif /* _test_test_ietf_RFC6234_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_test_ietf_RFC6234.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

