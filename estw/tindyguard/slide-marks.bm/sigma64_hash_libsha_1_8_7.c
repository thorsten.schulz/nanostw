/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sigma64_hash_libsha_1_8_7.h"

/* hash::libsha::sigma64/ */
kcg_uint64 sigma64_hash_libsha_1_8_7(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, _L1/, x/ */
  kcg_uint64 x_1_8_7)
{
  /* _L6/, ret/ */
  kcg_uint64 ret_1_8_7;

  ret_1_8_7 = ((x_1_8_7 >> kcg_lit_uint64(1)) | kcg_lsl_uint64(
        x_1_8_7,
        kcg_lit_uint64(63))) ^ ((x_1_8_7 >> kcg_lit_uint64(8)) | kcg_lsl_uint64(
        x_1_8_7,
        kcg_lit_uint64(56))) ^ (x_1_8_7 >> kcg_lit_uint64(7));
  return ret_1_8_7;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sigma64_hash_libsha_1_8_7.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

