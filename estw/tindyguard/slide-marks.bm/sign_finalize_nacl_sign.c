/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_finalize_nacl_sign.h"

/* nacl::sign::sign_finalize/ */
void sign_finalize_nacl_sign(
  /* _L31/, lastPart/ */
  StreamChunk_slideTypes *lastPart,
  /* _L28/, signin/ */
  prependAkku_nacl_sign *signin,
  /* _L37/, r/ */
  array_uint32_8 *r,
  /* _L33/, signature/ */
  Signature_nacl_sign *signature)
{
  StreamChunk_slideTypes tmp;
  kcg_size idx;
  /* @2/_L15/, @4/_L15/ */
  kcg_uint32 _L15_st32_1_st32x8_1;
  /* @2/_L14/, @4/_L14/ */
  kcg_uint32 _L14_st32_1_st32x8_1;
  /* @5/_L15/ */
  kcg_uint32 _L15_st32_casti64_1;
  /* @5/_L14/ */
  kcg_uint32 _L14_st32_casti64_1;
  array_uint32_8 tmp1;
  array_int64_64 tmp2;
  array_int64_64 acc;
  /* @1/_L6/ */
  i64416_nacl_sign _L6_reduce_1;
  /* @3/_L18/ */
  u848_slideTypes _L18_st32x8_1;
  /* _L36/ */
  i6448_nacl_sign _L36;
  /* @3/_L19/, @3/m/, _L77/ */
  array_uint8_32 _L77;

  /* _L29=(hash::sha512::finalize#1)/ */
  finalize_hash_sha512(
    &(*signin).accu,
    lastPart,
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &tmp);
  /* @1/_L6= */
  for (idx = 0; idx < 16; idx++) {
    _L6_reduce_1[idx][0] = /* @2/_L17= */(kcg_int64)
        /* @2/_L8= */(kcg_uint8) tmp[idx];
    _L14_st32_1_st32x8_1 = tmp[idx] >> kcg_lit_uint32(8);
    _L6_reduce_1[idx][1] = /* @2/_L18= */(kcg_int64)
        /* @2/_L7= */(kcg_uint8) _L14_st32_1_st32x8_1;
    _L15_st32_1_st32x8_1 = _L14_st32_1_st32x8_1 >> kcg_lit_uint32(8);
    _L6_reduce_1[idx][2] = /* @2/_L19= */(kcg_int64)
        /* @2/_L6= */(kcg_uint8) _L15_st32_1_st32x8_1;
    _L6_reduce_1[idx][3] = /* @2/_L20= */(kcg_int64)
        /* @2/_L5= */(kcg_uint8) (_L15_st32_1_st32x8_1 >> kcg_lit_uint32(8));
  }
  /* @3/_L18=, _L36= */
  for (idx = 0; idx < 8; idx++) {
    _L18_st32x8_1[idx][0] = /* @4/_L8= */(kcg_uint8) (*signin).d[idx];
    _L14_st32_1_st32x8_1 = (*signin).d[idx] >> kcg_lit_uint32(8);
    _L18_st32x8_1[idx][1] = /* @4/_L7= */(kcg_uint8) _L14_st32_1_st32x8_1;
    _L15_st32_1_st32x8_1 = _L14_st32_1_st32x8_1 >> kcg_lit_uint32(8);
    _L18_st32x8_1[idx][2] = /* @4/_L6= */(kcg_uint8) _L15_st32_1_st32x8_1;
    _L18_st32x8_1[idx][3] = /* @4/_L5= */(kcg_uint8)
        (_L15_st32_1_st32x8_1 >> kcg_lit_uint32(8));
    _L36[idx][0] = /* @5/_L17= */(kcg_int64) /* @5/_L8= */(kcg_uint8) (*r)[idx];
    _L14_st32_casti64_1 = (*r)[idx] >> kcg_lit_uint32(8);
    _L36[idx][1] = /* @5/_L18= */(kcg_int64)
        /* @5/_L7= */(kcg_uint8) _L14_st32_casti64_1;
    _L15_st32_casti64_1 = _L14_st32_casti64_1 >> kcg_lit_uint32(8);
    _L36[idx][2] = /* @5/_L19= */(kcg_int64)
        /* @5/_L6= */(kcg_uint8) _L15_st32_casti64_1;
    _L36[idx][3] = /* @5/_L20= */(kcg_int64)
        /* @5/_L5= */(kcg_uint8) (_L15_st32_casti64_1 >> kcg_lit_uint32(8));
  }
  kcg_copy_array_uint8_4(&_L77[0], &_L18_st32x8_1[0]);
  kcg_copy_array_uint8_4(&_L77[4], &_L18_st32x8_1[1]);
  kcg_copy_array_uint8_4(&_L77[8], &_L18_st32x8_1[2]);
  kcg_copy_array_uint8_4(&_L77[12], &_L18_st32x8_1[3]);
  kcg_copy_array_uint8_4(&_L77[16], &_L18_st32x8_1[4]);
  kcg_copy_array_uint8_4(&_L77[20], &_L18_st32x8_1[5]);
  kcg_copy_array_uint8_4(&_L77[24], &_L18_st32x8_1[6]);
  kcg_copy_array_uint8_4(&_L77[28], &_L18_st32x8_1[7]);
  kcg_copy_array_int64_4(&acc[0], &_L36[0]);
  kcg_copy_array_int64_4(&acc[4], &_L36[1]);
  kcg_copy_array_int64_4(&acc[8], &_L36[2]);
  kcg_copy_array_int64_4(&acc[12], &_L36[3]);
  kcg_copy_array_int64_4(&acc[16], &_L36[4]);
  kcg_copy_array_int64_4(&acc[20], &_L36[5]);
  kcg_copy_array_int64_4(&acc[24], &_L36[6]);
  kcg_copy_array_int64_4(&acc[28], &_L36[7]);
  for (idx = 0; idx < 32; idx++) {
    acc[idx + 32] = kcg_lit_int64(0);
  }
  kcg_copy_array_int64_4(&tmp2[0], &_L6_reduce_1[0]);
  kcg_copy_array_int64_4(&tmp2[4], &_L6_reduce_1[1]);
  kcg_copy_array_int64_4(&tmp2[8], &_L6_reduce_1[2]);
  kcg_copy_array_int64_4(&tmp2[12], &_L6_reduce_1[3]);
  kcg_copy_array_int64_4(&tmp2[16], &_L6_reduce_1[4]);
  kcg_copy_array_int64_4(&tmp2[20], &_L6_reduce_1[5]);
  kcg_copy_array_int64_4(&tmp2[24], &_L6_reduce_1[6]);
  kcg_copy_array_int64_4(&tmp2[28], &_L6_reduce_1[7]);
  kcg_copy_array_int64_4(&tmp2[32], &_L6_reduce_1[8]);
  kcg_copy_array_int64_4(&tmp2[36], &_L6_reduce_1[9]);
  kcg_copy_array_int64_4(&tmp2[40], &_L6_reduce_1[10]);
  kcg_copy_array_int64_4(&tmp2[44], &_L6_reduce_1[11]);
  kcg_copy_array_int64_4(&tmp2[48], &_L6_reduce_1[12]);
  kcg_copy_array_int64_4(&tmp2[52], &_L6_reduce_1[13]);
  kcg_copy_array_int64_4(&tmp2[56], &_L6_reduce_1[14]);
  kcg_copy_array_int64_4(&tmp2[60], &_L6_reduce_1[15]);
  /* @1/_L3=(nacl::sign::modL#1)/ */ modL_nacl_sign(&tmp2, &tmp1);
  kcg_copy_array_int64_64(&tmp2, &acc);
  /* _L38= */
  for (idx = 0; idx < 8; idx++) {
    kcg_copy_array_int64_64(&acc, &tmp2);
    /* _L38=(nacl::sign::sign_finalize_it1#1)/ */
    sign_finalize_it1_nacl_sign(
      /* _L38= */(kcg_int32) idx,
      &acc,
      tmp1[idx],
      &_L77,
      &tmp2);
  }
  /* _L32=(nacl::sign::modL#1)/ */ modL_nacl_sign(&tmp2, &tmp1);
  kcg_copy_array_uint32_8(&(*signature)[0], &(*signin).stash);
  kcg_copy_array_uint32_8(&(*signature)[8], &tmp1);
}

/*
  Expanded instances for: nacl::sign::sign_finalize/
  @1: (nacl::sign::reduce#1)
  @2: @1/(slideTypes::st32_casti64#1)
  @3: (slideTypes::st32x8#1)
  @4: @3/(slideTypes::st32#1)
  @5: (slideTypes::st32_casti64#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_finalize_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

