/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_hash512_test_ietf_RFC6234_256.h"

/* test::ietf_RFC6234::bench_hash512/ */
kcg_bool bench_hash512_test_ietf_RFC6234_256(
  /* _L1/, failing/ */
  kcg_bool failing_256,
  /* tc/ */
  simpleTC_test *tc_256)
{
  StreamChunk_slideTypes tmp;
  /* _L10/ */
  array_uint32_16_256 _L10_256;
  kcg_size idx;
  /* _L2/, failed/ */
  kcg_bool failed_256;

  for (idx = 0; idx < 256; idx++) {
    kcg_copy_StreamChunk_slideTypes(&_L10_256[idx], &(*tc_256).m[0]);
  }
  /* _L19=(hash::sha512::run#1)/ */
  run_hash_sha512_256(
    &_L10_256,
    (*tc_256).mlen + kcg_lit_int32(16320),
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &tmp);
  failed_256 = failing_256 || kcg_comp_StreamChunk_slideTypes(
      &tmp,
      (array_uint32_16 *) &ZeroChunk_slideTypes);
  return failed_256;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_hash512_test_ietf_RFC6234_256.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

