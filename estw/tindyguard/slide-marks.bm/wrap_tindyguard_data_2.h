/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _wrap_tindyguard_data_2_H_
#define _wrap_tindyguard_data_2_H_

#include "kcg_types.h"
#include "aead_nacl_box_2_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::wrap/ */
extern void wrap_tindyguard_data_2(
  /* @1/_L1/, @1/i/, _L65/, len/ */
  size_slideTypes len_2,
  /* msg/ */
  array_uint32_16_2 *msg_2,
  /* s/ */
  Session_tindyguardTypes *s_2,
  /* slength/ */
  length_t_udp *slength_2,
  /* _L75/, soffs/ */
  length_t_udp *soffs_2,
  /* datamsg/ */
  array_uint32_16_3 *datamsg_2,
  /* soo/ */
  Session_tindyguardTypes *soo_2,
  /* _L73/, fail/, failed/ */
  kcg_bool *failed_2);

/*
  Expanded instances for: tindyguard::data::wrap/
  @1: (M::dec#2)
*/

#endif /* _wrap_tindyguard_data_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** wrap_tindyguard_data_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

