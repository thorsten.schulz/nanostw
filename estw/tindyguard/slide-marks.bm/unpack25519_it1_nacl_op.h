/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _unpack25519_it1_nacl_op_H_
#define _unpack25519_it1_nacl_op_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::unpack25519_it1/ */
extern kcg_int64 unpack25519_it1_nacl_op(
  /* i/ */
  kcg_uint32 i,
  /* n/ */
  array_uint32_8 *n);



#endif /* _unpack25519_it1_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unpack25519_it1_nacl_op.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

