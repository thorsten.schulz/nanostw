/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _respond_tindyguard_handshake_8_H_
#define _respond_tindyguard_handshake_8_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "eval_init_tindyguard_handshake_8.h"
#include "aead_nacl_box_1_1_20.h"
#include "hkdf_hash_blake2s_1_3.h"
#include "hkdf_hash_blake2s_1_2.h"
#include "scalarmultDonna_nacl_box.h"
#include "hkdfChunk1_hash_blake2s_1.h"
#include "block_refine_hash_blake2s.h"
#include "keyPair_nacl_box_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_keyPair_nacl_box_20 /* @2/IfBlock1:else:_L3=(nacl::box::keyPair#2)/ */ Context_keyPair_2_ephemeral_1_build_response_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_respond_tindyguard_handshake_8;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::handshake::respond/ */
extern void respond_tindyguard_handshake_8(
  /* _L21/, rlength/ */
  length_t_udp rlength_8,
  /* _L12/, initmsg/ */
  array_uint32_16_4 *initmsg_8,
  /* _L32/, endpoint/ */
  peer_t_udp *endpoint_8,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8,
  /* session/ */
  Session_tindyguardTypes *session_8,
  /* knownPeer/ */
  _6_array *knownPeer_8,
  /* _L34/, now/ */
  kcg_int64 now_8,
  /* _L24/, slength/ */
  length_t_udp *slength_8,
  /* _L25/, responsemsg/ */
  array_uint32_16_4 *responsemsg_8,
  /* _L3/, soo/ */
  Session_tindyguardTypes *soo_8,
  /* _L33/, failed/ */
  kcg_bool *failed_8,
  /* _L30/, noPeer/ */
  kcg_bool *noPeer_8,
  /* _L1/, weakKey/ */
  kcg_bool *weakKey_8,
  outC_respond_tindyguard_handshake_8 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void respond_reset_tindyguard_handshake_8(
  outC_respond_tindyguard_handshake_8 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void respond_init_tindyguard_handshake_8(
  outC_respond_tindyguard_handshake_8 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::handshake::respond/
  @1: (tindyguard::handshake::build_response#1)
  @2: @1/(tindyguard::handshake::ephemeral#1)
*/

#endif /* _respond_tindyguard_handshake_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** respond_tindyguard_handshake_8.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

