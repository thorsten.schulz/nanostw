/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _prependData_slideTypes_8_H_
#define _prependData_slideTypes_8_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* slideTypes::prependData/ */
extern void prependData_slideTypes_8(
  /* _L3/, prep/ */
  array_uint32_8 *prep_8,
  /* _L1/, in/ */
  StreamChunk_slideTypes *in_8,
  /* _L5/, prepo/ */
  array_uint32_8 *prepo_8,
  /* _L4/, out/ */
  StreamChunk_slideTypes *out_8);



#endif /* _prependData_slideTypes_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** prependData_slideTypes_8.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

