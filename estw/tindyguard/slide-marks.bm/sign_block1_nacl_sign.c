/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_block1_nacl_sign.h"

/* nacl::sign::sign_block1/ */
void sign_block1_nacl_sign(
  /* _L4/, i/ */
  size_slideTypes i,
  /* _L5/, signin/ */
  prependAkku_nacl_sign *signin,
  /* _L8/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L2/, goOn/ */
  kcg_bool *goOn,
  /* _L29/, signoo/ */
  prependAkku_nacl_sign *signoo)
{
  array_uint32_16 tmp;

  kcg_copy_array_uint32_8(&(*signoo).d, &(*signin).d);
  kcg_copy_array_uint32_8(&(*signoo).stash, (array_uint32_8 *) &(*msg)[8]);
  kcg_copy_array_uint32_8(&tmp[0], &(*signin).stash);
  kcg_copy_array_uint32_8(&tmp[8], (array_uint32_8 *) &(*msg)[0]);
  /* _L2=(hash::sha512::stream_it#1)/ */
  stream_it_hash_sha512(i, &(*signin).accu, &tmp, goOn, &(*signoo).accu);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_block1_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

