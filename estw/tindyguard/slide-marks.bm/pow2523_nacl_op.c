/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pow2523_nacl_op.h"

/* nacl::op::pow2523/ */
void pow2523_nacl_op(/* _L3/, i/ */ gf_nacl_op *i, /* _L2/, o/ */ gf_nacl_op *o)
{
  gf_nacl_op acc;
  kcg_size idx;

  kcg_copy_gf_nacl_op(o, i);
  /* _L2= */
  for (idx = 0; idx < 251; idx++) {
    kcg_copy_gf_nacl_op(&acc, o);
    /* _L2=(nacl::op::pow2523_it1#1)/ */
    pow2523_it1_nacl_op(/* _L2= */(kcg_int32) idx, &acc, i, o);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pow2523_nacl_op.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

