/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _bench_hash_test_ietf_RFC7693_64_H_
#define _bench_hash_test_ietf_RFC7693_64_H_

#include "kcg_types.h"
#include "block_refine_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC7693::bench_hash/ */
extern kcg_bool bench_hash_test_ietf_RFC7693_64(
  /* _L1/, failing/ */
  kcg_bool failing_64,
  /* tc/ */
  simpleTC_test *tc_64);



#endif /* _bench_hash_test_ietf_RFC7693_64_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_hash_test_ietf_RFC7693_64.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

