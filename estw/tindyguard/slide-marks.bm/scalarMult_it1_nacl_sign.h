/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _scalarMult_it1_nacl_sign_H_
#define _scalarMult_it1_nacl_sign_H_

#include "kcg_types.h"
#include "scalarMult_it2_nacl_sign.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::scalarMult_it1/ */
extern void scalarMult_it1_nacl_sign(
  /* _L2/, akku/ */
  pq_pair_nacl_sign *akku,
  /* _L3/, s/ */
  kcg_uint8 s,
  /* _L1/, akkoo/ */
  pq_pair_nacl_sign *akkoo);



#endif /* _scalarMult_it1_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarMult_it1_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

