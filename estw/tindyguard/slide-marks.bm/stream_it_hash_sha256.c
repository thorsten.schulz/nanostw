/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "stream_it_hash_sha256.h"

/* hash::sha256::stream_it/ */
void stream_it_hash_sha256(
  /* _L9/, i/ */
  size_slideTypes i,
  /* _L3/, zin/ */
  array_uint32_8 *zin,
  /* _L4/, m/ */
  StreamChunk_slideTypes *m,
  /* _L5/, len/ */
  size_slideTypes len,
  /* _L10/, goOn/ */
  kcg_bool *goOn,
  /* _L2/, zout/ */
  array_uint32_8 *zout)
{
  *goOn = len >= i * hBlockBytes_hash_sha256 + hBlockBytes_hash_sha256 +
    hBlockBytes_hash_sha256;
  /* _L2=(hash::sha256::block_refine#2)/ */
  block_refine_hash_sha256(zin, m, zout);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** stream_it_hash_sha256.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

