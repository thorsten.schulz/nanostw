/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sigma64_hash_libsha_1_8_7_H_
#define _sigma64_hash_libsha_1_8_7_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::sigma64/ */
extern kcg_uint64 sigma64_hash_libsha_1_8_7(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, _L1/, x/ */
  kcg_uint64 x_1_8_7);

/*
  Expanded instances for: hash::libsha::sigma64/
  @1: (M::Ror64#1)
  @2: (M::Ror64#2)
*/

#endif /* _sigma64_hash_libsha_1_8_7_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sigma64_hash_libsha_1_8_7.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

