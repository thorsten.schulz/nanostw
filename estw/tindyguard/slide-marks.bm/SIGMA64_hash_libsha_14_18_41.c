/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "SIGMA64_hash_libsha_14_18_41.h"

/* hash::libsha::SIGMA64/ */
kcg_uint64 SIGMA64_hash_libsha_14_18_41(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, @3/_L11/, @3/x/, _L1/, x/ */
  kcg_uint64 x_14_18_41)
{
  /* _L6/, ret/ */
  kcg_uint64 ret_14_18_41;

  ret_14_18_41 = ((x_14_18_41 >> kcg_lit_uint64(14)) | kcg_lsl_uint64(
        x_14_18_41,
        kcg_lit_uint64(50))) ^ ((x_14_18_41 >> kcg_lit_uint64(18)) |
      kcg_lsl_uint64(x_14_18_41, kcg_lit_uint64(46))) ^ ((x_14_18_41 >>
        kcg_lit_uint64(41)) | kcg_lsl_uint64(x_14_18_41, kcg_lit_uint64(23)));
  return ret_14_18_41;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** SIGMA64_hash_libsha_14_18_41.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

