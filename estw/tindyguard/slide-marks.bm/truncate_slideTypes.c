/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "truncate_slideTypes.h"

/* slideTypes::truncate/ */
void truncate_slideTypes(
  /* _L1/, mlast/ */
  StreamChunk_slideTypes *mlast,
  /* _L2/, mlen/ */
  size_slideTypes mlen,
  /* _L5/, mtrunc/ */
  StreamChunk_slideTypes *mtrunc)
{
  kcg_bool cond_iterw;
  kcg_size idx;
  size_slideTypes noname;
  /* _L6/ */
  kcg_int32 _L6;
  /* _L13/ */
  kcg_int32 _L13;
  /* _L20/ */
  kcg_uint32 _L20;

  _L6 = mlen % StreamChunkBytes_slideTypes;
  /* _L20= */
  switch (_L6 % kcg_lit_int32(4)) {
    case kcg_lit_int32(0) :
      _L20 = kcg_lit_uint32(0);
      break;
    case kcg_lit_int32(1) :
      _L20 = kcg_lit_uint32(255);
      break;
    case kcg_lit_int32(2) :
      _L20 = kcg_lit_uint32(65535);
      break;
    default :
      _L20 = kcg_lit_uint32(16777215);
      break;
  }
  _L13 = _L6 / kcg_lit_int32(4);
  /* _L3= */
  for (idx = 0; idx < 16; idx++) {
    cond_iterw = /* _L3= */(kcg_int32) idx != _L13;
    /* @1/_L5= */
    if (cond_iterw) {
      (*mtrunc)[idx] = (*mlast)[idx];
    }
    else {
      (*mtrunc)[idx] = (*mlast)[idx] & _L20;
    }
    noname = /* _L3= */(kcg_int32) (idx + 1);
    /* _L3= */
    if (!cond_iterw) {
      break;
    }
  }
#ifdef KCG_MAPW_CPY

  /* _L3= */
  for (idx = /* _L3= */(kcg_size) noname; idx < 16; idx++) {
    (*mtrunc)[idx] = kcg_lit_uint32(0);
  }
#endif /* KCG_MAPW_CPY */

}

/*
  Expanded instances for: slideTypes::truncate/
  @1: (slideTypes::truncate_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** truncate_slideTypes.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

