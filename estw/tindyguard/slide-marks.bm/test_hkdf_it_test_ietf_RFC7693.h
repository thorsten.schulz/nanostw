/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _test_hkdf_it_test_ietf_RFC7693_H_
#define _test_hkdf_it_test_ietf_RFC7693_H_

#include "kcg_types.h"
#include "hmacChunk_hash_blake2s_2.h"
#include "hmacChunk_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC7693::test_hkdf_it/ */
extern kcg_bool test_hkdf_it_test_ietf_RFC7693(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L4/, tc/ */
  testcase_hkdf_test_ietf_RFC7693 *tc);



#endif /* _test_hkdf_it_test_ietf_RFC7693_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_hkdf_it_test_ietf_RFC7693.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

