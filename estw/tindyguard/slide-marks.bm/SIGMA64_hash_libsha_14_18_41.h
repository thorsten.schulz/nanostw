/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _SIGMA64_hash_libsha_14_18_41_H_
#define _SIGMA64_hash_libsha_14_18_41_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::SIGMA64/ */
extern kcg_uint64 SIGMA64_hash_libsha_14_18_41(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, @3/_L11/, @3/x/, _L1/, x/ */
  kcg_uint64 x_14_18_41);

/*
  Expanded instances for: hash::libsha::SIGMA64/
  @1: (M::Ror64#1)
  @2: (M::Ror64#2)
  @3: (M::Ror64#3)
*/

#endif /* _SIGMA64_hash_libsha_14_18_41_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** SIGMA64_hash_libsha_14_18_41.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

