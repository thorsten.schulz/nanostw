/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _tests_test_ietf_RFC8032_H_
#define _tests_test_ietf_RFC8032_H_

#include "kcg_types.h"
#include "test_ed25519_test_ietf_RFC8032.h"
#include "bench_ed25520_test_ietf_RFC8032_256.h"
#include "bench_ed25520_test_ietf_RFC8032_64.h"
#include "bench_ed25520_test_ietf_RFC8032_16.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC8032::tests/ */
extern kcg_bool tests_test_ietf_RFC8032(void);



#endif /* _tests_test_ietf_RFC8032_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** tests_test_ietf_RFC8032.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

