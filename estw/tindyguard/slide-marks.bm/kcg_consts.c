/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

#include "kcg_consts.h"

/* test::ietf_RFC7539::xnonce/ */
const XNonce_nacl_core_chacha xnonce_test_ietf_RFC7539 = { kcg_lit_uint32(0x07),
  kcg_lit_uint32(0x43424140), kcg_lit_uint32(0x47464544), kcg_lit_uint32(
    0x4b4a4948), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* test::ietf_RFC7539::firstkey/ */
const Key_nacl_core firstkey_test_ietf_RFC7539 = { kcg_lit_uint32(0x83828180),
  kcg_lit_uint32(0x87868584), kcg_lit_uint32(0x8b8a8988), kcg_lit_uint32(
    0x8f8e8d8c), kcg_lit_uint32(0x93929190), kcg_lit_uint32(0x97969594),
  kcg_lit_uint32(0x9b9a9998), kcg_lit_uint32(0x9f9e9d9c) };

/* test::ietf_RFC7539::nonce/ */
const Nonce_nacl_core_chacha nonce_test_ietf_RFC7539 = { kcg_lit_uint32(0x07),
  kcg_lit_uint32(0x43424140), kcg_lit_uint32(0x47464544) };

/* test::ietf_RFC7539::addata/ */
const array_uint32_16_1 addata_test_ietf_RFC7539 = { { kcg_lit_uint32(
      0x53525150), kcg_lit_uint32(0xc3c2c1c0), kcg_lit_uint32(0xc7c6c5c4),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) } };

/* test::ietf_RFC7539::mdata/ */
const array_uint32_16_2 mdata_test_ietf_RFC7539 = { { kcg_lit_uint32(
      0x6964614c), kcg_lit_uint32(0x61207365), kcg_lit_uint32(0x4720646e),
    kcg_lit_uint32(0x6c746e65), kcg_lit_uint32(0x6e656d65), kcg_lit_uint32(
      0x20666f20), kcg_lit_uint32(0x20656874), kcg_lit_uint32(0x73616c63),
    kcg_lit_uint32(0x666f2073), kcg_lit_uint32(0x39392720), kcg_lit_uint32(
      0x6649203a), kcg_lit_uint32(0x63204920), kcg_lit_uint32(0x646c756f),
    kcg_lit_uint32(0x66666f20), kcg_lit_uint32(0x79207265), kcg_lit_uint32(
      0x6f20756f) }, { kcg_lit_uint32(0x20796c6e), kcg_lit_uint32(0x20656e6f),
    kcg_lit_uint32(0x20706974), kcg_lit_uint32(0x20726f66), kcg_lit_uint32(
      0x20656874), kcg_lit_uint32(0x75747566), kcg_lit_uint32(0x202c6572),
    kcg_lit_uint32(0x736e7573), kcg_lit_uint32(0x65657263), kcg_lit_uint32(
      0x6f77206e), kcg_lit_uint32(0x20646c75), kcg_lit_uint32(0x69206562),
    kcg_lit_uint32(0x00002e74), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) } };

/* hash::sha256::K/ */
const array_uint32_16_4 K_hash_sha256 = { { kcg_lit_uint32(0x428a2f98),
    kcg_lit_uint32(0x71374491), kcg_lit_uint32(0xb5c0fbcf), kcg_lit_uint32(
      0xe9b5dba5), kcg_lit_uint32(0x3956c25b), kcg_lit_uint32(0x59f111f1),
    kcg_lit_uint32(0x923f82a4), kcg_lit_uint32(0xab1c5ed5), kcg_lit_uint32(
      0xd807aa98), kcg_lit_uint32(0x12835b01), kcg_lit_uint32(0x243185be),
    kcg_lit_uint32(0x550c7dc3), kcg_lit_uint32(0x72be5d74), kcg_lit_uint32(
      0x80deb1fe), kcg_lit_uint32(0x9bdc06a7), kcg_lit_uint32(0xc19bf174) }, {
    kcg_lit_uint32(0xe49b69c1), kcg_lit_uint32(0xefbe4786), kcg_lit_uint32(
      0x0fc19dc6), kcg_lit_uint32(0x240ca1cc), kcg_lit_uint32(0x2de92c6f),
    kcg_lit_uint32(0x4a7484aa), kcg_lit_uint32(0x5cb0a9dc), kcg_lit_uint32(
      0x76f988da), kcg_lit_uint32(0x983e5152), kcg_lit_uint32(0xa831c66d),
    kcg_lit_uint32(0xb00327c8), kcg_lit_uint32(0xbf597fc7), kcg_lit_uint32(
      0xc6e00bf3), kcg_lit_uint32(0xd5a79147), kcg_lit_uint32(0x06ca6351),
    kcg_lit_uint32(0x14292967) }, { kcg_lit_uint32(0x27b70a85), kcg_lit_uint32(
      0x2e1b2138), kcg_lit_uint32(0x4d2c6dfc), kcg_lit_uint32(0x53380d13),
    kcg_lit_uint32(0x650a7354), kcg_lit_uint32(0x766a0abb), kcg_lit_uint32(
      0x81c2c92e), kcg_lit_uint32(0x92722c85), kcg_lit_uint32(0xa2bfe8a1),
    kcg_lit_uint32(0xa81a664b), kcg_lit_uint32(0xc24b8b70), kcg_lit_uint32(
      0xc76c51a3), kcg_lit_uint32(0xd192e819), kcg_lit_uint32(0xd6990624),
    kcg_lit_uint32(0xf40e3585), kcg_lit_uint32(0x106aa070) }, { kcg_lit_uint32(
      0x19a4c116), kcg_lit_uint32(0x1e376c08), kcg_lit_uint32(0x2748774c),
    kcg_lit_uint32(0x34b0bcb5), kcg_lit_uint32(0x391c0cb3), kcg_lit_uint32(
      0x4ed8aa4a), kcg_lit_uint32(0x5b9cca4f), kcg_lit_uint32(0x682e6ff3),
    kcg_lit_uint32(0x748f82ee), kcg_lit_uint32(0x78a5636f), kcg_lit_uint32(
      0x84c87814), kcg_lit_uint32(0x8cc70208), kcg_lit_uint32(0x90befffa),
    kcg_lit_uint32(0xa4506ceb), kcg_lit_uint32(0xbef9a3f7), kcg_lit_uint32(
      0xc67178f2) } };

/* hash::sha256::IV/ */
const array_uint32_8 IV_hash_sha256 = { kcg_lit_uint32(0x6A09E667),
  kcg_lit_uint32(0xBB67AE85), kcg_lit_uint32(0x3C6EF372), kcg_lit_uint32(
    0xA54FF53A), kcg_lit_uint32(0x510E527F), kcg_lit_uint32(0x9B05688C),
  kcg_lit_uint32(0x1F83D9AB), kcg_lit_uint32(0x5BE0CD19) };

/* test::ietf_RFC6234::test_hkdf256/ */
const _3_array test_hkdf256_test_ietf_RFC6234 = { { kcg_lit_int32(22), { {
        kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(
          0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b),
        kcg_lit_uint32(0xb0b), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(13), { { kcg_lit_uint32(0x3020100), kcg_lit_uint32(0x7060504),
        kcg_lit_uint32(0xb0a0908), kcg_lit_uint32(0x0c), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(10), { {
        kcg_lit_uint32(0xf3f2f1f0), kcg_lit_uint32(0xf7f6f5f4), kcg_lit_uint32(
          0xf9f8), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { kcg_lit_uint32(0x36097707),
      kcg_lit_uint32(0xdf322e2c), kcg_lit_uint32(0xd3fdc0d), kcg_lit_uint32(
        0x63ba7bc4), kcg_lit_uint32(0x3bc7b690), kcg_lit_uint32(0x319c0fb5),
      kcg_lit_uint32(0x4a84ec22), kcg_lit_uint32(0xe5b3c2d7), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) },
    kcg_lit_int32(42), { { kcg_lit_uint32(0x255fb23c), kcg_lit_uint32(
          0x7ad5acfa), kcg_lit_uint32(0x644f4390), kcg_lit_uint32(0x2a2f36d0),
        kcg_lit_uint32(0x900a2d2d), kcg_lit_uint32(0x4c5a1acf), kcg_lit_uint32(
          0x562db05d), kcg_lit_uint32(0xbfc5c4ec), kcg_lit_uint32(0x8720034),
        kcg_lit_uint32(0x1887b8d5), kcg_lit_uint32(0x6558), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } } }, { kcg_lit_int32(80), {
      { kcg_lit_uint32(0x3020100), kcg_lit_uint32(0x7060504), kcg_lit_uint32(
          0xb0a0908), kcg_lit_uint32(0xf0e0d0c), kcg_lit_uint32(0x13121110),
        kcg_lit_uint32(0x17161514), kcg_lit_uint32(0x1b1a1918), kcg_lit_uint32(
          0x1f1e1d1c), kcg_lit_uint32(0x23222120), kcg_lit_uint32(0x27262524),
        kcg_lit_uint32(0x2b2a2928), kcg_lit_uint32(0x2f2e2d2c), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x3b3a3938),
        kcg_lit_uint32(0x3f3e3d3c) }, { kcg_lit_uint32(0x43424140),
        kcg_lit_uint32(0x47464544), kcg_lit_uint32(0x4b4a4948), kcg_lit_uint32(
          0x4f4e4d4c), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(80), { {
        kcg_lit_uint32(0x63626160), kcg_lit_uint32(0x67666564), kcg_lit_uint32(
          0x6b6a6968), kcg_lit_uint32(0x6f6e6d6c), kcg_lit_uint32(0x73727170),
        kcg_lit_uint32(0x77767574), kcg_lit_uint32(0x7b7a7978), kcg_lit_uint32(
          0x7f7e7d7c), kcg_lit_uint32(0x83828180), kcg_lit_uint32(0x87868584),
        kcg_lit_uint32(0x8b8a8988), kcg_lit_uint32(0x8f8e8d8c), kcg_lit_uint32(
          0x93929190), kcg_lit_uint32(0x97969594), kcg_lit_uint32(0x9b9a9998),
        kcg_lit_uint32(0x9f9e9d9c) }, { kcg_lit_uint32(0xa3a2a1a0),
        kcg_lit_uint32(0xa7a6a5a4), kcg_lit_uint32(0xabaaa9a8), kcg_lit_uint32(
          0xafaeadac), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(80), { {
        kcg_lit_uint32(0xb3b2b1b0), kcg_lit_uint32(0xb7b6b5b4), kcg_lit_uint32(
          0xbbbab9b8), kcg_lit_uint32(0xbfbebdbc), kcg_lit_uint32(0xc3c2c1c0),
        kcg_lit_uint32(0xc7c6c5c4), kcg_lit_uint32(0xcbcac9c8), kcg_lit_uint32(
          0xcfcecdcc), kcg_lit_uint32(0xd3d2d1d0), kcg_lit_uint32(0xd7d6d5d4),
        kcg_lit_uint32(0xdbdad9d8), kcg_lit_uint32(0xdfdedddc), kcg_lit_uint32(
          0xe3e2e1e0), kcg_lit_uint32(0xe7e6e5e4), kcg_lit_uint32(0xebeae9e8),
        kcg_lit_uint32(0xefeeedec) }, { kcg_lit_uint32(0xf3f2f1f0),
        kcg_lit_uint32(0xf7f6f5f4), kcg_lit_uint32(0xfbfaf9f8), kcg_lit_uint32(
          0xfffefdfc), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { kcg_lit_uint32(0x8cb8a606),
      kcg_lit_uint32(0x1a365358), kcg_lit_uint32(0x9c4c1006), kcg_lit_uint32(
        0x5cb435eb), kcg_lit_uint32(0x140076ef), kcg_lit_uint32(0x1714690),
      kcg_lit_uint32(0x403f194a), kcg_lit_uint32(0x44c25fc1), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) },
    kcg_lit_int32(82), { { kcg_lit_uint32(0x8d391eb1), kcg_lit_uint32(
          0xa12703c8), kcg_lit_uint32(0x8cf7e7c8), kcg_lit_uint32(0x34496a59),
        kcg_lit_uint32(0xda2e014f), kcg_lit_uint32(0xd8fa4e2d), kcg_lit_uint32(
          0x4ccc50a0), kcg_lit_uint32(0x7ca9af19), kcg_lit_uint32(0x995a0459),
        kcg_lit_uint32(0x7282c7ca), kcg_lit_uint32(0xc641cb71), kcg_lit_uint32(
          0x90e595e), kcg_lit_uint32(0x607532da), kcg_lit_uint32(0xb8092f0c),
        kcg_lit_uint32(0xa9937736), kcg_lit_uint32(0x71dba3ac) }, {
        kcg_lit_uint32(0x81c530cc), kcg_lit_uint32(0x873eec79), kcg_lit_uint32(
          0xd5014cc1), kcg_lit_uint32(0x4f43f3c1), kcg_lit_uint32(0x871d),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) } } }, { kcg_lit_int32(22), { { kcg_lit_uint32(0xb0b0b0b),
        kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(
          0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(0), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, {
      kcg_lit_uint32(0xa324ef19), kcg_lit_uint32(0x167b712c), kcg_lit_uint32(
        0x1da9337f), kcg_lit_uint32(0xdf8b646f), kcg_lit_uint32(0x76675996),
      kcg_lit_uint32(0x7763dbaf), kcg_lit_uint32(0x1c4c43ac), kcg_lit_uint32(
        0x4cb3c29), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0) }, kcg_lit_int32(42), { { kcg_lit_uint32(
          0x75e7a48d), kcg_lit_uint32(0x8fc163a5), kcg_lit_uint32(0x2a805f71),
        kcg_lit_uint32(0x315a3c06), kcg_lit_uint32(0x5c1fa1b8), kcg_lit_uint32(
          0x9e87e15e), kcg_lit_uint32(0x5f4e45c3), kcg_lit_uint32(0x2d8d733c),
        kcg_lit_uint32(0x9513209d), kcg_lit_uint32(0x1ab6a4fa), kcg_lit_uint32(
          0xc896), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } } } };

/* test::ietf_RFC6234::test_sha256/ */
const _4_array test_sha256_test_ietf_RFC6234 = { { { { kcg_lit_uint32(0x636261),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(3), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x00),
    kcg_lit_int32(0), { kcg_lit_uint32(0xbf1678ba), kcg_lit_uint32(0xeacf018f),
      kcg_lit_uint32(0xde404141), kcg_lit_uint32(0x2322ae5d), kcg_lit_uint32(
        0xa36103b0), kcg_lit_uint32(0x9c7a1796), kcg_lit_uint32(0x61ff10b4),
      kcg_lit_uint32(0xad1500f2), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(
          0x64636261), kcg_lit_uint32(0x65646362), kcg_lit_uint32(0x66656463),
        kcg_lit_uint32(0x67666564), kcg_lit_uint32(0x68676665), kcg_lit_uint32(
          0x69686766), kcg_lit_uint32(0x6a696867), kcg_lit_uint32(0x6b6a6968),
        kcg_lit_uint32(0x6c6b6a69), kcg_lit_uint32(0x6d6c6b6a), kcg_lit_uint32(
          0x6e6d6c6b), kcg_lit_uint32(0x6f6e6d6c), kcg_lit_uint32(0x706f6e6d),
        kcg_lit_uint32(0x71706f6e), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(56), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), kcg_lit_uint32(0x00), kcg_lit_int32(0), { kcg_lit_uint32(
        0x616a8d24), kcg_lit_uint32(0xb83806d2), kcg_lit_uint32(0x9326c0e5),
      kcg_lit_uint32(0x39603e0c), kcg_lit_uint32(0x59e43ca3), kcg_lit_uint32(
        0x6721ff64), kcg_lit_uint32(0xd4edecf6), kcg_lit_uint32(0xc106db19),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) }, {
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(640), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x00),
    kcg_lit_int32(0), { kcg_lit_uint32(0x32474859), kcg_lit_uint32(0xfabd5184),
      kcg_lit_uint32(0x25620585), kcg_lit_uint32(0xd8c12c46), kcg_lit_uint32(
        0xfb77d867), kcg_lit_uint32(0xcef08d38), kcg_lit_uint32(0xb55af235),
      kcg_lit_uint32(0xb5fb2b56), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(0), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x68),
    kcg_lit_int32(5), { kcg_lit_uint32(0x2ae0d3d6), kcg_lit_uint32(0x8c4aa831),
      kcg_lit_uint32(0xed1897aa), kcg_lit_uint32(0xbe57206c), kcg_lit_uint32(
        0xe745db09), kcg_lit_uint32(0x7b53e82), kcg_lit_uint32(0x73a5e79c),
      kcg_lit_uint32(0x950f76a3), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(
          0x19), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(1), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x00),
    kcg_lit_int32(0), { kcg_lit_uint32(0x2e2eaa68), kcg_lit_uint32(0x6ef9dfe5),
      kcg_lit_uint32(0xc7e65533), kcg_lit_uint32(0x3d3e37ee), kcg_lit_uint32(
        0xf7174e6a), kcg_lit_uint32(0xd818955f), kcg_lit_uint32(0xc9c7043),
      kcg_lit_uint32(0xd4e3c39b), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(
          0xc64627be), kcg_lit_uint32(0x5f7652db), kcg_lit_uint32(0x70882fdb),
        kcg_lit_uint32(0x739a0f), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(15), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), kcg_lit_uint32(0x60), kcg_lit_int32(3), { kcg_lit_uint32(
        0xc81dec77), kcg_lit_uint32(0xf21f829c), kcg_lit_uint32(0x899027a1),
      kcg_lit_uint32(0x351b09fa), kcg_lit_uint32(0xb96cdb8), kcg_lit_uint32(
        0x1def7ca), kcg_lit_uint32(0x768a7c6), kcg_lit_uint32(0x72b9be56),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(0x7025d7e3), kcg_lit_uint32(
          0x7c78dddc), kcg_lit_uint32(0xb27a88e3), kcg_lit_uint32(0x524668cd),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(16), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x00),
    kcg_lit_int32(0), { kcg_lit_uint32(0x9be65e17), kcg_lit_uint32(0x589bba02),
      kcg_lit_uint32(0xfda5b0e2), kcg_lit_uint32(0xea9c8113), kcg_lit_uint32(
        0x40393f57), kcg_lit_uint32(0x51824fa9), kcg_lit_uint32(0x942cf28),
      kcg_lit_uint32(0xe8b4abbe), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(
          0x7103743e), kcg_lit_uint32(0xb9c210c8), kcg_lit_uint32(0x804ec09f),
        kcg_lit_uint32(0x7cef0749), kcg_lit_uint32(0x8be26bf2), kcg_lit_uint32(
          0xa358cb57), kcg_lit_uint32(0x7c0f3e2), kcg_lit_uint32(0xc1496e16),
        kcg_lit_uint32(0x4ca39b2e), kcg_lit_uint32(0x91060401), kcg_lit_uint32(
          0x1576ea29), kcg_lit_uint32(0x70452564), kcg_lit_uint32(0x1d92b3a),
        kcg_lit_uint32(0xe0b06ee1), kcg_lit_uint32(0x14a0eb5d), kcg_lit_uint32(
          0x664ffeb) }, { kcg_lit_uint32(0x36547da0), kcg_lit_uint32(
          0x2d74ff4e), kcg_lit_uint32(0xb3b079a7), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(76), { { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(0),
    kcg_lit_uint32(0xa0), kcg_lit_int32(3), { kcg_lit_uint32(0x46d69a3e),
      kcg_lit_uint32(0x2aadbb8b), kcg_lit_uint32(0xc2cdc2c3), kcg_lit_uint32(
        0xba18e092), kcg_lit_uint32(0x960bd75f), kcg_lit_uint32(0x9767f10c),
      kcg_lit_uint32(0x8e7fc77), kcg_lit_uint32(0xe966b0fd), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { {
        kcg_lit_uint32(0x4e752683), kcg_lit_uint32(0x2f377722), kcg_lit_uint32(
          0x202bc14f), kcg_lit_uint32(0xf0fe7a52), kcg_lit_uint32(0x69058a4d),
        kcg_lit_uint32(0xd51ab171), kcg_lit_uint32(0xc1a72371), kcg_lit_uint32(
          0x7637), kcg_lit_uint32(0xf3f6bed7), kcg_lit_uint32(0x8a9f7c1),
        kcg_lit_uint32(0x819da33a), kcg_lit_uint32(0x7710b30d), kcg_lit_uint32(
          0x1e8bab7d), kcg_lit_uint32(0x4ab8027f), kcg_lit_uint32(0x3273c726),
        kcg_lit_uint32(0x74238b5f) }, { kcg_lit_uint32(0x5a4b7ade),
        kcg_lit_uint32(0x5c5ccb58), kcg_lit_uint32(0xe6ce5bf3), kcg_lit_uint32(
          0x5b6e94fb), kcg_lit_uint32(0x59fa94d6), kcg_lit_uint32(0x3feb8b3a),
        kcg_lit_uint32(0xec92659d), kcg_lit_uint32(0xca66aaed), kcg_lit_uint32(
          0xc9da282), kcg_lit_uint32(0x33f9bc51), kcg_lit_uint32(0xd7e53062),
        kcg_lit_uint32(0xa4c0e484), kcg_lit_uint32(0xa3798d3f), kcg_lit_uint32(
          0xba5c160a), kcg_lit_uint32(0x772b45be), kcg_lit_uint32(0x9719c4b) },
      { kcg_lit_uint32(0x8f137da9), kcg_lit_uint32(0x96289212), kcg_lit_uint32(
          0xdc0a6c6f), kcg_lit_uint32(0x5aad6a10), kcg_lit_uint32(0x8230dd9f),
        kcg_lit_uint32(0xc6b26957), kcg_lit_uint32(0x5967af71), kcg_lit_uint32(
          0x39eb28df), kcg_lit_uint32(0xd6543d), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(163), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x00),
    kcg_lit_int32(0), { kcg_lit_uint32(0x7dcadb97), kcg_lit_uint32(0xc8626df4),
      kcg_lit_uint32(0x41c922a4), kcg_lit_uint32(0x5b837edd), kcg_lit_uint32(
        0x1736d38a), kcg_lit_uint32(0xb2e9f763), kcg_lit_uint32(0xd4f5fd9),
      kcg_lit_uint32(0xbccce1a6), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } } };

/* test::ietf_RFC6234::test_hmac256/ */
const _7_array test_hmac256_test_ietf_RFC6234 = { { { { kcg_lit_uint32(
          0x54206948), kcg_lit_uint32(0x65726568), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(8), { { kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b),
        kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(
          0xb0b0b0b), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(20), kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(
        0x614c34b0), kcg_lit_uint32(0x5338dbd8), kcg_lit_uint32(0xceafa85c),
      kcg_lit_uint32(0x2bf10baf), kcg_lit_uint32(0xc21d88), kcg_lit_uint32(
        0xa73d83c9), kcg_lit_uint32(0x6c37e926), kcg_lit_uint32(0xf7cf322e),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(0x74616877), kcg_lit_uint32(
          0x206f6420), kcg_lit_uint32(0x77206179), kcg_lit_uint32(0x20746e61),
        kcg_lit_uint32(0x20726f66), kcg_lit_uint32(0x68746f6e), kcg_lit_uint32(
          0x3f676e69), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(28), { { kcg_lit_uint32(0x6566654a), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(4),
    kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(0x46c1dc5b),
      kcg_lit_uint32(0x4e7560bf), kcg_lit_uint32(0x2624046a), kcg_lit_uint32(
        0xc7759508), kcg_lit_uint32(0x83f005a), kcg_lit_uint32(0x8339279d),
      kcg_lit_uint32(0xb958ec9d), kcg_lit_uint32(0x4338ec64), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { {
        kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(
          0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd),
        kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(
          0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd),
        kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(
          0xdddd), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(50), { { kcg_lit_uint32(
          0xaaaaaaaa), kcg_lit_uint32(0xaaaaaaaa), kcg_lit_uint32(0xaaaaaaaa),
        kcg_lit_uint32(0xaaaaaaaa), kcg_lit_uint32(0xaaaaaaaa), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(20),
    kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(0x1ea93e77),
      kcg_lit_uint32(0x460e8036), kcg_lit_uint32(0xebb84d85), kcg_lit_uint32(
        0xa78191d0), kcg_lit_uint32(0x8b095929), kcg_lit_uint32(0x22c1f83e),
      kcg_lit_uint32(0x145563d9), kcg_lit_uint32(0xfe65d5ce), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { {
        kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(
          0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd),
        kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(
          0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd),
        kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(
          0xcdcd), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(50), { { kcg_lit_uint32(0x4030201),
        kcg_lit_uint32(0x8070605), kcg_lit_uint32(0xc0b0a09), kcg_lit_uint32(
          0x100f0e0d), kcg_lit_uint32(0x14131211), kcg_lit_uint32(0x18171615),
        kcg_lit_uint32(0x19), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(25),
    kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(0x388a5582),
      kcg_lit_uint32(0xe3c449a), kcg_lit_uint32(0x9881cca4), kcg_lit_uint32(
        0x3a08f299), kcg_lit_uint32(0xa3faf085), kcg_lit_uint32(0x7f878e5),
      kcg_lit_uint32(0xf43f2e7a), kcg_lit_uint32(0x5b662967), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } } };

/* test::ietf_RFC6234::test_hmac512/ */
const _7_array test_hmac512_test_ietf_RFC6234 = { { { { kcg_lit_uint32(
          0x54206948), kcg_lit_uint32(0x65726568), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(8), { { kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b),
        kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(0xb0b0b0b), kcg_lit_uint32(
          0xb0b0b0b), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(20), kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(
        0xde7caa87), kcg_lit_uint32(0x9d61efa5), kcg_lit_uint32(0x24b4f04f),
      kcg_lit_uint32(0xb06c1d1a), kcg_lit_uint32(0xe2f47923), kcg_lit_uint32(
        0x78c24ece), kcg_lit_uint32(0x5b3d07a), kcg_lit_uint32(0xde7ce145),
      kcg_lit_uint32(0xb733a8da), kcg_lit_uint32(0x2a7b8d6), kcg_lit_uint32(
        0x4e278b03), kcg_lit_uint32(0xe4f4a3ae), kcg_lit_uint32(0x4e919dbe),
      kcg_lit_uint32(0x70f161eb), kcg_lit_uint32(0x206c692e), kcg_lit_uint32(
        0x5468123a) } }, { { { kcg_lit_uint32(0x74616877), kcg_lit_uint32(
          0x206f6420), kcg_lit_uint32(0x77206179), kcg_lit_uint32(0x20746e61),
        kcg_lit_uint32(0x20726f66), kcg_lit_uint32(0x68746f6e), kcg_lit_uint32(
          0x3f676e69), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(28), { { kcg_lit_uint32(0x6566654a), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(4),
    kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(0x7b7a4b16),
      kcg_lit_uint32(0xe219f8fc), kcg_lit_uint32(0xe7fb95e3), kcg_lit_uint32(
        0xa3e0563b), kcg_lit_uint32(0x2264bd87), kcg_lit_uint32(0xd61f832e),
      kcg_lit_uint32(0xd70c2710), kcg_lit_uint32(0x540525ea), kcg_lit_uint32(
        0x75bf5897), kcg_lit_uint32(0x4a995ac0), kcg_lit_uint32(0x654f036d),
      kcg_lit_uint32(0xfde6f0f8), kcg_lit_uint32(0xa3b1eaca), kcg_lit_uint32(
        0x4b6b4a4d), kcg_lit_uint32(0xa076e63), kcg_lit_uint32(0x37e7bc38) } },
  { { { kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(
          0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd),
        kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(
          0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd),
        kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(0xdddddddd), kcg_lit_uint32(
          0xdddd), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(50), { { kcg_lit_uint32(
          0xaaaaaaaa), kcg_lit_uint32(0xaaaaaaaa), kcg_lit_uint32(0xaaaaaaaa),
        kcg_lit_uint32(0xaaaaaaaa), kcg_lit_uint32(0xaaaaaaaa), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(20),
    kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(0x8b073fa),
      kcg_lit_uint32(0x84a2569d), kcg_lit_uint32(0x75f0b0ef), kcg_lit_uint32(
        0xe90b896c), kcg_lit_uint32(0xdddbb5b1), kcg_lit_uint32(0x361ae88e),
      kcg_lit_uint32(0x333ef855), kcg_lit_uint32(0x399d27b2), kcg_lit_uint32(
        0x82843ebf), kcg_lit_uint32(0xc822a779), kcg_lit_uint32(0xa485b406),
      kcg_lit_uint32(0x7c8677e), kcg_lit_uint32(0x37a346b9), kcg_lit_uint32(
        0x2694e8be), kcg_lit_uint32(0x59882774), kcg_lit_uint32(0xfb9232e1) } },
  { { { kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(
          0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd),
        kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(
          0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd),
        kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(0xcdcdcdcd), kcg_lit_uint32(
          0xcdcd), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(50), { { kcg_lit_uint32(0x4030201),
        kcg_lit_uint32(0x8070605), kcg_lit_uint32(0xc0b0a09), kcg_lit_uint32(
          0x100f0e0d), kcg_lit_uint32(0x14131211), kcg_lit_uint32(0x18171615),
        kcg_lit_uint32(0x19), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(25),
    kcg_lit_uint32(0), kcg_lit_int32(0), { kcg_lit_uint32(0x5646bab0),
      kcg_lit_uint32(0x698c4537), kcg_lit_uint32(0xc5a8e590), kcg_lit_uint32(
        0xf74a1df6), kcg_lit_uint32(0x7fd976e5), kcg_lit_uint32(0x2d874bf9),
      kcg_lit_uint32(0x50806fe7), kcg_lit_uint32(0xdbe31e36), kcg_lit_uint32(
        0xc1a51ca9), kcg_lit_uint32(0xb45ea21a), kcg_lit_uint32(0x5c2779d6),
      kcg_lit_uint32(0x638078c5), kcg_lit_uint32(0x4197f1a5), kcg_lit_uint32(
        0x2d4f0c12), kcg_lit_uint32(0xebebade2), kcg_lit_uint32(
        0xdd98a210) } } };

/* test::ietf_RFC6234::test_sha512/ */
const _4_array test_sha512_test_ietf_RFC6234 = { { { { kcg_lit_uint32(0x636261),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(3), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x00),
    kcg_lit_int32(0), { kcg_lit_uint32(0xa135afdd), kcg_lit_uint32(0xba7a6193),
      kcg_lit_uint32(0x497341cc), kcg_lit_uint32(0x314120ae), kcg_lit_uint32(
        0x4efae612), kcg_lit_uint32(0xa27ea989), kcg_lit_uint32(0xe6ee9e0a),
      kcg_lit_uint32(0x9ad3554b), kcg_lit_uint32(0x2a999221), kcg_lit_uint32(
        0xa8c14f27), kcg_lit_uint32(0x233cba36), kcg_lit_uint32(0xbdebfea3),
      kcg_lit_uint32(0x23444d45), kcg_lit_uint32(0xee83c64), kcg_lit_uint32(
        0x4fc99a2a), kcg_lit_uint32(0x9fa44ca5) } }, { { { kcg_lit_uint32(
          0x64636261), kcg_lit_uint32(0x68676665), kcg_lit_uint32(0x65646362),
        kcg_lit_uint32(0x69686766), kcg_lit_uint32(0x66656463), kcg_lit_uint32(
          0x6a696867), kcg_lit_uint32(0x67666564), kcg_lit_uint32(0x6b6a6968),
        kcg_lit_uint32(0x68676665), kcg_lit_uint32(0x6c6b6a69), kcg_lit_uint32(
          0x69686766), kcg_lit_uint32(0x6d6c6b6a), kcg_lit_uint32(0x6a696867),
        kcg_lit_uint32(0x6e6d6c6b), kcg_lit_uint32(0x6b6a6968), kcg_lit_uint32(
          0x6f6e6d6c) }, { kcg_lit_uint32(0x6c6b6a69), kcg_lit_uint32(
          0x706f6e6d), kcg_lit_uint32(0x6d6c6b6a), kcg_lit_uint32(0x71706f6e),
        kcg_lit_uint32(0x6e6d6c6b), kcg_lit_uint32(0x7271706f), kcg_lit_uint32(
          0x6f6e6d6c), kcg_lit_uint32(0x73727170), kcg_lit_uint32(0x706f6e6d),
        kcg_lit_uint32(0x74737271), kcg_lit_uint32(0x71706f6e), kcg_lit_uint32(
          0x75747372), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(112), { { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(0),
    kcg_lit_uint32(0x00), kcg_lit_int32(0), { kcg_lit_uint32(0x759b958e),
      kcg_lit_uint32(0xda13e3da), kcg_lit_uint32(0x28f7f48c), kcg_lit_uint32(
        0x3f14fc14), kcg_lit_uint32(0xc679778f), kcg_lit_uint32(0xa17f9feb),
      kcg_lit_uint32(0xadae9972), kcg_lit_uint32(0x189088b6), kcg_lit_uint32(
        0x9e281d50), kcg_lit_uint32(0xe4f70049), kcg_lit_uint32(0xde991b33),
      kcg_lit_uint32(0x3a43b5c4), kcg_lit_uint32(0xee29d3c7), kcg_lit_uint32(
        0x5426ddb6), kcg_lit_uint32(0x5be5965e), kcg_lit_uint32(0x9e94b87) } },
  { { { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534) }, { kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534),
        kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(
          0x33323130), kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130),
        kcg_lit_uint32(0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(
          0x37363534), kcg_lit_uint32(0x33323130), kcg_lit_uint32(0x37363534) },
      { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(640), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), kcg_lit_uint32(0x00), kcg_lit_int32(0), { kcg_lit_uint32(
        0xa65bd089), kcg_lit_uint32(0xc399c632), kcg_lit_uint32(0xd4de3112),
      kcg_lit_uint32(0xd527c1ff), kcg_lit_uint32(0xd4da94a8), kcg_lit_uint32(
        0x24e0c012), kcg_lit_uint32(0x1a2d87db), kcg_lit_uint32(0x14a82bbd),
      kcg_lit_uint32(0x7850f1a), kcg_lit_uint32(0xe2e19b2a), kcg_lit_uint32(
        0x33cf04aa), kcg_lit_uint32(0x51cb65c7), kcg_lit_uint32(0x9ca31308),
      kcg_lit_uint32(0x4a4ca8d5), kcg_lit_uint32(0x3f4da6ca), kcg_lit_uint32(
        0xe9bab73f) } }, { { { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), kcg_lit_uint32(0xb0), kcg_lit_int32(5), { kcg_lit_uint32(
        0xa929eed4), kcg_lit_uint32(0x448509e9), kcg_lit_uint32(0xf13c916b),
      kcg_lit_uint32(0x836c37d1), kcg_lit_uint32(0xc1e24b6f), kcg_lit_uint32(
        0xa0ad3ccf), kcg_lit_uint32(0xf46b0a72), kcg_lit_uint32(0x6a887d85),
      kcg_lit_uint32(0x4e3ccb7e), kcg_lit_uint32(0xc7a80f4c), kcg_lit_uint32(
        0xe41452f9), kcg_lit_uint32(0xd2b0c11d), kcg_lit_uint32(0x4ca8221b),
      kcg_lit_uint32(0xcef83bc0), kcg_lit_uint32(0x4df34548), kcg_lit_uint32(
        0xd4babdd5) } }, { { { kcg_lit_uint32(0xd0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(1), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), kcg_lit_uint32(0x00), kcg_lit_int32(0), { kcg_lit_uint32(
        0x29209299), kcg_lit_uint32(0xe782e838), kcg_lit_uint32(0xb6f6203e),
      kcg_lit_uint32(0xa7a0689e), kcg_lit_uint32(0x42909014), kcg_lit_uint32(
        0x1bc8933d), kcg_lit_uint32(0x67213fab), kcg_lit_uint32(0xeece4a8d),
      kcg_lit_uint32(0x8c4e0ee5), kcg_lit_uint32(0xc8a4adaf), kcg_lit_uint32(
        0x83ea545a), kcg_lit_uint32(0x4a6c8206), kcg_lit_uint32(0xec4ce7d6),
      kcg_lit_uint32(0xfa1b63e9), kcg_lit_uint32(0x4a9b548a), kcg_lit_uint32(
        0x15bafbb3) } }, { { { kcg_lit_uint32(0x2eb5ec08), kcg_lit_uint32(
          0x42f7e1ba), kcg_lit_uint32(0xcd2bb62d), kcg_lit_uint32(0x702654),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(15), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x80),
    kcg_lit_int32(3), { kcg_lit_uint32(0x8ec78ded), kcg_lit_uint32(0x97b6018b),
      kcg_lit_uint32(0xbb3d0550), kcg_lit_uint32(0xda9e0a7a), kcg_lit_uint32(
        0xd2e9b90f), kcg_lit_uint32(0x71edb192), kcg_lit_uint32(0xfea7805e),
      kcg_lit_uint32(0x164e0a29), kcg_lit_uint32(0x13d94f66), kcg_lit_uint32(
        0x405458e8), kcg_lit_uint32(0x5ef05a0c), kcg_lit_uint32(0x6b31ad6d),
      kcg_lit_uint32(0x3eb45973), kcg_lit_uint32(0xc3bef864), kcg_lit_uint32(
        0x1137f2c1), kcg_lit_uint32(0xb6bb8699) } }, { { { kcg_lit_uint32(
          0xe3c4e8d), kcg_lit_uint32(0x14198938), kcg_lit_uint32(0x9d6e8191),
        kcg_lit_uint32(0xa0f0bf98), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(16), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), kcg_lit_uint32(0x00), kcg_lit_int32(0), { kcg_lit_uint32(
        0xa4670bcb), kcg_lit_uint32(0xd72c71b8), kcg_lit_uint32(0xc0ab9a3c),
      kcg_lit_uint32(0x26e999b1), kcg_lit_uint32(0x4a84209b), kcg_lit_uint32(
        0xbdac75fb), kcg_lit_uint32(0xc953c1d1), kcg_lit_uint32(0xc3248982),
      kcg_lit_uint32(0xfeaaeddd), kcg_lit_uint32(0xdd5f9c66), kcg_lit_uint32(
        0x636fc60b), kcg_lit_uint32(0x9873670f), kcg_lit_uint32(0x1beb1382),
      kcg_lit_uint32(0xad17f516), kcg_lit_uint32(0xf0b2e40d), kcg_lit_uint32(
        0xf8905cc9) } }, { { { kcg_lit_uint32(0x85ecdd3a), kcg_lit_uint32(
          0xd1163259), kcg_lit_uint32(0x2da09a61), kcg_lit_uint32(0xb975697),
        kcg_lit_uint32(0xe2ac70fc), kcg_lit_uint32(0x6b7c4f74), kcg_lit_uint32(
          0x10158827), kcg_lit_uint32(0xa2b6f728), kcg_lit_uint32(0x4ad70f55),
        kcg_lit_uint32(0xc2696e7e), kcg_lit_uint32(0xc45fb4c9), kcg_lit_uint32(
          0xc36d9654), kcg_lit_uint32(0xda102e1d), kcg_lit_uint32(0x2ce951f),
        kcg_lit_uint32(0x87bfb4be), kcg_lit_uint32(0xbd4c5765) }, {
        kcg_lit_uint32(0xef37836e), kcg_lit_uint32(0x98dc0a42), kcg_lit_uint32(
          0xd5b65cc1), kcg_lit_uint32(0x1b24a0e4), kcg_lit_uint32(0x256d04a0),
        kcg_lit_uint32(0x3102510e), kcg_lit_uint32(0x6c04c2ca), kcg_lit_uint32(
          0xab061699), kcg_lit_uint32(0x5b14e44e), kcg_lit_uint32(0xbbf42fee),
        kcg_lit_uint32(0x49ab3a12), kcg_lit_uint32(0x79449d8d), kcg_lit_uint32(
          0xadcc994f), kcg_lit_uint32(0x62a1a989), kcg_lit_uint32(0xa7ed5912),
        kcg_lit_uint32(0xd46d5b0a) }, { kcg_lit_uint32(0x7877d8bd),
        kcg_lit_uint32(0x933b04c9), kcg_lit_uint32(0x649f584), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(140), { {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(0), kcg_lit_uint32(0x80),
    kcg_lit_int32(3), { kcg_lit_uint32(0xfc76ba32), kcg_lit_uint32(0x20a0ea30),
      kcg_lit_uint32(0xff50eb8a), kcg_lit_uint32(0x6418afb5), kcg_lit_uint32(
        0x9017bffd), kcg_lit_uint32(0xa6c04d2a), kcg_lit_uint32(0xce1fc682),
      kcg_lit_uint32(0x782bd9a6), kcg_lit_uint32(0x10b26732), kcg_lit_uint32(
        0x37183080), kcg_lit_uint32(0x9ce79df5), kcg_lit_uint32(0xb27d336b),
      kcg_lit_uint32(0xa8a6f52), kcg_lit_uint32(0x535e0e51), kcg_lit_uint32(
        0x35d4feca), kcg_lit_uint32(0xf1c2e75f) } }, { { { kcg_lit_uint32(
          0xc4205fa5), kcg_lit_uint32(0x32d1aa11), kcg_lit_uint32(0x2d507a80),
        kcg_lit_uint32(0x314e8265), kcg_lit_uint32(0x325430a2), kcg_lit_uint32(
          0xd3063daa), kcg_lit_uint32(0xd8a882e2), kcg_lit_uint32(0xdee10d4e),
        kcg_lit_uint32(0x49bf7469), kcg_lit_uint32(0x7ffc6954), kcg_lit_uint32(
          0x54808f33), kcg_lit_uint32(0xc4268cd5), kcg_lit_uint32(0xe8c36093),
        kcg_lit_uint32(0x2365f57a), kcg_lit_uint32(0x9dd8f6ac), kcg_lit_uint32(
          0xf26fe503) }, { kcg_lit_uint32(0x2b0068f8), kcg_lit_uint32(
          0xed31e4c3), kcg_lit_uint32(0xf0f24dc4), kcg_lit_uint32(0xb34b3d22),
        kcg_lit_uint32(0x6e5843b2), kcg_lit_uint32(0x49927d1a), kcg_lit_uint32(
          0xcb4f6936), kcg_lit_uint32(0x958df8ba), kcg_lit_uint32(0x50ebe419),
        kcg_lit_uint32(0xe4f844a6), kcg_lit_uint32(0xeab05ef9), kcg_lit_uint32(
          0x6544bc95), kcg_lit_uint32(0xac1a82c8), kcg_lit_uint32(0xab15fed2),
        kcg_lit_uint32(0x4b168149), kcg_lit_uint32(0x2fc36dbb) }, {
        kcg_lit_uint32(0xa1879096), kcg_lit_uint32(0xccd9b045), kcg_lit_uint32(
          0x2bc2679c), kcg_lit_uint32(0x41993276), kcg_lit_uint32(0x8b12c49c),
        kcg_lit_uint32(0xb377a0e9), kcg_lit_uint32(0x634e6ac), kcg_lit_uint32(
          0x28996d4e), kcg_lit_uint32(0x6dc1335), kcg_lit_uint32(0xd5d51e7),
        kcg_lit_uint32(0x9a2e1373), kcg_lit_uint32(0xb1d3c60d), kcg_lit_uint32(
          0xf146b2f8), kcg_lit_uint32(0xc73f8aa9), kcg_lit_uint32(0xe3b14129),
        kcg_lit_uint32(0xe89820bb) }, { kcg_lit_uint32(0x68f216bf),
        kcg_lit_uint32(0xf0b4fd6), kcg_lit_uint32(0x1efe0747), kcg_lit_uint32(
          0x1b79a1a1), kcg_lit_uint32(0xc7c0f3a2), kcg_lit_uint32(0x51f5e558),
        kcg_lit_uint32(0xc9963a86), kcg_lit_uint32(0xd747ad49), kcg_lit_uint32(
          0xd240fb), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(227), { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), kcg_lit_uint32(0x00), kcg_lit_int32(0), { kcg_lit_uint32(
        0xfbbe65c6), kcg_lit_uint32(0x9d18da36), kcg_lit_uint32(0x102d8278),
      kcg_lit_uint32(0x3bbf8c52), kcg_lit_uint32(0xf7eeb312), kcg_lit_uint32(
        0x9990326), kcg_lit_uint32(0x276aa1c1), kcg_lit_uint32(0x9371480d),
      kcg_lit_uint32(0x956b9677), kcg_lit_uint32(0x728e877a), kcg_lit_uint32(
        0x9a778405), kcg_lit_uint32(0x185c8262), kcg_lit_uint32(0x5e4126da),
      kcg_lit_uint32(0x6a17a749), kcg_lit_uint32(0x10754e89), kcg_lit_uint32(
        0xf55114fd) } } };

/* test::ietf_RFC7693::test_blake2s_keyed/ */
const testcase_test_ietf_RFC7693 test_blake2s_keyed_test_ietf_RFC7693 = { { {
      kcg_lit_uint32(0x00000001), kcg_lit_uint32(0x00000001), kcg_lit_uint32(
        0x9292e0a4), kcg_lit_uint32(0x78c251b6), kcg_lit_uint32(0x562c77b9),
      kcg_lit_uint32(0xbba95f9f), kcg_lit_uint32(0xb406d913), kcg_lit_uint32(
        0x9d8cb66a), kcg_lit_uint32(0x442bdcf9), kcg_lit_uint32(0x09a2f809),
      kcg_lit_uint32(0xa5d4ad85), kcg_lit_uint32(0x92493304), kcg_lit_uint32(
        0xdb0fb034), kcg_lit_uint32(0x73eda27a), kcg_lit_uint32(0x05efd7af),
      kcg_lit_uint32(0x9c84343b) }, { kcg_lit_uint32(0x07b6e5ed),
      kcg_lit_uint32(0xfb4fbbd8), kcg_lit_uint32(0xa1cbfd1b), kcg_lit_uint32(
        0xc0b66caf), kcg_lit_uint32(0xa4ef6a75), kcg_lit_uint32(0xcf581bab),
      kcg_lit_uint32(0xe5205e39), kcg_lit_uint32(0xbfbee35b), kcg_lit_uint32(
        0xfd0db384), kcg_lit_uint32(0x1d3646ca), kcg_lit_uint32(0x865c739b),
      kcg_lit_uint32(0xa1cca03d), kcg_lit_uint32(0xd1f4a71d), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(116), {
    kcg_lit_uint32(0x64dbf39e), kcg_lit_uint32(0x779f1fa6), kcg_lit_uint32(
      0x90dc780c), kcg_lit_uint32(0x9c1d5bd1), kcg_lit_uint32(0xae939c42),
    kcg_lit_uint32(0x5c3d2c6a), kcg_lit_uint32(0x4a66db8d), kcg_lit_uint32(
      0x1ba780fd), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) }, kcg_lit_int32(32), { kcg_lit_uint32(0x17d76576),
    kcg_lit_uint32(0x1722a0f1), kcg_lit_uint32(0xa0572c96), kcg_lit_uint32(
      0xf78b15d1), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) } };

/* test::ietf_RFC7693::test_hkdf/ */
const testcase_hkdf_test_ietf_RFC7693 test_hkdf_test_ietf_RFC7693 = { { {
      kcg_lit_uint32(0x61b31122), kcg_lit_uint32(0x66c51a08), kcg_lit_uint32(
        0xdb431269), kcg_lit_uint32(0x32d58a45), kcg_lit_uint32(0x666c9c2d),
      kcg_lit_uint32(0xb7e89322), kcg_lit_uint32(0x659ce10e), kcg_lit_uint32(
        0xf39e07ba), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
  kcg_lit_int32(32), { kcg_lit_uint32(0xae6de260), kcg_lit_uint32(0xc0ef27f3),
    kcg_lit_uint32(0xe235c32e), kcg_lit_uint32(0xd0d225a0), kcg_lit_uint32(
      0x0642eb16), kcg_lit_uint32(0xf57772f8), kcg_lit_uint32(0x98d1382d),
    kcg_lit_uint32(0x36cd788b), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0) }, { { kcg_lit_uint32(0x180108e2),
      kcg_lit_uint32(0xda9f6650), kcg_lit_uint32(0x2ea82f85), kcg_lit_uint32(
        0x97d7ab0c), kcg_lit_uint32(0x69334445), kcg_lit_uint32(0x8f88323d),
      kcg_lit_uint32(0xcd61482a), kcg_lit_uint32(0xa7657ab4), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
      kcg_lit_uint32(0x705bd9f3), kcg_lit_uint32(0xb1455265), kcg_lit_uint32(
        0x51bbba92), kcg_lit_uint32(0x9fdfc6a5), kcg_lit_uint32(0x8d904eb8),
      kcg_lit_uint32(0x24139c13), kcg_lit_uint32(0x84679d87), kcg_lit_uint32(
        0x25aa8a62), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0x93b4f9af), kcg_lit_uint32(
        0xf4ff51b7), kcg_lit_uint32(0xa93d7e24), kcg_lit_uint32(0x6b14a912),
      kcg_lit_uint32(0x70f562c8), kcg_lit_uint32(0xaef5cbe9), kcg_lit_uint32(
        0x66896dba), kcg_lit_uint32(0x6b39c865), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } } };

/* test::ietf_RFC7693::exp_hkdf3/ */
const array_uint32_16_3 exp_hkdf3_test_ietf_RFC7693 = { { kcg_lit_uint32(
      0x180108e2), kcg_lit_uint32(0xda9f6650), kcg_lit_uint32(0x2ea82f85),
    kcg_lit_uint32(0x97d7ab0c), kcg_lit_uint32(0x69334445), kcg_lit_uint32(
      0x8f88323d), kcg_lit_uint32(0xcd61482a), kcg_lit_uint32(0xa7657ab4),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { kcg_lit_uint32(0x705bd9f3), kcg_lit_uint32(0xb1455265),
    kcg_lit_uint32(0x51bbba92), kcg_lit_uint32(0x9fdfc6a5), kcg_lit_uint32(
      0x8d904eb8), kcg_lit_uint32(0x24139c13), kcg_lit_uint32(0x84679d87),
    kcg_lit_uint32(0x25aa8a62), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0x93b4f9af),
    kcg_lit_uint32(0xf4ff51b7), kcg_lit_uint32(0xa93d7e24), kcg_lit_uint32(
      0x6b14a912), kcg_lit_uint32(0x70f562c8), kcg_lit_uint32(0xaef5cbe9),
    kcg_lit_uint32(0x66896dba), kcg_lit_uint32(0x6b39c865), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } };

/* test::ietf_RFC7693::test_blake2s/ */
const testcase_test_ietf_RFC7693 test_blake2s_test_ietf_RFC7693 = { { {
      kcg_lit_uint32(0x00636261), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) } }, kcg_lit_int32(3), { kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, kcg_lit_int32(0),
  { kcg_lit_uint32(0x8C5E8C50), kcg_lit_uint32(0xE2147C32), kcg_lit_uint32(
      0xA32BA7E1), kcg_lit_uint32(0x2F45EB4E), kcg_lit_uint32(0x208B4537),
    kcg_lit_uint32(0x293AD69E), kcg_lit_uint32(0x4C9B994D), kcg_lit_uint32(
      0x82596786), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) } };

/* test::stc/ */
const array stc_test = { { { { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(0), { kcg_lit_uint32(0x9db1619d), kcg_lit_uint32(0x605afdef),
      kcg_lit_uint32(0xf44a84ba), kcg_lit_uint32(0xc42cec92), kcg_lit_uint32(
        0x69c54944), kcg_lit_uint32(0x1969327b), kcg_lit_uint32(0x3ac3b70),
      kcg_lit_uint32(0x607fae1c), kcg_lit_uint32(0x1985ad7), kcg_lit_uint32(
        0xb70ab182), kcg_lit_uint32(0xd3fe4bd5), kcg_lit_uint32(0x3a0764c9),
      kcg_lit_uint32(0xf372e10e), kcg_lit_uint32(0x2523a6da), kcg_lit_uint32(
        0x681a02af), kcg_lit_uint32(0x1a5107f7) } }, { { { kcg_lit_uint32(0x72),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(1), { kcg_lit_uint32(0x9b08cd4c),
      kcg_lit_uint32(0xda96ff28), kcg_lit_uint32(0x46c3b69d), kcg_lit_uint32(
        0xf4e11ec), kcg_lit_uint32(0x9f318a5b), kcg_lit_uint32(0x24a6ab35),
      kcg_lit_uint32(0xedf68cda), kcg_lit_uint32(0xfba6b84f), kcg_lit_uint32(
        0xc317403d), kcg_lit_uint32(0x5a8943e8), kcg_lit_uint32(0xa70ab792),
      kcg_lit_uint32(0xbc7e1b4d), kcg_lit_uint32(0xcf2c989c), kcg_lit_uint32(
        0x8c96c42e), kcg_lit_uint32(0xf155cdc0), kcg_lit_uint32(0xc66f42a) } },
  { { { kcg_lit_uint32(0x82af), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(2), {
      kcg_lit_uint32(0xf48daac5), kcg_lit_uint32(0x7b839f3f), kcg_lit_uint32(
        0x2f44b7ed), kcg_lit_uint32(0xb1b7dc31), kcg_lit_uint32(0x3585d366),
      kcg_lit_uint32(0x4b096f07), kcg_lit_uint32(0x2e3ace85), kcg_lit_uint32(
        0xf758440b), kcg_lit_uint32(0x8ecd51fc), kcg_lit_uint32(0xa3a11862),
      kcg_lit_uint32(0xd07ea48d), kcg_lit_uint32(0x58f03002), kcg_lit_uint32(
        0x13ed1608), kcg_lit_uint32(0xac0333ba), kcg_lit_uint32(0x1591eb5d),
      kcg_lit_uint32(0x25809048) } }, { { { kcg_lit_uint32(0x7bc7cb),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(3), { kcg_lit_uint32(0xb0054a0d),
      kcg_lit_uint32(0x43a55273), kcg_lit_uint32(0x5603186e), kcg_lit_uint32(
        0xefe60ada), kcg_lit_uint32(0xf75f34a0), kcg_lit_uint32(0x577215fb),
      kcg_lit_uint32(0xe87257), kcg_lit_uint32(0xe978d95e), kcg_lit_uint32(
        0x5b181ae6), kcg_lit_uint32(0x3a61f2ce), kcg_lit_uint32(0x97b77c6c),
      kcg_lit_uint32(0x5d94ce63), kcg_lit_uint32(0x765d243b), kcg_lit_uint32(
        0x40d44d11), kcg_lit_uint32(0xdcf2f5bc), kcg_lit_uint32(0x5770a51a) } },
  { { { kcg_lit_uint32(0x89894c5f), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(4), {
      kcg_lit_uint32(0xc34f96d), kcg_lit_uint32(0x88c18c13), kcg_lit_uint32(
        0x6444feb5), kcg_lit_uint32(0x7f3faaeb), kcg_lit_uint32(0xd5a206c2),
      kcg_lit_uint32(0x7034345c), kcg_lit_uint32(0xfcc9747e), kcg_lit_uint32(
        0xbb0ee204), kcg_lit_uint32(0x2c1dac0), kcg_lit_uint32(0x863153c4),
      kcg_lit_uint32(0x31c45de2), kcg_lit_uint32(0x53234728), kcg_lit_uint32(
        0x87dbabea), kcg_lit_uint32(0xeb2a158b), kcg_lit_uint32(0x921f008e),
      kcg_lit_uint32(0xa73302d9) } }, { { { kcg_lit_uint32(0xc0beb618),
        kcg_lit_uint32(0x97), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) } }, kcg_lit_int32(5), { kcg_lit_uint32(
        0x1a3880b7), kcg_lit_uint32(0xb7f8ed65), kcg_lit_uint32(0xe845698f),
      kcg_lit_uint32(0x4179ecdb), kcg_lit_uint32(0xd49f04ac), kcg_lit_uint32(
        0xcf4010c6), kcg_lit_uint32(0x5743320c), kcg_lit_uint32(0x3c295a97),
      kcg_lit_uint32(0x7af53e2), kcg_lit_uint32(0x864b8066), kcg_lit_uint32(
        0x5b59b19b), kcg_lit_uint32(0x535b76e9), kcg_lit_uint32(0xaabb8648),
      kcg_lit_uint32(0xf55b30b8), kcg_lit_uint32(0x897fbc0d), kcg_lit_uint32(
        0x15ffb9b) } }, { { { kcg_lit_uint32(0x850d0189), kcg_lit_uint32(
          0x7259), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) } }, kcg_lit_int32(6), { kcg_lit_uint32(0xff9eae78),
      kcg_lit_uint32(0xe945f2e6), kcg_lit_uint32(0x63bea724), kcg_lit_uint32(
        0xeb461104), kcg_lit_uint32(0xd3db70c6), kcg_lit_uint32(0x67ba0c06),
      kcg_lit_uint32(0x6f21c6fb), kcg_lit_uint32(0x4645c4eb), kcg_lit_uint32(
        0xa4bfcffb), kcg_lit_uint32(0xf2d70505), kcg_lit_uint32(0x334a44be),
      kcg_lit_uint32(0x54cc85d1), kcg_lit_uint32(0x52616de1), kcg_lit_uint32(
        0xb64e160), kcg_lit_uint32(0xb887502b), kcg_lit_uint32(0x3d64e33e) } },
  { { { kcg_lit_uint32(0x81f3a8b4), kcg_lit_uint32(0x7a0ee7), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(7), {
      kcg_lit_uint32(0xbf651869), kcg_lit_uint32(0x4b1e2ac8), kcg_lit_uint32(
        0xdeec4e57), kcg_lit_uint32(0x919754c), kcg_lit_uint32(0xf80caf3f),
      kcg_lit_uint32(0x34023867), kcg_lit_uint32(0x454666e3), kcg_lit_uint32(
        0x795f1cc6), kcg_lit_uint32(0xa3e3a598), kcg_lit_uint32(0xbaaa676e),
      kcg_lit_uint32(0xf08b8889), kcg_lit_uint32(0xd91ade93), kcg_lit_uint32(
        0x174e763), kcg_lit_uint32(0xbf02393b), kcg_lit_uint32(0x8b6d35ab),
      kcg_lit_uint32(0x638a1790) } }, { { { kcg_lit_uint32(0xc5ab8442),
        kcg_lit_uint32(0x3572b61b), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) } }, kcg_lit_int32(8), { kcg_lit_uint32(
        0x6f51263b), kcg_lit_uint32(0xeb88dcb3), kcg_lit_uint32(0xd79e1b18),
      kcg_lit_uint32(0x52cd0b3f), kcg_lit_uint32(0xc7b4d6bc), kcg_lit_uint32(
        0xafbce488), kcg_lit_uint32(0xd07f0546), kcg_lit_uint32(0x73e0be78),
      kcg_lit_uint32(0x4ab51ff8), kcg_lit_uint32(0xd9ce5f82), kcg_lit_uint32(
        0xaf33b05e), kcg_lit_uint32(0x403164cd), kcg_lit_uint32(0xafbab75),
      kcg_lit_uint32(0x70a920bd), kcg_lit_uint32(0x43032589), kcg_lit_uint32(
        0x63b8346f) } }, { { { kcg_lit_uint32(0x96f82b67), kcg_lit_uint32(
          0x51bc045d), kcg_lit_uint32(0x46), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(9), { kcg_lit_uint32(0xfbf5c6ed),
      kcg_lit_uint32(0x4dee1cdd), kcg_lit_uint32(0x35061c10), kcg_lit_uint32(
        0x9004a330), kcg_lit_uint32(0x68be21b2), kcg_lit_uint32(0xb0f536c0),
      kcg_lit_uint32(0x3b950f7d), kcg_lit_uint32(0x92f15d74), kcg_lit_uint32(
        0x669ca4c1), kcg_lit_uint32(0xeff917e6), kcg_lit_uint32(0xc46bc65e),
      kcg_lit_uint32(0xa34c56c6), kcg_lit_uint32(0xfba5e23d), kcg_lit_uint32(
        0x664145e), kcg_lit_uint32(0x626c6d2e), kcg_lit_uint32(
        0xfd5e1519) } } };

/* test::ietf_RFC7748::RFC7748_exp_k1000/ */
const array_uint32_8 RFC7748_exp_k1000_test_ietf_RFC7748 = { kcg_lit_uint32(
    2616544360), kcg_lit_uint32(1426666408), kcg_lit_uint32(1458503720),
  kcg_lit_uint32(1011691375), kcg_lit_uint32(3297196060), kcg_lit_uint32(
    2279825555), kcg_lit_uint32(1303981663), kcg_lit_uint32(1361859481) };

/* nacl::op::I/ */
const gf_nacl_op I_nacl_op = { kcg_lit_int64(0xa0b0), kcg_lit_int64(0x4a0e),
  kcg_lit_int64(0x1b27), kcg_lit_int64(0xc4ee), kcg_lit_int64(0xe478),
  kcg_lit_int64(0xad2f), kcg_lit_int64(0x1806), kcg_lit_int64(0x2f43),
  kcg_lit_int64(0xd7a7), kcg_lit_int64(0x3dfb), kcg_lit_int64(0x0099),
  kcg_lit_int64(0x2b4d), kcg_lit_int64(0xdf0b), kcg_lit_int64(0x4fc1),
  kcg_lit_int64(0x2480), kcg_lit_int64(0x2b83) };

/* nacl::op::D/ */
const gf_nacl_op D_nacl_op = { kcg_lit_int64(0x78a3), kcg_lit_int64(0x1359),
  kcg_lit_int64(0x4dca), kcg_lit_int64(0x75eb), kcg_lit_int64(0xd8ab),
  kcg_lit_int64(0x4141), kcg_lit_int64(0x0a4d), kcg_lit_int64(0x0070),
  kcg_lit_int64(0xe898), kcg_lit_int64(0x7779), kcg_lit_int64(0x4079),
  kcg_lit_int64(0x8cc7), kcg_lit_int64(0xfe73), kcg_lit_int64(0x2b6f),
  kcg_lit_int64(0x6cee), kcg_lit_int64(0x5203) };

/* nacl::op::D2/ */
const gf_nacl_op D2_nacl_op = { kcg_lit_int64(0xf159), kcg_lit_int64(0x26b2),
  kcg_lit_int64(0x9b94), kcg_lit_int64(0xebd6), kcg_lit_int64(0xb156),
  kcg_lit_int64(0x8283), kcg_lit_int64(0x149a), kcg_lit_int64(0x00e0),
  kcg_lit_int64(0xd130), kcg_lit_int64(0xeef3), kcg_lit_int64(0x80f2),
  kcg_lit_int64(0x198e), kcg_lit_int64(0xfce7), kcg_lit_int64(0x56df),
  kcg_lit_int64(0xd9dc), kcg_lit_int64(0x2406) };

/* nacl::op::Y/ */
const gf_nacl_op Y_nacl_op = { kcg_lit_int64(0x6658), kcg_lit_int64(0x6666),
  kcg_lit_int64(0x6666), kcg_lit_int64(0x6666), kcg_lit_int64(0x6666),
  kcg_lit_int64(0x6666), kcg_lit_int64(0x6666), kcg_lit_int64(0x6666),
  kcg_lit_int64(0x6666), kcg_lit_int64(0x6666), kcg_lit_int64(0x6666),
  kcg_lit_int64(0x6666), kcg_lit_int64(0x6666), kcg_lit_int64(0x6666),
  kcg_lit_int64(0x6666), kcg_lit_int64(0x6666) };

/* nacl::op::X/ */
const gf_nacl_op X_nacl_op = { kcg_lit_int64(0xd51a), kcg_lit_int64(0x8f25),
  kcg_lit_int64(0x2d60), kcg_lit_int64(0xc956), kcg_lit_int64(0xa7b2),
  kcg_lit_int64(0x9525), kcg_lit_int64(0xc760), kcg_lit_int64(0x692c),
  kcg_lit_int64(0xdc5c), kcg_lit_int64(0xfdd6), kcg_lit_int64(0xe231),
  kcg_lit_int64(0xc0a4), kcg_lit_int64(0x53fe), kcg_lit_int64(0xcd6e),
  kcg_lit_int64(0x36d3), kcg_lit_int64(0x2169) };

/* nacl::sign::L/ */
const array_int64_32 L_nacl_sign = { kcg_lit_int64(0xed), kcg_lit_int64(0xd3),
  kcg_lit_int64(0xf5), kcg_lit_int64(0x5c), kcg_lit_int64(0x1a), kcg_lit_int64(
    0x63), kcg_lit_int64(0x12), kcg_lit_int64(0x58), kcg_lit_int64(0xd6),
  kcg_lit_int64(0x9c), kcg_lit_int64(0xf7), kcg_lit_int64(0xa2), kcg_lit_int64(
    0xde), kcg_lit_int64(0xf9), kcg_lit_int64(0xde), kcg_lit_int64(0x14),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0x10) };

/* hash::sha512::K/ */
const array_uint64_16_5 K_hash_sha512 = { { kcg_lit_uint64(0x428a2f98d728ae22),
    kcg_lit_uint64(0x7137449123ef65cd), kcg_lit_uint64(0xb5c0fbcfec4d3b2f),
    kcg_lit_uint64(0xe9b5dba58189dbbc), kcg_lit_uint64(0x3956c25bf348b538),
    kcg_lit_uint64(0x59f111f1b605d019), kcg_lit_uint64(0x923f82a4af194f9b),
    kcg_lit_uint64(0xab1c5ed5da6d8118), kcg_lit_uint64(0xd807aa98a3030242),
    kcg_lit_uint64(0x12835b0145706fbe), kcg_lit_uint64(0x243185be4ee4b28c),
    kcg_lit_uint64(0x550c7dc3d5ffb4e2), kcg_lit_uint64(0x72be5d74f27b896f),
    kcg_lit_uint64(0x80deb1fe3b1696b1), kcg_lit_uint64(0x9bdc06a725c71235),
    kcg_lit_uint64(0xc19bf174cf692694) }, { kcg_lit_uint64(0xe49b69c19ef14ad2),
    kcg_lit_uint64(0xefbe4786384f25e3), kcg_lit_uint64(0x0fc19dc68b8cd5b5),
    kcg_lit_uint64(0x240ca1cc77ac9c65), kcg_lit_uint64(0x2de92c6f592b0275),
    kcg_lit_uint64(0x4a7484aa6ea6e483), kcg_lit_uint64(0x5cb0a9dcbd41fbd4),
    kcg_lit_uint64(0x76f988da831153b5), kcg_lit_uint64(0x983e5152ee66dfab),
    kcg_lit_uint64(0xa831c66d2db43210), kcg_lit_uint64(0xb00327c898fb213f),
    kcg_lit_uint64(0xbf597fc7beef0ee4), kcg_lit_uint64(0xc6e00bf33da88fc2),
    kcg_lit_uint64(0xd5a79147930aa725), kcg_lit_uint64(0x06ca6351e003826f),
    kcg_lit_uint64(0x142929670a0e6e70) }, { kcg_lit_uint64(0x27b70a8546d22ffc),
    kcg_lit_uint64(0x2e1b21385c26c926), kcg_lit_uint64(0x4d2c6dfc5ac42aed),
    kcg_lit_uint64(0x53380d139d95b3df), kcg_lit_uint64(0x650a73548baf63de),
    kcg_lit_uint64(0x766a0abb3c77b2a8), kcg_lit_uint64(0x81c2c92e47edaee6),
    kcg_lit_uint64(0x92722c851482353b), kcg_lit_uint64(0xa2bfe8a14cf10364),
    kcg_lit_uint64(0xa81a664bbc423001), kcg_lit_uint64(0xc24b8b70d0f89791),
    kcg_lit_uint64(0xc76c51a30654be30), kcg_lit_uint64(0xd192e819d6ef5218),
    kcg_lit_uint64(0xd69906245565a910), kcg_lit_uint64(0xf40e35855771202a),
    kcg_lit_uint64(0x106aa07032bbd1b8) }, { kcg_lit_uint64(0x19a4c116b8d2d0c8),
    kcg_lit_uint64(0x1e376c085141ab53), kcg_lit_uint64(0x2748774cdf8eeb99),
    kcg_lit_uint64(0x34b0bcb5e19b48a8), kcg_lit_uint64(0x391c0cb3c5c95a63),
    kcg_lit_uint64(0x4ed8aa4ae3418acb), kcg_lit_uint64(0x5b9cca4f7763e373),
    kcg_lit_uint64(0x682e6ff3d6b2b8a3), kcg_lit_uint64(0x748f82ee5defb2fc),
    kcg_lit_uint64(0x78a5636f43172f60), kcg_lit_uint64(0x84c87814a1f0ab72),
    kcg_lit_uint64(0x8cc702081a6439ec), kcg_lit_uint64(0x90befffa23631e28),
    kcg_lit_uint64(0xa4506cebde82bde9), kcg_lit_uint64(0xbef9a3f7b2c67915),
    kcg_lit_uint64(0xc67178f2e372532b) }, { kcg_lit_uint64(0xca273eceea26619c),
    kcg_lit_uint64(0xd186b8c721c0c207), kcg_lit_uint64(0xeada7dd6cde0eb1e),
    kcg_lit_uint64(0xf57d4f7fee6ed178), kcg_lit_uint64(0x06f067aa72176fba),
    kcg_lit_uint64(0x0a637dc5a2c898a6), kcg_lit_uint64(0x113f9804bef90dae),
    kcg_lit_uint64(0x1b710b35131c471b), kcg_lit_uint64(0x28db77f523047d84),
    kcg_lit_uint64(0x32caab7b40c72493), kcg_lit_uint64(0x3c9ebe0a15c9bebc),
    kcg_lit_uint64(0x431d67c49c100d4c), kcg_lit_uint64(0x4cc5d4becb3e42b6),
    kcg_lit_uint64(0x597f299cfc657e2a), kcg_lit_uint64(0x5fcb6fab3ad6faec),
    kcg_lit_uint64(0x6c44198c4a475817) } };

/* hash::sha512::IV/ */
const array_uint64_8 IV_hash_sha512 = { kcg_lit_uint64(0x6a09e667f3bcc908),
  kcg_lit_uint64(0xbb67ae8584caa73b), kcg_lit_uint64(0x3c6ef372fe94f82b),
  kcg_lit_uint64(0xa54ff53a5f1d36f1), kcg_lit_uint64(0x510e527fade682d1),
  kcg_lit_uint64(0x9b05688c2b3e6c1f), kcg_lit_uint64(0x1f83d9abfb41bd6b),
  kcg_lit_uint64(0x5be0cd19137e2179) };

/* test::ietf_RFC8032::tc/ */
const _2_array tc_test_ietf_RFC8032 = { { { { kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) } }, kcg_lit_int32(0), { kcg_lit_uint32(0x9db1619d),
      kcg_lit_uint32(0x605afdef), kcg_lit_uint32(0xf44a84ba), kcg_lit_uint32(
        0xc42cec92), kcg_lit_uint32(0x69c54944), kcg_lit_uint32(0x1969327b),
      kcg_lit_uint32(0x3ac3b70), kcg_lit_uint32(0x607fae1c), kcg_lit_uint32(
        0x1985ad7), kcg_lit_uint32(0xb70ab182), kcg_lit_uint32(0xd3fe4bd5),
      kcg_lit_uint32(0x3a0764c9), kcg_lit_uint32(0xf372e10e), kcg_lit_uint32(
        0x2523a6da), kcg_lit_uint32(0x681a02af), kcg_lit_uint32(0x1a5107f7) }, {
      kcg_lit_uint32(0x4356e5), kcg_lit_uint32(0x72ac60c3), kcg_lit_uint32(
        0xcce28690), kcg_lit_uint32(0x8a826e80), kcg_lit_uint32(0x1e7f8784),
      kcg_lit_uint32(0x74d9e5b8), kcg_lit_uint32(0x65e073d8), kcg_lit_uint32(
        0x55014922), kcg_lit_uint32(0x1582b85f), kcg_lit_uint32(0xac3ba390),
      kcg_lit_uint32(0x70391ec6), kcg_lit_uint32(0x6bb4f91c), kcg_lit_uint32(
        0xf0f55bd2), kcg_lit_uint32(0x24be5b59), kcg_lit_uint32(0x43415165),
      kcg_lit_uint32(0xb107a8e) } }, { { { kcg_lit_uint32(0x72), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) } }, kcg_lit_int32(1), { kcg_lit_uint32(0x9b08cd4c),
      kcg_lit_uint32(0xda96ff28), kcg_lit_uint32(0x46c3b69d), kcg_lit_uint32(
        0xf4e11ec), kcg_lit_uint32(0x9f318a5b), kcg_lit_uint32(0x24a6ab35),
      kcg_lit_uint32(0xedf68cda), kcg_lit_uint32(0xfba6b84f), kcg_lit_uint32(
        0xc317403d), kcg_lit_uint32(0x5a8943e8), kcg_lit_uint32(0xa70ab792),
      kcg_lit_uint32(0xbc7e1b4d), kcg_lit_uint32(0xcf2c989c), kcg_lit_uint32(
        0x8c96c42e), kcg_lit_uint32(0xf155cdc0), kcg_lit_uint32(0xc66f42a) }, {
      kcg_lit_uint32(0xa909a092), kcg_lit_uint32(0xb8cad4f0), kcg_lit_uint32(
        0xb820e72), kcg_lit_uint32(0x4025645f), kcg_lit_uint32(0x547bb2a2),
      kcg_lit_uint32(0x8f3f5016), kcg_lit_uint32(0x232276b3), kcg_lit_uint32(
        0xda69dbeb), kcg_lit_uint32(0xe4c15a08), kcg_lit_uint32(0x6e99153e),
      kcg_lit_uint32(0x13368f45), kcg_lit_uint32(0x8c1df1d0), kcg_lit_uint32(
        0xae2e7b38), kcg_lit_uint32(0xee2a30b4), kcg_lit_uint32(0x16290db0),
      kcg_lit_uint32(0xcbb12) } }, { { { kcg_lit_uint32(0x82af), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) } }, kcg_lit_int32(2), { kcg_lit_uint32(0xf48daac5),
      kcg_lit_uint32(0x7b839f3f), kcg_lit_uint32(0x2f44b7ed), kcg_lit_uint32(
        0xb1b7dc31), kcg_lit_uint32(0x3585d366), kcg_lit_uint32(0x4b096f07),
      kcg_lit_uint32(0x2e3ace85), kcg_lit_uint32(0xf758440b), kcg_lit_uint32(
        0x8ecd51fc), kcg_lit_uint32(0xa3a11862), kcg_lit_uint32(0xd07ea48d),
      kcg_lit_uint32(0x58f03002), kcg_lit_uint32(0x13ed1608), kcg_lit_uint32(
        0xac0333ba), kcg_lit_uint32(0x1591eb5d), kcg_lit_uint32(0x25809048) }, {
      kcg_lit_uint32(0x57d69162), kcg_lit_uint32(0x224ecde), kcg_lit_uint32(
        0x9ce62748), kcg_lit_uint32(0xa301be3a), kcg_lit_uint32(0xa248e50c),
      kcg_lit_uint32(0x443a7484), kcg_lit_uint32(0xd780365e), kcg_lit_uint32(
        0xacc35adb), kcg_lit_uint32(0x539bff18), kcg_lit_uint32(0x90f2168d),
      kcg_lit_uint32(0x60f767ae), kcg_lit_uint32(0x59c64d98), kcg_lit_uint32(
        0xe9157c4a), kcg_lit_uint32(0x8dd26e71), kcg_lit_uint32(0xcebe27c0),
      kcg_lit_uint32(0xac41eea) } }, { { { kcg_lit_uint32(0x7bc7cb),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(3), { kcg_lit_uint32(0xb0054a0d),
      kcg_lit_uint32(0x43a55273), kcg_lit_uint32(0x5603186e), kcg_lit_uint32(
        0xefe60ada), kcg_lit_uint32(0xf75f34a0), kcg_lit_uint32(0x577215fb),
      kcg_lit_uint32(0xe87257), kcg_lit_uint32(0xe978d95e), kcg_lit_uint32(
        0x5b181ae6), kcg_lit_uint32(0x3a61f2ce), kcg_lit_uint32(0x97b77c6c),
      kcg_lit_uint32(0x5d94ce63), kcg_lit_uint32(0x765d243b), kcg_lit_uint32(
        0x40d44d11), kcg_lit_uint32(0xdcf2f5bc), kcg_lit_uint32(0x5770a51a) }, {
      kcg_lit_uint32(0x528d86d9), kcg_lit_uint32(0xe5bcbec2), kcg_lit_uint32(
        0x795afaf3), kcg_lit_uint32(0xf3701989), kcg_lit_uint32(0x9165cb09),
      kcg_lit_uint32(0x2a70e1e3), kcg_lit_uint32(0xa96f2770), kcg_lit_uint32(
        0xa8b3247c), kcg_lit_uint32(0xc30686e5), kcg_lit_uint32(0x5258978c),
      kcg_lit_uint32(0xe30ea59d), kcg_lit_uint32(0xcb19821b), kcg_lit_uint32(
        0xc67152a4), kcg_lit_uint32(0xba6af89), kcg_lit_uint32(0x996ca20e),
      kcg_lit_uint32(0xcb019db) } }, { { { kcg_lit_uint32(0x89894c5f),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) } }, kcg_lit_int32(4), { kcg_lit_uint32(0xc34f96d),
      kcg_lit_uint32(0x88c18c13), kcg_lit_uint32(0x6444feb5), kcg_lit_uint32(
        0x7f3faaeb), kcg_lit_uint32(0xd5a206c2), kcg_lit_uint32(0x7034345c),
      kcg_lit_uint32(0xfcc9747e), kcg_lit_uint32(0xbb0ee204), kcg_lit_uint32(
        0x2c1dac0), kcg_lit_uint32(0x863153c4), kcg_lit_uint32(0x31c45de2),
      kcg_lit_uint32(0x53234728), kcg_lit_uint32(0x87dbabea), kcg_lit_uint32(
        0xeb2a158b), kcg_lit_uint32(0x921f008e), kcg_lit_uint32(0xa73302d9) }, {
      kcg_lit_uint32(0xc66f4f12), kcg_lit_uint32(0x8400d1b0), kcg_lit_uint32(
        0x1be76927), kcg_lit_uint32(0x4d6630d5), kcg_lit_uint32(0x50f88d88),
      kcg_lit_uint32(0x6dc5f67d), kcg_lit_uint32(0x9b5fded), kcg_lit_uint32(
        0x1634b9ae), kcg_lit_uint32(0x8d916be2), kcg_lit_uint32(0x3006aa38),
      kcg_lit_uint32(0x5609f35d), kcg_lit_uint32(0x2a8bc197), kcg_lit_uint32(
        0xa5ea32a8), kcg_lit_uint32(0xe40adc2e), kcg_lit_uint32(0xa8e5ba9f),
      kcg_lit_uint32(0x70c155e) } }, { { { kcg_lit_uint32(0xc0beb618),
        kcg_lit_uint32(0x97), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) } }, kcg_lit_int32(5), { kcg_lit_uint32(
        0x1a3880b7), kcg_lit_uint32(0xb7f8ed65), kcg_lit_uint32(0xe845698f),
      kcg_lit_uint32(0x4179ecdb), kcg_lit_uint32(0xd49f04ac), kcg_lit_uint32(
        0xcf4010c6), kcg_lit_uint32(0x5743320c), kcg_lit_uint32(0x3c295a97),
      kcg_lit_uint32(0x7af53e2), kcg_lit_uint32(0x864b8066), kcg_lit_uint32(
        0x5b59b19b), kcg_lit_uint32(0x535b76e9), kcg_lit_uint32(0xaabb8648),
      kcg_lit_uint32(0xf55b30b8), kcg_lit_uint32(0x897fbc0d), kcg_lit_uint32(
        0x15ffb9b) }, { kcg_lit_uint32(0xad46fcb2), kcg_lit_uint32(0x4446af47),
      kcg_lit_uint32(0xe199c178), kcg_lit_uint32(0x9f16bef8), kcg_lit_uint32(
        0x7c32e61b), kcg_lit_uint32(0x660a9a7f), kcg_lit_uint32(0xa91c3789),
      kcg_lit_uint32(0x604af4c), kcg_lit_uint32(0x2ab2014a), kcg_lit_uint32(
        0xab2015ff), kcg_lit_uint32(0x345189d5), kcg_lit_uint32(0xedfa0316),
      kcg_lit_uint32(0x8cf78c76), kcg_lit_uint32(0xb0e77ae9), kcg_lit_uint32(
        0x45feab38), kcg_lit_uint32(0x97ca16a) } }, { { { kcg_lit_uint32(
          0x850d0189), kcg_lit_uint32(0x7259), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(6), {
      kcg_lit_uint32(0xff9eae78), kcg_lit_uint32(0xe945f2e6), kcg_lit_uint32(
        0x63bea724), kcg_lit_uint32(0xeb461104), kcg_lit_uint32(0xd3db70c6),
      kcg_lit_uint32(0x67ba0c06), kcg_lit_uint32(0x6f21c6fb), kcg_lit_uint32(
        0x4645c4eb), kcg_lit_uint32(0xa4bfcffb), kcg_lit_uint32(0xf2d70505),
      kcg_lit_uint32(0x334a44be), kcg_lit_uint32(0x54cc85d1), kcg_lit_uint32(
        0x52616de1), kcg_lit_uint32(0xb64e160), kcg_lit_uint32(0xb887502b),
      kcg_lit_uint32(0x3d64e33e) }, { kcg_lit_uint32(0xfc29d66e),
      kcg_lit_uint32(0xe1e99c1d), kcg_lit_uint32(0xff558746), kcg_lit_uint32(
        0x3f5a6d63), kcg_lit_uint32(0xc9d9a540), kcg_lit_uint32(0xb793fd1a),
      kcg_lit_uint32(0x3018249d), kcg_lit_uint32(0x29fae5f7), kcg_lit_uint32(
        0x208f4b85), kcg_lit_uint32(0xbbec6ecc), kcg_lit_uint32(0x8dbd8d24),
      kcg_lit_uint32(0x994ed116), kcg_lit_uint32(0xe4942175), kcg_lit_uint32(
        0xc7094d90), kcg_lit_uint32(0x1895634d), kcg_lit_uint32(0x239d83) } }, {
    { { kcg_lit_uint32(0x81f3a8b4), kcg_lit_uint32(0x7a0ee7), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, kcg_lit_int32(7), {
      kcg_lit_uint32(0xbf651869), kcg_lit_uint32(0x4b1e2ac8), kcg_lit_uint32(
        0xdeec4e57), kcg_lit_uint32(0x919754c), kcg_lit_uint32(0xf80caf3f),
      kcg_lit_uint32(0x34023867), kcg_lit_uint32(0x454666e3), kcg_lit_uint32(
        0x795f1cc6), kcg_lit_uint32(0xa3e3a598), kcg_lit_uint32(0xbaaa676e),
      kcg_lit_uint32(0xf08b8889), kcg_lit_uint32(0xd91ade93), kcg_lit_uint32(
        0x174e763), kcg_lit_uint32(0xbf02393b), kcg_lit_uint32(0x8b6d35ab),
      kcg_lit_uint32(0x638a1790) }, { kcg_lit_uint32(0xfef20a6e),
      kcg_lit_uint32(0x7a37ae55), kcg_lit_uint32(0x78727a6b), kcg_lit_uint32(
        0x9b41fbed), kcg_lit_uint32(0x6de021d3), kcg_lit_uint32(0x70e2f50d),
      kcg_lit_uint32(0x1288db37), kcg_lit_uint32(0x9852e3e7), kcg_lit_uint32(
        0x5255fa10), kcg_lit_uint32(0x902c0f6), kcg_lit_uint32(0xa017ca85),
      kcg_lit_uint32(0x6d032ee0), kcg_lit_uint32(0x242a227b), kcg_lit_uint32(
        0xb7779bf9), kcg_lit_uint32(0xcb16dd5f), kcg_lit_uint32(0x7815605) } },
  { { { kcg_lit_uint32(0xc5ab8442), kcg_lit_uint32(0x3572b61b), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(8), { kcg_lit_uint32(0x6f51263b), kcg_lit_uint32(0xeb88dcb3),
      kcg_lit_uint32(0xd79e1b18), kcg_lit_uint32(0x52cd0b3f), kcg_lit_uint32(
        0xc7b4d6bc), kcg_lit_uint32(0xafbce488), kcg_lit_uint32(0xd07f0546),
      kcg_lit_uint32(0x73e0be78), kcg_lit_uint32(0x4ab51ff8), kcg_lit_uint32(
        0xd9ce5f82), kcg_lit_uint32(0xaf33b05e), kcg_lit_uint32(0x403164cd),
      kcg_lit_uint32(0xafbab75), kcg_lit_uint32(0x70a920bd), kcg_lit_uint32(
        0x43032589), kcg_lit_uint32(0x63b8346f) }, { kcg_lit_uint32(0xc5deadd6),
      kcg_lit_uint32(0x8a52b0af), kcg_lit_uint32(0x78b17bc1), kcg_lit_uint32(
        0x88f2e7d3), kcg_lit_uint32(0xb1db9a7f), kcg_lit_uint32(0x10e116ad),
      kcg_lit_uint32(0xbcf35e54), kcg_lit_uint32(0x23def957), kcg_lit_uint32(
        0x38c8a514), kcg_lit_uint32(0x893b728f), kcg_lit_uint32(0x3a0fbe07),
      kcg_lit_uint32(0x59620cc9), kcg_lit_uint32(0xec85e8bb), kcg_lit_uint32(
        0xdf4576c1), kcg_lit_uint32(0x88d4b73d), kcg_lit_uint32(0x8fa05f8) } },
  { { { kcg_lit_uint32(0x96f82b67), kcg_lit_uint32(0x51bc045d), kcg_lit_uint32(
          0x46), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } },
    kcg_lit_int32(9), { kcg_lit_uint32(0xfbf5c6ed), kcg_lit_uint32(0x4dee1cdd),
      kcg_lit_uint32(0x35061c10), kcg_lit_uint32(0x9004a330), kcg_lit_uint32(
        0x68be21b2), kcg_lit_uint32(0xb0f536c0), kcg_lit_uint32(0x3b950f7d),
      kcg_lit_uint32(0x92f15d74), kcg_lit_uint32(0x669ca4c1), kcg_lit_uint32(
        0xeff917e6), kcg_lit_uint32(0xc46bc65e), kcg_lit_uint32(0xa34c56c6),
      kcg_lit_uint32(0xfba5e23d), kcg_lit_uint32(0x664145e), kcg_lit_uint32(
        0x626c6d2e), kcg_lit_uint32(0xfd5e1519) }, { kcg_lit_uint32(0x4aa0762c),
      kcg_lit_uint32(0x141c39f2), kcg_lit_uint32(0x3fe38270), kcg_lit_uint32(
        0x56becdaa), kcg_lit_uint32(0x131e2a64), kcg_lit_uint32(0x6288d34b),
      kcg_lit_uint32(0x902b850b), kcg_lit_uint32(0x6fc16b1a), kcg_lit_uint32(
        0x94ccc9f6), kcg_lit_uint32(0xea1dc404), kcg_lit_uint32(0x1d28ed12),
      kcg_lit_uint32(0x51a167a0), kcg_lit_uint32(0xd9f96638), kcg_lit_uint32(
        0xd2bdf864), kcg_lit_uint32(0x6c855349), kcg_lit_uint32(
        0x1290450) } } };

/* tindyguard::CONSTRUCTION_chunk/ */
const StreamChunk_slideTypes CONSTRUCTION_chunk_tindyguard = { kcg_lit_uint32(
    0xae6de260), kcg_lit_uint32(0xc0ef27f3), kcg_lit_uint32(0xe235c32e),
  kcg_lit_uint32(0xd0d225a0), kcg_lit_uint32(0x0642eb16), kcg_lit_uint32(
    0xf57772f8), kcg_lit_uint32(0x98d1382d), kcg_lit_uint32(0x36cd788b),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* tindyguardTypes::EmptySession/ */
const Session_tindyguardTypes EmptySession_tindyguardTypes = { { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) }, kcg_lit_uint64(0), kcg_lit_uint64(0), kcg_lit_uint64(
    0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_int64(0), kcg_lit_int32(
    -1), kcg_lit_int32(0), kcg_lit_int64(-1), kcg_lit_int64(0), kcg_lit_int32(
    -1), { { { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { { kcg_lit_uint32(0) }, {
        kcg_lit_uint32(0xFFFFFFFF) } }, { { kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { kcg_lit_uint32(0) },
      kcg_lit_int32(0), kcg_lit_int32(0), kcg_lit_int64(0) }, { kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, kcg_false, kcg_lit_int32(
      -1), kcg_false, { { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) },
      kcg_lit_int64(0) } }, kcg_false, kcg_false, kcg_false };

/* tindyguardTypes::EmptyPeer/ */
const Peer_tindyguardTypes EmptyPeer_tindyguardTypes = { { kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { { kcg_lit_uint32(0) }, { kcg_lit_uint32(0xFFFFFFFF) } }, { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) } }, { { kcg_lit_uint32(0) }, kcg_lit_int32(0),
    kcg_lit_int32(0), kcg_lit_int64(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(
      0), kcg_lit_uint32(0) }, kcg_false, kcg_lit_int32(-1), kcg_false, { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0) }, kcg_lit_int64(0) } };

/* nacl::onetime::kMask32/ */
const array_uint32_4 kMask32_nacl_onetime = { kcg_lit_uint32(0x0FFFFFFF),
  kcg_lit_uint32(0x0FFFFFFC), kcg_lit_uint32(0x0FFFFFFC), kcg_lit_uint32(
    0x0FFFFFFC) };

/* nacl::onetime::minusp/ */
const array_uint8_17 minusp_nacl_onetime = { kcg_lit_uint8(5), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(0),
  kcg_lit_uint8(0), kcg_lit_uint8(0), kcg_lit_uint8(252) };

/* nacl::core::sigma/ */
const array_uint32_4 sigma_nacl_core = { kcg_lit_uint32(0x61707865),
  kcg_lit_uint32(0x3320646e), kcg_lit_uint32(0x79622d32), kcg_lit_uint32(
    0x6b206574) };

/* slideTypes::ZeroChunk/ */
const StreamChunk_slideTypes ZeroChunk_slideTypes = { kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* tindyguard::conf::fixedEphemeral/ */
const secret_tindyguardTypes fixedEphemeral_tindyguard_conf = { kcg_lit_uint32(
    0x01010100), kcg_lit_uint32(0x01010101), kcg_lit_uint32(0x01010101),
  kcg_lit_uint32(0x01010101), kcg_lit_uint32(0x01010101), kcg_lit_uint32(
    0x01010101), kcg_lit_uint32(0x01010101), kcg_lit_uint32(0x41010101) };

/* tindyguard::handshake::EmptyState/ */
const State_tindyguard_handshake EmptyState_tindyguard_handshake = { { {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0) } };

/* tindyguard::test::Lani_pub/ */
const pub_tindyguardTypes Lani_pub_tindyguard_test = { kcg_lit_uint32(
    0x6DDBC87E), kcg_lit_uint32(0xFF0FCCE9), kcg_lit_uint32(0x412D7526),
  kcg_lit_uint32(0xB39D2BB5), kcg_lit_uint32(0x74BE3186), kcg_lit_uint32(
    0x94F04406), kcg_lit_uint32(0x4F8CC83F), kcg_lit_uint32(0x429F1E4D) };

/* tindyguard::test::net10_allowed/ */
const range_t_udp net10_allowed_tindyguard_test = { { kcg_lit_uint32(
      0x0a000000) }, { kcg_lit_uint32(0xFFFFFF00) } };

/* tindyguard::test::Lani_endpoint/ */
const peer_t_udp Lani_endpoint_tindyguard_test = { { kcg_lit_uint32(
      0x8b1ec9a0) }, kcg_lit_int32(35863), kcg_lit_int32(0), kcg_lit_int64(
    -1) };

/* tindyguard::test::ChickenJoe_endpoint/ */
const peer_t_udp ChickenJoe_endpoint_tindyguard_test = { { kcg_lit_uint32(
      0x8b1ec9af) }, kcg_lit_int32(35361), kcg_lit_int32(0), kcg_lit_int64(
    -1) };

/* tindyguard::test::ChickenJoe_pub/ */
const pub_tindyguardTypes ChickenJoe_pub_tindyguard_test = { kcg_lit_uint32(
    0xF2FEC77C), kcg_lit_uint32(0x1CB02902), kcg_lit_uint32(0xC00E2537),
  kcg_lit_uint32(0x4BD66C62), kcg_lit_uint32(0x139FFDFD), kcg_lit_uint32(
    0xADDCD3B6), kcg_lit_uint32(0xD93748DF), kcg_lit_uint32(0x25CCC50F) };

/* tindyguard::test::ChickenJoe_allowed/ */
const range_t_udp ChickenJoe_allowed_tindyguard_test = { { kcg_lit_uint32(
      0x0a000001) }, { kcg_lit_uint32(0xFFFFFFFF) } };

/* tindyguard::test::ChickenJoe_ip/ */
const IpAddress_udp ChickenJoe_ip_tindyguard_test = { kcg_lit_uint32(
    0x0a000001) };

/* tindyguard::test::BigZ_priv/ */
const secret_tindyguardTypes BigZ_priv_tindyguard_test = { kcg_lit_uint32(
    0x0CBF24C8), kcg_lit_uint32(0xA836543B), kcg_lit_uint32(0xDA921A4A),
  kcg_lit_uint32(0xB59DB43F), kcg_lit_uint32(0xC6287806), kcg_lit_uint32(
    0xFFB1F6F4), kcg_lit_uint32(0x7340B1E9), kcg_lit_uint32(0x51F12C25) };

/* tindyguard::test::BigZ_pub/ */
const pub_tindyguardTypes BigZ_pub_tindyguard_test = { kcg_lit_uint32(
    0xE3E8BA7D), kcg_lit_uint32(0xB5512B3D), kcg_lit_uint32(0x20E47063),
  kcg_lit_uint32(0xC24C282D), kcg_lit_uint32(0x2A7FEE0A), kcg_lit_uint32(
    0x28B075E2), kcg_lit_uint32(0x502D916C), kcg_lit_uint32(0x29F5915D) };

/* tindyguard::test::BigZ_allowed/ */
const range_t_udp BigZ_allowed_tindyguard_test = { { kcg_lit_uint32(
      0x0a000003) }, { kcg_lit_uint32(0xFFFFFFFF) } };

/* tindyguard::test::BigZ_ip/ */
const IpAddress_udp BigZ_ip_tindyguard_test = { kcg_lit_uint32(0x0a000003) };

/* tindyguard::test::BigZ_endpoint/ */
const peer_t_udp BigZ_endpoint_tindyguard_test = { { kcg_lit_uint32(0) },
  kcg_lit_int32(0), kcg_lit_int32(0), kcg_lit_int64(0) };

/* udp::emptyPeer/ */
const peer_t_udp emptyPeer_udp = { { kcg_lit_uint32(0) }, kcg_lit_int32(0),
  kcg_lit_int32(0), kcg_lit_int64(0) };

/* udp::ipAny/ */
const IpAddress_udp ipAny_udp = { kcg_lit_uint32(0) };

/* tindyguard::test::preshared/ */
const secret_tindyguardTypes preshared_tindyguard_test = { kcg_lit_uint32(
    0x87B29016), kcg_lit_uint32(0x1C733D0B), kcg_lit_uint32(0x315EA116),
  kcg_lit_uint32(0x265FBB10), kcg_lit_uint32(0xEC37C9F8), kcg_lit_uint32(
    0xC81355D0), kcg_lit_uint32(0x5A51594A), kcg_lit_uint32(0x51A5A807) };

/* tindyguard::test::ChickenJoe_priv/ */
const secret_tindyguardTypes ChickenJoe_priv_tindyguard_test = { kcg_lit_uint32(
    0x584be4c8), kcg_lit_uint32(0x5388d6e1), kcg_lit_uint32(0xddc930ba),
  kcg_lit_uint32(0xfa4eaf2e), kcg_lit_uint32(0xdf0e9da0), kcg_lit_uint32(
    0x311078ca), kcg_lit_uint32(0x4f8c6daa), kcg_lit_uint32(0x695b42d2) };

/* tindyguardTypes::EmptyKey/ */
const KeySalted_tindyguardTypes EmptyKey_tindyguardTypes = { { { kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0) } }, { { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, {
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0) }, { kcg_lit_uint32(0), kcg_lit_uint32(0),
      kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
        0), kcg_lit_uint32(0), kcg_lit_uint32(0) } }, { { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, kcg_lit_int64(0) }, { { kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
          0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
        kcg_lit_uint32(0) }, kcg_lit_int64(0) } } };

/* tindyguardTypes::EmptyJar/ */
const CookieJar_tindyguardTypes EmptyJar_tindyguardTypes = { { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, kcg_lit_int64(0) };

/* slideTypes::ZeroKeyPair/ */
const KeyPair32_slideTypes ZeroKeyPair_slideTypes = { { kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) } };

/* nacl::box::badKey/ */
const array_uint32_8_5 badKey_nacl_box = { { kcg_lit_uint32(0), kcg_lit_uint32(
      0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0) }, { kcg_lit_uint32(
      1), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
    kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
      0) }, { kcg_lit_uint32(0xffffffEC), kcg_lit_uint32(0xffffffff),
    kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(
      0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff),
    kcg_lit_uint32(0x7Fffffff) }, { kcg_lit_uint32(0x7c7aebe0), kcg_lit_uint32(
      0xaeb8413b), kcg_lit_uint32(0xfae35616), kcg_lit_uint32(0x6ac49ff1),
    kcg_lit_uint32(0xeb8d09da), kcg_lit_uint32(0xfdb1329c), kcg_lit_uint32(
      0x16056286), kcg_lit_uint32(0x00b8495f) }, { kcg_lit_uint32(0xbc959c5f),
    kcg_lit_uint32(0x248c50a3), kcg_lit_uint32(0x55b1d0b1), kcg_lit_uint32(
      0x5bef839c), kcg_lit_uint32(0xc45c4404), kcg_lit_uint32(0x868e1c58),
    kcg_lit_uint32(0xdd4e22d8), kcg_lit_uint32(0x57119fd0) } };

/* nacl::box::bad2/ */
const Key32_slideTypes bad2_nacl_box = { kcg_lit_uint32(0xbc959c5f),
  kcg_lit_uint32(0x248c50a3), kcg_lit_uint32(0x55b1d0b1), kcg_lit_uint32(
    0x5bef839c), kcg_lit_uint32(0xc45c4404), kcg_lit_uint32(0x868e1c58),
  kcg_lit_uint32(0xdd4e22d8), kcg_lit_uint32(0x57119fd0) };

/* nacl::box::bad1/ */
const Key32_slideTypes bad1_nacl_box = { kcg_lit_uint32(0x7c7aebe0),
  kcg_lit_uint32(0xaeb8413b), kcg_lit_uint32(0xfae35616), kcg_lit_uint32(
    0x6ac49ff1), kcg_lit_uint32(0xeb8d09da), kcg_lit_uint32(0xfdb1329c),
  kcg_lit_uint32(0x16056286), kcg_lit_uint32(0x00b8495f) };

/* nacl::box::invKey/ */
const Key32_slideTypes invKey_nacl_box = { kcg_lit_uint32(0xffffffEC),
  kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(
    0xffffffff), kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0xffffffff),
  kcg_lit_uint32(0xffffffff), kcg_lit_uint32(0x7Fffffff) };

/* nacl::box::oneKey/ */
const Key32_slideTypes oneKey_nacl_box = { kcg_lit_uint32(1), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* nacl::box::zeroKey/ */
const Key32_slideTypes zeroKey_nacl_box = { kcg_lit_uint32(0), kcg_lit_uint32(
    0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(
    0), kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* nacl::op::_121665/ */
const gf_nacl_op _121665_nacl_op = { kcg_lit_int64(0xDB41), kcg_lit_int64(1),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0) };

/* nacl::box::initialScalMulAcc/ */
const scalMulAcc_nacl_box initialScalMulAcc_nacl_box = { kcg_lit_int32(32) +
  kcg_lit_int32(1), { kcg_lit_int64(1), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) }, { kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) }, { kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) }, { kcg_lit_int64(1), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
    kcg_lit_int64(0) } };

/* nacl::op::gf0/ */
const gf_nacl_op gf0_nacl_op = { kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0) };

/* nacl::op::gf1/ */
const gf_nacl_op gf1_nacl_op = { kcg_lit_int64(1), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0), kcg_lit_int64(0),
  kcg_lit_int64(0), kcg_lit_int64(0) };

/* nacl::box::_9/ */
const array_uint32_8 _9_nacl_box = { kcg_lit_uint32(9), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0), kcg_lit_uint32(0),
  kcg_lit_uint32(0), kcg_lit_uint32(0) };

/* hash::blake2s::sigma/ */
const array_uint8_16_10 sigma_hash_blake2s = { { kcg_lit_uint8(0),
    kcg_lit_uint8(1), kcg_lit_uint8(2), kcg_lit_uint8(3), kcg_lit_uint8(4),
    kcg_lit_uint8(5), kcg_lit_uint8(6), kcg_lit_uint8(7), kcg_lit_uint8(8),
    kcg_lit_uint8(9), kcg_lit_uint8(10), kcg_lit_uint8(11), kcg_lit_uint8(12),
    kcg_lit_uint8(13), kcg_lit_uint8(14), kcg_lit_uint8(15) }, { kcg_lit_uint8(
      14), kcg_lit_uint8(10), kcg_lit_uint8(4), kcg_lit_uint8(8), kcg_lit_uint8(
      9), kcg_lit_uint8(15), kcg_lit_uint8(13), kcg_lit_uint8(6), kcg_lit_uint8(
      1), kcg_lit_uint8(12), kcg_lit_uint8(0), kcg_lit_uint8(2), kcg_lit_uint8(
      11), kcg_lit_uint8(7), kcg_lit_uint8(5), kcg_lit_uint8(3) }, {
    kcg_lit_uint8(11), kcg_lit_uint8(8), kcg_lit_uint8(12), kcg_lit_uint8(0),
    kcg_lit_uint8(5), kcg_lit_uint8(2), kcg_lit_uint8(15), kcg_lit_uint8(13),
    kcg_lit_uint8(10), kcg_lit_uint8(14), kcg_lit_uint8(3), kcg_lit_uint8(6),
    kcg_lit_uint8(7), kcg_lit_uint8(1), kcg_lit_uint8(9), kcg_lit_uint8(4) }, {
    kcg_lit_uint8(7), kcg_lit_uint8(9), kcg_lit_uint8(3), kcg_lit_uint8(1),
    kcg_lit_uint8(13), kcg_lit_uint8(12), kcg_lit_uint8(11), kcg_lit_uint8(14),
    kcg_lit_uint8(2), kcg_lit_uint8(6), kcg_lit_uint8(5), kcg_lit_uint8(10),
    kcg_lit_uint8(4), kcg_lit_uint8(0), kcg_lit_uint8(15), kcg_lit_uint8(8) }, {
    kcg_lit_uint8(9), kcg_lit_uint8(0), kcg_lit_uint8(5), kcg_lit_uint8(7),
    kcg_lit_uint8(2), kcg_lit_uint8(4), kcg_lit_uint8(10), kcg_lit_uint8(15),
    kcg_lit_uint8(14), kcg_lit_uint8(1), kcg_lit_uint8(11), kcg_lit_uint8(12),
    kcg_lit_uint8(6), kcg_lit_uint8(8), kcg_lit_uint8(3), kcg_lit_uint8(13) }, {
    kcg_lit_uint8(2), kcg_lit_uint8(12), kcg_lit_uint8(6), kcg_lit_uint8(10),
    kcg_lit_uint8(0), kcg_lit_uint8(11), kcg_lit_uint8(8), kcg_lit_uint8(3),
    kcg_lit_uint8(4), kcg_lit_uint8(13), kcg_lit_uint8(7), kcg_lit_uint8(5),
    kcg_lit_uint8(15), kcg_lit_uint8(14), kcg_lit_uint8(1), kcg_lit_uint8(9) },
  { kcg_lit_uint8(12), kcg_lit_uint8(5), kcg_lit_uint8(1), kcg_lit_uint8(15),
    kcg_lit_uint8(14), kcg_lit_uint8(13), kcg_lit_uint8(4), kcg_lit_uint8(10),
    kcg_lit_uint8(0), kcg_lit_uint8(7), kcg_lit_uint8(6), kcg_lit_uint8(3),
    kcg_lit_uint8(9), kcg_lit_uint8(2), kcg_lit_uint8(8), kcg_lit_uint8(11) }, {
    kcg_lit_uint8(13), kcg_lit_uint8(11), kcg_lit_uint8(7), kcg_lit_uint8(14),
    kcg_lit_uint8(12), kcg_lit_uint8(1), kcg_lit_uint8(3), kcg_lit_uint8(9),
    kcg_lit_uint8(5), kcg_lit_uint8(0), kcg_lit_uint8(15), kcg_lit_uint8(4),
    kcg_lit_uint8(8), kcg_lit_uint8(6), kcg_lit_uint8(2), kcg_lit_uint8(10) }, {
    kcg_lit_uint8(6), kcg_lit_uint8(15), kcg_lit_uint8(14), kcg_lit_uint8(9),
    kcg_lit_uint8(11), kcg_lit_uint8(3), kcg_lit_uint8(0), kcg_lit_uint8(8),
    kcg_lit_uint8(12), kcg_lit_uint8(2), kcg_lit_uint8(13), kcg_lit_uint8(7),
    kcg_lit_uint8(1), kcg_lit_uint8(4), kcg_lit_uint8(10), kcg_lit_uint8(5) }, {
    kcg_lit_uint8(10), kcg_lit_uint8(2), kcg_lit_uint8(8), kcg_lit_uint8(4),
    kcg_lit_uint8(7), kcg_lit_uint8(6), kcg_lit_uint8(1), kcg_lit_uint8(5),
    kcg_lit_uint8(15), kcg_lit_uint8(11), kcg_lit_uint8(9), kcg_lit_uint8(14),
    kcg_lit_uint8(3), kcg_lit_uint8(12), kcg_lit_uint8(13), kcg_lit_uint8(
      0) } };

/* hash::blake2s::IV/ */
const array_uint32_8 IV_hash_blake2s = { kcg_lit_uint32(0x6A09E667),
  kcg_lit_uint32(0xBB67AE85), kcg_lit_uint32(0x3C6EF372), kcg_lit_uint32(
    0xA54FF53A), kcg_lit_uint32(0x510E527F), kcg_lit_uint32(0x9B05688C),
  kcg_lit_uint32(0x1F83D9AB), kcg_lit_uint32(0x5BE0CD19) };

/* tindyguard::MAC1_label/ */
const array_uint32_2 MAC1_label_tindyguard = { kcg_lit_uint32(0x3163616d),
  kcg_lit_uint32(0x2d2d2d2d) };

/* tindyguard::CONSTRUCTION_IDENTIFIER_hash/ */
const array_uint32_8 CONSTRUCTION_IDENTIFIER_hash_tindyguard = { kcg_lit_uint32(
    0x61b31122), kcg_lit_uint32(0x66c51a08), kcg_lit_uint32(0xdb431269),
  kcg_lit_uint32(0x32d58a45), kcg_lit_uint32(0x666c9c2d), kcg_lit_uint32(
    0xb7e89322), kcg_lit_uint32(0x659ce10e), kcg_lit_uint32(0xf39e07ba) };

/* tindyguard::COOKIE_label/ */
const array_uint32_2 COOKIE_label_tindyguard = { kcg_lit_uint32(0x6b6f6f63),
  kcg_lit_uint32(0x2d2d6569) };

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** kcg_consts.c
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

