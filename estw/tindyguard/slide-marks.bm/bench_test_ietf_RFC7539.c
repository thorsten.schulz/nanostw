/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_test_ietf_RFC7539.h"

/* test::ietf_RFC7539::bench/ */
kcg_bool bench_test_ietf_RFC7539(void)
{
  /* _L9/, failed/ */
  kcg_bool failed;

  failed = /* _L1=(test::ietf_RFC7539::bench_aead#1)/ */
    bench_aead_test_ietf_RFC7539_2() ||
    /* _L2=(test::ietf_RFC7539::bench_xaead#1)/ */
    bench_xaead_test_ietf_RFC7539_2() ||
    /* _L3=(test::ietf_RFC7539::bench_xaead#2)/ */
    bench_xaead_test_ietf_RFC7539_23() ||
    /* _L4=(test::ietf_RFC7539::bench_aead#2)/ */
    bench_aead_test_ietf_RFC7539_23() ||
    /* _L5=(test::ietf_RFC7539::bench_aead#3)/ */
    bench_aead_test_ietf_RFC7539_64() ||
    /* _L6=(test::ietf_RFC7539::bench_xaead#3)/ */
    bench_xaead_test_ietf_RFC7539_64() ||
    /* _L7=(test::ietf_RFC7539::bench_aead#4)/ */
    bench_aead_test_ietf_RFC7539_256() ||
    /* _L8=(test::ietf_RFC7539::bench_xaead#4)/ */
    bench_xaead_test_ietf_RFC7539_256();
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_test_ietf_RFC7539.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

