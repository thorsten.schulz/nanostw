/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_halftime_nacl_sign.h"

/* nacl::sign::sign_halftime/ */
void sign_halftime_nacl_sign(
  /* SecretKey/, _L54/ */
  KeyPair32_slideTypes *SecretKey,
  /* _L61/, lastPart/ */
  StreamChunk_slideTypes *lastPart,
  /* _L57/, _L64/, intermediateAccu1/ */
  prependAkku_nacl_sign *intermediateAccu1,
  /* _L65/, goOnHashBlocks/ */
  kcg_bool *goOnHashBlocks,
  /* _L89/, signoo/ */
  prependAkku_nacl_sign *signoo,
  /* @1/_L3/, @1/rout/, _L31/, r/ */
  array_uint32_8 *r)
{
  StreamChunk_slideTypes tmp;
  array_uint32_16 tmp1;
  kcg_size idx;
  /* @2/_L15/ */
  kcg_uint32 _L15_st32_casti64_1_reduce_1;
  /* @2/_L14/ */
  kcg_uint32 _L14_st32_casti64_1_reduce_1;
  array_int64_64 tmp2;
  cpoint_nacl_op tmp3;
  /* @1/_L6/ */
  i64416_nacl_sign _L6_reduce_1;
  /* _L32/ */
  array_uint32_8 _L32;
  /* _L59/ */
  kcg_int32 _L59;

  kcg_copy_array_uint32_8(&(*signoo).d, &(*intermediateAccu1).d);
  kcg_copy_array_uint32_8(&tmp1[0], &(*intermediateAccu1).stash);
  kcg_copy_array_uint32_8(&tmp1[8], (array_uint32_8 *) &(*lastPart)[0]);
  /* _L62=(hash::sha512::finalize#1)/ */
  finalize_hash_sha512(
    &(*intermediateAccu1).accu,
    &tmp1,
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &tmp);
  /* @1/_L6= */
  for (idx = 0; idx < 16; idx++) {
    _L6_reduce_1[idx][0] = /* @2/_L17= */(kcg_int64)
        /* @2/_L8= */(kcg_uint8) tmp[idx];
    _L14_st32_casti64_1_reduce_1 = tmp[idx] >> kcg_lit_uint32(8);
    _L6_reduce_1[idx][1] = /* @2/_L18= */(kcg_int64)
        /* @2/_L7= */(kcg_uint8) _L14_st32_casti64_1_reduce_1;
    _L15_st32_casti64_1_reduce_1 = _L14_st32_casti64_1_reduce_1 >>
      kcg_lit_uint32(8);
    _L6_reduce_1[idx][2] = /* @2/_L19= */(kcg_int64)
        /* @2/_L6= */(kcg_uint8) _L15_st32_casti64_1_reduce_1;
    _L6_reduce_1[idx][3] = /* @2/_L20= */(kcg_int64)
        /* @2/_L5= */(kcg_uint8) (_L15_st32_casti64_1_reduce_1 >> kcg_lit_uint32(8));
  }
  kcg_copy_array_int64_4(&tmp2[0], &_L6_reduce_1[0]);
  kcg_copy_array_int64_4(&tmp2[4], &_L6_reduce_1[1]);
  kcg_copy_array_int64_4(&tmp2[8], &_L6_reduce_1[2]);
  kcg_copy_array_int64_4(&tmp2[12], &_L6_reduce_1[3]);
  kcg_copy_array_int64_4(&tmp2[16], &_L6_reduce_1[4]);
  kcg_copy_array_int64_4(&tmp2[20], &_L6_reduce_1[5]);
  kcg_copy_array_int64_4(&tmp2[24], &_L6_reduce_1[6]);
  kcg_copy_array_int64_4(&tmp2[28], &_L6_reduce_1[7]);
  kcg_copy_array_int64_4(&tmp2[32], &_L6_reduce_1[8]);
  kcg_copy_array_int64_4(&tmp2[36], &_L6_reduce_1[9]);
  kcg_copy_array_int64_4(&tmp2[40], &_L6_reduce_1[10]);
  kcg_copy_array_int64_4(&tmp2[44], &_L6_reduce_1[11]);
  kcg_copy_array_int64_4(&tmp2[48], &_L6_reduce_1[12]);
  kcg_copy_array_int64_4(&tmp2[52], &_L6_reduce_1[13]);
  kcg_copy_array_int64_4(&tmp2[56], &_L6_reduce_1[14]);
  kcg_copy_array_int64_4(&tmp2[60], &_L6_reduce_1[15]);
  /* @1/_L3=(nacl::sign::modL#1)/ */ modL_nacl_sign(&tmp2, r);
  _L59 = kcg_lit_int32(32) + (*intermediateAccu1).accu.length;
  /* _L33=(nacl::sign::scalarBase#1)/ */ scalarBase_nacl_sign(r, &tmp3);
  /* _L32=(nacl::sign::pack#1)/ */ pack_nacl_sign(&tmp3, &_L32);
  kcg_copy_array_uint32_8(&(*signoo).stash, &_L32);
  kcg_copy_array_uint64_8(&(*signoo).accu.z, (array_uint64_8 *) &IV_hash_sha512);
  (*signoo).accu.length = _L59;
  kcg_copy_array_uint32_8(&(*signoo).accu.porch[0], &_L32);
  kcg_copy_Key32_slideTypes(&(*signoo).accu.porch[8], &(*SecretKey).pk_y);
  *goOnHashBlocks = _L59 >= BlockBytes_slideTypes;
}

/*
  Expanded instances for: nacl::sign::sign_halftime/
  @1: (nacl::sign::reduce#1)
  @2: @1/(slideTypes::st32_casti64#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_halftime_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

