/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdf_extract_hash_sha256_2_2.h"

/* hash::sha256::hkdf_extract/ */
void hkdf_extract_hash_sha256_2_2(
  /* _L9/, ikmlen/ */
  size_slideTypes ikmlen_2_2,
  /* _L8/, ikm/ */
  array_uint32_16_2 *ikm_2_2,
  /* _L1/, _L10/, saltlen/ */
  size_slideTypes saltlen_2_2,
  /* _L5/, salt/ */
  array_uint32_16_2 *salt_2_2,
  /* _L7/, prk/ */
  StreamChunk_slideTypes *prk_2_2)
{
  array_uint32_16_2 tmp;
  size_slideTypes tmp1;
  /* _L2/ */
  kcg_bool _L2_2_2;
  kcg_size idx;

  _L2_2_2 = saltlen_2_2 > kcg_lit_int32(0);
  /* _L4= */
  if (_L2_2_2) {
    kcg_copy_array_uint32_16_2(&tmp, salt_2_2);
    tmp1 = saltlen_2_2;
  }
  else {
    for (idx = 0; idx < 2; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &tmp[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    tmp1 = Bytes_hash_sha256;
  }
  /* _L7=(hash::sha256::HMAC#1)/ */
  HMAC_hash_sha256_2_2(ikm_2_2, ikmlen_2_2, &tmp, tmp1, prk_2_2);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_extract_hash_sha256_2_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

