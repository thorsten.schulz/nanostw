/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sign_finalize_it3_nacl_sign_H_
#define _sign_finalize_it3_nacl_sign_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::sign_finalize_it3/ */
extern void sign_finalize_it3_nacl_sign(
  /* _L5/, j/ */
  int_slideTypes j,
  /* _L14/, _L2/, a/ */
  array_int64_64 *a,
  /* _L6/, i/ */
  int_slideTypes i,
  /* _L3/, h/ */
  kcg_uint8 h,
  /* _L4/, d/ */
  kcg_uint8 d,
  /* _L8/, aoo/ */
  array_int64_64 *aoo);



#endif /* _sign_finalize_it3_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_finalize_it3_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

