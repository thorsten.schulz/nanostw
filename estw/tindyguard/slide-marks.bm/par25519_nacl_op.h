/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _par25519_nacl_op_H_
#define _par25519_nacl_op_H_

#include "kcg_types.h"
#include "pack25519_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::par25519/ */
extern kcg_uint32 par25519_nacl_op(/* _L2/, i/ */ gf_nacl_op *i);



#endif /* _par25519_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** par25519_nacl_op.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

