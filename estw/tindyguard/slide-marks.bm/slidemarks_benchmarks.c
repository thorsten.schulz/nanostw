/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "slidemarks_benchmarks.h"

/* benchmarks::slidemarks/ */
void slidemarks_benchmarks(
  /* _L4/, failed/ */
  kcg_bool *failed,
  /* _L3/, failed2/ */
  kcg_bool *failed2,
  outC_slidemarks_benchmarks *outC)
{
  /* _L2/ */
  kcg_bool _L2;
  /* _L1/ */
  kcg_bool _L1;

  /* _L2=(test::pullAll#1)/ */
  pullAll_test(&_L2, failed2, &outC->Context_pullAll_1);
  /* _L1=(tindyguard::test::ProtocolDryRun#1)/ */
  ProtocolDryRun_tindyguard_test(&_L1, &outC->Context_ProtocolDryRun_1);
  *failed = _L1 || _L2;
}

#ifndef KCG_USER_DEFINED_INIT
void slidemarks_init_benchmarks(outC_slidemarks_benchmarks *outC)
{
  /* _L1=(tindyguard::test::ProtocolDryRun#1)/ */
  ProtocolDryRun_init_tindyguard_test(&outC->Context_ProtocolDryRun_1);
  /* _L2=(test::pullAll#1)/ */ pullAll_init_test(&outC->Context_pullAll_1);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void slidemarks_reset_benchmarks(outC_slidemarks_benchmarks *outC)
{
  /* _L1=(tindyguard::test::ProtocolDryRun#1)/ */
  ProtocolDryRun_reset_tindyguard_test(&outC->Context_ProtocolDryRun_1);
  /* _L2=(test::pullAll#1)/ */ pullAll_reset_test(&outC->Context_pullAll_1);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** slidemarks_benchmarks.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

