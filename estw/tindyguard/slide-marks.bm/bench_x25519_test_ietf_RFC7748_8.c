/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_x25519_test_ietf_RFC7748_8.h"

/* test::ietf_RFC7748::bench_x25519/ */
void bench_x25519_test_ietf_RFC7748_8(
  /* failed/ */
  kcg_bool *failed_8,
  outC_bench_x25519_test_ietf_RFC7748_8 *outC)
{
  KeyPair32_slideTypes tmp_str;
  /* SM3: */
  kcg_bool SM3_reset_sel_8;
  /* SM3: */
  SSM_ST_SM3 SM3_state_sel_8;
  kcg_bool noname;
  /* SM3:compute:_L2/, k3/ */
  array_uint32_8 _L2_compute_SM3_8;
  /* SM3:compute:_L1/, runAgain3/ */
  kcg_bool _L1_compute_SM3_8;
  /* SM3:compute:_L12/ */
  array_uint32_8 _L12_compute_SM3_8;
  kcg_size idx;

  SM3_state_sel_8 = outC->SM3_state_nxt_8;
  SM3_reset_sel_8 = outC->SM3_reset_nxt_8;
  /* SM3: */
  switch (SM3_state_sel_8) {
    case SSM_st_more_SM3 :
      outC->SM3_reset_nxt_8 = kcg_true;
      *failed_8 = kcg_true;
      kcg_copy_array_uint32_8(&outC->u_8, &outC->k_8);
      kcg_copy_array_uint32_8(&outC->k_8, &outC->k3_8);
      outC->SM3_state_nxt_8 = SSM_st_compute_SM3;
      outC->iter_8 = outC->iter_8 + kcg_lit_int32(1);
      break;
    case SSM_st_fail_SM3 :
      outC->SM3_reset_nxt_8 = kcg_false;
      *failed_8 = kcg_true;
      outC->SM3_state_nxt_8 = SSM_st_fail_SM3;
      break;
    case SSM_st_ok_SM3 :
      outC->SM3_reset_nxt_8 = kcg_false;
      *failed_8 = kcg_false;
      outC->SM3_state_nxt_8 = SSM_st_ok_SM3;
      break;
    case SSM_st_compute_SM3 :
      *failed_8 = kcg_true;
      if (SM3_reset_sel_8) {
        /* SM3:compute:_L1=(nacl::box::scalarmult#3)/ */
        scalarmult_reset_nacl_box_8(&outC->Context_scalarmult_3);
      }
      for (idx = 0; idx < 8; idx++) {
        _L12_compute_SM3_8[idx] = kcg_lit_uint32(0);
      }
      kcg_copy_array_uint32_8(&tmp_str.sk_x, &outC->k_8);
      kcg_copy_array_uint32_8(&tmp_str.pk_y, &_L12_compute_SM3_8);
      /* SM3:compute:_L1=(nacl::box::scalarmult#3)/ */
      scalarmult_nacl_box_8(
        &tmp_str,
        &outC->u_8,
        kcg_false,
        &_L1_compute_SM3_8,
        &_L2_compute_SM3_8,
        &noname,
        &outC->Context_scalarmult_3);
      outC->SM3_reset_nxt_8 = !_L1_compute_SM3_8;
      kcg_copy_array_uint32_8(&outC->k3_8, &_L2_compute_SM3_8);
      if (outC->SM3_reset_nxt_8) {
        if (outC->iter_8 == kcg_lit_int32(999)) {
          if (kcg_comp_array_uint32_8(
              &_L2_compute_SM3_8,
              (array_uint32_8 *) &RFC7748_exp_k1000_test_ietf_RFC7748)) {
            outC->SM3_state_nxt_8 = SSM_st_ok_SM3;
          }
          else {
            outC->SM3_state_nxt_8 = SSM_st_fail_SM3;
          }
        }
        else {
          outC->SM3_state_nxt_8 = SSM_st_more_SM3;
        }
      }
      else {
        outC->SM3_state_nxt_8 = SSM_st_compute_SM3;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
}

#ifndef KCG_USER_DEFINED_INIT
void bench_x25519_init_test_ietf_RFC7748_8(
  outC_bench_x25519_test_ietf_RFC7748_8 *outC)
{
  kcg_size idx;

  outC->SM3_reset_nxt_8 = kcg_false;
  for (idx = 0; idx < 8; idx++) {
    outC->k3_8[idx] = kcg_lit_uint32(0);
  }
  /* SM3:compute:_L1=(nacl::box::scalarmult#3)/ */
  scalarmult_init_nacl_box_8(&outC->Context_scalarmult_3);
  outC->iter_8 = kcg_lit_int32(0);
  kcg_copy_array_uint32_8(&outC->u_8, (array_uint32_8 *) &_9_nacl_box);
  kcg_copy_array_uint32_8(&outC->k_8, (array_uint32_8 *) &_9_nacl_box);
  outC->SM3_state_nxt_8 = SSM_st_compute_SM3;
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void bench_x25519_reset_test_ietf_RFC7748_8(
  outC_bench_x25519_test_ietf_RFC7748_8 *outC)
{
  outC->SM3_reset_nxt_8 = kcg_false;
  /* SM3:compute:_L1=(nacl::box::scalarmult#3)/ */
  scalarmult_reset_nacl_box_8(&outC->Context_scalarmult_3);
  outC->iter_8 = kcg_lit_int32(0);
  kcg_copy_array_uint32_8(&outC->u_8, (array_uint32_8 *) &_9_nacl_box);
  kcg_copy_array_uint32_8(&outC->k_8, (array_uint32_8 *) &_9_nacl_box);
  outC->SM3_state_nxt_8 = SSM_st_compute_SM3;
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_x25519_test_ietf_RFC7748_8.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

