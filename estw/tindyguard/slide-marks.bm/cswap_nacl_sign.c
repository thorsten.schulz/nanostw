/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "cswap_nacl_sign.h"

/* nacl::sign::cswap/ */
void cswap_nacl_sign(
  /* _L1/, pqp/ */
  pq_pair_nacl_sign *pqp,
  /* _L8/, b/ */
  kcg_uint8 b,
  /* _L21/, o/ */
  pq_pair_nacl_sign *o)
{
  kcg_size idx;
  /* @1/_L7/ */
  kcg_uint64 _L7_sel25519_1;
  /* @2/_L9/ */
  kcg_uint64 _L9_sel25519_it1_1_sel25519_1;
  kcg_size idx_sel25519_1;
  /* _L7/ */
  cpoint_nacl_op _L7;

  /* _L6= */
  for (idx = 0; idx < 4; idx++) {
    _L7_sel25519_1 = kcg_lnot_uint64(
        /* @1/_L6= */(kcg_uint64) (/* _L9= */(kcg_int32) b - kcg_lit_int32(1)));
    /* @1/_L8= */
    for (idx_sel25519_1 = 0; idx_sel25519_1 < 16; idx_sel25519_1++) {
      _L9_sel25519_it1_1_sel25519_1 = (/* @2/_L14= */(kcg_uint64)
            (*pqp).p[idx][idx_sel25519_1] ^ /* @2/_L15= */(kcg_uint64)
            (*pqp).q[idx][idx_sel25519_1]) & /* @2/_L16= */(kcg_uint64)
          /* @1/_L11= */(kcg_int64) _L7_sel25519_1;
      _L7[idx][idx_sel25519_1] = /* @2/_L18= */(kcg_int64)
          (/* @2/_L15= */(kcg_uint64) (*pqp).q[idx][idx_sel25519_1] ^
            _L9_sel25519_it1_1_sel25519_1);
      (*o).p[idx][idx_sel25519_1] = /* @2/_L17= */(kcg_int64)
          (/* @2/_L14= */(kcg_uint64) (*pqp).p[idx][idx_sel25519_1] ^
            _L9_sel25519_it1_1_sel25519_1);
    }
  }
  kcg_copy_cpoint_nacl_op(&(*o).q, &_L7);
}

/*
  Expanded instances for: nacl::sign::cswap/
  @1: (nacl::op::sel25519#1)
  @2: @1/(nacl::op::sel25519_it1#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** cswap_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

