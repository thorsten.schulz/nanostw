/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_nacl_sign_16.h"

/* nacl::sign::sign/ */
void sign_nacl_sign_16(
  /* _L23/, _L59/, _L62/, _L71/, msg/ */
  array_uint32_16_16 *msg_16,
  /* _L24/, len/ */
  kcg_int32 len_16,
  /* _L19/, _L40/, key/ */
  KeyPair32_slideTypes *key_16,
  /* _L41/, signature/ */
  Signature_nacl_sign *signature_16)
{
  kcg_size idx;
  prependAkku_nacl_sign acc;
  kcg_bool cond_iterw;
  StreamChunk_slideTypes tmp;
  /* _L39/ */
  array_uint32_8 _L39_16;
  /* _L30/, _L36/ */
  kcg_bool _L36_16;
  /* _L64/ */
  array_uint32_16_15 _L64_16;
  /* _L60/, _L66/ */
  size_slideTypes _L66_16;
  /* _L67/ */
  prependAkku_nacl_sign _L67_16;

  /* _L30=(nacl::sign::sign_init#1)/ */
  sign_init_nacl_sign(key_16, len_16, &(*msg_16)[0], &_L36_16, &acc);
  kcg_copy_array_uint32_16_15(&_L64_16, (array_uint32_16_15 *) &(*msg_16)[1]);
  /* _L60= */
  if (_L36_16) {
    /* _L60= */
    for (idx = 0; idx < 15; idx++) {
      kcg_copy_prependAkku_nacl_sign(&_L67_16, &acc);
      /* _L60=(nacl::sign::sign_block1#1)/ */
      sign_block1_nacl_sign(
        /* _L60= */(kcg_int32) idx,
        &_L67_16,
        &_L64_16[idx],
        &cond_iterw,
        &acc);
      _L66_16 = /* _L60= */(kcg_int32) (idx + 1);
      /* _L60= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    _L66_16 = kcg_lit_int32(0);
  }
  if (kcg_lit_int32(0) <= _L66_16 && _L66_16 < kcg_lit_int32(15)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &_L64_16[_L66_16]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* _L36=(nacl::sign::sign_halftime#1)/ */
  sign_halftime_nacl_sign(key_16, &tmp, &acc, &_L36_16, &_L67_16, &_L39_16);
  /* _L66= */
  if (_L36_16) {
    /* _L66= */
    for (idx = 0; idx < 16; idx++) {
      kcg_copy_prependAkku_nacl_sign(&acc, &_L67_16);
      /* _L66=(nacl::sign::sign_block2#2)/ */
      sign_block2_nacl_sign(
        /* _L66= */(kcg_int32) idx,
        &acc,
        &(*msg_16)[idx],
        &cond_iterw,
        &_L67_16);
      _L66_16 = /* _L66= */(kcg_int32) (idx + 1);
      /* _L66= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    _L66_16 = kcg_lit_int32(0);
  }
  if (kcg_lit_int32(0) <= _L66_16 && _L66_16 < kcg_lit_int32(16)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &(*msg_16)[_L66_16]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* _L41=(nacl::sign::sign_finalize#1)/ */
  sign_finalize_nacl_sign(&tmp, &_L67_16, &_L39_16, signature_16);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_nacl_sign_16.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

