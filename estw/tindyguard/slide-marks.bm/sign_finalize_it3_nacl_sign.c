/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_finalize_it3_nacl_sign.h"

/* nacl::sign::sign_finalize_it3/ */
void sign_finalize_it3_nacl_sign(
  /* _L5/, j/ */
  int_slideTypes j,
  /* _L14/, _L2/, a/ */
  array_int64_64 *a,
  /* _L6/, i/ */
  int_slideTypes i,
  /* _L3/, h/ */
  kcg_uint8 h,
  /* _L4/, d/ */
  kcg_uint8 d,
  /* _L8/, aoo/ */
  array_int64_64 *aoo)
{
  /* _L11/ */
  kcg_int32 _L11;

  _L11 = j + i;
  kcg_copy_array_int64_64(aoo, a);
  if (kcg_lit_int32(0) <= _L11 && _L11 < kcg_lit_int32(64)) {
    (*aoo)[_L11] = (*a)[_L11] + /* _L17= */(kcg_int64)
        (/* _L15= */(kcg_uint64) h * /* _L16= */(kcg_uint64) d);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_finalize_it3_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

