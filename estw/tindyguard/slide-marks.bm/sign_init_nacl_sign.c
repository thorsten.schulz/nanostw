/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_init_nacl_sign.h"

/* nacl::sign::sign_init/ */
void sign_init_nacl_sign(
  /* SecretKey/, _L19/ */
  KeyPair32_slideTypes *SecretKey,
  /* _L28/, length/ */
  kcg_int32 length,
  /* _L62/, msg0/ */
  StreamChunk_slideTypes *msg0,
  /* _L29/, goOnHashBlocks/ */
  kcg_bool *goOnHashBlocks,
  /* _L49/, signoo/ */
  prependAkku_nacl_sign *signoo)
{
  array_uint32_16 tmp;
  /* _L27/ */
  kcg_int32 _L27;
  /* _L40/ */
  StreamChunk_slideTypes _L40;
  /* _L9/ */
  StreamChunk_slideTypes _L9;
  kcg_size idx;

  kcg_copy_array_uint32_8(&(*signoo).stash, (array_uint32_8 *) &(*msg0)[8]);
  _L27 = length + kcg_lit_int32(32);
  kcg_copy_Key32_slideTypes(&tmp[0], &(*SecretKey).sk_x);
  for (idx = 0; idx < 8; idx++) {
    tmp[idx + 8] = kcg_lit_uint32(0);
  }
  /* _L40=(hash::sha512::single#1)/ */
  single_hash_sha512(
    &tmp,
    kcg_lit_int32(32),
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &_L40);
  kcg_copy_StreamChunk_slideTypes(&_L9, &_L40);
  _L9[0] = _L40[0] & kcg_lit_uint32(4294967288);
  _L9[7] = (_L40[7] & kcg_lit_uint32(2147483647)) | kcg_lit_uint32(1073741824);
  kcg_copy_array_uint32_8(&(*signoo).d, (array_uint32_8 *) &_L9[0]);
  kcg_copy_array_uint64_8(&(*signoo).accu.z, (array_uint64_8 *) &IV_hash_sha512);
  (*signoo).accu.length = _L27;
  kcg_copy_array_uint32_8(&(*signoo).accu.porch[0], (array_uint32_8 *) &_L9[8]);
  kcg_copy_array_uint32_8(
    &(*signoo).accu.porch[8],
    (array_uint32_8 *) &(*msg0)[0]);
  *goOnHashBlocks = _L27 >= hBlockBytes_hash_sha512;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_init_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

