/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sign_finalize_it2_nacl_sign_H_
#define _sign_finalize_it2_nacl_sign_H_

#include "kcg_types.h"
#include "sign_finalize_it3_nacl_sign.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::sign_finalize_it2/ */
extern void sign_finalize_it2_nacl_sign(
  /* _L29/, i4/ */
  int_slideTypes i4,
  /* _L2/, a/ */
  array_int64_64 *a,
  /* _L1/, i/ */
  int_slideTypes i,
  /* _L3/, hm/ */
  kcg_uint8 hm,
  /* _L4/, d/ */
  array_uint8_32 *d,
  /* _L5/, aoo/ */
  array_int64_64 *aoo);



#endif /* _sign_finalize_it2_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_finalize_it2_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

