/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_test_ietf_RFC7693.h"

/* test::ietf_RFC7693::test/ */
kcg_bool test_test_ietf_RFC7693(void)
{
  kcg_bool tmp;
  kcg_size idx;
  kcg_bool tmp1;
  kcg_bool tmp2;
  /* _L3/, failed/ */
  kcg_bool failed;

  tmp2 = kcg_false;
  tmp1 = kcg_false;
  tmp = kcg_false;
  /* _L13=, _L16=, _L19= */
  for (idx = 0; idx < 10; idx++) {
    tmp = /* _L13=(test::ietf_RFC7693::bench_hash#1)/ */
      bench_hash_test_ietf_RFC7693_16(tmp, (simpleTC_test *) &stc_test[idx]);
    tmp1 = /* _L16=(test::ietf_RFC7693::bench_hash#2)/ */
      bench_hash_test_ietf_RFC7693_64(tmp1, (simpleTC_test *) &stc_test[idx]);
    tmp2 = /* _L19=(test::ietf_RFC7693::bench_hash#3)/ */
      bench_hash_test_ietf_RFC7693_256(tmp2, (simpleTC_test *) &stc_test[idx]);
  }
  failed = /* _L1=(test::ietf_RFC7693::test_BLAKE2s_it#1)/ */
    test_BLAKE2s_it_test_ietf_RFC7693(
      kcg_false,
      (testcase_test_ietf_RFC7693 *) &test_blake2s_test_ietf_RFC7693) ||
    /* _L8=(test::ietf_RFC7693::test_hkdf_it#1)/ */
    test_hkdf_it_test_ietf_RFC7693(
      kcg_false,
      (testcase_hkdf_test_ietf_RFC7693 *) &test_hkdf_test_ietf_RFC7693) ||
    /* _L10=(test::ietf_RFC7693::test_BLAKE2s_key_it#1)/ */
    test_BLAKE2s_key_it_test_ietf_RFC7693(
      kcg_false,
      (testcase_test_ietf_RFC7693 *) &test_blake2s_keyed_test_ietf_RFC7693) ||
    tmp || tmp1 || tmp2;
  return failed;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_test_ietf_RFC7693.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

