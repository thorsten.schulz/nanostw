/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _bench_aead_test_ietf_RFC7539_256_H_
#define _bench_aead_test_ietf_RFC7539_256_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_256_1_20.h"
#include "aead_nacl_box_256_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC7539::bench_aead/ */
extern kcg_bool bench_aead_test_ietf_RFC7539_256(void);



#endif /* _bench_aead_test_ietf_RFC7539_256_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_aead_test_ietf_RFC7539_256.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

