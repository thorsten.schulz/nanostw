/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_BLAKE2s_key_it_test_ietf_RFC7693.h"

/* test::ietf_RFC7693::test_BLAKE2s_key_it/ */
kcg_bool test_BLAKE2s_key_it_test_ietf_RFC7693(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L4/, tc/ */
  testcase_test_ietf_RFC7693 *tc)
{
  array_uint32_16_3 tmp;
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  /* @2/_L8/ */
  kcg_int32 _L8_stream_it_1_hash128_1;
  kcg_int32 tmp_stream_it_1_hash128_1;
  /* @1/_L76/ */
  kcg_int32 _L76_hash128_1_3;
  /* @1/_L61/ */
  array_uint32_8 _L61_hash128_1_3;
  /* _L3/, failed/ */
  kcg_bool failed;

  _L76_hash128_1_3 = (*tc).mlen + kcg_lit_int32(64);
  _L61_hash128_1_3[0] = IV_hash_blake2s[0] ^ (kcg_lit_uint32(16842768) |
      kcg_lsl_uint32(/* @1/_L72= */(kcg_uint32) (*tc).klen, kcg_lit_uint32(8)));
  kcg_copy_array_uint32_7(
    &_L61_hash128_1_3[1],
    (array_uint32_7 *) &IV_hash_blake2s[1]);
  kcg_copy_StreamChunk_slideTypes(&tmp[0], &(*tc).key);
  kcg_copy_array_uint32_16_2(&tmp[1], &(*tc).msg);
  /* @1/_L60= */
  for (idx = 0; idx < 3; idx++) {
    kcg_copy_array_uint32_8(&acc, &_L61_hash128_1_3);
    _L8_stream_it_1_hash128_1 = /* @1/_L60= */(kcg_int32) idx *
      hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
    cond_iterw = _L76_hash128_1_3 > _L8_stream_it_1_hash128_1;
    /* @2/_L14= */
    if (cond_iterw) {
      tmp_stream_it_1_hash128_1 = _L8_stream_it_1_hash128_1;
    }
    else {
      tmp_stream_it_1_hash128_1 = _L76_hash128_1_3;
    }
    /* @2/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &acc,
      tmp_stream_it_1_hash128_1,
      &tmp[idx],
      cond_iterw,
      &_L61_hash128_1_3);
    /* @1/_L60= */
    if (!cond_iterw) {
      break;
    }
  }
  failed = failing || !kcg_comp_array_uint32_4(
      (array_uint32_4 *) &_L61_hash128_1_3[0],
      (array_uint32_4 *) &(*tc).exp[0]);
  return failed;
}

/*
  Expanded instances for: test::ietf_RFC7693::test_BLAKE2s_key_it/
  @1: (hash::blake2s::hash128#1)
  @2: @1/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_BLAKE2s_key_it_test_ietf_RFC7693.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

