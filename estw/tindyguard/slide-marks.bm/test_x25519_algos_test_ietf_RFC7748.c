/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "test_x25519_algos_test_ietf_RFC7748.h"

/* test::ietf_RFC7748::test_x25519_algos/ */
void test_x25519_algos_test_ietf_RFC7748(
  /* _L6/, failed/ */
  kcg_bool *failed,
  outC_test_x25519_algos_test_ietf_RFC7748 *outC)
{
  kcg_bool tmp;
  kcg_bool tmp1;
  kcg_bool tmp2;
  kcg_bool tmp3;
  kcg_bool tmp4;

  /* _L1=(test::ietf_RFC7748::bench_x25519#1)/ */
  bench_x25519_test_ietf_RFC7748_1(&tmp, &outC->Context_bench_x25519_1);
  /* _L2=(test::ietf_RFC7748::bench_x25519#2)/ */
  bench_x25519_test_ietf_RFC7748_8(&tmp1, &outC->Context_bench_x25519_2);
  /* _L3=(test::ietf_RFC7748::bench_x25519#3)/ */
  bench_x25519_test_ietf_RFC7748_19(&tmp2, &outC->Context_bench_x25519_3);
  /* _L4=(test::ietf_RFC7748::bench_x25519#4)/ */
  bench_x25519_test_ietf_RFC7748_21(&tmp3, &outC->Context_bench_x25519_4);
  /* _L5=(test::ietf_RFC7748::bench_x25519#5)/ */
  bench_x25519_test_ietf_RFC7748_20(&tmp4, &outC->Context_bench_x25519_5);
  *failed = tmp || tmp1 || tmp2 || tmp3 || tmp4;
}

#ifndef KCG_USER_DEFINED_INIT
void test_x25519_algos_init_test_ietf_RFC7748(
  outC_test_x25519_algos_test_ietf_RFC7748 *outC)
{
  /* _L5=(test::ietf_RFC7748::bench_x25519#5)/ */
  bench_x25519_init_test_ietf_RFC7748_20(&outC->Context_bench_x25519_5);
  /* _L4=(test::ietf_RFC7748::bench_x25519#4)/ */
  bench_x25519_init_test_ietf_RFC7748_21(&outC->Context_bench_x25519_4);
  /* _L3=(test::ietf_RFC7748::bench_x25519#3)/ */
  bench_x25519_init_test_ietf_RFC7748_19(&outC->Context_bench_x25519_3);
  /* _L2=(test::ietf_RFC7748::bench_x25519#2)/ */
  bench_x25519_init_test_ietf_RFC7748_8(&outC->Context_bench_x25519_2);
  /* _L1=(test::ietf_RFC7748::bench_x25519#1)/ */
  bench_x25519_init_test_ietf_RFC7748_1(&outC->Context_bench_x25519_1);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void test_x25519_algos_reset_test_ietf_RFC7748(
  outC_test_x25519_algos_test_ietf_RFC7748 *outC)
{
  /* _L5=(test::ietf_RFC7748::bench_x25519#5)/ */
  bench_x25519_reset_test_ietf_RFC7748_20(&outC->Context_bench_x25519_5);
  /* _L4=(test::ietf_RFC7748::bench_x25519#4)/ */
  bench_x25519_reset_test_ietf_RFC7748_21(&outC->Context_bench_x25519_4);
  /* _L3=(test::ietf_RFC7748::bench_x25519#3)/ */
  bench_x25519_reset_test_ietf_RFC7748_19(&outC->Context_bench_x25519_3);
  /* _L2=(test::ietf_RFC7748::bench_x25519#2)/ */
  bench_x25519_reset_test_ietf_RFC7748_8(&outC->Context_bench_x25519_2);
  /* _L1=(test::ietf_RFC7748::bench_x25519#1)/ */
  bench_x25519_reset_test_ietf_RFC7748_1(&outC->Context_bench_x25519_1);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_x25519_algos_test_ietf_RFC7748.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

