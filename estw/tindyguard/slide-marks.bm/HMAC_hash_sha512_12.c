/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "HMAC_hash_sha512_12.h"

/* hash::sha512::HMAC/ */
void HMAC_hash_sha512_12(
  /* _L8/, msg/ */
  array_uint32_16_12 *msg_12,
  /* _L9/, mlen/ */
  int_slideTypes mlen_12,
  /* _L16/, _L4/, key/ */
  array_uint32_16_2 *key_12,
  /* _L20/, hmac/ */
  StreamChunk_slideTypes *hmac_12)
{
  array_uint32_16_3 tmp;
  kcg_size idx;
  array_uint32_16 tmp1;
  array_uint32_16_14 tmp2;
  array_uint32_16 tmp3;

  /* @1/_L2=, @2/_L2=, @3/_L2=, @4/_L2= */
  for (idx = 0; idx < 16; idx++) {
    tmp[0][idx] = (*key_12)[0][idx] ^ HMAC_outer_xor_hash_libsha;
    tmp1[idx] = (*key_12)[1][idx] ^ HMAC_outer_xor_hash_libsha;
    tmp2[0][idx] = (*key_12)[0][idx] ^ HMAC_inner_xor_hash_libsha;
    tmp3[idx] = (*key_12)[1][idx] ^ HMAC_inner_xor_hash_libsha;
  }
  kcg_copy_array_uint32_16(&tmp[1], &tmp1);
  kcg_copy_array_uint32_16(&tmp2[1], &tmp3);
  kcg_copy_array_uint32_16_12(&tmp2[2], msg_12);
  /* _L1=(hash::sha512::run#1)/ */
  run_hash_sha512_14(
    &tmp2,
    hBlockBytes_hash_sha512 + mlen_12,
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    &tmp[2]);
  /* _L20=(hash::sha512::run#2)/ */
  run_hash_sha512_3(
    &tmp,
    kcg_lit_int32(192),
    kcg_lit_uint32(0),
    kcg_lit_int32(0),
    hmac_12);
}

/*
  Expanded instances for: hash::sha512::HMAC/
  @1: (slideTypes::xorChunkConst#2)
  @2: (slideTypes::xorChunkConst#1)
  @3: (slideTypes::xorChunkConst#4)
  @4: (slideTypes::xorChunkConst#3)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** HMAC_hash_sha512_12.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

