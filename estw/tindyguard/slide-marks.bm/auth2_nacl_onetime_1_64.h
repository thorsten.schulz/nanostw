/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _auth2_nacl_onetime_1_64_H_
#define _auth2_nacl_onetime_1_64_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::onetime::auth2/ */
extern void auth2_nacl_onetime_1_64(
  /* _L47/, msg1/ */
  array_uint32_16_1 *msg1_1_64,
  /* _L46/, _L50/, _L55/, mlen1/ */
  int_slideTypes mlen1_1_64,
  /* _L12/, msg2/ */
  array_uint32_16_64 *msg2_1_64,
  /* _L15/, _L18/, _L56/, mlen2/ */
  int_slideTypes mlen2_1_64,
  /* _L2/, _L36/, k/ */
  StreamChunk_slideTypes *k_1_64,
  /* _L34/, a/ */
  Mac_nacl_onetime *a_1_64);



#endif /* _auth2_nacl_onetime_1_64_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** auth2_nacl_onetime_1_64.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

