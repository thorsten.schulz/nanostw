/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "finalize_hash_sha512.h"

/* hash::sha512::finalize/ */
void finalize_hash_sha512(
  /* _L101/, _L105/, _L121/, _L122/, zin/ */
  hashAkkuWithChunk_hash_sha512 *zin,
  /* _L115/, m/ */
  StreamChunk_slideTypes *m,
  /* _L137/, finalbits/ */
  kcg_uint32 finalbits,
  /* _L117/, _L135/, finalbitsLen/ */
  size_slideTypes finalbitsLen,
  /* _L100/, hash/ */
  StreamChunk_slideTypes *hash)
{
  StreamChunk_slideTypes tmp;
  kcg_uint32 tmp1;
  array_uint64_8 tmp2;
  array_uint64_8 tmp3;
  array_uint32_32 tmp4;
  /* @1/_L21/ */
  kcg_uint64 _L21_BE64toLEa_1;
  /* _L148/ */
  kcg_bool _L148;
  /* _L116/, _L132/, _L139/, m32/ */
  array_uint32_32 _L116;
  /* _L114/ */
  kcg_uint32 _L114;
  /* @1/_L18/, @1/_L27/, @1/be/, _L107/ */
  kcg_uint64 _L107;
  /* _L147/, _L97/, _L99/, bytesAppendix/ */
  kcg_int32 _L99;
  /* _L98/ */
  kcg_int32 _L98;
  kcg_size idx;

  _L107 = kcg_lsl_uint64(
      /* _L133= */(kcg_uint64) (*zin).length,
      kcg_lit_uint64(3)) + /* _L136= */(kcg_uint64) finalbitsLen;
  _L21_BE64toLEa_1 = _L107 >> kcg_lit_uint64(32);
  _L99 = (*zin).length % hBlockBytes_hash_sha512;
  _L148 = _L99 >= kcg_lit_int32(111);
  /* _L126= */
  if (_L99 > StreamChunkBytes_slideTypes) {
    kcg_copy_StreamChunk_slideTypes(&tmp, m);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  kcg_copy_StreamChunk_slideTypes(&tmp4[0], &(*zin).porch);
  kcg_copy_StreamChunk_slideTypes(&tmp4[16], &tmp);
  _L114 = (kcg_lit_uint32(128) >> finalbitsLen) | (finalbits & (kcg_lit_uint32(
          32512) >> finalbitsLen) & kcg_lit_uint32(254));
  _L98 = _L99 / kcg_lit_int32(4);
  kcg_copy_array_uint32_32(&_L116, &tmp4);
  /* _L131= */
  switch (_L99 % kcg_lit_int32(4)) {
    case kcg_lit_int32(0) :
      tmp1 = _L114;
      break;
    case kcg_lit_int32(1) :
      tmp1 = kcg_lsl_uint32(_L114, kcg_lit_uint32(8));
      break;
    case kcg_lit_int32(2) :
      tmp1 = kcg_lsl_uint32(_L114, kcg_lit_uint32(16));
      break;
    default :
      tmp1 = kcg_lsl_uint32(_L114, kcg_lit_uint32(24));
      break;
  }
  if (kcg_lit_int32(0) <= _L98 && _L98 < kcg_lit_int32(32)) {
    _L116[_L98] = tmp4[_L98] | tmp1;
  }
  /* _L138= */
  if (_L148) {
    /* _L130=(hash::sha512::block_refine#2)/ */
    block_refine_hash_sha512(&(*zin).z, &_L116, &tmp3);
    for (idx = 0; idx < 30; idx++) {
      tmp4[idx] = kcg_lit_uint32(0);
    }
  }
  else {
    kcg_copy_array_uint64_8(&tmp3, &(*zin).z);
    kcg_copy_array_uint32_30(&tmp4[0], (array_uint32_30 *) &_L116[0]);
  }
  tmp4[30] = ((/* @1/_L20= */(kcg_uint32) _L21_BE64toLEa_1 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @1/_L20= */(kcg_uint32)
          _L21_BE64toLEa_1 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @1/_L20= */(kcg_uint32) _L21_BE64toLEa_1 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L20= */(kcg_uint32) _L21_BE64toLEa_1 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  tmp4[31] = ((/* @1/_L28= */(kcg_uint32) _L107 & kcg_lit_uint32(4278190080)) >>
      kcg_lit_uint32(24)) | ((/* @1/_L28= */(kcg_uint32) _L107 & kcg_lit_uint32(
          16711680)) >> kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L28= */(kcg_uint32) _L107 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L28= */(kcg_uint32) _L107 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  /* _L108=(hash::sha512::block_refine#1)/ */
  block_refine_hash_sha512(&tmp3, &tmp4, &tmp2);
  /* _L100=(slideTypes::beHash2Chunk#1)/ */ beHash2Chunk_slideTypes(&tmp2, hash);
}

/*
  Expanded instances for: hash::sha512::finalize/
  @1: (slideTypes::BE64toLEa#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** finalize_hash_sha512.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

