/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "Ch_hash_libsha_uint64.h"

/* hash::libsha::Ch/ */
kcg_uint64 Ch_hash_libsha_uint64(
  /* _L1/, x/ */
  kcg_uint64 x_uint64,
  /* _L6/, y/ */
  kcg_uint64 y_uint64,
  /* _L2/, z/ */
  kcg_uint64 z_uint64)
{
  /* _L5/, ret/ */
  kcg_uint64 ret_uint64;

  ret_uint64 = z_uint64 ^ ((y_uint64 ^ z_uint64) & x_uint64);
  return ret_uint64;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Ch_hash_libsha_uint64.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

