/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _test_SHA512_it_test_ietf_RFC6234_H_
#define _test_SHA512_it_test_ietf_RFC6234_H_

#include "kcg_types.h"
#include "run_hash_sha512_12.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC6234::test_SHA512_it/ */
extern kcg_bool test_SHA512_it_test_ietf_RFC6234(
  /* _L2/, failing/ */
  kcg_bool failing,
  /* _L4/, tc/ */
  testcase_test_ietf_RFC6234 *tc);



#endif /* _test_SHA512_it_test_ietf_RFC6234_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_SHA512_it_test_ietf_RFC6234.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

