/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _unpack25519_nacl_op_H_
#define _unpack25519_nacl_op_H_

#include "kcg_types.h"
#include "unpack25519_it1_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::op::unpack25519/ */
extern void unpack25519_nacl_op(
  /* _L2/, i/ */
  Key32_slideTypes *i,
  /* _L4/, o/ */
  gf_nacl_op *o);



#endif /* _unpack25519_nacl_op_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unpack25519_nacl_op.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

