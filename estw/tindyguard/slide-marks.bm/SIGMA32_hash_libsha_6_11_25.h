/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _SIGMA32_hash_libsha_6_11_25_H_
#define _SIGMA32_hash_libsha_6_11_25_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::SIGMA32/ */
extern kcg_uint32 SIGMA32_hash_libsha_6_11_25(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, @3/_L11/, @3/x/, _L1/, x/ */
  kcg_uint32 x_6_11_25);

/*
  Expanded instances for: hash::libsha::SIGMA32/
  @1: (M::Ror32#1)
  @2: (M::Ror32#2)
  @3: (M::Ror32#3)
*/

#endif /* _SIGMA32_hash_libsha_6_11_25_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** SIGMA32_hash_libsha_6_11_25.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

