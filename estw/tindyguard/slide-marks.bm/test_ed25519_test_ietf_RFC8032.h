/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _test_ed25519_test_ietf_RFC8032_H_
#define _test_ed25519_test_ietf_RFC8032_H_

#include "kcg_types.h"
#include "verify_nacl_sign_2.h"
#include "sign_nacl_sign_2.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC8032::test_ed25519/ */
extern kcg_bool test_ed25519_test_ietf_RFC8032(
  /* _L17/, failing/ */
  kcg_bool failing,
  /* tc/ */
  testcase_test_ietf_RFC8032 *tc);



#endif /* _test_ed25519_test_ietf_RFC8032_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** test_ed25519_test_ietf_RFC8032.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

