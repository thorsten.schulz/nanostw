/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "Ch_hash_libsha_uint32.h"

/* hash::libsha::Ch/ */
kcg_uint32 Ch_hash_libsha_uint32(
  /* _L1/, x/ */
  kcg_uint32 x_uint32,
  /* _L6/, y/ */
  kcg_uint32 y_uint32,
  /* _L2/, z/ */
  kcg_uint32 z_uint32)
{
  /* _L5/, ret/ */
  kcg_uint32 ret_uint32;

  ret_uint32 = z_uint32 ^ ((y_uint32 ^ z_uint32) & x_uint32);
  return ret_uint32;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Ch_hash_libsha_uint32.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

