/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "modL_it3_nacl_sign.h"

/* nacl::sign::modL_it3/ */
void modL_it3_nacl_sign(
  /* _L1/, carry/ */
  kcg_int64 carry,
  /* _L2/, x/ */
  kcg_int64 x,
  /* _L3/, l/ */
  kcg_int64 l,
  /* _L4/, x31/ */
  kcg_int64 x31,
  /* @1/_L3/, @1/o/, _L18/, carryo/ */
  kcg_int64 *carryo,
  /* _L15/, xo/ */
  kcg_int64 *xo)
{
  /* @1/_L4/ */
  kcg_uint64 _L4_srs8_1;
  kcg_bool every_srs8_1;
  /* @1/_L1/, @1/i/, _L7/ */
  kcg_int64 _L7;

  _L7 = x + (carry - l * x31);
  every_srs8_1 = _L7 < kcg_lit_int64(0);
  _L4_srs8_1 = /* @1/_L2= */(kcg_uint64) _L7 >> kcg_lit_uint64(8);
  if (every_srs8_1) {
    *carryo = /* @1/_L3= */(kcg_int64)
        (kcg_lit_uint64(0xFF00000000000000) | _L4_srs8_1);
  }
  else {
    *carryo = /* @1/_L3= */(kcg_int64) _L4_srs8_1;
  }
  *xo = /* _L15= */(kcg_int64) /* _L16= */(kcg_uint8) _L7;
}

/*
  Expanded instances for: nacl::sign::modL_it3/
  @1: (nacl::op::srs8#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_it3_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

