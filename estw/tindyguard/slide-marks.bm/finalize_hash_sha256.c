/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "finalize_hash_sha256.h"

/* hash::sha256::finalize/ */
void finalize_hash_sha256(
  /* _L40/, zin/ */
  array_uint32_8 *zin,
  /* _L61/, m/ */
  StreamChunk_slideTypes *m,
  /* _L91/, _L93/, len/ */
  size_slideTypes len,
  /* _L73/, finalbits/ */
  kcg_uint32 finalbits,
  /* _L74/, _L89/, finalbitsLen/ */
  size_slideTypes finalbitsLen,
  /* _L94/, hash/ */
  StreamChunk_slideTypes *hash)
{
  kcg_uint32 tmp;
  array_uint32_8 tmp1;
  array_uint32_8 tmp2;
  array_uint32_16 tmp3;
  kcg_size idx;
  /* @1/_L21/ */
  kcg_uint64 _L21_BE64toLEa_1;
  /* _L14/ */
  kcg_bool _L14;
  /* _L10/, _L38/, bytesAppendix/ */
  kcg_int32 _L38;
  /* _L48/ */
  kcg_int32 _L48;
  /* _L57/, _L62/, _L63/, m32/ */
  array_uint32_16 _L63;
  /* _L83/ */
  kcg_uint32 _L83;
  /* @1/_L18/, @1/_L27/, @1/be/, _L88/ */
  kcg_uint64 _L88;

  _L88 = kcg_lsl_uint64(/* _L25= */(kcg_uint64) len, kcg_lit_uint64(3)) +
    /* _L90= */(kcg_uint64) finalbitsLen;
  _L21_BE64toLEa_1 = _L88 >> kcg_lit_uint64(32);
  _L38 = len % hBlockBytes_hash_sha256;
  _L14 = _L38 >= kcg_lit_int32(55);
  _L83 = (kcg_lit_uint32(128) >> finalbitsLen) | (finalbits & (kcg_lit_uint32(
          32512) >> finalbitsLen) & kcg_lit_uint32(254));
  _L48 = _L38 / kcg_lit_int32(4);
  kcg_copy_StreamChunk_slideTypes(&_L63, m);
  /* _L51= */
  switch (_L38 % kcg_lit_int32(4)) {
    case kcg_lit_int32(0) :
      tmp = _L83;
      break;
    case kcg_lit_int32(1) :
      tmp = kcg_lsl_uint32(_L83, kcg_lit_uint32(8));
      break;
    case kcg_lit_int32(2) :
      tmp = kcg_lsl_uint32(_L83, kcg_lit_uint32(16));
      break;
    default :
      tmp = kcg_lsl_uint32(_L83, kcg_lit_uint32(24));
      break;
  }
  if (kcg_lit_int32(0) <= _L48 && _L48 < kcg_lit_int32(16)) {
    _L63[_L48] = (*m)[_L48] | tmp;
  }
  /* _L16= */
  if (_L14) {
    /* _L17=(hash::sha256::block_refine#2)/ */
    block_refine_hash_sha256(zin, &_L63, &tmp2);
    for (idx = 0; idx < 14; idx++) {
      tmp3[idx] = kcg_lit_uint32(0);
    }
  }
  else {
    kcg_copy_array_uint32_8(&tmp2, zin);
    kcg_copy_array_uint32_14(&tmp3[0], (array_uint32_14 *) &_L63[0]);
  }
  tmp3[14] = ((/* @1/_L20= */(kcg_uint32) _L21_BE64toLEa_1 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @1/_L20= */(kcg_uint32)
          _L21_BE64toLEa_1 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @1/_L20= */(kcg_uint32) _L21_BE64toLEa_1 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L20= */(kcg_uint32) _L21_BE64toLEa_1 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  tmp3[15] = ((/* @1/_L28= */(kcg_uint32) _L88 & kcg_lit_uint32(4278190080)) >>
      kcg_lit_uint32(24)) | ((/* @1/_L28= */(kcg_uint32) _L88 & kcg_lit_uint32(
          16711680)) >> kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L28= */(kcg_uint32) _L88 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L28= */(kcg_uint32) _L88 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  /* _L1=(hash::sha256::block_refine#1)/ */
  block_refine_hash_sha256(&tmp2, &tmp3, &tmp1);
  /* _L92= */
  for (idx = 0; idx < 8; idx++) {
    (*hash)[idx] = ((tmp1[idx] & kcg_lit_uint32(4278190080)) >> kcg_lit_uint32(
          24)) | ((tmp1[idx] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
      kcg_lsl_uint32(tmp1[idx] & kcg_lit_uint32(65280), kcg_lit_uint32(8)) |
      kcg_lsl_uint32(tmp1[idx] & kcg_lit_uint32(255), kcg_lit_uint32(24));
  }
  for (idx = 0; idx < 8; idx++) {
    (*hash)[idx + 8] = kcg_lit_uint32(0);
  }
}

/*
  Expanded instances for: hash::sha256::finalize/
  @1: (slideTypes::BE64toLEa#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** finalize_hash_sha256.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

