/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "LEtoBE64a_slideTypes_16.h"

/* slideTypes::LEtoBE64a/ */
void LEtoBE64a_slideTypes_16(
  /* LE/, _L1/ */
  array_uint32_32 *LE_16,
  /* BE/, _L4/ */
  array_uint64_16 *BE_16)
{
  kcg_size idx;
  kcg_int32 idx_LEtoBE64a_it_1;
  kcg_int32 _1_idx_LEtoBE64a_it_1;
  /* @1/_L2/, @2/_L11/, @2/be/ */
  kcg_uint32 _L2_LEtoBE64a_it_1_16;
  /* @1/_L4/, @3/_L11/, @3/be/ */
  kcg_uint32 _L4_LEtoBE64a_it_1_16;

  /* _L4= */
  for (idx = 0; idx < 16; idx++) {
    _1_idx_LEtoBE64a_it_1 = /* _L4= */(kcg_int32) idx * kcg_lit_int32(2);
    idx_LEtoBE64a_it_1 = _1_idx_LEtoBE64a_it_1 + kcg_lit_int32(1);
    if (kcg_lit_int32(0) <= idx_LEtoBE64a_it_1 && idx_LEtoBE64a_it_1 <
      kcg_lit_int32(32)) {
      _L4_LEtoBE64a_it_1_16 = (*LE_16)[idx_LEtoBE64a_it_1];
    }
    else {
      _L4_LEtoBE64a_it_1_16 = kcg_lit_uint32(0);
    }
    if (kcg_lit_int32(0) <= _1_idx_LEtoBE64a_it_1 && _1_idx_LEtoBE64a_it_1 <
      kcg_lit_int32(32)) {
      _L2_LEtoBE64a_it_1_16 = (*LE_16)[_1_idx_LEtoBE64a_it_1];
    }
    else {
      _L2_LEtoBE64a_it_1_16 = kcg_lit_uint32(0);
    }
    (*BE_16)[idx] = kcg_lsl_uint64(
        /* @1/_L7= */(kcg_uint64)
          (((_L2_LEtoBE64a_it_1_16 & kcg_lit_uint32(4278190080)) >>
              kcg_lit_uint32(24)) | ((_L2_LEtoBE64a_it_1_16 & kcg_lit_uint32(
                  16711680)) >> kcg_lit_uint32(8)) | kcg_lsl_uint32(
              _L2_LEtoBE64a_it_1_16 & kcg_lit_uint32(65280),
              kcg_lit_uint32(8)) | kcg_lsl_uint32(
              _L2_LEtoBE64a_it_1_16 & kcg_lit_uint32(255),
              kcg_lit_uint32(24))),
        kcg_lit_uint64(32)) | /* @1/_L8= */(kcg_uint64)
        (((_L4_LEtoBE64a_it_1_16 & kcg_lit_uint32(4278190080)) >>
            kcg_lit_uint32(24)) | ((_L4_LEtoBE64a_it_1_16 & kcg_lit_uint32(
                16711680)) >> kcg_lit_uint32(8)) | kcg_lsl_uint32(
            _L4_LEtoBE64a_it_1_16 & kcg_lit_uint32(65280),
            kcg_lit_uint32(8)) | kcg_lsl_uint32(
            _L4_LEtoBE64a_it_1_16 & kcg_lit_uint32(255),
            kcg_lit_uint32(24)));
  }
}

/*
  Expanded instances for: slideTypes::LEtoBE64a/
  @1: (slideTypes::LEtoBE64a_it#1)
  @2: @1/(slideTypes::BEtoLE#1)
  @3: @1/(slideTypes::BEtoLE#2)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** LEtoBE64a_slideTypes_16.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

