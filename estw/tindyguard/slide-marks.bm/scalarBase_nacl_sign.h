/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _scalarBase_nacl_sign_H_
#define _scalarBase_nacl_sign_H_

#include "kcg_types.h"
#include "scalarMult_nacl_sign.h"
#include "M_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::scalarBase/ */
extern void scalarBase_nacl_sign(
  /* _L11/, s/ */
  array_uint32_8 *s,
  /* _L1/, pout/ */
  cpoint_nacl_op *pout);



#endif /* _scalarBase_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarBase_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

