/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "modL_nacl_sign.h"

/* nacl::sign::modL/ */
void modL_nacl_sign(
  /* _L1/, x/ */
  array_int64_64 *x,
  /* @1/_L24/, @1/u32/, _L25/, rout/ */
  array_uint32_8 *rout)
{
  array_int64_64 acc;
  kcg_size idx;
  kcg_int64 acc1;
  /* @2/_L4/, @4/_L4/ */
  kcg_uint64 _L4_srs8_1_modL_it5_1_mapfold_4;
  kcg_bool every_srs8_1_modL_it5_1_mapfold_4;
  kcg_int64 tmp_mapfold_4;
  u848_slideTypes tmp;
  /* @2/_L8/ */
  kcg_uint64 _L8_srs4_1;
  kcg_int64 noname;
  /* _L3/ */
  array_int64_64 _L3;
  /* _L5/ */
  array_int64_32 _L5;
  /* _L4/ */
  kcg_int64 _L4;
  /* @1/_L1/, @1/m/, _L18/ */
  array_uint8_32 _L18;

  kcg_copy_array_int64_64(&_L3, x);
  /* _L3= */
  for (idx = 0; idx < 32; idx++) {
    kcg_copy_array_int64_64(&acc, &_L3);
    /* _L3=(nacl::sign::modL_it1#1)/ */
    modL_it1_nacl_sign(/* _L3= */(kcg_int32) idx, &acc, &_L3);
  }
  _L4_srs8_1_modL_it5_1_mapfold_4 = /* @2/_L2= */(kcg_uint64) _L3[31] >>
    kcg_lit_uint64(4);
  every_srs8_1_modL_it5_1_mapfold_4 = _L3[31] < kcg_lit_int64(0);
  if (every_srs8_1_modL_it5_1_mapfold_4) {
    _L8_srs4_1 = kcg_lit_uint64(0xF000000000000000) |
      _L4_srs8_1_modL_it5_1_mapfold_4;
  }
  else {
    _L8_srs4_1 = _L4_srs8_1_modL_it5_1_mapfold_4;
  }
  _L4 = kcg_lit_int64(0);
  /* _L4= */
  for (idx = 0; idx < 32; idx++) {
    noname = _L4;
    /* _L4=(nacl::sign::modL_it3#1)/ */
    modL_it3_nacl_sign(
      noname,
      _L3[idx],
      L_nacl_sign[idx],
      /* @2/_L3= */(kcg_int64) _L8_srs4_1,
      &_L4,
      &_L5[idx]);
  }
  noname = kcg_lit_int64(0);
  /* _L17= */
  for (idx = 0; idx < 32; idx++) {
    acc1 = noname;
    tmp_mapfold_4 = _L5[idx] - L_nacl_sign[idx] * _L4;
    every_srs8_1_modL_it5_1_mapfold_4 = tmp_mapfold_4 < kcg_lit_int64(0);
    _L4_srs8_1_modL_it5_1_mapfold_4 = /* @4/_L2= */(kcg_uint64) tmp_mapfold_4 >>
      kcg_lit_uint64(8);
    if (every_srs8_1_modL_it5_1_mapfold_4) {
      noname = /* @4/_L3= */(kcg_int64)
          (kcg_lit_uint64(0xFF00000000000000) | _L4_srs8_1_modL_it5_1_mapfold_4);
    }
    else {
      noname = /* @4/_L3= */(kcg_int64) _L4_srs8_1_modL_it5_1_mapfold_4;
    }
    _L18[idx] = /* @3/_L7= */(kcg_uint8) (acc1 + tmp_mapfold_4);
  }
  kcg_copy_array_uint8_4(&tmp[0], (array_uint8_4 *) &_L18[0]);
  kcg_copy_array_uint8_4(&tmp[1], (array_uint8_4 *) &_L18[4]);
  kcg_copy_array_uint8_4(&tmp[2], (array_uint8_4 *) &_L18[8]);
  kcg_copy_array_uint8_4(&tmp[3], (array_uint8_4 *) &_L18[12]);
  kcg_copy_array_uint8_4(&tmp[4], (array_uint8_4 *) &_L18[16]);
  kcg_copy_array_uint8_4(&tmp[5], (array_uint8_4 *) &_L18[20]);
  kcg_copy_array_uint8_4(&tmp[6], (array_uint8_4 *) &_L18[24]);
  kcg_copy_array_uint8_4(&tmp[7], (array_uint8_4 *) &_L18[28]);
  /* @1/_L24= */
  for (idx = 0; idx < 8; idx++) {
    (*rout)[idx] = /* @5/_L14= */(kcg_uint32) tmp[idx][0] | kcg_lsl_uint32(
        /* @5/_L15= */(kcg_uint32) tmp[idx][1],
        kcg_lit_uint32(8)) | kcg_lsl_uint32(
        /* @5/_L16= */(kcg_uint32) tmp[idx][2],
        kcg_lit_uint32(16)) | kcg_lsl_uint32(
        /* @5/_L17= */(kcg_uint32) tmp[idx][3],
        kcg_lit_uint32(24));
  }
}

/*
  Expanded instances for: nacl::sign::modL/
  @2: (nacl::op::srs4#1)
  @3: (nacl::sign::modL_it5#1)
  @4: @3/(nacl::op::srs8#1)
  @1: (slideTypes::ld32x8#2)
  @5: @1/(slideTypes::ld32x1#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

