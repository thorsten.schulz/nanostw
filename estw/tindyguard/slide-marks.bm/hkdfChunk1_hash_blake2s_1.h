/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hkdfChunk1_hash_blake2s_1_H_
#define _hkdfChunk1_hash_blake2s_1_H_

#include "kcg_types.h"
#include "hmacChunk_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hkdfChunk1/ */
extern void hkdfChunk1_hash_blake2s_1(
  /* @1/_L4/, @1/ikm/, _L4/, ikm/ */
  array_uint32_16_1 *ikm_1,
  /* @1/_L5/, @1/ikmlen/, _L5/, ikmlen/ */
  size_slideTypes ikmlen_1,
  /* @1/_L3/, @1/chainingKey/, _L3/, chainingKey/ */
  HashChunk_hash_blake2s *chainingKey_1,
  /* _L22/, cKout/ */
  HashChunk_hash_blake2s *cKout_1);

/*
  Expanded instances for: hash::blake2s::hkdfChunk1/
  @1: (hash::blake2s::hkdfChunk#1)
*/

#endif /* _hkdfChunk1_hash_blake2s_1_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdfChunk1_hash_blake2s_1.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

