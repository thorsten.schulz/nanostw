/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _verify_nacl_sign_2_H_
#define _verify_nacl_sign_2_H_

#include "kcg_types.h"
#include "verify_init_nacl_sign.h"
#include "verify_finalize_nacl_sign.h"
#include "stream_it_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::verify/ */
extern kcg_bool verify_nacl_sign_2(
  /* _L23/, _L35/, msg/ */
  array_uint32_16_2 *msg_2,
  /* _L24/, len/ */
  size_slideTypes len_2,
  /* _L19/, _L20/, pk/ */
  Key32_slideTypes *pk_2,
  /* _L21/, _L22/, signature/ */
  Signature_nacl_sign *signature_2);



#endif /* _verify_nacl_sign_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** verify_nacl_sign_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

