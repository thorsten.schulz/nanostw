/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _stream_it_hash_sha512_H_
#define _stream_it_hash_sha512_H_

#include "kcg_types.h"
#include "block_refine_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha512::stream_it/ */
extern void stream_it_hash_sha512(
  /* i/ */
  size_slideTypes i,
  /* zin/ */
  hashAkkuWithChunk_hash_sha512 *zin,
  /* m/ */
  StreamChunk_slideTypes *m,
  /* goOn/ */
  kcg_bool *goOn,
  /* zout/ */
  hashAkkuWithChunk_hash_sha512 *zout);



#endif /* _stream_it_hash_sha512_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** stream_it_hash_sha512.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

