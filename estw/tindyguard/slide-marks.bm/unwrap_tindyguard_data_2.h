/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _unwrap_tindyguard_data_2_H_
#define _unwrap_tindyguard_data_2_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_2_1_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::unwrap/ */
extern void unwrap_tindyguard_data_2(
  /* _L131/, s/ */
  Session_tindyguardTypes *s_2,
  /* fail_in/ */
  kcg_bool fail_in_2,
  /* rlength/ */
  size_slideTypes rlength_2,
  /* datamsg/ */
  array_uint32_16_3 *datamsg_2,
  /* endpoint/ */
  peer_t_udp *endpoint_2,
  /* soo/ */
  Session_tindyguardTypes *soo_2,
  /* fail_flag/ */
  kcg_bool *fail_flag_2,
  /* len/ */
  length_t_udp *len_2,
  /* msg/ */
  array_uint32_16_2 *msg_2);



#endif /* _unwrap_tindyguard_data_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unwrap_tindyguard_data_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

