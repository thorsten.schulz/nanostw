/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "singleChunk_hash_blake2s.h"

/* hash::blake2s::singleChunk/ */
void singleChunk_hash_blake2s(
  /* @1/_L64/, @1/msg/, @2/m/, _L64/, msg/ */
  StreamChunk_slideTypes *msg,
  /* @1/_L66/, @1/len/, @2/_L5/, @2/len/, _L66/, len/ */
  size_slideTypes len,
  /* _L65/, chunk/ */
  HashChunk_hash_blake2s *chunk)
{
  array_uint32_8 tmp;
  kcg_int32 tmp1;
  /* @1/_L61/, @2/_L10/, @2/goOn/ */
  kcg_bool _L10_stream_it_1_single_1;
  kcg_size idx;

  _L10_stream_it_1_single_1 = len > kcg_lit_int32(64);
  tmp[0] = kcg_lit_uint32(1795745351);
  kcg_copy_array_uint32_7(&tmp[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* @2/_L14= */
  if (_L10_stream_it_1_single_1) {
    tmp1 = kcg_lit_int32(64);
  }
  else {
    tmp1 = len;
  }
  /* @2/_L2=(hash::blake2s::block_refine#1)/ */
  block_refine_hash_blake2s(
    &tmp,
    tmp1,
    msg,
    _L10_stream_it_1_single_1,
    (array_uint32_8 *) &(*chunk)[0]);
  for (idx = 0; idx < 8; idx++) {
    (*chunk)[idx + 8] = kcg_lit_uint32(0);
  }
}

/*
  Expanded instances for: hash::blake2s::singleChunk/
  @1: (hash::blake2s::single#1)
  @2: @1/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** singleChunk_hash_blake2s.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

