/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_finalize_it1_nacl_sign.h"

/* nacl::sign::sign_finalize_it1/ */
void sign_finalize_it1_nacl_sign(
  /* _L1/, i/ */
  int_slideTypes i,
  /* _L2/, a/ */
  array_int64_64 *a,
  /* @1/_L1/, @1/u32/, _L3/, h/ */
  kcg_uint32 h,
  /* _L4/, d/ */
  array_uint8_32 *d,
  /* _L5/, aoo/ */
  array_int64_64 *aoo)
{
  array_uint8_4 tmp;
  array_int64_64 acc;
  kcg_size idx;
  /* _L27/ */
  kcg_int32 _L27;
  /* @1/_L15/ */
  kcg_uint32 _L15_st32_2;
  /* @1/_L14/ */
  kcg_uint32 _L14_st32_2;

  tmp[0] = /* @1/_L8= */(kcg_uint8) h;
  _L14_st32_2 = h >> kcg_lit_uint32(8);
  tmp[1] = /* @1/_L7= */(kcg_uint8) _L14_st32_2;
  _L15_st32_2 = _L14_st32_2 >> kcg_lit_uint32(8);
  tmp[2] = /* @1/_L6= */(kcg_uint8) _L15_st32_2;
  tmp[3] = /* @1/_L5= */(kcg_uint8) (_L15_st32_2 >> kcg_lit_uint32(8));
  _L27 = i * kcg_lit_int32(4);
  kcg_copy_array_int64_64(aoo, a);
  /* _L5= */
  for (idx = 0; idx < 4; idx++) {
    kcg_copy_array_int64_64(&acc, aoo);
    /* _L5=(nacl::sign::sign_finalize_it2#1)/ */
    sign_finalize_it2_nacl_sign(
      /* _L5= */(kcg_int32) idx,
      &acc,
      _L27,
      tmp[idx],
      d,
      aoo);
  }
}

/*
  Expanded instances for: nacl::sign::sign_finalize_it1/
  @1: (slideTypes::st32#2)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_finalize_it1_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

