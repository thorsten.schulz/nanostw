/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdf_expand_it2_hash_sha256_2.h"

/* hash::sha256::hkdf_expand_it2/ */
void hkdf_expand_it2_hash_sha256_2(
  /* @1/_L1/, @1/i/, _L48/, _L66/, _L7/, i/ */
  size_slideTypes i_2,
  /* _L26/, hin/ */
  array_uint32_8 *hin_2,
  /* _L3/, prk/ */
  StreamChunk_slideTypes *prk_2,
  /* _L51/, m0/ */
  array_uint32_16_2 *m0_2,
  /* _L21/, _L36/, m/ */
  array_uint32_16_2 *m_2,
  /* _L29/, _L43/, infolen/ */
  size_slideTypes infolen_2,
  /* _L13/, okmlen/ */
  size_slideTypes okmlen_2,
  /* _L54/, orig/ */
  kcg_uint32 orig_2,
  /* x/ */
  size_slideTypes x_2,
  /* y/ */
  size_slideTypes y_2,
  /* _L58/, _L60/, z/ */
  kcg_uint32 z_2,
  /* _L12/, goOn/ */
  kcg_bool *goOn_2,
  /* _L20/, hoo/ */
  array_uint32_8 *hoo_2,
  /* _L16/, okm/ */
  StreamChunk_slideTypes *okm_2)
{
  kcg_int32 tmp;
  StreamChunk_slideTypes tmp1;
  array_uint32_16_2 tmp2;
  /* _L55/ */
  kcg_uint32 _L55_2;
  /* _L39/ */
  kcg_int32 _L39_2;
  /* _L38/ */
  array_uint32_16 _L38_2;
  /* _L24/ */
  array_uint32_8 _L24_2;
  /* _L19/ */
  array_uint32_16_1 _L19_2;
  /* _L18/, _L47/ */
  kcg_bool _L18_2;

  _L55_2 = orig_2 | (z_2 * /* _L62= */(kcg_uint32)
        (kcg_lit_int32(2) * i_2 + kcg_lit_int32(1)));
  _L18_2 = i_2 == kcg_lit_int32(0);
  _L39_2 = kcg_lit_int32(1) + infolen_2 + Bytes_hash_sha256;
  kcg_copy_array_uint32_8(&_L24_2, (array_uint32_8 *) &(*m_2)[0][8]);
  kcg_copy_StreamChunk_slideTypes(&_L19_2[0], prk_2);
  /* _L46= */
  if (_L18_2) {
    kcg_copy_array_uint32_16_2(&tmp2, m0_2);
    tmp = kcg_lit_int32(1) + infolen_2;
  }
  else {
    kcg_copy_array_uint32_16_2(&tmp2, m_2);
    kcg_copy_array_uint32_8(&tmp2[0][0], hin_2);
    kcg_copy_array_uint32_8(&tmp2[0][8], &_L24_2);
    if (kcg_lit_int32(0) <= y_2 && y_2 < kcg_lit_int32(2) && (kcg_lit_int32(
          0) <= x_2 && x_2 < kcg_lit_int32(16))) {
      tmp2[y_2][x_2] = _L55_2;
    }
    tmp = _L39_2;
  }
  /* _L1=(hash::sha256::HMAC#1)/ */
  HMAC_hash_sha256_2_1(&tmp2, tmp, &_L19_2, Bytes_hash_sha256, &tmp1);
  kcg_copy_array_uint32_8(&_L38_2[0], (array_uint32_8 *) &tmp1[0]);
  kcg_copy_array_uint32_16_2(&tmp2, m_2);
  kcg_copy_array_uint32_8(&tmp2[0][0], (array_uint32_8 *) &_L38_2[0]);
  kcg_copy_array_uint32_8(&tmp2[0][8], &_L24_2);
  if (kcg_lit_int32(0) <= y_2 && y_2 < kcg_lit_int32(2) && (kcg_lit_int32(0) <=
      x_2 && x_2 < kcg_lit_int32(16))) {
    tmp2[y_2][x_2] = _L55_2 + z_2;
  }
  /* _L31=(hash::sha256::HMAC#2)/ */
  HMAC_hash_sha256_2_1(&tmp2, _L39_2, &_L19_2, Bytes_hash_sha256, &tmp1);
  kcg_copy_array_uint32_8(hoo_2, (array_uint32_8 *) &tmp1[0]);
  kcg_copy_array_uint32_8(&_L38_2[8], hoo_2);
  *goOn_2 = okmlen_2 > (i_2 + kcg_lit_int32(1)) * StreamChunkBytes_slideTypes;
  _L18_2 = !*goOn_2;
  if (_L18_2) {
    /* _L16=(slideTypes::truncate#1)/ */
    truncate_slideTypes(&_L38_2, okmlen_2, okm_2);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(okm_2, &_L38_2);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_expand_it2_hash_sha256_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

