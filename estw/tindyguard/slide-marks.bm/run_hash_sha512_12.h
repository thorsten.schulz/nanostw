/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _run_hash_sha512_12_H_
#define _run_hash_sha512_12_H_

#include "kcg_types.h"
#include "stream_it_hash_sha512.h"
#include "finalize_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha512::run/ */
extern void run_hash_sha512_12(
  /* _L32/, _L39/, msg/ */
  array_uint32_16_12 *msg_12,
  /* _L34/, _L41/, len/ */
  size_slideTypes len_12,
  /* finalbits/ */
  kcg_uint32 finalbits_12,
  /* finalbitsLen/ */
  size_slideTypes finalbitsLen_12,
  /* _L33/, hash/ */
  StreamChunk_slideTypes *hash_12);



#endif /* _run_hash_sha512_12_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** run_hash_sha512_12.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

