/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdf_expand_it2_hash_sha256_3.h"

/* hash::sha256::hkdf_expand_it2/ */
void hkdf_expand_it2_hash_sha256_3(
  /* @1/_L1/, @1/i/, _L48/, _L66/, _L7/, i/ */
  size_slideTypes i_3,
  /* _L26/, hin/ */
  array_uint32_8 *hin_3,
  /* _L3/, prk/ */
  StreamChunk_slideTypes *prk_3,
  /* _L51/, m0/ */
  array_uint32_16_3 *m0_3,
  /* _L21/, _L36/, m/ */
  array_uint32_16_3 *m_3,
  /* _L29/, _L43/, infolen/ */
  size_slideTypes infolen_3,
  /* _L13/, okmlen/ */
  size_slideTypes okmlen_3,
  /* _L54/, orig/ */
  kcg_uint32 orig_3,
  /* x/ */
  size_slideTypes x_3,
  /* y/ */
  size_slideTypes y_3,
  /* _L58/, _L60/, z/ */
  kcg_uint32 z_3,
  /* _L12/, goOn/ */
  kcg_bool *goOn_3,
  /* _L20/, hoo/ */
  array_uint32_8 *hoo_3,
  /* _L16/, okm/ */
  StreamChunk_slideTypes *okm_3)
{
  kcg_int32 tmp;
  StreamChunk_slideTypes tmp1;
  array_uint32_16_3 tmp2;
  /* _L55/ */
  kcg_uint32 _L55_3;
  /* _L39/ */
  kcg_int32 _L39_3;
  /* _L38/ */
  array_uint32_16 _L38_3;
  /* _L24/ */
  array_uint32_8 _L24_3;
  /* _L19/ */
  array_uint32_16_1 _L19_3;
  /* _L18/, _L47/ */
  kcg_bool _L18_3;

  _L55_3 = orig_3 | (z_3 * /* _L62= */(kcg_uint32)
        (kcg_lit_int32(2) * i_3 + kcg_lit_int32(1)));
  _L18_3 = i_3 == kcg_lit_int32(0);
  _L39_3 = kcg_lit_int32(1) + infolen_3 + Bytes_hash_sha256;
  kcg_copy_array_uint32_8(&_L24_3, (array_uint32_8 *) &(*m_3)[0][8]);
  kcg_copy_StreamChunk_slideTypes(&_L19_3[0], prk_3);
  /* _L46= */
  if (_L18_3) {
    kcg_copy_array_uint32_16_3(&tmp2, m0_3);
    tmp = kcg_lit_int32(1) + infolen_3;
  }
  else {
    kcg_copy_array_uint32_16_3(&tmp2, m_3);
    kcg_copy_array_uint32_8(&tmp2[0][0], hin_3);
    kcg_copy_array_uint32_8(&tmp2[0][8], &_L24_3);
    if (kcg_lit_int32(0) <= y_3 && y_3 < kcg_lit_int32(3) && (kcg_lit_int32(
          0) <= x_3 && x_3 < kcg_lit_int32(16))) {
      tmp2[y_3][x_3] = _L55_3;
    }
    tmp = _L39_3;
  }
  /* _L1=(hash::sha256::HMAC#1)/ */
  HMAC_hash_sha256_3_1(&tmp2, tmp, &_L19_3, Bytes_hash_sha256, &tmp1);
  kcg_copy_array_uint32_8(&_L38_3[0], (array_uint32_8 *) &tmp1[0]);
  kcg_copy_array_uint32_16_3(&tmp2, m_3);
  kcg_copy_array_uint32_8(&tmp2[0][0], (array_uint32_8 *) &_L38_3[0]);
  kcg_copy_array_uint32_8(&tmp2[0][8], &_L24_3);
  if (kcg_lit_int32(0) <= y_3 && y_3 < kcg_lit_int32(3) && (kcg_lit_int32(0) <=
      x_3 && x_3 < kcg_lit_int32(16))) {
    tmp2[y_3][x_3] = _L55_3 + z_3;
  }
  /* _L31=(hash::sha256::HMAC#2)/ */
  HMAC_hash_sha256_3_1(&tmp2, _L39_3, &_L19_3, Bytes_hash_sha256, &tmp1);
  kcg_copy_array_uint32_8(hoo_3, (array_uint32_8 *) &tmp1[0]);
  kcg_copy_array_uint32_8(&_L38_3[8], hoo_3);
  *goOn_3 = okmlen_3 > (i_3 + kcg_lit_int32(1)) * StreamChunkBytes_slideTypes;
  _L18_3 = !*goOn_3;
  if (_L18_3) {
    /* _L16=(slideTypes::truncate#1)/ */
    truncate_slideTypes(&_L38_3, okmlen_3, okm_3);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(okm_3, &_L38_3);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_expand_it2_hash_sha256_3.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

