/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "verify_nacl_sign_2.h"

/* nacl::sign::verify/ */
kcg_bool verify_nacl_sign_2(
  /* _L23/, _L35/, msg/ */
  array_uint32_16_2 *msg_2,
  /* _L24/, len/ */
  size_slideTypes len_2,
  /* _L19/, _L20/, pk/ */
  Key32_slideTypes *pk_2,
  /* _L21/, _L22/, signature/ */
  Signature_nacl_sign *signature_2)
{
  hashAkkuWithChunk_hash_sha512 acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  StreamChunk_slideTypes tmp;
  /* _L30/ */
  kcg_bool _L30_2;
  /* _L32/ */
  size_slideTypes _L32_2;
  /* _L33/ */
  hashAkkuWithChunk_hash_sha512 _L33_2;
  /* _L18/, failed/ */
  kcg_bool failed_2;

  /* _L30=(nacl::sign::verify_init#1)/ */
  verify_init_nacl_sign(signature_2, pk_2, len_2, &_L30_2, &_L33_2);
  /* _L32= */
  if (_L30_2) {
    /* _L32= */
    for (idx = 0; idx < 2; idx++) {
      kcg_copy_hashAkkuWithChunk_hash_sha512(&acc, &_L33_2);
      /* _L32=(hash::sha512::stream_it#1)/ */
      stream_it_hash_sha512(
        /* _L32= */(kcg_int32) idx,
        &acc,
        &(*msg_2)[idx],
        &cond_iterw,
        &_L33_2);
      _L32_2 = /* _L32= */(kcg_int32) (idx + 1);
      /* _L32= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    _L32_2 = kcg_lit_int32(0);
  }
  if (kcg_lit_int32(0) <= _L32_2 && _L32_2 < kcg_lit_int32(2)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &(*msg_2)[_L32_2]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  failed_2 = /* _L18=(nacl::sign::verify_finalize#1)/ */
    verify_finalize_nacl_sign(signature_2, pk_2, &tmp, &_L33_2);
  return failed_2;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** verify_nacl_sign_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

