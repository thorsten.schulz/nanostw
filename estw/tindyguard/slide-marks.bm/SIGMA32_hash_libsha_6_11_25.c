/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "SIGMA32_hash_libsha_6_11_25.h"

/* hash::libsha::SIGMA32/ */
kcg_uint32 SIGMA32_hash_libsha_6_11_25(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, @3/_L11/, @3/x/, _L1/, x/ */
  kcg_uint32 x_6_11_25)
{
  /* _L6/, ret/ */
  kcg_uint32 ret_6_11_25;

  ret_6_11_25 = ((x_6_11_25 >> kcg_lit_uint32(6)) | kcg_lsl_uint32(
        x_6_11_25,
        kcg_lit_uint32(26))) ^ ((x_6_11_25 >> kcg_lit_uint32(11)) |
      kcg_lsl_uint32(x_6_11_25, kcg_lit_uint32(21))) ^ ((x_6_11_25 >>
        kcg_lit_uint32(25)) | kcg_lsl_uint32(x_6_11_25, kcg_lit_uint32(7)));
  return ret_6_11_25;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** SIGMA32_hash_libsha_6_11_25.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

