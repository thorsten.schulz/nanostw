/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _add_nacl_sign_H_
#define _add_nacl_sign_H_

#include "kcg_types.h"
#include "A_nacl_op.h"
#include "M_nacl_op.h"
#include "Z_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::add/ */
extern void add_nacl_sign(
  /* _L26/, _L37/, _L41/, _L5/, q/ */
  cpoint_nacl_op *q,
  /* _L19/, _L24/, _L36/, _L42/, p/ */
  cpoint_nacl_op *p,
  /* _L66/, s/ */
  cpoint_nacl_op *s);



#endif /* _add_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** add_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

