/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hmacChunk_hash_blake2s_2.h"

/* hash::blake2s::hmacChunk/ */
void hmacChunk_hash_blake2s_2(
  /* @1/_L8/, @1/msg/, _L8/, msg/ */
  array_uint32_16_2 *msg_2,
  /* @1/_L9/, @1/mlen/, _L9/, mlen/ */
  int_slideTypes mlen_2,
  /* @1/_L32/, @1/key/, @2/_L1/, @2/chunk/, @3/_L1/, @3/chunk/, _L32/, key/ */
  HashChunk_hash_blake2s *key_2,
  /* _L34/, chunk/ */
  HashChunk_hash_blake2s *chunk_2)
{
  array_uint32_16_2 tmp;
  kcg_size idx;
  array_uint32_16_3 tmp1;
  array_uint32_8 acc;
  kcg_bool cond_iterw;
  /* @5/_L8/ */
  kcg_int32 _L8_stream_it_1_hash_1_hmac_1;
  kcg_int32 tmp_stream_it_1_hash_1_hmac_1;
  /* @1/_L20/, @1/hmac/, @4/_L61/, @4/hash/, _L33/ */
  Hash_hash_blake2s _L33_2;

  _L33_2[0] = kcg_lit_uint32(1795745351);
  kcg_copy_array_uint32_7(&_L33_2[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* @2/_L2=, @3/_L2= */
  for (idx = 0; idx < 16; idx++) {
    tmp[0][idx] = (*key_2)[idx] ^ HMAC_outer_xor_hash_libsha;
    tmp1[0][idx] = (*key_2)[idx] ^ HMAC_inner_xor_hash_libsha;
  }
  kcg_copy_array_uint32_16_2(&tmp1[1], msg_2);
  /* @1/_L1=(hash::blake2s::hashChunk#1)/ */
  hashChunk_hash_blake2s_3(
    &tmp1,
    hBlockBytes_hash_blake2s + mlen_2,
    kcg_lit_int32(0),
    &tmp[1]);
  /* @4/_L60= */
  for (idx = 0; idx < 2; idx++) {
    kcg_copy_Hash_hash_blake2s(&acc, &_L33_2);
    _L8_stream_it_1_hash_1_hmac_1 = /* @4/_L60= */(kcg_int32) idx *
      hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
    cond_iterw = kcg_lit_int32(96) > _L8_stream_it_1_hash_1_hmac_1;
    /* @5/_L14= */
    if (cond_iterw) {
      tmp_stream_it_1_hash_1_hmac_1 = _L8_stream_it_1_hash_1_hmac_1;
    }
    else {
      tmp_stream_it_1_hash_1_hmac_1 = kcg_lit_int32(96);
    }
    /* @5/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &acc,
      tmp_stream_it_1_hash_1_hmac_1,
      &tmp[idx],
      cond_iterw,
      &_L33_2);
    /* @4/_L60= */
    if (!cond_iterw) {
      break;
    }
  }
  kcg_copy_array_uint32_8(&(*chunk_2)[0], (array_uint32_8 *) &_L33_2[0]);
  for (idx = 0; idx < 8; idx++) {
    (*chunk_2)[idx + 8] = kcg_lit_uint32(0);
  }
}

/*
  Expanded instances for: hash::blake2s::hmacChunk/
  @1: (hash::blake2s::hmac#1)
  @3: @1/(slideTypes::xorChunkConst#1)
  @2: @1/(slideTypes::xorChunkConst#2)
  @4: @1/(hash::blake2s::hash#1)
  @5: @4/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hmacChunk_hash_blake2s_2.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

