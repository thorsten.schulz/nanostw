/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sign_nacl_sign_2_H_
#define _sign_nacl_sign_2_H_

#include "kcg_types.h"
#include "sign_block2_nacl_sign.h"
#include "sign_block1_nacl_sign.h"
#include "sign_finalize_nacl_sign.h"
#include "sign_halftime_nacl_sign.h"
#include "sign_init_nacl_sign.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::sign/ */
extern void sign_nacl_sign_2(
  /* _L23/, _L59/, _L62/, _L71/, msg/ */
  array_uint32_16_2 *msg_2,
  /* _L24/, len/ */
  kcg_int32 len_2,
  /* _L19/, _L40/, key/ */
  KeyPair32_slideTypes *key_2,
  /* _L41/, signature/ */
  Signature_nacl_sign *signature_2);



#endif /* _sign_nacl_sign_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_nacl_sign_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

