/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "bench_hash_test_ietf_RFC7693_64.h"

/* test::ietf_RFC7693::bench_hash/ */
kcg_bool bench_hash_test_ietf_RFC7693_64(
  /* _L1/, failing/ */
  kcg_bool failing_64,
  /* tc/ */
  simpleTC_test *tc_64)
{
  kcg_bool cond_iterw;
  kcg_size idx;
  /* @2/_L8/ */
  kcg_int32 _L8_stream_it_1_hash_2;
  kcg_int32 tmp_stream_it_1_hash_2;
  /* _L18/ */
  array_uint32_8 _L18_64;
  /* @1/_L66/, @1/len/, _L16/ */
  kcg_int32 _L16_64;
  /* @1/_L61/, @1/hash/, _L5/ */
  Hash_hash_blake2s _L5_64;
  /* _L2/, failed/ */
  kcg_bool failed_64;

  _L16_64 = kcg_lit_int32(4032) + (*tc_64).mlen;
  _L5_64[0] = kcg_lit_uint32(1795745351);
  kcg_copy_array_uint32_7(&_L5_64[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* @1/_L60= */
  for (idx = 0; idx < 64; idx++) {
    kcg_copy_Hash_hash_blake2s(&_L18_64, &_L5_64);
    _L8_stream_it_1_hash_2 = /* @1/_L60= */(kcg_int32) idx *
      hBlockBytes_hash_blake2s + hBlockBytes_hash_blake2s;
    cond_iterw = _L16_64 > _L8_stream_it_1_hash_2;
    /* @2/_L14= */
    if (cond_iterw) {
      tmp_stream_it_1_hash_2 = _L8_stream_it_1_hash_2;
    }
    else {
      tmp_stream_it_1_hash_2 = _L16_64;
    }
    /* @2/_L2=(hash::blake2s::block_refine#1)/ */
    block_refine_hash_blake2s(
      &_L18_64,
      tmp_stream_it_1_hash_2,
      &(*tc_64).m[0],
      cond_iterw,
      &_L5_64);
    /* @1/_L60= */
    if (!cond_iterw) {
      break;
    }
  }
  for (idx = 0; idx < 8; idx++) {
    _L18_64[idx] = kcg_lit_uint32(0);
  }
  failed_64 = failing_64 || kcg_comp_Hash_hash_blake2s(&_L5_64, &_L18_64);
  return failed_64;
}

/*
  Expanded instances for: test::ietf_RFC7693::bench_hash/
  @1: (hash::blake2s::hash#2)
  @2: @1/(hash::blake2s::stream_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_hash_test_ietf_RFC7693_64.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

