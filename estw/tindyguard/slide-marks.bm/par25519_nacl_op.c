/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "par25519_nacl_op.h"

/* nacl::op::par25519/ */
kcg_uint32 par25519_nacl_op(/* _L2/, i/ */ gf_nacl_op *i)
{
  array_uint32_8 tmp;
  /* _L3/, o/ */
  kcg_uint32 o;

  /* _L1=(nacl::op::pack25519#1)/ */ pack25519_nacl_op(i, &tmp);
  o = tmp[0] & kcg_lit_uint32(1);
  return o;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** par25519_nacl_op.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

