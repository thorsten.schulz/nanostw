/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarBase_nacl_sign.h"

/* nacl::sign::scalarBase/ */
void scalarBase_nacl_sign(
  /* _L11/, s/ */
  array_uint32_8 *s,
  /* _L1/, pout/ */
  cpoint_nacl_op *pout)
{
  cpoint_nacl_op tmp;

  kcg_copy_gf_nacl_op(&tmp[0], (gf_nacl_op *) &X_nacl_op);
  kcg_copy_gf_nacl_op(&tmp[1], (gf_nacl_op *) &Y_nacl_op);
  kcg_copy_gf_nacl_op(&tmp[2], (gf_nacl_op *) &gf1_nacl_op);
  /* _L12=(nacl::op::M#1)/ */
  M_nacl_op((gf_nacl_op *) &X_nacl_op, (gf_nacl_op *) &Y_nacl_op, &tmp[3]);
  /* _L1=(nacl::sign::scalarMult#1)/ */ scalarMult_nacl_sign(&tmp, s, pout);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarBase_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

