/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _pullAll_test_H_
#define _pullAll_test_H_

#include "kcg_types.h"
#include "bench_test_ietf_RFC7539.h"
#include "test_test_ietf_RFC6234.h"
#include "test_test_ietf_RFC7693.h"
#include "tests_test_ietf_RFC8032.h"
#include "test_x25519_algos_test_ietf_RFC7748.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* -----------------------  no local memory  ----------------------- */
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_test_x25519_algos_test_ietf_RFC7748 /* _L2=(test::ietf_RFC7748::test_x25519_algos#1)/ */ Context_test_x25519_algos_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_pullAll_test;

/* ===========  node initialization and cycle functions  =========== */
/* test::pullAll/ */
extern void pullAll_test(
  /* _L3/, failed/ */
  kcg_bool *failed,
  /* _L2/, failed2/ */
  kcg_bool *failed2,
  outC_pullAll_test *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void pullAll_reset_test(outC_pullAll_test *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void pullAll_init_test(outC_pullAll_test *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _pullAll_test_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pullAll_test.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

