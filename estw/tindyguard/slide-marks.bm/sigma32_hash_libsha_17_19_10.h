/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sigma32_hash_libsha_17_19_10_H_
#define _sigma32_hash_libsha_17_19_10_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::sigma32/ */
extern kcg_uint32 sigma32_hash_libsha_17_19_10(
  /* @1/_L11/, @1/x/, @2/_L11/, @2/x/, _L1/, x/ */
  kcg_uint32 x_17_19_10);

/*
  Expanded instances for: hash::libsha::sigma32/
  @1: (M::Ror32#1)
  @2: (M::Ror32#2)
*/

#endif /* _sigma32_hash_libsha_17_19_10_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sigma32_hash_libsha_17_19_10.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

