/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _finalize_hash_sha512_H_
#define _finalize_hash_sha512_H_

#include "kcg_types.h"
#include "block_refine_hash_sha512.h"
#include "beHash2Chunk_slideTypes.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha512::finalize/ */
extern void finalize_hash_sha512(
  /* _L101/, _L105/, _L121/, _L122/, zin/ */
  hashAkkuWithChunk_hash_sha512 *zin,
  /* _L115/, m/ */
  StreamChunk_slideTypes *m,
  /* _L137/, finalbits/ */
  kcg_uint32 finalbits,
  /* _L117/, _L135/, finalbitsLen/ */
  size_slideTypes finalbitsLen,
  /* _L100/, hash/ */
  StreamChunk_slideTypes *hash);



#endif /* _finalize_hash_sha512_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** finalize_hash_sha512.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

