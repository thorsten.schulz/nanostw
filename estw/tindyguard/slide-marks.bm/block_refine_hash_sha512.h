/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block_refine_hash_sha512_H_
#define _block_refine_hash_sha512_H_

#include "kcg_types.h"
#include "LEtoBE64a_slideTypes_16.h"
#include "process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::sha512::block_refine/ */
extern void block_refine_hash_sha512(
  /* _L2/, zin/ */
  array_uint64_8 *zin,
  /* _L4/, m/ */
  array_uint32_32 *m,
  /* _L1/, zout/ */
  array_uint64_8 *zout);



#endif /* _block_refine_hash_sha512_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block_refine_hash_sha512.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

