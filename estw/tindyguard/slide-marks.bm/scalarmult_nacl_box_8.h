/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _scalarmult_nacl_box_8_H_
#define _scalarmult_nacl_box_8_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "assertGoodKey_nacl_box.h"
#include "inv25519_nacl_op.h"
#include "pack25519_nacl_op.h"
#include "scalarmult_it2_nacl_box.h"
#include "M_nacl_op.h"
#include "unpack25519_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  SSM_ST_SM1 /* SM1: */ SM1_state_nxt_8;
  gf_nacl_op /* x/ */ x_8;
  array_uint8_32 /* z/ */ z_8;
  scalMulAcc_nacl_box /* a/ */ a_8;
  array_uint32_8 /* k/ */ k_8;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_scalarmult_nacl_box_8;

/* ===========  node initialization and cycle functions  =========== */
/* nacl::box::scalarmult/ */
extern void scalarmult_nacl_box_8(
  /* our/ */
  KeyPair32_slideTypes *our_8,
  /* their/ */
  Key32_slideTypes *their_8,
  /* check/ */
  kcg_bool check_8,
  /* again/ */
  kcg_bool *again_8,
  /* _L1/, q/ */
  array_uint32_8 *q_8,
  /* failed/ */
  kcg_bool *failed_8,
  outC_scalarmult_nacl_box_8 *outC);

extern void scalarmult_reset_nacl_box_8(outC_scalarmult_nacl_box_8 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void scalarmult_init_nacl_box_8(outC_scalarmult_nacl_box_8 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _scalarmult_nacl_box_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmult_nacl_box_8.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

