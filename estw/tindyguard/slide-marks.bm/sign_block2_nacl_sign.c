/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_block2_nacl_sign.h"

/* nacl::sign::sign_block2/ */
void sign_block2_nacl_sign(
  /* _L4/, i/ */
  size_slideTypes i,
  /* _L30/, _L5/, signin/ */
  prependAkku_nacl_sign *signin,
  /* _L8/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L2/, goOn/ */
  kcg_bool *goOn,
  /* _L29/, signoo/ */
  prependAkku_nacl_sign *signoo)
{
  /* _L3/ */
  hashAkkuWithChunk_hash_sha512 _L3;

  /* _L2=(hash::sha512::stream_it#1)/ */
  stream_it_hash_sha512(i, &(*signin).accu, msg, goOn, &_L3);
  kcg_copy_prependAkku_nacl_sign(signoo, signin);
  kcg_copy_hashAkkuWithChunk_hash_sha512(&(*signoo).accu, &_L3);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_block2_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

