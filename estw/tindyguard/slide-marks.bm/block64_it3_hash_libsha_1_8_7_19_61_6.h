/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _block64_it3_hash_libsha_1_8_7_19_61_6_H_
#define _block64_it3_hash_libsha_1_8_7_19_61_6_H_

#include "kcg_types.h"
#include "sigma64_hash_libsha_19_61_6.h"
#include "sigma64_hash_libsha_1_8_7.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::libsha::block64_it3/ */
extern void block64_it3_hash_libsha_1_8_7_19_61_6(
  /* _L6/, i/ */
  size_slideTypes i_1_8_7_19_61_6,
  /* _L1/, w/ */
  array_uint64_16 *w_1_8_7_19_61_6,
  /* _L2/, wo/ */
  array_uint64_16 *wo_1_8_7_19_61_6);



#endif /* _block64_it3_hash_libsha_1_8_7_19_61_6_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block64_it3_hash_libsha_1_8_7_19_61_6.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

