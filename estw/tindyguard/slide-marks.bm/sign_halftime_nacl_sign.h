/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _sign_halftime_nacl_sign_H_
#define _sign_halftime_nacl_sign_H_

#include "kcg_types.h"
#include "scalarBase_nacl_sign.h"
#include "pack_nacl_sign.h"
#include "modL_nacl_sign.h"
#include "finalize_hash_sha512.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::sign::sign_halftime/ */
extern void sign_halftime_nacl_sign(
  /* SecretKey/, _L54/ */
  KeyPair32_slideTypes *SecretKey,
  /* _L61/, lastPart/ */
  StreamChunk_slideTypes *lastPart,
  /* _L57/, _L64/, intermediateAccu1/ */
  prependAkku_nacl_sign *intermediateAccu1,
  /* _L65/, goOnHashBlocks/ */
  kcg_bool *goOnHashBlocks,
  /* _L89/, signoo/ */
  prependAkku_nacl_sign *signoo,
  /* @1/_L3/, @1/rout/, _L31/, r/ */
  array_uint32_8 *r);

/*
  Expanded instances for: nacl::sign::sign_halftime/
  @1: (nacl::sign::reduce#1)
*/

#endif /* _sign_halftime_nacl_sign_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_halftime_nacl_sign.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

