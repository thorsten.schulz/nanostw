/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6.h"

/* hash::libsha::process64/ */
void process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6(
  /* _L2/, _L4/, zin/ */
  array_uint64_8 *zin_5_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L13/, m/ */
  array_uint64_16 *m_5_28_34_39_14_18_41_1_8_7_19_61_6,
  /* K/, _L26/, _L27/ */
  array_uint64_16_5 *K_5_28_34_39_14_18_41_1_8_7_19_61_6,
  /* _L3/, zout/ */
  array_uint64_8 *zout_5_28_34_39_14_18_41_1_8_7_19_61_6)
{
  kcg_size idx;
  array_uint64_8 acc;
  array_uint64_16 acc1;
  array_uint64_16 noname;
  /* _L11/ */
  array_uint64_8 _L11_5_28_34_39_14_18_41_1_8_7_19_61_6;

  kcg_copy_array_uint64_8(
    &_L11_5_28_34_39_14_18_41_1_8_7_19_61_6,
    zin_5_28_34_39_14_18_41_1_8_7_19_61_6);
  /* _L21= */
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint64_8(&acc, &_L11_5_28_34_39_14_18_41_1_8_7_19_61_6);
    /* _L21=(hash::libsha::block64_it2#1)/ */
    block64_it2_hash_libsha_28_34_39_14_18_41(
      &acc,
      (*m_5_28_34_39_14_18_41_1_8_7_19_61_6)[idx],
      (*K_5_28_34_39_14_18_41_1_8_7_19_61_6)[0][idx],
      &_L11_5_28_34_39_14_18_41_1_8_7_19_61_6);
  }
  kcg_copy_array_uint64_16(&noname, m_5_28_34_39_14_18_41_1_8_7_19_61_6);
  /* _L11= */
  for (idx = 0; idx < 4; idx++) {
    kcg_copy_array_uint64_8(&acc, &_L11_5_28_34_39_14_18_41_1_8_7_19_61_6);
    kcg_copy_array_uint64_16(&acc1, &noname);
    /* _L11=(hash::libsha::block64_it1#1)/ */
    block64_it1_hash_libsha_28_34_39_14_18_41_1_8_7_19_61_6(
      &acc,
      &acc1,
      &(*K_5_28_34_39_14_18_41_1_8_7_19_61_6)[idx + 1],
      &_L11_5_28_34_39_14_18_41_1_8_7_19_61_6,
      &noname);
  }
  /* _L3= */
  for (idx = 0; idx < 8; idx++) {
    (*zout_5_28_34_39_14_18_41_1_8_7_19_61_6)[idx] =
      _L11_5_28_34_39_14_18_41_1_8_7_19_61_6[idx] +
      (*zin_5_28_34_39_14_18_41_1_8_7_19_61_6)[idx];
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** process64_hash_libsha_5_28_34_39_14_18_41_1_8_7_19_61_6.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

