/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "ProtocolDryRun_tindyguard_test.h"

/* tindyguard::test::ProtocolDryRun/ */
void ProtocolDryRun_tindyguard_test(
  /* _L69/, fail/, failed/ */
  kcg_bool *failed,
  outC_ProtocolDryRun_tindyguard_test *outC)
{
  size_tindyguardTypes acc;
  kcg_size idx;
  Peer_tindyguardTypes tmp;
  array_uint32_16_4 tmp1;
  array_uint32_16_4 tmp2;
  array_uint32_16_2 noname;
  Session_tindyguardTypes _3_noname;
  length_t_udp _4_noname;
  /* BigZ/, _L87/ */
  KeySalted_tindyguardTypes BigZ;
  /* @1/_L17/, @1/knownPeer/, _L155/, _L70/, initialPeers/ */
  _6_array initialPeers;
  /* ChickenJoe/, _L5/ */
  KeySalted_tindyguardTypes _L5;
  /* @2/_L20/, @3/_L10/, @3/o/, _L6/ */
  kcg_bool _L6;
  /* _L61/, _L86/ */
  kcg_bool _L61;
  /* _L63/ */
  Session_tindyguardTypes _L63;
  /* _L4/, _L81/ */
  kcg_bool _L81;
  /* _L88/ */
  kcg_bool _L88;
  /* _L126/, _L158/ */
  kcg_bool _L126;
  /* _L125/ */
  Session_tindyguardTypes _L125;
  /* @4/_L1/, @4/i/, @5/_L1/, @5/i/, _L134/, _L79/ */
  array_uint32_16_4 _L134;
  /* _L138/, _L147/ */
  kcg_bool _L138;
  /* _L132/, _L136/ */
  length_t_udp _L136;
  /* _L135/ */
  array_uint32_16_3 _L135;
  /* _L145/ */
  kcg_bool _L145;
  /* @1/_L4/, @1/pid/, _L153/ */
  size_tindyguardTypes _L153;

  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[3],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[4],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[5],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[6],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  kcg_copy_Peer_tindyguardTypes(
    &initialPeers[7],
    (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  /* _L49=(tindyguard::initPeer#1)/ */
  initPeer_tindyguard(
    (pub_tindyguardTypes *) &BigZ_pub_tindyguard_test,
    (secret_tindyguardTypes *) &preshared_tindyguard_test,
    (peer_t_udp *) &BigZ_endpoint_tindyguard_test,
    (range_t_udp *) &BigZ_allowed_tindyguard_test,
    &initialPeers[0]);
  /* _L104=(tindyguard::initPeer#2)/ */
  initPeer_tindyguard(
    (pub_tindyguardTypes *) &ChickenJoe_pub_tindyguard_test,
    (secret_tindyguardTypes *) &preshared_tindyguard_test,
    (peer_t_udp *) &ChickenJoe_endpoint_tindyguard_test,
    (range_t_udp *) &ChickenJoe_allowed_tindyguard_test,
    &initialPeers[1]);
  /* _L111=(tindyguard::initPeer#3)/ */
  initPeer_tindyguard(
    (pub_tindyguardTypes *) &Lani_pub_tindyguard_test,
    (secret_tindyguardTypes *) &preshared_tindyguard_test,
    (peer_t_udp *) &Lani_endpoint_tindyguard_test,
    (range_t_udp *) &net10_allowed_tindyguard_test,
    &initialPeers[2]);
  _L153 = InvalidPeer_tindyguardTypes;
  /* @1/_L3= */
  for (idx = 0; idx < 8; idx++) {
    acc = _L153;
    _L6 = BigZ_ip_tindyguard_test.addr != kcg_lit_uint32(0) && kcg_lit_uint32(
        0) == ((BigZ_ip_tindyguard_test.addr ^
          initialPeers[idx].allowed.net.addr) &
        initialPeers[idx].allowed.mask.addr) && kcg_lit_uint32(0) !=
      initialPeers[idx].allowed.net.addr;
    /* @2/_L9= */
    if (_L6) {
      _L153 = /* @1/_L3= */(kcg_int32) idx;
    }
    else {
      _L153 = acc;
    }
    /* @1/_L3= */
    if (!!_L6) {
      break;
    }
  }
  /* _L4=(tindyguard::initOur#2)/ */
  initOur_tindyguard(
    (Key32_slideTypes *) &ChickenJoe_priv_tindyguard_test,
    &_L81,
    &_L5,
    &_L6,
    &outC->Context_initOur_2);
  if (kcg_lit_int32(0) <= _L153 && _L153 < kcg_lit_int32(8)) {
    kcg_copy_Peer_tindyguardTypes(&tmp, &initialPeers[_L153]);
  }
  else {
    kcg_copy_Peer_tindyguardTypes(
      &tmp,
      (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
  }
  /* _L132=(tindyguard::handshake::init#1)/ */
  init_tindyguard_handshake(
    &tmp,
    _L153,
    &_L5,
    kcg_lit_int64(1234),
    &_L136,
    &_L134,
    &_3_noname,
    &_L81,
    &outC->Context_init_1);
  /* _L86=(tindyguard::initOur#3)/ */
  initOur_tindyguard(
    (Key32_slideTypes *) &BigZ_priv_tindyguard_test,
    &_L61,
    &BigZ,
    &_L88,
    &outC->Context_initOur_3);
  for (idx = 0; idx < 12; idx++) {
    tmp1[0][idx] = kcg_lit_uint32(0);
    tmp2[0][idx] = kcg_lit_uint32(0);
  }
  kcg_copy_array_uint32_4(&tmp1[0][12], (array_uint32_4 *) &_L134[0][0]);
  kcg_copy_array_uint32_12(&tmp1[1][0], (array_uint32_12 *) &_L134[0][4]);
  kcg_copy_array_uint32_4(&tmp1[1][12], (array_uint32_4 *) &_L134[1][0]);
  kcg_copy_array_uint32_12(&tmp1[2][0], (array_uint32_12 *) &_L134[1][4]);
  kcg_copy_array_uint32_4(&tmp1[2][12], (array_uint32_4 *) &_L134[2][0]);
  kcg_copy_array_uint32_12(&tmp1[3][0], (array_uint32_12 *) &_L134[2][4]);
  kcg_copy_array_uint32_4(&tmp1[3][12], (array_uint32_4 *) &_L134[3][0]);
  /* _L133=(tindyguard::handshake::respond#2)/ */
  respond_tindyguard_handshake_8(
    _L136,
    &tmp1,
    (peer_t_udp *) &emptyPeer_udp,
    &BigZ,
    (Session_tindyguardTypes *) &EmptySession_tindyguardTypes,
    &initialPeers,
    kcg_lit_int64(1357),
    &_4_noname,
    &_L134,
    &_L63,
    &_L61,
    &_L138,
    &_L126,
    &outC->Context_respond_2);
  kcg_copy_array_uint32_4(&tmp2[0][12], (array_uint32_4 *) &_L134[0][0]);
  kcg_copy_array_uint32_12(&tmp2[1][0], (array_uint32_12 *) &_L134[0][4]);
  kcg_copy_array_uint32_4(&tmp2[1][12], (array_uint32_4 *) &_L134[1][0]);
  kcg_copy_array_uint32_12(&tmp2[2][0], (array_uint32_12 *) &_L134[1][4]);
  kcg_copy_array_uint32_4(&tmp2[2][12], (array_uint32_4 *) &_L134[2][0]);
  kcg_copy_array_uint32_12(&tmp2[3][0], (array_uint32_12 *) &_L134[2][4]);
  kcg_copy_array_uint32_4(&tmp2[3][12], (array_uint32_4 *) &_L134[3][0]);
  /* _L125=(tindyguard::handshake::eval_response#1)/ */
  eval_response_tindyguard_handshake_3(
    &_3_noname,
    _L61,
    _4_noname,
    &tmp2,
    (peer_t_udp *) &emptyPeer_udp,
    &_L5,
    &_L125,
    &_L126);
  for (idx = 0; idx < 2; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &noname[idx],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* _L136=(tindyguard::data::wrap#1)/ */
  wrap_tindyguard_data_2(
    kcg_lit_int32(0),
    &noname,
    &_L125,
    &_L136,
    &_4_noname,
    &_L135,
    &_3_noname,
    &_L138);
  /* _L144=(tindyguard::data::unwrap#1)/ */
  unwrap_tindyguard_data_2(
    &_L63,
    _L138,
    _L136,
    &_L135,
    (peer_t_udp *) &emptyPeer_udp,
    &_3_noname,
    &_L145,
    &_4_noname,
    &noname);
  *failed = _L61 || _L6 || _L81 || _L88 || _L126 || _L138 || _L145 ||
    InvalidPeer_tindyguardTypes == _L153;
}

#ifndef KCG_USER_DEFINED_INIT
void ProtocolDryRun_init_tindyguard_test(
  outC_ProtocolDryRun_tindyguard_test *outC)
{
  /* _L133=(tindyguard::handshake::respond#2)/ */
  respond_init_tindyguard_handshake_8(&outC->Context_respond_2);
  /* _L86=(tindyguard::initOur#3)/ */
  initOur_init_tindyguard(&outC->Context_initOur_3);
  /* _L132=(tindyguard::handshake::init#1)/ */
  init_init_tindyguard_handshake(&outC->Context_init_1);
  /* _L4=(tindyguard::initOur#2)/ */
  initOur_init_tindyguard(&outC->Context_initOur_2);
}
#endif /* KCG_USER_DEFINED_INIT */


#ifndef KCG_NO_EXTERN_CALL_TO_RESET
void ProtocolDryRun_reset_tindyguard_test(
  outC_ProtocolDryRun_tindyguard_test *outC)
{
  /* _L133=(tindyguard::handshake::respond#2)/ */
  respond_reset_tindyguard_handshake_8(&outC->Context_respond_2);
  /* _L86=(tindyguard::initOur#3)/ */
  initOur_reset_tindyguard(&outC->Context_initOur_3);
  /* _L132=(tindyguard::handshake::init#1)/ */
  init_reset_tindyguard_handshake(&outC->Context_init_1);
  /* _L4=(tindyguard::initOur#2)/ */
  initOur_reset_tindyguard(&outC->Context_initOur_2);
}
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

/*
  Expanded instances for: tindyguard::test::ProtocolDryRun/
  @1: (tindyguard::data::selectPeer#1)
  @2: @1/(tindyguard::data::selectPeer_it#1)
  @3: @2/(udp::within#1)
  @4: (tindyguard::test::shiftHS#2)
  @5: (tindyguard::test::shiftHS#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** ProtocolDryRun_tindyguard_test.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

