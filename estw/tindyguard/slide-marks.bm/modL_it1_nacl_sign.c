/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "modL_it1_nacl_sign.h"

/* nacl::sign::modL_it1/ */
void modL_it1_nacl_sign(
  /* _L14/, it/ */
  int_slideTypes it,
  /* _L9/, x/ */
  array_int64_64 *x,
  /* _L3/, xo/ */
  array_int64_64 *xo)
{
  kcg_int64 acc;
  kcg_size idx;
  /* _L5/ */
  kcg_int32 _L5;
  /* _L8/ */
  array_int64_64 _L8;
  /* _L11/ */
  kcg_int64 _L11;
  /* _L15/ */
  kcg_int32 _L15;
  /* _L16/ */
  kcg_int64 _L16;

  _L15 = kcg_lit_int32(63) - it;
  if (kcg_lit_int32(0) <= _L15 && _L15 < kcg_lit_int32(64)) {
    _L16 = (*x)[_L15];
  }
  else {
    _L16 = kcg_lit_int64(0);
  }
  _L11 = kcg_lit_int64(0);
  /* _L11= */
  for (idx = 0; idx < 64; idx++) {
    acc = _L11;
    /* _L11=(nacl::sign::modL_it2#1)/ */
    modL_it2_nacl_sign(
      /* _L11= */(kcg_int32) idx,
      acc,
      (*x)[idx],
      _L16,
      _L15,
      &_L11,
      &_L8[idx]);
  }
  _L5 = _L15 - kcg_lit_int32(12);
  kcg_copy_array_int64_64(xo, &_L8);
  if (kcg_lit_int32(0) <= _L5 && _L5 < kcg_lit_int32(64)) {
    (*xo)[_L5] = _L8[_L5] + _L11;
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** modL_it1_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

