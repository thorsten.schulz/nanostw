/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10.h"

/* hash::libsha::block32_it1/ */
void block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10(
  /* _L2/, ain/ */
  array_uint32_8 *ain_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L3/, w/ */
  array_uint32_16 *w_2_13_22_6_11_25_7_18_3_17_19_10,
  /* K/, _L17/ */
  array_uint32_16 *K_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L6/, aout/ */
  array_uint32_8 *aout_2_13_22_6_11_25_7_18_3_17_19_10,
  /* _L5/, woo/ */
  array_uint32_16 *woo_2_13_22_6_11_25_7_18_3_17_19_10)
{
  array_uint32_16 acc;
  kcg_size idx;
  array_uint32_8 acc1;

  kcg_copy_array_uint32_16(
    woo_2_13_22_6_11_25_7_18_3_17_19_10,
    w_2_13_22_6_11_25_7_18_3_17_19_10);
  /* _L5= */
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint32_16(&acc, woo_2_13_22_6_11_25_7_18_3_17_19_10);
    /* _L5=(hash::libsha::block32_it3#1)/ */
    block32_it3_hash_libsha_7_18_3_17_19_10(
      /* _L5= */(kcg_int32) idx,
      &acc,
      woo_2_13_22_6_11_25_7_18_3_17_19_10);
  }
  kcg_copy_array_uint32_8(
    aout_2_13_22_6_11_25_7_18_3_17_19_10,
    ain_2_13_22_6_11_25_7_18_3_17_19_10);
  /* _L6= */
  for (idx = 0; idx < 16; idx++) {
    kcg_copy_array_uint32_8(&acc1, aout_2_13_22_6_11_25_7_18_3_17_19_10);
    /* _L6=(hash::libsha::block32_it2#1)/ */
    block32_it2_hash_libsha_2_13_22_6_11_25(
      &acc1,
      (*woo_2_13_22_6_11_25_7_18_3_17_19_10)[idx],
      (*K_2_13_22_6_11_25_7_18_3_17_19_10)[idx],
      aout_2_13_22_6_11_25_7_18_3_17_19_10);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** block32_it1_hash_libsha_2_13_22_6_11_25_7_18_3_17_19_10.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

