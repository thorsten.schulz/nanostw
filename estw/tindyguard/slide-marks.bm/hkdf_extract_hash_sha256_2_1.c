/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hkdf_extract_hash_sha256_2_1.h"

/* hash::sha256::hkdf_extract/ */
void hkdf_extract_hash_sha256_2_1(
  /* _L9/, ikmlen/ */
  size_slideTypes ikmlen_2_1,
  /* _L8/, ikm/ */
  array_uint32_16_2 *ikm_2_1,
  /* _L1/, _L10/, saltlen/ */
  size_slideTypes saltlen_2_1,
  /* _L5/, salt/ */
  array_uint32_16_1 *salt_2_1,
  /* _L7/, prk/ */
  StreamChunk_slideTypes *prk_2_1)
{
  array_uint32_16_1 tmp;
  size_slideTypes tmp1;
  /* _L2/ */
  kcg_bool _L2_2_1;

  _L2_2_1 = saltlen_2_1 > kcg_lit_int32(0);
  /* _L4= */
  if (_L2_2_1) {
    kcg_copy_array_uint32_16_1(&tmp, salt_2_1);
    tmp1 = saltlen_2_1;
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    tmp1 = Bytes_hash_sha256;
  }
  /* _L7=(hash::sha256::HMAC#1)/ */
  HMAC_hash_sha256_2_1(ikm_2_1, ikmlen_2_1, &tmp, tmp1, prk_2_1);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_extract_hash_sha256_2_1.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

