/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _bench_x25519_test_ietf_RFC7748_1_H_
#define _bench_x25519_test_ietf_RFC7748_1_H_

#include "kcg_types.h"
#include "scalarmult_nacl_box_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  array_uint32_8 /* u/ */ u_1;
  kcg_int32 /* iter/ */ iter_1;
  array_uint32_8 /* k/ */ k_1;
  array_uint32_8 /* k3/ */ k3_1;
  kcg_bool /* SM3: */ SM3_reset_nxt_1;
  SSM_ST_SM3 /* SM3: */ SM3_state_nxt_1;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_scalarmult_nacl_box_1 /* SM3:compute:_L1=(nacl::box::scalarmult#3)/ */ Context_scalarmult_3;
  /* ----------------- no clocks of observable data ------------------ */
} outC_bench_x25519_test_ietf_RFC7748_1;

/* ===========  node initialization and cycle functions  =========== */
/* test::ietf_RFC7748::bench_x25519/ */
extern void bench_x25519_test_ietf_RFC7748_1(
  /* failed/ */
  kcg_bool *failed_1,
  outC_bench_x25519_test_ietf_RFC7748_1 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void bench_x25519_reset_test_ietf_RFC7748_1(
  outC_bench_x25519_test_ietf_RFC7748_1 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void bench_x25519_init_test_ietf_RFC7748_1(
  outC_bench_x25519_test_ietf_RFC7748_1 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _bench_x25519_test_ietf_RFC7748_1_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_x25519_test_ietf_RFC7748_1.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

