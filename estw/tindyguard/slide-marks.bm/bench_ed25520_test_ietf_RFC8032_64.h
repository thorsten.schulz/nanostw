/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _bench_ed25520_test_ietf_RFC8032_64_H_
#define _bench_ed25520_test_ietf_RFC8032_64_H_

#include "kcg_types.h"
#include "verify_nacl_sign_64.h"
#include "sign_nacl_sign_64.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* test::ietf_RFC8032::bench_ed25520/ */
extern kcg_bool bench_ed25520_test_ietf_RFC8032_64(
  /* _L17/, failing/ */
  kcg_bool failing_64,
  /* tc/ */
  testcase_test_ietf_RFC8032 *tc_64);



#endif /* _bench_ed25520_test_ietf_RFC8032_64_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** bench_ed25520_test_ietf_RFC8032_64.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

