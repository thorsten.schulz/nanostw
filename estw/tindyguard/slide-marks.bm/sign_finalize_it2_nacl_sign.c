/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "sign_finalize_it2_nacl_sign.h"

/* nacl::sign::sign_finalize_it2/ */
void sign_finalize_it2_nacl_sign(
  /* _L29/, i4/ */
  int_slideTypes i4,
  /* _L2/, a/ */
  array_int64_64 *a,
  /* _L1/, i/ */
  int_slideTypes i,
  /* _L3/, hm/ */
  kcg_uint8 hm,
  /* _L4/, d/ */
  array_uint8_32 *d,
  /* _L5/, aoo/ */
  array_int64_64 *aoo)
{
  array_int64_64 acc;
  kcg_size idx;
  /* _L30/ */
  kcg_int32 _L30;

  _L30 = i4 + i;
  kcg_copy_array_int64_64(aoo, a);
  /* _L5= */
  for (idx = 0; idx < 32; idx++) {
    kcg_copy_array_int64_64(&acc, aoo);
    /* _L5=(nacl::sign::sign_finalize_it3#1)/ */
    sign_finalize_it3_nacl_sign(
      /* _L5= */(kcg_int32) idx,
      &acc,
      _L30,
      hm,
      (*d)[idx],
      aoo);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** sign_finalize_it2_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

