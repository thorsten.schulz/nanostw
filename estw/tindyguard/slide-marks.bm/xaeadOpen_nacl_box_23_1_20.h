/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _xaeadOpen_nacl_box_23_1_20_H_
#define _xaeadOpen_nacl_box_23_1_20_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_23_1_20.h"
#include "half_nacl_core_chacha_20.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* nacl::box::xaeadOpen/ */
extern void xaeadOpen_nacl_box_23_1_20(
  /* _L5/, a/ */
  Mac_nacl_onetime *a_23_1_20,
  /* _L7/, cm/ */
  array_uint32_16_23 *cm_23_1_20,
  /* _L6/, mlen/ */
  int_slideTypes mlen_23_1_20,
  /* _L18/, ad/ */
  array_uint32_16_1 *ad_23_1_20,
  /* _L19/, adlen/ */
  int_slideTypes adlen_23_1_20,
  /* _L29/, xnonce/ */
  XNonce_nacl_core_chacha *xnonce_23_1_20,
  /* _L24/, key/ */
  Key_nacl_core *key_23_1_20,
  /* _L20/, msg/ */
  array_uint32_16_23 *msg_23_1_20,
  /* _L21/, failed/ */
  kcg_bool *failed_23_1_20);



#endif /* _xaeadOpen_nacl_box_23_1_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** xaeadOpen_nacl_box_23_1_20.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

