/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "beHash2Chunk_slideTypes.h"

/* slideTypes::beHash2Chunk/ */
void beHash2Chunk_slideTypes(
  /* _L112/, zin/ */
  beHash_slideTypes *zin,
  /* _L111/, sha/ */
  StreamChunk_slideTypes *sha)
{
  /* @1/_L21/,
     @2/_L21/,
     @3/_L21/,
     @4/_L21/,
     @5/_L21/,
     @6/_L21/,
     @7/_L21/,
     @8/_L21/ */
  kcg_uint64 _L21_BE64toLE_2;

  (*sha)[1] = ((/* @1/_L28= */(kcg_uint32) (*zin)[0] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @1/_L28= */(kcg_uint32)
          (*zin)[0] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @1/_L28= */(kcg_uint32) (*zin)[0] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L28= */(kcg_uint32) (*zin)[0] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  (*sha)[3] = ((/* @2/_L28= */(kcg_uint32) (*zin)[1] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @2/_L28= */(kcg_uint32)
          (*zin)[1] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @2/_L28= */(kcg_uint32) (*zin)[1] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @2/_L28= */(kcg_uint32) (*zin)[1] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  (*sha)[5] = ((/* @3/_L28= */(kcg_uint32) (*zin)[2] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @3/_L28= */(kcg_uint32)
          (*zin)[2] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @3/_L28= */(kcg_uint32) (*zin)[2] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @3/_L28= */(kcg_uint32) (*zin)[2] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  (*sha)[7] = ((/* @4/_L28= */(kcg_uint32) (*zin)[3] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @4/_L28= */(kcg_uint32)
          (*zin)[3] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @4/_L28= */(kcg_uint32) (*zin)[3] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @4/_L28= */(kcg_uint32) (*zin)[3] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  (*sha)[9] = ((/* @8/_L28= */(kcg_uint32) (*zin)[4] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @8/_L28= */(kcg_uint32)
          (*zin)[4] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @8/_L28= */(kcg_uint32) (*zin)[4] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @8/_L28= */(kcg_uint32) (*zin)[4] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  (*sha)[11] = ((/* @7/_L28= */(kcg_uint32) (*zin)[5] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @7/_L28= */(kcg_uint32)
          (*zin)[5] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @7/_L28= */(kcg_uint32) (*zin)[5] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @7/_L28= */(kcg_uint32) (*zin)[5] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  (*sha)[13] = ((/* @6/_L28= */(kcg_uint32) (*zin)[6] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @6/_L28= */(kcg_uint32)
          (*zin)[6] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @6/_L28= */(kcg_uint32) (*zin)[6] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @6/_L28= */(kcg_uint32) (*zin)[6] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  (*sha)[15] = ((/* @5/_L28= */(kcg_uint32) (*zin)[7] & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @5/_L28= */(kcg_uint32)
          (*zin)[7] & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @5/_L28= */(kcg_uint32) (*zin)[7] & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @5/_L28= */(kcg_uint32) (*zin)[7] & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[0] >> kcg_lit_uint64(32);
  (*sha)[0] = ((/* @1/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @1/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @1/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @1/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[1] >> kcg_lit_uint64(32);
  (*sha)[2] = ((/* @2/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @2/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @2/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @2/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[2] >> kcg_lit_uint64(32);
  (*sha)[4] = ((/* @3/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @3/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @3/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @3/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[3] >> kcg_lit_uint64(32);
  (*sha)[6] = ((/* @4/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @4/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @4/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @4/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[7] >> kcg_lit_uint64(32);
  (*sha)[14] = ((/* @5/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @5/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @5/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @5/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[6] >> kcg_lit_uint64(32);
  (*sha)[12] = ((/* @6/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @6/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @6/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @6/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[5] >> kcg_lit_uint64(32);
  (*sha)[10] = ((/* @7/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @7/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @7/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @7/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
  _L21_BE64toLE_2 = (*zin)[4] >> kcg_lit_uint64(32);
  (*sha)[8] = ((/* @8/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(
          4278190080)) >> kcg_lit_uint32(24)) | ((/* @8/_L20= */(kcg_uint32)
          _L21_BE64toLE_2 & kcg_lit_uint32(16711680)) >> kcg_lit_uint32(8)) |
    kcg_lsl_uint32(
      /* @8/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(65280),
      kcg_lit_uint32(8)) | kcg_lsl_uint32(
      /* @8/_L20= */(kcg_uint32) _L21_BE64toLE_2 & kcg_lit_uint32(255),
      kcg_lit_uint32(24));
}

/*
  Expanded instances for: slideTypes::beHash2Chunk/
  @1: (slideTypes::BE64toLE#1)
  @2: (slideTypes::BE64toLE#2)
  @3: (slideTypes::BE64toLE#3)
  @4: (slideTypes::BE64toLE#4)
  @5: (slideTypes::BE64toLE#5)
  @6: (slideTypes::BE64toLE#6)
  @7: (slideTypes::BE64toLE#7)
  @8: (slideTypes::BE64toLE#8)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** beHash2Chunk_slideTypes.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

