/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _LEtoBE64a_slideTypes_16_H_
#define _LEtoBE64a_slideTypes_16_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* slideTypes::LEtoBE64a/ */
extern void LEtoBE64a_slideTypes_16(
  /* LE/, _L1/ */
  array_uint32_32 *LE_16,
  /* BE/, _L4/ */
  array_uint64_16 *BE_16);



#endif /* _LEtoBE64a_slideTypes_16_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** LEtoBE64a_slideTypes_16.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

