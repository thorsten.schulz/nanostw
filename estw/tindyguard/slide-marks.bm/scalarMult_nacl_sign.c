/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "scalarMult_nacl_sign.h"

/* nacl::sign::scalarMult/ */
void scalarMult_nacl_sign(
  /* _L1/, qin/ */
  cpoint_nacl_op *qin,
  /* _L2/, s/ */
  array_uint32_8 *s,
  /* _L29/, p/ */
  cpoint_nacl_op *p)
{
  kcg_size idx;
  /* @1/_L15/ */
  kcg_uint32 _L15_st32_1;
  /* @1/_L14/ */
  kcg_uint32 _L14_st32_1;
  pq_pair_nacl_sign tmp;
  array_uint8_32 tmp1;
  pq_pair_nacl_sign acc;
  /* _L30/ */
  u848_slideTypes _L30;

  kcg_copy_gf_nacl_op(&tmp.p[0], (gf_nacl_op *) &gf0_nacl_op);
  kcg_copy_gf_nacl_op(&tmp.p[1], (gf_nacl_op *) &gf1_nacl_op);
  kcg_copy_gf_nacl_op(&tmp.p[2], (gf_nacl_op *) &gf1_nacl_op);
  kcg_copy_gf_nacl_op(&tmp.p[3], (gf_nacl_op *) &gf0_nacl_op);
  kcg_copy_cpoint_nacl_op(&tmp.q, qin);
  /* _L30= */
  for (idx = 0; idx < 8; idx++) {
    _L30[idx][0] = /* @1/_L8= */(kcg_uint8) (*s)[idx];
    _L14_st32_1 = (*s)[idx] >> kcg_lit_uint32(8);
    _L30[idx][1] = /* @1/_L7= */(kcg_uint8) _L14_st32_1;
    _L15_st32_1 = _L14_st32_1 >> kcg_lit_uint32(8);
    _L30[idx][2] = /* @1/_L6= */(kcg_uint8) _L15_st32_1;
    _L30[idx][3] = /* @1/_L5= */(kcg_uint8) (_L15_st32_1 >> kcg_lit_uint32(8));
  }
  kcg_copy_array_uint8_4(&tmp1[0], &_L30[0]);
  kcg_copy_array_uint8_4(&tmp1[4], &_L30[1]);
  kcg_copy_array_uint8_4(&tmp1[8], &_L30[2]);
  kcg_copy_array_uint8_4(&tmp1[12], &_L30[3]);
  kcg_copy_array_uint8_4(&tmp1[16], &_L30[4]);
  kcg_copy_array_uint8_4(&tmp1[20], &_L30[5]);
  kcg_copy_array_uint8_4(&tmp1[24], &_L30[6]);
  kcg_copy_array_uint8_4(&tmp1[28], &_L30[7]);
  /* _L10= */
  for (idx = 0; idx < 32; idx++) {
    kcg_copy_pq_pair_nacl_sign(&acc, &tmp);
    /* _L10=(nacl::sign::scalarMult_it1#1)/ */
    scalarMult_it1_nacl_sign(&acc, tmp1[31 - idx], &tmp);
  }
  kcg_copy_cpoint_nacl_op(p, &tmp.p);
}

/*
  Expanded instances for: nacl::sign::scalarMult/
  @1: (slideTypes::st32#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarMult_nacl_sign.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

