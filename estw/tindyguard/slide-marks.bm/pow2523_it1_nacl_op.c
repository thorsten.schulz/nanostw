/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "pow2523_it1_nacl_op.h"

/* nacl::op::pow2523_it1/ */
void pow2523_it1_nacl_op(
  /* _L3/, a/ */
  int_slideTypes a,
  /* _L4/, c/ */
  gf_nacl_op *c,
  /* _L1/, i/ */
  gf_nacl_op *i,
  /* _L12/, o/ */
  gf_nacl_op *o)
{
  /* _L2/ */
  gf_nacl_op _L2;
  /* _L7/ */
  kcg_bool _L7;

  _L7 = a != kcg_lit_int32(249);
  /* _L2=(nacl::op::S#1)/ */ S_nacl_op(c, &_L2);
  if (_L7) {
    /* _L12=(nacl::op::M#1)/ */ M_nacl_op(&_L2, i, o);
  }
  else {
    kcg_copy_gf_nacl_op(o, &_L2);
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** pow2523_it1_nacl_op.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

