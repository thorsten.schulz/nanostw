/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */
#ifndef _hmacChunk_hash_blake2s_2_H_
#define _hmacChunk_hash_blake2s_2_H_

#include "kcg_types.h"
#include "hashChunk_hash_blake2s_3.h"
#include "block_refine_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hmacChunk/ */
extern void hmacChunk_hash_blake2s_2(
  /* @1/_L8/, @1/msg/, _L8/, msg/ */
  array_uint32_16_2 *msg_2,
  /* @1/_L9/, @1/mlen/, _L9/, mlen/ */
  int_slideTypes mlen_2,
  /* @1/_L32/, @1/key/, @2/_L1/, @2/chunk/, @3/_L1/, @3/chunk/, _L32/, key/ */
  HashChunk_hash_blake2s *key_2,
  /* _L34/, chunk/ */
  HashChunk_hash_blake2s *chunk_2);

/*
  Expanded instances for: hash::blake2s::hmacChunk/
  @1: (hash::blake2s::hmac#1)
  @2: @1/(slideTypes::xorChunkConst#2)
  @3: @1/(slideTypes::xorChunkConst#1)
*/

#endif /* _hmacChunk_hash_blake2s_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hmacChunk_hash_blake2s_2.h
** Generation date: 2020-07-28T11:10:44
*************************************************************$ */

