/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/slide-marks.bm/config.txt
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "run_hash_sha512_16.h"

/* hash::sha512::run/ */
void run_hash_sha512_16(
  /* _L32/, _L39/, msg/ */
  array_uint32_16_16 *msg_16,
  /* _L34/, _L41/, len/ */
  size_slideTypes len_16,
  /* finalbits/ */
  kcg_uint32 finalbits_16,
  /* finalbitsLen/ */
  size_slideTypes finalbitsLen_16,
  /* _L33/, hash/ */
  StreamChunk_slideTypes *hash_16)
{
  hashAkkuWithChunk_hash_sha512 acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  StreamChunk_slideTypes tmp;
  /* @1/_L2/, @1/o/, _L45/ */
  size_slideTypes _L45_16;
  /* @1/_L1/, @1/i/, _L43/ */
  size_slideTypes _L43_16;
  /* _L44/ */
  hashAkkuWithChunk_hash_sha512 _L44_16;

  kcg_copy_array_uint64_8(&_L44_16.z, (array_uint64_8 *) &IV_hash_sha512);
  _L44_16.length = len_16;
  kcg_copy_StreamChunk_slideTypes(&_L44_16.porch, &(*msg_16)[0]);
  /* _L43= */
  if (len_16 >= BlockBytes_slideTypes) {
    /* _L43= */
    for (idx = 0; idx < 15; idx++) {
      kcg_copy_hashAkkuWithChunk_hash_sha512(&acc, &_L44_16);
      /* _L43=(hash::sha512::stream_it#1)/ */
      stream_it_hash_sha512(
        /* _L43= */(kcg_int32) idx,
        &acc,
        &(*msg_16)[idx + 1],
        &cond_iterw,
        &_L44_16);
      _L43_16 = /* _L43= */(kcg_int32) (idx + 1);
      /* _L43= */
      if (!cond_iterw) {
        break;
      }
    }
  }
  else {
    _L43_16 = kcg_lit_int32(0);
  }
  _L45_16 = _L43_16 + kcg_lit_int32(1);
  if (kcg_lit_int32(0) <= _L45_16 && _L45_16 < kcg_lit_int32(16)) {
    kcg_copy_StreamChunk_slideTypes(&tmp, &(*msg_16)[_L45_16]);
  }
  else {
    kcg_copy_StreamChunk_slideTypes(
      &tmp,
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* _L33=(hash::sha512::finalize#1)/ */
  finalize_hash_sha512(&_L44_16, &tmp, finalbits_16, finalbitsLen_16, hash_16);
}

/*
  Expanded instances for: hash::sha512::run/
  @1: (M::inc#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** run_hash_sha512_16.c
** Generation date: 2020-07-28T11:10:45
*************************************************************$ */

