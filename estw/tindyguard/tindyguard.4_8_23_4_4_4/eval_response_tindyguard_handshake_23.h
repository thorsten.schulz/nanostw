/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _eval_response_tindyguard_handshake_23_H_
#define _eval_response_tindyguard_handshake_23_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_1_1_20.h"
#include "hkdf_hash_blake2s_1_3.h"
#include "hkdf_hash_blake2s_1_2.h"
#include "scalarmultDonna_nacl_box.h"
#include "single_hash_blake2s.h"
#include "hkdf_cKonly_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::handshake::eval_response/ */
extern void eval_response_tindyguard_handshake_23(
  /* s/ */
  Session_tindyguardTypes *s_23,
  /* fail_in/ */
  kcg_bool fail_in_23,
  /* rlength/ */
  length_t_udp rlength_23,
  /* responsemsg/ */
  array_uint32_16_24 *responsemsg_23,
  /* endpoint/ */
  peer_t_udp *endpoint_23,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_23,
  /* soo/ */
  Session_tindyguardTypes *soo_23,
  /* allfail/ */
  kcg_bool *allfail_23);



#endif /* _eval_response_tindyguard_handshake_23_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_response_tindyguard_handshake_23.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

