/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _mapInitPeer_tindyguard_session_4_H_
#define _mapInitPeer_tindyguard_session_4_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapInitPeer/ */
extern void mapInitPeer_tindyguard_session_4(
  /* pid_req/ */
  array_int32_4 *pid_req_4,
  /* s/ */
  Session_tindyguardTypes *s_4,
  /* IfBlock1:, res_s_init/ */
  kcg_bool res_s_init_4,
  /* pid_not_taken/ */
  array_int32_4 *pid_not_taken_4,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_4);



#endif /* _mapInitPeer_tindyguard_session_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapInitPeer_tindyguard_session_4.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

