/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "filter1_tindyguard_data_23.h"

/* tindyguard::data::filter1/ */
void filter1_tindyguard_data_23(
  /* _L19/, i/ */
  size_tindyguardTypes i_23,
  /* _L21/, acc/ */
  size_tindyguardTypes acc_23,
  /* _L1/, len/ */
  size_tindyguardTypes len_23,
  /* _L2/, msg/ */
  array_uint32_16_23 *msg_23,
  /* _L3/, protocol/ */
  kcg_uint8 protocol_23,
  /* _L4/, goOn/ */
  kcg_bool *goOn_23,
  /* _L20/, ioo/ */
  size_tindyguardTypes *ioo_23,
  /* _L18/, matching/ */
  kcg_bool *matching_23)
{
  kcg_int32 tmp;

  /* _L7= */
  switch (protocol_23) {
    case kcg_lit_uint8(1) :
      tmp = kcg_lit_int32(36);
      break;
    case kcg_lit_uint8(6) :
      tmp = kcg_lit_int32(40);
      break;
    case kcg_lit_uint8(17) :
      tmp = kcg_lit_int32(28);
      break;
    default :
      tmp = len_23 + kcg_lit_int32(1);
      break;
  }
  *matching_23 = /* _L15= */(kcg_uint8)
      ((*msg_23)[0][2] >> kcg_lit_uint32(8)) == protocol_23 && len_23 >= tmp;
  /* _L20= */
  if (*matching_23) {
    *ioo_23 = i_23;
  }
  else {
    *ioo_23 = acc_23;
  }
  *goOn_23 = !*matching_23;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** filter1_tindyguard_data_23.c
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

