/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapRekey_tindyguard_session_4.h"

/* tindyguard::session::mapRekey/ */
void mapRekey_tindyguard_session_4(
  /* rekey/ */
  array_int32_4 *rekey_4,
  /* s/ */
  Session_tindyguardTypes *s_4,
  /* pid_cmd_in/ */
  size_tindyguardTypes pid_cmd_in_4,
  /* pid_not_taken/ */
  array_int32_4 *pid_not_taken_4,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_4)
{
  size_tindyguardTypes acc;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_findNonNil_it_1;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_4;

  IfBlock1_clock_4 = pid_cmd_in_4 != nil_tindyguard_session || (*s_4).pid !=
    nil_tindyguard_session;
  /* IfBlock1: */
  if (IfBlock1_clock_4) {
    kcg_copy_array_int32_4(pid_not_taken_4, rekey_4);
    *pid_cmd_4 = pid_cmd_in_4;
  }
  else {
    *pid_cmd_4 = InvalidPeer_tindyguardTypes;
    /* IfBlock1:else:_L7= */
    for (idx = 0; idx < 4; idx++) {
      acc = *pid_cmd_4;
      IfBlock1_clock_findNonNil_it_1 = acc == InvalidPeer_tindyguardTypes;
      /* @1/IfBlock1: */
      if (IfBlock1_clock_findNonNil_it_1) {
        /* @1/IfBlock1:then:_L1= */
        if ((*rekey_4)[idx] != InvalidPeer_tindyguardTypes) {
          *pid_cmd_4 = (*rekey_4)[idx];
        }
        else {
          *pid_cmd_4 = InvalidPeer_tindyguardTypes;
        }
        (*pid_not_taken_4)[idx] = InvalidPeer_tindyguardTypes;
      }
      else {
        *pid_cmd_4 = acc;
        (*pid_not_taken_4)[idx] = (*rekey_4)[idx];
      }
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapRekey/
  @1: (tindyguard::session::findNonNil_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapRekey_tindyguard_session_4.c
** Generation date: 2020-03-11T13:42:26
*************************************************************$ */

