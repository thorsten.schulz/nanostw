/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _initPeer_tindyguard_H_
#define _initPeer_tindyguard_H_

#include "kcg_types.h"
#include "singleChunk_hash_blake2s.h"
#include "single_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::initPeer/ */
extern void initPeer_tindyguard(
  /* @1/_L24/, @1/pub/, _L14/, _L26/, pub/ */
  pub_tindyguardTypes *pub,
  /* _L15/, preshared/ */
  secret_tindyguardTypes *preshared,
  /* _L16/, endpoint/ */
  peer_t_udp *endpoint,
  /* _L17/, allowed/ */
  range_t_udp *allowed,
  /* _L12/, p/ */
  Peer_tindyguardTypes *p);

/*
  Expanded instances for: tindyguard::initPeer/
  @1: (tindyguard::initHashes#2)
*/

#endif /* _initPeer_tindyguard_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** initPeer_tindyguard.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

