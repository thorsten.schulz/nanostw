/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _updatePeer_tindyguard_session_4_H_
#define _updatePeer_tindyguard_session_4_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::updatePeer/ */
extern void updatePeer_tindyguard_session_4(
  /* _L23/, pid/ */
  size_tindyguardTypes pid_4,
  /* _L21/, knownPeer/ */
  Peer_tindyguardTypes *knownPeer_4,
  /* _L18/, isNewTAI/ */
  array_bool_4 *isNewTAI_4,
  /* _L19/, s/ */
  _4_array *s_4,
  /* _L25/, t_flag/ */
  array_bool_4 *t_flag_4,
  /* _L22/, updatedPeer/ */
  Peer_tindyguardTypes *updatedPeer_4);



#endif /* _updatePeer_tindyguard_session_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** updatePeer_tindyguard_session_4.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

