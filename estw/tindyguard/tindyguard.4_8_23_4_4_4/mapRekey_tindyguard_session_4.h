/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */
#ifndef _mapRekey_tindyguard_session_4_H_
#define _mapRekey_tindyguard_session_4_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapRekey/ */
extern void mapRekey_tindyguard_session_4(
  /* rekey/ */
  array_int32_4 *rekey_4,
  /* s/ */
  Session_tindyguardTypes *s_4,
  /* pid_cmd_in/ */
  size_tindyguardTypes pid_cmd_in_4,
  /* pid_not_taken/ */
  array_int32_4 *pid_not_taken_4,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_4);



#endif /* _mapRekey_tindyguard_session_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapRekey_tindyguard_session_4.h
** Generation date: 2020-03-11T13:42:23
*************************************************************$ */

