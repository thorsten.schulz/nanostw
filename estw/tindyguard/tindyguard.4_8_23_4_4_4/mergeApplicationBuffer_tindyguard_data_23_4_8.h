/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */
#ifndef _mergeApplicationBuffer_tindyguard_data_23_4_8_H_
#define _mergeApplicationBuffer_tindyguard_data_23_4_8_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  array_uint32_16_23 /* txmbuffer/ */ mem_txmbuffer_23_4_8;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_mergeApplicationBuffer_tindyguard_data_23_4_8;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::data::mergeApplicationBuffer/ */
extern void mergeApplicationBuffer_tindyguard_data_23_4_8(
  /* ii/ */
  size_tindyguardTypes ii_23_4_8,
  /* no_peer_acc/ */
  kcg_uint64 no_peer_acc_23_4_8,
  /* txm_residual_length/ */
  size_slideTypes txm_residual_length_23_4_8,
  /* length/ */
  array_int32_4 *length_23_4_8,
  /* msg/ */
  array_uint32_16_23_4 *msg_23_4_8,
  /* knownPeer/ */
  _3_array *knownPeer_23_4_8,
  /* ioo/ */
  size_tindyguardTypes *ioo_23_4_8,
  /* no_peer_mask/ */
  kcg_uint64 *no_peer_mask_23_4_8,
  /* pid_req/ */
  size_tindyguardTypes *pid_req_23_4_8,
  /* txmbuffer/ */
  array_uint32_16_23 *txmbuffer_23_4_8,
  /* txmlength/ */
  size_slideTypes *txmlength_23_4_8,
  /* sid/ */
  size_tindyguardTypes *sid_23_4_8,
  outC_mergeApplicationBuffer_tindyguard_data_23_4_8 *outC);

extern void mergeApplicationBuffer_reset_tindyguard_data_23_4_8(
  outC_mergeApplicationBuffer_tindyguard_data_23_4_8 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void mergeApplicationBuffer_init_tindyguard_data_23_4_8(
  outC_mergeApplicationBuffer_tindyguard_data_23_4_8 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _mergeApplicationBuffer_tindyguard_data_23_4_8_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mergeApplicationBuffer_tindyguard_data_23_4_8.h
** Generation date: 2020-03-11T13:42:22
*************************************************************$ */

