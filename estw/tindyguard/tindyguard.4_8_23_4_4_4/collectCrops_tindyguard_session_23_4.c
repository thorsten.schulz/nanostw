/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "collectCrops_tindyguard_session_23_4.h"

/* tindyguard::session::collectCrops/ */
void collectCrops_tindyguard_session_23_4(
  /* _L5/, worker_sid/ */
  size_tindyguardTypes worker_sid_23_4,
  /* _L2/, rxmlength/ */
  array_int32_4 *rxmlength_23_4,
  /* _L1/, rxmbuffer/ */
  array_uint32_16_23_4 *rxmbuffer_23_4,
  /* _L4/, length_in/ */
  length_t_udp *length_in_23_4,
  /* _L3/, in/ */
  array_uint32_16_23 *in_23_4)
{
  kcg_size idx;

  if (kcg_lit_int32(0) <= worker_sid_23_4 && worker_sid_23_4 < kcg_lit_int32(
      4)) {
    *length_in_23_4 = (*rxmlength_23_4)[worker_sid_23_4];
    kcg_copy_array_uint32_16_23(in_23_4, &(*rxmbuffer_23_4)[worker_sid_23_4]);
  }
  else {
    *length_in_23_4 = nil_udp;
    for (idx = 0; idx < 23; idx++) {
      kcg_copy_chunk_t_udp(&(*in_23_4)[idx], (chunk_t_udp *) &Zero_udp);
    }
  }
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** collectCrops_tindyguard_session_23_4.c
** Generation date: 2020-03-11T13:42:24
*************************************************************$ */

