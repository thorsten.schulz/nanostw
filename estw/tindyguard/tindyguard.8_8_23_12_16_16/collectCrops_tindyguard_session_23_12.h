/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _collectCrops_tindyguard_session_23_12_H_
#define _collectCrops_tindyguard_session_23_12_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::collectCrops/ */
extern void collectCrops_tindyguard_session_23_12(
  /* _L5/, worker_sid/ */
  size_tindyguardTypes worker_sid_23_12,
  /* _L2/, rxmlength/ */
  array_int32_12 *rxmlength_23_12,
  /* _L1/, rxmbuffer/ */
  array_uint32_16_23_12 *rxmbuffer_23_12,
  /* _L4/, length_in/ */
  length_t_udp *length_in_23_12,
  /* _L3/, in/ */
  array_uint32_16_23 *in_23_12);



#endif /* _collectCrops_tindyguard_session_23_12_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** collectCrops_tindyguard_session_23_12.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

