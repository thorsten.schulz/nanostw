/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */
#ifndef _mapOutgoing_tindyguard_session_16_H_
#define _mapOutgoing_tindyguard_session_16_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapOutgoing/ */
extern void mapOutgoing_tindyguard_session_16(
  /* _L7/, sid/ */
  size_tindyguardTypes sid_16,
  /* _L5/, length/ */
  array_int32_16 *length_16,
  /* _L8/, txm_sid/ */
  array_int32_16 *txm_sid_16,
  /* _L2/, length_not_taken/ */
  array_int32_16 *length_not_taken_16,
  /* _L3/, length_picked/ */
  array_int32_16 *length_picked_16);



#endif /* _mapOutgoing_tindyguard_session_16_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapOutgoing_tindyguard_session_16.h
** Generation date: 2020-03-11T15:14:40
*************************************************************$ */

