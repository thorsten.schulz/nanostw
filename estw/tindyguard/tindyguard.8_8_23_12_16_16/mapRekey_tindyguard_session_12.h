/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _mapRekey_tindyguard_session_12_H_
#define _mapRekey_tindyguard_session_12_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::mapRekey/ */
extern void mapRekey_tindyguard_session_12(
  /* rekey/ */
  array_int32_12 *rekey_12,
  /* s/ */
  Session_tindyguardTypes *s_12,
  /* pid_cmd_in/ */
  size_tindyguardTypes pid_cmd_in_12,
  /* pid_not_taken/ */
  array_int32_12 *pid_not_taken_12,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_12);



#endif /* _mapRekey_tindyguard_session_12_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapRekey_tindyguard_session_12.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

