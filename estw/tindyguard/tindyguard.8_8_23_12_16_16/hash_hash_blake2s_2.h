/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */
#ifndef _hash_hash_blake2s_2_H_
#define _hash_hash_blake2s_2_H_

#include "kcg_types.h"
#include "stream_it_hash_blake2s.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hash/ */
extern void hash_hash_blake2s_2(
  /* _L64/, msg/ */
  array_uint32_16_2 *msg_2,
  /* _L66/, len/ */
  size_slideTypes len_2,
  /* _L70/, keybytes/ */
  size_slideTypes keybytes_2,
  /* _L61/, hash/ */
  Hash_hash_blake2s *hash_2);



#endif /* _hash_hash_blake2s_2_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hash_hash_blake2s_2.h
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

