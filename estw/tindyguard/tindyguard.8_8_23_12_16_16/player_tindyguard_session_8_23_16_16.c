/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "player_tindyguard_session_8_23_16_16.h"

/* tindyguard::session::player/ */
void player_tindyguard_session_8_23_16_16(
  /* _L19/, fd_bad/ */
  kcg_bool fd_bad_8_23_16_16,
  /* rxdlength/ */
  array_int32_16 *rxdlength_8_23_16_16,
  /* rxdbuffer/ */
  array_uint32_16_24_16 *rxdbuffer_8_23_16_16,
  /* rxdendpoint/ */
  _5_array *rxdendpoint_8_23_16_16,
  /* pid/ */
  size_tindyguardTypes pid_8_23_16_16,
  /* txm/ */
  array_uint32_16_23_16 *txm_8_23_16_16,
  /* _L10/, txmlen/ */
  array_int32_16 *txmlen_8_23_16_16,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8_23_16_16,
  /* knownPeers/ */
  _4_array *knownPeers_8_23_16_16,
  /* socket/ */
  fd_t_udp socket_8_23_16_16,
  /* now/ */
  kcg_int64 now_8_23_16_16,
  /* _L1/, fd_failing/ */
  kcg_bool *fd_failing_8_23_16_16,
  /* rxmlength/ */
  array_int32_16 *rxmlength_8_23_16_16,
  /* rxmbuffer/ */
  array_uint32_16_23_16 *rxmbuffer_8_23_16_16,
  /* taken/ */
  array_bool_16 *taken_8_23_16_16,
  /* _L15/, soo/ */
  Session_tindyguardTypes *soo_8_23_16_16,
  /* _L5/, weakKey/, weakKeyFail/ */
  kcg_bool *weakKeyFail_8_23_16_16,
  /* latchTAI/ */
  kcg_bool *latchTAI_8_23_16_16,
  /* _L14/, timedOut/, timeout/ */
  kcg_bool *timeout_8_23_16_16,
  /* _L27/, reInitPid/ */
  size_tindyguardTypes *reInitPid_8_23_16_16,
  outC_player_tindyguard_session_8_23_16_16 *outC)
{
  kcg_bool acc;
  kcg_size idx;
  kcg_bool acc1;
  Session_tindyguardTypes acc2;
  kcg_bool acc3;
  Session_tindyguardTypes acc4;
  Session_tindyguardTypes acc5;
  /* @6/_L5/ */
  array_uint32_16_4 _L5_respondSending_1_8_23;
  /* @6/_L6/, @7/s/ */
  Session_tindyguardTypes _L6_respondSending_1_8_23;
  kcg_bool tmp;
  array_int32_16 tmp6;
  kcg_bool cond_iterw;
  Session_tindyguardTypes acc7;
  /* @7/IfBlock1:else:then:_L9/, @8/_L5/, @9/IfBlock1:, @9/failed/ */
  kcg_bool _L5_wrapSend_1_23;
  /* @8/_L9/, @9/s/ */
  Session_tindyguardTypes _L9_wrapSend_1_23;
  /* @8/_L8/ */
  array_uint32_16_24 _L8_wrapSend_1_23;
  /* @6/_L4/, @7/length/, @8/_L7/ */
  length_t_udp _L7_wrapSend_1_23;
  /* @8/_L6/, @9/length/ */
  length_t_udp _L6_wrapSend_1_23;
  kcg_bool tmp8;
  array_uint32_16_23 pow;
  /* @1/_/v4/, @3/_/v4/ */
  kcg_size v4_times_2_size;
  /* @4/_/v4/ */
  kcg_size v4_times_3_size;
  /* @2/_/v4/, @5/_/v4/ */
  kcg_size v4_times_4_size;
  /* SM1:respond:_L25/, SM2:work_out:_L41/, taken/ */
  array_bool_16 taken_partial_8_23_16_16;
  kcg_int32 noname;
  /* SM1:respond:_L41/ */
  array_bool_16 _L41_respond_SM1_8_23_16_16;
  /* SM1:respond:<1>, SM1:respond:_L28/ */
  kcg_bool _L28_respond_SM1_8_23_16_16;
  /* SM1:, SM1:initiate:<1>, SM1:initiate:_L26/ */
  kcg_bool _L26_initiate_SM1_8_23_16_16;
  /* @1/_/c/, SM1:await_response:<2>, SM1:initiate:_L27/ */
  kcg_bool _L27_initiate_SM1_8_23_16_16;
  /* @9/IfBlock1:else:then:_L10/,
     @9/sent/,
     SM1:await_response:<1><1>,
     SM1:await_response:_L2/ */
  kcg_bool _L2_await_response_SM1_8_23_16_16;
  /* SM1:await_ackn:_L2/, rxmlength/ */
  array_int32_16 rxmlength_partial_8_23_16_16;
  /* SM1:await_ackn:_L1/, rxmbuffer/ */
  array_uint32_16_23_16 rxmbuffer_partial_8_23_16_16;
  /* @9/IfBlock1:else:then:_L9/, SM1:await_ackn:_L4/, SM1:await_response:<1> */
  kcg_bool _L4_await_ackn_SM1_8_23_16_16;
  /* SM1:Initiator_inTransmission:_L37/, rxmlength/ */
  array_int32_16 _9_rxmlength_partial_8_23_16_16;
  /* SM1:Initiator_inTransmission:_L38/, rxmbuffer/ */
  array_uint32_16_23_16 _10_rxmbuffer_partial_8_23_16_16;
  /* @3/_/c/, SM1:, SM1:Initiator_inTransmission:_L45/, SM1:initiate: */
  kcg_bool _L45_Initiator_inTransmission_SM1_8_23_16_16;
  /* @9/IfBlock1:else:, SM1:Initiator_inTransmission:_L39/, SM1:await_ackn:<2> */
  kcg_bool _L39_Initiator_inTransmission_SM1_8_23_16_16;
  /* SM1:Initiator_inTransmission:_L36/, s_u1/ */
  Session_tindyguardTypes _L36_Initiator_inTransmission_SM1_8_23_16_16;
  /* SM1:Responder_inTransmission:_L37/, rxmlength/ */
  array_int32_16 _11_rxmlength_partial_8_23_16_16;
  /* SM1:Responder_inTransmission:_L38/, rxmbuffer/ */
  array_uint32_16_23_16 _12_rxmbuffer_partial_8_23_16_16;
  /* @8/_L10/, SM1:Responder_inTransmission:_L39/, _L20/, reject/ */
  kcg_bool _L39_Responder_inTransmission_SM1_8_23_16_16;
  /* @6/_L8/, keepAlive/ */
  kcg_bool keepAlive_8_23_16_16;
  /* SM1: */
  _6_SSM_ST_SM1 SM1_state_act_8_23_16_16;
  /* SM1: */
  _7_SSM_TR_SM1 SM1_fired_strong_8_23_16_16;
  /* SM2: */
  SSM_ST_SM2 SM2_state_act_8_23_16_16;
  /* s_u1/ */
  Session_tindyguardTypes s_u1_8_23_16_16;

  _L39_Responder_inTransmission_SM1_8_23_16_16 = outC->s_8_23_16_16.sTime +
    WG_Reject_After_Time_tindyguard < now_8_23_16_16;
  /* SM1: */
  switch (outC->SM1_state_nxt_8_23_16_16) {
    case SSM_st_Responder_inTransmission_SM1 :
      _L45_Initiator_inTransmission_SM1_8_23_16_16 =
        _L39_Responder_inTransmission_SM1_8_23_16_16;
      if (_L39_Responder_inTransmission_SM1_8_23_16_16) {
        SM1_state_act_8_23_16_16 = SSM_st_reset_SM1;
        SM1_fired_strong_8_23_16_16 =
          SSM_TR_Responder_inTransmission_reset_1_Responder_inTransmission_SM1;
      }
      else {
        SM1_state_act_8_23_16_16 = SSM_st_Responder_inTransmission_SM1;
        SM1_fired_strong_8_23_16_16 = _10_SSM_TR_no_trans_SM1;
      }
      *timeout_8_23_16_16 = kcg_false;
      break;
    case SSM_st_Initiator_inTransmission_SM1 :
      _L45_Initiator_inTransmission_SM1_8_23_16_16 =
        _L39_Responder_inTransmission_SM1_8_23_16_16;
      if (_L39_Responder_inTransmission_SM1_8_23_16_16) {
        SM1_state_act_8_23_16_16 = SSM_st_reset_SM1;
        SM1_fired_strong_8_23_16_16 =
          SSM_TR_Initiator_inTransmission_reset_1_Initiator_inTransmission_SM1;
      }
      else {
        SM1_state_act_8_23_16_16 = SSM_st_Initiator_inTransmission_SM1;
        SM1_fired_strong_8_23_16_16 = _10_SSM_TR_no_trans_SM1;
      }
      *timeout_8_23_16_16 = kcg_false;
      break;
    case SSM_st_await_ackn_SM1 :
      _L39_Initiator_inTransmission_SM1_8_23_16_16 = !outC->fail_8_23_16_16;
      _L45_Initiator_inTransmission_SM1_8_23_16_16 =
        _L39_Responder_inTransmission_SM1_8_23_16_16 ||
        _L39_Initiator_inTransmission_SM1_8_23_16_16;
      if (_L39_Responder_inTransmission_SM1_8_23_16_16) {
        SM1_state_act_8_23_16_16 = SSM_st_reset_SM1;
        SM1_fired_strong_8_23_16_16 = SSM_TR_await_ackn_reset_1_await_ackn_SM1;
      }
      else if (_L39_Initiator_inTransmission_SM1_8_23_16_16) {
        SM1_state_act_8_23_16_16 = SSM_st_Responder_inTransmission_SM1;
        SM1_fired_strong_8_23_16_16 =
          SSM_TR_await_ackn_Responder_inTransmission_2_await_ackn_SM1;
      }
      else {
        SM1_state_act_8_23_16_16 = SSM_st_await_ackn_SM1;
        SM1_fired_strong_8_23_16_16 = _10_SSM_TR_no_trans_SM1;
      }
      *timeout_8_23_16_16 = kcg_false;
      break;
    case SSM_st_await_response_SM1 :
      _L27_initiate_SM1_8_23_16_16 = !outC->fail_8_23_16_16;
      _L2_await_response_SM1_8_23_16_16 = outC->s_8_23_16_16.tx_cnt >=
        retryHandshake_tindyguard_conf;
      _L4_await_ackn_SM1_8_23_16_16 = outC->s_8_23_16_16.txTime +
        waitForResponseBeforeRetry_tindyguard_session < now_8_23_16_16;
      _L45_Initiator_inTransmission_SM1_8_23_16_16 =
        _L4_await_ackn_SM1_8_23_16_16 || _L27_initiate_SM1_8_23_16_16;
      /* SM1:await_response:<1> */
      if (_L4_await_ackn_SM1_8_23_16_16) {
        /* SM1:await_response:<1><1> */
        if (_L2_await_response_SM1_8_23_16_16) {
          SM1_state_act_8_23_16_16 = SSM_st_reset_SM1;
          SM1_fired_strong_8_23_16_16 =
            SSM_TR_await_response_reset_1_1_await_response_SM1;
          *timeout_8_23_16_16 = kcg_true;
        }
        else {
          SM1_state_act_8_23_16_16 = SSM_st_initiate_SM1;
          SM1_fired_strong_8_23_16_16 =
            SSM_TR_await_response_initiate_2_1_await_response_SM1;
          *timeout_8_23_16_16 = kcg_false;
        }
      }
      else {
        if (_L27_initiate_SM1_8_23_16_16) {
          SM1_state_act_8_23_16_16 = SSM_st_Initiator_inTransmission_SM1;
          SM1_fired_strong_8_23_16_16 =
            SSM_TR_await_response_Initiator_inTransmission_2_await_response_SM1;
        }
        else {
          SM1_state_act_8_23_16_16 = SSM_st_await_response_SM1;
          SM1_fired_strong_8_23_16_16 = _10_SSM_TR_no_trans_SM1;
        }
        *timeout_8_23_16_16 = kcg_false;
      }
      break;
    case SSM_st_initiate_SM1 :
      _L26_initiate_SM1_8_23_16_16 = !(outC->fdfail_8_23_16_16 ||
          outC->fail_8_23_16_16);
      _L45_Initiator_inTransmission_SM1_8_23_16_16 = _L26_initiate_SM1_8_23_16_16;
      if (_L26_initiate_SM1_8_23_16_16) {
        SM1_state_act_8_23_16_16 = SSM_st_await_response_SM1;
        SM1_fired_strong_8_23_16_16 = SSM_TR_initiate_await_response_1_initiate_SM1;
      }
      else {
        SM1_state_act_8_23_16_16 = SSM_st_initiate_SM1;
        SM1_fired_strong_8_23_16_16 = _10_SSM_TR_no_trans_SM1;
      }
      *timeout_8_23_16_16 = kcg_false;
      break;
    case SSM_st_respond_SM1 :
      acc = !outC->fail_8_23_16_16;
      _L28_respond_SM1_8_23_16_16 = fd_bad_8_23_16_16 ||
        (outC->fail_8_23_16_16 && pid_8_23_16_16 != PeerFromRxPacket_tindyguardTypes);
      _L45_Initiator_inTransmission_SM1_8_23_16_16 =
        _L28_respond_SM1_8_23_16_16 || acc;
      if (_L28_respond_SM1_8_23_16_16) {
        SM1_state_act_8_23_16_16 = SSM_st_reset_SM1;
        SM1_fired_strong_8_23_16_16 = SSM_TR_respond_reset_1_respond_SM1;
      }
      else if (acc) {
        SM1_state_act_8_23_16_16 = SSM_st_await_ackn_SM1;
        SM1_fired_strong_8_23_16_16 = SSM_TR_respond_await_ackn_2_respond_SM1;
      }
      else {
        SM1_state_act_8_23_16_16 = SSM_st_respond_SM1;
        SM1_fired_strong_8_23_16_16 = _10_SSM_TR_no_trans_SM1;
      }
      *timeout_8_23_16_16 = kcg_false;
      break;
    case SSM_st_reset_SM1 :
      acc3 = pid_8_23_16_16 > nil_tindyguard_session;
      acc1 = pid_8_23_16_16 == PeerFromRxPacket_tindyguardTypes;
      _L45_Initiator_inTransmission_SM1_8_23_16_16 = acc1 || acc3;
      if (acc1) {
        SM1_state_act_8_23_16_16 = SSM_st_respond_SM1;
        SM1_fired_strong_8_23_16_16 = SSM_TR_reset_respond_1_reset_SM1;
      }
      else if (acc3) {
        SM1_state_act_8_23_16_16 = SSM_st_initiate_SM1;
        SM1_fired_strong_8_23_16_16 = SSM_TR_reset_initiate_2_reset_SM1;
      }
      else {
        SM1_state_act_8_23_16_16 = SSM_st_reset_SM1;
        SM1_fired_strong_8_23_16_16 = _10_SSM_TR_no_trans_SM1;
      }
      *timeout_8_23_16_16 = kcg_false;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* SM1: */
  switch (SM1_state_act_8_23_16_16) {
    case SSM_st_Responder_inTransmission_SM1 :
      _L39_Responder_inTransmission_SM1_8_23_16_16 = kcg_true;
      if (_L45_Initiator_inTransmission_SM1_8_23_16_16) {
        outC->init = kcg_true;
      }
      kcg_copy_Session_tindyguardTypes(&s_u1_8_23_16_16, &outC->s_8_23_16_16);
      /* SM1:Responder_inTransmission:_L36= */
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_Session_tindyguardTypes(&acc7, &s_u1_8_23_16_16);
        acc = _L39_Responder_inTransmission_SM1_8_23_16_16;
        /* SM1:Responder_inTransmission:_L36=(tindyguard::data::unwrap#5)/ */
        unwrap_tindyguard_data_23(
          &acc7,
          acc,
          (*rxdlength_8_23_16_16)[idx],
          &(*rxdbuffer_8_23_16_16)[idx],
          &(*rxdendpoint_8_23_16_16)[idx],
          &s_u1_8_23_16_16,
          &_L39_Responder_inTransmission_SM1_8_23_16_16,
          &_11_rxmlength_partial_8_23_16_16[idx],
          &_12_rxmbuffer_partial_8_23_16_16[idx]);
      }
      break;
    case SSM_st_Initiator_inTransmission_SM1 :
      _L39_Initiator_inTransmission_SM1_8_23_16_16 = kcg_true;
      if (_L45_Initiator_inTransmission_SM1_8_23_16_16) {
        outC->init1 = kcg_true;
      }
      kcg_copy_Session_tindyguardTypes(
        &_L36_Initiator_inTransmission_SM1_8_23_16_16,
        &outC->s_8_23_16_16);
      /* SM1:Initiator_inTransmission:_L36= */
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_Session_tindyguardTypes(
          &_L9_wrapSend_1_23,
          &_L36_Initiator_inTransmission_SM1_8_23_16_16);
        acc1 = _L39_Initiator_inTransmission_SM1_8_23_16_16;
        /* SM1:Initiator_inTransmission:_L36=(tindyguard::data::unwrap#4)/ */
        unwrap_tindyguard_data_23(
          &_L9_wrapSend_1_23,
          acc1,
          (*rxdlength_8_23_16_16)[idx],
          &(*rxdbuffer_8_23_16_16)[idx],
          &(*rxdendpoint_8_23_16_16)[idx],
          &_L36_Initiator_inTransmission_SM1_8_23_16_16,
          &_L39_Initiator_inTransmission_SM1_8_23_16_16,
          &_9_rxmlength_partial_8_23_16_16[idx],
          &_10_rxmbuffer_partial_8_23_16_16[idx]);
      }
      kcg_copy_Session_tindyguardTypes(
        &s_u1_8_23_16_16,
        &_L36_Initiator_inTransmission_SM1_8_23_16_16);
      break;
    case SSM_st_await_ackn_SM1 :
      _L4_await_ackn_SM1_8_23_16_16 = kcg_true;
      if (_L45_Initiator_inTransmission_SM1_8_23_16_16) {
        outC->init2 = kcg_true;
      }
      /* @2/_/v4= */
      if (outC->init2) {
        v4_times_4_size = 1;
      }
      else {
        v4_times_4_size = outC->v3_times_7_size;
      }
      kcg_copy_Session_tindyguardTypes(&s_u1_8_23_16_16, &outC->s_8_23_16_16);
      /* SM1:await_ackn:_L3= */
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_Session_tindyguardTypes(&acc2, &s_u1_8_23_16_16);
        acc3 = _L4_await_ackn_SM1_8_23_16_16;
        /* SM1:await_ackn:_L3=(tindyguard::data::unwrap#1)/ */
        unwrap_tindyguard_data_23(
          &acc2,
          acc3,
          (*rxdlength_8_23_16_16)[idx],
          &(*rxdbuffer_8_23_16_16)[idx],
          &(*rxdendpoint_8_23_16_16)[idx],
          &s_u1_8_23_16_16,
          &_L4_await_ackn_SM1_8_23_16_16,
          &rxmlength_partial_8_23_16_16[idx],
          &rxmbuffer_partial_8_23_16_16[idx]);
      }
      break;
    case SSM_st_await_response_SM1 :
      _L2_await_response_SM1_8_23_16_16 = kcg_true;
      kcg_copy_Session_tindyguardTypes(&s_u1_8_23_16_16, &outC->s_8_23_16_16);
      /* SM1:await_response:_L1= */
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_Session_tindyguardTypes(&acc4, &s_u1_8_23_16_16);
        tmp = _L2_await_response_SM1_8_23_16_16;
        /* SM1:await_response:_L1=(tindyguard::handshake::eval_response#1)/ */
        eval_response_tindyguard_handshake_23(
          &acc4,
          tmp,
          (*rxdlength_8_23_16_16)[idx],
          &(*rxdbuffer_8_23_16_16)[idx],
          &(*rxdendpoint_8_23_16_16)[idx],
          sks_8_23_16_16,
          &s_u1_8_23_16_16,
          &_L2_await_response_SM1_8_23_16_16);
      }
      break;
    case SSM_st_initiate_SM1 :
      if (_L45_Initiator_inTransmission_SM1_8_23_16_16) {
        outC->init3 = kcg_true;
        /* SM1:initiate:_L25=(tindyguard::handshake::initSending#1)/ */
        initSending_reset_tindyguard_handshake_8(&outC->Context_initSending_1);
      }
      /* @1/_/v4= */
      if (outC->init3) {
        v4_times_2_size = 3;
      }
      else {
        v4_times_2_size = outC->v3_times_1_size;
      }
      /* SM1:initiate:_L25=(tindyguard::handshake::initSending#1)/ */
      initSending_tindyguard_handshake_8(
        &outC->s_8_23_16_16,
        pid_8_23_16_16,
        knownPeers_8_23_16_16,
        sks_8_23_16_16,
        socket_8_23_16_16,
        now_8_23_16_16,
        &s_u1_8_23_16_16,
        &_L26_initiate_SM1_8_23_16_16,
        &_L27_initiate_SM1_8_23_16_16,
        &outC->Context_initSending_1);
      /* @1/_/v3= */
      if (v4_times_2_size < 0) {
        outC->v3_times_1_size = v4_times_2_size;
      }
      else /* @1/_/v3= */
      if (_L27_initiate_SM1_8_23_16_16) {
        outC->v3_times_1_size = v4_times_2_size - 1;
      }
      else {
        outC->v3_times_1_size = v4_times_2_size;
      }
      break;
    case SSM_st_respond_SM1 :
      if (_L45_Initiator_inTransmission_SM1_8_23_16_16) {
        /* @6/_L4=(tindyguard::handshake::respond#1)/ */
        for (idx = 0; idx < 16; idx++) {
          /* @6/_L4=(tindyguard::handshake::respond#1)/ */
          respond_reset_tindyguard_handshake_8(
            &outC->Context_respond_1_respondSending_1[idx]);
        }
      }
      kcg_copy_Session_tindyguardTypes(
        &s_u1_8_23_16_16,
        (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
      /* SM1:respond:_L26= */
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_Session_tindyguardTypes(&acc5, &s_u1_8_23_16_16);
        /* @6/_L4=(tindyguard::handshake::respond#1)/ */
        respond_tindyguard_handshake_8(
          (*rxdlength_8_23_16_16)[idx],
          (array_uint32_16_4 *) &(*rxdbuffer_8_23_16_16)[idx][0],
          &(*rxdendpoint_8_23_16_16)[idx],
          sks_8_23_16_16,
          &acc5,
          knownPeers_8_23_16_16,
          now_8_23_16_16,
          &_L7_wrapSend_1_23,
          &_L5_respondSending_1_8_23,
          &_L6_respondSending_1_8_23,
          &taken_partial_8_23_16_16[idx],
          &keepAlive_8_23_16_16,
          &_L41_respond_SM1_8_23_16_16[idx],
          &outC->Context_respond_1_respondSending_1[idx]);
        tmp8 = /* @6/_L3=(udp::sendChunksTo#1)/ */
          sendChunks04_udp_specialization(
            _L7_wrapSend_1_23,
            kcg_lit_int32(0),
            &_L5_respondSending_1_8_23,
            &_L6_respondSending_1_8_23.peer.endpoint,
            socket_8_23_16_16);
        /* @7/IfBlock1: */
        if (tmp8) {
          kcg_copy_Session_tindyguardTypes(&s_u1_8_23_16_16, &_L6_respondSending_1_8_23);
          s_u1_8_23_16_16.pid = InvalidPeer_tindyguardTypes;
          kcg_copy_Peer_tindyguardTypes(
            &s_u1_8_23_16_16.peer,
            (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
          kcg_copy_State_tindyguard_handshake(
            &s_u1_8_23_16_16.handshake,
            (State_tindyguard_handshake *) &EmptyState_tindyguard_handshake);
        }
        else {
          cond_iterw = _L7_wrapSend_1_23 > nil_tindyguard_session;
          /* @7/IfBlock1:else: */
          if (cond_iterw) {
            _L5_wrapSend_1_23 = _L7_wrapSend_1_23 == kcg_lit_int32(32);
            kcg_copy_Session_tindyguardTypes(&s_u1_8_23_16_16, &_L6_respondSending_1_8_23);
            s_u1_8_23_16_16.tx_cnt = _L6_respondSending_1_8_23.tx_cnt + kcg_lit_int32(1);
            s_u1_8_23_16_16.sentKeepAlive = _L5_wrapSend_1_23;
            s_u1_8_23_16_16.gotKeepAlive = !_L5_wrapSend_1_23;
            s_u1_8_23_16_16.txTime = now_8_23_16_16;
          }
          else {
            kcg_copy_Session_tindyguardTypes(&s_u1_8_23_16_16, &_L6_respondSending_1_8_23);
          }
        }
        _L28_respond_SM1_8_23_16_16 = !tmp8;
        noname = /* SM1:respond:_L26= */(kcg_int32) (idx + 1);
        /* SM1:respond:_L26= */
        if (!_L28_respond_SM1_8_23_16_16) {
          break;
        }
      }
#ifdef KCG_MAPW_CPY

      /* SM1:respond:_L26= */
      for (idx = /* SM1:respond:_L26= */(kcg_size) noname; idx < 16; idx++) {
        taken_partial_8_23_16_16[idx] = kcg_true;
        _L41_respond_SM1_8_23_16_16[idx] = kcg_false;
      }
#endif /* KCG_MAPW_CPY */

      break;
    case SSM_st_reset_SM1 :
      kcg_copy_Session_tindyguardTypes(
        &s_u1_8_23_16_16,
        (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  acc = !s_u1_8_23_16_16.sentKeepAlive && s_u1_8_23_16_16.peer.endpoint.mtime <
    s_u1_8_23_16_16.txTime && s_u1_8_23_16_16.txTime +
    waitForResponseBeforeRetry_tindyguard_session +
    waitForMoreBeforeKeepAlive_tindyguard_session < now_8_23_16_16;
  /* SM1: */
  switch (SM1_state_act_8_23_16_16) {
    case SSM_st_Responder_inTransmission_SM1 :
      outC->fail_8_23_16_16 = _L39_Responder_inTransmission_SM1_8_23_16_16;
      break;
    case SSM_st_Initiator_inTransmission_SM1 :
      /* @3/_/v4= */
      if (outC->init1) {
        v4_times_2_size = 1;
      }
      else {
        v4_times_2_size = outC->v3_times_2_size;
      }
      outC->fail_8_23_16_16 = _L39_Initiator_inTransmission_SM1_8_23_16_16;
      break;
    case SSM_st_await_ackn_SM1 :
      /* @2/_/v3= */
      if (v4_times_4_size < 0) {
        outC->v3_times_7_size = v4_times_4_size;
      }
      else /* @2/_/v3= */
      if (acc) {
        outC->v3_times_7_size = v4_times_4_size - 1;
      }
      else {
        outC->v3_times_7_size = v4_times_4_size;
      }
      outC->fail_8_23_16_16 = _L4_await_ackn_SM1_8_23_16_16;
      break;
    case SSM_st_await_response_SM1 :
      outC->fail_8_23_16_16 = _L2_await_response_SM1_8_23_16_16;
      break;
    case SSM_st_initiate_SM1 :
      outC->fail_8_23_16_16 = _L27_initiate_SM1_8_23_16_16;
      break;
    case SSM_st_respond_SM1 :
      outC->fail_8_23_16_16 = kcg_true;
      /* SM1:respond:_L30= */
      for (idx = 0; idx < 16; idx++) {
        outC->fail_8_23_16_16 = outC->fail_8_23_16_16 && taken_partial_8_23_16_16[idx];
      }
      break;
    case SSM_st_reset_SM1 :
      outC->fail_8_23_16_16 = kcg_false;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* _L7= */
  for (idx = 0; idx < 16; idx++) {
    acc1 = (*txmlen_8_23_16_16)[idx] <= nil_udp;
    /* _L7= */
    if (!acc1) {
      break;
    }
  }
  /* SM1: */
  switch (SM1_state_act_8_23_16_16) {
    case SSM_st_Responder_inTransmission_SM1 :
      tmp = kcg_false;
      /* @5/_/v4= */
      if (outC->init) {
        v4_times_4_size = 1;
      }
      else {
        v4_times_4_size = outC->v3_times_4_size;
      }
      /* @5/_/v3= */
      if (v4_times_4_size < 0) {
        outC->v3_times_4_size = v4_times_4_size;
      }
      else /* @5/_/v3= */
      if (acc) {
        outC->v3_times_4_size = v4_times_4_size - 1;
      }
      else {
        outC->v3_times_4_size = v4_times_4_size;
      }
      acc3 = kcg_true;
      tmp8 = acc && outC->v3_times_4_size == 0;
      break;
    case SSM_st_Initiator_inTransmission_SM1 :
      tmp = kcg_false;
      _L45_Initiator_inTransmission_SM1_8_23_16_16 = (!acc1 &&
          _L36_Initiator_inTransmission_SM1_8_23_16_16.peer.endpoint.mtime <=
          _L36_Initiator_inTransmission_SM1_8_23_16_16.txTime &&
          _L36_Initiator_inTransmission_SM1_8_23_16_16.sTime +
          WG_Rekey_After_Time_tindyguard < now_8_23_16_16) ||
        (!outC->fail_8_23_16_16 &&
          _L36_Initiator_inTransmission_SM1_8_23_16_16.peer.endpoint.mtime >
          _L36_Initiator_inTransmission_SM1_8_23_16_16.txTime &&
          _L36_Initiator_inTransmission_SM1_8_23_16_16.sTime + kcg_lit_int64(
            165000000000) < now_8_23_16_16);
      /* @3/_/v3= */
      if (v4_times_2_size < 0) {
        outC->v3_times_2_size = v4_times_2_size;
      }
      else /* @3/_/v3= */
      if (_L45_Initiator_inTransmission_SM1_8_23_16_16) {
        outC->v3_times_2_size = v4_times_2_size - 1;
      }
      else {
        outC->v3_times_2_size = v4_times_2_size;
      }
      /* @4/_/v4= */
      if (outC->init1) {
        v4_times_3_size = 1;
      }
      else {
        v4_times_3_size = outC->v3_times_3_size;
      }
      /* @4/_/v3= */
      if (v4_times_3_size < 0) {
        outC->v3_times_3_size = v4_times_3_size;
      }
      else /* @4/_/v3= */
      if (acc) {
        outC->v3_times_3_size = v4_times_3_size - 1;
      }
      else {
        outC->v3_times_3_size = v4_times_3_size;
      }
      acc3 = kcg_true;
      tmp8 = (_L45_Initiator_inTransmission_SM1_8_23_16_16 &&
          outC->v3_times_2_size == 0) || (acc && outC->v3_times_3_size == 0);
      break;
    case SSM_st_await_ackn_SM1 :
      tmp = kcg_false;
      acc3 = !outC->fail_8_23_16_16;
      tmp8 = acc && outC->v3_times_7_size == 0;
      break;
    case SSM_st_await_response_SM1 :
      acc3 = !_L2_await_response_SM1_8_23_16_16;
      tmp = acc3;
      tmp8 = kcg_false;
      break;
    case SSM_st_initiate_SM1 :
      tmp = kcg_false;
      acc3 = kcg_false;
      tmp8 = kcg_false;
      break;
    case SSM_st_respond_SM1 :
      tmp = kcg_false;
      acc3 = kcg_false;
      tmp8 = kcg_false;
      break;
    case SSM_st_reset_SM1 :
      tmp = kcg_false;
      acc3 = kcg_false;
      tmp8 = kcg_false;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  keepAlive_8_23_16_16 = tmp || (!s_u1_8_23_16_16.gotKeepAlive &&
      s_u1_8_23_16_16.peer.endpoint.mtime > s_u1_8_23_16_16.txTime &&
      s_u1_8_23_16_16.txTime > kcg_lit_int64(0) &&
      s_u1_8_23_16_16.peer.endpoint.mtime +
      waitForMoreBeforeKeepAlive_tindyguard_session < now_8_23_16_16);
  /* SM2: */
  switch (outC->SM2_state_nxt_8_23_16_16) {
    case SSM_st_idle_SM2 :
      if (acc3 && (!acc1 || keepAlive_8_23_16_16)) {
        SM2_state_act_8_23_16_16 = SSM_st_work_out_SM2;
      }
      else {
        SM2_state_act_8_23_16_16 = SSM_st_idle_SM2;
      }
      break;
    case SSM_st_work_out_SM2 :
      if (!acc3 || (acc1 && !keepAlive_8_23_16_16)) {
        SM2_state_act_8_23_16_16 = SSM_st_idle_SM2;
      }
      else {
        SM2_state_act_8_23_16_16 = SSM_st_work_out_SM2;
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* SM2: */
  switch (SM2_state_act_8_23_16_16) {
    case SSM_st_idle_SM2 :
      kcg_copy_Session_tindyguardTypes(&outC->s_8_23_16_16, &s_u1_8_23_16_16);
      for (idx = 0; idx < 16; idx++) {
        (*taken_8_23_16_16)[idx] = kcg_false;
      }
      outC->SM2_state_nxt_8_23_16_16 = SSM_st_idle_SM2;
      acc = kcg_false;
      break;
    case SSM_st_work_out_SM2 :
      /* SM2:work_out:_L25= */
      if (keepAlive_8_23_16_16 && acc1) {
        kcg_copy_array_int32_16(&tmp6, txmlen_8_23_16_16);
        tmp6[0] = kcg_lit_int32(0);
      }
      else {
        kcg_copy_array_int32_16(&tmp6, txmlen_8_23_16_16);
      }
      kcg_copy_Session_tindyguardTypes(&outC->s_8_23_16_16, &s_u1_8_23_16_16);
      /* SM2:work_out:_L23= */
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_Session_tindyguardTypes(&acc7, &outC->s_8_23_16_16);
        /* @8/_L6=(tindyguard::data::wrap#1)/ */
        wrap_tindyguard_data_23(
          tmp6[idx],
          &(*txm_8_23_16_16)[idx],
          &acc7,
          &_L6_wrapSend_1_23,
          &_L7_wrapSend_1_23,
          &_L8_wrapSend_1_23,
          &_L9_wrapSend_1_23,
          &_L39_Responder_inTransmission_SM1_8_23_16_16);
        _L5_wrapSend_1_23 = /* @8/_L5=(udp::sendChunksTo#1)/ */
          sendChunks24_udp_specialization(
            _L6_wrapSend_1_23,
            _L7_wrapSend_1_23,
            &_L8_wrapSend_1_23,
            &acc7.peer.endpoint,
            socket_8_23_16_16);
        /* @9/IfBlock1: */
        if (_L5_wrapSend_1_23) {
          taken_partial_8_23_16_16[idx] = kcg_false;
          kcg_copy_Session_tindyguardTypes(&outC->s_8_23_16_16, &_L9_wrapSend_1_23);
          outC->s_8_23_16_16.pid = InvalidPeer_tindyguardTypes;
          kcg_copy_Peer_tindyguardTypes(
            &outC->s_8_23_16_16.peer,
            (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
          kcg_copy_State_tindyguard_handshake(
            &outC->s_8_23_16_16.handshake,
            (State_tindyguard_handshake *) &EmptyState_tindyguard_handshake);
        }
        else {
          _L39_Initiator_inTransmission_SM1_8_23_16_16 = _L6_wrapSend_1_23 >
            nil_tindyguard_session;
          /* @9/IfBlock1:else: */
          if (_L39_Initiator_inTransmission_SM1_8_23_16_16) {
            _L4_await_ackn_SM1_8_23_16_16 = _L6_wrapSend_1_23 == kcg_lit_int32(32);
            _L2_await_response_SM1_8_23_16_16 = !_L4_await_ackn_SM1_8_23_16_16;
            taken_partial_8_23_16_16[idx] = _L2_await_response_SM1_8_23_16_16;
            kcg_copy_Session_tindyguardTypes(&outC->s_8_23_16_16, &_L9_wrapSend_1_23);
            outC->s_8_23_16_16.tx_cnt = _L9_wrapSend_1_23.tx_cnt + kcg_lit_int32(1);
            outC->s_8_23_16_16.sentKeepAlive = _L4_await_ackn_SM1_8_23_16_16;
            outC->s_8_23_16_16.gotKeepAlive = _L2_await_response_SM1_8_23_16_16;
            outC->s_8_23_16_16.txTime = now_8_23_16_16;
          }
          else {
            taken_partial_8_23_16_16[idx] = kcg_false;
            kcg_copy_Session_tindyguardTypes(&outC->s_8_23_16_16, &_L9_wrapSend_1_23);
          }
        }
        cond_iterw = !_L5_wrapSend_1_23;
        noname = /* SM2:work_out:_L23= */(kcg_int32) (idx + 1);
        /* SM2:work_out:_L23= */
        if (!cond_iterw) {
          break;
        }
      }
#ifdef KCG_MAPW_CPY

      /* SM2:work_out:_L23= */
      for (idx = /* SM2:work_out:_L23= */(kcg_size) noname; idx < 16; idx++) {
        taken_partial_8_23_16_16[idx] = kcg_false;
      }
#endif /* KCG_MAPW_CPY */

      kcg_copy_array_bool_16(taken_8_23_16_16, &taken_partial_8_23_16_16);
      outC->SM2_state_nxt_8_23_16_16 = SSM_st_work_out_SM2;
      acc = !cond_iterw;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  /* _L27= */
  if (tmp8) {
    *reInitPid_8_23_16_16 = outC->s_8_23_16_16.pid;
  }
  else {
    *reInitPid_8_23_16_16 = InvalidPeer_tindyguardTypes;
  }
  kcg_copy_Session_tindyguardTypes(soo_8_23_16_16, &outC->s_8_23_16_16);
  (*soo_8_23_16_16).transmissive = acc3;
  /* SM1: */
  switch (SM1_state_act_8_23_16_16) {
    case SSM_st_Responder_inTransmission_SM1 :
      kcg_copy_array_int32_16(
        rxmlength_8_23_16_16,
        &_11_rxmlength_partial_8_23_16_16);
      break;
    case SSM_st_Initiator_inTransmission_SM1 :
      kcg_copy_array_int32_16(rxmlength_8_23_16_16, &_9_rxmlength_partial_8_23_16_16);
      break;
    case SSM_st_await_ackn_SM1 :
      kcg_copy_array_int32_16(rxmlength_8_23_16_16, &rxmlength_partial_8_23_16_16);
      break;
    case SSM_st_await_response_SM1 :
      for (idx = 0; idx < 16; idx++) {
        (*rxmlength_8_23_16_16)[idx] = kcg_lit_int32(-1);
      }
      break;
    case SSM_st_initiate_SM1 :
      _L45_Initiator_inTransmission_SM1_8_23_16_16 =
        SM1_fired_strong_8_23_16_16 != _10_SSM_TR_no_trans_SM1;
      for (idx = 0; idx < 16; idx++) {
        (*rxmlength_8_23_16_16)[idx] = kcg_lit_int32(-1);
      }
      break;
    case SSM_st_respond_SM1 :
      for (idx = 0; idx < 16; idx++) {
        (*rxmlength_8_23_16_16)[idx] = kcg_lit_int32(-1);
      }
      break;
    case SSM_st_reset_SM1 :
      for (idx = 0; idx < 16; idx++) {
        (*rxmlength_8_23_16_16)[idx] = kcg_lit_int32(-1);
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  for (idx = 0; idx < 23; idx++) {
    kcg_copy_StreamChunk_slideTypes(
      &pow[idx],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
  }
  /* SM1: */
  switch (SM1_state_act_8_23_16_16) {
    case SSM_st_Responder_inTransmission_SM1 :
      tmp = kcg_false;
      *weakKeyFail_8_23_16_16 = kcg_false;
      *latchTAI_8_23_16_16 = kcg_false;
      kcg_copy_array_uint32_16_23_16(
        rxmbuffer_8_23_16_16,
        &_12_rxmbuffer_partial_8_23_16_16);
      break;
    case SSM_st_Initiator_inTransmission_SM1 :
      tmp = kcg_false;
      *weakKeyFail_8_23_16_16 = kcg_false;
      *latchTAI_8_23_16_16 = kcg_false;
      kcg_copy_array_uint32_16_23_16(
        rxmbuffer_8_23_16_16,
        &_10_rxmbuffer_partial_8_23_16_16);
      break;
    case SSM_st_await_ackn_SM1 :
      tmp = kcg_false;
      *weakKeyFail_8_23_16_16 = kcg_false;
      *latchTAI_8_23_16_16 = kcg_false;
      kcg_copy_array_uint32_16_23_16(
        rxmbuffer_8_23_16_16,
        &rxmbuffer_partial_8_23_16_16);
      break;
    case SSM_st_await_response_SM1 :
      tmp = kcg_false;
      *weakKeyFail_8_23_16_16 = kcg_false;
      *latchTAI_8_23_16_16 = kcg_false;
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_array_uint32_16_23(&(*rxmbuffer_8_23_16_16)[idx], &pow);
      }
      break;
    case SSM_st_initiate_SM1 :
      *latchTAI_8_23_16_16 = kcg_false;
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_array_uint32_16_23(&(*rxmbuffer_8_23_16_16)[idx], &pow);
      }
      *weakKeyFail_8_23_16_16 = _L27_initiate_SM1_8_23_16_16 &&
        outC->v3_times_1_size == 0;
      tmp = _L26_initiate_SM1_8_23_16_16;
      break;
    case SSM_st_respond_SM1 :
      *weakKeyFail_8_23_16_16 = kcg_false;
      *latchTAI_8_23_16_16 = !outC->fail_8_23_16_16;
      /* SM1:respond:_L44= */
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_array_uint32_16_23(&(*rxmbuffer_8_23_16_16)[idx], &pow);
        *weakKeyFail_8_23_16_16 = *weakKeyFail_8_23_16_16 ||
          _L41_respond_SM1_8_23_16_16[idx];
      }
      tmp = !_L28_respond_SM1_8_23_16_16;
      break;
    case SSM_st_reset_SM1 :
      tmp = kcg_false;
      *weakKeyFail_8_23_16_16 = kcg_false;
      *latchTAI_8_23_16_16 = kcg_false;
      for (idx = 0; idx < 16; idx++) {
        kcg_copy_array_uint32_16_23(&(*rxmbuffer_8_23_16_16)[idx], &pow);
      }
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
  *fd_failing_8_23_16_16 = tmp || acc || fd_bad_8_23_16_16;
  outC->fdfail_8_23_16_16 = *fd_failing_8_23_16_16;
  /* SM1: */
  switch (SM1_state_act_8_23_16_16) {
    case SSM_st_Responder_inTransmission_SM1 :
      outC->SM1_state_nxt_8_23_16_16 = SSM_st_Responder_inTransmission_SM1;
      outC->init = kcg_false;
      break;
    case SSM_st_Initiator_inTransmission_SM1 :
      outC->SM1_state_nxt_8_23_16_16 = SSM_st_Initiator_inTransmission_SM1;
      outC->init1 = kcg_false;
      break;
    case SSM_st_await_ackn_SM1 :
      outC->SM1_state_nxt_8_23_16_16 = SSM_st_await_ackn_SM1;
      outC->init2 = kcg_false;
      break;
    case SSM_st_await_response_SM1 :
      outC->SM1_state_nxt_8_23_16_16 = SSM_st_await_response_SM1;
      break;
    case SSM_st_initiate_SM1 :
      /* SM1:initiate: */
      if (_L45_Initiator_inTransmission_SM1_8_23_16_16) {
        outC->SM1_state_nxt_8_23_16_16 = SSM_st_initiate_SM1;
      }
      else if (*fd_failing_8_23_16_16 || *weakKeyFail_8_23_16_16) {
        outC->SM1_state_nxt_8_23_16_16 = SSM_st_reset_SM1;
      }
      else {
        outC->SM1_state_nxt_8_23_16_16 = SSM_st_initiate_SM1;
      }
      outC->init3 = kcg_false;
      break;
    case SSM_st_respond_SM1 :
      outC->SM1_state_nxt_8_23_16_16 = SSM_st_respond_SM1;
      break;
    case SSM_st_reset_SM1 :
      outC->SM1_state_nxt_8_23_16_16 = SSM_st_reset_SM1;
      break;
    default :
      /* this default branch is unreachable */
      break;
  }
}

#ifndef KCG_USER_DEFINED_INIT
void player_init_tindyguard_session_8_23_16_16(
  outC_player_tindyguard_session_8_23_16_16 *outC)
{
  kcg_size idx;

  outC->fail_8_23_16_16 = kcg_false;
  outC->fdfail_8_23_16_16 = kcg_false;
  outC->init = kcg_true;
  outC->init1 = kcg_true;
  outC->init2 = kcg_true;
  outC->init3 = kcg_true;
  outC->v3_times_4_size = 0;
  outC->v3_times_3_size = 0;
  outC->v3_times_2_size = 0;
  outC->v3_times_7_size = 0;
  outC->v3_times_1_size = 0;
  for (idx = 0; idx < 16; idx++) {
    /* @6/_L4=(tindyguard::handshake::respond#1)/ */
    respond_init_tindyguard_handshake_8(
      &outC->Context_respond_1_respondSending_1[idx]);
  }
  /* SM1:initiate:_L25=(tindyguard::handshake::initSending#1)/ */
  initSending_init_tindyguard_handshake_8(&outC->Context_initSending_1);
  outC->SM2_state_nxt_8_23_16_16 = SSM_st_idle_SM2;
  kcg_copy_Session_tindyguardTypes(
    &outC->s_8_23_16_16,
    (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
  outC->SM1_state_nxt_8_23_16_16 = SSM_st_reset_SM1;
}
#endif /* KCG_USER_DEFINED_INIT */


void player_reset_tindyguard_session_8_23_16_16(
  outC_player_tindyguard_session_8_23_16_16 *outC)
{
  kcg_size idx;

  outC->fail_8_23_16_16 = kcg_false;
  outC->fdfail_8_23_16_16 = kcg_false;
  outC->init = kcg_true;
  outC->init1 = kcg_true;
  outC->init2 = kcg_true;
  outC->init3 = kcg_true;
  for (idx = 0; idx < 16; idx++) {
    /* @6/_L4=(tindyguard::handshake::respond#1)/ */
    respond_reset_tindyguard_handshake_8(
      &outC->Context_respond_1_respondSending_1[idx]);
  }
  /* SM1:initiate:_L25=(tindyguard::handshake::initSending#1)/ */
  initSending_reset_tindyguard_handshake_8(&outC->Context_initSending_1);
  outC->SM2_state_nxt_8_23_16_16 = SSM_st_idle_SM2;
  kcg_copy_Session_tindyguardTypes(
    &outC->s_8_23_16_16,
    (Session_tindyguardTypes *) &EmptySession_tindyguardTypes);
  outC->SM1_state_nxt_8_23_16_16 = SSM_st_reset_SM1;
}

/*
  Expanded instances for: tindyguard::session::player/
  @1: (times#1)
  @6: (tindyguard::handshake::respondSending#1)
  @7: @6/(tindyguard::session::updateTx#1)
  @2: (times#7)
  @5: (times#4)
  @3: (times#2)
  @4: (times#3)
  @8: (tindyguard::data::wrapSend#1)
  @9: @8/(tindyguard::session::updateTx#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** player_tindyguard_session_8_23_16_16.c
** Generation date: 2020-03-11T15:14:44
*************************************************************$ */

