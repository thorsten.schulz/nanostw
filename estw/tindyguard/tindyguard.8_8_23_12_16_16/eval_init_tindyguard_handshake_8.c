/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "eval_init_tindyguard_handshake_8.h"

/* tindyguard::handshake::eval_init/ */
void eval_init_tindyguard_handshake_8(
  /* initmsg/ */
  array_uint32_16_4 *initmsg_8,
  /* rlength/ */
  length_t_udp rlength_8,
  /* endpoint/ */
  peer_t_udp *endpoint_8,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_8,
  /* knownPeer/ */
  _4_array *knownPeer_8,
  /* session/ */
  Session_tindyguardTypes *session_8,
  /* soo/ */
  Session_tindyguardTypes *soo_8,
  /* failed/ */
  kcg_bool *failed_8,
  /* noPeer/ */
  kcg_bool *noPeer_8)
{
  array_uint32_16 tmp;
  HashChunk_hash_blake2s tmp1;
  array_uint32_16_1 tmp2;
  array_uint32_16 tmp3;
  array_uint8_16 tmp4;
  array_uint32_16_1 tmp5;
  array_uint32_16 tmp6;
  array_uint32_16_1 tmp7;
  array_uint32_16 tmp8;
  array_uint32_16 tmp9;
  array_uint32_16_2 tmp10;
  array_uint32_16 tmp11;
  array_uint32_16 tmp12;
  array_uint8_16 tmp13;
  array_uint32_16_1 tmp14;
  array_uint32_16 tmp15;
  array_uint32_16_1 tmp16;
  array_uint32_16 tmp17;
  size_tindyguardTypes acc;
  kcg_bool cond_iterw;
  kcg_size idx;
  kcg_bool tmp_selectPeer_it_1_selectPeer_3;
  array_uint32_16 tmp18;
  array_uint32_16 tmp19;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_8;
  /* @3/_L10/,
     @3/their/,
     IfBlock1:then:_L112/,
     IfBlock1:then:_L83/,
     IfBlock1:then:_L97/,
     IfBlock1:then:their_static/ */
  Key32_slideTypes their_static_then_IfBlock1_8;
  /* IfBlock1:then:_L40/,
     IfBlock1:then:_L70/,
     IfBlock1:then:_L89/,
     IfBlock1:then:in_c_tai/ */
  array_uint32_3 _L40_then_IfBlock1_8;
  /* @2/_L10/,
     @2/their/,
     IfBlock1:then:_L120/,
     IfBlock1:then:_L37/,
     IfBlock1:then:_L72/,
     IfBlock1:then:_L90/,
     IfBlock1:then:their_ephemeral/ */
  array_uint32_8 _L37_then_IfBlock1_8;
  /* @4/_L25/,
     @4/u32/,
     IfBlock1:then:_L106/,
     IfBlock1:then:_L36/,
     IfBlock1:then:_L75/,
     IfBlock1:then:in_a_tai/ */
  array_uint32_4 _L36_then_IfBlock1_8;
  /* IfBlock1:then:_L34/ */
  array_uint32_64 _L34_then_IfBlock1_8;
  /* IfBlock1:then:_L31/,
     IfBlock1:then:_L68/,
     IfBlock1:then:_L88/,
     IfBlock1:then:in_c_static/ */
  array_uint32_8 _L31_then_IfBlock1_8;
  /* IfBlock1:then:_L124/ */
  array_uint32_3 _L124_then_IfBlock1_8;
  /* IfBlock1:then:_L122/ */
  array_uint32_16_1 _L122_then_IfBlock1_8;
  /* IfBlock1:then:_L123/ */
  kcg_bool _L123_then_IfBlock1_8;
  /* @2/_L3/, @2/fail/, IfBlock1:then:_L117/ */
  kcg_bool _L117_then_IfBlock1_8;
  /* IfBlock1:then:_L104/ */
  kcg_bool _L104_then_IfBlock1_8;
  /* @3/_L3/, @3/fail/, IfBlock1:then:_L101/ */
  kcg_bool _L101_then_IfBlock1_8;
  /* IfBlock1:then:_L61/ */
  array_uint32_16_1 _L61_then_IfBlock1_8;
  /* IfBlock1:then:_L62/ */
  kcg_bool _L62_then_IfBlock1_8;
  /* IfBlock1:then:_L118/, IfBlock1:then:_L56/, IfBlock1:then:fail/ */
  kcg_bool _L56_then_IfBlock1_8;
  /* IfBlock1:then:_L52/ */
  array_uint32_3 _L52_then_IfBlock1_8;
  size_tindyguardTypes op_call;
  kcg_bool _20_op_call;
  /* @5/_L20/ */
  kcg_bool _L20_selectPeer_3_8;
  /* @5/_L29/ */
  TAI_tindyguardTypes _L29_selectPeer_3_8;
  /* @4/_L15/ */
  kcg_uint32 _L15_stAuth_5;
  /* @4/_L14/ */
  kcg_uint32 _L14_stAuth_5;
  /* @4/_L32/ */
  kcg_uint32 _L32_stAuth_5;
  /* @4/_L30/ */
  kcg_uint32 _L30_stAuth_5;
  /* @4/_L38/ */
  kcg_uint32 _L38_stAuth_5;
  /* @4/_L37/ */
  kcg_uint32 _L37_stAuth_5;
  /* @4/_L46/ */
  kcg_uint32 _L46_stAuth_5;
  /* @4/_L42/ */
  kcg_uint32 _L42_stAuth_5;
  /* @2/_L6/ */
  array_uint32_8_2 _L6_DHDerive_5;
  /* @2/_L5/ */
  array_uint32_16 _L5_DHDerive_5;
  /* @2/_L14/ */
  array_uint32_16_1 _L14_DHDerive_5;
  /* @3/_L6/ */
  array_uint32_8_2 _L6_DHDerive_6;
  /* @3/_L5/ */
  array_uint32_16 _L5_DHDerive_6;
  /* @3/_L14/ */
  array_uint32_16_1 _L14_DHDerive_6;
  /* @1/_L15/ */
  kcg_uint32 _L15_stAuth_6;
  /* @1/_L14/ */
  kcg_uint32 _L14_stAuth_6;
  /* @1/_L32/ */
  kcg_uint32 _L32_stAuth_6;
  /* @1/_L30/ */
  kcg_uint32 _L30_stAuth_6;
  /* @1/_L38/ */
  kcg_uint32 _L38_stAuth_6;
  /* @1/_L37/ */
  kcg_uint32 _L37_stAuth_6;
  /* @1/_L46/ */
  kcg_uint32 _L46_stAuth_6;
  /* @1/_L42/ */
  kcg_uint32 _L42_stAuth_6;

  IfBlock1_clock_8 = rlength_8 == kcg_lit_int32(148) && (*initmsg_8)[0][12] ==
    MsgType_Initiation_tindyguard;
  /* IfBlock1: */
  if (IfBlock1_clock_8) {
    kcg_copy_StreamChunk_slideTypes(&_L34_then_IfBlock1_8[0], &(*initmsg_8)[0]);
    kcg_copy_StreamChunk_slideTypes(&_L34_then_IfBlock1_8[16], &(*initmsg_8)[1]);
    kcg_copy_StreamChunk_slideTypes(&_L34_then_IfBlock1_8[32], &(*initmsg_8)[2]);
    kcg_copy_StreamChunk_slideTypes(&_L34_then_IfBlock1_8[48], &(*initmsg_8)[3]);
    kcg_copy_array_uint32_4(
      &tmp12[0],
      (array_uint32_4 *) &_L34_then_IfBlock1_8[30]);
    tmp4[0] = /* @1/_L8= */(kcg_uint8) tmp12[0];
    tmp4[4] = /* @1/_L26= */(kcg_uint8) tmp12[1];
    tmp4[8] = /* @1/_L34= */(kcg_uint8) tmp12[2];
    tmp4[12] = /* @1/_L41= */(kcg_uint8) tmp12[3];
    _L14_stAuth_6 = tmp12[0] >> kcg_lit_uint32(8);
    tmp4[1] = /* @1/_L7= */(kcg_uint8) _L14_stAuth_6;
    _L15_stAuth_6 = _L14_stAuth_6 >> kcg_lit_uint32(8);
    tmp4[2] = /* @1/_L6= */(kcg_uint8) _L15_stAuth_6;
    tmp4[3] = /* @1/_L5= */(kcg_uint8) (_L15_stAuth_6 >> kcg_lit_uint32(8));
    _L32_stAuth_6 = tmp12[1] >> kcg_lit_uint32(8);
    tmp4[5] = /* @1/_L29= */(kcg_uint8) _L32_stAuth_6;
    _L30_stAuth_6 = _L32_stAuth_6 >> kcg_lit_uint32(8);
    tmp4[6] = /* @1/_L27= */(kcg_uint8) _L30_stAuth_6;
    tmp4[7] = /* @1/_L31= */(kcg_uint8) (_L30_stAuth_6 >> kcg_lit_uint32(8));
    _L37_stAuth_6 = tmp12[2] >> kcg_lit_uint32(8);
    tmp4[9] = /* @1/_L36= */(kcg_uint8) _L37_stAuth_6;
    _L38_stAuth_6 = _L37_stAuth_6 >> kcg_lit_uint32(8);
    tmp4[10] = /* @1/_L33= */(kcg_uint8) _L38_stAuth_6;
    tmp4[11] = /* @1/_L35= */(kcg_uint8) (_L38_stAuth_6 >> kcg_lit_uint32(8));
    _L46_stAuth_6 = tmp12[3] >> kcg_lit_uint32(8);
    tmp4[13] = /* @1/_L44= */(kcg_uint8) _L46_stAuth_6;
    _L42_stAuth_6 = _L46_stAuth_6 >> kcg_lit_uint32(8);
    tmp4[14] = /* @1/_L43= */(kcg_uint8) _L42_stAuth_6;
    tmp4[15] = /* @1/_L45= */(kcg_uint8) (_L42_stAuth_6 >> kcg_lit_uint32(8));
    kcg_copy_array_uint32_8(
      &_L31_then_IfBlock1_8,
      (array_uint32_8 *) &_L34_then_IfBlock1_8[22]);
    kcg_copy_array_uint32_8(
      &_L37_then_IfBlock1_8,
      (array_uint32_8 *) &_L34_then_IfBlock1_8[14]);
    kcg_copy_Hash_hash_blake2s(&tmp[0], &(*sks_8).hcache.hash1);
    kcg_copy_array_uint32_8(&tmp[8], &_L37_then_IfBlock1_8);
    /* IfBlock1:then:_L58=(hash::blake2s::single#7)/ */
    single_hash_blake2s(
      &tmp,
      kcg_lit_int32(64),
      (Hash_hash_blake2s *) &tmp11[0]);
    /* @2/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*sks_8).key,
      &_L37_then_IfBlock1_8,
      kcg_true,
      &_L117_then_IfBlock1_8,
      (Key32_slideTypes *) &_L5_DHDerive_5[0]);
    kcg_copy_array_uint32_8(&tmp3[0], &_L37_then_IfBlock1_8);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_5[idx + 8] = kcg_lit_uint32(0);
      tmp3[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp2[0], &tmp3);
    /* IfBlock1:then:_L64=(hash::blake2s::hkdf_cKonly#3)/ */
    hkdf_cKonly_hash_blake2s_1(
      &tmp2,
      kcg_lit_int32(32),
      (HashChunk_hash_blake2s *) &CONSTRUCTION_chunk_tindyguard,
      &tmp1);
    kcg_copy_array_uint32_16(&_L14_DHDerive_5[0], &_L5_DHDerive_5);
    /* @2/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_5,
      kcg_lit_int32(32),
      &tmp1,
      &_L6_DHDerive_5);
    kcg_copy_array_uint32_8(&tmp6[0], &_L31_then_IfBlock1_8);
    for (idx = 0; idx < 8; idx++) {
      tmp6[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp5[0], &tmp6);
    kcg_copy_array_uint32_8(&tmp8[0], (array_uint32_8 *) &tmp11[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp8[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp7[0], &tmp8);
    for (idx = 0; idx < 3; idx++) {
      _L124_then_IfBlock1_8[idx] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L122=(nacl::box::aeadOpen#5)/ */
    aeadOpen_nacl_box_1_1_20(
      &tmp4,
      &tmp5,
      kcg_lit_int32(32),
      &tmp7,
      kcg_lit_int32(32),
      &_L124_then_IfBlock1_8,
      &_L6_DHDerive_5[1],
      &_L122_then_IfBlock1_8,
      &_L123_then_IfBlock1_8);
    kcg_copy_Key32_slideTypes(
      &their_static_then_IfBlock1_8,
      (Key32_slideTypes *) &_L122_then_IfBlock1_8[0][0]);
    /* @3/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*sks_8).key,
      &their_static_then_IfBlock1_8,
      kcg_true,
      &_L101_then_IfBlock1_8,
      (Key32_slideTypes *) &_L5_DHDerive_6[0]);
    kcg_copy_Hash_hash_blake2s(&tmp9[0], &_L6_DHDerive_5[0]);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_6[idx + 8] = kcg_lit_uint32(0);
      tmp9[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&_L14_DHDerive_6[0], &_L5_DHDerive_6);
    /* @3/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(
      &_L14_DHDerive_6,
      kcg_lit_int32(32),
      &tmp9,
      &_L6_DHDerive_6);
    kcg_copy_array_uint32_4(
      &_L36_then_IfBlock1_8,
      (array_uint32_4 *) &_L34_then_IfBlock1_8[37]);
    tmp13[0] = /* @4/_L8= */(kcg_uint8) _L36_then_IfBlock1_8[0];
    tmp13[4] = /* @4/_L26= */(kcg_uint8) _L36_then_IfBlock1_8[1];
    tmp13[8] = /* @4/_L34= */(kcg_uint8) _L36_then_IfBlock1_8[2];
    tmp13[12] = /* @4/_L41= */(kcg_uint8) _L36_then_IfBlock1_8[3];
    _L14_stAuth_5 = _L36_then_IfBlock1_8[0] >> kcg_lit_uint32(8);
    tmp13[1] = /* @4/_L7= */(kcg_uint8) _L14_stAuth_5;
    _L15_stAuth_5 = _L14_stAuth_5 >> kcg_lit_uint32(8);
    tmp13[2] = /* @4/_L6= */(kcg_uint8) _L15_stAuth_5;
    tmp13[3] = /* @4/_L5= */(kcg_uint8) (_L15_stAuth_5 >> kcg_lit_uint32(8));
    _L32_stAuth_5 = _L36_then_IfBlock1_8[1] >> kcg_lit_uint32(8);
    tmp13[5] = /* @4/_L29= */(kcg_uint8) _L32_stAuth_5;
    _L30_stAuth_5 = _L32_stAuth_5 >> kcg_lit_uint32(8);
    tmp13[6] = /* @4/_L27= */(kcg_uint8) _L30_stAuth_5;
    tmp13[7] = /* @4/_L31= */(kcg_uint8) (_L30_stAuth_5 >> kcg_lit_uint32(8));
    _L37_stAuth_5 = _L36_then_IfBlock1_8[2] >> kcg_lit_uint32(8);
    tmp13[9] = /* @4/_L36= */(kcg_uint8) _L37_stAuth_5;
    _L38_stAuth_5 = _L37_stAuth_5 >> kcg_lit_uint32(8);
    tmp13[10] = /* @4/_L33= */(kcg_uint8) _L38_stAuth_5;
    tmp13[11] = /* @4/_L35= */(kcg_uint8) (_L38_stAuth_5 >> kcg_lit_uint32(8));
    _L46_stAuth_5 = _L36_then_IfBlock1_8[3] >> kcg_lit_uint32(8);
    tmp13[13] = /* @4/_L44= */(kcg_uint8) _L46_stAuth_5;
    _L42_stAuth_5 = _L46_stAuth_5 >> kcg_lit_uint32(8);
    tmp13[14] = /* @4/_L43= */(kcg_uint8) _L42_stAuth_5;
    tmp13[15] = /* @4/_L45= */(kcg_uint8) (_L42_stAuth_5 >> kcg_lit_uint32(8));
    kcg_copy_array_uint32_3(
      &_L40_then_IfBlock1_8,
      (array_uint32_3 *) &_L34_then_IfBlock1_8[34]);
    kcg_copy_array_uint32_8(&tmp11[8], &_L31_then_IfBlock1_8);
    kcg_copy_array_uint32_16(&tmp10[0], &tmp11);
    for (idx = 0; idx < 12; idx++) {
      tmp12[idx + 4] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp10[1], &tmp12);
    /* IfBlock1:then:_L109=(hash::blake2s::hash#3)/ */
    hash_hash_blake2s_2(
      &tmp10,
      kcg_lit_int32(80),
      kcg_lit_int32(0),
      (Hash_hash_blake2s *) &tmp18[0]);
    kcg_copy_array_uint32_3(&tmp15[0], &_L40_then_IfBlock1_8);
    for (idx = 0; idx < 13; idx++) {
      tmp15[idx + 3] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp14[0], &tmp15);
    kcg_copy_array_uint32_8(&tmp17[0], (array_uint32_8 *) &tmp18[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp17[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp16[0], &tmp17);
    for (idx = 0; idx < 3; idx++) {
      _L52_then_IfBlock1_8[idx] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L61=(nacl::box::aeadOpen#6)/ */
    aeadOpen_nacl_box_1_1_20(
      &tmp13,
      &tmp14,
      kcg_lit_int32(12),
      &tmp16,
      kcg_lit_int32(32),
      &_L52_then_IfBlock1_8,
      &_L6_DHDerive_6[1],
      &_L61_then_IfBlock1_8,
      &_L62_then_IfBlock1_8);
    _L56_then_IfBlock1_8 = _L101_then_IfBlock1_8 || _L123_then_IfBlock1_8 ||
      _L117_then_IfBlock1_8 || _L62_then_IfBlock1_8;
    _L104_then_IfBlock1_8 = !_L56_then_IfBlock1_8;
    if (_L104_then_IfBlock1_8) {
      /* @5/_L20=(tindyguardTypes::TAI_validate#1)/ */
      TAI_validate_tindyguardTypes(
        (TAI_tindyguardTypes *) &_L61_then_IfBlock1_8[0][0],
        &_L20_selectPeer_3_8,
        &_L29_selectPeer_3_8);
      op_call = InvalidPeer_tindyguardTypes;
      /* @5/_L3= */
      if (_L20_selectPeer_3_8) {
        /* @5/_L3= */
        for (idx = 0; idx < 8; idx++) {
          acc = op_call;
          cond_iterw = !kcg_comp_Key32_slideTypes(
              &their_static_then_IfBlock1_8,
              &(*knownPeer_8)[idx].tpub);
          tmp_selectPeer_it_1_selectPeer_3 =
            /* @6/_L15=(tindyguardTypes::TAI_le#1)/ */
            TAI_le_tindyguardTypes(
              &_L29_selectPeer_3_8,
              &(*knownPeer_8)[idx].tai);
          /* @6/_L9= */
          if (cond_iterw || tmp_selectPeer_it_1_selectPeer_3) {
            op_call = acc;
          }
          else {
            op_call = /* @5/_L3= */(kcg_int32) idx;
          }
          /* @5/_L3= */
          if (!cond_iterw) {
            break;
          }
        }
      }
      _20_op_call = op_call == InvalidPeer_tindyguardTypes;
      *noPeer_8 = _20_op_call;
    }
    else {
      *noPeer_8 = kcg_false;
    }
    kcg_copy_Session_tindyguardTypes(soo_8, session_8);
    kcg_copy_KeyPair32_slideTypes(
      &(*soo_8).handshake.ephemeral,
      (KeyPair32_slideTypes *) &ZeroKeyPair_slideTypes);
    kcg_copy_Key32_slideTypes(&(*soo_8).handshake.their, &_L37_then_IfBlock1_8);
    kcg_copy_array_uint32_3(&tmp18[8], &_L40_then_IfBlock1_8);
    kcg_copy_array_uint32_4(&tmp18[11], &_L36_then_IfBlock1_8);
    tmp18[15] = kcg_lit_uint32(0);
    /* IfBlock1:then:_L110=(hash::blake2s::single#6)/ */
    single_hash_blake2s(&tmp18, kcg_lit_int32(60), &(*soo_8).handshake.ihash);
    kcg_copy_Hash_hash_blake2s(&tmp19[0], &_L6_DHDerive_6[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp19[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_HashChunk_hash_blake2s(&(*soo_8).handshake.chainingKey, &tmp19);
    (*soo_8).their = _L34_then_IfBlock1_8[13];
    if (_L104_then_IfBlock1_8) {
      (*soo_8).pid = op_call;
      /* @5/_L24= */
      if (_20_op_call) {
        kcg_copy_Peer_tindyguardTypes(
          &(*soo_8).peer,
          (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
      }
      else {
        if (kcg_lit_int32(0) <= op_call && op_call < kcg_lit_int32(8)) {
          kcg_copy_Peer_tindyguardTypes(&(*soo_8).peer, &(*knownPeer_8)[op_call]);
        }
        else {
          kcg_copy_Peer_tindyguardTypes(
            &(*soo_8).peer,
            (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
        }
        kcg_copy_TAI_tindyguardTypes(&(*soo_8).peer.tai, &_L29_selectPeer_3_8);
        kcg_copy_peer_t_udp(&(*soo_8).peer.endpoint, endpoint_8);
      }
    }
    else {
      (*soo_8).pid = kcg_lit_int32(-1);
      kcg_copy_Peer_tindyguardTypes(
        &(*soo_8).peer,
        (Peer_tindyguardTypes *) &EmptyPeer_tindyguardTypes);
    }
    *failed_8 = *noPeer_8 || _L56_then_IfBlock1_8;
  }
  else {
    *noPeer_8 = kcg_false;
    *failed_8 = kcg_true;
    kcg_copy_Session_tindyguardTypes(soo_8, session_8);
  }
}

/*
  Expanded instances for: tindyguard::handshake::eval_init/
  @1: (slideTypes::stAuth#6)
  @2: (tindyguard::handshake::DHDerive#5)
  @3: (tindyguard::handshake::DHDerive#6)
  @4: (slideTypes::stAuth#5)
  @5: (tindyguard::handshake::selectPeer#3)
  @6: @5/(tindyguard::handshake::selectPeer_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_init_tindyguard_handshake_8.c
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

