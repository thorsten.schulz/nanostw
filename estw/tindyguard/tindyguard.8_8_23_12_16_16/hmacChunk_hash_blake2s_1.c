/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "hmacChunk_hash_blake2s_1.h"

/* hash::blake2s::hmacChunk/ */
void hmacChunk_hash_blake2s_1(
  /* _L8/, msg/ */
  array_uint32_16_1 *msg_1,
  /* _L9/, mlen/ */
  int_slideTypes mlen_1,
  /* @1/_L1/, @1/chunk/, @2/_L1/, @2/chunk/, _L32/, key/ */
  HashChunk_hash_blake2s *key_1,
  /* _L20/, chunk/ */
  HashChunk_hash_blake2s *chunk_1)
{
  array_uint32_16_2 tmp;
  kcg_size idx;
  array_uint32_16_2 tmp1;

  /* @1/_L2=, @2/_L2= */
  for (idx = 0; idx < 16; idx++) {
    tmp[0][idx] = (*key_1)[idx] ^ HMAC_outer_xor_hash_libsha;
    tmp1[0][idx] = (*key_1)[idx] ^ HMAC_inner_xor_hash_libsha;
  }
  kcg_copy_StreamChunk_slideTypes(&tmp1[1], &(*msg_1)[0]);
  /* _L1=(hash::blake2s::hashChunk#1)/ */
  hashChunk_hash_blake2s_2_8(
    &tmp1,
    hBlockBytes_hash_blake2s + mlen_1,
    kcg_lit_int32(0),
    &tmp[1]);
  /* _L20=(hash::blake2s::hashChunk#3)/ */
  hashChunk_hash_blake2s_2_8(
    &tmp,
    kcg_lit_int32(96),
    kcg_lit_int32(0),
    chunk_1);
}

/*
  Expanded instances for: hash::blake2s::hmacChunk/
  @1: (slideTypes::xorChunkConst#1)
  @2: (slideTypes::xorChunkConst#2)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hmacChunk_hash_blake2s_1.c
** Generation date: 2020-03-11T15:14:43
*************************************************************$ */

