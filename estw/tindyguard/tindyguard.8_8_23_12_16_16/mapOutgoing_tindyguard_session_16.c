/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapOutgoing_tindyguard_session_16.h"

/* tindyguard::session::mapOutgoing/ */
void mapOutgoing_tindyguard_session_16(
  /* _L7/, sid/ */
  size_tindyguardTypes sid_16,
  /* _L5/, length/ */
  array_int32_16 *length_16,
  /* _L8/, txm_sid/ */
  array_int32_16 *txm_sid_16,
  /* _L2/, length_not_taken/ */
  array_int32_16 *length_not_taken_16,
  /* _L3/, length_picked/ */
  array_int32_16 *length_picked_16)
{
  kcg_size idx;
  /* @1/_L1/ */
  kcg_bool _L1_mapOutgoing_it_3;

  /* _L2= */
  for (idx = 0; idx < 16; idx++) {
    _L1_mapOutgoing_it_3 = (*length_16)[idx] > nil_udp && (*txm_sid_16)[idx] ==
      sid_16;
    /* @1/_L2= */
    if (_L1_mapOutgoing_it_3) {
      (*length_not_taken_16)[idx] = nil_udp;
      (*length_picked_16)[idx] = (*length_16)[idx];
    }
    else {
      (*length_not_taken_16)[idx] = (*length_16)[idx];
      (*length_picked_16)[idx] = nil_udp;
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapOutgoing/
  @1: (tindyguard::session::mapOutgoing_it#3)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapOutgoing_tindyguard_session_16.c
** Generation date: 2020-03-11T15:14:41
*************************************************************$ */

