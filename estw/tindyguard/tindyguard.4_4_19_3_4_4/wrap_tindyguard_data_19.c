/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "wrap_tindyguard_data_19.h"

/* tindyguard::data::wrap/ */
void wrap_tindyguard_data_19(
  /* @1/_L1/, @1/i/, _L65/, len/ */
  size_slideTypes len_19,
  /* msg/ */
  array_uint32_16_19 *msg_19,
  /* s/ */
  Session_tindyguardTypes *s_19,
  /* slength/ */
  length_t_udp *slength_19,
  /* _L75/, soffs/ */
  length_t_udp *soffs_19,
  /* datamsg/ */
  array_uint32_16_20 *datamsg_19,
  /* soo/ */
  Session_tindyguardTypes *soo_19,
  /* _L73/, fail/, failed/ */
  kcg_bool *failed_19)
{
  array_uint32_3 tmp;
  array_uint8_4_4 tmp1;
  kcg_size idx;
  array_uint32_16 tmp2;
  /* _L66/, cmlen/ */
  kcg_int32 _L66_19;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_19;
  /* @2/_L16/, @2/m/, IfBlock1:then:_L29/ */
  StreamChunk_slideTypes _L29_then_IfBlock1_19;
  /* IfBlock1:then:_L26/ */
  array_uint32_16_1 _L26_then_IfBlock1_19;
  /* IfBlock1:then:_L18/ */
  kcg_uint64 _L18_then_IfBlock1_19;
  /* @2/_L6/, @2/a/, @3/_L1/, @3/a/, IfBlock1:then:_L3/ */
  Mac_nacl_onetime _L3_then_IfBlock1_19;
  /* IfBlock1:then:_L4/ */
  array_uint32_16_19 _L4_then_IfBlock1_19;
  /* IfBlock1:then:_L2/ */
  kcg_int32 _L2_then_IfBlock1_19;
  /* @2/_L5/, @3/_L24/, @3/u32/ */
  array_uint32_4 _L24_ldAuth_1_appendAuth_2;

  _L66_19 = /* _L67= */(kcg_int32)
      (/* _L64= */(kcg_uint32) (len_19 - kcg_lit_int32(1)) & kcg_lit_uint32(
          4294967280)) + kcg_lit_int32(16);
  *failed_19 = _L66_19 > kcg_lit_int32(1200) || (*s_19).pid ==
    InvalidPeer_tindyguardTypes || (*s_19).rx_cnt <= kcg_lit_int32(0);
  IfBlock1_clock_19 = !*failed_19 && len_19 >= kcg_lit_int32(0);
  *soffs_19 = kcg_lit_int32(48);
  /* IfBlock1: */
  if (IfBlock1_clock_19) {
    tmp[0] = kcg_lit_uint32(0);
    tmp[1] = /* IfBlock1:then:_L28= */(kcg_uint32) (*s_19).ot_cnt;
    _L18_then_IfBlock1_19 = (*s_19).ot_cnt >> kcg_lit_uint64(32);
    tmp[2] = /* IfBlock1:then:_L27= */(kcg_uint32) _L18_then_IfBlock1_19;
    kcg_copy_StreamChunk_slideTypes(
      &_L26_then_IfBlock1_19[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    /* IfBlock1:then:_L3=(nacl::box::aead#2)/ */
    aead_nacl_box_19_1_20(
      msg_19,
      _L66_19,
      &_L26_then_IfBlock1_19,
      kcg_lit_int32(0),
      &tmp,
      &(*s_19).ot,
      &_L3_then_IfBlock1_19,
      &_L4_then_IfBlock1_19);
    kcg_copy_array_uint8_4(&tmp1[0], (array_uint8_4 *) &_L3_then_IfBlock1_19[0]);
    kcg_copy_array_uint8_4(&tmp1[1], (array_uint8_4 *) &_L3_then_IfBlock1_19[4]);
    kcg_copy_array_uint8_4(&tmp1[2], (array_uint8_4 *) &_L3_then_IfBlock1_19[8]);
    kcg_copy_array_uint8_4(&tmp1[3], (array_uint8_4 *) &_L3_then_IfBlock1_19[12]);
    /* @3/_L24= */
    for (idx = 0; idx < 4; idx++) {
      _L24_ldAuth_1_appendAuth_2[idx] = /* @4/_L14= */(kcg_uint32)
          tmp1[idx][0] | kcg_lsl_uint32(
          /* @4/_L15= */(kcg_uint32) tmp1[idx][1],
          kcg_lit_uint32(8)) | kcg_lsl_uint32(
          /* @4/_L16= */(kcg_uint32) tmp1[idx][2],
          kcg_lit_uint32(16)) | kcg_lsl_uint32(
          /* @4/_L17= */(kcg_uint32) tmp1[idx][3],
          kcg_lit_uint32(24));
    }
    _L2_then_IfBlock1_19 = _L66_19 / StreamChunkBytes_slideTypes;
    if (kcg_lit_int32(0) <= _L2_then_IfBlock1_19 && _L2_then_IfBlock1_19 <
      kcg_lit_int32(19)) {
      kcg_copy_StreamChunk_slideTypes(
        &_L29_then_IfBlock1_19,
        &_L4_then_IfBlock1_19[_L2_then_IfBlock1_19]);
    }
    else {
      kcg_copy_StreamChunk_slideTypes(
        &_L29_then_IfBlock1_19,
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    for (idx = 0; idx < 12; idx++) {
      (*datamsg_19)[0][idx] = kcg_lit_uint32(0);
    }
    (*datamsg_19)[0][12] = MsgType_Data_tindyguard;
    (*datamsg_19)[0][13] = (*s_19).their;
    (*datamsg_19)[0][14] = /* IfBlock1:then:_L28= */(kcg_uint32) (*s_19).ot_cnt;
    (*datamsg_19)[0][15] = /* IfBlock1:then:_L27= */(kcg_uint32)
        _L18_then_IfBlock1_19;
    *slength_19 = _L66_19 + kcg_lit_int32(32);
    kcg_copy_array_uint32_16_19(&(*datamsg_19)[1], &_L4_then_IfBlock1_19);
    /* @2/_L1= */
    switch (_L66_19 % StreamChunkBytes_slideTypes / kcg_lit_int32(16)) {
      case kcg_lit_int32(0) :
        kcg_copy_array_uint32_4(&tmp2[0], &_L24_ldAuth_1_appendAuth_2);
        for (idx = 0; idx < 12; idx++) {
          tmp2[idx + 4] = kcg_lit_uint32(0);
        }
        break;
      case kcg_lit_int32(1) :
        kcg_copy_array_uint32_4(&tmp2[0], (array_uint32_4 *) &_L29_then_IfBlock1_19[0]);
        kcg_copy_array_uint32_4(&tmp2[4], &_L24_ldAuth_1_appendAuth_2);
        for (idx = 0; idx < 8; idx++) {
          tmp2[idx + 8] = kcg_lit_uint32(0);
        }
        break;
      case kcg_lit_int32(2) :
        kcg_copy_array_uint32_8(&tmp2[0], (array_uint32_8 *) &_L29_then_IfBlock1_19[0]);
        kcg_copy_array_uint32_4(&tmp2[8], &_L24_ldAuth_1_appendAuth_2);
        for (idx = 0; idx < 4; idx++) {
          tmp2[idx + 12] = kcg_lit_uint32(0);
        }
        break;
      default :
        kcg_copy_array_uint32_12(
          &tmp2[0],
          (array_uint32_12 *) &_L29_then_IfBlock1_19[0]);
        kcg_copy_array_uint32_4(&tmp2[12], &_L24_ldAuth_1_appendAuth_2);
        break;
    }
    if (kcg_lit_int32(0) <= _L2_then_IfBlock1_19 && _L2_then_IfBlock1_19 <
      kcg_lit_int32(19)) {
      kcg_copy_StreamChunk_slideTypes(
        &(*datamsg_19)[_L2_then_IfBlock1_19 + 1],
        &tmp2);
    }
    kcg_copy_Session_tindyguardTypes(soo_19, s_19);
    (*soo_19).ot_cnt = (*s_19).ot_cnt + kcg_lit_uint64(1);
  }
  else {
    *slength_19 = kcg_lit_int32(-1);
    for (idx = 0; idx < 20; idx++) {
      kcg_copy_StreamChunk_slideTypes(
        &(*datamsg_19)[idx],
        (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    }
    kcg_copy_Session_tindyguardTypes(soo_19, s_19);
  }
}

/*
  Expanded instances for: tindyguard::data::wrap/
  @2: (tindyguard::data::appendAuth#2)
  @3: @2/(slideTypes::ldAuth#1)
  @4: @3/(slideTypes::ld32x1#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** wrap_tindyguard_data_19.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

