/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "mapRekey_tindyguard_session_3.h"

/* tindyguard::session::mapRekey/ */
void mapRekey_tindyguard_session_3(
  /* rekey/ */
  array_int32_3 *rekey_3,
  /* s/ */
  Session_tindyguardTypes *s_3,
  /* pid_cmd_in/ */
  size_tindyguardTypes pid_cmd_in_3,
  /* pid_not_taken/ */
  array_int32_3 *pid_not_taken_3,
  /* pid_cmd/ */
  size_tindyguardTypes *pid_cmd_3)
{
  size_tindyguardTypes acc;
  kcg_size idx;
  /* @1/IfBlock1: */
  kcg_bool IfBlock1_clock_findNonNil_it_1;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_3;

  IfBlock1_clock_3 = pid_cmd_in_3 != nil_tindyguard_session || (*s_3).pid !=
    nil_tindyguard_session;
  /* IfBlock1: */
  if (IfBlock1_clock_3) {
    kcg_copy_array_int32_3(pid_not_taken_3, rekey_3);
    *pid_cmd_3 = pid_cmd_in_3;
  }
  else {
    *pid_cmd_3 = InvalidPeer_tindyguardTypes;
    /* IfBlock1:else:_L7= */
    for (idx = 0; idx < 3; idx++) {
      acc = *pid_cmd_3;
      IfBlock1_clock_findNonNil_it_1 = acc == InvalidPeer_tindyguardTypes;
      /* @1/IfBlock1: */
      if (IfBlock1_clock_findNonNil_it_1) {
        /* @1/IfBlock1:then:_L1= */
        if ((*rekey_3)[idx] != InvalidPeer_tindyguardTypes) {
          *pid_cmd_3 = (*rekey_3)[idx];
        }
        else {
          *pid_cmd_3 = InvalidPeer_tindyguardTypes;
        }
        (*pid_not_taken_3)[idx] = InvalidPeer_tindyguardTypes;
      }
      else {
        *pid_cmd_3 = acc;
        (*pid_not_taken_3)[idx] = (*rekey_3)[idx];
      }
    }
  }
}

/*
  Expanded instances for: tindyguard::session::mapRekey/
  @1: (tindyguard::session::findNonNil_it#1)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** mapRekey_tindyguard_session_3.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

