/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _echoRequest_icmp_19_H_
#define _echoRequest_icmp_19_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "ipsum_BEH_ip_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_uint32 /* seq/ */ seq_19;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_echoRequest_icmp_19;

/* ===========  node initialization and cycle functions  =========== */
/* icmp::echoRequest/ */
extern void echoRequest_icmp_19(
  /* @1/dst/, _L32/, dst/ */
  IpAddress_udp *dst_19,
  /* @1/src/, _L33/, src/ */
  IpAddress_udp *src_19,
  /* _L35/, id/ */
  kcg_uint32 id_19,
  /* _L81/, len/ */
  length_t_udp *len_19,
  /* _L71/, msg/ */
  array_uint32_16_19 *msg_19,
  /* _L26/, seqoo/ */
  kcg_uint32 *seqoo_19,
  outC_echoRequest_icmp_19 *outC);

#ifndef KCG_NO_EXTERN_CALL_TO_RESET
extern void echoRequest_reset_icmp_19(outC_echoRequest_icmp_19 *outC);
#endif /* KCG_NO_EXTERN_CALL_TO_RESET */

#ifndef KCG_USER_DEFINED_INIT
extern void echoRequest_init_icmp_19(outC_echoRequest_icmp_19 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: icmp::echoRequest/
  @1: (ip::hdr#1)
*/

#endif /* _echoRequest_icmp_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** echoRequest_icmp_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

