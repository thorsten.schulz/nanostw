/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "single_hash_blake2s.h"

/* hash::blake2s::single/ */
void single_hash_blake2s(
  /* _L64/, msg/ */
  StreamChunk_slideTypes *msg,
  /* _L66/, len/ */
  size_slideTypes len,
  /* _L72/, hash/ */
  Hash_hash_blake2s *hash)
{
  array_uint32_8 tmp;
  kcg_bool noname;

  tmp[0] = kcg_lit_uint32(1795745351);
  kcg_copy_array_uint32_7(&tmp[1], (array_uint32_7 *) &IV_hash_blake2s[1]);
  /* _L61=(hash::blake2s::stream_it#1)/ */
  stream_it_hash_blake2s(kcg_lit_int32(0), &tmp, msg, len, &noname, hash);
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** single_hash_blake2s.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

