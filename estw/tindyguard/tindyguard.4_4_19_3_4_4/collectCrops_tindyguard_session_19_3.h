/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _collectCrops_tindyguard_session_19_3_H_
#define _collectCrops_tindyguard_session_19_3_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::session::collectCrops/ */
extern void collectCrops_tindyguard_session_19_3(
  /* _L5/, worker_sid/ */
  size_tindyguardTypes worker_sid_19_3,
  /* _L2/, rxmlength/ */
  array_int32_3 *rxmlength_19_3,
  /* _L1/, rxmbuffer/ */
  array_uint32_16_19_3 *rxmbuffer_19_3,
  /* _L4/, length_in/ */
  length_t_udp *length_in_19_3,
  /* _L3/, in/ */
  array_uint32_16_19 *in_19_3);



#endif /* _collectCrops_tindyguard_session_19_3_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** collectCrops_tindyguard_session_19_3.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

