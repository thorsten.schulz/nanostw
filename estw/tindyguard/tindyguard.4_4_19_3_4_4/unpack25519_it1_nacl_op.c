/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "unpack25519_it1_nacl_op.h"

/* nacl::op::unpack25519_it1/ */
kcg_int64 unpack25519_it1_nacl_op(
  /* i/ */
  kcg_uint32 i,
  /* n/ */
  array_uint32_8 *n)
{
  /* IfBlock1: */
  kcg_bool IfBlock1_clock;
  kcg_uint32 idx;
  kcg_uint32 idx1;
  /* o/ */
  kcg_int64 o;

  IfBlock1_clock = i % kcg_lit_uint32(2) == kcg_lit_uint32(0);
  /* IfBlock1: */
  if (IfBlock1_clock) {
    idx = i / kcg_lit_uint32(2);
    if (idx < kcg_lit_uint32(8)) {
      o = /* IfBlock1:then:_L1= */(kcg_int64)
          /* IfBlock1:then:_L4= */(kcg_uint16) (*n)[idx];
    }
    else {
      o = kcg_lit_int64(0);
    }
  }
  else {
    idx1 = i / kcg_lit_uint32(2);
    if (idx1 < kcg_lit_uint32(8)) {
      o = /* IfBlock1:else:_L3= */(kcg_int64) ((*n)[idx1] >> kcg_lit_uint32(16));
    }
    else {
      o = /* IfBlock1:else:_L3= */(kcg_int64)
          (kcg_lit_uint32(0) >> kcg_lit_uint32(16));
    }
  }
  return o;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** unpack25519_it1_nacl_op.c
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

