/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _filter1_tindyguard_data_19_H_
#define _filter1_tindyguard_data_19_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::data::filter1/ */
extern void filter1_tindyguard_data_19(
  /* _L19/, i/ */
  size_tindyguardTypes i_19,
  /* _L21/, acc/ */
  size_tindyguardTypes acc_19,
  /* _L1/, len/ */
  size_tindyguardTypes len_19,
  /* _L2/, msg/ */
  array_uint32_16_19 *msg_19,
  /* _L3/, protocol/ */
  kcg_uint8 protocol_19,
  /* _L4/, goOn/ */
  kcg_bool *goOn_19,
  /* _L20/, ioo/ */
  size_tindyguardTypes *ioo_19,
  /* _L18/, matching/ */
  kcg_bool *matching_19);



#endif /* _filter1_tindyguard_data_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** filter1_tindyguard_data_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

