/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _ipsum_BEH_ip_19_H_
#define _ipsum_BEH_ip_19_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ip::ipsum_BEH/ */
extern kcg_uint32 ipsum_BEH_ip_19(
  /* _L1/, d/ */
  array_uint32_16_19 *d_19,
  /* _L2/, size/ */
  length_t_udp size_19,
  /* _L3/, offs/ */
  length_t_udp offs_19);



#endif /* _ipsum_BEH_ip_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** ipsum_BEH_ip_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

