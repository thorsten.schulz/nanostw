/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "TAI_le_tindyguardTypes.h"

/* tindyguardTypes::TAI_le/ */
kcg_bool TAI_le_tindyguardTypes(
  /* _L1/, x_le/ */
  TAI_tindyguardTypes *x_le,
  /* _L2/, y_le/ */
  TAI_tindyguardTypes *y_le)
{
  /* _L15/, le/ */
  kcg_bool le;

  le = (*x_le)[0] < (*y_le)[0] || ((*x_le)[0] == (*y_le)[0] && ((*x_le)[1] <
        (*y_le)[1] || ((*x_le)[1] == (*y_le)[1] && (*x_le)[2] <= (*y_le)[2])));
  return le;
}



/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** TAI_le_tindyguardTypes.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

