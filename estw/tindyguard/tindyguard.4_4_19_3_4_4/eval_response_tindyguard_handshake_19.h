/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _eval_response_tindyguard_handshake_19_H_
#define _eval_response_tindyguard_handshake_19_H_

#include "kcg_types.h"
#include "aeadOpen_nacl_box_1_1_20.h"
#include "hkdf_hash_blake2s_1_3.h"
#include "hkdf_hash_blake2s_1_2.h"
#include "scalarmultDonna_nacl_box.h"
#include "single_hash_blake2s.h"
#include "hkdf_cKonly_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::handshake::eval_response/ */
extern void eval_response_tindyguard_handshake_19(
  /* s/ */
  Session_tindyguardTypes *s_19,
  /* fail_in/ */
  kcg_bool fail_in_19,
  /* rlength/ */
  length_t_udp rlength_19,
  /* responsemsg/ */
  array_uint32_16_20 *responsemsg_19,
  /* endpoint/ */
  peer_t_udp *endpoint_19,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_19,
  /* soo/ */
  Session_tindyguardTypes *soo_19,
  /* allfail/ */
  kcg_bool *allfail_19);



#endif /* _eval_response_tindyguard_handshake_19_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_response_tindyguard_handshake_19.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

