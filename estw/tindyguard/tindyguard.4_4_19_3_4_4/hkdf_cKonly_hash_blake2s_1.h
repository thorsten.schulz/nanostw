/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _hkdf_cKonly_hash_blake2s_1_H_
#define _hkdf_cKonly_hash_blake2s_1_H_

#include "kcg_types.h"
#include "hmacChunk_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* hash::blake2s::hkdf_cKonly/ */
extern void hkdf_cKonly_hash_blake2s_1(
  /* _L4/, ikm/ */
  array_uint32_16_1 *ikm_1,
  /* _L5/, ikmlen/ */
  size_slideTypes ikmlen_1,
  /* _L3/, chainingKey/ */
  HashChunk_hash_blake2s *chainingKey_1,
  /* @1/hashoo/, @1/okm/, _L14/, _L16/, cKout/ */
  HashChunk_hash_blake2s *cKout_1);

/*
  Expanded instances for: hash::blake2s::hkdf_cKonly/
  @1: (hash::blake2s::hkdf_expand_it1#1)
*/

#endif /* _hkdf_cKonly_hash_blake2s_1_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** hkdf_cKonly_hash_blake2s_1.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

