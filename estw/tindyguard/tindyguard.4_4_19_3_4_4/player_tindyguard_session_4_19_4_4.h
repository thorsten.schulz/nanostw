/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */
#ifndef _player_tindyguard_session_4_19_4_4_H_
#define _player_tindyguard_session_4_19_4_4_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "wrap_tindyguard_data_19.h"
#include "unwrap_tindyguard_data_19.h"
#include "eval_response_tindyguard_handshake_19.h"
#include "initSending_tindyguard_handshake_4.h"
#include "respond_tindyguard_handshake_4.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  kcg_bool init;
  kcg_bool init1;
  kcg_bool init2;
  kcg_bool init3;
  kcg_size /* @1/_/v3/ */ v3_times_1_size;
  kcg_size /* @2/_/v3/ */ v3_times_7_size;
  kcg_size /* @3/_/v3/ */ v3_times_2_size;
  kcg_size /* @4/_/v3/ */ v3_times_3_size;
  kcg_size /* @5/_/v3/ */ v3_times_4_size;
  kcg_bool /* fdfail/ */ fdfail_4_19_4_4;
  kcg_bool /* fail/ */ fail_4_19_4_4;
  _6_SSM_ST_SM1 /* SM1: */ SM1_state_nxt_4_19_4_4;
  SSM_ST_SM2 /* SM2: */ SM2_state_nxt_4_19_4_4;
  Session_tindyguardTypes /* s/ */ s_4_19_4_4;
  /* ---------------------  sub nodes' contexts  --------------------- */
  outC_respond_tindyguard_handshake_4 /* @6/_L4=(tindyguard::handshake::respond#1)/ */ Context_respond_1_respondSending_1[4];
  outC_initSending_tindyguard_handshake_4 /* SM1:initiate:_L25=(tindyguard::handshake::initSending#1)/ */ Context_initSending_1;
  /* ----------------- no clocks of observable data ------------------ */
} outC_player_tindyguard_session_4_19_4_4;

/* ===========  node initialization and cycle functions  =========== */
/* tindyguard::session::player/ */
extern void player_tindyguard_session_4_19_4_4(
  /* _L19/, fd_bad/ */
  kcg_bool fd_bad_4_19_4_4,
  /* rxdlength/ */
  array_int32_4 *rxdlength_4_19_4_4,
  /* rxdbuffer/ */
  array_uint32_16_20_4 *rxdbuffer_4_19_4_4,
  /* rxdendpoint/ */
  _5_array *rxdendpoint_4_19_4_4,
  /* pid/ */
  size_tindyguardTypes pid_4_19_4_4,
  /* txm/ */
  array_uint32_16_19_4 *txm_4_19_4_4,
  /* _L10/, txmlen/ */
  array_int32_4 *txmlen_4_19_4_4,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_4_19_4_4,
  /* knownPeers/ */
  _4_array *knownPeers_4_19_4_4,
  /* socket/ */
  fd_t_udp socket_4_19_4_4,
  /* now/ */
  kcg_int64 now_4_19_4_4,
  /* _L1/, fd_failing/ */
  kcg_bool *fd_failing_4_19_4_4,
  /* rxmlength/ */
  array_int32_4 *rxmlength_4_19_4_4,
  /* rxmbuffer/ */
  array_uint32_16_19_4 *rxmbuffer_4_19_4_4,
  /* taken/ */
  array_bool_4 *taken_4_19_4_4,
  /* _L15/, soo/ */
  Session_tindyguardTypes *soo_4_19_4_4,
  /* _L5/, weakKey/, weakKeyFail/ */
  kcg_bool *weakKeyFail_4_19_4_4,
  /* latchTAI/ */
  kcg_bool *latchTAI_4_19_4_4,
  /* _L14/, timedOut/, timeout/ */
  kcg_bool *timeout_4_19_4_4,
  /* _L27/, reInitPid/ */
  size_tindyguardTypes *reInitPid_4_19_4_4,
  outC_player_tindyguard_session_4_19_4_4 *outC);

extern void player_reset_tindyguard_session_4_19_4_4(
  outC_player_tindyguard_session_4_19_4_4 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void player_init_tindyguard_session_4_19_4_4(
  outC_player_tindyguard_session_4_19_4_4 *outC);
#endif /* KCG_USER_DEFINED_INIT */

/*
  Expanded instances for: tindyguard::session::player/
  @1: (times#1)
  @2: (times#7)
  @3: (times#2)
  @4: (times#3)
  @5: (times#4)
  @6: (tindyguard::handshake::respondSending#1)
*/

#endif /* _player_tindyguard_session_4_19_4_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** player_tindyguard_session_4_19_4_4.h
** Generation date: 2019-10-22T14:01:46
*************************************************************$ */

