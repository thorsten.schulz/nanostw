/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _eval_init_tindyguard_handshake_4_H_
#define _eval_init_tindyguard_handshake_4_H_

#include "kcg_types.h"
#include "hash_hash_blake2s_2.h"
#include "TAI_le_tindyguardTypes.h"
#include "TAI_validate_tindyguardTypes.h"
#include "aeadOpen_nacl_box_1_1_20.h"
#include "hkdf_hash_blake2s_1_2.h"
#include "scalarmultDonna_nacl_box.h"
#include "single_hash_blake2s.h"
#include "hkdf_cKonly_hash_blake2s_1.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguard::handshake::eval_init/ */
extern void eval_init_tindyguard_handshake_4(
  /* initmsg/ */
  array_uint32_16_4 *initmsg_4,
  /* rlength/ */
  length_t_udp rlength_4,
  /* endpoint/ */
  peer_t_udp *endpoint_4,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_4,
  /* knownPeer/ */
  _4_array *knownPeer_4,
  /* session/ */
  Session_tindyguardTypes *session_4,
  /* soo/ */
  Session_tindyguardTypes *soo_4,
  /* failed/ */
  kcg_bool *failed_4,
  /* noPeer/ */
  kcg_bool *noPeer_4);



#endif /* _eval_init_tindyguard_handshake_4_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_init_tindyguard_handshake_4.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

