/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _scalarmult_nacl_box_20_H_
#define _scalarmult_nacl_box_20_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"
#include "assertGoodKey_nacl_box.h"
#include "inv25519_nacl_op.h"
#include "pack25519_nacl_op.h"
#include "scalarmult_it2_nacl_box.h"
#include "M_nacl_op.h"
#include "unpack25519_nacl_op.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* ========================  context type  ========================= */
typedef struct {
  /* -----------------------  no local probes  ----------------------- */
  /* ----------------------- local memories  ------------------------- */
  _8_SSM_ST_SM1 /* SM1: */ SM1_state_nxt_20;
  gf_nacl_op /* x/ */ x_20;
  array_uint8_32 /* z/ */ z_20;
  scalMulAcc_nacl_box /* a/ */ a_20;
  array_uint32_8 /* k/ */ k_20;
  /* -------------------- no sub nodes' contexts  -------------------- */
  /* ----------------- no clocks of observable data ------------------ */
} outC_scalarmult_nacl_box_20;

/* ===========  node initialization and cycle functions  =========== */
/* nacl::box::scalarmult/ */
extern void scalarmult_nacl_box_20(
  /* our/ */
  KeyPair32_slideTypes *our_20,
  /* their/ */
  Key32_slideTypes *their_20,
  /* check/ */
  kcg_bool check_20,
  /* again/ */
  kcg_bool *again_20,
  /* _L1/, q/ */
  array_uint32_8 *q_20,
  /* failed/ */
  kcg_bool *failed_20,
  outC_scalarmult_nacl_box_20 *outC);

extern void scalarmult_reset_nacl_box_20(outC_scalarmult_nacl_box_20 *outC);

#ifndef KCG_USER_DEFINED_INIT
extern void scalarmult_init_nacl_box_20(outC_scalarmult_nacl_box_20 *outC);
#endif /* KCG_USER_DEFINED_INIT */



#endif /* _scalarmult_nacl_box_20_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** scalarmult_nacl_box_20.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

