/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

#include "kcg_consts.h"
#include "kcg_sensors.h"
#include "eval_response_tindyguard_handshake_19.h"

/* tindyguard::handshake::eval_response/ */
void eval_response_tindyguard_handshake_19(
  /* s/ */
  Session_tindyguardTypes *s_19,
  /* fail_in/ */
  kcg_bool fail_in_19,
  /* rlength/ */
  length_t_udp rlength_19,
  /* responsemsg/ */
  array_uint32_16_20 *responsemsg_19,
  /* endpoint/ */
  peer_t_udp *endpoint_19,
  /* sks/ */
  KeySalted_tindyguardTypes *sks_19,
  /* soo/ */
  Session_tindyguardTypes *soo_19,
  /* allfail/ */
  kcg_bool *allfail_19)
{
  array_uint32_16_1 tmp;
  array_uint32_16 tmp1;
  array_uint32_16 tmp2;
  array_uint32_8_2 tmp3;
  array_uint32_16 tmp4;
  array_uint32_8_2 tmp5;
  HashChunk_hash_blake2s tmp6;
  array_uint32_16_1 tmp7;
  array_uint32_16 tmp8;
  array_uint32_16 tmp9;
  array_uint8_16 tmp10;
  array_uint32_16_1 tmp11;
  array_uint32_16 tmp12;
  array_uint32_16 tmp13;
  array_uint32_16 tmp14;
  /* IfBlock1: */
  kcg_bool IfBlock1_clock_19;
  array_uint32_16_1 noname;
  /* IfBlock1:then:_L76/, IfBlock1:then:fail/ */
  kcg_bool fail_then_IfBlock1_19;
  /* @1/_L25/,
     @1/u32/,
     IfBlock1:then:_L24/,
     IfBlock1:then:_L83/,
     IfBlock1:then:in_a_nothing/ */
  array_uint32_4 _L24_then_IfBlock1_19;
  /* IfBlock1:then:_L22/ */
  array_uint32_48 _L22_then_IfBlock1_19;
  /* @2/_L10/,
     @2/their/,
     @3/_L10/,
     @3/their/,
     IfBlock1:then:_L19/,
     IfBlock1:then:_L40/,
     IfBlock1:then:_L59/,
     IfBlock1:then:_L68/,
     IfBlock1:then:_L96/,
     IfBlock1:then:their_ephemeral/ */
  array_uint32_8 _L19_then_IfBlock1_19;
  /* IfBlock1:then:_L87/ */
  array_uint32_8_3 _L87_then_IfBlock1_19;
  /* IfBlock1:then:_L72/ */
  array_uint32_8_2 _L72_then_IfBlock1_19;
  /* IfBlock1:then:_L69/ */
  array_uint32_16_1 _L69_then_IfBlock1_19;
  /* IfBlock1:then:_L58/ */
  array_uint32_3 _L58_then_IfBlock1_19;
  /* @2/_L3/, @2/fail/, IfBlock1:then:_L54/ */
  kcg_bool _L54_then_IfBlock1_19;
  /* IfBlock1:then:_L45/ */
  kcg_bool _L45_then_IfBlock1_19;
  /* @3/_L3/, @3/fail/, IfBlock1:then:_L35/ */
  kcg_bool _L35_then_IfBlock1_19;
  /* @2/_L5/ */
  array_uint32_16 _L5_DHDerive_5;
  /* @2/_L14/ */
  array_uint32_16_1 _L14_DHDerive_5;
  /* @1/_L15/ */
  kcg_uint32 _L15_stAuth_4;
  /* @1/_L14/ */
  kcg_uint32 _L14_stAuth_4;
  /* @1/_L32/ */
  kcg_uint32 _L32_stAuth_4;
  /* @1/_L30/ */
  kcg_uint32 _L30_stAuth_4;
  /* @1/_L38/ */
  kcg_uint32 _L38_stAuth_4;
  /* @1/_L37/ */
  kcg_uint32 _L37_stAuth_4;
  /* @1/_L46/ */
  kcg_uint32 _L46_stAuth_4;
  /* @1/_L42/ */
  kcg_uint32 _L42_stAuth_4;
  /* @3/_L5/ */
  array_uint32_16 _L5_DHDerive_6;
  /* @3/_L14/ */
  array_uint32_16_1 _L14_DHDerive_6;
  kcg_size idx;

  IfBlock1_clock_19 = rlength_19 == kcg_lit_int32(92) &&
    (*responsemsg_19)[0][12] == MsgType_Response_tindyguard;
  /* IfBlock1: */
  if (IfBlock1_clock_19) {
    kcg_copy_StreamChunk_slideTypes(
      &_L69_then_IfBlock1_19[0],
      (StreamChunk_slideTypes *) &ZeroChunk_slideTypes);
    kcg_copy_StreamChunk_slideTypes(
      &_L22_then_IfBlock1_19[0],
      &(*responsemsg_19)[0]);
    kcg_copy_StreamChunk_slideTypes(
      &_L22_then_IfBlock1_19[16],
      &(*responsemsg_19)[1]);
    kcg_copy_StreamChunk_slideTypes(
      &_L22_then_IfBlock1_19[32],
      &(*responsemsg_19)[2]);
    kcg_copy_array_uint32_8(
      &_L19_then_IfBlock1_19,
      (array_uint32_8 *) &_L22_then_IfBlock1_19[15]);
    /* @3/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*sks_19).key,
      &_L19_then_IfBlock1_19,
      kcg_true,
      &_L35_then_IfBlock1_19,
      (Key32_slideTypes *) &_L5_DHDerive_6[0]);
    kcg_copy_array_uint32_4(
      &_L24_then_IfBlock1_19,
      (array_uint32_4 *) &_L22_then_IfBlock1_19[23]);
    tmp10[0] = /* @1/_L8= */(kcg_uint8) _L24_then_IfBlock1_19[0];
    tmp10[4] = /* @1/_L26= */(kcg_uint8) _L24_then_IfBlock1_19[1];
    tmp10[8] = /* @1/_L34= */(kcg_uint8) _L24_then_IfBlock1_19[2];
    tmp10[12] = /* @1/_L41= */(kcg_uint8) _L24_then_IfBlock1_19[3];
    _L14_stAuth_4 = _L24_then_IfBlock1_19[0] >> kcg_lit_uint32(8);
    tmp10[1] = /* @1/_L7= */(kcg_uint8) _L14_stAuth_4;
    _L15_stAuth_4 = _L14_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[2] = /* @1/_L6= */(kcg_uint8) _L15_stAuth_4;
    tmp10[3] = /* @1/_L5= */(kcg_uint8) (_L15_stAuth_4 >> kcg_lit_uint32(8));
    _L32_stAuth_4 = _L24_then_IfBlock1_19[1] >> kcg_lit_uint32(8);
    tmp10[5] = /* @1/_L29= */(kcg_uint8) _L32_stAuth_4;
    _L30_stAuth_4 = _L32_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[6] = /* @1/_L27= */(kcg_uint8) _L30_stAuth_4;
    tmp10[7] = /* @1/_L31= */(kcg_uint8) (_L30_stAuth_4 >> kcg_lit_uint32(8));
    _L37_stAuth_4 = _L24_then_IfBlock1_19[2] >> kcg_lit_uint32(8);
    tmp10[9] = /* @1/_L36= */(kcg_uint8) _L37_stAuth_4;
    _L38_stAuth_4 = _L37_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[10] = /* @1/_L33= */(kcg_uint8) _L38_stAuth_4;
    tmp10[11] = /* @1/_L35= */(kcg_uint8) (_L38_stAuth_4 >> kcg_lit_uint32(8));
    _L46_stAuth_4 = _L24_then_IfBlock1_19[3] >> kcg_lit_uint32(8);
    tmp10[13] = /* @1/_L44= */(kcg_uint8) _L46_stAuth_4;
    _L42_stAuth_4 = _L46_stAuth_4 >> kcg_lit_uint32(8);
    tmp10[14] = /* @1/_L43= */(kcg_uint8) _L42_stAuth_4;
    tmp10[15] = /* @1/_L45= */(kcg_uint8) (_L42_stAuth_4 >> kcg_lit_uint32(8));
    /* @2/_L3=(nacl::box::scalarmultDonna#1)/ */
    scalarmultDonna_nacl_box(
      &(*s_19).handshake.ephemeral,
      &_L19_then_IfBlock1_19,
      kcg_true,
      &_L54_then_IfBlock1_19,
      (Key32_slideTypes *) &_L5_DHDerive_5[0]);
    kcg_copy_psk_tindyguardTypes(&tmp1[0], &(*s_19).peer.preshared);
    for (idx = 0; idx < 8; idx++) {
      _L5_DHDerive_6[idx + 8] = kcg_lit_uint32(0);
      _L5_DHDerive_5[idx + 8] = kcg_lit_uint32(0);
      tmp1[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp[0], &tmp1);
    kcg_copy_array_uint32_8(&tmp8[0], &_L19_then_IfBlock1_19);
    for (idx = 0; idx < 8; idx++) {
      tmp8[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp7[0], &tmp8);
    /* IfBlock1:then:_L75=(hash::blake2s::hkdf_cKonly#3)/ */
    hkdf_cKonly_hash_blake2s_1(
      &tmp7,
      kcg_lit_int32(32),
      &(*s_19).handshake.chainingKey,
      &tmp6);
    kcg_copy_array_uint32_16(&_L14_DHDerive_5[0], &_L5_DHDerive_5);
    /* @2/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(&_L14_DHDerive_5, kcg_lit_int32(32), &tmp6, &tmp5);
    kcg_copy_Hash_hash_blake2s(&tmp4[0], &tmp5[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp4[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&_L14_DHDerive_6[0], &_L5_DHDerive_6);
    /* @3/_L6=(hash::blake2s::hkdf#1)/ */
    hkdf_hash_blake2s_1_2(&_L14_DHDerive_6, kcg_lit_int32(32), &tmp4, &tmp3);
    kcg_copy_Hash_hash_blake2s(&tmp2[0], &tmp3[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp2[idx + 8] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L87=(hash::blake2s::hkdf#5)/ */
    hkdf_hash_blake2s_1_3(
      &tmp,
      kcg_lit_int32(32),
      &tmp2,
      &_L87_then_IfBlock1_19);
    kcg_copy_Hash_hash_blake2s(&tmp9[0], &_L87_then_IfBlock1_19[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp9[idx + 8] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L72=(hash::blake2s::hkdf#6)/ */
    hkdf_hash_blake2s_1_2(
      &_L69_then_IfBlock1_19,
      kcg_lit_int32(0),
      &tmp9,
      &_L72_then_IfBlock1_19);
    kcg_copy_Hash_hash_blake2s(&tmp14[0], &(*s_19).handshake.ihash);
    kcg_copy_array_uint32_8(&tmp14[8], &_L19_then_IfBlock1_19);
    /* IfBlock1:then:_L57=(hash::blake2s::single#6)/ */
    single_hash_blake2s(
      &tmp14,
      kcg_lit_int32(64),
      (Hash_hash_blake2s *) &tmp13[0]);
    kcg_copy_Hash_hash_blake2s(&tmp13[8], &_L87_then_IfBlock1_19[1]);
    /* IfBlock1:then:_L32=(hash::blake2s::single#7)/ */
    single_hash_blake2s(
      &tmp13,
      kcg_lit_int32(64),
      (Hash_hash_blake2s *) &tmp12[0]);
    for (idx = 0; idx < 8; idx++) {
      tmp12[idx + 8] = kcg_lit_uint32(0);
    }
    kcg_copy_array_uint32_16(&tmp11[0], &tmp12);
    for (idx = 0; idx < 3; idx++) {
      _L58_then_IfBlock1_19[idx] = kcg_lit_uint32(0);
    }
    /* IfBlock1:then:_L44=(nacl::box::aeadOpen#3)/ */
    aeadOpen_nacl_box_1_1_20(
      &tmp10,
      &_L69_then_IfBlock1_19,
      kcg_lit_int32(0),
      &tmp11,
      kcg_lit_int32(32),
      &_L58_then_IfBlock1_19,
      &_L87_then_IfBlock1_19[2],
      &noname,
      &_L45_then_IfBlock1_19);
    fail_then_IfBlock1_19 = _L35_then_IfBlock1_19 || _L54_then_IfBlock1_19 ||
      _L45_then_IfBlock1_19;
    /* IfBlock1:then:_L93= */
    if (fail_then_IfBlock1_19) {
      kcg_copy_Session_tindyguardTypes(soo_19, s_19);
    }
    else {
      kcg_copy_secret_tindyguardTypes(&(*soo_19).ot, &_L72_then_IfBlock1_19[0]);
      kcg_copy_secret_tindyguardTypes(&(*soo_19).to, &_L72_then_IfBlock1_19[1]);
      (*soo_19).ot_cnt = kcg_lit_uint64(0);
      (*soo_19).to_cnt = kcg_lit_uint64(0);
      (*soo_19).to_cnt_cache = kcg_lit_uint64(0);
      (*soo_19).our = (*s_19).our;
      (*soo_19).their = _L22_then_IfBlock1_19[13];
      (*soo_19).rx_bytes = kcg_lit_int64(0);
      (*soo_19).rx_cnt = kcg_lit_int32(1);
      (*soo_19).tx_cnt = kcg_lit_int32(0);
      (*soo_19).txTime = kcg_lit_int64(-1);
      (*soo_19).sTime = (*endpoint_19).mtime;
      (*soo_19).pid = (*s_19).pid;
      kcg_copy_State_tindyguard_handshake(
        &(*soo_19).handshake,
        (State_tindyguard_handshake *) &EmptyState_tindyguard_handshake);
      (*soo_19).transmissive = kcg_false;
      (*soo_19).gotKeepAlive = kcg_false;
      (*soo_19).sentKeepAlive = kcg_false;
      kcg_copy_Peer_tindyguardTypes(&(*soo_19).peer, &(*s_19).peer);
      kcg_copy_peer_t_udp(&(*soo_19).peer.endpoint, endpoint_19);
    }
    *allfail_19 = fail_in_19 && fail_then_IfBlock1_19;
  }
  else {
    kcg_copy_Session_tindyguardTypes(soo_19, s_19);
    *allfail_19 = fail_in_19;
  }
}

/*
  Expanded instances for: tindyguard::handshake::eval_response/
  @1: (slideTypes::stAuth#4)
  @2: (tindyguard::handshake::DHDerive#5)
  @3: (tindyguard::handshake::DHDerive#6)
*/

/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** eval_response_tindyguard_handshake_19.c
** Generation date: 2019-10-22T14:01:48
*************************************************************$ */

