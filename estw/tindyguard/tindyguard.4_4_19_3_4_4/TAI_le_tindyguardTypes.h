/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _TAI_le_tindyguardTypes_H_
#define _TAI_le_tindyguardTypes_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguardTypes::TAI_le/ */
extern kcg_bool TAI_le_tindyguardTypes(
  /* _L1/, x_le/ */
  TAI_tindyguardTypes *x_le,
  /* _L2/, y_le/ */
  TAI_tindyguardTypes *y_le);



#endif /* _TAI_le_tindyguardTypes_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** TAI_le_tindyguardTypes.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

