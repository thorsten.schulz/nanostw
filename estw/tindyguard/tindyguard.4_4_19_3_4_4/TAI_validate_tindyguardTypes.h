/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** Command: kcg66.exe -config P:/ew/tindyguard/KCGopt/config.txt
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */
#ifndef _TAI_validate_tindyguardTypes_H_
#define _TAI_validate_tindyguardTypes_H_

#include "kcg_types.h"
#include "kcg_imported_functions.h"

/* =====================  no input structure  ====================== */

/* =====================  no output structure  ====================== */

/* tindyguardTypes::TAI_validate/ */
extern void TAI_validate_tindyguardTypes(
  /* _L1/, x_n/ */
  TAI_tindyguardTypes *x_n,
  /* _L15/, ok/ */
  kcg_bool *ok,
  /* _L52/, x_h/ */
  TAI_tindyguardTypes *x_h);



#endif /* _TAI_validate_tindyguardTypes_H_ */
/* $********** SCADE Suite KCG 32-bit 6.6 (build i19) ***********
** TAI_validate_tindyguardTypes.h
** Generation date: 2019-10-22T14:01:45
*************************************************************$ */

